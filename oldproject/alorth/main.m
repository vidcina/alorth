//
//  main.m
//  alorth
//
//  Created by John Hsu on 2015/5/25.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ALAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ALAppDelegate class]));
    }
}
