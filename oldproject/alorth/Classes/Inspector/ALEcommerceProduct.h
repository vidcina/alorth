//
//  ALEcommerceProduct.h
//  alorth
//
//  Created by w91379137 on 2016/1/21.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ALEcommerceProduct : NSObject

@property(nonatomic ,strong) NSString *productID;           //商品
@property(nonatomic ,strong) NSString *productTitle;        //名稱
@property(nonatomic ,strong) NSString *category;            //分類

@property(nonatomic) float price;                           //單價
@property(nonatomic ,strong) NSString *currency;            //貨幣
@property(nonatomic) NSInteger quantity;                    //數量
@property(nonatomic ,readonly) float totalPrice;            //總價

@property(nonatomic) float revenueRate;                     //稅收比例
@property(nonatomic ,readonly) float profit;                //稅收

- (NSDictionary *)ecommerceProductDictionary:(NSString *)suffix;

@end
