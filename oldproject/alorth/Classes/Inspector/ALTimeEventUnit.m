//
//  ALTimeEventUnit.m
//  alorth
//
//  Created by w91379137 on 2016/2/15.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALTimeEventUnit.h"
#import "Mixpanel.h"

@implementation ALTimeEventUnit

+ (instancetype)startVideoTimeEvent
{
    ALTimeEventUnit *inspectorUnit = [ALTimeEventUnit create];
    return [inspectorUnit startTimeEventID:Track_Event_StreamTime];
}

- (instancetype)startTimeEventID:(NSString *)timeEventID
{
    self.trackName = timeEventID;
    [[Mixpanel sharedInstance] timeEvent:timeEventID];
    return self;
}

- (instancetype)endTimeEventData:(NSDictionary *)dictionary
{
    [self readData:dictionary];
    return self;
}

@end
