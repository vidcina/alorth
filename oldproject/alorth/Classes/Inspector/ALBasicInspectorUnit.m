//
//  ALBasicInspectorUnit.m
//  alorth
//
//  Created by w91379137 on 2015/11/23.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALBasicInspectorUnit.h"

@implementation ALBasicInspectorUnit

+ (nonnull instancetype)create
{
    return [[self alloc] init];
}

- (void)sendInspector
{
    if (self.trackName) {
        [ALInspector logEventTrack:self.trackName
                        Parameters:self.trackDictionary];
    }
    else {
        ALLog(@"%@ %s error",NSStringFromClass([self class]),__func__);
    }
}

-(NSMutableDictionary *)trackDictionary
{
    if (![_trackDictionary isKindOfClass:[NSMutableDictionary class]]) {
        _trackDictionary = [NSMutableDictionary dictionary];
    }
    return _trackDictionary;
}

- (NSMutableArray *)ecommerceProductArray
{
    if (![_ecommerceProductArray isKindOfClass:[NSMutableArray class]]) {
        _ecommerceProductArray = [NSMutableArray array];
    }
    return _ecommerceProductArray;
}

- (nonnull instancetype)readData:(nullable NSDictionary *)dictionary
{
    if ([dictionary isKindOfClass:[NSDictionary class]]) {
        NSArray *keys = dictionary.allKeys;
        for (NSString *key in keys) {
            if (![dictionary[key] isKindOfClass:[NSArray class]] ||
                ![dictionary[key] isKindOfClass:[NSDictionary class]] ||
                ![dictionary[key] isKindOfClass:[NSNull class]]) {
                self.trackDictionary[key] = dictionary[key];
            }
        }
    }
    else {
        ALLog(@"%@ %s error",NSStringFromClass([self class]),__func__);
    }
    return self;
}

@end
