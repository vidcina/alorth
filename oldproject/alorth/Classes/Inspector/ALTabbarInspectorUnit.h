//
//  ALTabbarInspector.h
//  alorth
//
//  Created by w91379137 on 2015/11/23.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALBasicInspectorUnit.h"

//=====.=====.=====.=====畫面=====.=====.=====.=====
#define Track_TabbarClick           @"Track_TabbarClick"
#define Track_Tabbar_Home           @"Home"
//#define Track_Tabbar_Favorite       @"Favorite"
#define Track_Tabbar_Cart           @"Cart"
#define Track_Tabbar_Sell           @"Sell"
#define Track_Tabbar_Chat           @"Chat"
#define Track_Tabbar_Account        @"Account"

@interface ALTabbarInspectorUnit : ALBasicInspectorUnit

- (instancetype)readClassName:(NSString *)className;

@end
