//
//  ALBuyProcessInspectorUnit.m
//  alorth
//
//  Created by w91379137 on 2015/11/23.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALBuyProcessInspectorUnit.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"

@implementation ALBuyProcessInspectorUnit

- (void)setBuyType:(ALBuyType)buyType
{
    _buyType = buyType;
    [self combineTrackName];
}

- (NSString *)prefix
{
    switch (self.buyType) {
        case ALProductBuy: return @"ProductBuy";
        case ALStreamBuy: return @"StreamBuy";
        default:
            break;
    }
    return nil;
}

- (void)setBuyStep:(ALBuyStep)buyStep
{
    _buyStep = buyStep;
    [self combineTrackName];
}

- (NSString *)suffix
{
    switch (self.buyStep) {
        case ALBuyStepDetailPage: return @"DetailPage";
        case ALBuyStepOrderShow: return @"OrderShow";
   
        case ALBuyStepBuyOnlinePayment_ApplePay: return @"OnlinePayment_ApplePay";
        case ALBuyStepBuyOnlinePayment_Stripe: return @"OnlinePayment_Stripe";
        case ALBuyStepBuyOnlinePayment_Authorize: return @"OnlinePayment_Authorize";
        case ALBuyStepBuyOnlinePayment_ChangeCard: return @"OnlinePayment_ChangeCard";
        case ALBuyStepBuyOnlinePayment_Purchase: return @"OnlinePayment_Purchase";
            
        case ALBuyStepBuyDealMySelf_Order: return @"DealMySelf_Order";
        case ALBuyStepBuyDealMySelf_Purchase: return @"DealMySelf_Purchase";

        case ALBuyStepBuyCancel: return @"BuyCancel";
   
            
        case ALV3StreamBuy: return @"V3StreamBuy";
        case ALV3StreamAddCart: return @"V3StreamAddCart";
        case ALV3StreamViewDanmu: return @"V3StreamViewDanmu";
        case ALV3StreamViewProductDetail: return @"V3StreamViewProductDetail";
        case ALV3CartBuy: return @"V3CartBuy";
    
            
        default:
            break;
    }
    return nil;
}

- (void)combineTrackName
{
    NSString *prefix = [self prefix];
    NSString *suffix = [self suffix];
    if (prefix && suffix) {
        self.trackName = [NSString stringWithFormat:@"%@_%@",prefix,suffix];
    }
}

+ (instancetype)logProductBuy:(ALBuyStep)buyStep
                      Product:(ALEcommerceProduct *)product
                      DevDict:(NSDictionary *)devDict
{
    ALBuyProcessInspectorUnit *buyProcess = [ALBuyProcessInspectorUnit create];
    buyProcess.buyType = ALProductBuy;
    buyProcess.buyStep = buyStep;
    
    NSString *suffix = @"";
    if ([MIXPANEL_TOKEN isEqualToString:@"780ad5e683c1d0497c20a7ac35220962"]) {
        [buyProcess readData:devDict];
        suffix = @"TR";
    }
    
    if (product) {
        [buyProcess.ecommerceProductArray addObject:product];
        [buyProcess readData:[product ecommerceProductDictionary:suffix]];
    }
   
    return buyProcess;
}

- (void)sendInspector
{
    [super sendInspector];
    
    //https://developers.google.com/analytics/devguides/collection/ios/v3/enhanced-ecommerce
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    for (NSInteger k = 0; k < self.ecommerceProductArray.count; k++) {
        ALEcommerceProduct *alProduct = [self.ecommerceProductArray objectAtIndex:k];
        
        GAIEcommerceProduct *product = [[GAIEcommerceProduct alloc] init];
        [product setId:alProduct.productID];
        [product setName:alProduct.productTitle];
        [product setCategory:alProduct.category];
        [product setCustomDimension:alProduct.quantity value:@""];
        
        GAIEcommerceProductAction *action = [[GAIEcommerceProductAction alloc] init];
        [action setAction:self.suffix];
        [action setProductActionList:self.prefix];
        
        GAIDictionaryBuilder *builder = [GAIDictionaryBuilder createScreenView];
        [builder setProductAction:action];
        
        // Sets the product for the next available slot, starting with 1
        [builder addProduct:product];
        
        [tracker set:self.trackName value:@"My Impression Screen"];
        [tracker send:[builder build]];
    }
}

@end
