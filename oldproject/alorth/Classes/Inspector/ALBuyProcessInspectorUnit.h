//
//  ALBuyProcessInspectorUnit.h
//  alorth
//
//  Created by w91379137 on 2015/11/23.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALBasicInspectorUnit.h"
@class ALEcommerceProduct;

typedef NS_ENUM(NSUInteger, ALBuyType) {
    ALBuyInit,//未指定
    ALProductBuy,
    ALStreamBuy
};

typedef NS_ENUM(NSUInteger, ALBuyStep) {
    ALBuyStepInit,//未指定
    ALBuyStepDetailPage,//ProductBuy 商品畫面
    ALBuyStepOrderShow,//ProductBuy order畫面 StreamBuy 出現在表單上
    
    ALBuyStepBuyOnlinePayment_ApplePay,
    ALBuyStepBuyOnlinePayment_Stripe,
    ALBuyStepBuyOnlinePayment_Authorize,
    ALBuyStepBuyOnlinePayment_ChangeCard,
    ALBuyStepBuyOnlinePayment_Purchase,
    
    ALBuyStepBuyDealMySelf_Order,//ProductBuy 才有
    ALBuyStepBuyDealMySelf_Purchase,//ProductBuy 才有
    
    ALBuyStepBuyCancel,
    
    
    ALV3StreamBuy,
    ALV3StreamAddCart,
    ALV3StreamViewDanmu,
    ALV3StreamViewProductDetail,
    ALV3CartBuy,
};

#warning 上架前需要再確認以上資料
//=====.=====.=====.=====購買動作=====.=====.=====.=====

@interface ALBuyProcessInspectorUnit : ALBasicInspectorUnit

@property (nonatomic) ALBuyType buyType;
@property (nonatomic) ALBuyStep buyStep;

//ALProductBuy
+ (instancetype)logProductBuy:(ALBuyStep)buyStep
                      Product:(ALEcommerceProduct *)product
                      DevDict:(NSDictionary *)devDict;         //正式MIXPANEL不輸出

//ALStreamBuy

@end
