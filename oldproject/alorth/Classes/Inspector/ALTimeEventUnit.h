//
//  ALTimeEventUnit.h
//  alorth
//
//  Created by w91379137 on 2016/2/15.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALBasicInspectorUnit.h"

//https://mixpanel.com/help/reference/ios
//Timing events

//=====.=====.=====.=====時間記錄=====.=====.=====.=====
#define Track_Event_StreamTime     @"Track_Event_StreamTime"

@interface ALTimeEventUnit : ALBasicInspectorUnit

+ (instancetype)startVideoTimeEvent;
- (instancetype)startTimeEventID:(NSString *)timeEventID;
- (instancetype)endTimeEventData:(NSDictionary *)dictionary;

@end