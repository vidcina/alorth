//
//  ALEcommerceProduct.m
//  alorth
//
//  Created by w91379137 on 2016/1/21.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALEcommerceProduct.h"

@implementation ALEcommerceProduct

- (NSDictionary *)ecommerceProductDictionary
{
    return [self ecommerceProductDictionary:@""];
}

- (NSDictionary *)ecommerceProductDictionary:(NSString *)suffix
{
    NSMutableDictionary *productDictionary = [NSMutableDictionary dictionary];
    [productDictionary setObjectxNil:self.productID forKey:[self longkey:@"ProductID" suffix:suffix]];
    [productDictionary setObjectxNil:self.productTitle forKey:[self longkey:@"ProductTitle" suffix:suffix]];
    [productDictionary setObjectxNil:self.category forKey:[self longkey:@"Category" suffix:suffix]];
    
    [productDictionary setObjectxNil:@(self.price) forKey:[self longkey:@"Price" suffix:suffix]];
    [productDictionary setObjectxNil:self.currency forKey:[self longkey:@"Currency" suffix:suffix]];
    [productDictionary setObjectxNil:@(self.quantity) forKey:[self longkey:@"Quantity" suffix:suffix]];
    [productDictionary setObjectxNil:@(self.totalPrice) forKey:[self longkey:@"TotalPrice" suffix:suffix]];
    
    [productDictionary setObjectxNil:@(self.revenueRate) forKey:[self longkey:@"RevenueRate" suffix:suffix]];
    [productDictionary setObjectxNil:@(self.profit) forKey:[self longkey:@"Profit" suffix:suffix]];
    return productDictionary;
}

- (float)totalPrice
{
    return self.price * self.quantity;
}

- (float)profit
{
    return self.totalPrice * self.revenueRate;
}

- (NSString *)longkey:(NSString *)key suffix:(NSString *)suffix
{
    return [NSString stringWithFormat:@"%@_%@",suffix,key];
}

@end
