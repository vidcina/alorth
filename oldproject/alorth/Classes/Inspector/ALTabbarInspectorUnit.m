//
//  ALTabbarInspector.m
//  alorth
//
//  Created by w91379137 on 2015/11/23.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALTabbarInspectorUnit.h"

//#import "ALHomeViewController.h"
#import "ALHomeV3Controller.h"
//#import "ALFavoriteViewController.h"
#import "ALCartViewController.h"
#import "ALPublishViewController.h"
#import "ALChatTableViewController.h"
#import "ALAccountViewController.h"

@implementation ALTabbarInspectorUnit

- (instancetype)readClassName:(NSString *)className
{
    if ([className isEqualToString:NSStringFromClass([ALHomeV3Controller class])]) self.trackName = Track_Tabbar_Home;
//    if ([className isEqualToString:NSStringFromClass([ALFavoriteViewController class])]) self.trackName = Track_Tabbar_Favorite;
    if ([className isEqualToString:NSStringFromClass([ALCartViewController class])]) self.trackName = Track_Tabbar_Cart;
    if ([className isEqualToString:NSStringFromClass([ALPublishViewController class])]) self.trackName = Track_Tabbar_Sell;
    if ([className isEqualToString:NSStringFromClass([ALChatTableViewController class])]) self.trackName = Track_Tabbar_Chat;
    if ([className isEqualToString:NSStringFromClass([ALAccountViewController class])]) self.trackName = Track_Tabbar_Account;
    if (self.trackName) self.trackDictionary[@"action"] = Track_TabbarClick;
    return self;
}

@end
