//
//  ALBasicInspectorUnit.h
//  alorth
//
//  Created by w91379137 on 2015/11/23.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ALBasicInspectorUnit : NSObject

@property (nonatomic, strong, nullable) NSString *trackName;                    //標準追蹤ID
@property (nonatomic, strong, nonnull) NSMutableDictionary *trackDictionary;    //其他參數
@property (nonatomic, strong, nonnull) NSMutableArray *ecommerceProductArray;   //所有附帶商品

+ (nonnull instancetype)create;

- (void)sendInspector;

- (nonnull instancetype)readData:(nullable NSDictionary *)dictionary;

@end
