//
//  ALInspector.h
//  alorth
//
//  Created by w91379137 on 2015/11/23.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import <Foundation/Foundation.h>

//事件輸入
#import "ALTabbarInspectorUnit.h"
#import "ALBuyProcessInspectorUnit.h"
#import "ALTimeEventUnit.h"

//物品輸入
#import "ALEcommerceProduct.h"

//=====.=====.=====.=====登入狀態(自動帶入)=====.=====.=====.=====
#define Track_identifierForVendor   @"Track_identifierForVendor"
#define Track_isLogin               @"Track_isLogin"
#define Track_UserID                @"Track_UserID"
#define Track_isSimulator           @"Track_isSimulator"
#define Track_Version               @"Track_Version"

//=====.=====.=====.=====登入記錄=====.=====.=====.=====
#define Track_Event_LaunchStaus     @"Track_Event_LaunchStaus"

//=====.=====.=====.=====x=====.=====.=====.=====

@interface ALInspector : NSObject

+(void)startSession;

//暫時版本 只有Google
+(void)logEventCategory:(NSString *)category
                 Action:(NSString *)action
                  Label:(NSString *)label
                  Value:(NSNumber *)value;

//暫時版本 只有Mixpanel
+(void)logEventTrack:(NSString *)track
          Parameters:(NSDictionary *)parameters;

@end
