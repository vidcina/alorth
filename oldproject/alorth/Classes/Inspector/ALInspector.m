//
//  ALInspector.m
//  alorth
//
//  Created by w91379137 on 2015/11/23.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALInspector.h"
#import "Mixpanel.h"

#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import <Intercom/Intercom.h>
#import <AppsFlyer/AppsFlyer.h>

@implementation ALInspector

+(void)startSession
{
    //Mixpanel
    [Mixpanel sharedInstanceWithToken:MIXPANEL_TOKEN];
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    
    if ([ALAppDelegate sharedAppDelegate].userID) {
        mixpanel.nameTag = [ALAppDelegate sharedAppDelegate].userID;
        
        [mixpanel createAlias:[ALAppDelegate sharedAppDelegate].userID
                forDistinctID:mixpanel.distinctId];
        [mixpanel identify:mixpanel.distinctId];
        
        [mixpanel.people set:@{@"Name":[ALAppDelegate sharedAppDelegate].userID,
                               @"Plan": @"Premium"}];
        
    }
    
    //Google
    [GAI sharedInstance].dispatchInterval = 10;
    [GAI sharedInstance].trackUncaughtExceptions = NO;
    __strong static id<GAITracker> tracker = nil;
    tracker = [[GAI sharedInstance] trackerWithName:@"AimcueIOS" trackingId:@"UA-77277604-2"];
    
    [GAI sharedInstance].defaultTracker = tracker;
    tracker.allowIDFACollection = YES;
}

+(void)logEventCategory:(NSString *)category
                 Action:(NSString *)action
                  Label:(NSString *)label
                  Value:(NSNumber *)value
{
    //Mixpanel
    //ALLog(@"Mixpanel logEventCategory no implementation");
    
    //Google
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:category
                                                          action:action
                                                           label:label
                                                           value:value] build]];
    
    // Intercom
    NSMutableDictionary *metaData = [NSMutableDictionary dictionary];
    [metaData setObjectPreventNil:@"category" object:category];
    [metaData setObjectPreventNil:@"label" object:label];
    [metaData setObjectPreventNil:@"value" object:value];

    [Intercom logEventWithName:action metaData:metaData];
    
    // AppsFlyer
    [[AppsFlyerTracker sharedTracker] trackEvent:action withValues:metaData];
}

+(void)logEventTrack:(NSString *)track
          Parameters:(NSDictionary *)parameters
{
    if (![parameters isKindOfClass:[NSDictionary class]]) parameters = nil;
    
    
    //Mixpanel
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:track
         properties:[self logDictionaryPlugIn:parameters]];
    
    //Google
    //ALLog(@"Google logEventTrack no implementation");
    
    // intercom
    [Intercom logEventWithName:track
                      metaData:[self logDictionaryPlugIn:parameters]];
    
    // AppsFlyer
    [[AppsFlyerTracker sharedTracker] trackEvent:track withValues:[self logDictionaryPlugIn:parameters]];

#pragma mark experiment
//    Mixpanel *mixpanel = [Mixpanel sharedInstance];
//    [mixpanel track:@"VideoPlayStart"
//         properties:@{@"date":@"2016-01-06 20:00:00"}];

    
//    Mixpanel *mixpanel = [Mixpanel sharedInstance];
//    [mixpanel track:@"VideoPlayStop"
//         properties:@{@"length":@(3600)}];
//
//    [mixpanel track:@"VideoPlayStop"
//         properties:@{@"length":@(3600)}];
//
//    [mixpanel track:@"VideoPlayStop"
//         properties:@{@"length":@(3600)}];

}

+ (NSDictionary *)logDictionaryPlugIn:(NSDictionary *)dataDictionary
{
    if (![dataDictionary isKindOfClass:[NSDictionary class]]) dataDictionary = @{};
    
    NSMutableDictionary *newDictionary = [dataDictionary mutableCopy];
    
    newDictionary[Track_isLogin] = [PDSAccountManager isLoggedIn] ? @"Yes" : @"No";
    newDictionary[Track_isSimulator] = TARGET_IPHONE_SIMULATOR ? @"Yes" : @"No";
    newDictionary[Track_Version] = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    
    return newDictionary;
}

@end
