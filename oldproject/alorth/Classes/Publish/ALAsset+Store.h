//
//  ALAsset+Store.h
//  alorth
//
//  Created by w91379137 on 2015/7/1.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>

@interface ALAsset (Store)

-(void)turnToRealPath:(NSString *)aPath;
-(void)turnThumbnailToRealPath:(NSString *)aPath;

@end
