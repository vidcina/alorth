//
//  ALAsset+Store.m
//  alorth
//
//  Created by w91379137 on 2015/7/1.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALAsset+Store.h"

@implementation ALAsset (Store)

-(void)turnToRealPath:(NSString *)aPath;
{
    ALAssetRepresentation *rep = [self defaultRepresentation];
    unsigned long dataSize = (unsigned long)rep.size;
    Byte *buffer = (Byte*)malloc(dataSize);
    NSUInteger buffered = [rep getBytes:buffer fromOffset:0.0 length:dataSize error:nil];
    NSData *dty = [NSData dataWithBytesNoCopy:buffer length:buffered freeWhenDone:NO];
    [dty writeToFile:aPath atomically:YES];
    
    #warning TODO:Deal with large file use: http://stackoverflow.com/a/4878870
}

-(void)turnThumbnailToRealPath:(NSString *)aPath
{
    UIImage *image = [UIImage imageWithCGImage:[self thumbnail]];
    NSData *imageData = UIImagePNGRepresentation(image);
    [imageData writeToFile:aPath atomically:YES];
}

@end
