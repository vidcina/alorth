//
//  ALPublishViewController.h
//  alorth
//
//  Created by papayabird on 2015/5/26.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicMainViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "SKTagView.h"
#import "SKTag.h"

#import "ALPublishConfirmViewController.h"

typedef NS_OPTIONS(NSUInteger, ALPublishViewControllerSelect) {
    ALPublishViewControllerSelectNone,
    ALPublishViewControllerSelectMainCategory,
    ALPublishViewControllerSelectSubCategory
};

@interface ALPublishViewController : ALBasicSubViewController
<UITableViewDataSource, UITableViewDelegate>
{
    BOOL isReadyToPop;
    
    ALAssetsLibrary *assetLibrary;
    
    NSMutableArray *tagArray;
    NSArray *subCategoryArray;
    
    NSString *realVideoFilePath;
    NSString *realImageFilePath;
}

@property (nonatomic, strong) IBOutlet UIView *titleBarView;
@property (nonatomic, strong) IBOutlet UIScrollView *mainScrollView;
@property (nonatomic, strong) IBOutlet UIView *mainScrollViewContainer;

#pragma mark - 資料
@property (nonatomic, strong) NSURL *mediaURL;
@property (nonatomic, strong) IBOutlet UIImageView *kvo_productImage; //參照mediaURL
@property (nonatomic, strong) IBOutlet UITextField *productNameField;

@property (nonatomic, strong) NSString *mainCategoryString;
@property (nonatomic, strong) NSString *subCategoryString;
@property (nonatomic, strong) IBOutlet UITextView *productDescriptionTextView;

#pragma mark - 分類
@property (nonatomic) ALPublishViewControllerSelect select;
@property (nonatomic, strong) IBOutlet UIButton *mainCategoryButton; //參照mainCategoryString
@property (nonatomic, strong) IBOutlet UIButton *subCategoryButton; //參照subCategoryString

#pragma mark - 選單
@property (nonatomic, strong) IBOutlet UIView *categorySelectContainer;
@property (nonatomic, strong) IBOutlet UILabel *categoryTitleLabel;
@property (nonatomic, strong) IBOutlet UITableView *categoryTableView;
@property (nonatomic, strong) NSArray *categoryArray;

#pragma mark - 關鍵字
@property (nonatomic, strong) IBOutlet SKTagView *tagView;

@end
