//
//  ALPublishViewController.m
//  alorth
//
//  Created by papayabird on 2015/5/26.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALPublishViewController.h"
#import "ALAsset+Store.h"
#import "ALBroadcastMainViewController.h"

static const float BigTagHeight = 20.0;
static const float SmallTagHeight = BigTagHeight * 3 / 4;

@interface ALPublishViewController ()

@end

@implementation ALPublishViewController

#pragma mark - VC Life
- (instancetype)init
{
    self = [super init];
    if (self) {
        if (!tagArray) {
            tagArray = [NSMutableArray array];
        }
        isReadyToPop = NO;
        if (!assetLibrary) {
            assetLibrary = [[ALAssetsLibrary alloc] init];
        }
        
        self.categoryArray = [NSMutableArray array];
        
        self.mainCategoryString = nil;
        self.subCategoryString = nil;
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.mainScrollViewContainer.superview == nil) {
        
        [self.mainScrollView addSubview:self.mainScrollViewContainer];
        
        [self.mainScrollViewContainer mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(self.mainScrollView.mas_width);
            
            make.top.equalTo(self.mainScrollView.mas_top);
            make.bottom.equalTo(self.mainScrollView.mas_bottom);
            make.leading.equalTo(self.mainScrollView.mas_leading);
            make.trailing.equalTo(self.mainScrollView.mas_trailing);
        }];
        
        self.tagView.padding    = UIEdgeInsetsMake(3, 10, 3, 10);
        self.tagView.insets     = 5;
        self.tagView.lineSpace  = 8;
        
        __block typeof(self) __weak weakself = self;
        self.tagView.didClickTagAtIndex = ^(NSUInteger index){
            [weakself checkToDeleteTag:index];
        };
    }
    
    [self createNewProductImage];
}

-(void)viewWillAppearFirstTime:(BOOL)animated
{
    [super viewWillAppearFirstTime:animated];
    [[ALPublishSwitchView sharedInstance].selectScrollView selectIndex:0]; //預設打開直賣
}

#pragma mark - Photo Method
-(void)createNewProductImage
{
    if (_mediaURL == nil && isReadyToPop == NO) {
        __block typeof(self) __weak weakself = self;
        [[PDSEnvironmentViewController sharedInstance] requestMakeMediaCompletionBlock:^(NSURL *assetURL, NSError *error) {
            if ([assetURL isKindOfClass:[NSURL class]]) {
                //有效圖案
                weakself.mediaURL = assetURL;
            }
            else {
                //無效圖案
                isReadyToPop = YES;
                
                //如果選到變換種類 就push
                if ([ALPublishSwitchView sharedInstance].publishStatus == LiveSell) {
                    
                    ALBroadcastMainViewController *vc =
                    [[ALBroadcastMainViewController alloc] init];
                    
                    NSMutableArray *viewControllers =
                    [weakself.navigationController.viewControllers mutableCopy];
                    
                    [viewControllers removeLastObject];
                    [viewControllers addObject:vc];
                    
                    [weakself.navigationController setViewControllers:viewControllers];
                }
                else {
                    [weakself.navigationController popViewControllerAnimated:NO];
                }
            }
        }];
    }
}

#pragma mark - IBAction_分類
-(IBAction)selectMainCategory:(UIButton *)sender
{
    [self keyboardReturn];
    
    self.select = ALPublishViewControllerSelectMainCategory;
    
    self.categoryArray =
    [[ALBundleTextResources categoryArray] valueForKey:@"CategoryEn"];
    
    [self.categoryTableView reloadData];
    [self showCategorySelectContainer];
}

-(IBAction)selectSubCategory:(UIButton *)sender
{
    [self keyboardReturn];

    self.select = ALPublishViewControllerSelectSubCategory;
    
    if ([subCategoryArray isKindOfClass:[NSArray class]]) {
        self.categoryArray = [subCategoryArray mutableCopy];
    }
    else {
        self.categoryArray = [NSMutableArray array];
    }
    
    [self.categoryTableView reloadData];
    [self showCategorySelectContainer];
}

-(void)showCategorySelectContainer
{
    [self.view addSubview:self.categorySelectContainer];
    [self alTableViewSetting:self.categoryTableView];
    
    [self.categorySelectContainer mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleBarView.mas_bottom).offset(-44);
        make.bottom.equalTo(self.view.mas_bottom);
        make.leading.equalTo(self.view.mas_leading);
        make.trailing.equalTo(self.view.mas_trailing);
    }];
}

-(IBAction)hiddenCategorySelectContainer
{
    [self.categorySelectContainer removeFromSuperview];
}

#pragma mark - IBAction_關鍵字
-(IBAction)addTagAction
{
    __block typeof(self) __weak weakself = self;
    [YKBlockAlert alertWithTitle:ALLocalizedString(@"Enter your tag", nil)
                defaultInputText:@""
                         button1:ALLocalizedString(@"Cancel", nil)
                         button2:ALLocalizedString(@"OK", nil)
                          block1:^(NSString *theKey) {
                              
                          }
                          block2:^(NSString *theKey) {
                              [weakself checkToAddTag:theKey];
                          }];
}

-(void)checkToAddTag:(NSString *)theKey
{
    if ([tagArray containsObject:theKey]) {
        ALLog(@"tag重複");
    }
    else {
        SKTag *tag = [SKTag tagWithText:theKey];
        
        NSMutableAttributedString *string =
        [[NSMutableAttributedString alloc] initWithString:
         [NSString stringWithFormat:@" %@",theKey]];
        
        {
            //製圖
            UIImage *tagImage = [UIImage imageNamed:@"tag"];
            tagImage = [UIImage reSizeImage:tagImage
                                     toSize:CGSizeMake(SmallTagHeight / tagImage.size.height * tagImage.size.width,
                                                       SmallTagHeight)];
            
            //設定
            NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
            textAttachment.image = tagImage;
            
            //安插
            [string replaceCharactersInRange:NSMakeRange(0, 0) withAttributedString:[NSAttributedString attributedStringWithAttachment:textAttachment]];
            
            [string replaceCharactersInRange:NSMakeRange(0, 0) withAttributedString:[[NSMutableAttributedString alloc] initWithString:@" "]];
        }
        
        NSRange range = NSMakeRange(0,string.length);
        [string addAttribute:NSForegroundColorAttributeName
                       value:COMMON_GRAY4_COLOR
                       range:range];
        [string addAttribute:NSFontAttributeName
                       value:[UIFont systemFontOfSize:15]
                       range:range];
        
        tag.attributedText = string;
        tag.bgColor = COMMON_GRAY1_COLOR;
        tag.padding = UIEdgeInsetsMake(3.0, 0.0, 3.0, 5.0);
        [self.tagView addTag:tag];
        [tagArray addObject:theKey];
    }
}

-(void)checkToDeleteTag:(NSUInteger)index
{
    [YKBlockAlert alertWithTitle:ALLocalizedString(@"Removal confirmation", nil)
                         message:ALLocalizedString(@"Are you sure you want to remove the tag?", nil)
                blocksAndButtons:
     ^{
         [self.tagView removeTagAtIndex:index];
         [tagArray removeObjectAtIndex:index];
     },
     ALLocalizedString(@"Remove", nil),
     ^{},
     ALLocalizedString(@"Keep", nil), nil];
}

#pragma mark - IBAction_步驟
-(IBAction)previousStepAction
{
    [YKBlockAlert alertWithTitle:ALLocalizedString(@"Confirm",nil) message:ALLocalizedString(@"Abort stock creation?", nil) blocksAndButtons:^{
        _mediaURL = nil;
        [self createNewProductImage];
    },ALLocalizedString(@"Abort",nil),^{},ALLocalizedString(@"Continue Editing",nil), nil];
}

-(IBAction)nextStepAction
{
    if (self.productNameField.text.length == 0) {
        YKSimpleAlert(ALLocalizedString(@"Please enter a product name", nil));
        return;
    }
    
    if (self.mainCategoryString == nil) {
        YKSimpleAlert(ALLocalizedString(@"Please choose a category", nil));
        return;
    }
    
    if (self.subCategoryString == nil) {
        YKSimpleAlert(ALLocalizedString(@"No sub-category", nil));
        return;
    }
    
    [self addMBProgressHUDWithKey:@"checkProductExistA008"];
    [ALNetWorkAPI checkProductExistA008ProductTitle:self.productNameField.text
                                        ProductSize:self.subCategoryString
                                           Category:[self indexOfMainCategory]
                                    CompletionBlock:^(NSDictionary *responseObject) {
        [self removeMBProgressHUDWithKey:@"checkProductExistA008"];
                                            
        NSArray *productSizeArray = [_subCategoryString componentsSeparatedByString:@", "];

        if(![responseObject[@"resHeader"][@"TXID"] isEqualToString:@"A008"]) {
            YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
            return ;
        }
        else if ([responseObject[@"resBody"][@"ProductID"] length] == 0 &&
                 [responseObject[@"resHeader"][@"TXID"] isEqualToString:@"A008"]) {
            //無此商品 新增此筆商品
            [self productMediaStoreStep];
        }
        else {
            [YKBlockAlert alertWithTitle:ALLocalizedString(@"$Same product already exists$. $Will use the currently existing product$.", nil)
                                 message:nil
                        blocksAndButtons:^{
                            
                NSString *productID = responseObject[@"resBody"][@"ProductID"];
                NSString *productImageURL = responseObject[@"resBody"][@"ProductImage"];
                [self addMBProgressHUDWithKey:@"ProductPublishLoadProductImage"];
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    
                    NSData *imageData = [ALNetWorkAPI cachedImageFromURL:productImageURL];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self removeMBProgressHUDWithKey:@"ProductPublishLoadProductImage"];
                        ALPublishConfirmViewController *vc =
                        [[ALPublishConfirmViewController alloc] initWithProductID:productID
                                                                      productSize:productSizeArray
                                                                      productName:_productNameField.text
                                                                     productImage:[UIImage imageWithData:imageData]
                                                                  productImageURL:nil];
                        vc.publishDelegate = self;
                        [self.navigationController pushViewController:vc animated:YES];
                    });
                });
                
            },ALLocalizedString(@"Continue", nil), nil];
        }
    }];
}

-(void)productMediaStoreStep
{
    [self addMBProgressHUDWithKey:@"productMediaStoreStep"];

    //檢查上載暫存資料夾
    NSString *cachesImageFileDirPath = [NSHomeDirectory() stringByAppendingPathComponent:@"/Library/Caches/upload/"];
    [ALNetWorkAPI createCachesImageFileDir:cachesImageFileDirPath];
    
    //初始化
    realImageFilePath = nil;
    realVideoFilePath = nil;
    
    __block typeof(self) __weak weakself = self;
        [assetLibrary assetForURL:_mediaURL resultBlock:^(ALAsset *asset) {
            [self removeMBProgressHUDWithKey:@"productMediaStoreStep"];

            if ([[asset valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypePhoto]) {
                
                realImageFilePath = [NSString stringWithFormat:@"%@/%u.jpg",cachesImageFileDirPath,arc4random_uniform(9999)];
                [asset turnToRealPath:realImageFilePath];
                
                {//限制圖片大小
                    NSFileManager *fileManager = [NSFileManager defaultManager];
                    UIImage *image = [UIImage imageWithData:[fileManager contentsAtPath:realImageFilePath]];
                    image = [self limitImageLength:image];
                    NSData *imageData = UIImageJPEGRepresentation(image, 0.8);
                    [imageData writeToFile:realImageFilePath atomically:YES];
                }
                
                ALLog(@"轉存圖像地址 %@",realImageFilePath);
                [weakself performSelector:@selector(productMediaUploadStep) withObject:nil afterDelay:1];
//                [weakself performSelectorOnMainThread:@selector(productMediaUploadStep) withObject:nil waitUntilDone:YES];
            }
            else if ([[asset valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo]) {
                
                realImageFilePath = [NSString stringWithFormat:@"%@/%u.jpg",cachesImageFileDirPath,arc4random_uniform(9999)];
                [asset turnThumbnailToRealPath:realImageFilePath];
                ALLog(@"轉存圖像地址 %@",realImageFilePath);
                
                realVideoFilePath = [NSString stringWithFormat:@"%@/%u.mp4",cachesImageFileDirPath,arc4random_uniform(9999)];
                [asset turnToRealPath:realVideoFilePath];
                ALLog(@"轉存影片地址 %@",realVideoFilePath);
                [weakself performSelector:@selector(productMediaUploadStep) withObject:nil afterDelay:1];
//                [weakself performSelectorOnMainThread:@selector(productMediaUploadStep) withObject:nil waitUntilDone:YES];
            }
            else {
                ALLog(@"媒體格式錯誤");
            }
            
        } failureBlock:^(NSError *error) {
            ALLog(@"媒體轉存失敗");
            [self removeMBProgressHUDWithKey:@"productMediaStoreStep"];
            
        }];
}

-(void)productMediaUploadStep
{
    __block typeof(self) __weak weakself = self;
    
    //初始化
    __block NSString *imageURLString = nil;
    __block NSString *videoURLString = nil;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        //連續api 初始設定
        __block int apiTryTimes = 0;
        __block BOOL isSuccess = YES;
        __block double defaultDelayInSeconds = 0.5;                 //失敗延遲
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        
        if (isSuccess && realImageFilePath) {
            apiTryTimes = 0;
            while (apiTryTimes < 2) {
                
                double delayInSeconds = defaultDelayInSeconds;
                if (apiTryTimes == 0) {
                    delayInSeconds = 0;
                }
                
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    
                    [self addMBProgressHUDWithKey:@"uploadImage"];
                    
                    // Amazon
                    NSLog(@"Amazon Update Image start:%@",[NSDate date]);
                    [[ALAmazonUploadManager sharedManager] uploadImageFilePath:realImageFilePath
                                                              progressCallback:nil
                                                               completionBlock:^(NSString *uploadedURLString) {
                                                                   [self removeMBProgressHUDWithKey:@"uploadImage"];
                                                                   NSLog(@"Amazon Update Image finish:%@",[NSDate date]);
                                                                   
                                                                   if (uploadedURLString.length > 0) {
                                                                       [self deleteFile:realImageFilePath];
                                                                       imageURLString = uploadedURLString;
                                                                       isSuccess = YES;
                                                                   }
                                                                   else {
                                                                       isSuccess = NO;
                                                                   }
                                                                   dispatch_semaphore_signal(sema);
                                                               }];
                });
                dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
                
                if (isSuccess == YES) {
                    break;
                }
                else {
                    apiTryTimes ++;
                    ALLog(@"uploadImage 重試%d",apiTryTimes);
                }
            }
        }
        
        if (isSuccess && realVideoFilePath) {
            apiTryTimes = 0;
            while (apiTryTimes < 2) {
                
                double delayInSeconds = defaultDelayInSeconds;
                if (apiTryTimes == 0) {
                    delayInSeconds = 0;
                }
                
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    
                    UIView *view = [PDSEnvironmentViewController sharedInstance].view;
                    MBProgressHUD *testhub = [[MBProgressHUD alloc] initWithView:view];
                    testhub.mode = MBProgressHUDModeDeterminate;
                    [view addSubview:testhub];
                    [testhub show:YES];
                    
//                    __block typeof(testhub) __weak weakhub = testhub;
                    
                    // Amazon
                    NSLog(@"Amazon Update Video start:%@",[NSDate date]);
                    [[ALAmazonUploadManager sharedManager] uploadVideoFilePath:realVideoFilePath
                                                              progressCallback:nil
                                                               completionBlock:^(NSString *uploadedURLString) {
                                                                   [testhub removeFromSuperview];
                                                                   NSLog(@"Amazon Update Video finish:%@",[NSDate date]);
                                                                   
                                                                   if (uploadedURLString.length > 0) {
                                                                       [self deleteFile:realVideoFilePath];
                                                                       videoURLString = uploadedURLString;
                                                                       isSuccess = YES;
                                                                   }
                                                                   else {
                                                                       isSuccess = NO;
                                                                   }
                                                                   dispatch_semaphore_signal(sema);
                                                               }];
                });
                dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
                
                if (isSuccess == YES) {
                    break;
                }
                else {
                    apiTryTimes ++;
                    ALLog(@"uploadVideo 重試%d",apiTryTimes);
                }
            }
        }
        
        if (isSuccess) {
            [weakself productDataWriteDownStepImage:imageURLString
                                              Video:videoURLString];
        }
        else {
            YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        }
    });
}

-(void)deleteFile:(NSString *)aPath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:aPath]) {
        NSError *error;
        if ([[NSFileManager defaultManager] isDeletableFileAtPath:aPath]) {
            BOOL success = [[NSFileManager defaultManager] removeItemAtPath:aPath error:&error];
            if (!success) {
                ALLog(@"Error removing file at path: %@", error.localizedDescription);
            }
        }
    }
}

-(void)productDataWriteDownStepImage:(NSString *)imageURLString
                               Video:(NSString *)videoURLString
{
    [self addMBProgressHUDWithKey:@"addStockA005"];
    [ALNetWorkAPI addStockA005ProductID:nil
                            productClip:videoURLString
                           productImage:imageURLString
                            productSize:self.subCategoryString
                     productDescription:self.productDescriptionTextView.text
                               Category:[self indexOfMainCategory]
                           ProductTitle:self.productNameField.text
                                   Tags:tagArray
                        CompletionBlock:^(NSDictionary *responseObjectA005) {
                            [self removeMBProgressHUDWithKey:@"addStockA005"];
                            
                            if(![responseObjectA005[@"resHeader"][@"TXID"] isEqualToString:@"A005"]) {
                                YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
                                return ;
                            }
                            NSString *productID = responseObjectA005[@"resBody"][@"ProductID"];
                            
                            NSArray *productSizeArray = [_subCategoryString componentsSeparatedByString:@", "];
                            ALPublishConfirmViewController *vc =
                            [[ALPublishConfirmViewController alloc] initWithProductID:productID
                                                                          productSize:productSizeArray
                                                                          productName:_productNameField.text
                                                                         productImage:_kvo_productImage.image
                                                                      productImageURL:nil];
                            
                            vc.publishDelegate = self;
                            
                            [self.navigationController pushViewController:vc animated:YES];
                        }];
}

#pragma mark - UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _categoryArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@""];
    
    NSString *orString = self.categoryArray[indexPath.row];
    
    cell.textLabel.localText = orString;
    cell.textLabel.textColor = COMMON_GRAY4_COLOR;
    return cell;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.select == ALPublishViewControllerSelectMainCategory) {
        
        self.mainCategoryString =
        [[[ALBundleTextResources categoryArray] valueForKey:@"CategoryEn"] objectAtIndex:indexPath.row];
        
        id safeTest = [ALBundleTextResources categoryArray].safe[indexPath.row][@"GoodsSize"];
        if (safeTest) {
            subCategoryArray = [ALBundleTextResources categoryArray][indexPath.row][@"GoodsSize"];
        }
        else {
            subCategoryArray = nil;
        }
    }
    else if (self.select == ALPublishViewControllerSelectSubCategory) {
        self.subCategoryString = subCategoryArray.safe[indexPath.row];
    }
    [self hiddenCategorySelectContainer];
}

#pragma mark - Keyboard Auto System
-(void)keyboardDidShow:(NSNotification *)notification
{
    [super keyboardDidShow:notification];
    if (editFirstResponder == self.productDescriptionTextView) {
        float keyboardHeight = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
        self.mainScrollView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0);
    }
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    self.mainScrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    [super keyboardWillHide:notification];
}

#pragma mark - Setter / Getter
-(void)setMediaURL:(NSURL *)mediaURL
{
    _mediaURL = mediaURL;
    
    if (![mediaURL isKindOfClass:[NSURL class]]) {
        self.kvo_productImage.image = nil;
        return;
    }
    
    [assetLibrary assetForURL:mediaURL
                  resultBlock:^(ALAsset *asset) {
        self.kvo_productImage.image = [UIImage imageWithCGImage:[asset aspectRatioThumbnail]];
    }
                 failureBlock:^(NSError *error) {
        self.kvo_productImage.image = nil;
    }];
}

-(void)setMainCategoryString:(NSString *)mainCategoryString
{
    if ([_mainCategoryString isEqualToString:mainCategoryString]) {
        return;
    }
    _mainCategoryString = mainCategoryString;
    self.mainCategoryButton.normalTitle = mainCategoryString;
    
    self.subCategoryString = nil;
    self.subCategoryButton.enabled = YES;
}

-(void)setSubCategoryString:(NSString *)subCategoryString
{
    _subCategoryString = subCategoryString;
    
    if (subCategoryString) {
        self.subCategoryButton.normalTitle = subCategoryString;
    }
    else {
        self.subCategoryButton.normalTitle = @"Size";
    }
}

- (NSString *)indexOfMainCategory
{
    NSArray *categoryEn = [[ALBundleTextResources categoryArray] valueForKey:@"CategoryEn"];
    NSInteger index = [categoryEn indexOfObject:self.mainCategoryString];
    
    return [[ALBundleTextResources categoryArray] valueForKey:@"CategoryIndex"][index];
}

@end
