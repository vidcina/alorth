//
//  ALPublishConfirmViewController.h
//  alorth
//
//  Created by w91379137 on 2015/6/22.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicSubViewController.h"

#import "ALProductDashboard.h"

@class ALPublishViewController;

@interface ALPublishConfirmViewController : ALBasicSubViewController <ALProductDashboardSelectDelegate>

- (instancetype)initWithProductID:(NSString *)pid
                      productSize:(NSArray *)ps
                      productName:(NSString *)aName
                     productImage:(UIImage *)image
                  productImageURL:(NSString *)imageURL;

@property (nonatomic,strong) NSMutableArray *productStockArray;
@property (nonatomic,strong) NSString *productID;
@property (nonatomic,strong) NSArray *productSize;
@property (nonatomic,strong) NSString *productName;

@property (nonatomic,strong) UIImage *productImage;
@property (nonatomic,strong) NSString *productImageURL;

// isUsed/ Size selected from other product's SELL button
@property (nonatomic,strong) NSString *presetIsUsed;
@property (nonatomic,strong) NSString *presetSize;

@property (weak) ALPublishViewController *publishDelegate;

@property (nonatomic, strong) IBOutlet UIScrollView *mainScrollView;

#pragma mark - 類型選擇
@property (nonatomic, strong) IBOutlet ALProductDashboard *productView;

@property (nonatomic, strong) UIView *priceStockView;

@end
