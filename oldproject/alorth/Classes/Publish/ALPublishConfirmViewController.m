//
//  ALPublishConfirmViewController.m
//  alorth
//
//  Created by w91379137 on 2015/6/22.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALPublishConfirmViewController.h"
#import "ALPublishViewController.h"

#import "ALMyStockListViewController.h"

@interface ALPublishConfirmViewController ()

@end

@implementation ALPublishConfirmViewController

#pragma mark - VC Life
- (instancetype)init
{
    self = [super init];
    if (self) {
        NSAssert(NO, @"Use initWithProductID: instead");
    }
    return self;
}

- (instancetype)initWithProductID:(NSString *)pid
                      productSize:(NSArray *)ps
                      productName:(NSString *)aName
                     productImage:(UIImage *)image
                  productImageURL:(NSString *)imageURL
{
    self = [super init];
    if (self) {
        self.productID = pid;
        self.productSize = ps;
        self.productName = aName;
        self.productImage = image;
        self.productImageURL = imageURL;
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.productView.superview == nil) {
        [self.mainScrollView masSetContentView:self.productView isVerticalFix:NO];
        self.productView.stepButtonView.hidden = YES; //自己產生
        self.productView.productID = self.productID;
    }
    
    //圖片
    if (self.productImage) {
        self.productView.productImageView.image = self.productImage;
    }
    else if (self.productImageURL) {
        [self.productView.productImageView imageFromURLString:self.productImageURL];
    }
    
    //標題
    self.productView.productNameLabel.text = self.productName;
    
    //種類
    self.productView.usedSelectTypeScrollView.infoObjectArray = [@[@"New",@"Used"] mutableCopy];
    self.productView.sizeSelectTypeScrollView.infoObjectArray = [self.productSize mutableCopy];
    
    //價格數量框框
    self.priceStockView = self.productView.priceStockView;
    
    //讀取資料
    typeof(self) weakSelf = self;
    [self addMBProgressHUDWithKey:@"getStockListA007"];
    [ALNetWorkAPI getStockListA007ProductID:self.productID CompletionBlock:^(NSDictionary *responseObject) {
        [self removeMBProgressHUDWithKey:@"getStockListA007"];
        
        NSPredicate *filterMyself =
        [NSPredicate predicateWithFormat:@"%K == %@",@"SellerID",[[ALAppDelegate sharedAppDelegate] userID]];
        
        self.productStockArray =
        [[responseObject[@"resBody"][@"StockList"] filteredArrayUsingPredicate:filterMyself] mutableCopy];
        
        [weakSelf selectFirst];
    }];
}

-(void)selectFirst
{
    {
        ALSelectTypeScrollView *usedSelect = _productView.usedSelectTypeScrollView;
        [usedSelect selectView:usedSelect.typeViewArray.firstObject];
    }
    {
        ALSelectTypeScrollView *sizeSelect = _productView.sizeSelectTypeScrollView;
        NSInteger index = 0;
        
        for (int k = 0; k < sizeSelect.infoObjectArray.count; k++) {
            NSString *key = sizeSelect.infoObjectArray[k];
            if ([key isEqualToString:_presetSize]) {
                index = k;
                break;
            }
        }
        
        [sizeSelect selectIndex:index];
    }
}

#pragma mark - ALProductViewDelegate
-(void)didSelectUsed:(NSString *)used Size:(NSString *)size sender:(ALProductDashboard *)sender
{
    ALLog(@"選中 select : %@, %@, %@",sender.productID,used,size);
    [self keyboardReturn];
    
    NSPredicate *usedPredicate = [NSPredicate predicateWithFormat:@"%K == %@",apiKeyIsUsed,used];
    NSPredicate *sizePredicate = [NSPredicate predicateWithFormat:@"%K == %@",apiKeySize,size];
    
    NSPredicate *concatPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[sizePredicate, usedPredicate]];
    NSArray *stockArray = [self.productStockArray filteredArrayUsingPredicate:concatPredicate];
    
    NSMutableDictionary *fitDataDict = nil;
    if ([stockArray count] == 0) {
        fitDataDict = [NSMutableDictionary dictionary];
        
        //新增物品
        fitDataDict[@"Currency"] = [ALAppDelegate sharedAppDelegate].currency;
        fitDataDict[apiKeyIsUsed] = used;
        fitDataDict[apiKeySize] = size;
        fitDataDict[@"Amount"] = @(0);
        fitDataDict[@"Price"] = @(0.00);
    }
    else {
        fitDataDict = stockArray[0];
    }
    //ALLog(@"資料進入畫面: %@",fitDataDict);
    //ALLog(@"全部資料: %@",self.productStockArray);
    _productView.infoDict = fitDataDict;
}

-(void)dataDidChange:(NSDictionary *)infoDict sender:(ALProductDashboard *)sender
{
    ALLog(@"改變 dataDidChange : %@",infoDict);
    
    NSPredicate *usedPredicate =
    [NSPredicate predicateWithFormat:@"%K == %@",apiKeyIsUsed,infoDict[apiKeyIsUsed]];
    NSPredicate *sizePredicate =
    [NSPredicate predicateWithFormat:@"%K == %@",apiKeySize,infoDict[apiKeySize]];
    
    NSPredicate *concatPredicate =
    [NSCompoundPredicate andPredicateWithSubpredicates:@[sizePredicate, usedPredicate]];
    NSArray *stockArray = [self.productStockArray filteredArrayUsingPredicate:concatPredicate];
    
    #warning TODO:看看能不能濾掉 原本是 0 則不新增 也不上傳 有空再做 複製一份原本的 拿來核對就知道了
    if ([stockArray count] == 0) {
        NSMutableDictionary *newStockDict = [NSMutableDictionary dictionary];
        newStockDict = [infoDict mutableCopy];
        [self.productStockArray addObject:newStockDict];
    }
    else {
        NSDictionary *oldStockDict = stockArray[0];
        NSMutableDictionary *newStockDict = [oldStockDict mutableCopy];
        
        newStockDict[@"Price"]  = infoDict[@"Price"];
        newStockDict[@"Amount"] = infoDict[@"Amount"];
        
        NSInteger replaceIndex = [self.productStockArray indexOfObject:oldStockDict];
        [self.productStockArray replaceObjectAtIndex:replaceIndex withObject:newStockDict];
    }
}

-(void)errorMag:(NSString *)errorMag sender:(ALProductDashboard *)sender
{
    YKSimpleAlert(errorMag);
}

#pragma mark - IBAction_步驟
-(IBAction)removeAction:(UIButton *)sender
{
    [self keyboardReturn];
    self.publishDelegate.mediaURL = nil;
    
    [YKBlockAlert alertWithTitle:ALLocalizedString(@"Warning", nil)
                         message:ALLocalizedString(@"All stocks will be deleted", nil)
                blocksAndButtons:
     ^{
         [self addMBProgressHUDWithKey:@"deleteAllStockA027ProductID"];
         [ALNetWorkAPI deleteAllStockA027ProductID:self.productID
                                   completionBlock:^(NSDictionary *responseObject) {
                                       [self removeMBProgressHUDWithKey:@"deleteAllStockA027ProductID"];
                                       [self.navigationController popViewControllerAnimated:YES];
                                   }];
     },
     ALLocalizedString(@"Yes",nil),//左邊
     ^{
         
     },
     ALLocalizedString(@"No",nil),//右邊
     nil];
}

-(IBAction)saveAction:(UIButton *)sender
{
    [self keyboardReturn];
    
    [self addMBProgressHUDWithKey:@"ALPublishConfirmViewController saveAction"];
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        for (NSDictionary *stockDict in self.productStockArray) {
            ALLog(@"輸入%@,%@,%@,%@,%@",self.productID,stockDict[apiKeyIsUsed],stockDict[apiKeySize],stockDict[@"Amount"],stockDict[@"Price"]);
            [ALNetWorkAPI editStockA026ProductID:self.productID
                                         stockID:stockDict[@"StockID"]
                                          isUsed:stockDict[apiKeyIsUsed]
                                            size:stockDict[apiKeySize]
                                           price:stockDict[@"Price"]
                                          amount:stockDict[@"Amount"]
                                 completionBlock:^(NSDictionary *responseObject) {
                                     
                                     if([responseObject.safe[@"resHeader"][@"TXID"] isEqualToString:@"A026"] &&
                                        [responseObject.safe[@"resBody"][@"ResultCode"] intValue] == 0) {
                                         ALLog(@"輸出成功%@",stockDict);
                                     }
                                     else {
                                         ALLog(@"輸出失敗%@",stockDict);
                                     }
                                     dispatch_semaphore_signal(sema);
                                     
            }];
            dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self removeMBProgressHUDWithKey:@"ALPublishConfirmViewController saveAction"];
            [YKBlockAlert alertWithTitle:ALLocalizedString(@"Success",nil)
                                 message:ALLocalizedString(@"",nil)
                        blocksAndButtons:^{
                            self.publishDelegate.mediaURL = nil;
                            ALNavigationViewController *nv = (ALNavigationViewController *)self.navigationController;
                            [nv clearOtherController:ALNavigationSubClear
                                                push:ALNavigationTabBarSetting
                                       subController:@[[[ALMyStockListViewController alloc] init]]];
                        },ALLocalizedString(@"OK",nil), nil];
        });
    });
}

#pragma mark - IBAction_價格數目
-(void)textFieldChangeAction:(UITextField *)sender
{
    [_productView writeChange];
}

#pragma mark - Keyboard Auto System
-(void)keyboardDidShow:(NSNotification *)notification
{
    [super keyboardDidShow:notification];
    
    float keyboardHeight = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    float keyBoardYPosition = self.view.frame.size.height - keyboardHeight;
    
    CGRect windowRect = [_priceStockView convertRect:_priceStockView.bounds toView:self.view.window];
    
    float upShift = windowRect.origin.y + windowRect.size.height - keyBoardYPosition;
    if (upShift > 0) {
        CATransform3D transform = CATransform3DTranslate(CATransform3DIdentity, 0, - upShift, 0);
        
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             _priceStockView.layer.transform = transform;
                         } completion:^(BOOL finished) {}];
    }
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         _priceStockView.layer.transform = CATransform3DIdentity;
                     } completion:^(BOOL finished) {}];
    [super keyboardWillHide:notification];
}

@end
