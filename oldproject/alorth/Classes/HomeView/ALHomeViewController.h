//
//  ALHomeViewController.h
//  alorth
//
//  Created by w91379137 on 2015/5/25.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicMainViewController.h"

@interface ALHomeViewController : ALBasicMainViewController
<UISearchBarDelegate>
{
    //搜尋條
    IBOutlet UISearchBar *topSearchBar;
}

@property (nonatomic, strong) IBOutlet UIView *searchResultContainerView;

@end