//
//  ALHomeViewController.m
//  alorth
//
//  Created by w91379137 on 2015/5/25.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALHomeViewController.h"
#import "ALDiscoverListViewController.h"

#import "ALComplexSearchViewController.h"

@interface ALHomeViewController ()
{
    ALDiscoverListViewController *discoverListVC;
    NSArray *applyButtonConstraints;
}

@end

@implementation ALHomeViewController

#pragma mark - VC Life
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (discoverListVC == nil) {
        discoverListVC = [[ALDiscoverListViewController alloc] init];
        [self addChildViewController:discoverListVC];
        
        [discoverListVC addSafeObserver:self
                             forKeyPath:@"discoverStatus"
                                options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew
                                context:NULL];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (discoverListVC.view.superview == nil) {
        [self.searchResultContainerView addSubview:discoverListVC.view];
        [discoverListVC didMoveToParentViewController:self];
        [discoverListVC.view mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(discoverListVC.view.superview);
        }];
    }
}

#pragma mark - Constraint
- (void)adjustContainerView
{
    BOOL shouldUp = (discoverListVC.discoverStatus == DiscoverMap);
    
    if (shouldUp) {
        if (applyButtonConstraints == nil) {
            applyButtonConstraints = [self.searchResultContainerView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.view.mas_top);
            }];
        }
    }
    
    for (MASConstraint *obj in applyButtonConstraints) {
        if([obj isKindOfClass:MASConstraint.class]) {
            if (shouldUp) {
                [obj install];
            }
            else {
                [obj uninstall];
            }
        }
    }
    
    [UIView animateKeyframesWithDuration:0.1
                                   delay:0.0
                                 options:UIViewKeyframeAnimationOptionBeginFromCurrentState
                              animations:^{
                                  [self.view layoutIfNeeded];
                              } completion:^(BOOL finished) {}];
    
    if (discoverListVC.discoverStatus == DiscoverMap &&
        topSearchBar.isFirstResponder) {
        [topSearchBar resignFirstResponder];
    }
}

#pragma mark - Observer
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if (object == discoverListVC) {
        if ([change[@"old"] integerValue] != 0) {
            [self adjustContainerView];
        }
    }
    else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

#pragma mark - UISearchBarDelegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    if (searchBar.inputAccessoryView == nil) {
        searchBar.inputAccessoryView = [searchBar keyboardReturnToolbar];
    }
    return YES;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [topSearchBar resignFirstResponder];
    
    ALComplexSearchViewController *next =
    [[ALComplexSearchViewController alloc] init];
    
    [next firstSearchingText:searchBar.text];
    [self.navigationController pushViewController:next animated:NO];
    searchBar.text = @""; //清空搜尋
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = NO;
    [topSearchBar resignFirstResponder];
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = YES;
    editFirstResponder = searchBar;
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = NO;
}

@end