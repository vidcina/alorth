//
//  ALProductEditHistoryViewController.m
//  alorth
//
//  Created by w91379137 on 2015/9/22.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALProductEditHistoryViewController.h"

#import "ALProductEditHistoryCell.h"
#import "ALMessageTableViewCell.h"

@interface ALProductEditHistoryViewController ()
{
    ALProductEditHistoryCell *productDemoCell;
    ALMessageTableViewCell *messageDemoCell;
}

@end

@implementation ALProductEditHistoryViewController

#pragma mark - init
-(instancetype)init
{
    self = [super init];
    if (self) {
        objectListArray = [NSMutableArray array];
        
        currentTableViewControl = [[ALTableViewControl alloc] init];
        currentTableViewControl.delegate = self;
        currentTableViewControl.apiDataArray = objectListArray;
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self alTableViewSetting:mainTableView];
    
    if (objectListArray.count == 0) {
        [self checkNewData:currentTableViewControl];
    }
}

-(void)viewWillAppearFirstTime:(BOOL)animated
{
    [super viewWillAppearFirstTime:animated];
    [headerView.backButton addTarget:self
                              action:@selector(back:)
                    forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return objectListArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        if (productDemoCell == nil) {
            productDemoCell = [ALProductEditHistoryCell cellFromXib];
            productDemoCell.frame = CGRectMake(0, 0, tableView.contentSize.width, 0);
            [productDemoCell layoutIfNeeded];
        }
        
        return [ALProductEditHistoryCell performCell:productDemoCell
                                        withinfoDict:nil];
    }
    
    NSDictionary *infoDict = objectListArray[indexPath.section];
    if (indexPath.row == 1) {
        if (messageDemoCell == nil) {
            messageDemoCell = [ALMessageTableViewCell cellFromXib];
            messageDemoCell.frame = CGRectMake(0, 0, tableView.contentSize.width, 0);
            [messageDemoCell layoutIfNeeded];
        }
        return [ALMessageTableViewCell performCell:messageDemoCell
                               withProductInfoDict:infoDict];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *infoDict = objectListArray[indexPath.section];
    if (indexPath.row == 0) {
        ALProductEditHistoryCell *cell = [ALProductEditHistoryCell cellFromXib];
        [ALProductEditHistoryCell performCell:cell withinfoDict:infoDict];
        return cell;
    }
    if (indexPath.row == 1) {
        ALMessageTableViewCell *cell = [ALMessageTableViewCell cellFromXib];
        [ALMessageTableViewCell performCell:cell
                        withProductInfoDict:infoDict];
        return cell;
    }
    return [self defaultUITableViewCell];
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *upIndexPath = [NSIndexPath indexPathForRow:0 inSection:indexPath.section];
    ALProductEditHistoryCell *cell = (ALProductEditHistoryCell *)[tableView cellForRowAtIndexPath:upIndexPath];
    [[PDSEnvironmentViewController sharedInstance] showBigImage:cell.productImageView.image];
}


#pragma mark - scrollView Delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == mainTableView) {
        [currentTableViewControl scrollViewDidScroll:scrollView];
    }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView == mainTableView) {
        [currentTableViewControl scrollViewDidEndDragging:scrollView willDecelerate:(BOOL)decelerate];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == mainTableView) {
        [currentTableViewControl scrollViewDidEndDecelerating:(UIScrollView *)scrollView];
    }
}

#pragma mark - ALTableViewControlDelegate
-(void)checkNewData:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    willReplaceWithNew = YES;
    tableViewControl.lastDict[@"StartNum"] = @"0";
    
    ALLog(@"重新整理");
    [self addMBProgressHUDWithKey:@"EditProductListA036"];
    [self listLoadApiSourceParameters:tableViewControl.lastDict
                          FinishBlock:^(BOOL isSuccess) {
                              [self removeMBProgressHUDWithKey:@"EditProductListA036"];
                          }];
}

-(void)loadMore:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    
    ALLog(@"載入更多:目前第 %@ 頁 %lu 筆資料",tableViewControl.lastDict[@"StartNum"],(unsigned long)objectListArray.count);
    
    tableViewControl.lastDict[@"StartNum"] =
    [NSString stringWithFormat:@"%d",[tableViewControl.lastDict[@"StartNum"] intValue] + 1];
    
    [self addMBProgressHUDWithKey:@"EditProductListA036"];
    [self listLoadApiSourceParameters:tableViewControl.lastDict
                          FinishBlock:^(BOOL isSuccess) {
                              [self removeMBProgressHUDWithKey:@"EditProductListA036"];
                          }];
}

#pragma mark - API
-(void)listLoadApiSourceParameters:(NSDictionary *)parameters
                       FinishBlock:(void (^)(BOOL isSuccess))finishBlockToRun
{
    [ALNetWorkAPI reviewEditProductListA036ProductID:self.infoDict[@"ProductID"]
                                            StartNum:parameters[@"StartNum"]
                                      CompletionBlock:^(NSDictionary *responseObject) {
                                          
                                          BOOL isWiseData = YES;
                                          if (![responseObject[@"resBody"] isKindOfClass:[NSDictionary class]]) {
                                              isWiseData = NO;
                                              
                                              if (![responseObject[@"resBody"][@"ProductList"] isKindOfClass:[NSArray class]]) {
                                                  isWiseData = NO;
                                              }
                                          }
                                          
                                          if (isWiseData) {
                                              if (willReplaceWithNew) {
                                                  [objectListArray removeAllObjects];
                                                  willReplaceWithNew = NO;
                                              }
                                              
                                              NSArray *addArray = [responseObject[@"resBody"][@"ProductList"] mutableCopy];
                                              if (addArray.count > 0) {
                                                  [objectListArray addObjectsFromArray:addArray];
                                              }
                                              [mainTableView reloadData];
                                          }
                                          else {
                                              ALLog(@"goodsA036 格式錯誤");
                                          }
                                          finishBlockToRun(YES);
                                      }];
}

@end
