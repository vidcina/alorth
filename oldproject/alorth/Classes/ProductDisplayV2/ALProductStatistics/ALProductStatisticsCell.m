//
//  ALProductStatisticsCell.m
//  alorth
//
//  Created by w91379137 on 2015/9/22.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALProductStatisticsCell.h"

@implementation ALProductStatisticsCell

#pragma mark - Cell Life
+(ALProductStatisticsCell *)cell
{
    ALProductStatisticsCell *cell =
    [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class])
                                   owner:nil
                                 options:nil] lastObject];
    return cell;
}

+ (CGFloat)performCell:(ALProductStatisticsCell *)cell
        withDictionary:(NSDictionary *)dictionary
{
    cell.conditionLabel.text = [dictionary[apiKeyIsUsed] intValue] == 0 ?
    ALLocalizedString(@"New", nil) : ALLocalizedString(@"Used", nil);
    cell.sizeLabel.text = dictionary[apiKeySize];
    cell.priceLabel.text = [NSString stringWithFormat:@"%@ %@",dictionary[@"Price"],dictionary[@"Currency"]];
    cell.sellerAccountLabel.text = dictionary[@"UserID"];
    
    NSDateFormatter *formatter =
    [ALFormatter dateFormatteWithTimeZone:[NSTimeZone systemTimeZone]
                             FormatString:@"yyyy-MM-dd' 'HH:mm:ss"];
    
    cell.orderTimeLabel.text =
    [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:[dictionary[@"OrderTime"] intValue]]];
    
    return cell.baseLineView.frame.size.height;
}

@end
