//
//  ALProductStatisticsCell.h
//  alorth
//
//  Created by w91379137 on 2015/9/22.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ALProductStatisticsCell : UITableViewCell

@property (nonatomic, strong) IBOutlet LineView *baseLineView;

@property (nonatomic, strong) IBOutlet UILabel *conditionLabel;
@property (nonatomic, strong) IBOutlet UILabel *sizeLabel;
@property (nonatomic, strong) IBOutlet UILabel *priceLabel;
@property (nonatomic, strong) IBOutlet UILabel *sellerAccountLabel;
@property (nonatomic, strong) IBOutlet UILabel *orderTimeLabel;

+ (ALProductStatisticsCell *)cell;
+ (CGFloat)performCell:(ALProductStatisticsCell *)cell
        withDictionary:(NSDictionary *)dictionary;

@end
