//
//  ALProductStatisticsViewController.m
//  alorth
//
//  Created by w91379137 on 2015/9/22.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALProductStatisticsViewController.h"
#import "ALProductStatisticsCell.h"

@interface ALProductStatisticsViewController ()
{
    ALProductStatisticsCell *demoCell;
}

@end

@implementation ALProductStatisticsViewController

#pragma mark - init
-(instancetype)init
{
    self = [super init];
    if (self) {
        objectListArray = [NSMutableArray array];
        
        currentTableViewControl = [[ALTableViewControl alloc] init];
        currentTableViewControl.delegate = self;
        currentTableViewControl.apiDataArray = objectListArray;
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self alTableViewSetting:mainTableView];
    
    if (objectListArray.count == 0) {
        [self checkNewData:currentTableViewControl];
    }
}

-(void)viewWillAppearFirstTime:(BOOL)animated
{
    [super viewWillAppearFirstTime:animated];
    [headerView.backButton addTarget:self
                              action:@selector(back:)
                    forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [objectListArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (demoCell == nil) {
        demoCell = [ALProductStatisticsCell cell];
        demoCell.frame = CGRectMake(0, 0, tableView.contentSize.width, 0);
        [demoCell layoutIfNeeded];
    }
    NSDictionary *infoDict = objectListArray[indexPath.row];
    return [ALProductStatisticsCell performCell:demoCell
                                 withDictionary:infoDict];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ALProductStatisticsCell *cell = [ALProductStatisticsCell cell];
    NSDictionary *infoDict = objectListArray[indexPath.row];
    [ALProductStatisticsCell performCell:cell
                          withDictionary:infoDict];
    return cell;
}

#pragma mark - scrollView Delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == mainTableView) {
        [currentTableViewControl scrollViewDidScroll:scrollView];
    }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView == mainTableView) {
        [currentTableViewControl scrollViewDidEndDragging:scrollView willDecelerate:(BOOL)decelerate];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == mainTableView) {
        [currentTableViewControl scrollViewDidEndDecelerating:(UIScrollView *)scrollView];
    }
}

#pragma mark - ALTableViewControlDelegate
-(void)checkNewData:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    willReplaceWithNew = YES;
    tableViewControl.lastDict[@"StartNum"] = @"0";
    
    ALLog(@"重新整理");
    [self addMBProgressHUDWithKey:@"reportProductSalesListA037"];
    [self listLoadApiSourceParameters:tableViewControl.lastDict
                               FinishBlock:^(BOOL isSuccess) {
                                   [self removeMBProgressHUDWithKey:@"reportProductSalesListA037"];
                               }];
}

-(void)loadMore:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    
    ALLog(@"載入更多:目前第 %@ 頁 %lu 筆資料",tableViewControl.lastDict[@"StartNum"],(unsigned long)objectListArray.count);
    
    tableViewControl.lastDict[@"StartNum"] =
    [NSString stringWithFormat:@"%d",[tableViewControl.lastDict[@"StartNum"] intValue] + 1];
    
    [self addMBProgressHUDWithKey:@"reportProductSalesListA037"];
    [self listLoadApiSourceParameters:tableViewControl.lastDict
                               FinishBlock:^(BOOL isSuccess) {
                                   [self removeMBProgressHUDWithKey:@"reportProductSalesListA037"];
                               }];
}

#pragma mark - API
-(void)listLoadApiSourceParameters:(NSDictionary *)parameters
                       FinishBlock:(void (^)(BOOL isSuccess))finishBlockToRun
{
    [ALNetWorkAPI reportProductSalesListA037ProductID:self.infoDict[@"ProductID"]
                                             StartNum:parameters[@"StartNum"]
                                      CompletionBlock:^(NSDictionary *responseObject) {
                                          
                                          BOOL isWiseData = YES;
                                          if (![responseObject[@"resBody"] isKindOfClass:[NSDictionary class]]) {
                                              isWiseData = NO;
                                              
                                              if (![responseObject[@"resBody"][@"SalesList"] isKindOfClass:[NSArray class]]) {
                                                  isWiseData = NO;
                                              }
                                          }
                                          
                                          if (isWiseData) {
                                              if (willReplaceWithNew) {
                                                  [objectListArray removeAllObjects];
                                                  willReplaceWithNew = NO;
                                              }
                                              
                                              NSArray *addArray = [responseObject[@"resBody"][@"SalesList"] mutableCopy];
                                              if (addArray.count > 0) {
                                                  [objectListArray addObjectsFromArray:addArray];
                                              }
                                              [mainTableView reloadData];
                                          }
                                          else {
                                              ALLog(@"goodsA037 格式錯誤");
                                          }
                                          finishBlockToRun(YES);
                                      }];
}

@end
