//
//  ALProductEditHistoryCell.m
//  alorth
//
//  Created by w91379137 on 2015/9/23.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALProductEditHistoryCell.h"

@implementation ALProductEditHistoryCell

- (void)cellFromXibSetting
{
    self.productLabel.numberOfLines = 0;
}

+ (CGFloat)performCell:(ALProductEditHistoryCell *)cell
          withinfoDict:(NSDictionary *)dictionary
{
    cell.infoDict = dictionary;
    
    [cell.productImageView imageFromURLString:cell.infoDict[@"ProductImage"]];
    cell.productLabel.text = cell.infoDict[@"ProductTitle"];
    
    return cell.mainContainerView.frame.size.height;
}

@end
