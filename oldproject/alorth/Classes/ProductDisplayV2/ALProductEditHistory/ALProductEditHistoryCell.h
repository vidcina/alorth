//
//  ALProductEditHistoryCell.h
//  alorth
//
//  Created by w91379137 on 2015/9/23.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALCommonTableViewCell.h"

@interface ALProductEditHistoryCell : ALCommonTableViewCell

@property (nonatomic, strong) IBOutlet UIView *mainContainerView;
@property (nonatomic, strong) IBOutlet UIImageView *productImageView;
@property (nonatomic, strong) IBOutlet UILabel *productLabel;

+ (CGFloat)performCell:(ALProductEditHistoryCell *)cell
          withinfoDict:(NSDictionary *)dictionary;

@end
