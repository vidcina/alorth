//
//  ALProductEditViewController.h
//  alorth
//
//  Created by w91379137 on 2015/9/22.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicSubViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface ALProductEditViewController : ALBasicSubViewController
{
    NSArray *originalTagArray;
    NSMutableArray *tagArray;
}

@property (nonatomic, strong) NSDictionary *infoDict;
@property (nonatomic, strong) NSDictionary *infoA034Dict;

@property (nonatomic, strong) IBOutlet UIScrollView *mainScrollView;
@property (nonatomic, strong) IBOutlet UIView *mainScrollViewContainer;

#pragma mark - 資料
@property (nonatomic, strong) UIImage *selectProductImage;
@property (nonatomic, strong) IBOutlet UIImageView *kvo_productImage; //參照mediaURL
@property (nonatomic, strong) IBOutlet UITextField *productNameField;

@property (nonatomic, strong) IBOutlet UITextView *productDescriptionTextView;
@property (nonatomic, strong) IBOutlet UILabel *productDescriptionPlaceHolderLabel;

#pragma mark - 關鍵字
@property (nonatomic, strong) IBOutlet UIButton *tagAddButton;
@property (nonatomic, strong) IBOutlet SKTagView *tagView;

@end
