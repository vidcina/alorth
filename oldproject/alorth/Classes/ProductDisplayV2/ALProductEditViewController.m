//
//  ALProductEditViewController.m
//  alorth
//
//  Created by w91379137 on 2015/9/22.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALProductEditViewController.h"
#import "UIViewController+UIImagePickerControllerDelegate.h"
#import "UIViewController+ImageOnline.h"

static const float BigTagHeight = 20.0;
static const float SmallTagHeight = BigTagHeight * 3 / 4;

@interface ALProductEditViewController ()

@end

@implementation ALProductEditViewController

#pragma mark - init
-(void)viewDidLoad
{
    [super viewDidLoad];
    if (!tagArray) {
        tagArray = [NSMutableArray array];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.mainScrollViewContainer.superview == nil) {
        
        
        [self.mainScrollView addSubview:self.mainScrollViewContainer];
        
        [self.mainScrollViewContainer mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(self.mainScrollView.mas_width);
            
            make.top.equalTo(self.mainScrollView.mas_top);
            make.bottom.equalTo(self.mainScrollView.mas_bottom);
            make.leading.equalTo(self.mainScrollView.mas_leading);
            make.trailing.equalTo(self.mainScrollView.mas_trailing);
        }];
        
        self.tagView.padding    = UIEdgeInsetsMake(3, 10, 3, 10);
        self.tagView.insets     = 5;
        self.tagView.lineSpace  = 8;
        
        __block typeof(self) __weak weakself = self;
        self.tagView.didClickTagAtIndex = ^(NSUInteger index){
            [weakself checkToDeleteTag:index];
        };
    }
    
    [self addMBProgressHUDWithKey:@"getEditProductInformationA034"];
    [ALNetWorkAPI getEditProductInformationA034ProductID:self.infoDict[@"ProductID"]
                                         CompletionBlock:^(NSDictionary *responseObject) {
                                             [self removeMBProgressHUDWithKey:@"getEditProductInformationA034"];
                                             
                                             if ([responseObject isKindOfClass:[NSDictionary class]]) {
                                                 self.infoA034Dict = responseObject[@"resBody"];
                                             }
                                             
                                             if (self.infoA034Dict) {
                                                 self.selectProductImage = nil;
                                                 self.productNameField.text = self.infoA034Dict[@"ProductTitle"];
                                                 self.productDescriptionTextView.text = self.infoA034Dict[@"Description"];
                                                 self.productDescriptionPlaceHolderLabel.hidden =
                                                 self.productDescriptionTextView.text.length > 0;
                                                 
                                                 NSMutableArray *tagsArray = self.infoA034Dict[@"Tags"];
                                                 for (NSInteger k = 0; k < tagsArray.count; k++) {
                                                     NSString *tagString = tagsArray[k];
                                                     if ([tagString isKindOfClass:[NSString class]]) {
                                                         [self checkToAddTag:tagString];
                                                     }
                                                 }
                                                 originalTagArray = tagsArray;
                                             }
                                             else {
                                                 YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
                                             }
                                         }];
}

-(void)viewWillAppearFirstTime:(BOOL)animated
{
    [super viewWillAppearFirstTime:animated];
    headerView.backButton.hidden = YES;
    
    {
        NSMutableAttributedString *string =
        [[NSMutableAttributedString alloc] initWithString:
         [NSString stringWithFormat:@" %@",ALLocalizedString(@"ADD NEW TAG", nil)]];
        
        NSRange range = NSMakeRange(0,string.length);
        [string addAttribute:NSFontAttributeName
                       value:self.tagAddButton.titleLabel.font
                       range:range];
        
        [string addAttribute:NSForegroundColorAttributeName
                       value:[self.tagAddButton titleColorForState:UIControlStateNormal]
                       range:range];
        
        {
            //製圖
            UIImage *tagImage = [UIImage imageNamed:@"tag"];
            tagImage = [UIImage reSizeImage:tagImage
                                     toSize:CGSizeMake(BigTagHeight / tagImage.size.height * tagImage.size.width,
                                                       BigTagHeight)];
            
            //設定
            NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
            textAttachment.image = tagImage;
            
            //安插
            [string replaceCharactersInRange:NSMakeRange(0, 0) withAttributedString:[NSAttributedString attributedStringWithAttachment:textAttachment]];
            
            [string replaceCharactersInRange:NSMakeRange(0, 0) withAttributedString:[[NSMutableAttributedString alloc] initWithString:@"   "]];
        }
        
        [_tagAddButton setAttributedTitle:string forState:UIControlStateNormal];
    }
    
    {
        UITapGestureRecognizer *tap =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(createNewProductImage)];
        [self.kvo_productImage addGestureRecognizer:tap];
    }
}

#pragma mark - Photo Method
-(void)createNewProductImage
{
    [self showChoosePickerControllerType];
}

- (void)didPickerControllerReturnImage:(UIImage *)avatarImage
{
    self.selectProductImage = avatarImage;
}

#pragma mark - UITextView Delegate
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    BOOL shouldBeginEditing = [super textViewShouldBeginEditing:textView];
    if (shouldBeginEditing && textView == self.productDescriptionTextView) {
        self.productDescriptionPlaceHolderLabel.hidden = YES;
    }
    return shouldBeginEditing;
}

#pragma mark - Keyboard Auto System
-(void)keyboardDidShow:(NSNotification *)notification
{
    [super keyboardDidShow:notification];
    if (editFirstResponder == self.productDescriptionTextView) {
        float keyboardHeight = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
        self.mainScrollView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0);
    }
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    self.mainScrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    [super keyboardWillHide:notification];
}

#pragma mark - IBAction_關鍵字
-(IBAction)addTagAction
{
    __block typeof(self) __weak weakself = self;
    [YKBlockAlert alertWithTitle:ALLocalizedString(@"Enter your tag", nil)
                defaultInputText:@""
                         button1:ALLocalizedString(@"Cancel", nil)
                         button2:ALLocalizedString(@"OK", nil)
                          block1:^(NSString *theKey) {
                              
                          }
                          block2:^(NSString *theKey) {
                              [weakself checkToAddTag:theKey];
                          }];
}

-(void)checkToAddTag:(NSString *)theKey
{
    if ([tagArray containsObject:theKey] ||
        [originalTagArray containsObject:theKey]) {
        ALLog(@"tag重複");
    }
    else {
        SKTag *tag = [SKTag tagWithText:theKey];
        
        NSMutableAttributedString *string =
        [[NSMutableAttributedString alloc] initWithString:
         [NSString stringWithFormat:@" %@",theKey]];
        
        {
            //製圖
            UIImage *tagImage = [UIImage imageNamed:@"tag"];
            tagImage = [UIImage reSizeImage:tagImage
                                     toSize:CGSizeMake(SmallTagHeight / tagImage.size.height * tagImage.size.width,
                                                       SmallTagHeight)];
            
            //設定
            NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
            textAttachment.image = tagImage;
            
            //安插
            [string replaceCharactersInRange:NSMakeRange(0, 0) withAttributedString:[NSAttributedString attributedStringWithAttachment:textAttachment]];
            
            [string replaceCharactersInRange:NSMakeRange(0, 0) withAttributedString:[[NSMutableAttributedString alloc] initWithString:@" "]];
        }
        
        NSRange range = NSMakeRange(0,string.length);
        [string addAttribute:NSForegroundColorAttributeName
                       value:COMMON_GRAY4_COLOR
                       range:range];
        [string addAttribute:NSFontAttributeName
                       value:[UIFont systemFontOfSize:15]
                       range:range];
        
        tag.attributedText = string;
        tag.bgColor = COMMON_GRAY1_COLOR;
        tag.padding = UIEdgeInsetsMake(3.0, 0.0, 3.0, 5.0);
        [self.tagView addTag:tag];
        [tagArray addObject:theKey];
    }
}

-(void)checkToDeleteTag:(NSUInteger)index
{
    if (index < originalTagArray.count) {
        ALLog(@"原本的tag");
        return;
    }
    [YKBlockAlert alertWithTitle:ALLocalizedString(@"Removal confirmation", nil)
                         message:ALLocalizedString(@"Are you sure you want to remove the tag?", nil)
                blocksAndButtons:
     ^{
         [self.tagView removeTagAtIndex:index];
         [tagArray removeObjectAtIndex:index];
     },
     ALLocalizedString(@"Remove", nil),
     ^{},
     ALLocalizedString(@"Keep", nil), nil];
}

#pragma mark - IBAction_步驟
-(IBAction)nextStepAction
{
    if (self.selectProductImage != nil) {
        //選擇新圖片
        [self uploadImage:self.selectProductImage];
    }
    else {
        [self submitNewData:nil];
    }
}

- (void)didSaveImageAtURL:(NSString *)string
{
    if (string == nil) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
    }
    else {
        [self submitNewData:string];
    }
}

-(void)submitNewData:(NSString *)imageURLString
{
    NSMutableArray *allTags = [NSMutableArray arrayWithArray:originalTagArray];
    [allTags removeObjectsInArray:tagArray];
    [allTags addObjectsFromArray:tagArray];
    
    [ALNetWorkAPI editProductA035ProductID:self.infoDict[@"ProductID"]
                              ProductImage:imageURLString == nil ? @"" : imageURLString
                        ProductDescription:self.productDescriptionTextView.text
                                      Tags:allTags
                           CompletionBlock:^(NSDictionary *responseObject) {
                               
                               if ([ALNetWorkAPI checkSerialNumber:@"A035"
                                                    ResponseObject:responseObject]) {
                                   [YKBlockAlert alertWithTitle:ALLocalizedString(@"Success",nil)
                                                        message:ALLocalizedString(@"",nil)
                                               blocksAndButtons:^{
                                                   [self back:nil];
                                               },ALLocalizedString(@"OK",nil), nil];
                               }
                               else {
                                   YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
                               }
                           }];
}

#pragma mark - KVO
-(void)setSelectProductImage:(UIImage *)selectProductImage
{
    _selectProductImage = selectProductImage;
    
    if (![selectProductImage isKindOfClass:[UIImage class]]) {
         [self.kvo_productImage imageFromURLString:self.infoA034Dict[@"ProductImage"]];
        return;
    }
    self.kvo_productImage.image = selectProductImage;
}

@end
