//
//  ALProductStatisticsViewController.h
//  alorth
//
//  Created by w91379137 on 2015/9/22.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicSubViewController.h"

@interface ALProductStatisticsViewController : ALBasicSubViewController
<UITableViewDataSource, UITableViewDelegate, ALTableViewControlDelegate>
{
    NSMutableArray *objectListArray;            //列表
    BOOL willReplaceWithNew;                    //是否清空之前資料
    IBOutlet UITableView *mainTableView;        //顯示表
    ALTableViewControl *currentTableViewControl;//控制器
}

@property (nonatomic, strong) NSDictionary *infoDict;

@end
