//
//  ALPostReviewContentCell.m
//  alorth
//
//  Created by ZZB on 2016/4/27.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALPostReviewContentCell.h"

@implementation ALPostReviewContentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentTextView.layer.cornerRadius = 6.0;
    self.contentTextView.layer.borderColor = [COMMON_GRAY_Circle_COLOR CGColor];
    self.contentTextView.layer.borderWidth = 1.0;
}

@end
