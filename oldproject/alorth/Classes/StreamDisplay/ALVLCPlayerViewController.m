//
//  ALVLCPlayerViewController.m
//  alorth
//
//  Created by w91379137 on 2015/10/29.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALVLCPlayerViewController.h"
#import "ALStreamHelper.h"
#import "ALPersonIntroductionViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <PLPlayerKitFramework/PLPlayerKitFramework.h>
#import "ALPostReviewViewController.h"
#import "ALCheckoutViewController.h"
#import "ALVLCPlayerViewController+RotateVideo.h"

static NSString *status[] = {
    @"PLPlayerStatusUnknow",
    @"PLPlayerStatusPreparing",
    @"PLPlayerStatusReady",
    @"PLPlayerStatusCaching",
    @"PLPlayerStatusPlaying",
    @"PLPlayerStatusPaused",
    @"PLPlayerStatusStopped",
    @"PLPlayerStatusError"
};


@interface ALVLCPlayerViewController ()
<ALOnlinePaymentViewControllerDelegate,PLPlayerDelegate, ALCheckoutViewControllerDelegate>
{
    PLPlayer *plPlayer;
    MPMoviePlayerController *mpPlayer;
    
    ALTimeEventUnit *videoTimeEvent;
}

@property (nonatomic, strong) UINavigationController *checkoutNavigationController;

@end

@implementation ALVLCPlayerViewController

#pragma mark - Init
- (instancetype)init
{
    self = [super init];
    if (self) {
        _currentVideoClip = 0;
        _selectVideoClip = -1;
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardDidShow:)
                                                     name:UIKeyboardDidShowNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardDidShow:)
                                                     name:UIKeyboardDidChangeFrameNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillHide:)
                                                     name:UIKeyboardDidHideNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(stopPlayAction)
                                                     name:UIApplicationDidEnterBackgroundNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(mpMoviePlayerFinishNotification:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleAppActive)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged) name:UIDeviceOrientationDidChangeNotification object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateMovieDurationForSlider) name:MPMovieDurationAvailableNotification object:nil];
        self.bigBuyViewTransform = CATransform3DIdentity;
        danmuVC = [[ALDamnuViewController alloc] init];
        [self addChildViewController:danmuVC];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [ALInspector logEventTrack:@"V3Enterstream"
                    Parameters:nil];

    if (self.onlinePaymentVC == nil) {
        self.onlinePaymentVC = [[ALOnlinePaymentViewController alloc] init];
        self.onlinePaymentVC.delegate = self;
    }
    
    self.followActionButton.layer.borderWidth = 1;
    self.followActionButton.layer.borderColor = COMMON_GRAY3_COLOR.CGColor;
    self.followActionButton.layer.cornerRadius = 4;
    
    chatActionButton.layer.borderWidth = 1;
    chatActionButton.layer.borderColor = COMMON_GRAY3_COLOR.CGColor;
    chatActionButton.layer.cornerRadius = 4;
    [self.partialDanmuListView addSubview:danmuVC.view];
    [danmuVC.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.partialDanmuListView);
    }];
    
//    [self.mainScrollContainerView insertSubview:danmuVC.view belowSubview:self.danmuView];
    [self.fullDanmuListView addSubview:danmuVC.allCommentsTableView];
    [danmuVC.allCommentsTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.fullDanmuListView.mas_top).offset(20);
        make.left.equalTo(self.fullDanmuListView.mas_left);
        make.right.equalTo(self.fullDanmuListView.mas_right);
        make.bottom.equalTo(self.fullDanmuListView.mas_bottom).offset(-60);
    }];
    
    // (howard): Default is not hidden
    danmuVC.view.hidden = NO;    
    for (UIButton *btn in self.toggleDamnuButtons) {
        [btn setImage:[UIImage imageNamed:@"CommentClose"] forState:UIControlStateNormal];
    }
    
    // (howard): Check user see the swipe before or not
    BOOL isSwiped = [[NSUserDefaults standardUserDefaults] objectForKey:@"isSwipedRemindView"]? [[[NSUserDefaults standardUserDefaults] objectForKey:@"isSwipedRemindView"] boolValue]: NO;
    if (isSwiped == NO) {
        self.remindSwipeView.hidden = NO;
        UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
        gestureRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
        [self.remindSwipeView addGestureRecognizer:gestureRecognizer];
        [self.remindSwipeView addGestureRecognizer:tapRecognizer];
    }
    
    ALCheckoutViewController *checkoutViewController = [[ALCheckoutViewController alloc] initWithNibName:@"ALCheckoutViewController" bundle:nil];
    checkoutViewController.delegate = self;
    
    self.checkoutNavigationController = [[UINavigationController alloc] initWithRootViewController:checkoutViewController];
    self.checkoutNavigationController.view.frame = CGRectMake(8.0, CGRectGetHeight([UIScreen mainScreen].bounds), CGRectGetWidth(self.view.bounds) - 8.0*2,  CGRectGetHeight(self.view.bounds) - 64.0);
    
    [self.view addSubview:self.checkoutNavigationController.view];
}

-(void)handleAppActive
{
    [self.player performSelector:@selector(play) withObject:nil afterDelay:1];
}

- (void)viewWillAppearFirstTime:(BOOL)animated
{
    [super viewWillAppearFirstTime:animated];
    
    //調整左右感應區大小
    [self.mainScrollView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
    }];
    
    //安插播放畫面
    if (self.aVLCMediaView == nil) {
        self.aVLCMediaView = [[UIView alloc] initWithFrame:CGRectZero];
        [self.view insertSubview:self.aVLCMediaView atIndex:0];
        [self.aVLCMediaView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
    }
    
    //把分享畫面設定在螢幕下方
    [self.view addSubview:self.shareButtonView];
    
    [self.shareButtonView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_bottom);
        make.leading.equalTo(self.view.mas_leading);
        make.trailing.equalTo(self.view.mas_trailing);
    }];
    
    if (self.mainScrollContainerView.superview == nil) {
        
        //移除所有subviews
        for (UIView *view in self.mainScrollView.subviews) {
            [view removeFromSuperview];
        }
        
        [self.mainScrollView addSubview:self.mainScrollContainerView];
        [self.mainScrollContainerView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.mainScrollView);
            make.width.equalTo(self.mainScrollView.mas_width).multipliedBy(3.0);
            make.height.equalTo(self.mainScrollView.mas_height);
        }];
        self.mainScrollView.delegate = self;
    }
    
    [self.bigBuyView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mainScrollContainerView.mas_bottom);
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self replayAction:nil];
    
    //跟隨狀態調整
    self.followButton.layer.cornerRadius = self.followButton.frame.size.height / 2;
    self.followButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.followButton.layer.borderWidth  = 1;
    self.followButton.hidden = ![self isValidFollowPerson];
    self.followImageView.hidden = ![self isValidFollowPerson];
    
    //個人卡畫面
    self.avaterImageView.layer.cornerRadius = self.avaterImageView.frame.size.height / 2;
    [self.avaterImageView imageFromURLString:self.infoDict[@"AccountHeadImage"]];    
    self.userDisplayNameLabel.text = [ALStreamHelper userNameString:self.infoDict];
    
    self.folloChatView.hidden =
    ([[ALAppDelegate sharedAppDelegate].userID isEqualToString:self.infoDict[@"StreamerUserID"]]);
    
    danmuVC.streamID = self.infoDict.safe[@"StreamID"];
    //偵測追尋狀態
    if (![self.infoDict.safe[@"StreamerUserID"] isEqualToString:[ALAppDelegate sharedAppDelegate].userID]) {
        
        self.followActionButton.enabled = NO;
        [ALNetWorkAPI followStatusA068UserID:self.infoDict[@"StreamerUserID"]
                             CompletionBlock:^(NSDictionary *responseObject) {
                                 self.followActionButton.enabled = YES;
                                 
                                 if ([ALNetWorkAPI checkSerialNumber:@"A068"
                                                      ResponseObject:responseObject]) {
                                     self.followStatus = responseObject[@"resBody"][@"FollowStatus"];
                                 }
                                 else {
                                     //followActionButton.enabled = NO;
                                     self.followStatus = @"0";
                                     ALLog(@"無法取得跟隨狀態");
                                 }
                             }];
    }
    
    //購買畫面
    dispatch_group_t downloadGroup = dispatch_group_create();
    
    self.smallBuyView.userInteractionEnabled = NO;
    for (NSInteger index = 0; index < 3; index++) {
        NSString *productID = [self productIDOfContentInfo:index];
        
        if (productID &&  self.productA052InfoDict.safe[productID] == nil) {
            dispatch_group_enter(downloadGroup);
            [ALNetWorkAPI getStreamProductA052ProductID:productID
                                               StreamID:self.infoDict.safe[@"StreamID"]
                                        CompletionBlock:^(NSDictionary *responseObject) {
                                            
                                            if ([ALNetWorkAPI checkSerialNumber:@"A052"
                                                                 ResponseObject:responseObject]) {
                                                
                                                ALLog(@"ProductA052:%@ 初始加載完成",productID);
                                                self.productA052InfoDict.safe[productID] = responseObject[@"resBody"];
                                            }
                                            else {
                                                ALLog(@"ProductA052 錯誤");
                                            }
                                            
                                            dispatch_group_leave(downloadGroup);
                                        }];
        }
    }
    
    dispatch_group_notify(downloadGroup, dispatch_get_main_queue(), ^{
        self.smallBuyView.userInteractionEnabled = YES;
    });
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];

    UIBezierPath *maskPath;
    maskPath = [UIBezierPath bezierPathWithRoundedRect:self.checkoutNavigationController.view.bounds
                                     byRoundingCorners:(UIRectCornerTopLeft|UIRectCornerTopRight)
                                           cornerRadii:CGSizeMake(10.0, 10.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.checkoutNavigationController.view.bounds;
    maskLayer.path = maskPath.CGPath;
    self.checkoutNavigationController.view.layer.mask = maskLayer;
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.player stop];
    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
//    [self stopPlayAction];
    [super viewWillDisappear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
//    [self adjustDanmuViewArea];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)back:(UIButton *)sender
{
    // 停止播放器
    [self stopPlayAction];
//    [self.player stop];
    [super back:sender];
}

#pragma mark - Setter / Getter
- (void)setFollowStatus:(NSString *)followStatus
{
    _followStatus = followStatus;
    self.addFollowLabel.hidden = !([self.followStatus intValue] == 0);
    if ([self.followStatus intValue] == 0) {
        self.followActionButton.normalTitle = @"    $Follow$";
        [self.followButton setImage:nil forState:UIControlStateNormal];
    }
    else {
        self.followActionButton.normalTitle = @"Following";
        [self.followButton setImage:self.avaterImageView.image forState:UIControlStateNormal];
    }
}

- (void)setCurrentVideoClip:(NSInteger)currentVideoClip
{
    if (_currentVideoClip == currentVideoClip) {
        return;
    }
    [self willChangeValueForKey:@"currentVideoClip"];
    ALLog(@"更換段數 %ld > %ld",(long)_currentVideoClip,(long)currentVideoClip);
    _currentVideoClip = currentVideoClip;
    
    [self didChangeValueForKey:@"currentVideoClip"];
}

- (void)setSelectVideoClip:(NSInteger)selectVideoClip
{
    if (_selectVideoClip == selectVideoClip) {
        return;
    }
    _selectVideoClip = selectVideoClip;
    
    ALCheckoutViewController *checkoutViewController = (ALCheckoutViewController *)self.checkoutNavigationController.topViewController;
    
    checkoutViewController.productInfoDict = [self productInfoDictOfContentInfo:selectVideoClip];

    checkoutViewController.productNameLabel.text = self.productNameLabel.text = [self productInfoDictOfContentInfo:selectVideoClip][@"ProductName"];
    [checkoutViewController.productImageView imageFromURLString:[self productInfoDictOfContentInfo:selectVideoClip][@"ImageList"][0][@"ImagePath"]];
    
    self.stockTextField.text = @"1";
    
    checkoutViewController.amountNumberBottomLabel.text = @"1";
    checkoutViewController.amountNumberHeadLabel.text = @"x1";
    
    NSString *productID = [self productIDOfContentInfo:selectVideoClip];
    
    if (productID) {
        NSDictionary *productInfoDict = self.productA052InfoDict[productID];
        ALLog(@"productInfoDict %@",productInfoDict);
        
        
        if ([productInfoDict[@"ModelList"] isKindOfClass:[NSArray class]]) {
            
            NSMutableArray *sizeArray = [productInfoDict[@"ModelList"] mutableCopy];
            
            NSDictionary *productInfo = [self productInfoDictOfContentInfo:self.selectVideoClip];
            NSArray *indexArray =
            [ALFormatter indexOfSortValueArray1:sizeArray
                                           Key1:apiKeySize
                                    SourceArray:productInfo[@"StockList"]];
            
            checkoutViewController.typeSelectTypeScrollView.selectDelegate = self;
            checkoutViewController.typeSelectTypeScrollView.infoObjectArray= sizeArray;
            [checkoutViewController.typeSelectTypeScrollView selectIndex:[indexArray[0] integerValue]];
            checkoutViewController.typeSelectTypeScrollView.hidden = NO;
            
            self.typeSelectTypeScrollView.infoObjectArray = sizeArray;
            [self.typeSelectTypeScrollView selectIndex:[indexArray[0] integerValue]];
            self.typeSelectTypeScrollView.hidden = NO;
        }
        else {
            self.typeSelectTypeScrollView.hidden = YES;
            checkoutViewController.typeSelectTypeScrollView.hidden = YES;
        }
    }
}

- (NSMutableDictionary *)productA052InfoDict
{
    if (_productA052InfoDict == nil) {
        _productA052InfoDict = [NSMutableDictionary dictionary];
    }
    return _productA052InfoDict;
}

#pragma mark - Data Method
- (BOOL)isValidFollowPerson
{
    if (![self.infoDict[@"StreamerUserID"] isKindOfClass:[NSString class]]) {
        return NO;
    }
    
    if ([[ALAppDelegate sharedAppDelegate].userID isEqualToString:self.infoDict[@"StreamerUserID"]]) {
        return NO;
    }
    return YES;
}

- (BOOL)isLiveStream
{
    NSString *videoType = self.infoDict.safe[@"VideoType"];
    if ([videoType isEqualToString:@"vod"]) {
        return NO;
    }
    return YES;
}

- (NSString *)streamURLString
{
    return [ALStreamHelper streamVideoURL:self.infoDict];
}

- (NSString *)productIDOfContentInfo:(NSInteger)index
{
    NSArray *contentInfo = self.infoDict[@"ContentInfo"];
    if (index >= 0 && index < contentInfo.count) {
        return self.infoDict.safe[@"ContentInfo"][index][@"ProductID"];
    }
    return nil;
}

- (NSInteger)indexOfContentInfo:(NSString *)productID
{
    NSArray *contentInfo = self.infoDict[@"ContentInfo"];
    NSInteger findIndex = 0;
    
    for (NSInteger index = 0; index < contentInfo.count; index++) {
        NSString *dataProductID = self.infoDict.safe[@"ContentInfo"][index][@"ProductID"];
        if ([dataProductID isEqualToString:productID]) {
            findIndex = index;
            break;
        }
    }
    return findIndex;
}

- (NSDictionary *)productInfoDictOfContentInfo:(NSInteger)index
{
    NSString *productID = [self productIDOfContentInfo:index];
    
    if (productID) {
        return self.productA052InfoDict[productID];
    }
    return nil;
}

#pragma mark - timerEvent
- (void)timerEvent:(NSTimer *)sender
{
// 更新MPMoviePlayer進度條
    MPMoviePlayerController *mpc = maybe(self.player, MPMoviePlayerController);
    if (mpc && mpc.playbackState == MPMoviePlaybackStatePlaying && !self.movieSlider.isTracking) {
        self.movieSlider.value = mpc.currentPlaybackTime;
    }
    
// 統計串流時間部分
    self.accumulationTime += sender.timeInterval;
    //NSLog(@"accumulationTime %f",accumulationTime);
    
    reloadPeriod += sender.timeInterval;
    if (reloadPeriod >= ALVLCPlayerReloadPeriodGap) {
        reloadPeriod = 0;
        
        if ([self isLiveStream]) {
            //live 藉由 A052 得知段數
            [ALNetWorkAPI getStreamProductA052ProductID:nil
                                               StreamID:self.infoDict.safe[@"StreamID"]
                                        CompletionBlock:^(NSDictionary *responseObject) {
                                            
                                            if ([ALNetWorkAPI checkSerialNumber:@"A052"
                                                                 ResponseObject:responseObject]) {
                                                
                                                NSString *productID = responseObject.safe[@"resBody"][@"ProductID"];
                                                
                                                if (productID) {
                                                    self.productA052InfoDict.safe[productID] = responseObject[@"resBody"];
                                                    
                                                    //直播需要經由此 ProductID 判定第幾段
                                                    NSInteger index = [self indexOfContentInfo:productID];
                                                    if (index >= 0 && index > self.currentVideoClip) {
                                                        //需要比現在的多 才增加 防止 播放結束 最後一個商品無法購買 跳到第一個
                                                        self.currentVideoClip = index;
                                                    }
                                                    self.productTitleLabel.text = responseObject.safe[@"resBody"][self.currentVideoClip][@"ProductName"];
                                                    self.productIntroductionLabel.text = responseObject.safe[@"resBody"][self.currentVideoClip][@"ProductInfo"];
                                                    [self.productImageView imageFromURLString:responseObject.safe[@"resBody"][self.currentVideoClip][0][@"ImagePath"]];
                                                }
                                            }
                                            else {
                                                ALLog(@"ProductA052 錯誤");
                                            }
                                        }];
            
        }
        else {
            //vod 藉由 時間 得知段數
            NSArray *contentInfo = self.infoDict[@"ContentInfo"];
            
            float clipTime = 0;
            NSInteger index = 0;
            while (index < contentInfo.count) {
                clipTime += [contentInfo.safe[index][@"ChangeTime"] floatValue];
                if (self.accumulationTime <= clipTime) {
                    //ALLog(@"%f %@",clipTime,contentInfo);
                    break;
                }
                else {
                    index ++;
                }
            }
            self.currentVideoClip = MIN(index, contentInfo.count - 1);
            // 更新商品圖片與文字介紹
            self.productTitleLabel.text = contentInfo.safe[self.currentVideoClip][@"ProductName"];
            self.productIntroductionLabel.text = contentInfo.safe[self.currentVideoClip][@"ProductInfo"];
            [self.productImageView imageFromURLString:contentInfo.safe[self.currentVideoClip][@"ImageList"][0][@"ImagePath"]];
        }
        
        [ALNetWorkAPI streamBuyCountA118StreamID:self.infoDict.safe[@"StreamID"]
                                 CompletionBlock:^(NSDictionary *responseObject) {
                                     
                                     if ([ALNetWorkAPI checkSerialNumber:@"A118"
                                                          ResponseObject:responseObject]) {
                                         self.buyCountLabel.text =
                                         [NSString stringWithFormat:@"%@",responseObject.safe[@"resBody"][@"SellAmount"]];
                                     }
                                 }];
    }
}

#pragma mark - Replay / Pause/ Stop
-(void)pause
{
    PLPlayer *player = maybe(self.player, PLPlayer);
    if (player.isPlaying) {
        [player pause];
    }
    
    MPMoviePlayerController *mpc = maybe(self.player, MPMoviePlayerController);
    if (mpc.playbackState == MPMoviePlaybackStatePlaying) {
        [mpc pause];
    }
    _videoPauseButton.selected = YES;
}

-(void)resume
{
    if (!self.player) {
        return;
    }
    PLPlayer *player = maybe(self.player, PLPlayer);
    if (!player.isPlaying) {
        [player play];
    }
    
    MPMoviePlayerController *mpc = maybe(self.player, MPMoviePlayerController);
    if (mpc.playbackState != MPMoviePlaybackStatePlaying) {
        [mpc play];
    }
    _videoPauseButton.selected = NO;
}

- (IBAction)pauseResumeAction:(UIButton *)sender
{

    if (self.player) {
        if (_videoPauseButton.selected) {
            [self resume];
        }
        else {
            [self pause];
        }
    }
    else {
        [self startPlayAction];
        _videoPauseButton.selected = NO;
    }
}

- (IBAction)replayAction:(id)sender
{
    //判定是否 live 型態
    //live 型態需要 重新跟A066 檢查與伺服器對齊 才進行連線
    
    dispatch_group_t downloadGroup = dispatch_group_create();
    
    if ([self isLiveStream]) {
        dispatch_group_enter(downloadGroup);
        [ALNetWorkAPI streamInfomationA066StreamID:self.infoDict[@"StreamID"]
                                   CompletionBlock:^(NSDictionary *responseObject) {
                                       
                                       if ([ALNetWorkAPI checkSerialNumber:@"A066"
                                                            ResponseObject:responseObject]) {
                                           
                                           self.infoDict = responseObject[@"resBody"];
                                       }
                                       
                                       dispatch_group_leave(downloadGroup);
                                   }];
    }
    
    dispatch_group_notify(downloadGroup, dispatch_get_main_queue(), ^{
        
        [self stopPlayAction];
        if (self.streamURLString) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self startPlayAction];
                [danmuVC replayComments];
            });
        }
        else {
            ALLog(@"無 streamURLString");
        }
    });
}

- (void)startPlayAction
{
    self.replayButton.hidden = YES;
    if (!self.player) {
        
        NSString *playURLString = [self.streamURLString copy];
                
        ALLog(@"%@",playURLString);
        for (UIView *view in self.aVLCMediaView.subviews) {
            [view removeFromSuperview];
        }
        

        if ([playURLString hasPrefix:@"http"] && [playURLString hasSuffix:@"mp4"]) {
            NSURL *url = [NSURL URLWithString:playURLString];
            if (!url) {
                url = [NSURL URLWithString:[playURLString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            }
            mpPlayer = [[MPMoviePlayerController alloc] initWithContentURL:url];
            self.player = (id)mpPlayer;
            [self.aVLCMediaView addSubview:mpPlayer.view];
            mpPlayer.controlStyle = MPMovieControlStyleNone;
            mpPlayer.view.frame = self.view.bounds;
            [mpPlayer play];
            self.movieSlider.hidden = NO;
        }
        else {
            plPlayer = [[PLPlayer alloc] initWithURL:[NSURL URLWithString:playURLString]];
            self.player = (id)plPlayer;

            plPlayer.delegate = self;
            
            plPlayer.playerView.backgroundColor = [UIColor clearColor];
            plPlayer.timeoutIntervalForMediaPackets = 15;
            [self.aVLCMediaView addSubview:plPlayer.playerView];
            plPlayer.playerView.frame = self.view.bounds;
            [plPlayer prepareToPlay];
            self.loadingIndicator.hidden = NO;
            self.movieSlider.hidden = YES;
        }
        
        videoTimeEvent = [ALTimeEventUnit startVideoTimeEvent];
    }
    
    if (!reloadTimer) {
        reloadTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                       target:self
                                                     selector:@selector(timerEvent:)
                                                     userInfo:nil
                                                      repeats:true];
        self.accumulationTime = 0;
        reloadPeriod = ALVLCPlayerReloadPeriodGap;
    }
    _videoPauseButton.selected = NO;

}

-(void)mpMoviePlayerFinishNotification:(NSNotification *)notif
{
    NSNumber *reasonNumber = [notif userInfo][MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    if (reasonNumber) {
        MPMovieFinishReason reason = [reasonNumber intValue];
        if (reason == MPMovieFinishReasonPlaybackEnded) {
            [self stopPlayAction];
        }
    }
}

- (void)stopPlayAction
{
    if (reloadTimer) {
        [reloadTimer invalidate];
        reloadTimer = nil;
    }
    
    if (self.player) {
        [[videoTimeEvent endTimeEventData:self.infoDict] sendInspector];
        videoTimeEvent = nil;
        if ([mpPlayer playbackState] == MPMoviePlaybackStatePlaying) {
            [mpPlayer stop];
        }
        [plPlayer stop];
        plPlayer.delegate = nil;
        
        self.player = nil;
    }
    self.replayButton.hidden = NO;
    _videoPauseButton.selected = YES;
}

-(IBAction)sliderTouchDownAction:(id)sender
{
    [self pause];
}

-(IBAction)sliderTouchUpAction:(id)sender
{
    [self resume];
}

-(IBAction)sliderDragAction:(id)sender
{
    MPMoviePlayerController *mpc = maybe(self.player, MPMoviePlayerController);
    mpc.currentPlaybackTime = self.movieSlider.value;
}

-(void)updateMovieDurationForSlider
{
    MPMoviePlayerController *mpc = maybe(self.player, MPMoviePlayerController);
    self.movieSlider.maximumValue = mpc.duration;
}

#pragma mark - IBAction Buy
- (void)installActionForBigBuyView:(UIView *)view
{
    for (UIView *checkView in @[self.onlinePaymentVC.view,self.addToCartView]) {
        if (checkView == view) {
            if (!checkView.superview) {
                ALCheckoutViewController *checkoutViewController = (ALCheckoutViewController *)self.checkoutNavigationController.topViewController;
                [checkoutViewController.actionContainerView addSubview:checkView];
                
//                [self.actionContainerView addSubview:checkView];
//                [checkView mas_updateConstraints:^(MASConstraintMaker *make) {
//                    make.edges.equalTo(self.actionContainerView);
//                }];
                [checkView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.edges.equalTo(checkoutViewController.actionContainerView);
                }];
            }
        }
        else {
            [checkView removeFromSuperview];
        }
    }
}

- (IBAction)bigBuyViewForBuy:(id)sender
{
    [self scrollToFirstPage];
    [self installActionForBigBuyView:self.onlinePaymentVC.view];
    [self showBigBuyView:sender];
}

- (IBAction)bigBuyViewForCart:(id)sender
{
    [self scrollToFirstPage];
    [self installActionForBigBuyView:self.addToCartView];
    [self showBigBuyView:sender];
}

- (void)showBigBuyView:(id)sender
{
    self.selectVideoClip = self.currentVideoClip;
    [self bigBuyViewIsShow:YES];
}

- (void)hiddenBigBuyView:(id)sender
{
    [self bigBuyViewIsShow:NO];
}

- (void)bigBuyViewIsShow:(BOOL)isShow
{
    if (self.bigBuyViewRuturnButton) {
        [self.bigBuyViewRuturnButton removeFromSuperview];
    }
    self.bigBuyViewRuturnButton = nil;
    
    if (isShow) {
        self.bigBuyViewRuturnButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [self.bigBuyViewRuturnButton addTarget:self action:@selector(hiddenBigBuyView:) forControlEvents:UIControlEventTouchUpInside];
        [self.view insertSubview:self.bigBuyViewRuturnButton belowSubview:self.checkoutNavigationController.view];
//        [self.mainScrollContainerView insertSubview:self.bigBuyViewRuturnButton belowSubview:self.bigBuyView];
        [self.bigBuyViewRuturnButton mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.mainScrollContainerView);
        }];
        
        self.bigBuyViewRuturnButton.backgroundColor = [UIColor blackColor];
        self.bigBuyViewRuturnButton.alpha = 0.7;
        
        self.bigBuyViewTransform = CATransform3DTranslate(CATransform3DIdentity, 0, -self.bigBuyView.frame.size.height, 0);
    }
    else {
        [self.stockTextField resignFirstResponder];
        
        [(ALCheckoutViewController *)self.checkoutNavigationController.topViewController setCurrentStatus:ALCheckoutStatusOrderInfo];
        
        self.bigBuyViewTransform = CATransform3DIdentity;
    }
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         CGRect frame = self.checkoutNavigationController.view.frame;
                         frame.origin.y = isShow? 64.0: CGRectGetMaxY(self.view.frame);
                         self.checkoutNavigationController.view.frame = frame;
                     } completion:^(BOOL finished) {
                         [self installActionForBigBuyView:self.onlinePaymentVC.view];
                     }];
    
    self.smallBuyView.hidden = isShow;
    self.mainScrollView.scrollEnabled = !isShow;
    
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.bigBuyView.layer.transform = self.bigBuyViewTransform;
                     } completion:nil];
}

- (IBAction)presentPostReviewViewController:(id)sender
{
    ALPostReviewViewController *reviewViewController = [[ALPostReviewViewController alloc] initWithNibName:@"ALPostReviewViewController" bundle:nil];
    reviewViewController.infoDict = self.infoDict[@"ContentInfo"][self.currentVideoClip];
    reviewViewController.streamID = self.infoDict.safe[@"StreamID"];
    NSTimeInterval time = 0;
    
    id player = self.player;
    PLPlayer *localPLPlayer = maybe(player, PLPlayer);
    if (localPLPlayer) {
        time = ceilf([self accumulationTime]);
    }
    else {
        MPMoviePlayerController *mpc = self.player;
        time = ceil(mpc.currentPlaybackTime);
    }
    reviewViewController.videoPlayTime = time;
    
    [[PDSEnvironmentViewController sharedInstance] submitViewController:reviewViewController];
}

- (void)swipe:(UISwipeGestureRecognizer *)gestureRecognizer
{
    [[NSUserDefaults standardUserDefaults] setObject:@(YES) forKey:@"isSwipedRemindView"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    self.remindSwipeView.hidden = YES;
}

#pragma mark - Cart & Buy Check
- (BOOL)isValidToBuy:(NSString **)errorMsgPointer
{
    return [self isValidToBuy:errorMsgPointer
                  ProductInfo:self.fitProductDictionary];
}

- (BOOL)isValidToBuy:(NSString **)errorMsgPointer
         ProductInfo:(NSDictionary *)productInfo
{
    BOOL isValidToBuy = YES;
    NSString *errorMsg = @"";
    
    //沒有登入 不需要擋 線上支付會跳出 登入視窗
    //    if (isValidToBuy &&
    //        [PDSAccountManager isLoggedIn] == NO) {
    //        isValidToBuy = NO;
    //    }
    
    //無效的資料
    if (isValidToBuy &&
        productInfo.safe[@"SellerID"] == nil) {
        errorMsg = @"There something wrong with the merchandise";//沒有商品賣家ID
        isValidToBuy = NO;
    }
    
    //這是 自己的 商品
    if (isValidToBuy &&
        [productInfo.safe[@"SellerID"] isEqualToString:[ALAppDelegate sharedAppDelegate].userID]) {
        errorMsg = @"This is your own merchandise";
        isValidToBuy = NO;
    }
    
    //無效的資料
    if (isValidToBuy &&
        self.infoDict.safe[@"StreamerUserID"] == nil) {
        errorMsg = @"There something wrong with the streaming";//沒有廣播ID
        isValidToBuy = NO;
    }
    
    //這是 自己廣播的 商品
    if (isValidToBuy &&
        [self.infoDict.safe[@"StreamerUserID"] isEqualToString:[ALAppDelegate sharedAppDelegate].userID]) {
        errorMsg = @"This is your own merchandise";
        isValidToBuy = NO;
    }
    
    if (errorMsgPointer) *errorMsgPointer = errorMsg;
    return isValidToBuy;
}

- (BOOL)isCompleteInformationBeforeAciotn:(NSString **)errorMsgPointer
{
    if ([PDSAccountManager isLoggedIn] == NO) {
        [[PDSEnvironmentViewController sharedInstance] presentLoginViewController];
        return NO;
    }
    
    if (self.currentVideoClip < 0) {
        return NO; //商品還沒準備
    }
    //NSLog(@"buyAction %@",[NSDate date]);
    BOOL isAmountError = NO;
    
    int buyAmount = [self.stockTextField.text intValue];
    if (buyAmount <= 0) {
        isAmountError = YES;
    }
    if (buyAmount > [self.fitProductDictionary.safe[@"Amount"] intValue]) {
        isAmountError = YES;
    }
    
    //數目檢查
    if (isAmountError) {
        if (errorMsgPointer) *errorMsgPointer = @"Please enter a valid quantity";
        return NO;
    }
    
    //購買權檢查
    return [self isValidToBuy:errorMsgPointer];
}

#pragma mark - Cart
- (IBAction)cartAction:(id)sender
{
    NSString *errMag = nil;
    BOOL isFineToAction = [self isCompleteInformationBeforeAciotn:&errMag];
    if (!isFineToAction) {
        if (errMag.length > 0) YKSimpleAlert(ALLocalizedString(errMag,nil));
        return;
    }
    
    [self addMBProgressHUDWithKey:@"cartAddV2ProductA080"];
    [ALNetWorkAPI cartAddV2ProductA080UserID:self.fitProductDictionary.safe[@"SellerID"]
                                    StreamID:self.infoDict.safe[@"StreamID"]
                                   ProductID:[self productIDOfContentInfo:self.selectVideoClip]
                                     StockID:self.fitProductDictionary.safe[@"StockID"]
                                    SellerID:self.infoDict.safe[@"StreamerUserID"] //廣播人
                                 OrderAmount:self.stockTextField.text
                             CompletionBlock:^(NSDictionary *responseObject) {
                                 [self removeMBProgressHUDWithKey:@"cartAddV2ProductA080"];
                                 
                                 if ([ALNetWorkAPI checkSerialNumber:@"A080"
                                                      ResponseObject:responseObject]) {
                                     ALEcommerceProduct *product = [[ALEcommerceProduct alloc] init];
                                     
                                     product.productID = self.infoDict.safe[@"ContentInfo"][0][@"ProductID"];
                                     product.productTitle = self.infoDict.safe[@"ContentInfo"][0][@"ProductName"];
                                     
                                     product.price = [self.fitProductDictionary.safe[@"Price"] doubleValue];
                                     product.currency = self.fitProductDictionary.safe[@"Currency"];
                                     
                                     [[ALBuyProcessInspectorUnit logProductBuy:ALV3StreamAddCart
                                                                       Product:product
                                                                       DevDict:@{}] sendInspector];

                                     YKSimpleAlert(ALLocalizedString(@"Success",nil));
                                 }
                                 else {
                                     YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A080", responseObject),nil));
                                 }
                             }];
}

#pragma mark - Buy
#pragma mark ALOnlinePaymentViewControllerDelegate
- (void)totalPrice:(NSDecimalNumber **)decimalNumber currency:(NSString **)currency
{
    double totalPrice = [self totalPrice:self.fitProductDictionary Amount:[self.stockTextField.text intValue]];
    *decimalNumber = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.2f",totalPrice]];
    *currency = self.fitProductDictionary[@"Currency"];
}

- (void)onlinePayAction
{
    NSString *errMag = nil;
    BOOL isFineToAction = [self isCompleteInformationBeforeAciotn:&errMag];
    if (!isFineToAction) {
        if (errMag.length > 0) YKSimpleAlert(ALLocalizedString(errMag,nil));
        return;
    }
    
    int buyAmount = [self.stockTextField.text intValue];
    
    NSString *totalPrice =
    [NSString stringWithFormat:@"%f",
     [self totalPrice:self.fitProductDictionary Amount:buyAmount]];
    
    //NSLog(@"%@",[NSDate date]);
    [self addMBProgressHUDWithKey:@"streamCheckoutA074"];
    [ALNetWorkAPI streamCheckoutA074ApplicationID:self.fitProductDictionary.safe[@"SellerID"] //持有人
                                         StreamID:self.infoDict.safe[@"StreamID"]
                                        ProductID:[self productIDOfContentInfo:self.selectVideoClip]
                                          StockID:self.fitProductDictionary.safe[@"StockID"]
                                         SellerID:self.infoDict.safe[@"StreamerUserID"] //廣播人
                                      OrderAmount:[NSString stringWithFormat:@"%d",buyAmount]
                                       TotalPrice:totalPrice
                                  CompletionBlock:^(NSDictionary *responseObject) {
                                      [self removeMBProgressHUDWithKey:@"streamCheckoutA074"];
                                      //NSLog(@"%@",[NSDate date]);
                                      
                                      if ([ALNetWorkAPI checkSerialNumber:@"A074"
                                                           ResponseObject:responseObject]) {
                                          ALEcommerceProduct *product = [[ALEcommerceProduct alloc] init];
                                          
                                          product.productID = self.infoDict.safe[@"ContentInfo"][0][@"ProductID"];
                                          product.productTitle = self.infoDict.safe[@"ContentInfo"][0][@"ProductName"];
                                          
                                          product.price = [self.fitProductDictionary.safe[@"Price"] doubleValue];
                                          product.currency = self.fitProductDictionary.safe[@"Currency"];
                                          
                                          [[ALBuyProcessInspectorUnit logProductBuy:ALV3StreamBuy
                                                                            Product:product
                                                                            DevDict:@{}] sendInspector];YKSimpleAlert(ALLocalizedString(@"Success",nil));
                                          [self hiddenBigBuyView:nil]; //購買成功 收下購買畫面
                                      }
                                      else {
                                          YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A074", responseObject),nil));
                                          ALLog(@"%@",responseObject);
                                      }
                                      
                                      NSString *productID = [self productIDOfContentInfo:self.selectVideoClip];
                                      [ALNetWorkAPI getStreamProductA052ProductID:productID
                                                                         StreamID:self.infoDict.safe[@"StreamID"]
                                                                  CompletionBlock:^(NSDictionary *responseObject) {
                                                                      
                                                                      if ([ALNetWorkAPI checkSerialNumber:@"A052"
                                                                                           ResponseObject:responseObject]) {
                                                                          
                                                                          self.productA052InfoDict.safe[productID] = responseObject[@"resBody"];
                                                                          [self writeChange]; //更新數量
                                                                      }
                                                                      else {
                                                                          ALLog(@"ProductA052 錯誤");
                                                                      }
                                                                  }];
                                  }];
}

#pragma mark - Big Buy View Action Amount
-(IBAction)addStockAction:(UIButton *)sender
{
    ALCheckoutViewController *checkoutViewController = (ALCheckoutViewController *)self.checkoutNavigationController.topViewController;
    
    [self.stockTextField resignFirstResponder];
    int stock = [self.stockTextField.text intValue];
    checkoutViewController.paymentAmountValueLabel.text = checkoutViewController.amountNumberBottomLabel.text = self.stockTextField.text = [NSString stringWithFormat:@"%d",stock + 1];
    checkoutViewController.amountNumberHeadLabel.text = [NSString stringWithFormat:@"x%d", stock + 1 ];
    
    [self writeChange];
}

-(IBAction)minusStockAction:(UIButton *)sender
{
    ALCheckoutViewController *checkoutViewController = (ALCheckoutViewController *)self.checkoutNavigationController.topViewController;

    [self.stockTextField resignFirstResponder];
    int stock = [self.stockTextField.text intValue];
    if (stock > 0) {
        checkoutViewController.paymentAmountValueLabel.text = checkoutViewController.amountNumberBottomLabel.text = self.stockTextField.text = [NSString stringWithFormat:@"%d",stock - 1];
        checkoutViewController.amountNumberHeadLabel.text = [NSString stringWithFormat:@"x%d", stock - 1 ];
    }
    [self writeChange];
}

- (IBAction)textFieldChangeAction:(UITextField *)sender
{
    [self writeChange];
}

#pragma mark - Display price
- (BOOL)writeChange
{
    ALCheckoutViewController *checkoutViewController = (ALCheckoutViewController *)self.checkoutNavigationController.topViewController;
    
    int stock = [_stockTextField.text intValue];
    if (stock < 1) {
        stock = 1;
        checkoutViewController.amountNumberBottomLabel.text = self.stockTextField.text = @"1";
        checkoutViewController.amountNumberHeadLabel.text = @"x1";
    }
    if (stock > 99) {
        stock = 99;
        checkoutViewController.amountNumberBottomLabel.text = self.stockTextField.text = @"99";
        checkoutViewController.amountNumberHeadLabel.text = @"x99";
    }

    NSPredicate *sizePredicate = [NSPredicate predicateWithFormat:@"%K == %@", apiKeySize, self.sizeString];
    NSDictionary *productInfo = [self productInfoDictOfContentInfo:self.selectVideoClip];
    
    NSArray *productArray = [productInfo[@"StockList"] filteredArrayUsingPredicate:sizePredicate];
    
    if (productArray.count > 0) {
        checkoutViewController.fitProductInfoDict = self.fitProductDictionary = productArray.firstObject;
        
        self.stockNumber.text = [NSString stringWithFormat:@"(%@)",self.fitProductDictionary[@"Amount"]];
        checkoutViewController.amountDetailBottomLabel.text = [NSString stringWithFormat:@"(%@ left in stock):",self.fitProductDictionary[@"Amount"]];
        
        self.shippingFeeFeeLabel.text = [NSString stringWithFormat:@"%.2f %@",
                                         [self transFee:self.fitProductDictionary],
                                         self.fitProductDictionary[@"Currency"]];
        
        self.totalFeeLabel.text = [NSString stringWithFormat:@"%.2f %@",
                                   [self totalPrice:self.fitProductDictionary Amount:stock],
                                   self.fitProductDictionary[@"Currency"]];
        checkoutViewController.paymentTotalWithoutOthersValueLabel.text = checkoutViewController.totalPriceNumberHeadLabel.text =  checkoutViewController.totalPriceNumberBottomLabel.text = [NSString stringWithFormat:@"%.2f %@",
                                                                                                                            [self totalPrice:self.fitProductDictionary Amount:stock],
                                                                                                                            self.fitProductDictionary[@"Currency"]];
    }
    else {
        self.totalFeeLabel.text = @"";
    }
    
    checkoutViewController.singlePriceLabel.text = [NSString stringWithFormat:@"%.2f %@",
                                                    [self totalPrice:self.fitProductDictionary Amount:1],
                                                    self.fitProductDictionary[@"Currency"]];
    
    checkoutViewController.paymentSizeValueLabel.text = self.sizeString;
    
    return YES;
}

- (double)totalPrice:(NSDictionary *)productDictionary Amount:(int)amount
{
    double price = [productDictionary.safe[@"Price"] doubleValue];
    double transFee = [self transFee:productDictionary];
    return price * amount + transFee;
}

- (double)transFee:(NSDictionary *)productDictionary
{
    return [productDictionary.safe[@"TransPrice"] doubleValue];
}

#pragma mark - ALSelectTypeScrollViewDelegate
-(UIView *)typeViewOfIndex:(NSInteger)index
                infoObject:(NSObject *)infoObject
      selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    if ([infoObject isKindOfClass:[NSString class]]) {
        
        UIButton *aButton =
        [ALSelectTypeScrollView alSelectButton:(NSString *)infoObject];
        
        [aButton addTarget:selectTypeScrollView
                    action:@selector(selectView:)
          forControlEvents:UIControlEventTouchUpInside];
        
        CGSize titleSize =
        [ALSelectTypeScrollView alSelectButtonSize:(NSString *)infoObject];
        
        [aButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@(titleSize.width + 40));
        }];
        return aButton;
    }
    else {
        return [[UIView alloc] initWithFrame:CGRectZero];
    }
}

-(void)didSelectIndex:(NSInteger)index
           infoObject:(NSObject *)infoObject
 selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    self.sizeString = (NSString *)infoObject;
    [self writeChange];
    
    //購買權檢查
    NSString *errMsg = nil;
    BOOL isValidToBuy = [self isValidToBuy:&errMsg];
    
    self.actionContainerView.hidden = !isValidToBuy;
    if (!isValidToBuy && errMsg.length > 0) {
        YKSimpleAlert(ALLocalizedString(errMsg,nil));
    }
}

-(void)adjustSelectView:(UIView *)view
             infoObject:(NSObject *)infoObject
               selected:(BOOL)selected
   selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    UIButton *button = (UIButton *)view;
    [ALSelectTypeScrollView alSelectButton:button Select:selected];
    
    BOOL isAnyFit = NO;
    
    NSDictionary *productInfo = [self productInfoDictOfContentInfo:self.selectVideoClip];
    NSArray *stockList = productInfo[@"StockList"];
    for (int t = 0 ; t < stockList.count; t++) {
        NSDictionary *aProduct = stockList[t];
        
        if ([aProduct[apiKeySize] isEqualToString:(NSString *)infoObject] &&
            [aProduct[@"Amount"] intValue] > 0) {
            isAnyFit = YES;
            break;
        }
    }
    if (isAnyFit == NO) {
        button.enabled = NO;
        button.alpha = 0.5;
    }
    else {
        button.enabled = YES;
        button.alpha = 1;
    }
}

#pragma mark - PLPlayerDelegate
- (void)player:(nonnull PLPlayer *)playerx statusDidChange:(PLPlayerStatus)state
{
//    ALLog(@"State: %@", status[state]);
    if (PLPlayerStatusReady == state) {
        self.loadingIndicator.hidden = YES;
        dispatch_queue_t queue =  dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_barrier_async(queue, ^{
            [playerx play];
        });
    }
}

- (void)player:(nonnull PLPlayer *)player stoppedWithError:(nullable NSError *)error
{
    [self stopPlayAction];
}

#pragma mark - keyboard
//-(void)keyboardDidShow:(NSNotification *)notification
//{
//    float keyboardHeight = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
//   
//    [UIView animateWithDuration:0.1
//                          delay:0.0
//                        options:UIViewAnimationOptionBeginFromCurrentState
//                     animations:^{
//                         self.bigBuyView.layer.transform =
//                         CATransform3DTranslate(self.bigBuyViewTransform, 0, - keyboardHeight + 60, 0);
//                         danmuVC.editArea.layer.transform =
//                         CATransform3DTranslate(CATransform3DIdentity, 0, - keyboardHeight, 0);
//                     }
//                     completion:^(BOOL finished) {}];
//}
//
//-(void)keyboardWillHide:(NSNotification *)notification
//{
//    [UIView animateWithDuration:0.1
//                          delay:0.0
//                        options:UIViewAnimationOptionBeginFromCurrentState
//                     animations:^{
//                         self.bigBuyView.layer.transform = self.bigBuyViewTransform;
//                         danmuVC.editArea.layer.transform = CATransform3DIdentity;
//                     }
//                     completion:^(BOOL finished) {}];
//}

//-(void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    [self adjustDanmuViewArea];
//}

#pragma mark scrollview handler
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate) {
        [self afterScrollAction];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self afterScrollAction];
}

-(void)afterScrollAction
{
    [self pauseMovieIfNotFirstPage];
    [self adjustDotsColor];
    [self adjustLayoutWithCurrentPage];

    CGFloat offsetX = self.mainScrollView.contentOffset.x;
    CGFloat scrollViewWidth = self.mainScrollView.frame.size.width;
    //    CGFloat scrollViewHeight = self.mainScrollView.frame.size.height;
    CGFloat scrollViewPage = offsetX / scrollViewWidth;
    if (scrollViewPage == 1) {

        ALEcommerceProduct *product = [[ALEcommerceProduct alloc] init];
        
        product.productID = self.infoDict.safe[@"ContentInfo"][0][@"ProductID"];
        product.productTitle = self.infoDict.safe[@"ContentInfo"][0][@"ProductName"];
        
        product.price = [self.fitProductDictionary.safe[@"Price"] doubleValue];
        product.currency = self.fitProductDictionary.safe[@"Currency"];
        
        [[ALBuyProcessInspectorUnit logProductBuy:ALV3StreamViewDanmu
                                          Product:product
                                          DevDict:@{}] sendInspector];
    }
    else if (scrollViewPage == 2) {
        
        ALEcommerceProduct *product = [[ALEcommerceProduct alloc] init];
        
        product.productID = self.infoDict.safe[@"ContentInfo"][0][@"ProductID"];
        product.productTitle = self.infoDict.safe[@"ContentInfo"][0][@"ProductName"];
        
        product.price = [self.fitProductDictionary.safe[@"Price"] doubleValue];
        product.currency = self.fitProductDictionary.safe[@"Currency"];
        
        [[ALBuyProcessInspectorUnit logProductBuy:ALV3StreamViewProductDetail
                                          Product:product
                                          DevDict:@{}] sendInspector];
    }
}

-(void)pauseMovieIfNotFirstPage
{
    CGFloat offsetX = self.mainScrollView.contentOffset.x;
    CGFloat scrollViewWidth = self.mainScrollView.frame.size.width;
//    CGFloat scrollViewHeight = self.mainScrollView.frame.size.height;
    CGFloat scrollViewPage = offsetX / scrollViewWidth;
    if (scrollViewPage > 0) {
        [self pause];
    }
    else {
        [self resume];
    }
}

- (void)adjustDotsColor
{
    CGFloat offsetX = self.mainScrollView.contentOffset.x;
    CGFloat scrollViewWidth = self.mainScrollView.frame.size.width;
    
    NSUInteger scrollViewPage = offsetX / scrollViewWidth;
    NSArray *buttons = @[self.pageDot1, self.pageDot2, self.pageDot3];
    for (UIButton *btn in buttons) {
        if ([buttons indexOfObject:btn] == scrollViewPage) {
            // Current page
            btn.backgroundColor = [UIColor colorWithHexString:@"E93A3F"];
        }
        else {
            btn.backgroundColor = [UIColor colorWithHexString:@"595959"];
        }
    }
}

- (void)adjustLayoutWithCurrentPage
{
    CGFloat offsetX = self.mainScrollView.contentOffset.x;
    CGFloat scrollViewWidth = self.mainScrollView.frame.size.width;
    
    NSUInteger scrollViewPage = offsetX / scrollViewWidth;
    self.dismissButton.hidden = self.productBoxImageView.hidden = self.followImageView.hidden = self.followButton.hidden = self.buyCountLabel.hidden = self.followActionButton.hidden = self.addFollowLabel.hidden = (scrollViewPage == 1);
}

-(void)scrollToFirstPage
{
    CGFloat offsetX = self.mainScrollView.contentOffset.x;
    CGFloat scrollViewWidth = self.mainScrollView.frame.size.width;
    //    CGFloat scrollViewHeight = self.mainScrollView.frame.size.height;
    CGFloat scrollViewPage = offsetX / scrollViewWidth;
    if (scrollViewPage > 0) {
        [self.mainScrollView setContentOffset:CGPointZero animated:YES];
        [self adjustDotsColor];
        [self resume];
    }
}
//-(void)adjustDanmuViewArea
//{
//    CGFloat offsetX = self.mainScrollView.contentOffset.x;
//    CGFloat scrollViewWidth = self.mainScrollView.frame.size.width;
//    CGFloat scrollViewHeight = self.mainScrollView.frame.size.height;
//
//    CGFloat scrollViewPage = offsetX / scrollViewWidth;
//    if (self.mainScrollView.contentOffset.x <= scrollViewWidth) {
//        danmuVC.view.frame = CGRectMake(offsetX, 0, scrollViewWidth, scrollViewHeight);
//    }
//    
//    if (offsetX == scrollViewWidth) {
//        danmuVC.editArea.hidden = NO;
//    }
//    else {
//        danmuVC.editArea.hidden = YES;
//    }
//
//    if (offsetX < scrollViewWidth) {
//        [danmuVC hideAllCommentsViewAction:nil];
//    }
//    
//    if ([danmuVC.textField isFirstResponder]) {
//        danmuVC.textField.text = @"";
//        [danmuVC.textField resignFirstResponder];
//    }
//}
#pragma mark show/hide danmu
- (IBAction)toggleDanmuDisplay:(UIButton *)sender
{
    if ([danmuVC.view isHidden]) {
        for (UIButton *btn in self.toggleDamnuButtons) {
            [btn setImage:[UIImage imageNamed:@"CommentClose"] forState:UIControlStateNormal];
        }
    }
    else {
        for (UIButton *btn in self.toggleDamnuButtons) {
            [btn setImage:[UIImage imageNamed:@"CommentOpen"] forState:UIControlStateNormal];
        }
    }
    danmuVC.view.hidden = !danmuVC.view.hidden;
}

#pragma mark - ALCheckoutViewControllerDelegate

- (void)checkoutViewControllerDidTapDismissButton:(ALCheckoutViewController *)checkoutViewController
{
    [self hiddenBigBuyView:nil];
}

- (void)checkoutViewControllerDidTapAddButton:(ALCheckoutViewController *)checkoutViewController
{
    [self addStockAction:nil];
}

- (void)checkoutViewControllerDidTapMinusButton:(ALCheckoutViewController *)checkoutViewController
{
    [self minusStockAction:nil];
}


@end
