//
//  ALVLCPlayerViewController.h
//  alorth
//
//  Created by w91379137 on 2015/10/29.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALBasicSubViewController.h"
#define ALVLCPlayerReloadPeriodGap 15
#import "ALDamnuViewController.h"
@interface ALVLCPlayerViewController : ALBasicSubViewController
<ALSelectTypeScrollViewDelegate, UIScrollViewDelegate>
{
    //時間相關
    NSTimer *reloadTimer;                   //通用計時器 單位 秒
    
    
    float reloadPeriod;                     //距離上次更新累計時間(live 專用)
    
    IBOutlet UIButton *chatActionButton;
    ALDamnuViewController *danmuVC;
}
@property (nonatomic)float accumulationTime;                 //打開螢幕累計時間

@property (nonatomic, strong, readonly) NSString *streamURLString;  //串流URL(完全由 啟動資訊 決定)
@property (nonatomic, strong) NSDictionary *infoDict;               //啟動資訊

@property (nonatomic, strong) NSMutableDictionary *productA052InfoDict; //所有商品的資訊
@property (nonatomic) NSInteger currentVideoClip;   //按照 accumulationTime 計算所得的段數
@property (nonatomic) NSInteger selectVideoClip;    //點開 購買畫面當下的段數

- (NSDictionary *)productInfoDictOfContentInfo:(NSInteger)index;

//主畫面
@property (nonatomic, strong) UIView *aVLCMediaView;    //影像畫面

@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *loadingIndicator; //負責從 按下重播 到 開始播放
@property (nonatomic, strong) IBOutlet UIButton *replayButton;//負責 影片停止 到 按下重播

@property (nonatomic, strong) NSString *followStatus;
@property (nonatomic, strong) IBOutlet UIScrollView *mainScrollView; //左右感應畫面
@property (nonatomic, strong) IBOutlet UIView *mainScrollContainerView;

//小的購買畫面
@property (nonatomic, strong) IBOutlet UIView *smallBuyView;

//大的購買畫面
@property (nonatomic, strong) UIButton *bigBuyViewRuturnButton;

@property (nonatomic, strong) IBOutlet UIView *bigBuyView;
@property (nonatomic) CATransform3D bigBuyViewTransform;

@property (nonatomic, strong) IBOutlet UILabel *productNameLabel;
@property (nonatomic, strong) IBOutlet ALSelectTypeScrollView *typeSelectTypeScrollView;
@property (nonatomic, strong) NSString *sizeString;
@property (nonatomic, strong) NSDictionary *fitProductDictionary; //選中的商品單一資訊

@property (nonatomic, strong) IBOutlet UILabel *stockNumber;

@property (nonatomic, strong) IBOutlet UITextField *stockTextField;
@property (nonatomic, strong) IBOutlet UILabel *totalFeeLabel;
@property (nonatomic, strong) IBOutlet UILabel *shippingFeeFeeLabel;

@property (nonatomic, strong) IBOutlet UIView *actionContainerView;
@property (nonatomic, strong) ALOnlinePaymentViewController *onlinePaymentVC;
@property (nonatomic, strong) IBOutlet UIView *addToCartView;

//分享按鈕
@property (nonatomic, strong) IBOutlet UIView *shareButtonView;

// 彈幕畫面
@property (nonatomic, strong) IBOutlet UIView *partialDanmuListView;
@property (nonatomic, strong) IBOutlet UIView *fullDanmuListView;

//個人卡畫面
@property (nonatomic, strong) IBOutlet UIImageView *avaterImageView;
@property (nonatomic, strong) IBOutlet UILabel *userDisplayNameLabel;
@property (nonatomic, strong) IBOutlet UIView *folloChatView;

// Remind view
@property (nonatomic, weak) IBOutlet UIView *remindSwipeView;

// 頁面點點點
@property (nonatomic, weak) IBOutlet UIButton *pageDot1;
@property (nonatomic, weak) IBOutlet UIButton *pageDot2;
@property (nonatomic, weak) IBOutlet UIButton *pageDot3;

@property (nonatomic, weak) id player;

@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *toggleDamnuButtons;
@property (nonatomic, weak) IBOutlet UIButton *videoPauseButton;

// 切換頁面可能更改顯示的 UI
@property (nonatomic, weak) IBOutlet UIButton *dismissButton;
@property (nonatomic, weak) IBOutlet UIImageView *productBoxImageView;
@property (nonatomic, weak) IBOutlet UIImageView *followImageView;
@property (nonatomic, weak) IBOutlet UIButton *followButton;
@property (nonatomic, weak) IBOutlet UILabel *buyCountLabel;
//追隨按鈕
@property (nonatomic, weak) IBOutlet UIButton *followActionButton;
//追隨+號
@property (nonatomic, weak) IBOutlet UILabel *addFollowLabel;

// 商品圖片與介紹資訊
@property (nonatomic, weak) IBOutlet UIImageView *productImageView;
@property (nonatomic, weak) IBOutlet UILabel *productTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *productIntroductionLabel;

// 影片進度調
@property (nonatomic, weak) IBOutlet UISlider *movieSlider;
@end
