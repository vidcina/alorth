//
//  ALStreamHelper.m
//  alorth
//
//  Created by w91379137 on 2015/10/21.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALStreamHelper.h"

@implementation ALStreamHelper

+(NSString *)userNameString:(NSDictionary *)infoDict
{
    NSString *userName =
    [ALFormatter preferredDisplayNameFirst:infoDict[@"FirstName"]
                                      Last:infoDict[@"LastName"]
                                    UserID:infoDict[@"StreamerUserID"]];
    
    if (userName.length == 0) {
        userName =
        [ALFormatter preferredDisplayNameFirst:infoDict[@"AccountFirstName"]
                                          Last:infoDict[@"AccountLastName"]
                                        UserID:infoDict[@"StreamerUserID"]];
    }
    return userName;
}

+ (BOOL)isLiveType:(NSDictionary *)infoDict
{
    NSString *videoType =
    [infoDict[@"VideoType"] isKindOfClass:[NSString class]] ? infoDict[@"VideoType"] : @"";
    BOOL isLive = [videoType isEqualToString:@"live"];
    
    return isLive;
}

+ (UIColor *)textColor:(BOOL)isLiveType
{
    return isLiveType ? COMMON_GRAY4_COLOR : COMMON_Yellow_COLOR;
}

+ (UIColor *)bgColor:(BOOL)isLiveType
{
    return isLiveType ? COMMON_Yellow_COLOR : COMMON_GRAY4_COLOR;
}

+ (NSString *)streamVideoURL:(NSDictionary *)infoDict
{
    //測試用位置
    //return @"rtmp://service.alorth.com:1935/live/aaa";
    //return @"rtmp://service.alorth.com:1935/live/bbb";
    
    if ([infoDict[@"StreamURL"] isKindOfClass:[NSString class]] && ![self isLiveType:infoDict]) {
        return [infoDict[@"StreamURL"] stringByReplacingOccurrencesOfString:@"https"
                                                         withString:@"http"];
    }

    if ([infoDict[@"StreamPrefix"] isKindOfClass:[NSString class]] &&
        [infoDict[@"StreamID"] isKindOfClass:[NSString class]]) {
        NSString *combinedHLSURL = [NSString stringWithFormat:@"%@%@",
                                     infoDict[@"StreamPrefix"],
                                     infoDict[@"StreamID"]];
                                    
        return [[combinedHLSURL stringByReplacingOccurrencesOfString:@"http" withString:@"rtmp"] stringByReplacingOccurrencesOfString:@"http" withString:@"rtmp"];
    }

    
//    if ([infoDict[@"StreamPrefix"] isKindOfClass:[NSString class]] &&
//        [infoDict[@"StreamID"] isKindOfClass:[NSString class]]) {
//        return [NSString stringWithFormat:@"%@%@/playlist.m3u8",
//                infoDict[@"StreamPrefix"],
//                infoDict[@"StreamID"]];
//    }
    return nil;
}

@end
