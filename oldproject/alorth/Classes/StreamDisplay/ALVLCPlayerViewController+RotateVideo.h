//
//  ALVLCPlayerViewController+RotateVideo.h
//  alorth
//
//  Created by John Hsu on 2016/6/6.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ALVLCPlayerViewController.h"
@interface ALVLCPlayerViewController (RotateVideo)
-(void)orientationChanged;
@end
