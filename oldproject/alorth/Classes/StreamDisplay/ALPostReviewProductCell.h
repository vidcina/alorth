//
//  ALPostReviewProductCell.h
//  alorth
//
//  Created by ZZB on 2016/4/27.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ALPostReviewProductCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *productImageView;
@property (nonatomic, weak) IBOutlet UILabel *productLabel;

@end
