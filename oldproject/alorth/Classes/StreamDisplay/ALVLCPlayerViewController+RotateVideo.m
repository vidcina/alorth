//
//  ALVLCPlayerViewController+RotateVideo.m
//  alorth
//
//  Created by John Hsu on 2016/6/6.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALVLCPlayerViewController+RotateVideo.h"
#import <MediaPlayer/MediaPlayer.h>
@implementation ALVLCPlayerViewController (RotateVideo)
-(void)orientationChanged
{
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    MPMoviePlayerController *mpc = maybe(self.player, MPMoviePlayerController);
    CGSize movieSize = mpc.naturalSize;
    if (!CGSizeEqualToSize(movieSize, CGSizeZero) && movieSize.width > movieSize.height) {
        [UIView animateWithDuration:0.1 animations:^{
            if (UIDeviceOrientationIsLandscape(orientation)) {
                mpc.view.transform = CGAffineTransformMakeRotation(M_PI_2);
                mpc.view.frame = self.view.bounds;
            }
            else if (UIDeviceOrientationIsPortrait(orientation)) {
                mpc.view.transform = CGAffineTransformIdentity;
                mpc.view.frame = self.view.bounds;
            }
        }];
        
    }
    
}
@end
