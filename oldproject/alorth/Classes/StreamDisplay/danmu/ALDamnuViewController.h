//
//  ALDamnuViewController.h
//  DanmuTest
//
//  Created by John Hsu on 2016/3/8.
//  Copyright © 2016年 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Alorth-swift.h"
#import "ALDamnuCell.h"

//#import <SocketIOClientSwift/SocketIOClientSwift-Swift.h>
//@import SocketIOClientSwift;

typedef enum : NSUInteger {
    ALDanmuFilterRuleNone, // all comments
    ALDanmuFilterRuleRecentComment,
    ALDanmuFilterRuleTopComment,
} ALDanmuFilterRule;

typedef NS_ENUM(NSUInteger, ALAllCommentsFileterType) {
    ALAllCommentsFileterTypeUpvote,
    ALAllCommentsFileterTypeDownVote,
    ALAllCommentsFileterTypeRecent
};

static NSTimeInterval kShowNextDanmuInterval = 5;

@interface ALDamnuViewController : UIViewController <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, ALDanmuUpdate>
{
    IBOutlet PeriscommentView *periView;
    int playCommentCounter;
    NSTimer *playCommentTimer;
    
    SocketIOClient* socket;
    ALDanmuFilterRule filterRule;
    BOOL isScoreReady;  // 已讀取購買分數
    IBOutlet UIView *filterSelectHeaderView;
}

-(void)replayComments;
-(IBAction)hideAllCommentsViewAction:(id)sender;

@property(nonatomic, strong) NSString *streamID;
@property(nonatomic, strong) NSArray *commentsArray;
@property(nonatomic, strong) NSArray *pointsArray;

@property(nonatomic, strong) NSArray *ruleFilteredCommentsArray;

@property(nonatomic, strong) IBOutlet UIView *editArea;
@property(nonatomic, strong) IBOutlet UITextField *textField;
@property(nonatomic, strong) IBOutlet UITableView *allCommentsTableView;
@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *filterButtons;
@property (nonatomic, assign) ALAllCommentsFileterType currentFilterType;


@end
