//
//  ViewController.m
//  DanmuTest
//
//  Created by John Hsu on 2016/3/8.
//  Copyright © 2016年 test. All rights reserved.
//

#import "ALDamnuCell.h"
#import "ALDanmuAPI.h"

@implementation ALDamnuCell

+(instancetype)cell
{
    ALDamnuCell *cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] firstObject];
    cell.roundRectedView.layer.cornerRadius = 6;
    cell.headImageView.layer.cornerRadius = 22;
    cell.headImageView.layer.masksToBounds = YES;
    cell.roundRectedView.layer.masksToBounds = YES;
#pragma mark set up nice and boo button and state image
    UIImage *likeImage = [UIImage imageNamed:@"like1"];
    UIImage *likeNormalImage = [UIImage changeColor:likeImage color:[UIColor colorWithHexString:@"9D9D9D"]];
    UIImage *likeSelectedImage = [UIImage changeColor:likeImage color:[UIColor colorWithHexString:@"E2E2E2"]];
    
    UIImage *unlikeNormalImage = [UIImage changeColor:likeImage color:[UIColor colorWithHexString:@"9D9D9D"]];
    UIImage *unlikeSelectedImage = [UIImage changeColor:likeImage color:[UIColor colorWithHexString:@"E2E2E2"]];
    [cell.niceButton setImage:likeNormalImage forState:UIControlStateNormal];
    [cell.niceButton setImage:likeSelectedImage forState:UIControlStateSelected];
    [cell.niceButton setBackgroundImage:[UIImage makePureColorImage:cell.niceButton.bounds.size Color:[UIColor clearColor]] forState:UIControlStateNormal];
    [cell.niceButton setBackgroundImage:[UIImage makePureColorImage:cell.niceButton.bounds.size Color:[UIColor colorWithHexString:@"239384"]] forState:UIControlStateSelected];

    [cell.booButton setImage:unlikeNormalImage forState:UIControlStateNormal];
    [cell.booButton setImage:unlikeSelectedImage forState:UIControlStateSelected];
    [cell.booButton setBackgroundImage:[UIImage makePureColorImage:cell.niceButton.bounds.size Color:[UIColor clearColor]] forState:UIControlStateNormal];
    [cell.booButton setBackgroundImage:[UIImage makePureColorImage:cell.niceButton.bounds.size Color:[UIColor colorWithHexString:@"239384"]] forState:UIControlStateSelected];

    cell.booButton.transform = CGAffineTransformMakeRotation(M_PI);

    return cell;
}

-(IBAction)niceAction:(id)sender
{
    if ([PDSAccountManager isLoggedIn]) {
        int totalScore = [self.scoreLabel.text intValue];
        int myScore;
        if (self.niceButton.selected) { // 要取消good
            totalScore --;
            myScore = 0;
            self.niceButton.selected = NO;
        }
        else if (self.booButton.selected) { // 取消boo並改為good
            totalScore += 2;
            myScore = 1;
            self.niceButton.selected = YES;
            self.booButton.selected = NO;
        }
        else {  //加分
            self.niceButton.selected = YES;
            myScore = 1;
            totalScore ++;
        }
        if (totalScore > 0) {
            self.scoreLabel.text = [NSString stringWithFormat:@"+%d",totalScore];
        }
        else {
            self.scoreLabel.text = [NSString stringWithFormat:@"%d",totalScore];
        }
        [[ALDanmuAPI sharedInstance] voteComment:myScore commentID:self.commentData[@"id"] forUserID:[[ALAppDelegate sharedAppDelegate] userID] userName:[[ALAppDelegate sharedAppDelegate] userNameString] completeBlock:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
            ALLog(@"加分:%@",responseObject);
            if (self.updater) {
                [self.updater updateDanmuDictionary:self.commentData withScore:totalScore];
            }
        }];
        
    }
    else {
        YKSimpleAlert(@"請先登入");
    }
}

-(IBAction)booAction:(id)sender
{
    if ([PDSAccountManager isLoggedIn]) {
        int totalScore = [self.scoreLabel.text intValue];
        int myScore;
        if (self.booButton.selected) { // 要取消boo
            totalScore ++;
            myScore = 0;
            self.booButton.selected = NO;
        }
        else if (self.niceButton.selected) { // 取消good並改為boo
            totalScore -= 2;
            myScore = -1;
            self.niceButton.selected = NO;
            self.booButton.selected = YES;
        }
        else {  //減分
            self.booButton.selected = YES;
            myScore = -1;
            totalScore --;
        }
        if (totalScore > 0) {
            self.scoreLabel.text = [NSString stringWithFormat:@"+%d",totalScore];
        }
        else {
            self.scoreLabel.text = [NSString stringWithFormat:@"%d",totalScore];
        }
        
        if (self.updater) {
            [self.updater updateDanmuDictionary:self.commentData withScore:totalScore];
        }
        [[ALDanmuAPI sharedInstance] voteComment:myScore commentID:self.commentData[@"id"] forUserID:[[ALAppDelegate sharedAppDelegate] userID] userName:[[ALAppDelegate sharedAppDelegate] userNameString] completeBlock:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
            ALLog(@"減分:%@",responseObject);
            
        }];

    }
    else {
        YKSimpleAlert(@"請先登入");
    }
}

@end
