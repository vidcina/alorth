//
//  ViewController.h
//  DanmuTest
//
//  Created by John Hsu on 2016/3/8.
//  Copyright © 2016年 test. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ALDanmuUpdate
-(void)updateDanmuDictionary:(NSDictionary *)dict withScore:(NSInteger)score;
@end

@interface ALDamnuCell : UIView
+(instancetype)cell;
@property(nonatomic,weak) IBOutlet UIView *roundRectedView;
@property(nonatomic,weak) IBOutlet UIImageView *headImageView;
@property(nonatomic,weak) IBOutlet UILabel *titleLabel;
@property(nonatomic,weak) IBOutlet UILabel *timeLabel;

@property(nonatomic,weak) IBOutlet UILabel *detailLabel;
@property(nonatomic,weak) IBOutlet UILabel *scoreLabel;

@property(nonatomic,weak) IBOutlet UIButton *niceButton;
@property(nonatomic,weak) IBOutlet UIButton *booButton;

@property (nonatomic,strong) NSDictionary *commentData;
@property (nonatomic,strong) NSDictionary *myVoteData;

@property (nonatomic, weak) id<ALDanmuUpdate> updater;
@end
