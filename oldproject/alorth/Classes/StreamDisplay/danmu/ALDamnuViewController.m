//
//  ALDamnuViewController.m
//  DanmuTest
//
//  Created by John Hsu on 2016/3/8.
//  Copyright © 2016年 test. All rights reserved.
//

#import "ALDamnuViewController.h"
#import "ALDanmuAPI.h"
#import "ALAllDanmuCell.h"
#import "ALUserInfoCacheManager.h"
#import "ALVLCPlayerViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <PLPlayerKitFramework/PLPlayerKitFramework.h>
@implementation ALDamnuViewController
-(void)dealloc
{
    [socket removeAllHandlers];
    [socket disconnect];
    [socket close];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        filterRule = ALDanmuFilterRuleTopComment;
        NSURL* url = [[NSURL alloc] initWithString:Danmu_Socket_Address];
        socket = [[SocketIOClient alloc] initWithSocketURL:url options:
  @{@"log": @YES,
    @"forcePolling": @YES,
    @"connectParams" : @{ @"__sails_io_sdk_version" : @"0.13.6" }
    }
                  ];
        __weak SocketIOClient *weakSocket = socket;
        weakSelfMake(weakSelf);
        [socket on:@"connect" callback:^(NSArray *data, SocketAckEmitter *ack) {
            //NSLog(@"socket connected");
            [weakSocket emit:@"get" withItems:@[
                                            @{
                                                @"method" : @"get",
                                                @"url" : [NSString stringWithFormat:@"/api/v1/comments/join/%@",weakSelf.streamID]
                                                }
                                            
                                            ]];
        }];
        [socket on:@"comment.created" callback:^(NSArray *array, SocketAckEmitter *ack) {
            //NSLog(@"get comment:%@",array);
            for (int i=0; i<[array count]; i++) {
                NSDictionary *dict = array[i];
                if (dict[@"message"] && dict[@"user"]) {
                    [weakSelf displayCommentNow:dict];
                    self.commentsArray = [self.commentsArray arrayByAddingObject:dict];
                }
            }
            [self reoderCommentsWithCurrentFilter];
            [self.allCommentsTableView reloadData];
        }];
        [socket connect];

    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (playCommentTimer) {
        [playCommentTimer invalidate];
        playCommentTimer = nil;
    }
    playCommentTimer = [NSTimer timerWithTimeInterval:kShowNextDanmuInterval target:self selector:@selector(playComments) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:playCommentTimer forMode:NSRunLoopCommonModes];
}

-(void)viewWillDisappear:(BOOL)animated
{
    if (playCommentTimer) {
        [playCommentTimer invalidate];
        playCommentTimer = nil;
    }
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.textField.layer.cornerRadius = 6;
    self.allCommentsTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    for (UIButton *filterButton in self.filterButtons) {
        [filterButton setBackgroundImage:[UIImage makePureColorImage:filterButton.bounds.size Color:[UIColor colorWithHexString:@"94CEC8"]] forState:UIControlStateNormal];
        [filterButton setBackgroundImage:[UIImage makePureColorImage:filterButton.bounds.size Color:[UIColor colorWithHexString:@"208577"]] forState:UIControlStateSelected];
        [filterButton setTitleColor:[UIColor colorWithHexString:@"23988A"] forState:UIControlStateNormal];
        [filterButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        if (filterButton.tag == ALAllCommentsFileterTypeUpvote) {
            filterButton.selected = YES;
        }
    }
    self.currentFilterType = ALAllCommentsFileterTypeUpvote;
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)aTextField
{
    [aTextField resignFirstResponder];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)aTextField
{
    if (![self.textField.text length]) {
        return;
    }
    if (![PDSAccountManager isLoggedIn]) {
        YKSimpleAlert(@"請先登入");
        return;
    }
    NSString *string = self.textField.text;
    NSTimeInterval time = 0;
    
    id player = [(ALVLCPlayerViewController *)self.parentViewController player];
    PLPlayer *plPlayer = maybe(player, PLPlayer);
    if (plPlayer) {
        time = ceilf([(ALVLCPlayerViewController *)self.parentViewController accumulationTime]);
    }
    else {
        MPMoviePlayerController *mpc = [(ALVLCPlayerViewController *)self.parentViewController player];
        time = ceil(mpc.currentPlaybackTime);
    }
    
    if (time < 1) {
        return;
    }
#pragma mark send thru api
    [socket emit:@"post" withItems:@[
                                     @{
                                         @"method" : @"post",
                                         @"url" : @"/api/v1/comments",
                                         @"data": @{
                                                 @"time": [NSString stringWithFormat:@"%.0f",time],
                                                 @"message": string,
                                                 @"video": @{
                                                         @"id": self.streamID
                                                         },
                                                 @"user": @{
                                                         @"id": [[ALAppDelegate sharedAppDelegate] userID],
                                                         @"name": [[ALAppDelegate sharedAppDelegate] userNameString]
                                                         }
                                                 }
                                         }
                                     ]];

    self.textField.text = @"";
}

-(void)displayCommentNow:(NSDictionary *)comment
{
    ALDamnuCell *cell = [ALDamnuCell cell];
    cell.updater = self;
    cell.commentData = comment;
    cell.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    int time = [comment[@"time"] intValue];
    int hour = time / 3600;
    int remain = time % 3600;
    int minute = remain / 60;
    int second = remain % 60;
    cell.timeLabel.text = [NSString stringWithFormat:@"%02d:%02d:%02d",hour,minute,second];
    cell.titleLabel.text = comment[@"user"];
    NSString *string = comment[@"message"];
    int score = [comment[@"totalScore"] intValue];
    if (score > 0) {
        cell.scoreLabel.text = [NSString stringWithFormat:@"+%@",comment[@"totalScore"]];
    }
    else {
        cell.scoreLabel.text = [NSString stringWithFormat:@"%@",comment[@"totalScore"]];
    }
    cell.detailLabel.text = string;
    [cell layoutIfNeeded];
    CGSize size = cell.roundRectedView.frame.size;
    cell.frame = CGRectMake(0, 0, self.view.bounds.size.width, size.height + 20);
    [ALUserInfoCacheManager getLocalUserInfomation:comment[@"user"]
                                   completionBlock:^(NSDictionary *responseObject) {
                                       NSString *headImage = responseObject.safe[@"resBody"][@"HeadImage"];
                                       [cell.headImageView imageFromURLString:headImage];
                                       //  NSLog(@"responseObject:%@",responseObject);
                                   }];
    NSDictionary *scoreInfo = [self.pointsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K==%@",@"user",comment[@"user"]]].firstObject;
    double buyScore = [scoreInfo[@"score"] doubleValue];
    if (buyScore > 0) {
        cell.roundRectedView.backgroundColor = [UIColor colorWithHexString:@"FDEE67"];
    }
    else {
        cell.roundRectedView.backgroundColor = [UIColor whiteColor];
    }
    [periView addCell:cell];
}

-(void)setStreamID:(NSString *)streamID
{
    if (![_streamID isEqualToString:streamID]) {
        _streamID = streamID;
        [[ALDanmuAPI sharedInstance] getComments:streamID completeBlock:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
//            NSLog(@"responseObject:%@",responseObject);
            self.commentsArray = responseObject;
//            self.pointsArray = responseObject[@"points"];
            [self reoderCommentsWithCurrentFilter];
            playCommentCounter = 0;  // replay
            [self.allCommentsTableView reloadData];
        }];
// streamID
        [[ALDanmuAPI sharedInstance] getPoints:streamID completeBlock:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
            //            NSLog(@"responseObject:%@",responseObject);
            /* 資料結構參考
            {
                createdAt = 1457384738876;
                id = 11;
                score = 3;
                updatedAt = 1457384750966;
                user = 123;
                video = v001;
            }
            */
            self.pointsArray = responseObject;
            isScoreReady = YES;
            [self reoderCommentsWithCurrentFilter];
        }];
    }
    
}
/*
-(void)updateFilteredArrayUsingRule
{
    NSArray *ruleSortedArray = nil;
    // rule not implemented
    if (filterRule == ALDanmuFilterRuleTopComment) {
        NSArray *scoreSortedArray = [self.commentsArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *comment1, NSDictionary *comment2) {
            // 取得購買分數
            NSDictionary *scoreInfo1 = [self.pointsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K==%@",@"user",comment1[@"user"]]].firstObject;
            NSDictionary *scoreInfo2 = [self.pointsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K==%@",@"user",comment2[@"user"]]].firstObject;

            double buyScore1 = [scoreInfo1[@"score"] doubleValue];
            double buyScore2 = [scoreInfo2[@"score"] doubleValue];
            double commentScore1 = [comment1[@"totalScore"] doubleValue];
            double commentScore2 = [comment2[@"totalScore"] doubleValue];

            //f = 45000 x log10(n) + t
            // 分數由大到小排 (新留言的優先顯示)
            double priority1 = (double)45000 * log10(buyScore1 * 10 + commentScore1) + [comment1[@"createdAt"] doubleValue];
            double priority2 = (double)45000 * log10(buyScore2 * 10 + commentScore2) + [comment2[@"createdAt"] doubleValue];
            
            if (priority1 > priority2) {
                return NSOrderedAscending;
            }
            else if (priority1 < priority2) {
                return NSOrderedDescending;
            }
            else {
                return NSOrderedSame;
            }
        }];
        ruleSortedArray = scoreSortedArray;
        self.ruleFilteredCommentsArray = ruleSortedArray;
    }
    else if (filterRule == ALDanmuFilterRuleRecentComment) {
        ruleSortedArray = [self.commentsArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *comment1, NSDictionary *comment2) {
            double score1 = [comment1[@"createdAt"] doubleValue];
            double score2 = [comment2[@"createdAt"] doubleValue];
             // 時間由大到小排 (新留言的優先顯示)
            if (score1 > score2) {
                return NSOrderedAscending;
            }
            else if (score1 < score2) {
                return NSOrderedDescending;
            }
            else {
                return NSOrderedSame;
            }
        }];
        self.ruleFilteredCommentsArray = ruleSortedArray;
    }
#pragma mark cut to video length capacity
    id player = [(ALVLCPlayerViewController *)self.parentViewController player];
    MPMoviePlayerController *mpPlayer = maybe(player, MPMoviePlayerController);
    if (mpPlayer && [mpPlayer duration]) { // 影片，非直播
        NSInteger capacity = [mpPlayer duration] / kShowNextDanmuInterval;
        ruleSortedArray = [ruleSortedArray subarrayWithRange:
                           NSMakeRange(0,  MIN(ruleSortedArray.count, capacity)  )];
    }
    else {  // 直播，不限制
        
    }

    if (mpPlayer && !isnan([mpPlayer currentPlaybackTime])) {
        playCommentCounter = 0;
        //[mpPlayer currentPlaybackTime] / kShowNextDanmuInterval;
    }
    else {
        playCommentCounter = 0;
    }
}
*/
- (void)reoderCommentsWithCurrentFilter
{
#pragma mark 一律將播放用(self.commentsArray)的彈幕呈分數排列
    self.commentsArray = [self.commentsArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *comment1, NSDictionary *comment2) {
        // 取得購買分數
        NSDictionary *scoreInfo1 = [self.pointsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K==%@",@"user",comment1[@"user"]]].firstObject;
        NSDictionary *scoreInfo2 = [self.pointsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K==%@",@"user",comment2[@"user"]]].firstObject;
        
        double buyScore1 = [scoreInfo1[@"score"] doubleValue];
        double buyScore2 = [scoreInfo2[@"score"] doubleValue];
        double commentScore1 = [comment1[@"totalScore"] doubleValue];
        double commentScore2 = [comment2[@"totalScore"] doubleValue];
        
        //f = 45000 x log10(n) + t
        // 分數由大到小排 (新留言的優先顯示)
        double priority1 = (double)45000 * log10(buyScore1 * 10 + commentScore1) + [comment1[@"createdAt"] doubleValue];
        double priority2 = (double)45000 * log10(buyScore2 * 10 + commentScore2) + [comment2[@"createdAt"] doubleValue];
        
        if (priority1 > priority2) {
            return NSOrderedAscending;
        }
        else if (priority1 < priority2) {
            return NSOrderedDescending;
        }
        else {
            return NSOrderedSame;
        }
    }];

#pragma mark TableView顯示的彈幕清單(self.ruleFilteredCommentsArray)依據選擇的過濾器排序排列
    switch (self.currentFilterType) {
        case ALAllCommentsFileterTypeUpvote:
        case ALAllCommentsFileterTypeDownVote:
        {
            NSArray *scoreSortedArray = [self.commentsArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *comment1, NSDictionary *comment2) {
                // 取得購買分數
                NSDictionary *scoreInfo1 = [self.pointsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K==%@",@"user",comment1[@"user"]]].firstObject;
                NSDictionary *scoreInfo2 = [self.pointsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K==%@",@"user",comment2[@"user"]]].firstObject;
                
                double buyScore1 = [scoreInfo1[@"score"] doubleValue];
                double buyScore2 = [scoreInfo2[@"score"] doubleValue];
                double commentScore1 = [comment1[@"totalScore"] doubleValue];
                double commentScore2 = [comment2[@"totalScore"] doubleValue];
                
                //f = 45000 x log10(n) + t
                // 分數由大到小排 (新留言的優先顯示)
                double priority1 = (double)45000 * log10(buyScore1 * 10 + commentScore1) + [comment1[@"createdAt"] doubleValue];
                double priority2 = (double)45000 * log10(buyScore2 * 10 + commentScore2) + [comment2[@"createdAt"] doubleValue];
                
//                if (self.currentFilterType == ALAllCommentsFileterTypeUpvote) {
//                    
//                }
                if (self.currentFilterType == ALAllCommentsFileterTypeDownVote) {
                    priority1 = (double)-45000 * log10(buyScore1 * 10 + commentScore1) + [comment1[@"createdAt"] doubleValue];
                    priority2 = (double)-45000 * log10(buyScore2 * 10 + commentScore2) + [comment2[@"createdAt"] doubleValue];
                }
                if (priority1 > priority2) {
                    return NSOrderedAscending;
                }
                else if (priority1 < priority2) {
                    return NSOrderedDescending;
                }
                return NSOrderedSame;
            }];
            NSArray *ruleSortedArray = scoreSortedArray;
            self.ruleFilteredCommentsArray = ruleSortedArray;
            
            break;
        }
        case ALAllCommentsFileterTypeRecent:
        {
            NSArray *ruleSortedArray = [self.commentsArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *comment1, NSDictionary *comment2) {
                double score1 = [comment1[@"createdAt"] doubleValue];
                double score2 = [comment2[@"createdAt"] doubleValue];
                // 時間由大到小排 (新留言的優先顯示)
                if (score1 > score2) {
                    return NSOrderedAscending;
                }
                else if (score1 < score2) {
                    return NSOrderedDescending;
                }
                else {
                    return NSOrderedSame;
                }
            }];
            self.ruleFilteredCommentsArray = ruleSortedArray;
            break;
        }
        default:
            break;
    }
}

-(void)replayComments
{
    playCommentCounter = 0;
}

-(void)playComments
{
    if (!isScoreReady) {
        return;  // 購買分數還沒讀到先不播放
    }
    if (playCommentCounter >= [self.commentsArray count]) {
        return;  // 先不循環  playCommentCounter = 0;
    }
//    NSLog(@"playCommentCounter:%d",playCommentCounter);
    NSDictionary *comment = self.commentsArray[playCommentCounter];
    ALDamnuCell *cell = [ALDamnuCell cell];
    cell.updater = self;
    cell.commentData = comment;
    cell.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    int time = [comment[@"time"] intValue];
    int hour = time / 3600;
    int remain = time % 3600;
    int minute = remain / 60;
    int second = remain % 60;
    cell.timeLabel.text = [NSString stringWithFormat:@"%02d:%02d:%02d",hour,minute,second];
    cell.titleLabel.text = comment[@"user"];
    NSString *string = comment[@"message"];
    int score = [comment[@"totalScore"] intValue];
    if (score > 0) {
        cell.scoreLabel.text = [NSString stringWithFormat:@"+%@",comment[@"totalScore"]];
    }
    else {
        cell.scoreLabel.text = [NSString stringWithFormat:@"%@",comment[@"totalScore"]];
    }
    cell.detailLabel.text = string;
    [cell layoutIfNeeded];
    CGSize size = cell.roundRectedView.frame.size;
    cell.frame = CGRectMake(0, 0, self.view.bounds.size.width, size.height + 20);
    [ALUserInfoCacheManager getLocalUserInfomation:comment[@"user"]
                                   completionBlock:^(NSDictionary *responseObject) {
                                       NSString *headImage = responseObject.safe[@"resBody"][@"HeadImage"];
                                       [cell.headImageView imageFromURLString:headImage];
                                       cell.titleLabel.text = [ALFormatter preferredDisplayNameFirst:responseObject.safe[@"resBody"][@"FirstName"]
                                          Last:responseObject.safe[@"resBody"][@"LastName"]
                                          UserID:comment[@"user"]];
                                       // NSLog(@"responseObject:%@",responseObject);
                                   }];
    NSDictionary *scoreInfo = [self.pointsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K==%@",@"user",comment[@"user"]]].firstObject;
    double buyScore = [scoreInfo[@"score"] doubleValue];
    if (buyScore > 0) {
        cell.roundRectedView.backgroundColor = [UIColor colorWithHexString:@"FDEE67"];
    }
    else {
        cell.roundRectedView.backgroundColor = [UIColor whiteColor];
    }
    if ([PDSAccountManager isLoggedIn]) {
        [[ALDanmuAPI sharedInstance] getVoteForUserID:[[ALAppDelegate sharedAppDelegate] userID] forComment:comment[@"id"] completeBlock:^(NSURLSessionDataTask *task, NSArray *responseObject, NSError *error) {
            if ([responseObject count] > 0) {
                //NSLog(@"myvote:%@",responseObject);
                int myScore = [responseObject[0][@"score"] intValue];
                if (myScore == 1) {
                    cell.niceButton.selected = YES;
                }
                else if (myScore == -1) {
                    cell.booButton.selected = YES;
                }
                cell.myVoteData = responseObject[0];
            }
            [periView addCell:cell];
        }];
    }
    else {
        [periView addCell:cell];
    }
    
    playCommentCounter++;
}

-(IBAction)swipeUpAction:(id)sender
{
    /*
//    NSLog(@"swipeUpAction");
    [self.view addSubview:allCommentsTableView];
    [allCommentsTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    allCommentsTableView.contentOffset = CGPointMake(0, -self.view.frame.size.height);
    [UIView animateWithDuration:1 animations:^{
        allCommentsTableView.contentOffset = CGPointZero;
//        allCommentsTableView.alpha = 1;
    }];
    */
}

#pragma mark - IBAtions

-(IBAction)hideAllCommentsViewAction:(id)sender
{
    /*
    if (!allCommentsTableView.superview) {
        return;
    }
    [allCommentsTableView removeConstraints:allCommentsTableView.constraints];
//    allCommentsTableView.contentOffset = CGPointZero;
    [UIView animateWithDuration:1 animations:^{
        allCommentsTableView.contentOffset = CGPointMake(0, -1024);
//        allCommentsTableView.frame = CGRectMake(0, self.view.frame.size.height, allCommentsTableView.frame.size.width, allCommentsTableView.frame.size.height);
//        allCommentsTableView.alpha = 0;
    } completion:^(BOOL finished) {
        if (finished) {
            [allCommentsTableView removeFromSuperview];
        }
    }];
     */
}

-(IBAction)switchCommentModeAction:(id)sender
{
    /* 此按鈕目前隱藏，不作用
    if (filterRule == ALDanmuFilterRuleTopComment) {
        filterRule = ALDanmuFilterRuleRecentComment;
        [self.view makeToast:ALLocalizedString(@"Show Recent Comment",nil)];
    }
    else {
        filterRule = ALDanmuFilterRuleTopComment;
        [self.view makeToast:ALLocalizedString(@"Show Top Comment",nil)];
    }
    [self updateFilteredArrayUsingRule];
     */
}

- (IBAction)changeFilterStatus:(UIButton *)sender
{
    if ([sender isKindOfClass:[UIButton class]]) {
        UIButton *touchedButton = (UIButton *)sender;
        for (UIButton *btn in self.filterButtons) {
            if ([btn isEqual:touchedButton]) {
                btn.selected = YES;
                self.currentFilterType = (ALAllCommentsFileterType)btn.tag;
            }
            else {
                btn.selected = NO;
            }
        }
        [self reoderCommentsWithCurrentFilter];
        [self.allCommentsTableView reloadData];
    }
}

#pragma mark all comments tableview
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return filterSelectHeaderView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return filterSelectHeaderView.frame.size.height;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    return 60;
    return 130;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.ruleFilteredCommentsArray count] < 6) {
        return 6;
    }
    return [self.ruleFilteredCommentsArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row >= [self.ruleFilteredCommentsArray count]) {
        UITableViewCell *motherCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        return motherCell;
    }
    NSDictionary *comment = self.ruleFilteredCommentsArray[indexPath.row];
    ALDamnuCell *cell = [ALDamnuCell cell];
    cell.updater = self;
    cell.commentData = comment;
    cell.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    int time = [comment[@"time"] intValue];
    int hour = time / 3600;
    int remain = time % 3600;
    int minute = remain / 60;
    int second = remain % 60;
    cell.timeLabel.text = [NSString stringWithFormat:@"%02d:%02d:%02d",hour,minute,second];
    cell.titleLabel.text = comment[@"user"];
    NSString *string = comment[@"message"];
    int score = [comment[@"totalScore"] intValue];
    if (score > 0) {
        cell.scoreLabel.text = [NSString stringWithFormat:@"+%@",comment[@"totalScore"]];
    }
    else {
        cell.scoreLabel.text = [NSString stringWithFormat:@"%@",comment[@"totalScore"]];
    }
    cell.detailLabel.text = string;
    [cell layoutIfNeeded];
    CGSize size = cell.roundRectedView.frame.size;
    cell.frame = CGRectMake(0, 0, self.view.bounds.size.width, size.height + 20);
    
    NSDictionary *scoreInfo = [self.pointsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K==%@",@"user",comment[@"user"]]].firstObject;
    double buyScore = [scoreInfo[@"score"] doubleValue];
    if (buyScore > 0) {
        cell.roundRectedView.backgroundColor = [UIColor colorWithHexString:@"FDEE67"];
    }
    else {
        cell.roundRectedView.backgroundColor = [UIColor colorWithHexString:@"E7E7E7"];
    }
    if ([PDSAccountManager isLoggedIn]) {
        [[ALDanmuAPI sharedInstance] getVoteForUserID:[[ALAppDelegate sharedAppDelegate] userID] forComment:comment[@"id"] completeBlock:^(NSURLSessionDataTask *task, NSArray *responseObject, NSError *error) {
            if ([responseObject count] > 0) {
                //NSLog(@"myvote:%@",responseObject);
                int myScore = [responseObject[0][@"score"] intValue];
                if (myScore == 1) {
                    cell.niceButton.selected = YES;
                }
                else if (myScore == -1) {
                    cell.booButton.selected = YES;
                }
                cell.myVoteData = responseObject[0];
            }
        }];
    }
    UITableViewCell *motherCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    [motherCell.contentView addSubview:cell];
    [cell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(motherCell);
    }];
    [ALUserInfoCacheManager getLocalUserInfomation:comment[@"user"]
                                   completionBlock:^(NSDictionary *responseObject) {
                                       NSString *headImage = responseObject.safe[@"resBody"][@"HeadImage"];
                                       cell.titleLabel.text = [ALFormatter preferredDisplayNameFirst:responseObject.safe[@"resBody"][@"FirstName"]
                                              Last:responseObject.safe[@"resBody"][@"LastName"]
                                              UserID:comment[@"user"]];
                                       [cell.headImageView imageFromURLString:headImage];
//                                       NSLog(@"responseObject:%@",responseObject);
                                   }];
    return motherCell;
}

-(void)updateDanmuDictionary:(NSDictionary *)dict withScore:(NSInteger)score
{
    NSInteger dictIndexInAll = [self.commentsArray indexOfObject:dict];
    if (dictIndexInAll != NSNotFound) {
        NSMutableArray *copyArray = [self.commentsArray mutableCopy];
        NSMutableDictionary *copyDict = [dict mutableCopy];
        copyDict[@"totalScore"] = @(score);
        [copyArray replaceObjectAtIndex:dictIndexInAll withObject:copyDict];
        self.commentsArray = copyArray;
    }
    [self reoderCommentsWithCurrentFilter];
//    [self.allCommentsTableView reloadData];
}

/*
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView.contentOffset.y < -100 && scrollView == allCommentsTableView) {
//        [scrollView setContentOffset:CGPointMake(0, scrollView.contentOffset.y) animated:YES];
        [self hideAllCommentsViewAction:nil];
    }
}
-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y < -100 && scrollView == allCommentsTableView) {
        [scrollView setContentOffset:CGPointMake(0, scrollView.contentOffset.y) animated:YES];
    }
}
 */
@end
