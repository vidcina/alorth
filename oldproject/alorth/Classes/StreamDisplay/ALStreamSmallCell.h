//
//  ALRecordListCell.h
//  alorth
//
//  Created by w91379137 on 2015/10/14.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALCommonTableViewCell.h"

@interface ALStreamSmallCell : ALCommonTableViewCell

@property (nonatomic, strong) IBOutlet LineView *mainContainerView;

@property (nonatomic, strong) IBOutlet UIImageView *avaterImageView;
@property (nonatomic, strong) IBOutlet UIImageView *streamImageView;

@property (nonatomic, strong) IBOutlet UILabel *streamTitleLabel;
@property (nonatomic, strong) IBOutlet UILabel *streamTextLabel;

@property (nonatomic, strong) IBOutlet UIView *whitebgView;
//@property (nonatomic, strong) IBOutlet UILabel *typeLabel;

//@property (nonatomic, strong) IBOutlet UIImageView *peopleImageView;
//@property (nonatomic, strong) IBOutlet UILabel *peopleLabel;

@end
