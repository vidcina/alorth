//
//  ALStreamHelper.h
//  alorth
//
//  Created by w91379137 on 2015/10/21.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ALStreamHelper : NSObject

+ (NSString *)userNameString:(NSDictionary *)infoDict;
+ (BOOL)isLiveType:(NSDictionary *)infoDict;
+ (UIColor *)textColor:(BOOL)isLiveType;
+ (UIColor *)bgColor:(BOOL)isLiveType;
+ (NSString *)streamVideoURL:(NSDictionary *)infoDict;

@end
