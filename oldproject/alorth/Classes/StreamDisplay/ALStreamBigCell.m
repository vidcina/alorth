//
//  ALStreamBigCell.m
//  alorth
//
//  Created by w91379137 on 2015/10/14.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALStreamBigCell.h"
#import "ALMapPOI.h"
#import "ALStreamHelper.h"

@implementation ALStreamBigCell

- (void)cellFromXibSetting
{
    self.avaterImageView.clipsToBounds = YES;
    self.avaterImageView.layer.cornerRadius = self.avaterImageView.frame.size.height / 2;
    [self.avaterImageView.layer setBorderColor:[UIColor whiteColor].CGColor];
    [self.avaterImageView.layer setBorderWidth:2];
    
    self.streamTextLabel.numberOfLines = 0;
    
    //self.peopleImageView.layer.shadowOpacity = 0.3;
    //self.peopleLabel.layer.shadowOpacity = 0.3;
}

- (CGFloat)performWithInfoDict:(NSDictionary *)infoDict
{
    self.infoDict = infoDict;
    
    if (infoDict) {
        [self.avaterImageView imageFromURLString:infoDict[@"AccountHeadImage"]];
        [self.streamImageView imageFromURLString:infoDict[@"StreamImage"]];
        
        self.streamTitleLabel.text = [ALStreamHelper userNameString:infoDict];
        self.streamTextLabel.text = [NSString stringWithFormat:@"%@",infoDict[@"StreamName"]];
        
        //self.peopleLabel.text = infoDict.safe[@"ViewerNumber"];
    }
    
    return 225;
}

@end
