//
//  ALPostReviewViewController.m
//  alorth
//
//  Created by ZZB on 2016/4/27.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALPostReviewProductCell.h"
#import "ALPostReviewContentCell.h"
#import "ALPostReviewSendCell.h"
#import "Alorth-swift.h"

#import "ALPostReviewViewController.h"

static NSString *ALPostReviewProductCellIdentifier = @"ALPostReviewProductCell";
static NSString *ALPostReviewContentCellIdentifier = @"ALPostReviewContentCell";
static NSString *ALPostReviewSendCellIdentifier = @"ALPostReviewSendCell";

typedef NS_ENUM(NSUInteger, ALPostReviewCellType) {
    ALPostReviewCellTypeProduct,
    ALPostReviewCellTypeContent,
    ALPostReviewCellTypeSend
};

@interface ALPostReviewViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) UITextView *contentTextView;
@property (nonatomic, strong) SocketIOClient *socket;
@property (nonatomic, assign) NSTimeInterval requestTimeInterval;
@property (nonatomic, strong) NSTimer *countTimer;

@end

@implementation ALPostReviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.tableView registerNib:[UINib nibWithNibName:ALPostReviewProductCellIdentifier bundle:nil] forCellReuseIdentifier:ALPostReviewProductCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:ALPostReviewContentCellIdentifier bundle:nil] forCellReuseIdentifier:ALPostReviewContentCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:ALPostReviewSendCellIdentifier bundle:nil] forCellReuseIdentifier:ALPostReviewSendCellIdentifier];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    
    [self setupSocket];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [self.socket removeAllHandlers];
    [self.socket disconnect];
    [self.socket close];
}

#pragma mark - Privates

- (void)setupSocket
{
    self.socket = [[SocketIOClient alloc] initWithSocketURL:[NSURL URLWithString:Danmu_Socket_Address] options:
                   @{
                     @"log": @YES,
                     @"forcePolling": @YES,
                     @"connectParams" : @{ @"__sails_io_sdk_version" : @"0.13.6" }
                     }];
    __weak SocketIOClient *weakSocket = self.socket;
    [self.socket on:@"connect" callback:^(NSArray *data, SocketAckEmitter *ack) {
        [weakSocket emit:@"get" withItems:@[
                                            @{
                                                @"method" : @"get",
                                                @"url" : [NSString stringWithFormat:@"/api/v1/comments/join/%@", self.infoDict.safe[@"StreamID"]]
                                                }
                                            ]];
    }];
    [self.socket on:@"comment.created" callback:^(NSArray *array, SocketAckEmitter *ack) {
        for (int i=0; i<[array count]; i++) {
            NSDictionary *dict = array[i];
            if (dict[@"message"]
                && dict[@"user"]
                && [dict[@"message"] isEqualToString:self.contentTextView.text]
                && [dict[@"user"] isEqualToString:[[ALAppDelegate sharedAppDelegate] userNameString]]) {
                self.requestTimeInterval = 0.0;
                [self.countTimer invalidate];
                [self removeMBProgressHUDWithKey:@"postReview"];
                [self dismiss:nil];
            }
        }
    }];
    [self.socket connect];
}

- (void)countRequestTime:(NSTimer *)timer
{
    self.requestTimeInterval += 1.0;
    if (self.requestTimeInterval > 10.0) {
        self.requestTimeInterval = 0.0;
        [self.countTimer invalidate];
        YKSimpleAlert(@"留言失敗，請稍後再試");
        [self removeMBProgressHUDWithKey:@"postReview"];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case ALPostReviewCellTypeProduct:
        {
            ALPostReviewProductCell *cell = [tableView dequeueReusableCellWithIdentifier:ALPostReviewProductCellIdentifier forIndexPath:indexPath];
            cell.productLabel.text = self.infoDict.safe[@"ProductName"];
            [cell.productImageView imageFromURLString:self.infoDict.safe[@"ImageList"][0][@"ImagePath"]];

            return cell;
            break;
        }
        case ALPostReviewCellTypeContent:
        {
            ALPostReviewContentCell *cell = [tableView dequeueReusableCellWithIdentifier:ALPostReviewContentCellIdentifier forIndexPath:indexPath];
            self.contentTextView = cell.contentTextView;
            if (![self.contentTextView isFirstResponder]) {
                [self.contentTextView becomeFirstResponder];
            }
            return cell;
            break;
        }
        case ALPostReviewCellTypeSend:
        {
            ALPostReviewSendCell *cell = [tableView dequeueReusableCellWithIdentifier:ALPostReviewSendCellIdentifier forIndexPath:indexPath];
            [cell.sendButton addTarget:self action:@selector(sendReview:) forControlEvents:UIControlEventTouchUpInside];
            return cell;
            break;
        }
        default:
            break;
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case ALPostReviewCellTypeProduct:
            return 91;
            break;
        case ALPostReviewCellTypeContent:
            return 163;
            break;
        case ALPostReviewCellTypeSend:
            return 60;
            break;
        default:
            break;
    }
    return 0;
}

#pragma mark - IBAction

- (IBAction)dismiss:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)sendReview:(id)sender
{
    if (![self.contentTextView.text length]) {
        return;
    }
    if (![PDSAccountManager isLoggedIn]) {
        YKSimpleAlert(@"請先登入");
        return;
    }
    NSString *string = self.contentTextView.text;

//    [self addMBProgressHUDWithKey:@"postReview"];
    self.requestTimeInterval = 0.0;
//    self.countTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countRequestTime:) userInfo:nil repeats:YES];
    [self.socket emit:@"post" withItems:@[
                                     @{
                                         @"method" : @"post",
                                         @"url" : @"/api/v1/comments",
                                         @"data": @{
                                                 @"time": [NSString stringWithFormat:@"%.0f", self.videoPlayTime],
                                                 @"message": string,
                                                 @"video": @{
                                                         @"id": self.streamID
                                                         },
                                                 @"user": @{
                                                         @"id": [[ALAppDelegate sharedAppDelegate] userID],
                                                         @"name": [[ALAppDelegate sharedAppDelegate] userNameString]
                                                         }
                                                 }
                                         }
                                     ]];
    [self dismiss:nil];
}

@end
