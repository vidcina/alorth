//
//  ALPostReviewViewController.h
//  alorth
//
//  Created by ZZB on 2016/4/27.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ALPostReviewViewController : UIViewController

@property (nonatomic, strong) NSDictionary *infoDict;
@property (nonatomic, assign) NSTimeInterval videoPlayTime;
@property (nonatomic, strong) NSString *streamID;
@end
