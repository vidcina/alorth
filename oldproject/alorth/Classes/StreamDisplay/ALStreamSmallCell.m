//
//  ALRecordListCell.m
//  alorth
//
//  Created by w91379137 on 2015/10/14.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALStreamSmallCell.h"
#import "ALMapPOI.h"
#import "ALStreamHelper.h"
#import "UIImageView+ALColorChange.h"

@implementation ALStreamSmallCell

- (void)cellFromXibSetting
{
    self.avaterImageView.clipsToBounds = YES;
    self.avaterImageView.layer.cornerRadius = self.avaterImageView.frame.size.height / 2;
    [self.avaterImageView.layer setBorderColor:[UIColor whiteColor].CGColor];
    [self.avaterImageView.layer setBorderWidth:2];
    
    self.streamTextLabel.numberOfLines = 0;
}

- (CGFloat)performWithInfoDict:(NSDictionary *)infoDict
{
    self.infoDict = infoDict;
    
    if (infoDict) {
        [self.avaterImageView imageFromURLString:infoDict[@"AccountHeadImage"]];
        [self.streamImageView imageFromURLString:infoDict[@"StreamImage"]];
        
        self.streamTitleLabel.text = [ALStreamHelper userNameString:infoDict];
        self.streamTextLabel.text = [NSString stringWithFormat:@"%@",infoDict[@"StreamName"]];
    }
    
    return self.mainContainerView.frame.size.height;
}

/*
- (void)didTransitionToState:(UITableViewCellStateMask)state
{
    [super didTransitionToState:state];
    for (UIView *subview in self.subviews)
    {
        if ([NSStringFromClass([subview class]) isEqualToString:@"UITableViewCellEditControl"] ||
            [NSStringFromClass([subview class]) isEqualToString:@"UITableViewCellReorderControl"]) {
            for (UIView *subsubview in subview.subviews) {
                subsubview.alpha = 0.5;
            }
        }
    }
}
 */

-(void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    
    self.avaterImageView.hidden = editing;
    self.streamImageView.hidden = editing;
    self.whitebgView.hidden = editing;
    self.mainContainerView.lineWidth = editing ? 1 : 0;
    self.backgroundColor = editing ? [UIColor whiteColor] : [UIColor clearColor];
    
    [self.mainContainerView mas_updateConstraints:^(MASConstraintMaker *make) {
        if (editing) {
            make.leading.equalTo(self.mainContainerView.superview.mas_leading).offset(-55);//-38
            make.trailing.equalTo(self.mainContainerView.superview.mas_trailing).offset(50);//40
        }
        else {
            make.leading.equalTo(self.mainContainerView.superview.mas_leading);
            make.trailing.equalTo(self.mainContainerView.superview.mas_trailing);
        }
    }];
}

@end
