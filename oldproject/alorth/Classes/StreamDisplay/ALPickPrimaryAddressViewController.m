//
//  ALPickPrimaryAddressViewController.m
//  alorth
//
//  Created by ZZB on 2016/5/30.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALPickPrimaryAddressViewController.h"

#import "ALNetWorkAPI+V3_0.h"

static NSString *addressCellIdentifier = @"addressCellIdentifier";

@interface ALPickPrimaryAddressViewController ()

@property (nonatomic, strong) UIButton *saveButton;

- (IBAction)saveAddress:(id)sender;

@end

@implementation ALPickPrimaryAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:addressCellIdentifier];
    self.title = ALLocalizedString(@"Primary Shipping Address", nil);    
}

- (void)viewWillAppear:(BOOL)animated
{
    UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeSystem];
    
    saveButton.layer.cornerRadius = 3.0;
    saveButton.backgroundColor = [UIColor colorWithHexString:@"FFF911"];
    [saveButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(saveAddress:) forControlEvents:UIControlEventTouchUpInside];
    [saveButton setTitle:ALLocalizedString(@"Save", nil) forState:UIControlStateNormal];
    self.saveButton = saveButton;
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, CGRectGetWidth([UIScreen mainScreen].bounds), 60)];
    [footerView addSubview:self.saveButton];
    self.tableView.tableFooterView = footerView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.saveButton.frame = CGRectMake(16.0, 8.0, CGRectGetWidth(self.view.bounds) - 16*2, 44.0);
}

#pragma mark - IBActions

- (IBAction)saveAddress:(id)sender
{
    NSString *primaryAddressID;
    for (NSDictionary *address in self.addresses) {
        if ([address[@"IsPrimary"] boolValue]) {
            primaryAddressID = address[@"AddressID"];
            break;
        }
    }
    
    if (primaryAddressID.length > 0) {
        [self addMBProgressHUDWithKey:@"setPrimaryAddressA202"];
        
        [ALNetWorkAPI setPrimaryAddressA202WithId:primaryAddressID completionBlock:^(NSDictionary *responseObject) {
            
            [self removeMBProgressHUDWithKey:@"setPrimaryAddressA202"];
            
            if ([ALNetWorkAPI checkSerialNumber:@"A202"
                                 ResponseObject:responseObject]) {
                [self.navigationController popViewControllerAnimated:YES];
            }
            else {
                YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
            }
        }];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.addresses.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:addressCellIdentifier];
    
    NSDictionary *addressInfo = [self.addresses objectAtIndex:indexPath.row];    
    
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@\n%@ %@\n%@ %@ %@\n%@\n%@", addressInfo[@"UserFirstName"], addressInfo[@"UserLastName"], addressInfo[@"Street1"], addressInfo[@"Street2"], addressInfo[@"City"], addressInfo[@"State"], addressInfo[@"PostalCode"], addressInfo[@"Country"], addressInfo[@"Phone"]];
    cell.textLabel.font = [UIFont systemFontOfSize:14.0];
    cell.tintColor = [UIColor colorWithHexString:@"007AFF"];
    
    if ([addressInfo[@"IsPrimary"] boolValue]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        cell.textLabel.textColor = [UIColor colorWithHexString:@"007AFF"];
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.textLabel.textColor = [UIColor blackColor];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *addressInfo = [self.addresses objectAtIndex:indexPath.row];
    
    NSString *address = [NSString stringWithFormat:@"%@ %@\n%@ %@\n%@ %@ %@\n%@\n%@", addressInfo[@"UserFirstName"], addressInfo[@"UserLastName"], addressInfo[@"Street1"], addressInfo[@"Street2"], addressInfo[@"City"], addressInfo[@"State"], addressInfo[@"PostalCode"], addressInfo[@"Country"], addressInfo[@"Phone"]];
    
    UIFont *cellFont = [UIFont systemFontOfSize:14.0];
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:address attributes:@{NSFontAttributeName: cellFont}];
    CGRect rect = [attributedText boundingRectWithSize:CGSizeMake(tableView.bounds.size.width, CGFLOAT_MAX)options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    return CGRectGetHeight(rect) + 8.0;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSMutableArray *mutableAddresses = [NSMutableArray array];
    for (int i=0; i<self.addresses.count; i++) {
        NSMutableDictionary *mutableAddress = [[self.addresses objectAtIndex:i] mutableCopy];
        if (i == indexPath.row) {
            [mutableAddress setObject:@(YES) forKey:@"IsPrimary"];
        }
        else {
            [mutableAddress setObject:@(NO) forKey:@"IsPrimary"];
        }
        [mutableAddresses addObject:[mutableAddress copy]];
    }
    self.addresses = [mutableAddresses copy];
    
    [tableView reloadData];
}

@end
