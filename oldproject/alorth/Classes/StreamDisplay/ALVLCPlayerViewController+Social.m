//
//  ALVLCPlayerViewController+Social.m
//  alorth
//
//  Created by w91379137 on 2015/11/16.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALVLCPlayerViewController+Social.h"

@implementation ALVLCPlayerViewController (Social)

#pragma mark - IBAction Share
- (IBAction)showShareButtonView:(id)sender
{
    
    self.smallBuyView.hidden = YES;
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.mainScrollView.alpha = 0.5;
                         self.shareButtonView.layer.transform =
                         CATransform3DTranslate(CATransform3DIdentity, 0, -self.shareButtonView.frame.size.height, 0);
                     } completion:nil];
}

- (IBAction)hiddenShareButtonView:(id)sender
{
    self.smallBuyView.hidden = NO;
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.mainScrollView.alpha = 1;
                         self.shareButtonView.layer.transform = CATransform3DIdentity;
                     } completion:nil];
}

-(IBAction)followAction:(id)sender
{
    NSString *willChangeToStatus = [self.followStatus intValue] == 0 ? @"1" : @"0";
    
    self.followActionButton.enabled = NO;
    [ALNetWorkAPI changeFollowStatusA040UserID:self.infoDict[@"StreamerUserID"]
                                  FollowStatus:willChangeToStatus
                               CompletionBlock:^(NSDictionary *responseObject) {
                                   self.followActionButton.enabled = YES;
                                   
                                   if ([ALNetWorkAPI checkSerialNumber:@"A040"
                                                        ResponseObject:responseObject]) {
                                       self.followStatus = willChangeToStatus;
                                   }
                                   else {
                                       ALLog(@"%@",responseObject);
                                   }
                               }];
}

- (IBAction)shareAction:(id)sender
{
    [self hiddenShareButtonView:nil];
    
    NSString *shortURL = [self productInfoDictOfContentInfo:self.currentVideoClip].safe[@"ShortURL"];
    if (shortURL) {
        UIActivityViewController *avc =
        [[UIActivityViewController alloc] initWithActivityItems:[NSArray arrayWithObjects:shortURL, nil] applicationActivities:nil];
        [[PDSEnvironmentViewController sharedInstance] presentViewController:avc animated:YES completion:nil];
    }
}

- (IBAction)copyURLAction:(id)sender
{
    [self hiddenShareButtonView:nil];
    
    NSString *shortURL = [self productInfoDictOfContentInfo:self.currentVideoClip].safe[@"ShortURL"];
    if (shortURL) {
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = shortURL;
        
        UIAlertView *alert =
        [[UIAlertView alloc] initWithTitle:@""
                                   message:shortURL
                                  delegate:nil
                         cancelButtonTitle:ALLocalizedString(@"OK",nil)
                         otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)reportAction:(UIButton *)sender
{
    [self hiddenShareButtonView:nil];
    UIAlertView *alert =
    [[UIAlertView alloc] initWithTitle:@""
                               message:ALLocalizedString(@"Report Sent",nil)
                              delegate:nil
                     cancelButtonTitle:ALLocalizedString(@"OK",nil)
                     otherButtonTitles:nil, nil];
    [alert show];
    
    sender.enabled = NO;
    [ALNetWorkAPI reportStreamA053StreamID:self.infoDict[@"StreamID"]
                           CompletionBlock:^(NSDictionary *responseObject) {
                               sender.enabled = YES;
                               
                               if (![ALNetWorkAPI checkSerialNumber:@"A053"
                                                    ResponseObject:responseObject]) {
                                   ALLog(@"%@",responseObject);
                               }
                           }];
}

#pragma mark - IBAction Person
- (IBAction)scrollToLeft:(id)sender
{
    [self.mainScrollView setContentOffset:CGPointZero animated:YES];
}

-(IBAction)showFollowerList:(id)sender
{
    ALPersonFollowersListViewController *next =
    [[ALPersonFollowersListViewController alloc] init];
    
    next.userID = self.infoDict[@"StreamerUserID"];
    
    [self.navigationController pushViewController:next animated:YES];
}

-(IBAction)showFollowingList:(id)sender
{
    ALPersonFollowingListViewController *next =
    [[ALPersonFollowingListViewController alloc] init];
    
    next.userID = self.infoDict[@"StreamerUserID"];
    
    [self.navigationController pushViewController:next animated:YES];
}

- (IBAction)personalPage:(id)sender
{
    ALPersonIntroductionViewController *vc =
    [[ALPersonIntroductionViewController alloc] initWithUserId:self.infoDict[@"StreamerUserID"]];
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)chatAction:(id)sender
{
    [self chatWithOtherId:self.infoDict[@"StreamerUserID"]];
}

@end
