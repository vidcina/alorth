//
//  ALCheckoutViewController.h
//  alorth
//
//  Created by ZZB on 2016/5/28.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ALCheckoutViewControllerDelegate;

typedef NS_ENUM(NSUInteger, ALCheckoutStatus) {
    ALCheckoutStatusOrderInfo,
    ALCheckoutStatusShippingDetail,
    ALCheckoutStatusPayment
};

@interface ALCheckoutViewController : UIViewController

@property (nonatomic, strong) UIButton *dismissButton;
@property (nonatomic, assign) ALCheckoutStatus currentStatus;
@property (nonatomic, weak) NSObject<ALCheckoutViewControllerDelegate> *delegate;
@property (nonatomic, weak) IBOutlet ALSelectTypeScrollView *typeSelectTypeScrollView;
@property (nonatomic, strong) NSDictionary *productInfoDict;    // All sizes
@property (nonatomic, strong) NSDictionary *fitProductInfoDict; // Current size

// Order Info View
@property (nonatomic, weak) IBOutlet UIImageView *productImageView;
@property (nonatomic, weak) IBOutlet UILabel *productNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *singlePriceLabel;
@property (nonatomic, weak) IBOutlet UILabel *amountNumberHeadLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalPriceNumberHeadLabel;
@property (nonatomic, weak) IBOutlet UILabel *amountBottomLabel;
@property (nonatomic, weak) IBOutlet UILabel *amountDetailBottomLabel;
@property (nonatomic, weak) IBOutlet UILabel *amountNumberBottomLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalPriceBottomLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalPriceDetailBottomLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalPriceNumberBottomLabel;
@property (nonatomic, weak) IBOutlet UIButton *addAmountButton;
@property (nonatomic, weak) IBOutlet UIButton *reduceAmountButton;

// Shipping Detail View
@property (nonatomic, weak) IBOutlet UIView *shippingDetailView;

// Payment View
@property (nonatomic, weak) IBOutlet UIView *paymentView;
@property (nonatomic, weak) IBOutlet UILabel *paymentAddressLabel;
@property (nonatomic, weak) IBOutlet UILabel *paymentAmountValueLabel;
@property (nonatomic, weak) IBOutlet UILabel *paymentSizeValueLabel;
@property (nonatomic, weak) IBOutlet UILabel *paymentTotalWithoutOthersValueLabel;
@property (nonatomic, weak) IBOutlet UILabel *paymentShippingValueLabel;
@property (nonatomic, weak) IBOutlet UILabel *paymentTaxValueLabel;
@property (nonatomic, weak) IBOutlet UILabel *paymentTotalValueLabel;
@property (nonatomic, weak) IBOutlet UIView *actionContainerView;

@end

@protocol ALCheckoutViewControllerDelegate

@optional
- (void)checkoutViewControllerDidTapDismissButton:(ALCheckoutViewController *)checkoutViewController;
- (void)checkoutViewControllerDidTapAddButton:(ALCheckoutViewController *)checkoutViewController;
- (void)checkoutViewControllerDidTapMinusButton:(ALCheckoutViewController *)checkoutViewController;

@end


