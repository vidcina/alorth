//
//  ALCheckoutViewController.m
//  alorth
//
//  Created by ZZB on 2016/5/28.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALCheckoutViewController.h"

#import "ALAddAddressViewController.h"
#import "ALAppDelegate.h"
#import "ALPickPrimaryAddressViewController.h"

static NSString *addressCellIdentifier = @"addressCellIdentifier";
static NSString *addAddressCellIdentifier = @"addNewAddressCellIdentifier";

@interface ALCheckoutViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) IBOutletCollection(UILabel) NSArray *stepLabels;
@property (nonatomic, weak) IBOutlet UITableView *addressTableView;
@property (nonatomic, weak) IBOutlet UIButton *nextButton;
@property (nonatomic, strong) NSArray *addresses;
@property (nonatomic, strong) NSDictionary *primaryAddress;

- (IBAction)nextStep:(id)sender;
- (IBAction)addAmount:(id)sender;
- (IBAction)reduceAmount:(id)sender;
- (IBAction)presentSetPrimaryAddressView:(id)sender;

@end

@implementation ALCheckoutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    for (UILabel *stepLabel in self.stepLabels) {
        stepLabel.layer.borderColor = [UIColor colorWithHexString:@"8B8D8C"].CGColor;
        stepLabel.layer.borderWidth = 6.0;        
        stepLabel.layer.cornerRadius = CGRectGetWidth(stepLabel.frame)/2.0;
    }
    
    [self.addressTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:addressCellIdentifier];
    [self.addressTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:addAddressCellIdentifier];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.title = ALLocalizedString(@"Checkout", nil);
    
    [self.navigationController setNavigationBarHidden:NO];
    if (self.dismissButton) {
        [self.dismissButton removeFromSuperview];
    }
    UIButton *dismissButton = [UIButton buttonWithType:UIButtonTypeSystem];
    dismissButton.backgroundColor = [UIColor colorWithHexString:@"F6EB0B"];
    [dismissButton setImage:[UIImage imageNamed:@"WhiteClose"] forState:UIControlStateNormal];
    dismissButton.tintColor = [UIColor blackColor];
    [dismissButton addTarget:self action:@selector(dismissCheckoutView:) forControlEvents:UIControlEventTouchUpInside];
    self.dismissButton = dismissButton;
    
    [self.navigationController.view addSubview:self.dismissButton];
    [self.dismissButton removeFromSuperview];
    [self.navigationController.navigationBar addSubview:self.dismissButton];
    
    [ALNetWorkAPI fetchExistedAddressA110CompletionBlock:^(NSDictionary *responseObject) {
        if ([ALNetWorkAPI checkSerialNumber:@"A110"
                             ResponseObject:responseObject]) {
            NSDictionary *resBody = responseObject[@"resBody"];
            self.addresses = resBody[@"AddressList"];
            [self.addressTableView reloadData];
        }
    }];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    self.dismissButton.frame = CGRectMake(CGRectGetWidth(self.navigationController.view.frame) - 44.0, 0.0, 44.0, 44.0);
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.dismissButton removeFromSuperview];
    self.title = @"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Publics

- (void)setProductInfoDict:(NSDictionary *)productInfoDict
{
    _productInfoDict = productInfoDict;
}

#pragma mark - Privates

- (void)setCurrentStatus:(ALCheckoutStatus)currentStatus
{
    UIColor *deselectTextColor = [(UILabel *)self.stepLabels[currentStatus] textColor];
    UIColor *deselectBackgroundColor = [(UILabel *)self.stepLabels[currentStatus] backgroundColor];
    
    [(UILabel *)self.stepLabels[currentStatus] setTextColor:[(UILabel *)self.stepLabels[_currentStatus] textColor]];
    [(UILabel *)self.stepLabels[currentStatus] setBackgroundColor:[(UILabel *)self.stepLabels[_currentStatus] backgroundColor]];

    [(UILabel *)self.stepLabels[_currentStatus] setTextColor:deselectTextColor];
    [(UILabel *)self.stepLabels[_currentStatus] setBackgroundColor:deselectBackgroundColor];
    
    switch (currentStatus) {
        case ALCheckoutStatusOrderInfo:
            self.shippingDetailView.hidden = YES;
            self.paymentView.hidden = YES;
            self.actionContainerView.hidden = YES;
            self.nextButton.hidden = NO;
            break;
        case ALCheckoutStatusShippingDetail:
            self.shippingDetailView.hidden = NO;
            self.paymentView.hidden = YES;
            self.actionContainerView.hidden = YES;
            self.nextButton.hidden = NO;
            break;
        case ALCheckoutStatusPayment:
            self.shippingDetailView.hidden = YES;
            self.paymentView.hidden = NO;
            self.actionContainerView.hidden = NO;
            self.nextButton.hidden = YES;
            break;
        default:
            break;
    }
    
    _currentStatus = currentStatus;
}

#pragma mark - IBActions

- (IBAction)nextStep:(id)sender
{
    if (self.currentStatus == ALCheckoutStatusPayment) {
        return;
    }
    else if (self.currentStatus == ALCheckoutStatusShippingDetail) {
        if (self.primaryAddress) {
            self.paymentAddressLabel.text = [NSString stringWithFormat:@"%@ %@\n%@ %@\n%@ %@ %@\n%@\n%@", self.primaryAddress[@"UserFirstName"], self.primaryAddress[@"UserLastName"], self.primaryAddress[@"Street1"], self.primaryAddress[@"Street2"], self.primaryAddress[@"City"], self.primaryAddress[@"State"], self.primaryAddress[@"PostalCode"], self.primaryAddress[@"Country"], self.primaryAddress[@"Phone"]];
            
            [[[ALAppDelegate sharedAppDelegate] userInfomation] setObject:self.primaryAddress[@"FullAddress"] forKey:@"Address"];
            __block float shippingFee = 0.0;
            
            [self addMBProgressHUDWithKey:@"ShipFeeA112"];
            [ALNetWorkAPI fetchShipFeeA112CompletionBlock:^(NSDictionary *responseObject) {
                [self removeMBProgressHUDWithKey:@"ShipFeeA112"];
                if ([ALNetWorkAPI checkSerialNumber:@"A112"
                                     ResponseObject:responseObject]) {
                    
                    NSDictionary *resBody = [responseObject objectForKey:@"resBody"];
                    if ([resBody[@"DomesticShipping"] boolValue] || [resBody[@"InternationalShipping"] boolValue]) {
                        // TODO: Has shipping fee
                        // shippingFee =?
                    }
                    self.paymentShippingValueLabel.text = [NSString stringWithFormat:@"%.2f %@", shippingFee, self.fitProductInfoDict[@"Currency"]];
                    
                    __block float taxFee = 0.0;
                    [self addMBProgressHUDWithKey:@"TaxA114"];
                    [ALNetWorkAPI fetchTaxA114CompletionBlock: ^(NSDictionary *responseObject) {
                        [self removeMBProgressHUDWithKey:@"TaxA114"];
                        if ([ALNetWorkAPI checkSerialNumber:@"A114"
                                             ResponseObject:responseObject]) {
                            NSDictionary *resBody = [responseObject objectForKey:@"resBody"];
                            if (resBody) {
                                if ([resBody[@"AutoTaxSetting"] boolValue]) {
                                    if (resBody[@"UsaTax"]) {
                                        // TODO
                                    }
                                    else if (resBody[@"EuTax"]) {
                                        // TODO
                                    }
                                }
                            }
                            
                            self.paymentTaxValueLabel.text = [NSString stringWithFormat:@"%.2f %@", taxFee, self.fitProductInfoDict[@"Currency"]];
                            self.paymentTotalValueLabel.text = [NSString stringWithFormat:@"%.2f %@", ([self.paymentTotalWithoutOthersValueLabel.text floatValue] + shippingFee + taxFee), self.fitProductInfoDict[@"Currency"]];
                            
                            self.currentStatus += 1;
                        }
                        else {
                            YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A114", responseObject),nil));
                        }
                    }];
                }
                else {
                    YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A112", responseObject),nil));
                }
            }];
        }
        else {
            YKSimpleAlert(ALLocalizedString(@"You need to setup the primary address before next step",nil));
            return;
        }
    }
    else {
        self.currentStatus += 1;
    }
}

- (IBAction)dismissCheckoutView:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(checkoutViewControllerDidTapDismissButton:)]) {
        [self.delegate checkoutViewControllerDidTapDismissButton:self];
    }
}

- (IBAction)addAmount:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(checkoutViewControllerDidTapAddButton:)]) {
        [self.delegate checkoutViewControllerDidTapAddButton:self];
    }
}

- (IBAction)reduceAmount:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(checkoutViewControllerDidTapMinusButton:)]) {
        [self.delegate checkoutViewControllerDidTapMinusButton:self];
    }
}

- (IBAction)presentSetPrimaryAddressView:(id)sender
{
    ALPickPrimaryAddressViewController *pickPrimaryAddressViewController = [[ALPickPrimaryAddressViewController alloc] initWithNibName:@"ALPickPrimaryAddressViewController" bundle:nil];
    pickPrimaryAddressViewController.addresses = self.addresses;
    [self.navigationController pushViewController:pickPrimaryAddressViewController animated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.addresses.count > 0) {
        if (!tableView.tableHeaderView) {
            UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, CGRectGetWidth(tableView.frame), 52.0)];
            headView.backgroundColor = [UIColor colorWithHexString:@"D0D1D1"];
            UILabel *primaryLabel = [[UILabel alloc] initWithFrame:CGRectMake(16.0, 4.0, CGRectGetWidth(tableView.frame) - 16*2, 44.0)];
            primaryLabel.text = ALLocalizedString(@"Primary Shipping Address", nil);
            primaryLabel.font = [UIFont systemFontOfSize:17.0];
            
            UIButton *editButton = [UIButton buttonWithType:UIButtonTypeSystem];
            [editButton setTitle:ALLocalizedString(@"Edit", nil) forState:UIControlStateNormal];
            editButton.frame = CGRectMake(CGRectGetWidth(tableView.frame) - 16.0 - 44.0, 4, 44.0, 44.0);
            [editButton addTarget:self action:@selector(presentSetPrimaryAddressView:) forControlEvents:UIControlEventTouchUpInside];
            
            [headView addSubview:primaryLabel];
            [headView addSubview:editButton];
            
            tableView.tableHeaderView = headView;
        }
        for (NSDictionary *addressInfo in self.addresses) {
            if ([addressInfo[@"IsPrimary"] boolValue]) {
                self.primaryAddress = addressInfo;
                return 2;
            }
        }
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.primaryAddress && indexPath.section == 0) {
        
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:addressCellIdentifier];
        
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.text = [NSString stringWithFormat:@"%@ %@\n%@ %@\n%@ %@ %@\n%@\n%@", self.primaryAddress[@"UserFirstName"], self.primaryAddress[@"UserLastName"], self.primaryAddress[@"Street1"], self.primaryAddress[@"Street2"], self.primaryAddress[@"City"], self.primaryAddress[@"State"], self.primaryAddress[@"PostalCode"], self.primaryAddress[@"Country"], self.primaryAddress[@"Phone"]];
        cell.textLabel.font = [UIFont systemFontOfSize:14.0];
        cell.textLabel.textColor = [UIColor colorWithHexString:@"007AFF"];
        cell.tintColor = [UIColor colorWithHexString:@"007AFF"];
        
        return cell;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:addAddressCellIdentifier forIndexPath:indexPath];
    cell.textLabel.text = ALLocalizedString(@"Enter a New Address", nil);
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.primaryAddress && indexPath.section == 0) {
        NSString *address = [NSString stringWithFormat:@"%@ %@\n%@ %@\n%@ %@ %@\n%@\n%@", self.primaryAddress[@"UserFirstName"], self.primaryAddress[@"UserLastName"], self.primaryAddress[@"Street1"], self.primaryAddress[@"Street2"], self.primaryAddress[@"City"], self.primaryAddress[@"State"], self.primaryAddress[@"PostalCode"], self.primaryAddress[@"Country"], self.primaryAddress[@"Phone"]];
        
        UIFont *cellFont = [UIFont systemFontOfSize:14.0];
        NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:address attributes:@{NSFontAttributeName: cellFont}];
        CGRect rect = [attributedText boundingRectWithSize:CGSizeMake(tableView.bounds.size.width, CGFLOAT_MAX)options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        
        return CGRectGetHeight(rect) + 8.0;
    }
    return 52.0;
}

#pragma mark - UITableViewDelegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1
        || self.primaryAddress == nil) {
        ALAddAddressViewController *addAddressViewController = [[ALAddAddressViewController alloc] initWithNibName:@"ALAddAddressViewController" bundle:nil];
        addAddressViewController.infoDict = [[ALAppDelegate sharedAppDelegate] userInfomation];
        
        [self.navigationController pushViewController:addAddressViewController animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1
        || self.primaryAddress == nil) {
        return 52.0;
    }
    return 0;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 1
        || self.primaryAddress == nil) {
        UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, CGRectGetWidth(tableView.frame), 52.0)];
        headView.backgroundColor = [UIColor colorWithHexString:@"D0D1D1"];
        
        UILabel *addAddressyLabel = [[UILabel alloc] initWithFrame:CGRectMake(16.0, 4.0, CGRectGetWidth(tableView.frame) - 16*2, 44.0)];
        addAddressyLabel.text = ALLocalizedString(@"Add Another Address", nil);
        addAddressyLabel.font = [UIFont systemFontOfSize:17.0];
        [headView addSubview:addAddressyLabel];
        
        return headView;
    }
    return nil;
}

@end
