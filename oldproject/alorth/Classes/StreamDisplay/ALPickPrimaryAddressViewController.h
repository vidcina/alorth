//
//  ALPickPrimaryAddressViewController.h
//  alorth
//
//  Created by ZZB on 2016/5/30.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ALPickPrimaryAddressViewController : UITableViewController

@property (nonatomic, strong) NSArray *addresses;

@end
