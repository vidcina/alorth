//
//  PDSAccountManager.h
//  PDSLeacloudDemo
//
//  Created by w91379137 on 2015/6/30.
//  Copyright (c) 2015年 w91379137. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <AVOSCloudIM/AVOSCloudIM.h>
#import "AVIMConversation_helper.h"

#import "ALStripeInfo.h"

// testing account:
// Jones  123456
// jonesking alorth

// john             123456 Credit Stripe @5s 8.1
// w913791379137    123456 Credit Stripe @5s 9.2
// w91379137        123456
// w913791379       123456 @5s 8.1

static NSString *kPDSAccountManagerUserDidLoad = @"PDSAccountManagerUserDidLoad";
static NSString *kPDSIMDidDisconnect = @"kPDSIMDidDisconnect";
static NSString *kPDSIMDidReconnect = @"kPDSIMDidReconnect";
static NSString *kPDSIMDidReceiveIM = @"kPDSIMDidReceiveIM";
static NSString *kPDSIMDidLoadConversationList = @"kPDSIMDidLoadConversationList";

@interface PDSAccountManager : NSObject <AVIMClientDelegate>
{
    BOOL isFailLoginOneTime;
}

#pragma mark - sharedInstance
+ (instancetype)sharedManager;

#pragma mark - Account/User Infomation
+ (NSString *)userName;
+ (NSString *)password;
+ (NSString *)userToken;
+ (NSString *)authTime;
+ (BOOL)isThirdPartyLogin;
@property(nonatomic, strong) NSString *userLanguage;

#pragma mark - Account/User Stripe
@property (nonatomic, strong) ALStripeInfo *stripeInfo;

#pragma mark - Account/User Login/Logout
+ (void)loginAccount:(NSString *)account
            Password:(NSString *)password
     CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;
+ (void)loginWithSocialAccount:(NSDictionary *)accountInfo country:(NSString *)countryCode completionBlock:(void (^)(NSDictionary *))completionBlock;

+ (BOOL)isLoggedIn;

- (void)logout;

#pragma mark - LeanCloud
@property (nonatomic, strong) AVIMClient *imClient;
@property (nonatomic, strong) NSString *imClientID;
@property (nonatomic, strong) NSMutableArray *conversationArray;

#pragma mark - qcloud
@property (nonatomic, strong) NSMutableArray *groupList;

@property (nonatomic, copy, readonly) NSDictionary *profileDictionary __deprecated_msg("Use [[ALAppDelegate sharedAppDelegate] userInfomation] instead");
@end
