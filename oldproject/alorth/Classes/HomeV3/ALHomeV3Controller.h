//
//  ALHomeViewController.h
//  alorth
//
//  Created by w91379137 on 2015/5/25.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicMainViewController.h"
#import "ALFilter.h"
#import "ALFilterViewController.h"
#import "ALStreamProductListViewController.h"
#import "ALStreamProductListViewController+PullMore.h"

@interface ALHomeV3Controller : ALBasicMainViewController
<UISearchBarDelegate,
ALFilterViewControllerDelegate,
ALStreamProductListViewControllerDelegate>

@property (nonatomic, strong) IBOutlet UISearchBar *topSearchBar;  //搜尋條

@property (nonatomic, strong) IBOutlet UIView *mapContainerView;
@property (nonatomic, strong) IBOutlet UIView *streamProductContainerView;

//@property (nonatomic) ALFilterTypeMode typeMode;
//@property (nonatomic) ALFilterSortMode sortMode;

@property (nonatomic, strong) NSMutableArray *languages;
@property (nonatomic, strong) NSMutableArray *categories;
@property (nonatomic, strong) NSMutableArray *locations;

@end