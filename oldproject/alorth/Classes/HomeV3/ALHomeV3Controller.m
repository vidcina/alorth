//
//  ALHomeViewController.m
//  alorth
//
//  Created by w91379137 on 2015/5/25.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALHomeV3Controller.h"

#import "ALDiscoverListViewController.h"
#import "ALStreamProductListViewController.h"

#import "ALSearchV3ViewController.h"

@interface ALHomeV3Controller ()
{
    ALDiscoverListViewController *discoverListVC;
    ALStreamProductListViewController *complexStreamProductVC;
}

@end

@implementation ALHomeV3Controller

#pragma mark - Init
- (void)viewDidLoad
{
    [super viewDidLoad];
    //self.typeMode = ALFilterTypeModeStreaming;
    //self.sortMode = 3;
    
    if (!discoverListVC) {
        discoverListVC = [[ALDiscoverListViewController alloc] init];
        [self addChildViewController:discoverListVC];
        [discoverListVC addSafeObserver:self
                             forKeyPath:@"discoverStatus"
                                options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew
                                context:NULL];
    }
    
    if (!complexStreamProductVC) {
        complexStreamProductVC = [[ALStreamProductListViewController alloc] init];
        [self addChildViewController:complexStreamProductVC];
        complexStreamProductVC.delegate = self;
        // 20160323 Jones: 預設想要小格狀顯示
        complexStreamProductVC.sizeType = ALComplexSPVCSize2;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (discoverListVC.view.superview == nil) {
        [self.mapContainerView addSubview:discoverListVC.view];
        [discoverListVC.view mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(discoverListVC.view.superview);
        }];
    }
    [self adjustContainerView:NO];
    
    if (complexStreamProductVC.view.superview == nil) {
        [self.streamProductContainerView addSubview:complexStreamProductVC.view];
        [complexStreamProductVC.view mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(complexStreamProductVC.view.superview);
        }];
    }
    
    if (!self.languages) [self selectAllSection:ALFilterSectionLanguage IsSelect:YES];
    if (!self.categories) [self selectAllSection:ALFilterSectionCategory IsSelect:YES];
    if (!self.locations) [self selectAllSection:ALFilterSectionLocation IsSelect:YES];
    //ALLog(@"全選%@,%@,%@",self.languages,self.categories,self.locations);
    
    if (complexStreamProductVC.objectListArray.count == 0) {
        [complexStreamProductVC checkNewData];
    }
}

#pragma mark - Constraint
- (void)adjustContainerView:(BOOL)animated
{
    void (^completion)(void) = ^{
        self.mapContainerView.alpha = (discoverListVC.discoverStatus == DiscoverMap) ? 1 : 0;
    };
    
    if (animated) {
        [UIView animateKeyframesWithDuration:0.1
                                       delay:0.0
                                     options:UIViewKeyframeAnimationOptionBeginFromCurrentState
                                  animations:^{
                                      completion();
                                  } completion:^(BOOL finished) {}];
    }
    else {
        completion();
    }
    
    if (discoverListVC.discoverStatus == DiscoverMap &&
        self.topSearchBar.isFirstResponder) {
        [self.topSearchBar resignFirstResponder];
    }
}

#pragma mark - Observer
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if (object == discoverListVC) {
        if ([change[@"old"] integerValue] != 0) {
            [self adjustContainerView:YES];
        }
    }
    else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

#pragma mark - UISearchBarDelegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    if (searchBar.inputAccessoryView == nil) {
        searchBar.inputAccessoryView = [searchBar keyboardReturnToolbar];
    }
    return YES;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = NO;
    [searchBar resignFirstResponder];
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = YES;
    editFirstResponder = searchBar;
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = NO;
    //[self startSearch];
    
    NSString *keyword = [self keyword];
    if (keyword.length > 0) {
        ALSearchV3ViewController *vc = [[ALSearchV3ViewController alloc] init];
        vc.keyword = keyword;
        [self.navigationController pushViewController:vc animated:YES];
        
        self.topSearchBar.text = @"";
    }
}

#pragma mark - IBAction
- (IBAction)mapAction:(id)sender
{
    [discoverListVC.selectScrollView selectIndex:1];
}

- (IBAction)cartAction:(id)sender
{
    ALNavigationViewController *nv = (ALNavigationViewController *)self.navigationController;
    [nv clearOtherController:ALNavigationSubClear
                        push:ALNavigationTabBarCart
               subController:nil];
}

- (IBAction)sizeAction:(UIButton *)sender
{
    switch (complexStreamProductVC.sizeType) {
        case ALComplexSPVCSize1: complexStreamProductVC.sizeType = ALComplexSPVCSize2; break;
        case ALComplexSPVCSize2: complexStreamProductVC.sizeType = ALComplexSPVCSize1; break;
        default: break;
    }
    
    switch (complexStreamProductVC.sizeType) {
        case ALComplexSPVCSize1:
            [sender setImage:[UIImage imageNamed:@"home_collection_view.png"] forState:UIControlStateNormal]; break;
        case ALComplexSPVCSize2: [sender setImage:[UIImage imageNamed:@"home_list_view.png"] forState:UIControlStateNormal]; break;
        default: break;
    }
}

- (IBAction)filterAction:(id)sender
{
    [self keyboardReturn];
    ALFilterViewController *vc = [[ALFilterViewController alloc] init];
    vc.delegate = self;
    [[PDSEnvironmentViewController sharedInstance] submitViewController:vc
                                                        CompletionBlock:^{
                                                            [self startSearch];
    }];
}

- (void)startSearch
{
    // Array 部分 先排序好 跟歷史紀錄 從頭到尾 逐一比對 若不相同就 重新讀取
    if (![complexStreamProductVC isSameParameters]) {
        [complexStreamProductVC checkNewData];
    }
}

#pragma mark - ALStreamProductListViewControllerDelegate
- (NSInteger)typeMode
{
    return ALFilterTypeModeStreaming;
}

- (NSInteger)sortMode
{
    return 3;//首頁特殊排序
}

- (NSString *)keyword
{
    return self.topSearchBar.text;
}

#pragma mark - ALFilterViewControllerDelegate
- (BOOL)isEnabledSection:(ALFilterSection)section
{
    if (section == ALFilterSectionType) return NO;
    if (section == ALFilterSectionSort) return NO;
    
    if (self.typeMode == ALFilterTypeModeProduct) {
        if (section == ALFilterSectionLanguage ||
            section == ALFilterSectionLocation) {
            return NO;
        }
    }
    return YES;
}

- (void)selectAllSection:(ALFilterSection)section IsSelect:(BOOL)isSelect
{
    if (section == ALFilterSectionLanguage) {
        if (isSelect) {
            self.languages = [[ALBundleTextResources.languageArray valueForKey:@"LanguageIndex"] mutableCopy];
        }
        else {
            self.languages = [@[] mutableCopy];
        }
    }
    else if (section == ALFilterSectionCategory) {
        if (isSelect) {
            self.categories = [[ALBundleTextResources.categoryArray valueForKey:@"CategoryIndex"] mutableCopy];
        }
        else {
            self.categories = [@[] mutableCopy];
        }
    }
    else if (section == ALFilterSectionLocation) {
        if (isSelect) {
            self.locations = [[ALBundleTextResources.locationArray valueForKey:@"LocationIndex"] mutableCopy];
        }
        else {
            self.locations = [@[] mutableCopy];
        }
    }
}

- (BOOL)isAllSelect:(ALFilterSection)section
{
    if (section == ALFilterSectionLanguage) {
        return self.languages.count == ALBundleTextResources.languageArray.count;
    }
    else if (section == ALFilterSectionCategory) {
        return self.categories.count == ALBundleTextResources.categoryArray.count;
    }
    else if (section == ALFilterSectionLocation) {
        return self.locations.count == ALBundleTextResources.locationArray.count;
    }
    return NO;
}

- (BOOL)isSelectOfSection:(ALFilterSection)section
                    Index:(NSInteger)index
                KeyString:(NSString *)keyString;
{
    switch ((ALFilterSection)section) {
        case ALFilterSectionType: return self.typeMode == index;
        case ALFilterSectionSort: return self.sortMode == index;
        case ALFilterSectionLanguage: return [self.languages containsObject:keyString];
        case ALFilterSectionCategory: return [self.categories containsObject:keyString];
        case ALFilterSectionLocation: return [self.locations containsObject:keyString];
        default:break;
    }
    return NO;
}

- (void)clickOfSection:(ALFilterSection)section
                 Index:(NSInteger)index
             KeyString:(NSString *)keyString
{
    switch ((ALFilterSection)section) {
        //case ALFilterSectionType: self.typeMode = index; break;
        //case ALFilterSectionSort: self.sortMode = index; break;
        case ALFilterSectionLanguage: {
            if ([self.languages containsObject:keyString]) {
                [self.languages removeObject:keyString];
            }
            else {
                [self.languages addObject:keyString];
            }
        }break;
        case ALFilterSectionCategory: {
            if ([self.categories containsObject:keyString]) {
                [self.categories removeObject:keyString];
            }
            else {
                [self.categories addObject:keyString];
            }
        }break;
        case ALFilterSectionLocation: {
            if ([self.locations containsObject:keyString]) {
                [self.locations removeObject:keyString];
            }
            else {
                [self.locations addObject:keyString];
            }
        }break;
        default:break;
    }
}

@end