//
//  PDSAccountManager+LeanCloud.h
//  alorth
//
//  Created by w91379137 on 2016/1/5.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "PDSAccountManager.h"

@interface PDSAccountManager (LeanCloud)

- (void)loginLeanCloud;
- (void)logoutLeanCloud;

@end
