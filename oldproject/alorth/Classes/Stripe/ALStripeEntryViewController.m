//
//  ALStripeEntryViewController.m
//  alorth
//
//  Created by John Hsu on 2015/10/30.
//  Copyright (c) 2015年 w91379137. All rights reserved.
//
#import "ALStripeEntryViewController.h"
#import <ApplePayStubs/STPTestPaymentAuthorizationViewController.h>

@interface ALStripeEntryViewController ()

@end

@implementation ALStripeEntryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    scrollView.contentSize = CGSizeMake(320, 568);
    scrollView.contentInset = UIEdgeInsetsMake(0, 0, 200, 0);

    // Do any additional setup after loading the view from its nib.
}

-(IBAction)applePayAction:(id)sender
{
    PKPaymentRequest *request = [Stripe paymentRequestWithMerchantIdentifier:@"merchant.com.miros.alorth"];
    // Configure your request here.
    NSString *label = [NSString stringWithFormat:@"[Alorth]%@-%@",stockNameField.text,productNameField.text];
    NSDecimalNumber *amount = [NSDecimalNumber decimalNumberWithString:amountField.text];
    request.paymentSummaryItems = @[
                                    [PKPaymentSummaryItem summaryItemWithLabel:label
                                                                        amount:amount]
                                    ];
    
    if ([Stripe canSubmitPaymentRequest:request]) {
#if TARGET_IPHONE_SIMULATOR
        STPTestPaymentAuthorizationViewController *paymentController = [[STPTestPaymentAuthorizationViewController alloc] initWithPaymentRequest:request];
#else
        PKPaymentAuthorizationViewController *paymentController = [[PKPaymentAuthorizationViewController alloc]
                                                                   initWithPaymentRequest:request];
#endif
        if (paymentController) {
            paymentController.delegate = self;
            [self presentViewController:paymentController animated:YES completion:nil];

        }
        else {
            [[[UIAlertView alloc] initWithTitle:@"錯誤"
                                        message:@"無法使用Apple Pay"
                                       delegate:nil
                              cancelButtonTitle:@"關閉"
                              otherButtonTitles:nil] show];
            // Show the user your own credit card form (see options 2 or 3)
        }
    }
    else {
        [[[UIAlertView alloc] initWithTitle:@"錯誤"
                                    message:@"無法使用Apple Pay"
                                   delegate:nil
                          cancelButtonTitle:@"關閉"
                          otherButtonTitles:nil] show];
        // Show the user your own credit card form (see options 2 or 3)
    }
}

- (void)paymentAuthorizationViewController:(PKPaymentAuthorizationViewController *)controller
                       didAuthorizePayment:(PKPayment *)payment
                                completion:(void (^)(PKPaymentAuthorizationStatus))completion {
    /*
     We'll implement this method below in 'Creating a single-use token'.
     Note that we've also been given a block that takes a
     PKPaymentAuthorizationStatus. We'll call this function with either
     PKPaymentAuthorizationStatusSuccess or PKPaymentAuthorizationStatusFailure
     after all of our asynchronous code is finished executing. This is how the
     PKPaymentAuthorizationViewController knows when and how to update its UI.
     */
    [self handlePaymentAuthorizationWithPayment:payment completion:completion];
}

- (void)handlePaymentAuthorizationWithPayment:(PKPayment *)payment
                                   completion:(void (^)(PKPaymentAuthorizationStatus))completion {
    [[STPAPIClient sharedClient] createTokenWithPayment:payment
                                             completion:^(STPToken *token, NSError *error) {
                                                 if (error) {
                                                     completion(PKPaymentAuthorizationStatusFailure);
                                                     return;
                                                 }
                                                 /*
                                                  We'll implement this below in "Sending the token to your server".
                                                  Notice that we're passing the completion block through.
                                                  See the above comment in didAuthorizePayment to learn why.
                                                  */
                                                 [self createBackendChargeWithToken:token completion:completion];
                                             }];
}

- (void)paymentAuthorizationViewControllerDidFinish:(PKPaymentAuthorizationViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
- (void)paymentCardTextFieldDidChange:(STPPaymentCardTextField *)textField {
    {
        // Toggle navigation, for example
        if (textField.isValid) {
            NSLog(@"card is valid");
        }
        
    }
}

-(IBAction)payAction:(id)sender
{
    if (!cardTextField.isValid) {
        [[[UIAlertView alloc] initWithTitle:@"錯誤" message:@"卡號有錯誤" delegate:nil cancelButtonTitle:@"關閉" otherButtonTitles:nil] show];
        return;
    }
    STPCardParams *card = [[STPCardParams alloc] init];
    card.number = cardTextField.card.number;
    card.expMonth = cardTextField.card.expMonth;
    card.expYear = cardTextField.card.expYear;
    card.cvc = cardTextField.card.cvc;
    [[STPAPIClient sharedClient] createTokenWithCard:card
                                          completion:^(STPToken *token, NSError *error) {
                                              if (error) {
                                                  [[[UIAlertView alloc] initWithTitle:@"錯誤" message:@"付款失敗" delegate:nil cancelButtonTitle:@"關閉" otherButtonTitles:nil] show];
                                              } else {
                                                  [self createBackendChargeWithToken:token completion:nil];
                                              }
                                          }];
}

#pragma mark -
// completion只用於Apple Pay時
- (void)createBackendChargeWithToken:(STPToken *)token
                          completion:(void (^)(PKPaymentAuthorizationStatus))completion
{
    NSURL *url = [NSURL URLWithString:@"https://example.com/token"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"POST";
    NSString *body     = [NSString stringWithFormat:@"stripeToken=%@", token.tokenId];
    request.HTTPBody   = [body dataUsingEncoding:NSUTF8StringEncoding];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data,
                                               NSError *error) {
                               if (error) {
                                   if (completion) {
                                       completion(PKPaymentAuthorizationStatusFailure);
                                   }
                               } else {
                                   if (completion) {
                                       completion(PKPaymentAuthorizationStatusSuccess);
                                   }
                               }
                           }];
}

@end
