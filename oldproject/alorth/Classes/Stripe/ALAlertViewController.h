//
//  ALAlertViewController.h
//  alorth
//
//  Created by w91379137 on 2015/12/22.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import <UIKit/UIKit.h>
#define ALAlertDuration 0.15f

typedef NS_ENUM(NSInteger, ALAlertViewType) {
    ALAlertViewTypeCenter,
    ALAlertViewTypeRight,
    ALAlertViewTypeBottom,
};

@interface ALAlertViewController : UIViewController
{
    //背景的取消按鈕
    UIButton *backButton;
    
    //中央控制項
    UIView *theCenterView;  //現在要至於中央畫面
    UIView *lastCenterView; //即將移除的中央畫面
}

@property(nonatomic) ALAlertViewType type;

#pragma mark - Keyboard Auto System
- (void)keyboardWillChange:(NSNotification *)notification;

- (void)sendViewToCenter:(UIView *)view;
- (void)showBackButton:(void (^)())completion;
- (IBAction)backAction:(id)sender;//overwrite

@end
