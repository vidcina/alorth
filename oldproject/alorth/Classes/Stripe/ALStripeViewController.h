//
//  ALStripeViewController.h
//  alorth
//
//  Created by w91379137 on 2015/12/16.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALAlertViewController.h"

//目前全部能輸入的種類
typedef NS_ENUM(NSInteger, ALStripeViewQuestion) {
    QuestionCamera,
    QuestionMircrophone,
    QuestionLocation,
    QuestionStripe
};

#define ALBroadcastMainTitleString @"Start broadcast"
#define ALBroadcastSubTitleString @"We need a few thing before you can broadcast"

#define ALStripeMainTitleString @"Start a Earn"
#define ALStripeSubTitleString @"We need a few thing before you can earn"

@interface ALStripeViewController : ALAlertViewController
{
    //問題框
    IBOutlet UILabel *mainTitleLabel;
    IBOutlet UILabel *subTitleLabel;
    IBOutlet UIView *baseContainerView;
}

+ (BOOL)checkQuestionTypeArray:(NSArray *)questionTypeArray;

@property (nonatomic, strong) NSString *mainTitleString;
@property (nonatomic, strong) NSString *subTitleString;

@property (nonatomic, strong) NSArray *questionTypeArray; //問題總表

@end
