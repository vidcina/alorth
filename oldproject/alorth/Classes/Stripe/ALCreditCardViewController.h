//
//  ALCreditCardViewController.h
//  alorth
//
//  Created by w91379137 on 2015/12/21.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALAlertViewController.h"
#import <Stripe/Stripe.h>

@protocol ALCreditCardViewControllerDelegate;
@interface ALCreditCardViewController : ALAlertViewController
{
    IBOutlet UIView *baseView;
    IBOutlet STPPaymentCardTextField *cardTextField;
    
    NSTimer *checkTimer;
}

@property(nonatomic, weak) id<ALCreditCardViewControllerDelegate> delegate;

@end

#pragma mark - delegate
@protocol ALCreditCardViewControllerDelegate <NSObject>

- (void)doneAction:(STPPaymentCardTextField *)cardTextField;

@end