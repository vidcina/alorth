//
//  ALCreditCardViewController.m
//  alorth
//
//  Created by w91379137 on 2015/12/21.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALCreditCardViewController.h"

@interface ALCreditCardViewController ()

@end

@implementation ALCreditCardViewController

#pragma mark - VC Life
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.type = ALAlertViewTypeBottom;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [baseView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@([UIScreen mainScreen].bounds.size.width));
    }];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (backButton.enabled == NO) {
        [UIView animateWithDuration:ALAlertDuration
                              delay:0.0
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             backButton.alpha = 1;
                         } completion:^(BOOL finished) {
                             backButton.enabled = YES;
                             [self sendViewToCenter:baseView];
                         }];
    }
    
    if (!checkTimer) {
        checkTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                       target:self
                                                     selector:@selector(timerEvent:)
                                                     userInfo:nil
                                                      repeats:true];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    if (checkTimer) {
        [checkTimer invalidate];
        checkTimer = nil;
    }
    
    [super viewWillDisappear:animated];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark -
- (void)timerEvent:(NSTimer *)sender
{
    if (cardTextField.cvc.length == 3) {
        [self doneAction:nil];
    }
}

- (IBAction)doneAction:(id)sender
{
    [cardTextField resignFirstResponder];
    [self.delegate doneAction:cardTextField];
    [self backAction:sender];
}

- (IBAction)backAction:(id)sender
{
    [cardTextField resignFirstResponder];
    if (backButton.enabled == YES) {
        backButton.enabled = NO;
        [self sendViewToCenter:nil];
        [UIView animateWithDuration:ALAlertDuration
                              delay:0.0
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             backButton.alpha = 0;
                         } completion:^(BOOL finished) {
                             [self.navigationController popViewControllerAnimated:YES];
                         }];
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - Keyboard Auto System
- (void)keyboardWillChange:(NSNotification *)notification
{
    CGRect keyboardEndFrame =
    [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    UIViewAnimationCurve animationCurve =
    [[[notification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    NSTimeInterval animationDuration =
    [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] integerValue];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    float keyboardDispalyHeight = [UIScreen mainScreen].bounds.size.height - keyboardEndFrame.origin.y;
    
    self.view.transform =
    CGAffineTransformTranslate(CGAffineTransformIdentity, 0, -keyboardDispalyHeight);
    
    [UIView commitAnimations];
}

@end
