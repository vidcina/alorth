//
//  ALOnlinePaymentViewController.m
//  alorth
//
//  Created by w91379137 on 2015/12/22.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALOnlinePaymentViewController.h"
#import <ApplePayStubs/STPTestPaymentAuthorizationViewController.h>
#import "ALProfileDataViewController.h"

@interface ALOnlinePaymentViewController ()
{
    ALAlertViewController *alert;
}

@end

@implementation ALOnlinePaymentViewController

#pragma mark - VC Life
- (void)viewDidLoad
{
    [super viewDidLoad];
    [[PDSAccountManager sharedManager].stripeInfo addSafeObserver:self
                                                       forKeyPath:@"creditCardStatus"
                                                          options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionInitial
                                                          context:NULL];
}

#pragma mark - Reload
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if (object == [PDSAccountManager sharedManager].stripeInfo &&
        [keyPath isEqualToString:@"creditCardStatus"]) {
        NSArray *views = @[self.unauthorizedButtonView,
                           self.authorizedButtonView];
        
        for (UIView *view in views) {
            if (view.superview == nil) {
                view.hidden = YES;
                
                [self.view insertSubview:view atIndex:0];
                [view mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.edges.equalTo(self.view);
                }];
            }
        }
        
        [self unauthorizedButtonViewReload];
        [self authorizedButtonViewReload];
        
        if ([PDSAccountManager sharedManager].stripeInfo.creditCardStatus == CreditCardStatusAuthorize) {
            [self logOnlinePayment:ALOnlinePaymentLogAuthorize];
        }
    }
    else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void)unauthorizedButtonViewReload
{
    self.unauthorizedButtonView.hidden =
    !([PDSAccountManager sharedManager].stripeInfo.creditCardStatus == CreditCardStatusUnknow ||
      [PDSAccountManager sharedManager].stripeInfo.creditCardStatus == CreditCardStatusNo);
    
    if (self.unauthorizedButtonView.hidden == NO) {
        [self logoView:self.applePayLogoView AlignView:self.selectApplePayButton];
        [self logoView:self.stripeLogoView AlignView:self.selectStripeButton];
        self.applePayLogoView.hidden = NO;
        self.stripeLogoView.hidden = NO;
    }
    
    
    // 20160505: if apple pay not supported, app review requires the button to be hidden
    PKPaymentRequest *request = [Stripe paymentRequestWithMerchantIdentifier:ALMerchantIdentifier];
    BOOL supportApplePay = [PKPaymentAuthorizationViewController canMakePaymentsUsingNetworks:request.supportedNetworks];
    if (!supportApplePay) {
        self.applePayLogoView.hidden = YES;
    }
}

- (void)authorizedButtonViewReload
{
    self.authorizedButtonView.hidden =
    !([PDSAccountManager sharedManager].stripeInfo.creditCardStatus == CreditCardStatusAuthorize);
    
    if (self.authorizedButtonView.hidden == NO) {
        [self logoView:self.applePayLongLogoView AlignView:self.checkoutButton];
        [self logoView:self.stripeLongLogoView AlignView:self.checkoutButton];
        
        BOOL isApplePay =
        [[PDSAccountManager sharedManager].stripeInfo.firstCardType isEqualToString:ALCreditCardPayWithApplePay];
        
        self.stripeLongLogoView.hidden = isApplePay;
        self.applePayLongLogoView.hidden = !isApplePay;
        
        ALLog(@"FirstCardType : %@",[PDSAccountManager sharedManager].stripeInfo.firstCardType);
    }
}

- (void)logoView:(UIView *)logoView AlignView:(UIView *)alignView
{
    if (logoView.superview != alignView.superview) {
        [alignView.superview addSubview:logoView];
    }
    
    [logoView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(alignView);
        make.height.lessThanOrEqualTo(alignView.mas_height);
        make.width.lessThanOrEqualTo(alignView.mas_width).offset(-5);
    }];
}

#pragma mark - IBAction
- (IBAction)applePayAction:(id)sender
{
    if ([self shouldLoginFirst]) {
        return;
    }
    if ([self shouldEnterDeliverData]) {
        return;
    }
    [self confirmDeliverDataWithConfirmFinishBlock:^{
        NSDecimalNumber *priceNumber = nil;
        NSString *currency = nil;
        if ([self.delegate respondsToSelector:@selector(totalPrice:currency:)]) {
            [self.delegate totalPrice:&priceNumber currency:&currency];
        }
        else {
            YKSimpleAlert(@"Error: price and currency not specified");
            return;
        }
        //NSLog(@"applePayAction %@",[NSDate date]);
        [self logOnlinePayment:ALOnlinePaymentLogApplePay];
        registerCardType = ALCreditCardPayWithApplePay;
        
        PKPaymentRequest *request = [Stripe paymentRequestWithMerchantIdentifier:ALMerchantIdentifier];
        
        //    if ([PKPaymentSummaryItem respondsToSelector:@selector(summaryItemWithLabel:amount:type:)]) {
        //
        //        // Configure your request here.
        //        request.paymentSummaryItems =
        //        @[[PKPaymentSummaryItem summaryItemWithLabel:@"Aimcue Service"
        //                                              amount:[NSDecimalNumber zero] // should use zero for pending
        //                                                type:PKPaymentSummaryItemTypePending]];
        //    }
        //    else {
        request.currencyCode = currency;
        request.paymentSummaryItems =
        @[[PKPaymentSummaryItem summaryItemWithLabel:@"Aimcue Service"
                                              amount:priceNumber]];
        //    }
        
        void (^alertBlock)(void)= ^(void){
            // Show the user your own credit card form (see options 2 or 3)
            [[[UIAlertView alloc] initWithTitle:ALLocalizedString(@"Error",nil)
                                        message:ALLocalizedString(@"Apple Pay not supported",nil)
                                       delegate:nil
                              cancelButtonTitle:ALLocalizedString(@"OK",nil)
                              otherButtonTitles:nil] show];
        };
        
        if ([Stripe canSubmitPaymentRequest:request]) {
#if TARGET_IPHONE_SIMULATOR
            STPTestPaymentAuthorizationViewController *paymentController =
            [[STPTestPaymentAuthorizationViewController alloc] initWithPaymentRequest:request];
#else
            PKPaymentAuthorizationViewController *paymentController =
            [[PKPaymentAuthorizationViewController alloc] initWithPaymentRequest:request];
#endif
            
            if (paymentController) {
                paymentController.delegate = self;
                
                //apply 實機只能用 present
                //並無法 > PDS裝入navi 然後整個連帶 navi 一起拔掉
                [[PDSEnvironmentViewController sharedInstance] presentViewController:paymentController
                                                                            animated:NO completion:^{
                                                                                
                                                                            }];
            }
            else {
                alertBlock();
            }
        }
        else {
            alertBlock();
        }
    }];

}

- (IBAction)stripeAction:(id)sender
{
    if ([self shouldLoginFirst]) {
        return;
    }
    if ([self shouldEnterDeliverData]) {
        return;
    }
    [self confirmDeliverDataWithConfirmFinishBlock:^{
        //NSLog(@"stripeAction %@",[NSDate date]);
        
        [self logOnlinePayment:ALOnlinePaymentLogStripe];
        registerCardType = ALCreditCardPayWithStripe;
        
        ALCreditCardViewController *vc = [[ALCreditCardViewController alloc] init];
        vc.delegate = self;
        [[PDSEnvironmentViewController sharedInstance] submitViewController:vc];
    }];
    
}

- (IBAction)changeCardAction:(id)sender
{
    [self logOnlinePayment:ALOnlinePaymentLogChangeCard];
    
    NSDictionary *cardDict = [PDSAccountManager sharedManager].stripeInfo.creditCardInfoArray.firstObject;
    
    [self addMBProgressHUDWithKey:@"creditCardTokenA075"];
    [ALNetWorkAPI creditCardTokenA075StripeToken:nil
                                      CardNumber:cardDict[@"CardNumber"]
                                        CardType:cardDict[@"CardType"]
                                            UUID:cardDict[@"UUID"]
                                          Stauts:@"1"
                                 CompletionBlock:^(NSDictionary *responseObject) {
                                     [self removeMBProgressHUDWithKey:@"creditCardTokenA075"];
                                     
                                     if ([ALNetWorkAPI checkSerialNumber:@"A075"
                                                          ResponseObject:responseObject]) {
                                         
                                         //重新取得 伺服器卡號狀態
                                         [self addMBProgressHUDWithKey:@"stripeInfo detectCreditCardStatus"];
                                         [[PDSAccountManager sharedManager].stripeInfo detectCreditCardStatusCompletionBlock:^{
                                             [self removeMBProgressHUDWithKey:@"stripeInfo detectCreditCardStatus"];
                                         }];
                                     }
                                 }];
}

- (IBAction)doubleCheckAction:(UIButton *)sender
{
    BOOL isDelegateHandle = NO;
    if ([self.delegate respondsToSelector:@selector(isCustomDoubleCheckView:)]) {
        isDelegateHandle = [self.delegate isCustomDoubleCheckView:self];
    }
    if (!isDelegateHandle) {
        alert = [[ALAlertViewController alloc] init];
        alert.type = ALAlertViewTypeBottom;
        [[PDSEnvironmentViewController sharedInstance] submitViewController:alert];
        
        [self.doubleCheckView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@([UIScreen mainScreen].bounds.size.width));
        }];
        
        self.doubleCheckView.userInteractionEnabled = YES;
        [alert sendViewToCenter:self.doubleCheckView];
        [alert showBackButton:nil];
    }
}

- (IBAction)confirmToPayAction:(UIButton *)sender
{
    if (alert) {
        self.doubleCheckView.userInteractionEnabled = NO;//只能一次
        
        [alert sendViewToCenter:nil];
        [alert.navigationController popToRootViewControllerAnimated:NO];
        alert = nil;
    }
    
    [self logOnlinePayment:ALOnlinePaymentLogCheckout];
    [self.delegate onlinePayAction];
}

- (BOOL)shouldLoginFirst
{
    if (![PDSAccountManager isLoggedIn]) {
        [[PDSEnvironmentViewController sharedInstance] presentLoginViewController];
        return YES;
    }
    return NO;
}

- (BOOL)shouldEnterDeliverData
{
    NSString *address = [[ALAppDelegate sharedAppDelegate] userInfomation][@"Address"];
//    NSLog(@"address1:%@\naddress2:%@",address1,address2);
    if ([address length] < 10) {
        ALProfileDataViewController *vc = [[ALProfileDataViewController alloc] init];
        [[PDSEnvironmentViewController sharedInstance] submitViewController:vc];
        YKSimpleAlert(ALLocalizedString(@"Please enter correct address", nil));
        return YES;
    }
    return NO;
}

- (void)confirmDeliverDataWithConfirmFinishBlock:(void(^)(void))block
{
    NSString *address = [[ALAppDelegate sharedAppDelegate] userInfomation][@"Address"];
    UIAlertController *ac = [UIAlertController alertControllerWithTitle:ALLocalizedString(@"Please confirm your address", nil) message:address preferredStyle:UIAlertControllerStyleAlert];
    [ac addAction:[UIAlertAction actionWithTitle:ALLocalizedString(@"Confirm", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        block();
    }]];
    [ac addAction:[UIAlertAction actionWithTitle:ALLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [[PDSEnvironmentViewController sharedInstance] presentViewController:ac animated:YES completion:nil];
}

#pragma mark - Stripe CreditCard
#pragma mark ALCreditCardViewControllerDelegate
- (void)doneAction:(STPPaymentCardTextField *)cardTextField
{
    if (cardTextField.cardNumber.length < 16) {
        //小於16碼 視為取消
        return;
    }
    
    void (^alertBlock)(void)= ^(void){
        [[[UIAlertView alloc] initWithTitle:ALLocalizedString(@"Error",nil)
                                    message:ALLocalizedString(@"Invalid Card Number",nil)
                                   delegate:nil
                          cancelButtonTitle:ALLocalizedString(@"OK",nil)
                          otherButtonTitles:nil] show];
    };
    
    if (!cardTextField.isValid) {
        alertBlock();
        return;
    }
    
    STPCardParams *card = [[STPCardParams alloc] init];
    card.number = cardTextField.cardParams.number;
    card.expMonth = cardTextField.cardParams.expMonth;
    card.expYear = cardTextField.cardParams.expYear;
    card.cvc = cardTextField.cardParams.cvc;
    [[STPAPIClient sharedClient] createTokenWithCard:card
                                          completion:^(STPToken *token, NSError *error) {
                                              if (error) {
                                                  alertBlock();
                                              }
                                              else {
                                                  [self createBackendChargeWithToken:token
                                                                          completion:^(PKPaymentAuthorizationStatus status){}];
                                              }
                                          }];
}

#pragma mark - ApplePay
#pragma mark PKPaymentAuthorizationViewControllerDelegate
- (void)paymentAuthorizationViewController:(PKPaymentAuthorizationViewController *)controller
                       didAuthorizePayment:(PKPayment *)payment
                                completion:(void (^)(PKPaymentAuthorizationStatus status))completion
{
    /*
     We'll implement this method below in 'Creating a single-use token'.
     Note that we've also been given a block that takes a
     PKPaymentAuthorizationStatus. We'll call this function with either
     PKPaymentAuthorizationStatusSuccess or PKPaymentAuthorizationStatusFailure
     after all of our asynchronous code is finished executing. This is how the
     PKPaymentAuthorizationViewController knows when and how to update its UI.
     */
    [self handlePaymentAuthorizationWithPayment:payment completion:completion];
}

- (void)handlePaymentAuthorizationWithPayment:(PKPayment *)payment
                                   completion:(void (^)(PKPaymentAuthorizationStatus))completion {
    
    [[STPAPIClient sharedClient] createTokenWithPayment:payment
                                             completion:^(STPToken *token, NSError *error) {
                                                 if (error) {
                                                     completion(PKPaymentAuthorizationStatusFailure);
                                                     return;
                                                 }
                                                 /*
                                                  We'll implement this below in "Sending the token to your server".
                                                  Notice that we're passing the completion block through.
                                                  See the above comment in didAuthorizePayment to learn why.
                                                  */
                                                 [self createBackendChargeWithToken:token completion:completion];
                                             }];
}

- (void)paymentAuthorizationViewControllerDidFinish:(PKPaymentAuthorizationViewController *)controller
{
    [controller dismissViewControllerAnimated:NO completion:^{
        
    }];
}

#pragma mark - 通用
// completion只用於Apple Pay時
- (void)createBackendChargeWithToken:(STPToken *)token
                          completion:(void (^)(PKPaymentAuthorizationStatus))completion
{
    [self addMBProgressHUDWithKey:@"creditCardTokenA075"];
    [ALNetWorkAPI creditCardTokenA075StripeToken:token.tokenId
                                      CardNumber:token.card.dynamicLast4 ? token.card.dynamicLast4 : token.card.last4
                                        CardType:registerCardType
                                            UUID:nil
                                          Stauts:@"0"
                                 CompletionBlock:^(NSDictionary *responseObject) {
                                     [self removeMBProgressHUDWithKey:@"creditCardTokenA075"];
                                     
                                     if ([ALNetWorkAPI checkSerialNumber:@"A075"
                                                          ResponseObject:responseObject]) {
                                         completion(PKPaymentAuthorizationStatusSuccess);
                                         //NSLog(@"creditCardToken %@",[NSDate date]);
                                         //重新取得 伺服器卡號狀態
                                         [self addMBProgressHUDWithKey:@"stripeInfo detectCreditCardStatus"];
                                         [[PDSAccountManager sharedManager].stripeInfo detectCreditCardStatusCompletionBlock:^{
                                             [self removeMBProgressHUDWithKey:@"stripeInfo detectCreditCardStatus"];
                                             
                                             if ([PDSAccountManager sharedManager].stripeInfo.creditCardStatus == CreditCardStatusAuthorize) {
                                                 [self doubleCheckAction:nil]; //馬上開始付款
                                             }
                                             else {
                                                 YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A075", responseObject),nil));
                                             }
                                         }];
                                     }
                                     else {
                                         YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A075", responseObject),nil));
                                         completion(PKPaymentAuthorizationStatusFailure);
                                     }
                                 }];
    /*
    NSURL *url = [NSURL URLWithString:@"https://example.com/token"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"POST";
    NSString *body     = [NSString stringWithFormat:@"stripeToken=%@", token.tokenId];
    request.HTTPBody   = [body dataUsingEncoding:NSUTF8StringEncoding];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data,
                                               NSError *error) {
                               if (error) {
                                   if (completion) {
                                       completion(PKPaymentAuthorizationStatusFailure);
                                   }
                               } else {
                                   if (completion) {
                                       completion(PKPaymentAuthorizationStatusSuccess);
                                   }
                               }
                           }];
     */
}

#pragma mark - log
- (void)logOnlinePayment:(ALOnlinePaymentViewControllerLogType)logType
{
    if ([self.delegate respondsToSelector:@selector(touchOnlinePaymentButtonLog:)]) {
        [self.delegate touchOnlinePaymentButtonLog:logType];
    }
}

@end
