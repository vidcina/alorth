//
//  ALStripeConnectViewController.m
//  alorth
//
//  Created by John Hsu on 2015/12/8.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALStripeConnectViewController.h"

@interface ALStripeConnectViewController ()

@end

@implementation ALStripeConnectViewController

- (instancetype)init
{
    self = [super initWithNibName:@"ALWebViewController" bundle:nil];
    if (self) {
        //??
    }
    return self;
}

-(BOOL)webView:(UIWebView *)aWebView shouldStartLoadWithRequest:(NSURLRequest *)request
navigationType:(UIWebViewNavigationType)navigationType
{
    if ([[request.URL absoluteString] hasPrefix:StripeAuthURL]) {
        
        //以下可以改寫成 dispatch_group_t
        
        [self addMBProgressHUDWithKey:@"FinishStripeConnect"];//0
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            //Delay 取得資料 須在 global_queue 執行
            NSData *data = [NSData dataWithContentsOfURL:request.URL options:NSDataReadingUncached error:nil];
            NSDictionary *dict = data ? [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil] : nil;
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                if ([dict[@"access_token"] length]) {
                    //NSString *fullJSONString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    
                    [ALNetWorkAPI finishStripeConnect:dict
                                      CompletionBlock:^(NSDictionary *responseObject) {
                                          
                                          dispatch_group_t wait = dispatch_group_create();
                                          
                                          dispatch_group_enter(wait);
                                          if ([ALNetWorkAPI checkSerialNumber:@"A072"
                                                               ResponseObject:responseObject]) {
                                              [[PDSAccountManager sharedManager].stripeInfo detectStripeStatusCompletionBlock:^{
                                                  dispatch_group_leave(wait);
                                              }];
                                          }
                                          else {
                                              dispatch_group_leave(wait);
                                          }
                                          
                                          dispatch_group_notify(wait,dispatch_get_main_queue(),^{
                                              //成功與否 都是跳出
                                              [self removeMBProgressHUDWithKey:@"FinishStripeConnect"];//1
                                              [self.navigationController popViewControllerAnimated:YES];//2
                                              
                                              if (![ALNetWorkAPI checkSerialNumber:@"A072"
                                                                    ResponseObject:responseObject]) {
                                                  //顯示錯誤代碼
                                                  YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A072", responseObject),nil));//3
                                              }
                                          });
                                      }];
                }
                else {
                    [self removeMBProgressHUDWithKey:@"FinishStripeConnect"];//1
                    [self.navigationController popViewControllerAnimated:YES];//2
                    YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));//3
                }
            });
        });
        return NO;
    }
    return YES;
}

@end
