//
//  ALStripeEntryViewController.h
//  alorth
//
//  Created by John Hsu on 2015/10/30.
//  Copyright (c) 2015年 w91379137. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <Stripe/Stripe.h>
#import <PassKit/PassKit.h>

@interface ALStripeEntryViewController : UIViewController <PKPaymentAuthorizationViewControllerDelegate, STPPaymentCardTextFieldDelegate>
{
    IBOutlet UIScrollView *scrollView;
    
    IBOutlet UITextField *productNameField;
    IBOutlet UITextField *stockNameField;
    IBOutlet UITextField *stockPriceField;
    IBOutlet UITextField *stockPriceUnitField;
    IBOutlet UITextField *amountField;
    IBOutlet UITextField *shipFeeField;
    IBOutlet UITextField *shipFeeUnitField;
    IBOutlet UITextField *totalPriceField;
    
    IBOutlet STPPaymentCardTextField *cardTextField;
}

@end

