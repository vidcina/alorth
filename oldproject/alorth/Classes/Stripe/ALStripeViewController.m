//
//  ALStripeViewController.m
//  alorth
//
//  Created by w91379137 on 2015/12/16.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALStripeViewController.h"
#import "PDSDeviceAuthorized.h"
#import "ALStripeConnectViewController.h"

@interface ALStripeViewController ()
@property (nonatomic, strong) NSMutableDictionary *questionimageViews;
@property (nonatomic, strong) IBOutlet UIView *firstAlertView;

@end

@implementation ALStripeViewController

#pragma mark - VC Life
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.type = ALAlertViewTypeCenter;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.firstAlertView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@([UIScreen mainScreen].bounds.size.width - 60));
    }];
    
    BOOL isPass = [self checkAndReloadAuthStatus]; //圖片上未產生 無法設定
    
    if (isPass) {
        self.view.hidden = YES;
    }
    else {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(checkAndReloadAuthStatus)
                                                     name:UIApplicationDidBecomeActiveNotification object:nil];
    }
    
    self.firstAlertView.layer.cornerRadius = 5.0;
    self.firstAlertView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.firstAlertView.layer.borderWidth = 1.5;
    
    mainTitleLabel.localText = self.mainTitleString;
    subTitleLabel.localText = self.subTitleString;
    
    self.questionimageViews = [NSMutableDictionary dictionary];
    float buttonHeight = 44;
    
    UIButton *lastOne = nil;
    for (NSInteger k = 0; k < self.questionTypeArray.count; k++) {
        
        ALStripeViewQuestion type = [self.questionTypeArray[k] integerValue];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.tag = type;
        
        button.backgroundColor = COMMON_Yellow_COLOR;
        button.layer.cornerRadius = buttonHeight / 2;
        button.clipsToBounds = YES;
        [button addTarget:self action:@selector(questionAction:) forControlEvents:UIControlEventTouchUpInside];
        
        {
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.contentMode = UIViewContentModeScaleAspectFit;
            self.questionimageViews[[NSString stringWithFormat:@"%ld",(long)type]] = imageView;
            
            [button addSubview:imageView];
            [imageView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(@(20));
                make.height.equalTo(@(20));
                make.leading.equalTo(button.mas_leading).offset(30);
                make.centerY.equalTo(button.mas_centerY);
            }];
            
            UILabel *titleLabel = [[UILabel alloc] init];
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.font = [UIFont systemFontOfSize:13.0f];
            titleLabel.textColor = COMMON_GRAY4_COLOR;
            
            if (type == QuestionMircrophone) titleLabel.localText = @"Enable Mircrophone";
            if (type == QuestionCamera) titleLabel.localText = @"Enable Camera";
            if (type == QuestionLocation) titleLabel.localText = @"Enable Location";
            if (type == QuestionStripe) titleLabel.localText = @"Stripe connect";
            
            [button addSubview:titleLabel];
            [titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(button.mas_top);
                make.bottom.equalTo(button.mas_bottom);
                make.trailing.equalTo(button.mas_trailing);
                make.leading.equalTo(imageView.mas_trailing);
            }];
        }
        
        [baseContainerView addSubview:button];
        [button mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@240);
            make.height.equalTo(@(buttonHeight));
            make.centerX.equalTo(baseContainerView.mas_centerX);
            
            if (lastOne) {
                make.top.equalTo(lastOne.mas_bottom).offset(10);
            }
            else {
                make.top.equalTo(baseContainerView.mas_top);
            }
        }];
        
        lastOne = button;
    }
    
    if (lastOne) {
        [lastOne mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(baseContainerView.mas_bottom);
        }];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self checkAndReloadAuthStatus]; //刷新圖片
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    BOOL isPass = [self checkAndReloadAuthStatus];
    
    if (isPass) {
        //已經全數通過
        [self backAction:backButton];
    }
    else {
        if (backButton.enabled == NO) {
            [UIView animateWithDuration:ALAlertDuration
                                  delay:0.0
                                options:UIViewAnimationOptionBeginFromCurrentState
                             animations:^{
                                 backButton.alpha = 1;
                             } completion:^(BOOL finished) {
                                 backButton.enabled = YES;
                                 [self sendViewToCenter:self.firstAlertView];
                             }];
        }
        
        {//螢幕顯示後 額外更新
            BOOL shouldRenewStripe = NO;
            for (NSNumber *number in self.questionTypeArray) {
                if (QuestionStripe == [number integerValue]) {
                    shouldRenewStripe = YES;
                    break;
                }
            }
            
            if (shouldRenewStripe) {
                //更新狀態
                [self addMBProgressHUDWithKey:@"detectStripeStatus"];
                [[PDSAccountManager sharedManager].stripeInfo detectStripeStatusCompletionBlock:^{
                    [self removeMBProgressHUDWithKey:@"detectStripeStatus"];
                    
                    [self checkAndReloadAuthStatus];
                }];
            }
        }
    }
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - IBAction
- (IBAction)backAction:(id)sender
{
    if (backButton.enabled == YES) {
        backButton.enabled = NO;
        [self sendViewToCenter:nil];
        [UIView animateWithDuration:ALAlertDuration
                              delay:0.0
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             backButton.alpha = 0;
                         } completion:^(BOOL finished) {
                             [self.navigationController popViewControllerAnimated:YES];
                         }];
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)questionAction:(UIButton *)sender
{
    switch ((ALStripeViewQuestion)sender.tag) {
        case QuestionCamera:
        {
            PDSDeviceAuthorizedStatus authStatus = [PDSDeviceAuthorized authorizedStatus:Camera];
            if (authStatus == Unknown) [PDSDeviceAuthorized openAlert:Camera CompletionBlock:^{
                [self checkAndReloadAuthStatus];
            }];
            if (authStatus == Unauthorized) {
                [YKBlockAlert alertWithTitle:ALLocalizedString(@"Services are off", nil)
                                     message:ALLocalizedString(@"Please turn on service to start broadcast", nil)
                            blocksAndButtons:
                 ^{
                     [PDSDeviceAuthorized openSetting];
                 }, ALLocalizedString(@"OK", nil),
                 
                 ^{}, ALLocalizedString(@"Cancel", nil),nil];
            }
        }
            break;
            
        case QuestionMircrophone:
        {
            PDSDeviceAuthorizedStatus authStatus = [PDSDeviceAuthorized authorizedStatus:Microphone];
            if (authStatus == Unknown) [PDSDeviceAuthorized openAlert:Microphone CompletionBlock:^{
                [self checkAndReloadAuthStatus];
            }];
            if (authStatus == Unauthorized) {
                [YKBlockAlert alertWithTitle:ALLocalizedString(@"Services are off", nil)
                                     message:ALLocalizedString(@"Please turn on service to start broadcast", nil)
                            blocksAndButtons:
                 ^{
                     [PDSDeviceAuthorized openSetting];
                 }, ALLocalizedString(@"OK", nil),
                 
                 ^{}, ALLocalizedString(@"Cancel", nil),nil];
            }
        }
            break;
            
        case QuestionLocation:
        {
            PDSDeviceAuthorizedStatus authStatus = [PDSDeviceAuthorized authorizedStatus:LocationInUse];
            if (authStatus == Unknown) [PDSDeviceAuthorized openAlert:LocationInUse CompletionBlock:^{
                [self checkAndReloadAuthStatus];
            }];
            if (authStatus == Unauthorized) {
                [YKBlockAlert alertWithTitle:ALLocalizedString(@"Services are off", nil)
                                     message:ALLocalizedString(@"Please turn on service to start broadcast", nil)
                            blocksAndButtons:
                 ^{
                     [PDSDeviceAuthorized openSetting];
                 }, ALLocalizedString(@"OK", nil),
                 
                 ^{}, ALLocalizedString(@"Cancel", nil),nil];
            }
        }
            break;
            
        case QuestionStripe:
        {
            //分成 綁定 未綁定 尚未得知
            PDSDeviceAuthorizedStatus authStatus = [ALStripeInfo stripeStatus];
            
            //目前這邊應該不會通過 viewDidAppear 有更新
            if (authStatus == Unknown) {
                [self addMBProgressHUDWithKey:@"detectStripeStatus"];
                [[PDSAccountManager sharedManager].stripeInfo detectStripeStatusCompletionBlock:^{
                    [self removeMBProgressHUDWithKey:@"detectStripeStatus"];
                    
                    [self checkAndReloadAuthStatus];
                }];
            }
            
            if (authStatus == Unauthorized) {
                
                //不需要選 直接跳過去
                [self addMBProgressHUDWithKey:@"stripeConnectURLA071"];
                [ALNetWorkAPI stripeConnectURLA071CompletionBlock:^(NSDictionary *responseObject) {
                    [self removeMBProgressHUDWithKey:@"stripeConnectURLA071"];
                    
                    BOOL isSuccess = NO;
                    if ([ALNetWorkAPI checkSerialNumber:@"A071"
                                         ResponseObject:responseObject]) {
                        NSString *connectURLString = responseObject.safe[@"resBody"][@"ConnectUrl"];
                        if ([connectURLString length] > 0) {
                            ALStripeConnectViewController *vc =
                            [[ALStripeConnectViewController alloc] init];
                            vc.urlString = connectURLString;
                            vc.titleString = @"";
                            [self.navigationController pushViewController:vc animated:YES];
                            isSuccess = YES;
                        }
                    }
                    
                    if (!isSuccess) {
                        YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A071", responseObject),nil));
                    }
                }];
            }
        }
            break;
            
        default:
            break;
    }
}

#pragma mark -
- (BOOL)checkAndReloadAuthStatus
{
    return [ALStripeViewController checkAndReloadAuthStatus:self QuestionTypeArray:self.questionTypeArray];
}

+ (BOOL)checkQuestionTypeArray:(NSArray *)questionTypeArray
{
    return [ALStripeViewController checkAndReloadAuthStatus:nil QuestionTypeArray:questionTypeArray];
}

+ (BOOL)checkAndReloadAuthStatus:(ALStripeViewController *)controller
               QuestionTypeArray:(NSArray *)questionTypeArray
{
    BOOL isPass = YES;
    
    for (NSInteger k = 0; k < questionTypeArray.count; k++) {
        
        ALStripeViewQuestion type = [questionTypeArray[k] integerValue];
        
        PDSDeviceAuthorizedStatus authStatus = Unauthorized;
        
        UIImageView *imageView = controller.questionimageViews[[NSString stringWithFormat:@"%ld",(long)type]];
        
        switch (type) {
            case QuestionCamera:
            {
                authStatus = [PDSDeviceAuthorized authorizedStatus:Camera];
                if (authStatus == Unknown) {
                    UIImage *camera = [UIImage imageNamed:@"PublishCamera"];
                    imageView.image = [UIImage changeColor:camera color:COMMON_GRAY4_COLOR];
                }
            }
                break;
            case QuestionMircrophone:
            {
                authStatus = [PDSDeviceAuthorized authorizedStatus:Microphone];
                if (authStatus == Unknown) imageView.image = [UIImage imageNamed:@"Authorized_Microphone"];
            }
                break;
            case QuestionLocation:
            {
                authStatus = [PDSDeviceAuthorized authorizedStatus:LocationInUse];
                if (authStatus == Unknown) imageView.image = [UIImage imageNamed:@"Authorized_Location"];
            }
                break;
            case QuestionStripe:
            {
                authStatus = [ALStripeInfo stripeStatus];
                if (authStatus == Unknown) imageView.image = [UIImage imageNamed:@"Authorized_Stripe"];
            }
                break;
                
            default:
                break;
        }
        
        if (authStatus == Unauthorized || authStatus == Disabled) {
            UIImage *black = [UIImage imageNamed:@"cross_white180.png"];
            imageView.image = [UIImage changeColor:black color:COMMON_GRAY4_COLOR];
        }
        if (authStatus == Authorized) imageView.image = [UIImage imageNamed:@"Authorized_finish"];
        
        isPass = isPass && (authStatus == Authorized);
    }
    
    ALLog(@"isPass %d",isPass);
    return isPass;
}

@end
