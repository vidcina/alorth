//
//  ALOnlinePaymentViewController.h
//  alorth
//
//  Created by w91379137 on 2015/12/22.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Stripe/Stripe.h>
#import <PassKit/PassKit.h>
#import "ALCreditCardViewController.h"

//apple pay 圖片
//https://developer.apple.com/apple-pay/

//stripe 圖片
//https://stripe.com/about/resources

@protocol ALOnlinePaymentViewControllerDelegate;
@interface ALOnlinePaymentViewController : UIViewController
<PKPaymentAuthorizationViewControllerDelegate,
STPPaymentCardTextFieldDelegate,
ALCreditCardViewControllerDelegate>
{
    NSString *registerCardType;
}

@property (nonatomic, weak) id<ALOnlinePaymentViewControllerDelegate> delegate;

@property (nonatomic, strong) IBOutlet UIView *applePayLogoView;
@property (nonatomic, strong) IBOutlet UIView *stripeLogoView;

@property (nonatomic, strong) IBOutlet UIView *applePayLongLogoView;
@property (nonatomic, strong) IBOutlet UIView *stripeLongLogoView;

@property (nonatomic, strong) IBOutlet UIView *unauthorizedButtonView;
@property (nonatomic, strong) IBOutlet UIButton *selectApplePayButton;
@property (nonatomic, strong) IBOutlet UIButton *selectStripeButton;

@property (nonatomic, strong) IBOutlet UIView *authorizedButtonView;
@property (nonatomic, strong) IBOutlet UIButton *checkoutButton;

@property (nonatomic, strong) IBOutlet UIView *doubleCheckView;

- (IBAction)confirmToPayAction:(UIButton *)sender; //若自製確認畫面 需要把按鈕 接上 驅動結帳

@end

typedef NS_ENUM(NSInteger, ALOnlinePaymentViewControllerLogType) {
    ALOnlinePaymentLogApplePay,
    ALOnlinePaymentLogStripe,
    ALOnlinePaymentLogAuthorize,
    ALOnlinePaymentLogChangeCard,
    ALOnlinePaymentLogCheckout
};

#pragma mark - delegate
@protocol ALOnlinePaymentViewControllerDelegate <NSObject>

- (void)onlinePayAction; //目前只有一張卡 沒有回傳參數
- (void)totalPrice:(NSDecimalNumber **)decimalNumber currency:(NSString **)currency;
@optional

- (BOOL)isCustomDoubleCheckView:(ALOnlinePaymentViewController *)onlinePaymentViewController;
- (void)touchOnlinePaymentButtonLog:(ALOnlinePaymentViewControllerLogType)logType;

@end