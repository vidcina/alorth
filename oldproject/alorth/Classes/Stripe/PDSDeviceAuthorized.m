//
//  PDSDeviceAuthorized.m
//
//  Created by w91379137 on 2015/12/16.
//

#import "PDSDeviceAuthorized.h"
static NSMutableSet *objects;

@implementation PDSDeviceAuthorized

+ (NSMutableSet *)retainObjectsSet
{
    if (objects == nil) {
        objects = [NSMutableSet set];
    }
    return objects;
}

+ (PDSDeviceAuthorizedStatus)authorizedStatus:(PDSDeviceAuthorizedType)type
{
    switch (type) {
        case Camera:
        {
            AVAuthorizationStatus status =
            [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
            
            switch (status) {
                case AVAuthorizationStatusAuthorized: return Authorized;
                case AVAuthorizationStatusDenied: return Unauthorized;
                case AVAuthorizationStatusRestricted: return Unauthorized;
                case AVAuthorizationStatusNotDetermined: return Unknown;
                default: return Disabled;
            }
        }
            break;
            
        case Microphone:
        {
            #if TARGET_IPHONE_SIMULATOR
            
            NSLog(@"模擬器 Microphone Authorized");
            return Authorized;
            
            #else
            
            if ([[ALAppDelegate sharedAppDelegate].userID isEqualToString:@"w913791379137"]) {
                NSLog(@"w Microphone Authorized");
                return Authorized;
            }
            if ([[ALAppDelegate sharedAppDelegate].userID isEqualToString:@"john"]) {
                NSLog(@"j Microphone Authorized");
                return Authorized;
            }
            
            AVAudioSessionRecordPermission status =
            [AVAudioSession sharedInstance].recordPermission;
            
            switch (status) {
                case AVAudioSessionRecordPermissionGranted: return Authorized;
                case AVAudioSessionRecordPermissionDenied: return Unauthorized;
                case AVAudioSessionRecordPermissionUndetermined: return Unknown;
                default: return Disabled;
            }
            #endif
        }
           break;
            
        case LocationInUse:
        {
            if ([CLLocationManager locationServicesEnabled]) {
                
                CLAuthorizationStatus status =
                [CLLocationManager authorizationStatus];
                
                switch (status) {
                    case kCLAuthorizationStatusNotDetermined: return Unknown;
                    case kCLAuthorizationStatusRestricted: return Unauthorized;
                    case kCLAuthorizationStatusDenied: return Unauthorized;
                    case kCLAuthorizationStatusAuthorizedAlways: return Authorized;
                    case kCLAuthorizationStatusAuthorizedWhenInUse: return Authorized;
                    default: return Disabled;
                }
            }
            else {
                return Disabled;
            }
        }
            break;
            
        default:
            break;
    }

    return Disabled;
}

+ (void)openAlert:(PDSDeviceAuthorizedType)type CompletionBlock:(void (^)())completionBlock;
{
    if (!completionBlock) {
        completionBlock = ^{};
    }
    
    switch (type) {
        case Camera:
        {
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                completionBlock();
            }];
        }
            break;
            
        case Microphone:
        {
            [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
                completionBlock();
            }];
        }
            break;
            
        case LocationInUse:
        {
            BOOL hasWhenInUseKey =
            !([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"] == nil);
            NSAssert(hasWhenInUseKey, @"NSLocationWhenInUseUsageDescription not found in Info.plist.");
            
            PDSLocationManagerDelegate *delegate = [[PDSLocationManagerDelegate alloc] init];
            delegate.completeBlock = completionBlock;
            
            CLLocationManager *locationManager = [[CLLocationManager alloc] init];
            locationManager.delegate = delegate;
            [locationManager requestWhenInUseAuthorization];
            
            [[self retainObjectsSet] addObject:delegate];
            [[self retainObjectsSet] addObject:locationManager];
        }
            break;
            
        default:
            break;
    }
}

+ (void)openSetting
{
    NSURL *settingsUrl =
    [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    [[UIApplication sharedApplication] openURL:settingsUrl];
}

@end

@implementation PDSLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (kCLAuthorizationStatusNotDetermined != status) {
        self.completeBlock();
        self.completeBlock = nil;
        manager.delegate = nil;
        [[PDSDeviceAuthorized retainObjectsSet] removeObject:manager];
        [[PDSDeviceAuthorized retainObjectsSet] removeObject:self];
    }
    else {
        NSLog(@"...");
    }
}

@end
