//
//  PDSDeviceAuthorized.h
//
//  Created by w91379137 on 2015/12/16.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, PDSDeviceAuthorizedType) {
    Contacts,
    LocationAlways,
    LocationInUse,
    Notifications,
    Microphone,
    Camera,
    Photos,
    Reminders,
    Events,
    Bluetooth,
    Motion,
};

typedef NS_ENUM(NSInteger, PDSDeviceAuthorizedStatus) {
    Authorized,
    Unauthorized,
    Unknown,
    Disabled
};

@interface PDSDeviceAuthorized : NSObject

+ (PDSDeviceAuthorizedStatus)authorizedStatus:(PDSDeviceAuthorizedType)type;
+ (NSMutableSet *)retainObjectsSet;

+ (void)openAlert:(PDSDeviceAuthorizedType)type CompletionBlock:(void (^)())completionBlock;;
+ (void)openSetting;

@end

@interface PDSLocationManagerDelegate : NSObject
<CLLocationManagerDelegate>

@property(nonatomic,copy) void(^completeBlock)();

@end

