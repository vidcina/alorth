//
//  ALAlertViewController.m
//  alorth
//
//  Created by w91379137 on 2015/12/22.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALAlertViewController.h"

@interface ALAlertViewController ()

@end

@implementation ALAlertViewController

#pragma mark - VC Life
- (instancetype)init
{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillChange:)
                                                     name:UIKeyboardWillChangeFrameNotification
                                                   object:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (backButton == nil) {
        backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
        backButton.enabled = NO;
        backButton.alpha = 0;
        [backButton addTarget:self
                       action:@selector(backAction:)
             forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:backButton];
        [backButton mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
    }
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillChangeFrameNotification
                                                  object:nil];
    NSLog(@"%@ dealloc",NSStringFromClass([self class]));
}

#pragma mark - Keyboard Auto System
- (void)keyboardWillChange:(NSNotification *)notification
{
    /*
    CGRect keyboardEndFrame =
    [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    UIViewAnimationCurve animationCurve =
    [[[notification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    NSTimeInterval animationDuration =
    [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] integerValue];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    float keyboardDispalyHeight = [UIScreen mainScreen].bounds.size.height - keyboardEndFrame.origin.y;
     */
}

#pragma mark - Center
- (void)sendViewToCenter:(UIView *)view
{
    if (theCenterView) {
        lastCenterView = theCenterView;
    }
    
    theCenterView = view;
    [self.view addSubview:theCenterView];
    
    float screenHeight = [UIScreen mainScreen].bounds.size.height;
    float screenWidth = [UIScreen mainScreen].bounds.size.width;
    CATransform3D transformWaitIn;
    CATransform3D transformWaitOut;
    
    switch (self.type) {
        case ALAlertViewTypeBottom:
        {
            [theCenterView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.view.mas_bottom);
                make.centerX.equalTo(self.view.mas_centerX);
            }];
            
            transformWaitIn = CATransform3DMakeTranslation(0, screenHeight, 0);
            transformWaitOut = CATransform3DMakeTranslation(0, screenHeight, 0);
        }
            break;
            
        case ALAlertViewTypeRight:
        {
            [theCenterView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.trailing.equalTo(self.view.mas_trailing);
                make.centerY.equalTo(self.view.mas_centerY);
            }];
            
            transformWaitIn = CATransform3DMakeTranslation(screenWidth, 0, 0);
            transformWaitOut = CATransform3DMakeTranslation(screenWidth, 0, 0);
        }
            break;
            
        
        default:
        {
            [theCenterView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.center.equalTo(self.view);
                make.width.equalTo(self.view.mas_width).offset(-30).with.priorityLow();
            }];
            
            transformWaitIn = CATransform3DMakeTranslation(0, -screenHeight, 0);
            transformWaitOut = CATransform3DMakeTranslation(0, screenHeight, 0);
        }
            break;
    }
    
    theCenterView.layer.transform = transformWaitIn;
    
    [UIView animateWithDuration:ALAlertDuration
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         theCenterView.layer.transform = CATransform3DIdentity;
                         lastCenterView.layer.transform = transformWaitOut;
                     } completion:^(BOOL finished) {}];
}

- (void)showBackButton:(void (^)())completion
{
    [UIView animateWithDuration:ALAlertDuration
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         backButton.alpha = 1;
                     } completion:^(BOOL finished) {
                         backButton.enabled = YES;
                         if (completion) completion();
                     }];
}

#pragma mark - IBAction
- (IBAction)backAction:(id)sender
{
    //ALLog(@"please overwrite %s",__func__);
    [self.navigationController popToRootViewControllerAnimated:NO];
}

@end
