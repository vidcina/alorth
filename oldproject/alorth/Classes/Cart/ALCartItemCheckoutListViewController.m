//
//  ALCartItemCheckoutListViewController.m
//  alorth
//
//  Created by w91379137 on 2016/3/5.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALCartItemCheckoutListViewController.h"
#import "ALCartItemView.h"
#import "ALCartItemCell.h"

@interface ALCartItemCheckoutListViewController ()

@end

@implementation ALCartItemCheckoutListViewController

#pragma mark - Init
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.type = ALAlertViewTypeBottom;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    backButton.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
    
    float totalPrice = [self.infoDict[@"TotalPrice"] floatValue];
    float totalShipping = [self.infoDict[@"TotalShipping"] floatValue];
    
    self.itemTotalPriceLabel.text =
    [NSString stringWithFormat:@"%.2f USD",totalPrice - totalShipping];
    
    self.shippingFeeLabel.text =
    [NSString stringWithFormat:@"%.2f USD",totalShipping];
    
    self.taxLabel.text =
    [NSString stringWithFormat:@"%.2f USD",0.0f];
    
    self.orderTotalPriceLabel.text =
    [NSString stringWithFormat:@"%.2f USD",totalPrice];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [baseView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@([UIScreen mainScreen].bounds.size.width - 30));
    }];
    
    NSArray *cartList = self.infoDict[@"CartList"];
    UIView *lastOne = nil;
    
    for (NSInteger k = 0; k < cartList.count; k++) {
        ALCartItemView *view = [[ALCartItemView alloc] initWithFrame:CGRectMake(0, 0, 320, 0)];
        view.infoDict = cartList[k];
        
        [self.cartItemListview addSubview:view];
        [view mas_updateConstraints:^(MASConstraintMaker *make) {
            
            if (!lastOne) {
                make.top.equalTo(self.cartItemListview.mas_top);
            }
            else {
                make.top.equalTo(lastOne.mas_bottom);
            }
            
            make.leading.equalTo(self.cartItemListview.mas_leading);
            make.trailing.equalTo(self.cartItemListview.mas_trailing);
            make.height.equalTo(@130);
        }];
        
        lastOne = view;
    }
    
    if (lastOne) {
        [lastOne mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.cartItemListview.mas_bottom);
        }];
    }
    
    if (!mainScrollView) {
        mainScrollView = [[UIScrollView alloc] init];
        mainScrollView.bounces = NO;
        
        [mainScrollView addSubview:baseView];
        [baseView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(mainScrollView);
        }];
    }
    
    {
        [baseView layoutIfNeeded];
        
        UIBezierPath *maskPath =
        [UIBezierPath bezierPathWithRoundedRect:baseView.bounds
                              byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight
                                    cornerRadii:CGSizeMake(10.0, 10.0)];
        
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = baseView.bounds;
        maskLayer.path = maskPath.CGPath;
        baseView.layer.mask = maskLayer;
    }
    
    float scrollViewHeight =
    MIN([UIScreen mainScreen].bounds.size.height - 50, baseView.frame.size.height);
    
    [mainScrollView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(baseView.mas_width);
        make.height.equalTo(@(scrollViewHeight));
    }];
    
    if (backButton.enabled == NO) {
        [self showBackButton:^{
            [self sendViewToCenter:mainScrollView];
        }];
    }
}

- (IBAction)confirmAction:(id)sender
{
    NSArray *cartList = self.infoDict[@"CartList"];
    
    NSString *errorMsg = @"";
    for (NSInteger k = 0; k < cartList.count; k++) {
        NSDictionary *cartItem = cartList[k];
        NSString *statusString = [ALCartItemCell statusString:cartItem];
        if (statusString.length > 0) {
            errorMsg = statusString;
            break;
        }
    }
    
    if (errorMsg.length > 0) {
        YKSimpleAlert(ALLocalizedString(@"Those items fail to checkout", nil));
        return;
    }
    
    [self.onlinePaymentViewController confirmToPayAction:sender];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
