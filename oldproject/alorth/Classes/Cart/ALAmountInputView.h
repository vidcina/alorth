//
//  ALAmountInputView.h
//  alorth
//
//  Created by w91379137 on 2016/2/2.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import <UIKit/UIKit.h>

#pragma mark - delegate
@class ALAmountInputView;
@protocol ALAmountInputViewDelegate <NSObject>

- (void)subviewDidBeginEditing:(ALAmountInputView *)amountInputView
                FirstResponder:(id)firstResponder;

- (void)subviewDidEndEditing:(ALAmountInputView *)amountInputView
              FirstResponder:(id)firstResponder;

- (void)valueDidChage:(ALAmountInputView *)amountInputView;

@end

IB_DESIGNABLE
@interface ALAmountInputView : PDSIBDesignABLEView
<UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet id<ALAmountInputViewDelegate> delegate;
@property (nonatomic) NSInteger amount;

@property (nonatomic, strong) IBOutlet UIButton *addAmountButton;
@property (nonatomic, strong) IBOutlet UITextField *amountTextField;
@property (nonatomic, strong) IBOutlet UIButton *minusAmountButton;

@end
