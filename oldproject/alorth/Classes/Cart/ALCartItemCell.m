//
//  ALCartItemCell.m
//  alorth
//
//  Created by w91379137 on 2016/2/1.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALCartItemCell.h"

@implementation ALCartItemCell

- (CGFloat)performWithInfoDict:(NSDictionary *)infoDict
{
    if ([infoDict isKindOfClass:[NSDictionary class]]) {
        self.infoDict = infoDict;
        
        [self.productImageView imageFromURLString:infoDict.safe[@"ProductImage"]];
        self.productNameLabel.text = [ALCartItemCell productName:infoDict];
        self.productAmountLabel.text = [NSString stringWithFormat:@"x %@",infoDict.safe[@"Amount"]];
        self.productSinglePriceLabel.text = [NSString stringWithFormat:@"%.2f %@",
                                             [infoDict.safe[@"ProductPrice"] floatValue] / [infoDict.safe[@"Amount"] intValue],
                                             infoDict.safe[@"ProductCurrency"]];
        if (self.withoutShippingFee) {
            self.totalPriceLabel.localText =
            [NSString stringWithFormat:@"$Total$ : %.2f %@",
             [infoDict.safe[@"ProductPrice"] floatValue], //只要顯示總價(不含運費)
             infoDict.safe[@"ProductCurrency"]];
            
        }
        else {
            self.totalPriceLabel.localText =
            [NSString stringWithFormat:@"$Total$ : %.2f %@",
             [infoDict.safe[@"ProductPrice"] floatValue] +
             [infoDict.safe[@"Shipping"] floatValue], //只要顯示總價(含運費)
             infoDict.safe[@"ProductCurrency"]];
            
        }
        
        self.statusLabel.localText = [ALCartItemCell statusString:infoDict];
    }
    return self.mainView.bounds.size.height;
}

#pragma mark - IBAction
- (IBAction)deleteAction:(id)sender
{
    self.cellSelect = ALCartItemCellSelectDelete;
}

- (IBAction)changeAction:(id)sender
{
    self.cellSelect = ALCartItemCellSelectChange;
}

#pragma mark - Setter / Getter
- (void)setCellSelect:(ALCartItemCellSelect)cellSelect
{
    _cellSelect = cellSelect;
    [self tryTableViewSelectCell];
    _cellSelect = ALCartItemCellSelectNone;
}

- (void)setIsInCartItem:(BOOL)isInCartItem
{
    _isInCartItem = isInCartItem;
    if (_isInCartItem) {
        self.changeButton.normalTitle = @"Save for later";
        self.changeButton.backgroundColor = COMMON_GRAY1_COLOR;
    }
    else {
        self.changeButton.normalTitle = @"Move to cart";
        self.changeButton.backgroundColor = COMMON_Yellow_COLOR;
    }
}

#pragma mark - Class Method
+ (NSString *)productName:(NSDictionary *)cartItem
{
//    NSInteger amount = [cartItem.safe[@"Amount"] intValue];
    
//    return amount > 1 ?
//    [NSString stringWithFormat:@"%@ x %ld",cartItem.safe[@"ProductTitle"],(long)amount] :
    return [NSString stringWithFormat:@"%@",cartItem.safe[@"ProductTitle"]];
}

+ (NSString *)statusString:(NSDictionary *)cartItem
{
    NSString *statusString = @"";
    if ([cartItem.safe[@"ProductCurrency"] integerValue] > 0) statusString = @"Inventory shortage";
    return statusString;
}

@end
