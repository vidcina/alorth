//
//  ALCartViewController.m
//  alorth
//
//  Created by w91379137 on 2016/1/31.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALCartViewController.h"
#import "ALCartItemCell.h"

#import "ALCartItemEditViewController.h"
#import "ALCartItemCheckoutListViewController.h"

typedef NS_ENUM(NSUInteger, ALCartViewStatus) {
    ALCartViewStatusInit,
    ALCartViewStatusInCart,
    ALCartViewStatusSaved
};

@interface ALCartViewController ()
<ALCartItemEditViewControllerDelegate>
{
    ALCartItemCell *demoCell;
    NSArray *statusDisplayViewRightConstraints;
    NSArray *checkoutButtonViewHiddenConstraints;
}

@property (nonatomic) ALCartViewStatus status;

@end

@implementation ALCartViewController

#pragma mark - Init
- (void)viewDidLoad
{
    [super viewDidLoad];
    if (self.onlinePaymentVC == nil) {
        self.onlinePaymentVC = [[ALOnlinePaymentViewController alloc] init];
        self.onlinePaymentVC.delegate = self;
        
        [self.inCartCheckoutButtonView addSubview:self.onlinePaymentVC.view];
        [self.onlinePaymentVC.view mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.inCartCheckoutButtonView);
        }];
    }
    
    [self alTableViewSetting:self.mainTableView];
    self.mainTableView.contentInset = UIEdgeInsetsMake(0, 0, 15, 0);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.status == ALCartViewStatusInit) {
        [self adjustConstraints:NO];
        self.status = ALCartViewStatusInCart;
    }
    else {
        //每次進到這個畫面都要清空 之前的資料 重新讀取
        self.inCartCartItemList = nil;
        self.savedCartItemList = nil;
        [self reloadData];
    }
}

#pragma mark - Setter / Getter
- (void)setStatus:(ALCartViewStatus)status
{
    if (_status == status) {
        return;
    }
    _status = status;
    [self reloadData];
    
    [ALNetWorkAPI renewCartA094CartStatus:_status == ALCartViewStatusInCart ? @"1" : @"2"
                          CompletionBlock:^(NSDictionary *responseObject) {
                              //意義不明
                          }];
}

- (void)reloadData
{
    dispatch_group_t checkGroup = dispatch_group_create();
    
    if (self.inCartCartItemList == nil) {
        
        dispatch_group_enter(checkGroup);
        [self addMBProgressHUDWithKey:@"inCartListA081"];
        [ALNetWorkAPI inCartListA081CompletionBlock:^(NSDictionary *responseObject) {
            [self removeMBProgressHUDWithKey:@"inCartListA081"];
            
            //2016/02/03 需要再確認 是否運費連動
            
            if ([ALNetWorkAPI checkSerialNumber:@"A081"
                                 ResponseObject:responseObject]) {
                
                if (responseObject.safe[@"resBody"][@"CartList"]) {
                    self.inCartCartItemList = responseObject[@"resBody"][@"CartList"];
                    self.totalPrice = responseObject.safe[@"resBody"][@"TotalPrice"];
//                    self.totalShipping = responseObject.safe[@"resBody"][@"TotalShipping"];
                }
            }
            else {
                YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A081", responseObject),nil));
            }
            dispatch_group_leave(checkGroup);
        }];
    }
    
    if (self.savedCartItemList == nil) {
        
        dispatch_group_enter(checkGroup);
        [self addMBProgressHUDWithKey:@"savedListA088"];
        [ALNetWorkAPI savedListA088CompletionBlock:^(NSDictionary *responseObject) {
            [self removeMBProgressHUDWithKey:@"savedListA088"];
            
            if ([ALNetWorkAPI checkSerialNumber:@"A088"
                                 ResponseObject:responseObject]) {
                
                if (responseObject.safe[@"resBody"][@"CartList"]) {
                    self.savedCartItemList = responseObject[@"resBody"][@"CartList"];
                }
            }
            else {
                YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A088", responseObject),nil));
            }
            dispatch_group_leave(checkGroup);
        }];
    }
    
    dispatch_group_notify(checkGroup, dispatch_get_main_queue(), ^{
        [self adjustConstraints:YES];
        
        self.inCartListButton.normalTitle =
        [NSString stringWithFormat:@"$In Cart$(%lu)",(unsigned long)self.inCartCartItemList.count];
        
        [self.inCartListButton setTitleColor:
         self.status == ALCartViewStatusInCart ? COMMON_GRAY4_COLOR : COMMON_GRAY2_COLOR
                                    forState:UIControlStateNormal];
        
        self.savedListButton.normalTitle =
        [NSString stringWithFormat:@"$Saved$(%lu)",(unsigned long)self.savedCartItemList.count];
        
        [self.savedListButton setTitleColor:
         self.status == ALCartViewStatusSaved ? COMMON_GRAY4_COLOR : COMMON_GRAY2_COLOR
                                   forState:UIControlStateNormal];
        
        [self.mainTableView reloadData];
    });
}

#pragma mark - Constraint
- (void)adjustConstraints:(BOOL)animated
{
    //似乎兩邊一起改 無法動畫
    [self adjustConstraints_statusDisplayView];
    [self adjustConstraints_inCartCheckoutButtonView];
}

- (void)adjustConstraints_statusDisplayView
{
    BOOL shouldInstall = self.status == ALCartViewStatusSaved;
    
    if (shouldInstall) {
        if (statusDisplayViewRightConstraints == nil) {
            statusDisplayViewRightConstraints =
            [self.statusDisplayView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self.savedListButton.mas_centerX);
            }];
        }
    }
    
    for (MASConstraint *obj in statusDisplayViewRightConstraints) {
        if([obj isKindOfClass:MASConstraint.class]) {
            if (shouldInstall) {
                [obj install];
            }
            else {
                [obj uninstall];
            }
        }
    }
}

- (void)adjustConstraints_inCartCheckoutButtonView
{
    BOOL shouldInstall =
    self.status == ALCartViewStatusSaved ||
    self.inCartCartItemList.count == 0;
    
    self.inCartCheckoutButtonView.hidden = shouldInstall;
    
    if (shouldInstall) {
        if (checkoutButtonViewHiddenConstraints == nil) {
            checkoutButtonViewHiddenConstraints =
            [self.inCartCheckoutButtonView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(@0);
            }];
        }
    }
    
    for (MASConstraint *obj in checkoutButtonViewHiddenConstraints) {
        if([obj isKindOfClass:MASConstraint.class]) {
            if (shouldInstall) {
                [obj install];
            }
            else {
                [obj uninstall];
            }
        }
    }
}

#pragma mark - IBAction
/*
- (IBAction)addFakeData
{
    //A006 能得到 ProductID
    //A007 才能得到 StockID SellerID
    //數目超過時 竟然顯示查無此商品
    
    
     //@"01EE4-S-One size1437987245"
     //@"john-01EE4-S-One size1437987245"
     //@"john"
 
    
    [ALNetWorkAPI cartAddV1ProductA079ProductID:@"976FF-S-EUR39, EUR40, EUR41, EUR42, EUR43, EUR44, EUR45, EUR461454385813"
                                        StockID:@"john-EUR39-0-1454385813"
                                       SellerID:@"john"
                                    OrderAmount:[NSString stringWithFormat:@"%u",arc4random_uniform(3) + 2]
                                CompletionBlock:^(NSDictionary *responseObject) {
                                    NSLog(@"%@",responseObject);
                                    self.inCartCartItemList = nil;
                                    [self reloadData];
                                }];
}

- (IBAction)addFakeData2
{
    [ALNetWorkAPI cartAddV2ProductA080UserID:@"john"
                                    StreamID:@"ad0234829205b9033196ba818f7a872b1454470664"
                                   ProductID:@"976FF-S-EUR39, EUR40, EUR41, EUR42, EUR43, EUR44, EUR45, EUR461454385813"
                                     StockID:@"john-EUR39-0-1454385813"
                                    SellerID:@"john"
                                 OrderAmount:[NSString stringWithFormat:@"%u",arc4random_uniform(3) + 2]
                             CompletionBlock:^(NSDictionary *responseObject) {
                                 NSLog(@"%@",responseObject);
                                 self.inCartCartItemList = nil;
                                 [self reloadData];
                             }];
}
 */

- (IBAction)inCartAction:(id)sender
{
    self.status = ALCartViewStatusInCart;
}

- (IBAction)savedAction:(id)sender
{
    self.status = ALCartViewStatusSaved;
}

#pragma mark - ALOnlinePaymentViewControllerDelegate
- (BOOL)isCustomDoubleCheckView:(ALOnlinePaymentViewController *)onlinePaymentViewController
{
    [self addMBProgressHUDWithKey:@"inCartListA081"];
    [ALNetWorkAPI inCartListA081CompletionBlock:^(NSDictionary *responseObject) {
        [self removeMBProgressHUDWithKey:@"inCartListA081"];
        
        if ([ALNetWorkAPI checkSerialNumber:@"A081"
                             ResponseObject:responseObject]) {
            //跳出自己的確認畫面
            ALCartItemCheckoutListViewController *vc =
            [[ALCartItemCheckoutListViewController alloc] init];
            vc.infoDict = responseObject[@"resBody"];
            
            [[PDSEnvironmentViewController sharedInstance] submitViewController:vc];
            
            vc.onlinePaymentViewController = onlinePaymentViewController;
        }
        else {
            YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A081", responseObject),nil));
        }
    }];
    return YES;
}

- (void)totalPrice:(NSDecimalNumber **)decimalNumber currency:(NSString **)currency
{
    NSDecimalNumber *totalPrice = [NSDecimalNumber decimalNumberWithString:self.totalPrice];
//    NSDecimalNumber *shippingPrice = [NSDecimalNumber decimalNumberWithString:self.totalShipping];
    *decimalNumber = totalPrice;
    *currency = @"USD";
    NSLog(@"totalPrice:%@ currency:%@",*decimalNumber, *currency);
}

- (void)onlinePayAction
{
    if (self.inCartCartItemList.count > 0) {
        
        NSArray *cartItemIDs = [self.inCartCartItemList valueForKey:@"CartItemID"];
        
        [self addMBProgressHUDWithKey:@"checkoutInCartItemA084"];
        [ALNetWorkAPI checkoutInCartItemA084CartItems:cartItemIDs
                                      CompletionBlock:^(NSDictionary *responseObject) {
                                          [self removeMBProgressHUDWithKey:@"checkoutInCartItemA084"];
                                          
                                          if ([ALNetWorkAPI checkSerialNumber:@"A084"
                                                               ResponseObject:responseObject]) {
                                              ALLog(@"ResultMessage %@",responseObject.safe[@"resBody"][@"ResultMessage"]);
                                              
                                              NSInteger buyerIsConnected =
                                              [responseObject.safe[@"resBody"][@"BuyerIsConnected"] integerValue];
                                              
                                              if (buyerIsConnected == 0) {
                                                  //信用卡錯誤
                                                  YKSimpleAlert(ALLocalizedString(@"$Credit card$ $error$, $\n$Try again later.$", nil));
                                                  
                                                  //重讀信用卡
                                                  [[PDSAccountManager sharedManager].stripeInfo detectCreditCardStatusCompletionBlock:^{
                                                      ALLog(@"CreditCard 重新偵測 %ld",(long)[PDSAccountManager sharedManager].stripeInfo.creditCardStatus);
                                                  }];
                                              }
                                              else {
                                                  
                                                  ALEcommerceProduct *product = [[ALEcommerceProduct alloc] init];
                                                  
                                                  product.productID = @"Cart_GroupCheckout";
                                                  product.productTitle = @"Cart_GroupCheckout";
                                                  
                                                  product.price = 0;
                                                  product.currency = @"USD";
                                                  
                                                  [[ALBuyProcessInspectorUnit logProductBuy:ALV3CartBuy
                                                                                    Product:product
                                                                                    DevDict:@{}] sendInspector];YKSimpleAlert(ALLocalizedString(@"Success",nil));
                                                  
                                                  NSArray *sellerList = responseObject[@"resBody"][@"SellerList"];
                                                  //NSMutableArray *successItemArray = [NSMutableArray array];
                                                  NSMutableArray *failureItemArray = [NSMutableArray array];
                                                  
                                                  for (NSInteger k = 0; k < sellerList.count; k++) {
                                                      
                                                      NSDictionary *item = sellerList[k];
                                                      if ([item[@"Msg"] isEqualToString:@"Order success"]) {
                                                          
                                                      }
                                                      else if ([item[@"Msg"] isEqualToString:@"Order failure"]) {
                                                          [failureItemArray addObject:item];
                                                      }
                                                      else {
                                                          ALLog(@"A084 strange case %@",item);
                                                      }
                                                  }
                                                  
                                                  if (failureItemArray.count == 0) {
                                                      //如果全部成功就直接顯示成功
                                                      YKSimpleAlert(ALLocalizedString(@"Success", nil));
                                                  }
                                                  else {
                                                      //有失敗就顯示失敗的項目及數量
                                                      NSMutableString *errorMsg = [NSMutableString string];
                                                      [errorMsg appendString:@"$Those items fail to checkout$"];
                                                      [errorMsg appendString:@"\n"];
                                                      [errorMsg appendString:@",$those will be transformed into save for later list$"];
                                                      [errorMsg appendString:@"\n"];
                                                      [errorMsg appendString:@"\n"];
                                                      
                                                      for (NSInteger k = 0; k < failureItemArray.count; k++) {
                                                          NSDictionary *failItem = failureItemArray[k];
                                                          NSString *cartItemID = failItem[@"CartItemID"];
                                                          
                                                          NSPredicate *predicate =
                                                          [NSPredicate predicateWithFormat:@"%K == %@",@"CartItemID",cartItemID];
                                                          
                                                          NSArray *cartItemArray =
                                                          [self.inCartCartItemList filteredArrayUsingPredicate:predicate];
                                                          
                                                          if (cartItemArray.count == 1) {
                                                              NSDictionary *cartItem = cartItemArray.firstObject;
                                                              
                                                              NSString *productDes =
                                                              [ALCartItemCell productName:cartItem];
                                                              
                                                              [errorMsg appendString:productDes];
                                                              [errorMsg appendString:@"\n"];
                                                          }
                                                          else {
                                                              ALLog(@"A084 failItem \n%@ not find in \n%@",failItem,self.inCartCartItemList);
                                                          }
                                                      }
                                                      
                                                      YKSimpleAlert(ALLocalizedString(errorMsg, nil));
                                                  }
                                              }
                                          }
                                          else {
                                              YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A084", responseObject),nil));
                                          }
                                          
                                          self.inCartCartItemList = nil;
                                          self.savedCartItemList = nil;//可以因為結帳失敗 導致商品移動到 save for later 所以一併要更新
                                          [self reloadData];
                                      }];
    }
    else {
        ALLog(@"購物車結帳數目異常");
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (self.status) {
        case ALCartViewStatusInCart: return self.inCartCartItemList.count;
        case ALCartViewStatusSaved: return self.savedCartItemList.count;
        default:
            break;
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!demoCell) {
        demoCell = [ALCartItemCell cellFromXib];
        [demoCell layoutAtWidth:tableView.contentSize.width];
    }
    return [demoCell performWithInfoDict:nil];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *dataArray = nil;
    if (self.status == ALCartViewStatusInCart) dataArray = self.inCartCartItemList;
    if (self.status == ALCartViewStatusSaved) dataArray = self.savedCartItemList;
    
    ALCartItemCell *cell = [ALCartItemCell cellFromXib];
    
    if (dataArray && dataArray.count > indexPath.row) {
        NSDictionary *data = dataArray[indexPath.row];
        [cell performWithInfoDict:data];
        cell.isInCartItem = self.status == ALCartViewStatusInCart;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ALCartItemCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    //NSLog(@"%lu",(unsigned long)cell.cellSelect);
    
    NSDictionary *infoDict = cell.infoDict;
    
    switch (cell.cellSelect) {
        case ALCartItemCellSelectNone:
        {
            //saved for later 不開編輯畫面
            
            //In Cart 關閉或結帳 都需要 先查核 數目 先call 修改api 再進行動作
            if (self.status == ALCartViewStatusInCart) {
                
                dispatch_group_t waitGroup = dispatch_group_create();
                
                __block NSDictionary *originalInfomationDict = nil;
                __block NSDictionary *stockInfomationDict = nil;
                
                dispatch_group_enter(waitGroup);
                [ALNetWorkAPI detailCartItemA086CartItemID:infoDict.safe[@"CartItemID"]
                                           CompletionBlock:^(NSDictionary *responseObject) {
                                               
                                               if ([ALNetWorkAPI checkSerialNumber:@"A086"
                                                                    ResponseObject:responseObject]) {
                                                   originalInfomationDict = responseObject[@"resBody"];
                                                   //YKSimpleAlert(ALLocalizedString(@"Success", nil));
                                               }
                                               else {
                                                   YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A086", responseObject),nil));
                                               }
                                               
                                               dispatch_group_leave(waitGroup);
                                           }];
                
                //V1 > A007
                if ([infoDict.safe[@"Type"] isEqualToString:@"V1"]) {
                    
                    dispatch_group_enter(waitGroup);
                    [self addMBProgressHUDWithKey:@"getStockListA007"];
                    [ALNetWorkAPI getStockListA007ProductID:infoDict.safe[@"ProdcutID"]
                                            CompletionBlock:^(NSDictionary *responseObject) {
                                                [self removeMBProgressHUDWithKey:@"getStockListA007"];
                                                
                                                if ([ALNetWorkAPI checkSerialNumber:@"A007"
                                                                     ResponseObject:responseObject]) {
                                                    
                                                    NSMutableDictionary *resBody = [responseObject[@"resBody"] mutableCopy];
                                                    {//需要濾掉 只有此賣家的
                                                        NSArray *stockList = resBody[@"StockList"];
                                                        
                                                        NSPredicate *predicate =
                                                        [NSPredicate predicateWithFormat:@"%K == %@", @"SellerID", infoDict.safe[@"ProductOwnerID"]];
                                                        
                                                        resBody[@"StockList"] = [stockList filteredArrayUsingPredicate:predicate];
                                                    }
                                                    stockInfomationDict = resBody;
                                                }
                                                else {
                                                    YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A083", responseObject),nil));
                                                }
                                                dispatch_group_leave(waitGroup);
                                            }];
                }
                
                //V2 > A052
                if ([infoDict.safe[@"Type"] isEqualToString:@"V2"]) {
                    
                    dispatch_group_enter(waitGroup);
                    [self addMBProgressHUDWithKey:@"getStreamProductA052"];
                    [ALNetWorkAPI getStreamProductA052ProductID:infoDict.safe[@"ProdcutID"]
                                                       StreamID:infoDict.safe[@"StreamID"]
                                                CompletionBlock:^(NSDictionary *responseObject) {
                                                    [self removeMBProgressHUDWithKey:@"getStreamProductA052"];
                                                    
                                                    if ([ALNetWorkAPI checkSerialNumber:@"A052"
                                                                         ResponseObject:responseObject]) {
                                                        stockInfomationDict = responseObject[@"resBody"];
                                                    }
                                                    else {
                                                        YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A083", responseObject),nil));
                                                    }
                                                    dispatch_group_leave(waitGroup);
                                                }];
                }
                
                dispatch_group_notify(waitGroup, dispatch_get_main_queue(), ^{
                    
                    if (originalInfomationDict && stockInfomationDict) {
                        ALCartItemEditViewController *vc = [[ALCartItemEditViewController alloc] init];
                        
                        vc.delegate = self;
                        vc.originalDict = [originalInfomationDict copy];
                        vc.infoDict = [infoDict mutableCopy];
                        vc.versionTypeString = infoDict.safe[@"Type"];
                        vc.modelListArray = stockInfomationDict[@"ModelList"];
                        vc.stockListArray = stockInfomationDict[@"StockList"];
                        
                        [[PDSEnvironmentViewController sharedInstance] submitViewController:vc];
                    }
                    else {
                        ALLog(@"original : %@\nstock:%@",originalInfomationDict,stockInfomationDict);
                    }
                });
            }
        }
            break;
        case ALCartItemCellSelectDelete:
        {
            [self addMBProgressHUDWithKey:@"deleteInCartItemA082"];
            [ALNetWorkAPI deleteInCartItemA082CartItemID:infoDict.safe[@"CartItemID"]
                                         CompletionBlock:^(NSDictionary *responseObject) {
                                             [self removeMBProgressHUDWithKey:@"deleteInCartItemA082"];
                                             
                                             if ([ALNetWorkAPI checkSerialNumber:@"A082"
                                                                  ResponseObject:responseObject]) {
                                                 if (self.status == ALCartViewStatusInCart) self.inCartCartItemList = nil;
                                                 if (self.status == ALCartViewStatusSaved) self.savedCartItemList = nil;
                                                 [self reloadData];
                                             }
                                             else {
                                                 YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A082", responseObject),nil));
                                             }
                                             
                                         }];
        }
            break;
        case ALCartItemCellSelectChange:
        {
            if (self.status == ALCartViewStatusInCart) {
                
                [self addMBProgressHUDWithKey:@"changeInCartToSavedA083"];
                [ALNetWorkAPI changeInCartToSavedA083CartItemID:infoDict.safe[@"CartItemID"]
                                                CompletionBlock:^(NSDictionary *responseObject) {
                                                    [self removeMBProgressHUDWithKey:@"changeInCartToSavedA083"];
                                                    
                                                    if ([ALNetWorkAPI checkSerialNumber:@"A083"
                                                                         ResponseObject:responseObject]) {
                                                        self.inCartCartItemList = nil;
                                                        self.savedCartItemList = nil;
                                                        [self reloadData];
                                                    }
                                                    else {
                                                        YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A083", responseObject),nil));
                                                    }
                                                    
                                                }];
            }
            
            if (self.status == ALCartViewStatusSaved) {
                
                [self addMBProgressHUDWithKey:@"changeSavedToInCartA089"];
                [ALNetWorkAPI changeSavedToInCartA089CartItemID:infoDict.safe[@"CartItemID"]
                                                CompletionBlock:^(NSDictionary *responseObject) {
                                                    [self removeMBProgressHUDWithKey:@"changeSavedToInCartA089"];
                                                    
                                                    if ([ALNetWorkAPI checkSerialNumber:@"A089"
                                                                         ResponseObject:responseObject]) {
                                                        //判斷數量無法移動 也無法修改 直接gg
                                                        switch ([responseObject.safe[@"resBody"][@"Status"] integerValue]) {
                                                            case 1:
                                                            case 2:
                                                            case 4:
                                                                YKSimpleAlert(ALLocalizedString(@"Inventory shortage",nil));
                                                                break;
                                                            
                                                            case 3:
                                                                //結果 超過3個 會直接噴 ResultCode 17 該處伺服器沒有實作
                                                                YKSimpleAlert(ALLocalizedString(@"Shopping cart is full",nil));
                                                                break;
                                                                
                                                            default:
                                                                break;
                                                        }
                                                    }
                                                    else {
                                                        YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A089", responseObject),nil));
                                                    }
                                                    
                                                    //不管成功與否 刷新所有數值
                                                    self.inCartCartItemList = nil;
                                                    self.savedCartItemList = nil;
                                                    [self reloadData];
                                                    
                                                }];
            }
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - ALCartItemEditViewControllerDelegate
- (void)cartItemIsChanged
{
    self.inCartCartItemList = nil;
    [self reloadData];
}

@end
