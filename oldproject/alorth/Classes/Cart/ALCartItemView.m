//
//  ALCartItemView.m
//  alorth
//
//  Created by w91379137 on 2016/3/7.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALCartItemView.h"
#import "ALCartItemCell.h"

@implementation ALCartItemView

- (void)setInfoDict:(NSDictionary *)infoDict
{
    _infoDict = infoDict;
    
    [self.cartItemImageView imageFromURLString:maybe(infoDict[@"ProductImage"], NSString)];
    self.cartItemTitleLabel.text = infoDict.safe[@"ProductTitle"];
    
    float allPrice = [infoDict.safe[@"ProductPrice"] floatValue];
    NSInteger amount = [infoDict.safe[@"Amount"] integerValue];
    NSString *currency = infoDict.safe[@"ProductCurrency"];
    
    self.cartItemOnePriceLabel.text =
    [NSString stringWithFormat:@"%.2f %@",allPrice / amount,currency];
    
    self.cartItemAmountLabel.text =
    [NSString stringWithFormat:@"x%d",(int)amount];
    
    self.cartItemAllPriceLabel.text =
    [NSString stringWithFormat:@"%.2f %@",allPrice,currency];
    
    self.statusLabel.localText = [ALCartItemCell statusString:infoDict];
}

@end
