//
//  ALCartViewController.h
//  alorth
//
//  Created by w91379137 on 2016/1/31.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALBasicMainViewController.h"

@interface ALCartViewController : ALBasicMainViewController
<UITableViewDataSource, UITableViewDelegate, ALOnlinePaymentViewControllerDelegate>

@property (nonatomic, strong) NSArray *inCartCartItemList;
@property (nonatomic, strong) NSArray *savedCartItemList;

@property (nonatomic, strong) IBOutlet UIButton *inCartListButton;
@property (nonatomic, strong) IBOutlet UIButton *savedListButton;
@property (nonatomic, strong) IBOutlet UIView *statusDisplayView;

@property (nonatomic, strong) IBOutlet UITableView *mainTableView;
@property (nonatomic, strong) IBOutlet UIView *inCartCheckoutButtonView;
@property (nonatomic, strong) ALOnlinePaymentViewController *onlinePaymentVC;

@property (nonatomic, strong) NSString *totalPrice;
//@property (nonatomic, strong) NSString *totalShipping;

@end
