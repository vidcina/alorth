//
//  ALCartItemEditViewController.m
//  alorth
//
//  Created by w91379137 on 2016/2/1.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALCartItemEditViewController.h"
#import "ALCartItemCell.h"

@interface ALCartItemEditViewController ()
{
    ALCartItemCell *displayCell;
}

@end

@implementation ALCartItemEditViewController

#pragma mark - Init
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.type = ALAlertViewTypeBottom;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    backButton.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
    [baseView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@([UIScreen mainScreen].bounds.size.width - 30));
    }];
    
    if (displayCell == nil) {
        displayCell = [ALCartItemCell cellFromXib];
        displayCell.withoutShippingFee = YES;
        
        [cellView addSubview:displayCell];
        [displayCell mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(cellView);
        }];
    }
    
    [self showInfoDict];
    [self prepareSizeSelectButton];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (backButton.enabled == NO) {
        [self showBackButton:^{
            [self sendViewToCenter:baseView];
        }];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self keyboardReturn];
    [super viewWillDisappear:animated];
}

#pragma mark - Reload
- (void)showInfoDict
{
    [displayCell performWithInfoDict:self.infoDict];
    self.amountInputView.amount = [self.infoDict.safe[@"Amount"] integerValue];
    
    NSInteger stockAmount =
    self.stockDictionary ? [self.stockDictionary[@"Amount"] integerValue] : 0;
    
    self.stockInfoLabel.localText =
    stockAmount > 10 ?
    @"In stock" :
    [NSString stringWithFormat:@"$Only$ %ld $left in stock$",(long)stockAmount];
    
    self.shippingFeeLabel.localText =
    [NSString stringWithFormat:@"$Shipping$ %.2f %@",
     [self.infoDict.safe[@"Shipping"] floatValue],
     self.infoDict.safe[@"ProductCurrency"]];
    
    // 這裡需包含運費了
    //self.totalPriceLabel.text = displayCell.totalPriceLabel.text;
    self.totalPriceLabel.localText =
    [NSString stringWithFormat:@"$Total$ : %.2f %@",
     [self.infoDict.safe[@"ProductPrice"] floatValue] +
     [self.infoDict.safe[@"Shipping"] floatValue], //顯示總價(含運費)
     self.infoDict.safe[@"ProductCurrency"]];;
}

#pragma mark - 類型選擇 顯示
-(void)prepareSizeSelectButton
{
    NSMutableArray *usedArray = [@[@"N",@"Y"] mutableCopy];
    NSMutableArray *sizeArray = [self.modelListArray mutableCopy];
    
    self.usedSelectTypeScrollView.infoObjectArray = usedArray;
    self.sizeSelectTypeScrollView.infoObjectArray = sizeArray;
    
    [self.usedSelectTypeScrollView selectIndex:0];
    for (NSInteger k = 1; k < usedArray.count; k++) {
        if ([self.originalDict.safe[@"IsUsed"] isEqualToString:usedArray[k]]) {
            [self.usedSelectTypeScrollView selectIndex:k];
            break;
        }
    }
    
    [self.sizeSelectTypeScrollView selectIndex:0];
    for (NSInteger k = 1; k < sizeArray.count; k++) {
        if ([self.originalDict.safe[@"Size"] isEqualToString:sizeArray[k]]) {
            [self.sizeSelectTypeScrollView selectIndex:k];
            break;
        }
    }
    
    if ([self.versionTypeString isEqualToString:@"V2"]) {
        //隱藏上方列
        self.usedSelectTypeScrollView.hidden = YES;
        [self.sizeSelectTypeScrollView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.usedSelectTypeScrollView.mas_top);
        }];
    }
}

#pragma mark - ALSelectTypeScrollViewDelegate
-(UIView *)typeViewOfIndex:(NSInteger)index
                infoObject:(NSObject *)infoObject
      selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    if ([infoObject isKindOfClass:[NSString class]]) {
        
        UIButton *aButton = nil;
        if (selectTypeScrollView == self.usedSelectTypeScrollView) {
            if ([(NSString *)infoObject isEqualToString:@"N"]) {
                aButton = [ALSelectTypeScrollView alSelectButton:@"New"];
            }
            if ([(NSString *)infoObject isEqualToString:@"Y"]) {
                aButton = [ALSelectTypeScrollView alSelectButton:@"Used"];
            }
        }
        else {
            aButton = [ALSelectTypeScrollView alSelectButton:(NSString *)infoObject];
        }
        
        [aButton addTarget:selectTypeScrollView
                    action:@selector(selectView:)
          forControlEvents:UIControlEventTouchUpInside];
        
        if (selectTypeScrollView == _usedSelectTypeScrollView) {
            [_usedSelectTypeScrollView addSubview:aButton]; //不先加上去 無法執行下一步
            [aButton mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(_usedSelectTypeScrollView.mas_width).multipliedBy(0.5).offset(-1);
            }];
        }
        else {
            CGSize titleSize =
            [ALSelectTypeScrollView alSelectButtonSize:(NSString *)infoObject];
            
            [aButton mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(@(titleSize.width + 40));
            }];
        }
        return aButton;
    }
    else {
        return [[UIView alloc] initWithFrame:CGRectZero];
    }
}

-(void)didSelectIndex:(NSInteger)index
           infoObject:(NSObject *)infoObject
 selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    if (selectTypeScrollView == _usedSelectTypeScrollView) {
        self.selectedIsUsed = (NSString *)infoObject;
        [_sizeSelectTypeScrollView reloadSubView];
    }
    else {
        self.selectedSizeString = (NSString *)infoObject;
    }
    [self didSelectProductSizeType];
}

-(void)adjustSelectView:(UIView *)view
             infoObject:(NSObject *)infoObject
               selected:(BOOL)selected
   selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    UIButton *button = (UIButton *)view;
    [ALSelectTypeScrollView alSelectButton:(UIButton *)view Select:selected];
    
    if (selectTypeScrollView == _sizeSelectTypeScrollView) {
        BOOL isAnyFit = NO;
        
        NSArray *stockList = self.stockListArray;
        for (int t = 0 ; t < stockList.count; t++) {
            NSDictionary *aProduct = stockList[t];
            
            if ([aProduct[apiKeyIsUsed] isEqualToString:self.selectedIsUsed] &&
                [aProduct[apiKeySize] isEqualToString:(NSString *)infoObject]) {
                
                isAnyFit = YES;
                break;
            }
        }
        if (isAnyFit == NO) {
            button.enabled = NO;
            button.alpha = 0.5;
        }
        else {
            button.enabled = YES;
            button.alpha = 1;
        }
    }
}

#pragma mark - 顯示符合商品
-(void)didSelectProductSizeType
{
    self.stockDictionary = nil;
    NSArray *stockList = self.stockListArray;
    
    if ([stockList isKindOfClass:[NSArray class]]) {
        for (int k = 0; k < stockList.count; k++) {
            NSDictionary *aProduct = stockList[k];
            
            BOOL isFit = YES;
            if (![aProduct[apiKeyIsUsed] isEqualToString:self.selectedIsUsed]) {
                isFit = NO;
            }
            if (![aProduct[apiKeySize] isEqualToString:self.selectedSizeString]) {
                isFit = NO;
            }
            if (isFit) {
                //這邊只有這個賣家
                self.stockDictionary = aProduct;
                break;
            }
        }
    }
    
    [self valueDidChage:self.amountInputView];
}

#pragma mark - Keyboard Auto System
- (void)keyboardWillChange:(NSNotification *)notification
{
    //記錄現在位置
    keyboardNotification = notification;
    [self checkSubview];
}

-(IBAction)keyboardReturn
{
    if ([editFirstResponder respondsToSelector:@selector(resignFirstResponder)]) {
        [editFirstResponder performSelector:@selector(resignFirstResponder) withObject:nil];
        //WNLog(@"%@ 收鍵盤",obj);
    }
}

- (void)setEditFirstResponderView:(UIView *)editFirstResponderView
{
    [self checkSubview];
}

- (void)checkSubview
{
    //http://stackoverflow.com/questions/26213681/ios-8-keyboard-hides-my-textview
    
    if (keyboardNotification == nil) {
        return;
    }
    
    CGRect keyboardEndFrame =
    [[[keyboardNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    UIViewAnimationCurve animationCurve =
    [[[keyboardNotification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    NSTimeInterval animationDuration =
    [[[keyboardNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] integerValue];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    float keyboardDispalyHeight = [UIScreen mainScreen].bounds.size.height - keyboardEndFrame.origin.y;
    
    self.view.transform =
    CGAffineTransformTranslate(CGAffineTransformIdentity, 0, -keyboardDispalyHeight);
    
    [UIView commitAnimations];
}

#pragma mark - ALAmountInputViewDelegate
- (void)subviewDidBeginEditing:(ALAmountInputView *)amountInputView
                FirstResponder:(id)firstResponder
{
    self.editFirstResponderView = amountInputView;
    editFirstResponder = firstResponder;
}

- (void)subviewDidEndEditing:(ALAmountInputView *)amountInputView
              FirstResponder:(id)firstResponder
{
    if (self.editFirstResponderView == amountInputView) self.editFirstResponderView = nil;
    if (editFirstResponder == firstResponder) editFirstResponder = nil;
}

- (void)valueDidChage:(ALAmountInputView *)amountInputView
{
    if (self.stockDictionary) {
        
        if (amountInputView.amount > [self.stockDictionary[@"Amount"] intValue]) {
            amountInputView.amount = [self.stockDictionary[@"Amount"] intValue];
            
            NSString *errMSG = [NSString stringWithFormat:@"This seller has only %@ of these available. To see if more are available from another seller, go to the product detail page.",self.stockDictionary[@"Amount"]];
            YKSimpleAlert(errMSG);
            return;
        }
        
        self.infoDict[@"Amount"] = [NSString stringWithFormat:@"%ld",(long)amountInputView.amount];
        self.infoDict[@"Currency"] = self.stockDictionary[@"Currency"];
        self.infoDict[@"ProductPrice"] =
        [NSString stringWithFormat:@"%.2f",
         amountInputView.amount * [self.stockDictionary[@"Price"] floatValue]];
        self.infoDict[@"Shipping"]  = self.stockDictionary[@"TransPrice"];
    }
    else {
        self.infoDict[@"ProductPrice"] = @"";
        self.infoDict[@"Currency"] = @"";
        self.infoDict[@"Shipping"] = @"";
    }
    
    [self showInfoDict];
}

#pragma mark -
- (IBAction)backAction:(id)sender
{
    [self dismiss];
}

- (IBAction)saveAction:(id)sender
{
    if (self.stockDictionary == nil) {
        ALLog(@"無效選擇");
        [self dismiss];
        return;
    }
    
    if ([self isSameOriginalDict]) {
        [self dismiss];
    }
    else {
        [self addMBProgressHUDWithKey:@"changeCartItemInfomationA087"];
        [ALNetWorkAPI changeCartItemInfomationA087CartItemID:self.infoDict.safe[@"CartItemID"]
                                                      IsUsed:self.stockDictionary.safe[@"IsUsed"]
                                                        Size:self.stockDictionary.safe[@"Size"]
                                                      Amount:self.infoDict.safe[@"Amount"]
                                             CompletionBlock:^(NSDictionary *responseObject) {
                                                 [self removeMBProgressHUDWithKey:@"changeCartItemInfomationA087"];
                                                 
                                                 if ([ALNetWorkAPI checkSerialNumber:@"A087"
                                                                      ResponseObject:responseObject]) {
                                                     [self.delegate cartItemIsChanged];
                                                 }
                                                 else {
                                                     ALLog(@"錯誤%@",responseObject);
                                                     YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A087", responseObject),nil));
                                                 }
                                                 [self dismiss];
                                             }];
    }
}
    
/*
- (IBAction)buyAction:(id)sender
{
    if (self.stockDictionary == nil) {
        ALLog(@"無效選擇");
        return;
    }
    
    dispatch_group_t waitGroup = dispatch_group_create();
    __block BOOL isRenew = YES;
    
    if (![self isSameOriginalDict]) {
        dispatch_group_enter(waitGroup);
        [self addMBProgressHUDWithKey:@"changeCartItemInfomationA087"];
        [ALNetWorkAPI changeCartItemInfomationA087CartItemID:self.infoDict.safe[@"CartItemID"]
                                                      IsUsed:self.stockDictionary.safe[@"IsUsed"]
                                                        Size:self.stockDictionary.safe[@"Size"]
                                                      Amount:self.infoDict.safe[@"Amount"]
                                             CompletionBlock:^(NSDictionary *responseObject) {
                                                 [self removeMBProgressHUDWithKey:@"changeCartItemInfomationA087"];
                                                 
                                                 if ([ALNetWorkAPI checkSerialNumber:@"A087"
                                                                      ResponseObject:responseObject]) {
                                                     ALLog(@"更新完成");
                                                 }
                                                 else {
                                                     isRenew = NO;
                                                     YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A087", responseObject),nil));
                                                 }
                                                 dispatch_group_leave(waitGroup);
                                             }];
    }
    
    dispatch_group_notify(waitGroup, dispatch_get_main_queue(), ^{
        if (isRenew) {
            [self addMBProgressHUDWithKey:@"checkoutCartItemA085"];
            [ALNetWorkAPI checkoutCartItemA085CartItemID:self.infoDict.safe[@"CartItemID"]
                                         CompletionBlock:^(NSDictionary *responseObject) {
                                             [self removeMBProgressHUDWithKey:@"checkoutCartItemA085"];
                                             
                                             if ([ALNetWorkAPI checkSerialNumber:@"A085"
                                                                  ResponseObject:responseObject]) {
                                                 [self.delegate cartItemIsChanged];
                                                 [self dismiss];
                                             }
                                             else {
                                                 YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A085", responseObject),nil));
                                             }
                                         }];
        }
    });
}
 */

- (BOOL)isSameOriginalDict
{
    //依照api 比較以下
    //IsUesd
    //Size
    //Amount
    
    BOOL isSame = YES;
    
    if (isSame &&
        !(self.originalDict.safe[@"IsUsed"] &&
          self.stockDictionary.safe[@"IsUsed"] &&
          [self.originalDict.safe[@"IsUsed"] isEqualToString:self.stockDictionary.safe[@"IsUsed"]])) {
            isSame = NO;
            NSLog(@"IsUesd");
        }
    
    if (isSame &&
        !(self.originalDict.safe[@"Size"] &&
          self.stockDictionary.safe[@"Size"] &&
          [self.originalDict.safe[@"Size"] isEqualToString:self.stockDictionary.safe[@"Size"]])) {
            isSame = NO;
            NSLog(@"Size");
        }
    
    if (isSame &&
        !(self.originalDict.safe[@"Amount"] &&
          self.infoDict.safe[@"Amount"] &&
          [self.originalDict.safe[@"Amount"] integerValue] == [self.infoDict.safe[@"Amount"] integerValue])) {
            isSame = NO;
            NSLog(@"Amount");
        }
    
    return isSame;
}

- (void)dismiss
{
    if (backButton.enabled == YES) {
        backButton.enabled = NO;
        [self sendViewToCenter:nil];
        [UIView animateWithDuration:ALAlertDuration
                              delay:0.0
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             backButton.alpha = 0;
                         } completion:^(BOOL finished) {
                             [self.navigationController popViewControllerAnimated:YES];
                         }];
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
