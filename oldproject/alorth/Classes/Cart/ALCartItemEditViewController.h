//
//  ALCartItemEditViewController.h
//  alorth
//
//  Created by w91379137 on 2016/2/1.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALAlertViewController.h"
#import "ALAmountInputView.h"

@protocol ALCartItemEditViewControllerDelegate <NSObject>

- (void)cartItemIsChanged;

@end

@interface ALCartItemEditViewController : ALAlertViewController
<ALAmountInputViewDelegate,
ALSelectTypeScrollViewDelegate>
{
    NSNotification *keyboardNotification;
    id editFirstResponder;
    
    IBOutlet UIView *baseView;
    IBOutlet UIView *cellView;
}

@property(nonatomic, weak) UIView *editFirstResponderView;

@property(nonatomic, weak) id<ALCartItemEditViewControllerDelegate> delegate;
@property(nonatomic, strong) NSDictionary *originalDict;    //初始的值
@property(nonatomic, strong) NSMutableDictionary *infoDict; //修改後的值 包含 數量 單價 運費
@property(nonatomic, strong) NSDictionary *stockDictionary; //修改後的值 包含 單價 運費 新舊 尺寸

@property(nonatomic, strong) NSString *versionTypeString;
@property(nonatomic, strong) NSArray *modelListArray;
@property(nonatomic, strong) NSArray *stockListArray;

//新舊樣式
@property (nonatomic, strong) IBOutlet ALSelectTypeScrollView *usedSelectTypeScrollView;
@property (nonatomic, strong) NSString *selectedIsUsed;

@property (nonatomic, strong) IBOutlet ALSelectTypeScrollView *sizeSelectTypeScrollView;
@property (nonatomic, strong) NSString *selectedSizeString;

//庫存數量
@property(nonatomic, strong) IBOutlet ALAmountInputView *amountInputView;
@property(nonatomic, strong) IBOutlet UILabel *stockInfoLabel;
@property(nonatomic, strong) IBOutlet UILabel *totalPriceLabel;
@property(nonatomic, strong) IBOutlet UILabel *shippingFeeLabel;

@end
