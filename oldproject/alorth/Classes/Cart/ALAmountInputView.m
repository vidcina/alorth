//
//  ALAmountInputView.m
//  alorth
//
//  Created by w91379137 on 2016/2/2.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALAmountInputView.h"

@implementation ALAmountInputView
- (void)dataSetup
{
    [super dataSetup];
    self.amountTextField.delegate = self;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldTextDidBeginEditing:)
                                                 name:UITextFieldTextDidBeginEditingNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldTextDidEndEditing:)
                                                 name:UITextFieldTextDidEndEditingNotification
                                               object:nil];
}

- (void)textFieldTextDidBeginEditing:(NSNotification *)notification
{
    if (notification.object == self.amountTextField) {
        [self.delegate subviewDidBeginEditing:self
                               FirstResponder:notification.object];
    }
}

- (void)textFieldTextDidEndEditing:(NSNotification *)notification
{
    if (notification.object == self.amountTextField) {
        [self.delegate subviewDidEndEditing:self
                             FirstResponder:notification.object];
    }
}

#pragma mark - Setter / Getter
- (void)setAmount:(NSInteger)amount
{
    //先規範 1 ~ 1000
    amount = MIN(amount, 1000);
    amount = MAX(amount, 1);
    
    if (_amount == amount) {
        //數字並未更動
    }
    else {
        _amount = amount;
        [self.delegate valueDidChage:self];
    }
    
    //標準化數字
    NSString *shouldText = [NSString stringWithFormat:@"%ld",(long)_amount];
    if (![shouldText isEqualToString:self.amountTextField.text]) {
        self.amountTextField.text = shouldText;
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField.inputAccessoryView == nil) {
        textField.inputAccessoryView = [textField keyboardReturnToolbar];
    }
    return YES;
}

#pragma mark - IBAction
- (IBAction)textFieldChangeAction:(UITextField *)sender
{
    self.amount = [self.amountTextField.text intValue];
}

- (IBAction)addStockAction:(UIButton *)sender
{
    self.amount = [self.amountTextField.text intValue] + 1;
}

- (IBAction)minusStockAction:(UIButton *)sender
{
    self.amount = [self.amountTextField.text intValue] - 1;
}

@end
