//
//  ALCartItemView.h
//  alorth
//
//  Created by w91379137 on 2016/3/7.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "PDSIBDesignABLEView.h"

@interface ALCartItemView : PDSIBDesignABLEView

@property(nonatomic, strong) NSDictionary *infoDict;

@property (nonatomic, strong) IBOutlet UIImageView *cartItemImageView;
@property (nonatomic, strong) IBOutlet UILabel *cartItemTitleLabel;
@property (nonatomic, strong) IBOutlet UILabel *cartItemOnePriceLabel;
@property (nonatomic, strong) IBOutlet UILabel *cartItemAmountLabel;

@property (nonatomic, strong) IBOutlet UILabel *cartItemAllPriceLabel;
@property (nonatomic, strong) IBOutlet UILabel *statusLabel;

@end
