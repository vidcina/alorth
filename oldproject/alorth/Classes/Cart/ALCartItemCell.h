//
//  ALCartItemCell.h
//  alorth
//
//  Created by w91379137 on 2016/2/1.
//  Copyright © 2016年 w91379137. All rights reserved.
//

typedef NS_ENUM(NSUInteger, ALCartItemCellSelect) {
    ALCartItemCellSelectNone,
    ALCartItemCellSelectDelete,
    ALCartItemCellSelectChange
};

#import "ALCommonTableViewCell.h"

@interface ALCartItemCell : ALCommonTableViewCell
@property (nonatomic) BOOL withoutShippingFee;

@property (nonatomic, readonly) ALCartItemCellSelect cellSelect;
@property (nonatomic, strong) IBOutlet UIView *mainView;

@property (nonatomic, strong) IBOutlet UIImageView *productImageView;
@property (nonatomic, strong) IBOutlet UILabel *productNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *productSinglePriceLabel;
@property (nonatomic, strong) IBOutlet UILabel *productAmountLabel;

@property (nonatomic, strong) IBOutlet UILabel *totalPriceLabel;
@property (nonatomic, strong) IBOutlet UILabel *statusLabel;

@property (nonatomic) BOOL isInCartItem;
@property (nonatomic, strong) IBOutlet UIButton *changeButton;

+ (NSString *)productName:(NSDictionary *)cartItem;
+ (NSString *)statusString:(NSDictionary *)cartItem;

@end
