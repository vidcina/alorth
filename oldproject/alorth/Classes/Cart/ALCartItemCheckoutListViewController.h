//
//  ALCartItemCheckoutListViewController.h
//  alorth
//
//  Created by w91379137 on 2016/3/5.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALAlertViewController.h"

@interface ALCartItemCheckoutListViewController : ALAlertViewController
{
    UIScrollView *mainScrollView;
    IBOutlet UIView *baseView;
}

@property(nonatomic, strong) NSDictionary *infoDict;

@property(nonatomic, strong) IBOutlet UIView *cartItemListview;

@property(nonatomic, strong) IBOutlet UILabel *itemTotalPriceLabel;
@property(nonatomic, strong) IBOutlet UILabel *shippingFeeLabel;
@property(nonatomic, strong) IBOutlet UILabel *taxLabel;
@property(nonatomic, strong) IBOutlet UILabel *orderTotalPriceLabel;

@property(nonatomic, weak) ALOnlinePaymentViewController *onlinePaymentViewController;

@end
