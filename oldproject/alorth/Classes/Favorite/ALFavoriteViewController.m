//
//  ALFavoriteViewController.m
//  alorth
//
//  Created by w91379137 on 2015/11/19.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALFavoriteViewController.h"
#import "ALGoodsSearchViewController.h"

@interface ALFavoriteViewController ()
{
    ALGoodsSearchViewController *goodsSearchVC;
}

@end

@implementation ALFavoriteViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (goodsSearchVC == nil) {
        goodsSearchVC = [[ALGoodsSearchViewController alloc] init];
        goodsSearchVC.delegateVC = self;
        goodsSearchVC.selectSortArray =
        @[@(ALGoodsSearchViewControllerStatusBestSell),
          @(ALGoodsSearchViewControllerStatusNewest)];
        ;
        [self addChildViewController:goodsSearchVC];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (goodsSearchVC.view.superview == nil) {
        [self.searchResultContainerView addSubview:goodsSearchVC.view];
        [goodsSearchVC didMoveToParentViewController:self];
        [goodsSearchVC.view mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(goodsSearchVC.view.superview);
        }];
    }
    
    if (goodsSearchVC.listStatus == ALGoodsSearchViewControllerStatusInit) {
        goodsSearchVC.listStatus = ALGoodsSearchViewControllerStatusBestSell;
        goodsSearchVC.shouldAutoSearch = YES;
    }
    else {
        [goodsSearchVC controlPlayCell:YES];
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    [goodsSearchVC controlPlayCell:NO];
    [super viewDidDisappear:animated];
}

@end
