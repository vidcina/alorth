//
//  ALFavoriteViewController.h
//  alorth
//
//  Created by w91379137 on 2015/11/19.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALBasicMainViewController.h"

@interface ALFavoriteViewController : ALBasicMainViewController

@property (nonatomic, strong) IBOutlet UIView *searchResultContainerView;

@end
