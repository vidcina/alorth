//
//  ALDiscoverAnnotationView.m
//  alorth
//
//  Created by w91379137 on 2015/10/12.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALDiscoverAnnotationView.h"
static NSString *bigCircleImagekey = @"ALDiscoverAnnotationView.Unique.BigCircle";
static NSString *smallCircleImagekey = @"ALDiscoverAnnotationView.Unique.SmallCircle";

@implementation ALDiscoverAnnotationView

- (void)generateImage:(NSString *)text
{
    UIImage *circle = nil;
    
    if ([text isEqualToString:@""]) {
        circle = [self createSmallCircleImage];
    }
    else {
        circle = [self createBigCircleImage];
    }
    
    NSMutableDictionary *attributes =
    [@{NSFontAttributeName:[UIFont boldSystemFontOfSize:7 * [UIScreen mainScreen].scale]} mutableCopy];
    [attributes setValue:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    
    CGSize realSize =
    CGSizeMake(circle.size.width, circle.size.height);
    
    CGSize titleSize =
    [text boundingRectWithSize:CGSizeMake(realSize.width, MAXFLOAT)
                        options:NSStringDrawingUsesLineFragmentOrigin
                     attributes:attributes
                        context:nil].size;
    CGPoint point =
    CGPointMake((int)roundf((realSize.width - titleSize.width) / 2),
                (int)roundf((realSize.height - titleSize.height) / 2));

    circle = [circle addString:text
                      withfont:attributes
                       atPoint:point];
    self.image = circle;
}

- (UIImage *)createBigCircleImage
{
    UIImage *bigCircle = [ALImagekeeper tryImage:bigCircleImagekey];
    if (!bigCircle) {
        float r1 = 40;
        float r2 = r1 - 1;
        float r3 = r2 - 5;
        
        bigCircle =
        [UIImage makePureColorImage:CGSizeMake(r1, r1) Color:[UIColor blackColor]];
        bigCircle = [bigCircle createRoundedRadius:r1 / 2];
        
        UIImage *circle2 =
        [UIImage makePureColorImage:CGSizeMake(r2, r2) Color:[UIColor whiteColor]];
        circle2 = [circle2 createRoundedRadius:r2 / 2];
        
        UIImage *circle3 =
        [UIImage makePureColorImage:CGSizeMake(r3, r3) Color:[UIColor colorWithRed:0.9
                                                                             green:0.1
                                                                              blue:0.1
                                                                             alpha:1]];
        circle3 = [circle3 createRoundedRadius:r3 / 2];
        
        bigCircle = [bigCircle addImageCenter:circle2];
        bigCircle = [bigCircle addImageCenter:circle3];
        
        [ALImagekeeper cacheImage:bigCircleImagekey image:bigCircle];
    }
    return bigCircle;
}

- (UIImage *)createSmallCircleImage
{
    UIImage *smallCircle = [ALImagekeeper tryImage:smallCircleImagekey];
    if (!smallCircle) {
        UIImage *bigCircle = [self createBigCircleImage];
        
        float number = 0.7;
        smallCircle =
        [UIImage reSizeImage:bigCircle
                      toSize:CGSizeMake(bigCircle.size.width * number, bigCircle.size.height * number)];
        [ALImagekeeper cacheImage:smallCircleImagekey image:smallCircle];
    }
    return smallCircle;
}

@end
