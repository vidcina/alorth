//
//  ALMapPOI.h
//  Alorth
//
//  Created by John Hsu on 2015/9/12.
//  Copyright (c) 2015年 Alorth. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#define latitudeKey @"GPSLatitude"
#define longitudeKey @"GPSLongitude"
#define locationKey @"StreamName"

@interface NSDictionary (ALMapPOI) <MKAnnotation>
+(NSArray *)generateFakeDataNear:(CLLocationCoordinate2D)coord;

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, readonly, copy) NSString *title;

@end
