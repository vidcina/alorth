//
//  ALDiscoverAnnotationView.h
//  alorth
//
//  Created by w91379137 on 2015/10/12.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface ALDiscoverAnnotationView : MKAnnotationView

- (void)generateImage:(NSString *)text;

@end
