//
//  ALDiscoverListViewController+PullMore.m
//  alorth
//
//  Created by w91379137 on 2015/10/15.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALDiscoverListViewController+PullMore.h"
static NSString *getStreamListA050 = @"getStreamListA050";

@implementation ALDiscoverListViewController (PullMore)

#pragma mark - scrollView Delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.listFullScreenTableView) {
        [currentTableViewControl scrollViewDidScroll:scrollView];
    }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView == self.listFullScreenTableView) {
        [currentTableViewControl scrollViewDidEndDragging:scrollView willDecelerate:(BOOL)decelerate];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.listFullScreenTableView) {
        [currentTableViewControl scrollViewDidEndDecelerating:(UIScrollView *)scrollView];
    }
}

#pragma mark - ALTableViewControlDelegate
-(void)checkNewData:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    willReplaceWithNew = YES;
    tableViewControl.lastDict[@"StartNum"] = @"0";
    
    ALLog(@"重新整理");
    [self addMBProgressHUDWithKey:getStreamListA050];
    [self goodsListLoadApiSourceParameters:tableViewControl.lastDict
                               FinishBlock:^(BOOL isSuccess) {
                                   [self removeMBProgressHUDWithKey:getStreamListA050];
                               }];
}

-(void)loadMore:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    
    ALLog(@"載入更多:目前第 %@ 頁 %lu 筆資料",
          tableViewControl.lastDict[@"StartNum"],
          (unsigned long)currentTableViewControl.apiDataArray.count);
    
    tableViewControl.lastDict[@"StartNum"] =
    [NSString stringWithFormat:@"%d",[tableViewControl.lastDict[@"StartNum"] intValue] + 1];
    
    [self addMBProgressHUDWithKey:getStreamListA050];
    [self goodsListLoadApiSourceParameters:tableViewControl.lastDict
                               FinishBlock:^(BOOL isSuccess) {
                                   [self removeMBProgressHUDWithKey:getStreamListA050];
                               }];
}

#pragma mark - API
-(void)goodsListLoadApiSourceParameters:(NSDictionary *)parameters
                            FinishBlock:(void (^)(BOOL isSuccess))finishBlockToRun
{
    if (UIInterfaceVersion < 3) {
        [ALNetWorkAPI getStreamListA050StartNum:parameters[@"StartNum"]
                                CompletionBlock:^(NSDictionary *responseObject) {
                                    
                                    BOOL isWiseData =
                                    [self checkListData:responseObject];
                                    
                                    if (!isWiseData) ALLog(@"%@ 錯誤",getStreamListA050);
                                    finishBlockToRun(isWiseData);
                                }];
    }
    else {
        finishBlockToRun(NO);
    }
}

- (BOOL)checkListData:(NSDictionary *)responseObject
{
    BOOL isWiseData = YES;
    NSArray *addArray = nil;
    
    isWiseData =
    isWiseData && [ALNetWorkAPI checkSerialNumber:@"A050" ResponseObject:responseObject];
    
    isWiseData =
    isWiseData && responseObject.safe[@"resBody"][@"StreamList"];
    
    isWiseData =
    isWiseData && [responseObject[@"resBody"][@"StreamList"] isKindOfClass:[NSArray class]];
    
    if (isWiseData) {
        addArray = responseObject[@"resBody"][@"StreamList"];
        
        if (willReplaceWithNew) {
            [currentTableViewControl.apiDataArray removeAllObjects];
        }
        
        [currentTableViewControl.apiDataArray addObjectsFromArray:addArray];
        [self.listFullScreenTableView reloadData];
        
        if (willReplaceWithNew) {
            [self.listFullScreenTableView setContentOffset:CGPointZero animated:YES];
        }
        willReplaceWithNew = NO;
    }
    return isWiseData;
}

@end
