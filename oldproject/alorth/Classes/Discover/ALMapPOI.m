//
//  ALMapPOI.h
//  Alorth
//
//  Created by John Hsu on 2015/9/12.
//  Copyright (c) 2015年 Alorth. All rights reserved.
//

#import "ALMapPOI.h"

@implementation NSDictionary (ALMapPOI)
+(NSArray *)generateFakeDataNear:(CLLocationCoordinate2D)coord
{
    NSMutableArray *array = [NSMutableArray array];
    for (int i=0; i<20; i++) {
        [array addObject:[self generateOneFakeDataNear:coord name:[NSString stringWithFormat:@"測試點%d",i]]];
    }
    return array;
}

+(NSDictionary *)generateOneFakeDataNear:(CLLocationCoordinate2D)coord name:(NSString *)name
{
    CLLocationCoordinate2D coordinate = {INT_MAX,INT_MAX};
    while (!CLLocationCoordinate2DIsValid(coordinate)) {
         coordinate = CLLocationCoordinate2DMake(
       (coord.latitude +  (arc4random_uniform(1000)-500) * 0.00001),
       (coord.longitude + (arc4random_uniform(1000)-500) * 0.00001));
    }
    return @{
             @"description" : [name stringByAppendingString:@"的敘述"],
             @"id" : @(12345 * 100 + arc4random_uniform(100)),
             latitudeKey : @(coordinate.latitude),
             longitudeKey : @(coordinate.longitude),
             locationKey : name,
             };
}

-(CLLocationCoordinate2D)coordinate
{
    return CLLocationCoordinate2DMake([self[latitudeKey] doubleValue], [self[longitudeKey] doubleValue]);
}

-(NSString *)title
{
    return [self objectForKey:locationKey];
}
@end
