//
//  ALDiscoverListViewController.h
//  alorth
//
//  Created by w91379137 on 2015/10/12.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBAnnotationClustering/FBAnnotationClustering.h>
typedef NS_OPTIONS(NSUInteger, ALDiscoverStatus) {
    DiscoverInit        = 0,
    DiscoverList        = 1,
    DiscoverMap         = 2
};

@interface ALDiscoverListViewController : UIViewController
<ALSelectTypeScrollViewDelegate,
UITableViewDataSource,UITableViewDelegate,
MKMapViewDelegate, FBClusteringManagerDelegate>
{
    //清單
    NSMutableArray *annotations;
    BOOL willReplaceWithNew;                    //是否清空之前資料
    ALTableViewControl *currentTableViewControl;
    
    //地圖區
    NSArray *mapAllAnnotations;
    NSMutableArray *mapSelectAnnotations;

}

@property (nonatomic) ALDiscoverStatus discoverStatus;
@property (nonatomic, strong) IBOutlet ALSelectTypeScrollView *selectScrollView;
@property (nonatomic, strong) IBOutlet UIView *containerBaseView;

@property (nonatomic, strong) IBOutlet UIView *listContainerView;
@property (nonatomic, strong) IBOutlet UITableView *listFullScreenTableView;

@property (nonatomic, strong) IBOutlet UIView *mapContainerView;
@property (nonatomic, strong) IBOutlet UIButton *mapBackButton;
@property (nonatomic, strong) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) IBOutlet UITableView *mapSmallTableView;
@property (nonatomic, strong) FBClusteringManager *clusteringManager;

@end
