//
//  ALDiscoverListViewController.m
//  alorth
//
//  Created by w91379137 on 2015/10/12.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALDiscoverListViewController.h"
#import "ALDiscoverListViewController+PullMore.h"

#import "ALStreamBigCell.h"
#import "ALStreamSmallCell.h"

#import "ALDiscoverAnnotationView.h"
#import "ALMapPOI.h"

static const int bigCellCount = INT32_MAX;
static NSString *ALDiscoverListMapIndexKey = @"ALDiscoverListMapIndexKey";

@interface ALDiscoverListViewController ()
{
    ALStreamBigCell *demoBigCell;
    ALStreamSmallCell *demoSmallCell;
}

@end

@implementation ALDiscoverListViewController

#pragma mark - VC Life
- (instancetype)init
{
    self = [super init];
    if (self) {
        annotations = [NSMutableArray array];
        
        mapAllAnnotations = @[];
        mapSelectAnnotations = [NSMutableArray array];
        
        currentTableViewControl = [[ALTableViewControl alloc] init];
        currentTableViewControl.delegate = self;
        currentTableViewControl.apiDataArray = annotations;
        
        self.clusteringManager = [[FBClusteringManager alloc] initWithAnnotations:@[]];
        self.clusteringManager.delegate = self;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.selectScrollView.infoObjectArray == nil) {
        self.selectScrollView.infoObjectArray = [@[@(DiscoverList),@(DiscoverMap)] mutableCopy];
        [self.selectScrollView selectIndex:0];
    }
    
    if (annotations.count == 0) {
        [self checkNewData:currentTableViewControl];
    }
}

#pragma mark - Data Transform
- (NSString *)titleOfALDiscoverStatus:(ALDiscoverStatus)status
{
    switch (status) {
        case DiscoverList: return @"List";
        case DiscoverMap: return @"Map";
        default: return @"";
    }
    return @"";
}

#pragma mark - Getter / Setter
-(void)setDiscoverStatus:(ALDiscoverStatus)discoverStatus
{
    if (self.discoverStatus == discoverStatus) {
        return;
    }
    
    _discoverStatus = discoverStatus;
    
    if (self.discoverStatus == DiscoverList) {
        if (self.listContainerView.superview == nil) {
            [self.containerBaseView addSubview:self.listContainerView];
            [self.listContainerView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(self.containerBaseView);
            }];
        }
    }
    
    if (self.discoverStatus == DiscoverMap) {
        if (self.mapContainerView.superview == nil) {
            [self.containerBaseView addSubview:self.mapContainerView];
            [self.mapContainerView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(self.containerBaseView);
            }];
            
            MKCoordinateRegion target =
            MKCoordinateRegionMake([ALAppDelegate sharedAppDelegate].lastLocation.coordinate, MKCoordinateSpanMake(2, 2));
            
            self.mapView.region = target;
            
            [self mapView:self.mapView regionDidChangeAnimated:YES];
        }
    }
    
    self.listContainerView.hidden = (self.discoverStatus != DiscoverList);
    self.mapContainerView.hidden = (self.discoverStatus != DiscoverMap);
    
    if (UIInterfaceVersion < 3) {
        self.mapBackButton.hidden = YES;
        self.selectScrollView.hidden = NO;
    }
    else {
        self.mapBackButton.hidden = NO;
        self.selectScrollView.hidden = YES;
    }
}

#pragma mark - 
- (IBAction)mapBackAction:(id)sender
{
    [self.selectScrollView selectIndex:0];
}

#pragma mark - ALSelectTypeScrollViewDelegate
-(UIView *)typeViewOfIndex:(NSInteger)index
                infoObject:(NSObject *)infoObject
      selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    if ([infoObject isKindOfClass:[NSNumber class]]) {
        
        ALDiscoverStatus status = [(NSNumber *)infoObject integerValue];
        
        UIButton *aButton =
        [ALSelectTypeScrollView alSelectButton:[self titleOfALDiscoverStatus:status]];
        
        [aButton addTarget:selectTypeScrollView
                    action:@selector(selectView:)
          forControlEvents:UIControlEventTouchUpInside];
        
        [selectTypeScrollView addSubview:aButton]; //不先加上去 無法執行下一步
        [aButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(selectTypeScrollView.mas_width).multipliedBy(0.5).offset(-1);
        }];
        
        return aButton;
    }
    return nil;
}

-(void)didSelectIndex:(NSInteger)index
           infoObject:(NSObject *)infoObject
 selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    if ([infoObject isKindOfClass:[NSNumber class]]) {
        ALDiscoverStatus status = [(NSNumber *)infoObject integerValue];
        self.discoverStatus = status;
    }
}

-(void)adjustSelectView:(UIView *)view
             infoObject:(NSObject *)infoObject
               selected:(BOOL)selected
   selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    UIButton *button = (UIButton *)view;
    if (selected) {
        //選到
        button.selected = YES;
        button.backgroundColor = kALProductDetailDidselectBgColor;
        [button setTitleColor:kALProductDetailDidselectFontColor forState:UIControlStateNormal];
    }
    else {
        //沒
        button.selected = NO;
        button.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5];
        [button setTitleColor:kALProductDetailUnselectFontColor forState:UIControlStateNormal];
    }
}

#pragma mark - UITableViewDataSource Section
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.listFullScreenTableView == tableView) {
        return 2;
    }
    else {
       return 1;
    }
}

#pragma mark - UITableViewDataSource Row
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.listFullScreenTableView == tableView) {
        if (section == 0) {
            return MIN(annotations.count, bigCellCount);
        }
        else {
            if (annotations.count > bigCellCount) {
                return annotations.count - bigCellCount;
            }
            return 0;
        }
    }
    else {
        return mapSelectAnnotations.count;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.listFullScreenTableView == tableView && indexPath.section == 0) {
        if (demoBigCell == nil) {
            demoBigCell = [ALStreamBigCell cellFromXib];
            [demoBigCell layoutAtWidth:tableView.frame.size.width];
        }
        return [demoBigCell performWithInfoDict:nil];
    }
    else {
        if (demoSmallCell == nil) {
            demoSmallCell = [ALStreamSmallCell cellFromXib];
            [demoSmallCell layoutAtWidth:tableView.frame.size.width];
            
            [self.mapSmallTableView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(@(demoSmallCell.frame.size.height * 3));
            }];
        }
        return [demoSmallCell performWithInfoDict:nil];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *infoDict = [self infoDictTableView:tableView IndexPath:indexPath];
    
    if (self.listFullScreenTableView == tableView && indexPath.section == 0) {
        ALStreamBigCell *cell = [ALStreamBigCell cellFromXib];
        [cell performWithInfoDict:infoDict];
        return cell;
    }
    else {
        ALStreamSmallCell *cell = [ALStreamSmallCell cellFromXib];
        [cell performWithInfoDict:infoDict];
        return cell;
    }
    return [self defaultUITableViewCell];
}

-(NSDictionary *)infoDictTableView:(UITableView *)tableView IndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *infoDict = nil;
    if (self.listFullScreenTableView == tableView) {
        if (indexPath.section == 0) {
            infoDict = annotations[indexPath.row];
        }
        else {
            if (annotations.count > bigCellCount) {
                NSInteger k = indexPath.row + bigCellCount;
                infoDict = annotations[k];
            }
        }
    }
    else {
        infoDict = mapSelectAnnotations[indexPath.row];
    }
    return infoDict;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];

    if([cell isKindOfClass:[ALCommonTableViewCell class]]) {
        ALCommonTableViewCell *cellx = (ALCommonTableViewCell *)cell;
        [self playStreamVideo:cellx.infoDict];
    }
}

#pragma mark - MKMapViewDelegate
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    if (self.discoverStatus != DiscoverMap) {
        return;
    }
    
    double latitude_center = mapView.region.center.latitude;
    double longitude_center = mapView.region.center.longitude;
    double latitude_span = mapView.region.span.latitudeDelta;
    double longitude_span = mapView.region.span.longitudeDelta;
    
    double latitude_min     = MAX( -90, floor(latitude_center - latitude_span));
    double latitude_max     = MIN(  90, ceil(latitude_center + latitude_span));
    double longitude_min    = MAX(-179, floor(longitude_center - longitude_span));
    double longitude_max    = MIN( 179, ceil(longitude_center + longitude_span));
    
    //如果超過換日線 就 載全球 但是 伺服器會爆炸
    /*
    if (floor(longitude_center - longitude_span) < -180 ||
        ceil(longitude_center + longitude_span) > 180) {
        longitude_min = -179;
        longitude_max = 179;
    }
     */
    
    {
        //這邊需要一個算法 批次取得地標 x
        //無效了 因為需要照伺服器排序 只能每次重新拉
        NSLog(@"查詢 緯度 %.1f～%.1f 經度 %.1f～%.1f",latitude_min,latitude_max,longitude_min,longitude_max);
        [ALNetWorkAPI getStreamListA051GPSLongitude1:[NSString stringWithFormat:@"%.1f",longitude_min]
                                        GPSLatitude1:[NSString stringWithFormat:@"%.1f",latitude_max]
                                       GPSLongitude2:[NSString stringWithFormat:@"%.1f",longitude_max]
                                        GPSLatitude2:[NSString stringWithFormat:@"%.1f",latitude_min]
                                     CompletionBlock:^(NSDictionary *responseObject) {
                                         
                                         BOOL isWishData = YES;
                                         NSArray *addArray = nil;
                                         
                                         isWishData =
                                         isWishData && [ALNetWorkAPI checkSerialNumber:@"A051" ResponseObject:responseObject];
                                         
                                         isWishData =
                                         isWishData && responseObject.safe[@"resBody"][@"StreamList"];
                                         
                                         isWishData =
                                         isWishData && [responseObject[@"resBody"][@"StreamList"] isKindOfClass:[NSArray class]];
                                         
                                         if (isWishData) {
                                             addArray = responseObject[@"resBody"][@"StreamList"];
                                             
                                             if (addArray.count > 0) {
                                                 
                                                 NSMutableArray *newArray = [NSMutableArray array];
                                                 for (NSInteger k = 0; k < addArray.count; k++) {
                                                     NSMutableDictionary *pos = [addArray[k] mutableCopy];
                                                     pos[ALDiscoverListMapIndexKey] = [NSString stringWithFormat:@"%ld",(long)k];
                                                     [newArray addObject:pos];
                                                 }
                                                 
                                                 mapAllAnnotations = newArray;
                                                 [self addData:mapView];
                                             }
                                             else {
                                                 NSLog(@"getStreamListA051 StreamList 空的");
                                             }
                                         }
                                     }];
    }
}

- (void)addData:(MKMapView *)mapView
{
    [self.clusteringManager removeAnnotations:self.clusteringManager.allAnnotations];
    [self.clusteringManager addAnnotations:mapAllAnnotations];
    
    [[NSOperationQueue new] addOperationWithBlock:^{
        double scale = mapView.bounds.size.width / mapView.visibleMapRect.size.width;
        NSArray *groupAnnotations =
        [self.clusteringManager clusteredAnnotationsWithinMapRect:mapView.visibleMapRect
                                                    withZoomScale:scale];
        [self.clusteringManager displayAnnotations:groupAnnotations
                                         onMapView:mapView];
    }];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    ALDiscoverAnnotationView *annotationView =
    [[ALDiscoverAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@""];
    annotationView.canShowCallout = NO;
    
    if ([annotation isKindOfClass:[FBAnnotationCluster class]]) {
        FBAnnotationCluster *cluster = (FBAnnotationCluster *)annotation;
        cluster.title = [NSString stringWithFormat:@"%lu", (unsigned long)cluster.annotations.count];
        
        if (cluster.annotations.count > 99) {
            [annotationView generateImage:@"99+"];
        }
        else {
            [annotationView generateImage:cluster.title];
        }
    }
    else {
        [annotationView generateImage:@""];
    }
    
    return annotationView;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    [mapSelectAnnotations removeAllObjects];
    
    id annotation = view.annotation;
    if ([annotation isKindOfClass:[FBAnnotationCluster class]]) {
        FBAnnotationCluster *cluster = (FBAnnotationCluster *)annotation;
        
        NSArray *sortArray = [cluster.annotations copy];
        sortArray =
        [sortArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
            
            return [[NSString stringWithFormat:@"%@",obj1[ALDiscoverListMapIndexKey]] compare:
                    [NSString stringWithFormat:@"%@",obj2[ALDiscoverListMapIndexKey]] options:NSNumericSearch];
        }];
        //NSLog(@"%@",sortArray);
        
        [mapSelectAnnotations addObjectsFromArray:sortArray];
    }
    else {
        //NSLog(@"%@",annotation);
        [mapSelectAnnotations addObject:annotation];
    }
    
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         view.alpha = 0.5;
                     } completion:nil];
    [self controlMapSmallTableView];
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
    [mapSelectAnnotations removeAllObjects];
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         view.alpha = 1;
                     } completion:nil];
    [self controlMapSmallTableView];
}

- (void)controlMapSmallTableView
{
    self.mapSmallTableView.hidden = (mapSelectAnnotations.count == 0);
    [self.mapSmallTableView reloadData];
}

#pragma mark - FBClusteringManagerDelegate
- (CGFloat)cellSizeFactorForCoordinator:(FBClusteringManager *)coordinator
{
    return 1.0;
}

@end
