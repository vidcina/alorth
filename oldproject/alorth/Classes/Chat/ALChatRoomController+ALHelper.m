//
//  CDChatRoomController+ALHelper.m
//  alorth
//
//  Created by w91379137 on 2015/9/9.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALChatRoomController+ALHelper.h"

@implementation ALChatRoomController (ALHelper)

-(void)addUpbar
{
    if (chatUpbar == nil) {
        chatUpbar = [[ALChatUpbar alloc] initWithFrame:CGRectZero];
        [self.view addSubview:chatUpbar];
        [chatUpbar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view.mas_top);
            make.leading.equalTo(self.view.mas_leading);
            make.trailing.equalTo(self.view.mas_trailing);
            make.height.equalTo(@(139));
        }];
        
        //第一排
        [chatUpbar.backButton addTarget:self
                                 action:@selector(back)
                       forControlEvents:UIControlEventTouchUpInside];
        
        //第二排
        if ([self.currentConversation.members count] > 2) {
            chatUpbar.nameLabel.text =
            [NSString stringWithFormat:@"(GroupChat)%@", self.currentConversation.name];
        }
        else {
            chatUpbar.nameLabel.text =
            [self.currentConversation otherId];
        }
        
        [chatUpbar.bigRatingButton addTarget:self
                                      action:@selector(showRating)
                            forControlEvents:UIControlEventTouchUpInside];
    }
    
    if (self.otherIdInfoDict == nil) {
        [ALUserInfoCacheManager getLocalUserInfomation:[[self currentConversation] otherId]
                                       completionBlock:^(NSDictionary *responseObject) {
                            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                                self.otherIdInfoDict = responseObject;
                                [self checkOthreInfo];
                            }
                        }];
    }
    else {
        [self checkOthreInfo];
    }
}

-(void)checkOthreInfo
{
    //名稱
    chatUpbar.nameLabel.text =
    [ALFormatter preferredDisplayNameForUserInfomation:self.otherIdInfoDict
                                                UserID:[[self currentConversation] otherId]];
    
    //評分
    [chatUpbar.starView rankStar:5
                             get:[self.otherIdInfoDict.safe[@"resBody"][@"Rating"] floatValue]];
    
    //頭像
    NSString *avatarURLString = self.otherIdInfoDict.safe[@"resBody"][@"HeadImage"];
    [chatUpbar.avatarImageView imageFromURLString:avatarURLString];
    
    //沒有快取資訊
    if (![ALNetWorkAPI isCachedImageFromURL:avatarURLString]) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [ALNetWorkAPI cachedImageFromURL:avatarURLString];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self showOthreInfo];
            });
        });
    }
    else {
        [self showOthreInfo];
    }
}

-(void)showOthreInfo
{
    [self.demoData reloadUserDict:self.otherIdInfoDict Completion:^{
        [self.collectionView reloadData];
    }];
}

#pragma mark - TOP IBAction
-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)showRating
{
    if ([[self currentConversation] otherId]) {
        
        UIViewController *vc = nil;
        if (UIInterfaceVersion == 1) {
            vc =
            [[ALPersonDetailViewController alloc] initWithUserId:[[self currentConversation] otherId]];
        }
        else {
            vc =
            [[ALPersonIntroductionViewController alloc] initWithUserId:[[self currentConversation] otherId]];
        }
        [self.navigationController pushViewController:vc animated:YES];
    }
    else {
        
    }
}

@end
