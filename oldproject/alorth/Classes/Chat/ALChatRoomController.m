//
//  ALChatRoomViewController.m
//  alorth
//
//  Created by John Hsu on 2015/9/2.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALChatRoomController.h"
#import "ALChatRoomController+ALHelper.h"
#import "ALChatRoomDefinition.h"

#import "UIViewController+UIImagePickerControllerDelegate.h"
#import "UIViewController+ImageOnline.h"
#import "NSDate+DKSHelper.h"
@interface ALChatRoomController ()

@end

@implementation ALChatRoomController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.collectionView.contentInset = UIEdgeInsetsMake(0, 0, 49, 0);
    
    /**
     *  You MUST set your senderId and display name
     */
    self.senderId = [ALAppDelegate sharedAppDelegate].userID;
    self.senderDisplayName = [ALAppDelegate sharedAppDelegate].userNameString;
    
    self.inputToolbar.contentView.textView.pasteDelegate = self;
    
    /**
     *  Load up our fake data for the demo
     */
    
    self.demoData = [[AVIMConversationModelData alloc] init];
    self.demoData.currentConversation = self.currentConversation;
    [self.demoData reloadMessageHistoryCompletion:^{
        [self.collectionView reloadData];
        [self scrollToBottomAnimated:NO];
    }];
    
    /**
     *  You can set custom avatar sizes
     */
//    if (![NSUserDefaults incomingAvatarSetting]) {
//        self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
//    }
//    
//    if (![NSUserDefaults outgoingAvatarSetting]) {
//        self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
//    }
    
    self.showLoadEarlierMessagesHeader = YES;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage jsq_defaultTypingIndicatorImage]
                                                                              style:UIBarButtonItemStyleBordered
                                                                             target:self
                                                                             action:@selector(receiveMessagePressed:)];
    
    /**
     *  Register custom menu actions for cells.
     */
//    [JSQMessagesCollectionViewCell registerMenuAction:@selector(customAction:)];
//    [UIMenuController sharedMenuController].menuItems = @[ [[UIMenuItem alloc] initWithTitle:@"Custom Action"
//                                                                                      action:@selector(customAction:)] ];
    
    /**
     *  OPT-IN: allow cells to be deleted
     */
    [JSQMessagesCollectionViewCell registerMenuAction:@selector(delete:)];
    
    /**
     *  Customize your toolbar buttons
     *
     *  self.inputToolbar.contentView.leftBarButtonItem = custom button or nil to remove
     *  self.inputToolbar.contentView.rightBarButtonItem = custom button or nil to remove
     */
    
    /**
     *  Set a maximum height for the input toolbar
     *
     *  self.inputToolbar.maximumHeight = 150;
     */
    
    //接收資料下載後更新
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(notificationReload)
                                                 name:ALDataDidDownloadShouldReloadNotification
                                               object:NULL];
    //接收資料下載後更新
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(messageUpdated:)
                                                 name:kPDSIMDidReceiveIM
                                               object:nil];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    
    self.showLoadEarlierMessagesHeader = NO; //這邊才來得及？
    [self addUpbar];
    [self.view bringSubviewToFront:self.inputToolbar];
    
    [[PDSEnvironmentViewController sharedInstance] hiddenBar];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    /**
     *  Enable/disable springy bubbles, default is NO.
     *  You must set this from `viewDidAppear:`
     *  Note: this feature is mostly stable, but still experimental
     */
    self.collectionView.collectionViewLayout.springinessEnabled = NO;
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Reload
- (void)messageUpdated:(NSNotification *)notification
{
    if ([notification isKindOfClass:[NSNotification class]]) {
        AVIMConversation *conversation = [notification object][@"conversation"];
        if (![conversation.conversationId isEqualToString:self.currentConversation.conversationId]) {
            // not my conversation, ignore it
            return;
        }
    }
    
    [self.demoData reloadMessageHistoryCompletion:^{
        [self.collectionView reloadData];
        [self scrollToBottomAnimated:YES];
    }];
}

-(void)notificationReload
{
    [self.collectionView reloadData];
}

#pragma mark - Testing
- (void)pushMainViewController
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *nc = [sb instantiateInitialViewController];
    [self.navigationController pushViewController:nc.topViewController animated:YES];
}

#pragma mark - Text view delegate
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [super textViewDidBeginEditing:textView];
    if (self.inputToolbar.contentView.textView) {
        if (inputToolbarReturnButton == nil) {
            inputToolbarReturnButton = [UIButton buttonWithType:UIButtonTypeCustom];
            inputToolbarReturnButton.backgroundColor = [UIColor clearColor];
            [inputToolbarReturnButton addTarget:self
                                         action:@selector(inputToolbarResignFirstResponder)
                               forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:inputToolbarReturnButton];
            [inputToolbarReturnButton mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(self.view);
            }];
        }
        [self.view insertSubview:inputToolbarReturnButton belowSubview:self.inputToolbar];
        inputToolbarReturnButton.hidden = NO;
    }
}

-(void)inputToolbarResignFirstResponder
{
    inputToolbarReturnButton.hidden = YES;
    [self.inputToolbar.contentView.textView resignFirstResponder];
}

#pragma mark - Actions
- (void)receiveMessagePressed:(UIBarButtonItem *)sender
{
    
}

#pragma mark - JSQMessagesViewController method overrides
- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSString *)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date
{
    [self finishSendingMessageAnimated:YES];
    [self.currentConversation sendMessage:[AVIMTextMessage messageWithText:text attributes:nil]
                                 callback:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            [self messageUpdated:nil];
//            [self finishSendingMessageAnimated:YES];
        }
        else {
            YKSimpleAlert(ALLocalizedString(@"$Error. Message not sent$. $Please try again later$.", nil));
        }
    }];
}

- (void)didPressAccessoryButton:(UIButton *)sender
{
    [self.inputToolbar.contentView.textView resignFirstResponder];
    [self showChoosePickerControllerType];
}

#pragma mark - UIImagePickerController Delegate
- (void)didPickerControllerReturnImage:(UIImage *)avatarImage
{
    NSData *data = UIImageJPEGRepresentation(avatarImage, 0.5);
    
    //用数据创建文件
    NSString *fileName = [NSString stringWithFormat:@"%@.png",[ALAmazonUploadManager timeStampString]];
    AVFile *file = [AVFile fileWithName:fileName data:data];
    
    //保存文件
    UIView *view = [PDSEnvironmentViewController sharedInstance].view;
    MBProgressHUD *testhub = [[MBProgressHUD alloc] initWithView:view];
    testhub.mode = MBProgressHUDModeDeterminate;
    [view addSubview:testhub];
    [testhub show:YES];
    
    __block typeof(testhub) __weak weakhub = testhub;
    
    [file saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        [testhub removeFromSuperview];
        if(succeeded){
            AVIMImageMessage *message = [AVIMImageMessage messageWithText:@"" file:file attributes:nil];
            [self.currentConversation sendMessage:message callback:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    [self messageUpdated:nil];
                    [self finishSendingMessageAnimated:YES];
                    
                }
                else {
                    YKSimpleAlert(ALLocalizedString(@"$Error. Message not sent$. $Please try again later$.", nil));
                }
            }];
            //            [[CDSessionManager sharedInstance] sendimage:file.url toPeerId:self.otherId];
            //[self log:[NSString stringWithFormat:@"文件已经保存到服务器:[%@] %@",file.objectId,file.url]];
        }
        else {
            YKSimpleAlert(ALLocalizedString(@"Failed. Please send again.", nil));
        }
    } progressBlock:^(NSInteger percentDone) {
        dispatch_async(dispatch_get_main_queue(), ^{
            weakhub.progress = percentDone;
        });
    }];
}

#pragma mark - JSQMessages CollectionView DataSource
- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.demoData.messages objectAtIndex:indexPath.item];
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didDeleteMessageAtIndexPath:(NSIndexPath *)indexPath
{
    [self.demoData.messages removeObjectAtIndex:indexPath.item];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView
             messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  You may return nil here if you do not want bubbles.
     *  In this case, you should set the background color of your collection view cell's textView.
     *
     *  Otherwise, return your previously created bubble image data objects.
     */
    
    JSQMessage *message = [self.demoData.messages objectAtIndex:indexPath.item];
    
    if ([message.senderId isEqualToString:self.senderId]) {
        return self.demoData.outgoingBubbleImageData;
    }
    
    return self.demoData.incomingBubbleImageData;
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView
                    avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Return `nil` here if you do not want avatars.
     *  If you do return `nil`, be sure to do the following in `viewDidLoad`:
     *
     *  self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
     *  self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
     *
     *  It is possible to have only outgoing avatars or only incoming avatars, too.
     */
    
    /**
     *  Return your previously created avatar image data objects.
     *
     *  Note: these the avatars will be sized according to these values:
     *
     *  self.collectionView.collectionViewLayout.incomingAvatarViewSize
     *  self.collectionView.collectionViewLayout.outgoingAvatarViewSize
     *
     *  Override the defaults in `viewDidLoad`
     */
    JSQMessage *message = [self.demoData.messages objectAtIndex:indexPath.item];
    
//    if ([message.senderId isEqualToString:self.senderId]) {
//        if (![NSUserDefaults outgoingAvatarSetting]) {
//            return nil;
//        }
//    }
//    else {
//        if (![NSUserDefaults incomingAvatarSetting]) {
//            return nil;
//        }
//    }
    
    
    return [self.demoData.avatars objectForKey:message.senderId];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
     *  The other label text delegate methods should follow a similar pattern.
     *
     *  Show a timestamp for every 3rd message
     */
    // display date for first row
    if (indexPath.item == 0) {
        JSQMessage *message = [self.demoData.messages objectAtIndex:indexPath.item];
        return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
    }
    JSQMessage *message = [self.demoData.messages objectAtIndex:(indexPath.item - 1)];
    JSQMessage *message2 = [self.demoData.messages objectAtIndex:indexPath.item];
    NSDate *date1 = message.date;
    NSDate *date2 = message2.date;
    
    // display date for different date
    if ([date2 beginningOfDay] != [date1 beginningOfDay]) {
        JSQMessage *message = [self.demoData.messages objectAtIndex:indexPath.item];
        return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
    }

    return nil;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.demoData.messages objectAtIndex:indexPath.item];
    
    /**
     *  iOS7-style sender name labels
     */
    if ([message.senderId isEqualToString:self.senderId]) {
        return nil;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.demoData.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:message.senderId]) {
            return nil;
        }
    }
    
    /**
     *  Don't specify attributes to use the defaults.
     */
    return [[NSAttributedString alloc] initWithString:message.senderDisplayName];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.demoData.messages count];
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Override point for customizing cells
     */
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    /**
     *  Configure almost *anything* on the cell
     *
     *  Text colors, label text, label colors, etc.
     *
     *
     *  DO NOT set `cell.textView.font` !
     *  Instead, you need to set `self.collectionView.collectionViewLayout.messageBubbleFont` to the font you want in `viewDidLoad`
     *
     *
     *  DO NOT manipulate cell layout information!
     *  Instead, override the properties you want on `self.collectionView.collectionViewLayout` from `viewDidLoad`
     */
    
    JSQMessage *msg = [self.demoData.messages objectAtIndex:indexPath.item];
    
    if (!msg.isMediaMessage) {
        //NSLog(@"%@%@",msg.date, msg.text);
        if ([msg.senderId isEqualToString:self.senderId]) {
            cell.textView.textColor = [UIColor blackColor];
        }
        else {
            cell.textView.textColor = [UIColor whiteColor];
        }
        
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor,
                                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
    }
    
    return cell;
}



#pragma mark - UICollectionView Delegate

#pragma mark - Custom menu items

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    if (action == @selector(customAction:)) {
        return YES;
    }
    
    return [super collectionView:collectionView canPerformAction:action forItemAtIndexPath:indexPath withSender:sender];
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    if (action == @selector(customAction:)) {
        [self customAction:sender];
        return;
    }
    
    [super collectionView:collectionView performAction:action forItemAtIndexPath:indexPath withSender:sender];
}

- (void)customAction:(id)sender
{
    NSLog(@"Custom action received! Sender: %@", sender);
    
    [[[UIAlertView alloc] initWithTitle:@"Custom Action"
                                message:nil
                               delegate:nil
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil]
     show];
}



#pragma mark - JSQMessages collection view flow layout delegate

#pragma mark - Adjusting cell label heights

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
     */
    
    /**
     *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
     *  The other label height delegate methods should follow similarly
     *
     *  Show a timestamp for every 3rd message
     */
    // display date for first row
    if (indexPath.item == 0) {
//        JSQMessage *message = [self.demoData.messages objectAtIndex:indexPath.item];
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
    JSQMessage *message = [self.demoData.messages objectAtIndex:(indexPath.item - 1)];
    JSQMessage *message2 = [self.demoData.messages objectAtIndex:indexPath.item];
    NSDate *date1 = message.date;
    NSDate *date2 = message2.date;
    
    // display date for different date
    if ([date2 beginningOfDay] != [date1 beginningOfDay]) {
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
    
    return 0.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  iOS7-style sender name labels
     */
    JSQMessage *currentMessage = [self.demoData.messages objectAtIndex:indexPath.item];
    if ([[currentMessage senderId] isEqualToString:self.senderId]) {
        return 0.0f;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.demoData.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:[currentMessage senderId]]) {
            return 0.0f;
        }
    }
    
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return 0.0f;
}

#pragma mark - Responding to collection view tap events
- (void)collectionView:(JSQMessagesCollectionView *)collectionView
                header:(JSQMessagesLoadEarlierHeaderView *)headerView
didTapLoadEarlierMessagesButton:(UIButton *)sender
{
    NSLog(@"Load earlier messages!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView
 didTapAvatarImageView:(UIImageView *)avatarImageView
           atIndexPath:(NSIndexPath *)indexPath
{
    AVIMMessage *currentMessage = [self.demoData.messages objectAtIndex:indexPath.item];
    if (![currentMessage.clientId isEqualToString:[ALAppDelegate sharedAppDelegate].userID]) {
        [self showRating];
    }
    //NSLog(@"Tapped avatar!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView
didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *msg = [self.demoData.messages objectAtIndex:indexPath.item];
    if (msg.isMediaMessage) {
        UIView *imageView = [msg.media mediaView];
        
        if ([imageView isKindOfClass:[UIImageView class]]) {
            [[PDSEnvironmentViewController sharedInstance] showBigImage:((UIImageView *)imageView).image];
        }
    }
    NSLog(@"Tapped message bubble!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView
 didTapCellAtIndexPath:(NSIndexPath *)indexPath
         touchLocation:(CGPoint)touchLocation
{
    //NSLog(@"Tapped cell at %@!", NSStringFromCGPoint(touchLocation));
}

#pragma mark - JSQMessagesComposerTextViewPasteDelegate methods
- (BOOL)composerTextView:(JSQMessagesComposerTextView *)textView shouldPasteWithSender:(id)sender
{
    if ([UIPasteboard generalPasteboard].image) {
        // If there's an image in the pasteboard, construct a media item with that image and `send` it.
        JSQPhotoMediaItem *item = [[JSQPhotoMediaItem alloc] initWithImage:[UIPasteboard generalPasteboard].image];
        JSQMessage *message = [[JSQMessage alloc] initWithSenderId:self.senderId
                                                 senderDisplayName:self.senderDisplayName
                                                              date:[NSDate date]
                                                             media:item];
        [self.demoData.messages addObject:message];
        [self finishSendingMessage];
        return NO;
    }
    return YES;
}


@end
