//
//  CDChatRoomController.h
//  AVOSChatDemo
//
//  Created by Qihe Bian on 7/28/14.
//  Copyright (c) 2014 AVOS. All rights reserved.
//

#import "CDCommon.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "JSMessagesViewController.h"
#import "CDSessionManager.h"
#import "ALChatRoomViewController.h"
@interface CDChatRoomController : ALChatRoomViewController

@end

@interface CDChatRoomController : JSMessagesViewController
<UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    UIView *backTitleContainerView;
    UIView *userInfoContainerView;
    UILabel *nameLabel;
    UIView *starView;
    
    NSString *otherIdAvatarURLString;
    
    BOOL isTableDown;
    UIImagePickerController *picker;
}

@property(nonatomic, strong) AVIMConversation *currentConversation;

@property (nonatomic, strong) NSDictionary *otherIdInfoDict;

@end
