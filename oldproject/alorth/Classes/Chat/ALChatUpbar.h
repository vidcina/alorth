//
//  ALChatUpbar.h
//  alorth
//
//  Created by w91379137 on 2015/9/10.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "PDSIBDesignABLEView.h"

@interface ALChatUpbar : PDSIBDesignABLEView

@property (nonatomic, strong) IBOutlet UIButton *backButton;

@property (nonatomic, strong) IBOutlet UIImageView *avatarImageView;
@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet UIView *starView;

@property (nonatomic, strong) IBOutlet UIButton *ratingButton;
@property (nonatomic, strong) IBOutlet UIButton *bigRatingButton;


@end
