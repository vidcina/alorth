//
//  ALChatViewController.m
//  alorth
//
//  Created by w91379137 on 2015/6/2.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALChatTableViewController.h"
#import <Intercom/Intercom.h>
@interface ALChatTableViewController ()

@end

@implementation ALChatTableViewController

#pragma mark - VC Life
-(instancetype)init
{
    self = [super init];
    if (self) {
        usersImageURLCacheArray = [NSMutableArray array];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadMessageHistory) name:kPDSIMDidLoadConversationList object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleIM:) name:kPDSIMDidReceiveIM object:nil];
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    [self alTableViewSetting:self.aTableView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self reloadMessageHistory];
}

-(void)reloadMessageHistory
{
    [_aTableView reloadData];
    
    NSMutableArray *noDataUserName = [NSMutableArray array];
    for (int k = 0; k < [[PDSAccountManager sharedManager] conversationArray].count; k++) {
        
        AVIMConversation *chatRoom = [[PDSAccountManager sharedManager] conversationArray][k];
        NSString *otherid = [chatRoom otherId];
        
        BOOL ischeck = NO;
        for (int t = 0; t < usersImageURLCacheArray.count; t++) {
            NSDictionary *record = usersImageURLCacheArray[t];
            
            if ([otherid isEqualToString:record[ChatTableUsersName]]) {
                ischeck = YES;
                break;
            }
        }
        
        if (ischeck == NO) {
            [noDataUserName addObject:otherid];
        }
    }
    
    __block int complete = 0;
    for (int k = 0; k < noDataUserName.count; k++) {
        NSString *userID = noDataUserName[k];
        [ALUserInfoCacheManager getLocalUserInfomation:userID
                                       completionBlock:^(NSDictionary *responseObject) {
                            
                            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                                [usersImageURLCacheArray addObject:@{ChatTableUsersName:userID,
                                                                     ChatTableUsersData:responseObject}];
                            }
                            
                            complete++;
                            if (complete >= noDataUserName.count) {
                                [_aTableView reloadData];
                            }
                        }];
    }
     
}

-(void)handleIM:(NSNotification *)notif
{
    NSDictionary *dict = [notif object];
    AVIMConversation *conversation = dict[@"conversation"];
    AVIMTypedMessage *message = dict[@"message"];
    NSString *notificationConversationID = conversation.conversationId;
    [[_aTableView visibleCells] enumerateObjectsUsingBlock:^(ALChatTableViewCell *cell, NSUInteger idx, BOOL *stop) {
        if ([cell.currentConversation.conversationId isEqualToString:notificationConversationID]) {
            [cell updateLastHistory:message];
        }
    }];

}

#pragma mark - UITableView Datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }
    return [[[PDSAccountManager sharedManager] conversationArray] count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        ALChatTableViewCell *cell = [ALChatTableViewCell cell];
        cell.nameLabel.text = @"Support";
        cell.avatarImageView.hidden = YES;
        cell.chatLabel.text = @"";
        cell.dateLabel.text = @"";
        return cell;
    }
    ALChatTableViewCell *cell = [ALChatTableViewCell cell];
    cell.currentConversation = [[PDSAccountManager sharedManager] conversationArray][indexPath.row];
    [cell updateLastHistory:nil];
    [cell searchUserData:usersImageURLCacheArray];
    return cell;
}

#pragma mark - UITableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        [Intercom presentMessageComposer];
        //[Intercom presentConversationList];
        return;
    }
    ALChatRoomController *controller = [[ALChatRoomController alloc] init];
    controller.currentConversation = [[PDSAccountManager sharedManager] conversationArray][indexPath.row];
    
    [self.navigationController pushViewController:controller animated:YES];
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return NO;
    }
    return YES;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        AVIMConversation *conversation = [[PDSAccountManager sharedManager] conversationArray][indexPath.row];
        
        [self addMBProgressHUDWithKey:@"delete conversation"];
        [conversation quitWithCallback:^(BOOL succeeded2, NSError *error2) {
            [self removeMBProgressHUDWithKey:@"delete conversation"];
            
            if (succeeded2) {
                [conversation fetchWithCallback:^(BOOL succeeded, NSError *error) {
                    [[[PDSAccountManager sharedManager] conversationArray] removeObject:conversation];
                    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                }];
            }
        }];
    }
}
@end
