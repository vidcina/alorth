//
//  ALChatRoomViewController.h
//  alorth
//
//  Created by John Hsu on 2015/9/2.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSQMessages.h"

#import "ALChatUpbar.h"
#import "AVIMConversationModelData.h"

@class DemoMessagesViewController;
@protocol JSQDemoViewControllerDelegate <NSObject>

- (void)didDismissJSQDemoViewController:(DemoMessagesViewController *)vc;

@end

@interface ALChatRoomController : JSQMessagesViewController
<JSQMessagesComposerTextViewPasteDelegate, UIActionSheetDelegate>
{
    ALChatUpbar *chatUpbar;
    UIButton *inputToolbarReturnButton;
}

@property (nonatomic, weak) id<JSQDemoViewControllerDelegate> delegateModal;

@property(nonatomic, strong) AVIMConversation *currentConversation;
@property (nonatomic, strong) AVIMConversationModelData *demoData;
@property (nonatomic, strong) NSDictionary *otherIdInfoDict;

- (void)receiveMessagePressed:(UIBarButtonItem *)sender;

@end
