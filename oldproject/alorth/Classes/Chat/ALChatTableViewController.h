//
//  ALChatViewController.h
//  alorth
//
//  Created by w91379137 on 2015/6/2.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicMainViewController.h"
#import "ALChatTableViewCell.h"

@interface ALChatTableViewController : ALBasicMainViewController
<UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *usersImageURLCacheArray;
}

//@property(nonatomic, strong) NSMutableArray *singleArray;

@property(nonatomic, strong) IBOutlet UITableView *aTableView;

@end
