//
//  ALChatTableViewCell.h
//  alorth
//
//  Created by w91379137 on 2015/7/21.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *ChatTableUsersName = @"ChatTableUsersName";
static NSString *ChatTableUsersData = @"ChatTableUsersData";

@interface ALChatTableViewCell : UITableViewCell

+(ALChatTableViewCell *)cell;

@property(nonatomic, strong) AVIMConversation *currentConversation;
@property(nonatomic, strong) NSDictionary *currentUserData;

@property(nonatomic, strong) IBOutlet UIImageView *avatarImageView;
@property(nonatomic, strong) IBOutlet UILabel *nameLabel;
@property(nonatomic, strong) IBOutlet UILabel *chatLabel;
@property(nonatomic, strong) IBOutlet UILabel *dateLabel;

-(void)searchUserData:(NSArray *)userDataArray;
-(void)updateLastHistory:(AVIMTypedMessage *)aMessage;

@end
