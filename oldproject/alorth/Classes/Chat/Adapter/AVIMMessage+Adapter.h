//
//  AVIMMessage+Adapter.h
//  alorth
//
//  Created by w91379137 on 2015/9/9.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "AVIMMessage.h"

@interface AVIMMessage (Adapter)

- (NSUInteger)messageHash;
- (NSString *)senderId;
- (NSString *)senderDisplayName;

- (NSDate *)date;

@end
