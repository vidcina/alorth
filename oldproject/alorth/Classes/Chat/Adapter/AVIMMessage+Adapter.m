//
//  AVIMMessage+Adapter.m
//  alorth
//
//  Created by w91379137 on 2015/9/9.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "AVIMMessage+Adapter.h"

@implementation AVIMMessage (Adapter)

- (NSUInteger)messageHash
{
    return self.hash;
}

-(NSString *)senderId
{
    return self.clientId;
}

- (NSString *)senderDisplayName
{
    return self.clientId;
}

- (NSDate *)date
{
    return [NSDate dateWithTimeIntervalSince1970:self.sendTimestamp / 1000];
}

@end
