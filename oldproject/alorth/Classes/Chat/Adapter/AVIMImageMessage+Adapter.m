//
//  AVIMImageMessage+Adapter.m
//  alorth
//
//  Created by w91379137 on 2015/9/9.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "AVIMImageMessage+Adapter.h"
#import "ALChatRoomController+ALHelper.h"
#import "ALChatRoomDefinition.h"

#import "NSObject+AssociatedObject.h"
static char isDownloadingKey;

@implementation AVIMImageMessage (Adapter)

-(BOOL)isMediaMessage;
{
    return YES;
}

-(id<JSQMessageMediaData>)media
{
    return self;
}

#pragma mark - downloadImage
-(void)setIsDownloading:(BOOL)isDownloading
{
    [self willChangeValueForKey:NSStringFromSelector(@selector(isDownloading))];
    [self setAssociatedObject:@(isDownloading) forKey:&isDownloadingKey];
    [self didChangeValueForKey:NSStringFromSelector(@selector(isDownloading))];
}

-(BOOL)isDownloading
{
    return [[self associatedObject:&isDownloadingKey] boolValue];
}

-(void)tryDownload
{
    if (self.isDownloading == YES) {
        
    }
    else {
        self.isDownloading = YES;
        
        AVFile *imageFile = [self file];
        NSString *urlString = [imageFile url];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [ALNetWorkAPI cachedImageFromURL:urlString];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:ALDataDidDownloadShouldReloadNotification object:nil];
            });
        });
    }
}

#pragma mark - media
- (UIView *)mediaView
{
    AVFile *imageFile = [self file];
    NSString *urlString = [imageFile url];
    
    UIView *view = [self fakeJSQPhotoMediaItem].mediaView;
    if (![ALNetWorkAPI isCachedImageFromURL:urlString]) {
        UIActivityIndicatorView *aIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        [view addSubview:aIndicator];
        [aIndicator startAnimating];
    }
    return view;
}

- (CGSize)mediaViewDisplaySize
{
    return [self fakeJSQPhotoMediaItem].mediaViewDisplaySize;
}

- (UIView *)mediaPlaceholderView
{
    return [self fakeJSQPhotoMediaItem].mediaPlaceholderView;
}

-(JSQPhotoMediaItem *)fakeJSQPhotoMediaItem
{
    AVFile *imageFile = [self file];
    NSString *urlString = [imageFile url];
    
    if ([ALNetWorkAPI isCachedImageFromURL:urlString]) {
        NSData *imageData = [ALNetWorkAPI cachedImageFromURL:urlString];
        JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:[UIImage imageWithData:imageData]];
        photoItem.appliesMediaViewMaskAsOutgoing = [self.clientId isEqualToString:[ALAppDelegate sharedAppDelegate].userID];
        return photoItem;
    }
    else {
        [self tryDownload];
        JSQPhotoMediaItem *photoItem =
        [[JSQPhotoMediaItem alloc] initWithImage:[UIImage makePureColorImage:CGSizeMake(50, 50) Color:[UIColor blackColor]]];
        return photoItem;
    }
}

- (NSUInteger)mediaHash
{
    return self.hash;
}

@end
