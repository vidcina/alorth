//
//  AVIMConversationModelData.m
//  alorth
//
//  Created by w91379137 on 2015/9/9.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "AVIMConversationModelData.h"
#import "AVIMConversation.h"

@implementation AVIMConversationModelData

#pragma mark - init
- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.messages = [NSMutableArray new];
        
        JSQMessagesBubbleImageFactory *bubbleFactory =
        [[JSQMessagesBubbleImageFactory alloc] init];
        
        self.outgoingBubbleImageData =
        [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
        
        self.incomingBubbleImageData =
        [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleGreenColor]];
        
    }
    return self;
}

#pragma mark - reload
-(void)reloadMessageHistoryCompletion:(void (^)(void))block;
{
    [self.currentConversation queryMessagesWithLimit:1000 callback:^(NSArray *objects, NSError *error) {
        self.messages = [objects mutableCopy];
        block();
    }];
}

-(void)defaultUserSrtting
{
    #warning TODO:是否有頭像預設
    NSMutableDictionary *avatars = [NSMutableDictionary dictionary];
    NSMutableDictionary *users = [NSMutableDictionary dictionary];
    {
        
    }
    {
        
    }
    [JSQMessagesAvatarImageFactory avatarImageWithUserInitials:@"JSQ"
                                               backgroundColor:[UIColor colorWithWhite:0.85f alpha:1.0f]
                                                     textColor:[UIColor colorWithWhite:0.60f alpha:1.0f]
                                                          font:[UIFont systemFontOfSize:14.0f]
                                                      diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
    self.avatars = avatars;
    self.users = users;
}

-(void)reloadUserDict:(NSDictionary *)dict
           Completion:(void (^)(void))block
{
    NSMutableDictionary *avatars = [NSMutableDictionary dictionary];
    NSMutableDictionary *users = [NSMutableDictionary dictionary];
    
    {
        {
            NSString *userName =
            [ALFormatter preferredDisplayNameForUserInfomation:dict UserID:[[self currentConversation] otherId]];
            
            [users setObject:userName forKey:[[self currentConversation] otherId]];
        }
        {
            NSString *avatarURLString = dict[@"resBody"][@"HeadImage"];
            NSData *avatarData = [ALNetWorkAPI cachedImageFromURL:avatarURLString];
            
            UIImage *avatar = [UIImage imageWithData:avatarData];
            if (avatar == nil) {
                avatar = [ALNothingToSeeImage whiteNothingToSeeImage];
            }
            
            JSQMessagesAvatarImage *userImage =
            [JSQMessagesAvatarImageFactory avatarImageWithImage:avatar
                                                       diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
            
            [avatars setObject:userImage forKey:[[self currentConversation] otherId]];
        }
    }
    
    {
        {
            NSString *userName =
            [ALAppDelegate sharedAppDelegate].userNameString;
            
            [users setObject:userName forKey:[ALAppDelegate sharedAppDelegate].userID];
        }
        
        {
            NSString *avatarURLString = [ALAppDelegate sharedAppDelegate].userAvatarURLString;
            NSData *avatarData = [ALNetWorkAPI cachedImageFromURL:avatarURLString];
            
            UIImage *avatar = [UIImage imageWithData:avatarData];
            if (avatar == nil) {
                avatar = [ALNothingToSeeImage whiteNothingToSeeImage];
            }
            
            JSQMessagesAvatarImage *userImage =
            [JSQMessagesAvatarImageFactory avatarImageWithImage:avatar
                                                       diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
            
            [avatars setObject:userImage forKey:[ALAppDelegate sharedAppDelegate].userID];
        }
    }
    self.avatars = avatars;
    self.users = users;
    block();
}

@end
