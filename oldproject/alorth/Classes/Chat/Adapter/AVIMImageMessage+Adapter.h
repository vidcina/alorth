//
//  AVIMImageMessage+Adapter.h
//  alorth
//
//  Created by w91379137 on 2015/9/9.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "AVIMImageMessage.h"

@interface AVIMImageMessage (Adapter)
<JSQMessageMediaData>

-(BOOL)isMediaMessage;

-(id<JSQMessageMediaData>)media;

@property (nonatomic) BOOL isDownloading;

#pragma mark - media
- (UIView *)mediaView;
- (CGSize)mediaViewDisplaySize;
- (UIView *)mediaPlaceholderView;
- (NSUInteger)mediaHash;

@end
