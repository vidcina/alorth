//
//  AVIMConversationModelData.h
//  alorth
//
//  Created by w91379137 on 2015/9/9.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AVIMConversationModelData : NSObject

@property(nonatomic, strong) AVIMConversation *currentConversation;

@property (strong, nonatomic) NSMutableArray *messages;
@property (strong, nonatomic) JSQMessagesBubbleImage *outgoingBubbleImageData;
@property (strong, nonatomic) JSQMessagesBubbleImage *incomingBubbleImageData;
@property (strong, nonatomic) NSDictionary *avatars;
@property (strong, nonatomic) NSDictionary *users;

-(void)reloadMessageHistoryCompletion:(void (^)(void))block;
-(void)reloadUserDict:(NSDictionary *)dict
           Completion:(void (^)(void))block;

@end
