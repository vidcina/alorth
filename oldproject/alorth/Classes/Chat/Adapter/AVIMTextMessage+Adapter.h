//
//  AVIMTextMessage+Adapter.h
//  alorth
//
//  Created by w91379137 on 2015/9/9.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "AVIMTextMessage.h"
#import "JSQMessages.h"

@interface AVIMTextMessage (Adapter)

-(BOOL)isMediaMessage;

@end
