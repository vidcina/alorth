//
//  CDChatRoomController+ALHelper.h
//  alorth
//
//  Created by w91379137 on 2015/9/9.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALChatRoomController.h"

@interface ALChatRoomController (ALHelper)

-(void)addUpbar;
-(void)showRating;

@end
