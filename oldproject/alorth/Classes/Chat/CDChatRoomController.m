//
//  CDChatRoomController.m
//  AVOSChatDemo
//
//  Created by Qihe Bian on 7/28/14.
//  Copyright (c) 2014 AVOS. All rights reserved.
//

#import "CDChatRoomController.h"
#import "CDSessionManager.h"

#import "ALPersonDetailViewController.h"

static NSString *ALChatTextKey = @"message";
static NSString *ALChatImageKey = @"imageURL";

@interface CDChatRoomController ()
<JSMessagesViewDelegate, JSMessagesViewDataSource>
{
    NSMutableArray *_timestampArray;
    NSDate *_lastTime;
}
@property (nonatomic, strong) NSArray *messages;
@end

@implementation CDChatRoomController

#pragma mark - VC Life

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(instancetype)init
{
    if ((self = [super init])) {
        self.hidesBottomBarWhenPushed = YES;
        self.delegate = self;
        self.dataSource = self;
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    if ([self.currentConversation.members count] > 2) {
        self.title = [NSString stringWithFormat:@"(GroupChat)%@", self.currentConversation.name];
    }
    else {
        self.title = [self.currentConversation otherId];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(messageUpdated:)
                                                 name:kPDSIMDidReceiveIM
                                               object:nil];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[PDSEnvironmentViewController sharedEnvironmentManager] hiddenBar];
    
    //第一排
    if (backTitleContainerView == nil) {
        [self reloadMessageHistory];
        
        //返回按鈕
        UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
        back.frame = CGRectMake(0, 0, 0, 64);
        
        [back setTitle:@"聊天" forState:UIControlStateNormal];
        [back setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        back.titleEdgeInsets = UIEdgeInsetsMake(20, 0, 0, 0);
        
        back.backgroundColor = [UIColor colorWithRed:235.0f/255.0f green:234.0f/255.0f blue:236.0f/255.0f alpha:1.0];
        [self.view addSubview:back];
        [back addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        
        [back mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view.mas_top);
            make.leading.equalTo(self.view.mas_leading);
            make.trailing.equalTo(self.view.mas_trailing);
            make.height.equalTo(@(64));
        }];
        backTitleContainerView = back;
        
        //返回圖示
        {
            UIImageView *backImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_black180.png"]];
            [self.view addSubview:backImage];
            [backImage mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(back.mas_top).offset(30);
                make.leading.equalTo(back.mas_leading).offset(5);
                make.width.equalTo(@(34));
                make.height.equalTo(@(24));
            }];
        }
    }
    
    #warning TODO:如果群聊跟單聊 上方列表應該不一樣
    //第二排
    if (userInfoContainerView == nil) {
        //看評價按鈕
        UIButton *rating = [UIButton buttonWithType:UIButtonTypeCustom];
        rating.frame = CGRectMake(0, 0, 0, 64);
        
        [rating setTitle:@"" forState:UIControlStateNormal];
        rating.backgroundColor = [UIColor colorWithWhite:1 alpha:1];
        [self.view addSubview:rating];
        [rating addTarget:self action:@selector(showRating) forControlEvents:UIControlEventTouchUpInside];
        
        [rating mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(backTitleContainerView.mas_bottom);
            make.leading.equalTo(self.view.mas_leading);
            make.trailing.equalTo(self.view.mas_trailing);
            make.height.equalTo(@(64));
        }];
        userInfoContainerView = rating;
        
        //名稱
        {
            nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
            if ([self.currentConversation.members count] > 2) {
                nameLabel.text = [NSString stringWithFormat:@"(GroupChat)%@", self.currentConversation.name];
            }
            else {
                nameLabel.text = [self.currentConversation otherId];
            }
            
            
            [self.view addSubview:nameLabel];
            [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(rating.mas_top);
                make.bottom.equalTo(rating.mas_centerY);
                
                make.leading.equalTo(rating.mas_leading).offset(15);
                make.trailing.equalTo(rating.mas_trailing).offset(-120);
            }];
        }
        
        //星星
        {
            starView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
            [self.view addSubview:starView];
            [starView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(rating.mas_centerY);
                make.bottom.equalTo(rating.mas_bottom);
                
                make.leading.equalTo(rating.mas_leading).offset(15);
                make.width.equalTo(@(110));
            }];
        }
        
        //箭頭
        {
            UIImageView *backImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_black180.png"]];
            backImage.layer.transform = CATransform3DRotate(CATransform3DIdentity, M_PI, 0, 0, 1);
            [self.view addSubview:backImage];
            [backImage mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.equalTo(rating.mas_centerY);
                make.trailing.equalTo(rating.mas_trailing);
                
                make.width.equalTo(@(34));
                make.height.equalTo(@(24));
            }];
        }
        
        //Listing
        {
            UILabel *listingLabel = [[UILabel alloc] initWithFrame:CGRectZero];
            listingLabel.text = NSLocalizedString(@"Listing", nil);
            listingLabel.textAlignment = NSTextAlignmentRight;
            [self.view addSubview:listingLabel];
            
            [listingLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.equalTo(rating.mas_centerY);
                make.trailing.equalTo(rating.mas_trailing).offset(-40);
                
                make.width.equalTo(@(150));
                make.height.equalTo(@(30));
            }];
            
        }
    }
    
    //強制調整畫面
    if (isTableDown != YES) {
        isTableDown = YES;
        
        CGRect rect = self.tableView.frame;
        self.tableView.frame = CGRectMake(rect.origin.x,
                                          backTitleContainerView.frame.size.height + userInfoContainerView.frame.size.height,
                                          rect.size.width,
                                          [UIScreen mainScreen].bounds.size.height -
                                          backTitleContainerView.frame.size.height -
                                          userInfoContainerView.frame.size.height -
                                          self.inputToolBarView.frame.size.height);
        [self scrollToBottomAnimated:NO];
        
        //增加點一下就收回手勢
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAndReturn)];
        [self.tableView addGestureRecognizer:tap];
        
        self.tableView.separatorColor = [UIColor clearColor];
        self.tableView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1];
    }
    
    if (self.otherIdInfoDict == nil) {
        [ALNetWorkAPI getUserInfomation:[[self currentConversation] otherId]
                        completionBlock:^(NSDictionary *responseObject) {
                            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                                self.otherIdInfoDict = responseObject;
                                [self displayOthreInfo];
                            }
                        }];
    }
    else {
        [self displayOthreInfo];
    }
}

-(void)displayOthreInfo
{
    //名稱
    nameLabel.text = [ALFormatter preferredDisplayNameForUserInfomation:self.otherIdInfoDict UserID:[[self currentConversation] otherId]];
    
    //評分
    [starView rankStar:5 get:[self.otherIdInfoDict[@"resBody"][@"Rating"] floatValue]];
    
    //頭像
    otherIdAvatarURLString = self.otherIdInfoDict[@"resBody"][@"HeadImage"];
    
    //沒有快取資訊
    if (![ALNetWorkAPI isCachedImageFromURL:otherIdAvatarURLString]) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [ALNetWorkAPI cachedImageFromURL:otherIdAvatarURLString];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
            });
        });
    }
    else {
        [self.tableView reloadData];
    }
}


#pragma mark - TOP IBAction
-(void)tapAndReturn
{
    [self.inputToolBarView.textView resignFirstResponder];
}

-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)showRating
{
    if ([[self currentConversation] otherId]) {
        ALPersonDetailViewController *vc = [[ALPersonDetailViewController alloc] initWithUserId:[[self currentConversation] otherId]];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else {
        
    }
}

#pragma mark - 聊天室 環境資訊
//自訂的時間分隔線 出現規則
-(void)refreshTimestampArray
{
    NSDate *lastDate = nil;
    NSMutableArray *hasTimestampArray = [NSMutableArray array];
    for (AVIMTypedMessage *message in self.messages) {
        NSTimeInterval timestamp = (message.deliveredTimestamp > 0) ? message.deliveredTimestamp : message.sendTimestamp;
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:(timestamp/1000)];
        if (!lastDate) {
            lastDate = date;
            [hasTimestampArray addObject:[NSNumber numberWithBool:YES]];
        }
        else {
            if ([date timeIntervalSinceDate:lastDate] > 60) {
                [hasTimestampArray addObject:[NSNumber numberWithBool:YES]];
                lastDate = date;
            }
            else {
                [hasTimestampArray addObject:[NSNumber numberWithBool:NO]];
            }
        }
    }
    _timestampArray = hasTimestampArray;
}

#pragma mark - Messages view delegate
//送出文字按鈕
- (void)sendPressed:(UIButton *)sender withText:(NSString *)text
{
    [self.currentConversation sendMessage:[AVIMTextMessage messageWithText:text attributes:nil] callback:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
//            NSLog(@"发送成功！");
            [self refreshTimestampArray];
            [self finishSend];
            [self reloadMessageHistory];
        }
        else {
            YKSimpleAlert(NSLocalizedString(@"Message send failed, please try again later", nil));
        }
    }];
//    if (self.type == CDChatRoomTypeGroup) {
//        if (!self.group.groupId) {
//            return;
//        }
//        [[CDSessionManager sharedInstance] sendMessage:text toGroup:self.group.groupId];
//    }
//    else {
//        [[CDSessionManager sharedInstance] sendMessage:text toPeerId:self.otherId];
//    }
}

//插入圖片按鈕(目前關掉)
//搜尋 kAllowsMedia
-(void)cameraPressed:(id)sender
{
    [self.inputToolBarView.textView resignFirstResponder];
    [self pickerAction];
}

-(void)sendImage:(UIImage *)image
{
    
    
    NSData *data = UIImageJPEGRepresentation(image, 0.5);
    
    //用数据创建文件
    NSString *fileName = [NSString stringWithFormat:@"%@.png",[ALMediaUploadManager timeStampString]];
    AVFile *file = [AVFile fileWithName:fileName data:data];
    
    //保存文件
    UIView *view = [PDSEnvironmentViewController sharedEnvironmentManager].view;
    MBProgressHUD *testhub = [[MBProgressHUD alloc] initWithView:view];
    testhub.mode = MBProgressHUDModeDeterminate;
    [view addSubview:testhub];
    [testhub show:YES];
    
    __block typeof(testhub) __weak weakhub = testhub;
    
    [file saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        [testhub removeFromSuperview];
        if(succeeded){
            AVIMImageMessage *message = [AVIMImageMessage messageWithText:@"" file:file attributes:nil];
            [self.currentConversation sendMessage:message callback:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    [self refreshTimestampArray];
                    [self reloadMessageHistory];
//                    NSLog(@"发送成功！");
                }
                else {
                    YKSimpleAlert(NSLocalizedString(@"Message send failed, please try again later", nil));
                }
            }];
//            [[CDSessionManager sharedInstance] sendimage:file.url toPeerId:self.otherId];
            //[self log:[NSString stringWithFormat:@"文件已经保存到服务器:[%@] %@",file.objectId,file.url]];
        }
        else {
            YKSimpleAlert(NSLocalizedString(@"Send Fail", nil));
        }
    } progressBlock:^(NSInteger percentDone) {
                          dispatch_async(dispatch_get_main_queue(), ^{
                                weakhub.progress = percentDone;
                          });
    }];
}

//泡泡框左右
-(JSBubbleMessageType)messageTypeForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AVIMTypedMessage *message = [self.messages objectAtIndex:indexPath.row];
    NSString *fromid = message.clientId;
    if ([fromid isEqualToString:[[ALAppDelegate sharedAppDelegate] userID]]) {
        return JSBubbleMessageTypeOutgoing;
    }
    return JSBubbleMessageTypeIncoming;
}

//泡泡框的外表
-(JSBubbleMessageStyle)messageStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return JSBubbleMessageStyleSquare;
}

-(JSBubbleMediaType)messageMediaTypeForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AVIMTypedMessage *message = [self.messages objectAtIndex:indexPath.row];
    if ([message isKindOfClass:[AVIMImageMessage class]]) {
        return JSBubbleMediaTypeImage;
    }
    else {
        return JSBubbleMediaTypeText;
    }
}

#pragma mark - 聊天室 環境資訊
- (UIButton *)sendButton
{
    return [UIButton defaultSendButton];
}

//選擇時間條的出現方法
- (JSMessagesViewTimestampPolicy)timestampPolicy
{
    /*
     JSMessagesViewTimestampPolicyAll = 0,
     JSMessagesViewTimestampPolicyAlternating,
     JSMessagesViewTimestampPolicyEveryThree,
     JSMessagesViewTimestampPolicyEveryFive,
     JSMessagesViewTimestampPolicyCustom
     */
    return JSMessagesViewTimestampPolicyCustom;
}

//大頭照出現的規則
- (JSMessagesViewAvatarPolicy)avatarPolicy
{
    /*
     JSMessagesViewAvatarPolicyIncomingOnly = 0,
     JSMessagesViewAvatarPolicyBoth,
     JSMessagesViewAvatarPolicyNone
     */
    return JSMessagesViewAvatarPolicyBoth;
}

//大頭照出現的樣式
- (JSAvatarStyle)avatarStyle
{
    /*
     JSAvatarStyleCircle = 0,
     JSAvatarStyleSquare,
     JSAvatarStyleNone
     */
    return JSAvatarStyleCircle;
}

//輸入框樣式 跟 對話背景
- (JSInputBarStyle)inputBarStyle
{
    /*
     JSInputBarStyleDefault,
     JSInputBarStyleFlat
     */
    return JSInputBarStyleFlat;
}

//  Optional delegate method
//  Required if using `JSMessagesViewTimestampPolicyCustom`
//
- (BOOL)hasTimestampForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [[_timestampArray objectAtIndex:indexPath.row] boolValue];
}

- (BOOL)hasNameForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.currentConversation.members count] > 2) {
        return YES;
    }
    return NO;
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.messages.count;
}

#pragma mark - Messages view data source
- (NSString *)textForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AVIMTypedMessage *message = [self.messages objectAtIndex:indexPath.row];
    return message.text;
}

- (id)dataForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AVIMImageMessage *message = [self.messages objectAtIndex:indexPath.row];
    if (![message isKindOfClass:[AVIMImageMessage class]]) {
        return [UIImage makePureColorImage:CGSizeMake(30, 30) Color:[UIColor blackColor]];
    }
    AVFile *imageFile = [message file];
    NSString *urlString = [imageFile url];
    
    if ([urlString isKindOfClass:[NSString class]] && urlString.length > 0) {
        if ([ALNetWorkAPI isCachedImageFromURL:urlString]) {
            return [UIImage imageWithData:[ALNetWorkAPI cachedImageFromURL:urlString]];
        }
        else {
            //沒有快取資訊
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                [ALNetWorkAPI cachedImageFromURL:urlString];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tableView reloadData];
                });
            });
        }
    }
    return [UIImage makePureColorImage:CGSizeMake(30, 30) Color:[UIColor blackColor]];
}

- (NSDate *)timestampForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AVIMTypedMessage *message = [self.messages objectAtIndex:indexPath.row];
    NSTimeInterval timestamp = (message.deliveredTimestamp > 0) ? message.deliveredTimestamp : message.sendTimestamp;
    return [NSDate dateWithTimeIntervalSince1970:(timestamp/1000)];
}

- (NSString *)nameForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AVIMTypedMessage *message = [self.messages objectAtIndex:indexPath.row];
    return message.clientId;
}

#pragma mark - 對方的
- (UIImage *)avatarImageForIncomingMessage
{
    NSString *urlString = otherIdAvatarURLString;
    if ([ALNetWorkAPI isCachedImageFromURL:urlString]) {
        return [UIImage imageWithData:[ALNetWorkAPI cachedImageFromURL:urlString]];
    }
    return [UIImage makePureColorImage:CGSizeMake(1, 1) Color:[UIColor blackColor]];
}

- (SEL)avatarImageForIncomingMessageAction
{
    return @selector(onInComingAvatarImageClick);
}

- (void)onInComingAvatarImageClick
{
    [self showRating];
    //NSLog(@"__%s__",__func__);
}

#pragma mark - 自己的
- (SEL)avatarImageForOutgoingMessageAction
{
    return @selector(onOutgoingAvatarImageClick);
}

- (void)onOutgoingAvatarImageClick
{
    //NSLog(@"__%s__",__func__);
}

- (UIImage *)avatarImageForOutgoingMessage
{
    NSString *urlString = [ALAppDelegate sharedAppDelegate].userAvatarURLString;
    if ([ALNetWorkAPI isCachedImageFromURL:urlString]) {
        return [UIImage imageWithData:[ALNetWorkAPI cachedImageFromURL:urlString]];
    }
    return [UIImage makePureColorImage:CGSizeMake(1, 1) Color:[UIColor blackColor]];
}

#pragma mark - 更新

- (void)messageUpdated:(NSNotification *)notification
{
    AVIMConversation *conversation = [notification object][@"conversation"];
    if (![conversation.conversationId isEqualToString:self.currentConversation.conversationId]) {
        // not my conversation, ignore it
        return;
    }
    [self reloadMessageHistory];
}

-(void)reloadMessageHistory
{
    [self.currentConversation queryMessagesWithLimit:1000 callback:^(NSArray *objects, NSError *error) {
        self.messages = objects;
        [self refreshTimestampArray];
        [self.tableView reloadData];
        [self scrollToBottomAnimated:YES];
    }];
}

#pragma mark - IBAction_載入相簿
-(IBAction)pickerAction
{
    if (picker == nil) {
        picker = [[UIImagePickerController alloc] init];
        picker.mediaTypes = @[(NSString *)kUTTypeImage];
        picker.delegate = self;
    }
    [self.view addSubview:picker.view];
}

#pragma mark - imagePicker
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self imagePickerRemove];
    //_aCompletionBlock([info objectForKey:UIImagePickerControllerReferenceURL],nil);
    
    ALAssetsLibrary *assetLibrary = [[ALAssetsLibrary alloc] init];
    [assetLibrary assetForURL:[info objectForKey:UIImagePickerControllerReferenceURL]
                  resultBlock:^(ALAsset *asset) {
                      
                      ALAssetRepresentation *rep = [asset defaultRepresentation];
                      Byte *buffer = (Byte*)malloc(rep.size);
                      NSUInteger buffered = [rep getBytes:buffer fromOffset:0.0 length:rep.size error:nil];
                      NSData *dty = [NSData dataWithBytesNoCopy:buffer length:buffered freeWhenDone:NO];
                      UIImage *ss = [UIImage imageWithData:dty];
                      [self sendImage:ss];
                  }
                 failureBlock:^(NSError *error) {
    }];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self imagePickerRemove];
}

-(void)imagePickerRemove
{
    [picker.view removeFromSuperview];
    picker = nil;
}

@end
