//
//  ALChatTableViewCell.m
//  alorth
//
//  Created by w91379137 on 2015/7/21.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALChatTableViewCell.h"

@implementation ALChatTableViewCell

#pragma mark - init
+(ALChatTableViewCell *)cell
{
    ALChatTableViewCell *cell =
    [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class])
                                   owner:nil
                                 options:nil] lastObject];
    cell.nameLabel.numberOfLines = 2;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)updateLastHistory:(AVIMTypedMessage *)aMessage
{
    __block AVIMTypedMessage *message = aMessage;
    void (^updateBlock)(void) = ^{
        NSTimeInterval timestamp = (message.deliveredTimestamp > 0) ? message.deliveredTimestamp : message.sendTimestamp;
        NSDate *lastUpdateDate = [NSDate dateWithTimeIntervalSince1970:(timestamp/1000)];
        self.dateLabel.localText = [ALFormatter localTextIntervalAgoTextForDate:lastUpdateDate];
        
        NSMutableString *localText = [NSMutableString string];
        
        if ([message.clientId isEqualToString:[[ALAppDelegate sharedAppDelegate] userID]]) {
            [localText appendFormat:@"$me$:"];
        }
        else {
            [localText appendFormat:@"$$"];
        }
        
        if ([message isKindOfClass:[AVIMImageMessage class]]) {
            [localText appendFormat:@"($Sent a picture$)"];
        }
        else {
            [localText appendFormat:@"%@",message.text];
        }
        
        self.chatLabel.localText = localText;
    };
    
    if (!message) {
        [self.currentConversation queryMessagesWithLimit:1 callback:^(NSArray *objects, NSError *error) {
            if (![objects count]) {
                self.dateLabel.localText = @"";
                self.chatLabel.localText = @"($No Chat History$)";
                return;
            }
            message = objects[0];
            updateBlock();
        }];
    }
    else {
        updateBlock();
    }
    
}

-(void)searchUserData:(NSArray *)userDataArray
{
    
    NSString *otherid = [self.currentConversation otherId];
    BOOL ischeck = NO;
    for (int t = 0; t < userDataArray.count; t++) {
        NSDictionary *record = userDataArray[t];
        
        if ([otherid isEqualToString:record[ChatTableUsersName]]) {
            ischeck = YES;
            self.currentUserData = record[@"ChatTableUsersData"];
            
            self.nameLabel.text =
            [ALFormatter preferredDisplayNameForUserInfomation:self.currentUserData UserID:otherid];
            
            NSString *headImage =
            _currentUserData[@"resBody"][@"HeadImage"];
            
            [_avatarImageView imageFromURLString:headImage];
            
            break;
        }
    }
    
    if (ischeck == NO) {
        //NSLog(@"沒有資料");
    }
}

@end
