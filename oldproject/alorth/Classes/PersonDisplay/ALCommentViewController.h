//
//  ALCommentViewController.h
//  alorth
//
//  Created by w91379137 on 2015/7/31.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>
#import "ALBasicSubViewController.h"
#import "ALBoughtOrderViewController.h"

@interface ALCommentViewController : ALBasicSubViewController
{
    int starCount;
}

-(instancetype)initWithInfoDict:(NSDictionary *)infoDict;

@property (nonatomic, strong) NSDictionary *infoDict;
@property (nonatomic, weak) ALBoughtOrderViewController *tableDelegate;

@property (nonatomic, strong) IBOutlet UIImageView *kvo_goodsImageView;
@property (nonatomic, strong) IBOutlet UIView *showStarView;

@property (nonatomic, strong) IBOutlet UITextView *commetTextView;
@property (nonatomic, strong) IBOutlet UILabel *commetTextPlaceHolderLabel;

@property (nonatomic, strong) IBOutlet UIButton *postButton;

@end
