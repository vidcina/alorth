//
//  ALAccountDetailRatingListViewController.m
//  alorth
//
//  Created by John Hsu on 2015/6/30.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALPersonRatingListViewController.h"
#import "ALPersonRatingListViewController+PullMore.h"

// cells
#import "ALOrderRateTableViewCell.h"
#import "ALMessageTableViewCell.h"

//reply
#import "ALReplyViewController.h"

@interface ALPersonRatingListViewController ()
{
    ALMessageTableViewCell *demoCell;
}

@end

@implementation ALPersonRatingListViewController

#pragma mark - VC Life
- (instancetype)initWithUserID:(NSString *)userID
{
    self = [super init];
    if (self) {
        _userID = userID;
        
        objectListArray = [NSMutableArray array];
        
        currentTableViewControl = [[ALTableViewControl alloc] init];
        currentTableViewControl.delegate = self;
        currentTableViewControl.apiDataArray = objectListArray;
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self alTableViewSetting:self.mainTableView];
    self.mainTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    if (objectListArray.count == 0) {
        [self checkNewData:currentTableViewControl];
    }
}

#pragma mark - UITableViewDataSource Row
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [objectListArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //NSDictionary *dataDict = listArray[section];
    //NSArray *replyList = dataDict[@"ReplyList"];
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dataDict = objectListArray[indexPath.section];
    if (indexPath.row == 0) {
        return 150;
    }
    
//    ALRateTextTableViewCell *cell = [ALRateTextTableViewCell cell];
//    NSDictionary *fontAttribute = @{NSFontAttributeName : cell.rateTextLabel.font};
//    NSString *text = dataDict[@"Comment"];
//    CGSize titleSize = [text boundingRectWithSize:CGSizeMake(tableView.frame.size.width - 100, MAXFLOAT)
//                                          options:NSStringDrawingUsesLineFragmentOrigin
//                                       attributes:fontAttribute
//                                          context:nil].size;
//    return 60 + titleSize.height;
    
    if (demoCell == nil) {
        demoCell = [ALMessageTableViewCell cellFromXib];
        demoCell.frame = CGRectMake(0, 0, tableView.contentSize.width, 0);
        [demoCell layoutIfNeeded];
    }
    return [ALMessageTableViewCell performCell:demoCell withCommentDict:dataDict];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dataDict = objectListArray[indexPath.section];
    
    if (indexPath.row == 0) {
        ALOrderRateTableViewCell *cell = [ALOrderRateTableViewCell cell];
        [ALOrderRateTableViewCell performCell:cell withDictionary:dataDict];
        return cell;
    }
    
    ALMessageTableViewCell *cell = [ALMessageTableViewCell cellFromXib];
    [ALMessageTableViewCell performCell:cell withCommentDict:dataDict];
    return cell;
}

#pragma mark - UITableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *dataDict = [objectListArray[indexPath.section] mutableCopy];
    
    //紀錄一下這是誰的列表
    dataDict[ALReplyCommentOwnerKey] = self.userID;
    
    ALReplyViewController *next = [[ALReplyViewController alloc] initWithInfoDict:dataDict];
    
    [self.navigationController pushViewController:next animated:YES];
}

@end
