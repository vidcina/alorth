//
//  ALCommentViewController.m
//  alorth
//
//  Created by w91379137 on 2015/7/31.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALCommentViewController.h"
#import "ALAsset+Store.h"
#import "UIViewController+UIImagePickerControllerDelegate.h"
#import "UIViewController+ImageOnline.h"

@interface ALCommentViewController ()

@end

@implementation ALCommentViewController

#pragma mark - VC Life
-(instancetype)initWithInfoDict:(NSDictionary *)infoDict
{
    self = [super init];
    if (self) {
        _infoDict = infoDict;
        starCount = 3;
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    self.kvo_goodsImageView.userInteractionEnabled = YES;
    [self.kvo_goodsImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showChoosePickerControllerType)]];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.showStarView rankStar:5 get:starCount];
}

#pragma mark - IBAction
-(IBAction)starAction:(UIButton *)sender
{
    starCount = (int)sender.tag;
    [self.showStarView rankStar:5 get:starCount];
}

-(IBAction)saveAction:(UIButton *)sender
{
    [self uploadImage:self.kvo_goodsImageView.image];
}

#pragma mark - Keyboard Auto System
-(void)keyboardDidShow:(NSNotification *)notification
{
    float keyboardHeight =
    [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    keyboardHeight -= self.postButton.frame.size.height;
    
    [UIView animateWithDuration:0.15
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.view.transform =
                         CGAffineTransformTranslate(CGAffineTransformIdentity, 0, -keyboardHeight);
                     } completion:nil];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.15
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.view.transform = CGAffineTransformIdentity;
                     } completion:nil];
}

#pragma mark - UITextView Delegate
- (void)textViewDidChange:(UITextView *)textView
{
    if (textView == self.commetTextView) {
        if (textView.text.length > 0) {
            self.commetTextPlaceHolderLabel.hidden = YES;
        }
        else {
            self.commetTextPlaceHolderLabel.hidden = NO;
        }
    }
}

#pragma mark - UIImagePickerController Delegate
- (void)didPickerControllerReturnImage:(UIImage *)avatarImage
{
    self.kvo_goodsImageView.image = avatarImage;
}

#pragma mark - ImageOnline
- (void)didSaveImageAtURL:(NSString *)string
{
    if (![string isKindOfClass:[NSString class]]) {
        string = @"";
    }
    [self productDataWriteDownStepImage:string];
}

#pragma mark - 
-(void)productDataWriteDownStepImage:(NSString *)imageURLString
{
    [self addMBProgressHUDWithKey:@"StockA014"];
    [ALNetWorkAPI commentStockA014StockID:self.infoDict[@"StockID"]
                                  orderID:self.infoDict[@"OrderID"]
                                     Rate:[NSString stringWithFormat:@"%d",starCount]
                                  Comment:self.commetTextView.text
                              ReviewImage:imageURLString
                          CompletionBlock:^(NSDictionary *responseObject) {
                              [self removeMBProgressHUDWithKey:@"StockA014"];
                              
                              NSDictionary *dict = responseObject[@"resBody"];
                              if (dict && dict[@"ResultCode"] && [dict[@"ResultCode"] intValue] == 0) {
                                  
                                  [self.tableDelegate checkNewData];
                                  YKSimpleAlert(ALLocalizedString(@"Comment uploaded",nil));
                                  [self.navigationController popViewControllerAnimated:YES];
                              }
                              else {
                                  YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
                              }
                          }];
}

-(void)deleteFile:(NSString *)aPath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:aPath]) {
        NSError *error;
        if ([[NSFileManager defaultManager] isDeletableFileAtPath:aPath]) {
            BOOL success = [[NSFileManager defaultManager] removeItemAtPath:aPath error:&error];
            if (!success) {
                ALLog(@"Error removing file at path: %@", error.localizedDescription);
            }
        }
    }
}

@end
