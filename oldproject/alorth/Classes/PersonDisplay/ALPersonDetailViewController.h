//
//  ALAccountDetailViewController.h
//  alorth
//
//  Created by w91379137 on 2015/6/24.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALAccountDefinition.h"
#import "ALBasicSubViewController.h"

@interface ALPersonDetailViewController : ALBasicSubViewController
<UITableViewDataSource, UITableViewDelegate>
{
    NSString *userID;
    
    IBOutlet UIImageView *userAvatarImageView;
    IBOutlet UILabel *userNameLabel;
    
    NSMutableArray *objectListArray;
    BOOL willReplaceWithNew;                    //是否清空之前資料
    __weak IBOutlet UITableView *mainTableView;
    ALTableViewControl *currentTableViewControl;
}

-(instancetype)initWithUserId:(NSString *)anUserID;

@end
