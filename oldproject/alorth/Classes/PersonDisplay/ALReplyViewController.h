//
//  ALReplyViewController.h
//  alorth
//
//  Created by w91379137 on 2015/7/31.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *ALReplyCommentOwnerKey = @"CommentOwnerKey";

@interface ALReplyViewController : ALBasicSubViewController
<UITableViewDataSource, UITableViewDelegate, ALTableViewControlDelegate>
{
    NSMutableArray *replyListArray;
    BOOL willReplaceWithNew;                    //是否清空之前資料
    __weak IBOutlet UITableView *mainTableView;
    ALTableViewControl *currentTableViewControl;
}

-(instancetype)initWithInfoDict:(NSDictionary *)infoDict;
@property (nonatomic, strong) NSDictionary *infoDict;
@property (nonatomic) BOOL canReply;

//輸入
@property (nonatomic, strong) IBOutlet UIButton *replyButton;
@property (nonatomic, strong) IBOutlet UITableViewCell *replyCell;
@property (nonatomic, strong) IBOutlet UITextView *replyTextView;

@end
