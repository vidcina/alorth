//
//  ALAccountDetailRatingListViewController.h
//  alorth
//
//  Created by John Hsu on 2015/6/30.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicViewController.h"

@interface ALPersonRatingListViewController : ALBasicSubViewController
<UITableViewDataSource, UITableViewDelegate>
{
    //列表
    NSMutableArray *objectListArray;
    BOOL willReplaceWithNew;                    //是否清空之前資料
    ALTableViewControl *currentTableViewControl;
}

- (instancetype)initWithUserID:(NSString *)userID;

@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) IBOutlet UITableView *mainTableView;

@end
