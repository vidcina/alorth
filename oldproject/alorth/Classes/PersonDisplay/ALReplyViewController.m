//
//  ALReplyViewController.m
//  alorth
//
//  Created by w91379137 on 2015/7/31.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALReplyViewController.h"
#import "ALMessageTableViewCell.h"

@interface ALReplyViewController ()
{
    ALMessageTableViewCell *demoCell;
}

@end

@implementation ALReplyViewController

#pragma mark - VC Life
-(instancetype)initWithInfoDict:(NSDictionary *)infoDict
{
    self = [super init];
    if (self) {
        _infoDict = infoDict;
        replyListArray = [NSMutableArray array];
        
        currentTableViewControl = [[ALTableViewControl alloc] init];
        currentTableViewControl.delegate = self;
        currentTableViewControl.apiDataArray = replyListArray;
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self alTableViewSetting:mainTableView];
    mainTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    self.replyTextView.font = [UIFont systemFontOfSize:17];
    
    self.canReply = NO;
    if ([self.infoDict[@"CommentName"] isEqualToString:[ALAppDelegate sharedAppDelegate].userID] ||
        [self.infoDict[ALReplyCommentOwnerKey] isEqualToString:[ALAppDelegate sharedAppDelegate].userID]) {
        self.canReply = YES;
        self.replyButton.hidden = NO;
    }
    
    [self displayWithInfoDict];
}

-(void)displayWithInfoDict
{
    [mainTableView reloadData];
    
    if (replyListArray.count == 0) {
        [self checkNewData:currentTableViewControl];
    }
}

#pragma mark - UITableView Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
        {
            return 1;
        }
            break;
        case 1:
        {
            return replyListArray.count;
        }
            break;
        case 2:
        {
            //檢測是否拉到最底
            NSInteger page = [currentTableViewControl.lastDict[@"StartNum"] integerValue];
            if (replyListArray.count < (page + 1) * 20) {
                return 1;
            }
            else {
                return 0;
            }
        }
            break;
        default:
        {
            return 0;
        }
            break;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (self.canReply == YES) {
            return self.replyCell.frame.size.height;
        }
        else {
            return 0;
        }
    }
    else {
        if (demoCell == nil) {
            demoCell = [ALMessageTableViewCell cellFromXib];
            demoCell.frame = CGRectMake(0, 0, tableView.contentSize.width, 0);
            [demoCell layoutIfNeeded];
        }
        
        if (indexPath.section == 1) {
            return [ALMessageTableViewCell performCell:demoCell
                                         withReplyDict:replyListArray[indexPath.row]];
        }
        else {
            return [ALMessageTableViewCell performCell:demoCell
                                       withCommentDict:self.infoDict];
        }
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return self.replyCell;
    }
    
    ALMessageTableViewCell *cell = [ALMessageTableViewCell cellFromXib];
    if (indexPath.section == 1) {
        [ALMessageTableViewCell performCell:cell withReplyDict:replyListArray[indexPath.row]];
    }
    else {
        [ALMessageTableViewCell performCell:cell withCommentDict:self.infoDict];
    }
    return cell;
}

#pragma mark - UITableView Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([cell isKindOfClass:[ALProductTableViewCell class]]) {
        ALProductTableViewCell *cellx = (ALProductTableViewCell *)cell;
        
        [ALInspector logEventCategory:@"ProductDetail"                                  // Event category (required)
                               Action:@"ALReplyViewController didSelectRowAtIndexPath"  // Event action (required)
                                Label:NSStringFromClass([self class])                   // Event label
                                Value:nil];                                             // Event value

        ALProductDetailViewController *a = [[ALProductDetailViewController alloc] initWithInfoDict:cellx.infoDict];
        [self.navigationController pushViewController:a animated:YES];
    }
}

#pragma mark - ALTableViewControlDelegate
-(void)checkNewData:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    willReplaceWithNew = YES;
    tableViewControl.lastDict[@"StartNum"] = @"0";
    
    ALLog(@"重新整理");
    [self addMBProgressHUDWithKey:@"replyListcheckNewData"];
    [self replyListLoadApiSourceParameters:tableViewControl.lastDict
                               FinishBlock:^(BOOL isSuccess) {
                                   [self removeMBProgressHUDWithKey:@"replyListcheckNewData"];
                               }];
}

-(void)loadMore:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    
    ALLog(@"載入更多:目前第 %@ 頁 %lu 筆資料",tableViewControl.lastDict[@"StartNum"],(unsigned long)replyListArray.count);
    
    tableViewControl.lastDict[@"StartNum"] =
    [NSString stringWithFormat:@"%d",[tableViewControl.lastDict[@"StartNum"] intValue] + 1];
    
    [self addMBProgressHUDWithKey:@"replyListloadMore"];
    [self replyListLoadApiSourceParameters:tableViewControl.lastDict
                               FinishBlock:^(BOOL isSuccess) {
                                   [self removeMBProgressHUDWithKey:@"replyListloadMore"];
                               }];
}

#pragma mark - scrollView Delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == mainTableView) {
        [currentTableViewControl scrollViewDidScroll:scrollView];
    }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView == mainTableView) {
        [currentTableViewControl scrollViewDidEndDragging:scrollView willDecelerate:(BOOL)decelerate];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == mainTableView) {
        [currentTableViewControl scrollViewDidEndDecelerating:(UIScrollView *)scrollView];
    }
}

#pragma mark - API
-(void)replyListLoadApiSourceParameters:(NSDictionary *)parameters
                            FinishBlock:(void (^)(BOOL isSuccess))finishBlockToRun
{
    [ALNetWorkAPI categoryCommentReplyA032:self.infoDict[@"CommentID"]
                                  startNum:parameters[@"StartNum"]
                           completionBlock:^(NSDictionary *responseObject) {
                       
                       BOOL isWiseData = YES;
                       if (![responseObject[@"resBody"] isKindOfClass:[NSDictionary class]]) {
                           isWiseData = NO;
                           
                           if (![responseObject[@"resBody"][@"ReplyList"] isKindOfClass:[NSArray class]]) {
                               isWiseData = NO;
                           }
                       }
                       
                       if (isWiseData) {
                           if (willReplaceWithNew) {
                               [replyListArray removeAllObjects];
                               willReplaceWithNew = NO;
                           }
                           
                           NSArray *addArray = [responseObject[@"resBody"][@"ReplyList"] mutableCopy];
                           if (addArray.count > 0) {
                               [replyListArray addObjectsFromArray:addArray];
                           }
                           [mainTableView reloadData];
                       }
                       else {
                           ALLog(@"A032_resBody_GoodsList_格式錯誤");
                       }
                       finishBlockToRun(YES);
                   }];
}

#pragma mark - 
-(IBAction)replyAction:(id)sender
{
    [self keyboardReturn];
    
    if (self.replyTextView.text.length == 0) {
        return;
    }
    
    [self addMBProgressHUDWithKey:@"replyCommentA015"];
    [ALNetWorkAPI replyCommentA015CommentID:self.infoDict[@"CommentID"]
                               ReplyComment:self.replyTextView.text
                            CompletionBlock:^(NSDictionary *responseObject) {
                                [self removeMBProgressHUDWithKey:@"replyCommentA015"];
                                
                                NSDictionary *dict = responseObject[@"resBody"];
                                if (dict && dict[@"ResultCode"] && [dict[@"ResultCode"] intValue] == 0) {
                                    self.replyTextView.text = @"";
                                    #warning TODO:這邊並沒有 通知placeholder 更新
                                    [self checkNewData:currentTableViewControl];
                                }
                                else {
                                    YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
                                }
                            }];
}

@end
