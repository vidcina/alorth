//
//  ALPersonRatingListViewController+PullMore.m
//  alorth
//
//  Created by w91379137 on 2015/10/21.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALPersonRatingListViewController+PullMore.h"

@implementation ALPersonRatingListViewController (PullMore)

#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.mainTableView) {
        [currentTableViewControl scrollViewDidScroll:scrollView];
    }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView == self.mainTableView) {
        [currentTableViewControl scrollViewDidEndDragging:scrollView willDecelerate:(BOOL)decelerate];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.mainTableView) {
        [currentTableViewControl scrollViewDidEndDecelerating:(UIScrollView *)scrollView];
    }
}

#pragma mark - ALTableViewControlDelegate
-(void)checkNewData:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    willReplaceWithNew = YES;
    tableViewControl.lastDict[@"StartNum"] = @"0";
    
    ALLog(@"重新整理");
    [self addMBProgressHUDWithKey:@"reviewListA013"];
    [self listLoadApiSourceParameters:tableViewControl.lastDict
                          FinishBlock:^(BOOL isSuccess) {
                              [self removeMBProgressHUDWithKey:@"reviewListA013"];
                          }];
}

-(void)loadMore:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    
    ALLog(@"載入更多:目前第 %@ 頁 %lu 筆資料",tableViewControl.lastDict[@"StartNum"],(unsigned long)objectListArray.count);
    
    tableViewControl.lastDict[@"StartNum"] =
    [NSString stringWithFormat:@"%d",[tableViewControl.lastDict[@"StartNum"] intValue] + 1];
    
    [self addMBProgressHUDWithKey:@"reviewListA013"];
    [self listLoadApiSourceParameters:tableViewControl.lastDict
                          FinishBlock:^(BOOL isSuccess) {
                              [self removeMBProgressHUDWithKey:@"reviewListA013"];
                          }];
}

#pragma mark - API
-(void)listLoadApiSourceParameters:(NSDictionary *)parameters
                       FinishBlock:(void (^)(BOOL isSuccess))finishBlockToRun
{
    [ALNetWorkAPI reviewListA013withUserID:self.userID
                                  startNum:parameters[@"StartNum"]
                           completionBlock:^(NSDictionary *responseObject) {
                               
                               BOOL isWiseData = YES;
                               if (![responseObject[@"resBody"] isKindOfClass:[NSDictionary class]]) {
                                   isWiseData = NO;
                                   
                                   if (![responseObject[@"resBody"][@"CommentList"] isKindOfClass:[NSArray class]]) {
                                       isWiseData = NO;
                                   }
                               }
                               
                               if (isWiseData) {
                                   if (willReplaceWithNew) {
                                       [objectListArray removeAllObjects];
                                       willReplaceWithNew = NO;
                                   }
                                   
                                   NSArray *addArray = [responseObject[@"resBody"][@"CommentList"] mutableCopy];
                                   if (addArray.count > 0) {
                                       [objectListArray addObjectsFromArray:addArray];
                                   }
                                   [self.mainTableView reloadData];
                               }
                               else {
                                   ALLog(@"eviewListA013_resBody_GoodsList_格式錯誤");
                               }
                               finishBlockToRun(YES);
                           }];
}

@end
