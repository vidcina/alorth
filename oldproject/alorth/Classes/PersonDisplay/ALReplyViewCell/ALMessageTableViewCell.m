//
//  ALMainCommentTableViewCell.m
//  alorth
//
//  Created by w91379137 on 2015/8/19.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALMessageTableViewCell.h"

@implementation ALMessageTableViewCell

- (void)cellFromXibSetting
{
    self.avatarImageView.clipsToBounds = YES;
    self.avatarImageView.layer.cornerRadius = self.avatarImageView.frame.size.width / 2;
    self.avatarImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.avatarImageView.layer.borderWidth = 2;
}

+ (CGFloat)performCell:(ALMessageTableViewCell *)cell withCommentDict:(NSDictionary *)dictionary
{
    cell.infoDict = dictionary;
    
    NSString *userID = cell.infoDict[@"CommentUser"];
    
    if ([userID isKindOfClass:[NSString class]]) {
        [ALUserInfoCacheManager getLocalUserInfomation:userID
                                       completionBlock:^(NSDictionary *responseObject) {
                                           
                                           cell.nameLabel.text =
                                           [ALFormatter preferredDisplayNameForUserInfomation:responseObject
                                                                                       UserID:userID];
                                           if (responseObject[@"resBody"][@"HeadImage"]) {
                                               [cell.avatarImageView imageFromURLString:responseObject[@"resBody"][@"HeadImage"]];
                                           }
                                           float rating= [responseObject[@"resBody"][@"Rating"] floatValue];
                                           
                                           [cell.showStarView rankStar:5 get:rating];
                                       }];
    }
    else {
        cell.nameLabel.text = cell.infoDict[@"CommentUser"];
        if (cell.infoDict[@"AvatarImage"]) {
            [cell.avatarImageView imageFromURLString:cell.infoDict[@"AvatarImage"]];
        }
    }
    
    
    
    {
        NSDate *commentDate = [NSDate dateWithTimeIntervalSince1970:[cell.infoDict[@"CommentDate"] doubleValue]];
        cell.dateLabel.localText = [ALFormatter localTextIntervalAgoTextForDate:commentDate];
        [cell.showStarView rankStar:5 get:[cell.infoDict[@"AverageRating"] floatValue]];
        
        cell.commentTextView.text = cell.infoDict[@"Comment"];
    }
   
    return [ALMessageTableViewCell performCellHeight:cell];
}

+ (CGFloat)performCell:(ALMessageTableViewCell *)cell withReplyDict:(NSDictionary *)dictionary
{
    cell.infoDict = dictionary;
    
    NSString *userID = cell.infoDict[@"ReplyUser"];
    if (![userID isKindOfClass:[NSString class]]) {
        userID = cell.infoDict[@"Replyer"];
    }
    
    if ([userID isKindOfClass:[NSString class]]) {
        [ALUserInfoCacheManager getLocalUserInfomation:userID
                                       completionBlock:^(NSDictionary *responseObject) {
                                           
                                           cell.nameLabel.text =
                                           [ALFormatter preferredDisplayNameForUserInfomation:responseObject
                                                                                       UserID:userID];
                                           if (responseObject[@"resBody"][@"HeadImage"]) {
                                               [cell.avatarImageView imageFromURLString:responseObject[@"resBody"][@"HeadImage"]];
                                           }
                                           float rating= [responseObject[@"resBody"][@"Rating"] floatValue];
                                           
                                           [cell.showStarView rankStar:5 get:rating];
                                       }];
    }
    else {
        if (cell.infoDict[@"UserImage"]) {
            [cell.avatarImageView imageFromURLString:cell.infoDict[@"UserImage"]];
        }
    }
    
    {
        NSDate *replyDate =
        [NSDate dateWithTimeIntervalSince1970:[cell.infoDict[@"ReplyDate"] doubleValue]];
        cell.dateLabel.localText = [ALFormatter localTextIntervalAgoTextForDate:replyDate];
        
        cell.commentTextView.text = cell.infoDict[@"ReplyMessage"];
    }
    
    return [ALMessageTableViewCell performCellHeight:cell];
}

+ (CGFloat)performCell:(ALMessageTableViewCell *)cell withProductInfoDict:(NSDictionary *)dictionary
{
    cell.infoDict = dictionary;
    
    [cell.avatarImageView imageFromURLString:cell.infoDict[@"HeadImage"]];
    cell.nameLabel.text = cell.infoDict[@"HeadImage"];
    
    if (dictionary) {
        cell.nameLabel.text =
        [ALFormatter preferredDisplayNameForUserInfomation:@{@"resBody":dictionary}
                                                    UserID:dictionary[@"UserID"]];
    }
    
    float rating= [dictionary[@"AverageRate"] floatValue];
    [cell.showStarView rankStar:5 get:rating];
    
    NSDate *replyDate =
    [NSDate dateWithTimeIntervalSince1970:[cell.infoDict[@"EditDate"] doubleValue]];
    
    NSDateFormatter *formatter =
    [ALFormatter dateFormatteWithTimeZone:[NSTimeZone systemTimeZone]
                             FormatString:@"yyyy-MM-dd' 'HH:mm:ss"];
    
    cell.dateLabel.text = [formatter stringFromDate:replyDate];
    
    cell.commentTextView.text = cell.infoDict[@"ProductDescription"];
    
    return [ALMessageTableViewCell performCellHeight:cell];
}

+ (CGFloat)performCellHeight:(ALMessageTableViewCell *)cell
{
    // selectable 會影響字體
    cell.commentTextView.selectable = YES;
    
    NSDictionary *attributes = @{NSFontAttributeName:cell.commentTextView.font};
    CGSize size =
    [cell.commentTextView.text boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width - 40, MAXFLOAT)
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:attributes
                                            context:nil].size;
    
    [cell.commentTextView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(size.height + 25));
    }];
    
    [cell layoutIfNeeded];
    return cell.commentTextView.frame.origin.y + cell.commentTextView.frame.size.height + 10;
}

@end
