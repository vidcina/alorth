//
//  ALMainCommentTableViewCell.h
//  alorth
//
//  Created by w91379137 on 2015/8/19.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALCommonTableViewCell.h"

@interface ALMessageTableViewCell : ALCommonTableViewCell

@property (nonatomic, strong) IBOutlet UIImageView *avatarImageView;
@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet UILabel *dateLabel;
@property (nonatomic, strong) IBOutlet UIView *showStarView;
@property (nonatomic, strong) IBOutlet UITextView *commentTextView;

+ (CGFloat)performCell:(ALMessageTableViewCell *)cell
       withCommentDict:(NSDictionary *)dictionary;

+ (CGFloat)performCell:(ALMessageTableViewCell *)cell
         withReplyDict:(NSDictionary *)dictionary;

+ (CGFloat)performCell:(ALMessageTableViewCell *)cell
   withProductInfoDict:(NSDictionary *)dictionary;

@end
