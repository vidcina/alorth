//
//  ALOrderRateTableViewCell.m
//  alorth
//
//  Created by w91379137 on 2015/8/19.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALOrderRateTableViewCell.h"

@implementation ALOrderRateTableViewCell

+(ALOrderRateTableViewCell *)cell
{
    ALOrderRateTableViewCell *cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class])
                                                                  owner:nil
                                                                options:nil] lastObject];
    cell.productImageView.image = [ALNothingToSeeImage whiteNothingToSeeImage];
    return cell;
}

+ (CGFloat)performCell:(ALOrderRateTableViewCell *)cell withDictionary:(NSDictionary *)dictionary
{
    cell.infoDict = dictionary;
    
    [cell.productImageView imageFromURLString:dictionary.safe[@"CommentImage"]];
    
    cell.productTitleLabel.text = dictionary.safe[@"ProductName"];
    cell.priceLabel.text =
    [NSString stringWithFormat:@"%@ %@",dictionary.safe[@"ProductPrice"],dictionary.safe[@"ProductCurrency"]];
    
    [cell.starView rankStar:5 get:[dictionary.safe[@"Rate"] floatValue]];

    return cell.middleHorizontalLine.frame.origin.y + 1;
}

@end
