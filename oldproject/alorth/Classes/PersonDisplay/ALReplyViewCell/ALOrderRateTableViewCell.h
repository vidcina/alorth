//
//  ALOrderRateTableViewCell.h
//  alorth
//
//  Created by w91379137 on 2015/8/19.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ALOrderRateTableViewCell : UITableViewCell

@property (nonatomic, strong) NSDictionary *infoDict;

//上區
@property (nonatomic, weak) IBOutlet UIImageView *productImageView;
@property (nonatomic, weak) IBOutlet UILabel *productTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *priceLabel;
@property (nonatomic, weak) IBOutlet UIView *starView;
@property (nonatomic, weak) IBOutlet UIButton *replyButton;
@property (nonatomic, weak) IBOutlet UIButton *mainFakeButton;

//底標線
@property (nonatomic, weak) IBOutlet UIView *middleHorizontalLine;

+ (ALOrderRateTableViewCell *)cell;
+ (CGFloat)performCell:(ALOrderRateTableViewCell *)cell withDictionary:(NSDictionary *)dictionary;

@end
