//
//  ALAccountDetailViewController.m
//  alorth
//
//  Created by w91379137 on 2015/6/24.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALPersonDetailViewController.h"
#import "ALPersonDetailViewController+PullMore.h"

#import "ALPersonRatingListViewController.h"

@interface ALPersonDetailViewController ()

@end

@implementation ALPersonDetailViewController

#pragma mark - VC Life
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self defaultInitMethod];
        userID = [[ALAppDelegate sharedAppDelegate] userID];
    }
    return self;
}

- (instancetype)initWithUserId:(NSString *)anUserID
{
    self = [super init];
    if (self) {
        [self defaultInitMethod];
        userID = anUserID;
        if (!anUserID) {
            userID = [[ALAppDelegate sharedAppDelegate] userID];
        }
    }
    return self;
}

-(void)defaultInitMethod
{
    objectListArray = [NSMutableArray array];
    
    currentTableViewControl = [[ALTableViewControl alloc] init];
    currentTableViewControl.delegate = self;
    currentTableViewControl.apiDataArray = objectListArray;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (objectListArray.count > 0) {
        [ALProductTableViewCell detectCenterCellToPlay:mainTableView];
    }
    
    [self alTableViewSetting:mainTableView];
    
    userAvatarImageView.layer.masksToBounds = YES;
    userAvatarImageView.layer.cornerRadius = (userAvatarImageView.frame.size.height / 2);
    [userAvatarImageView.layer setBorderColor:[UIColor whiteColor].CGColor];
    [userAvatarImageView.layer setBorderWidth:2];
    
    [ALUserInfoCacheManager getLocalUserInfomation:userID
                                   completionBlock:^(NSDictionary *responseObject) {
                        
                                       if([ALNetWorkAPI checkSerialNumber:@"A033"
                                                           ResponseObject:responseObject]) {
                                           userNameLabel.text =
                                           [ALFormatter preferredDisplayNameForUserInfomation:responseObject
                                                                                       UserID:userID];
                                           
                                           if (responseObject[@"resBody"][@"HeadImage"]) {
                                               [userAvatarImageView imageFromURLString:responseObject[@"resBody"][@"HeadImage"]];
                                           }
                                       }
                                       else {
                                           //@"網路連線錯誤，請稍後再試一次"
                                           YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
                                       }
                    }];
    
    if (objectListArray.count == 0) {
        [self checkNewData:currentTableViewControl];
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    [ALProductTableViewCell stopAllPlayCell:mainTableView];
    [super viewDidDisappear:animated];
}

#pragma mark - UITableViewDataSource Row
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return objectListArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *infoDict = objectListArray[indexPath.row];
    return [ALProductTableViewCell settingCell:nil WithInfoDict:infoDict];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ALProductTableViewCell *cell = [ALProductTableViewCell cell];
    
    NSDictionary *infoDict = objectListArray[indexPath.row];
    [ALProductTableViewCell settingCell:cell WithInfoDict:infoDict];
    
    return cell;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([cell isKindOfClass:[ALProductTableViewCell class]]) {
        ALProductTableViewCell *cellx = (ALProductTableViewCell *)cell;
        
        [ALInspector logEventCategory:@"ProductDetail"                                          // Event category (required)
                               Action:@"ALAccountDetailViewController didSelectRowAtIndexPath"  // Event action (required)
                                Label:NSStringFromClass([self class])                           // Event label
                                Value:nil];                                                     // Event value

        ALProductDetailViewController *a = [[ALProductDetailViewController alloc] initWithInfoDict:cellx.infoDict];
        [self.navigationController pushViewController:a animated:YES];
    }
}

#pragma mark - IBAction
-(IBAction)ratingListAction:(id)sender
{
    ALPersonRatingListViewController *vc = [[ALPersonRatingListViewController alloc] initWithUserID:userID];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
