//
//  ALPersonDetailViewController+PullMore.m
//  alorth
//
//  Created by w91379137 on 2015/10/21.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALPersonDetailViewController+PullMore.h"

@implementation ALPersonDetailViewController (PullMore)

#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == mainTableView) {
        [currentTableViewControl scrollViewDidScroll:scrollView];
        [ALProductTableViewCell detectCenterCellToPlay:mainTableView];
    }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView == mainTableView) {
        [currentTableViewControl scrollViewDidEndDragging:scrollView willDecelerate:(BOOL)decelerate];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == mainTableView) {
        [currentTableViewControl scrollViewDidEndDecelerating:(UIScrollView *)scrollView];
    }
}

#pragma mark - ALTableViewControlDelegate
-(void)checkNewData:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    willReplaceWithNew = YES;
    tableViewControl.lastDict[@"StartNum"] = @"0";
    tableViewControl.lastDict[@"UserID"] = userID;
    
    ALLog(@"重新整理");
    [self addMBProgressHUDWithKey:@"goodsA006Myself"];
    [self goodsListLoadApiSourceParameters:tableViewControl.lastDict
                               FinishBlock:^(BOOL isSuccess) {
                                   [self removeMBProgressHUDWithKey:@"goodsA006Myself"];
                               }];
}

-(void)loadMore:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    
    ALLog(@"載入更多:目前第 %@ 頁 %lu 筆資料",tableViewControl.lastDict[@"StartNum"],(unsigned long)objectListArray.count);
    
    tableViewControl.lastDict[@"StartNum"] =
    [NSString stringWithFormat:@"%d",[tableViewControl.lastDict[@"StartNum"] intValue] + 1];
    
    [self addMBProgressHUDWithKey:@"goodsA006Myself"];
    [self goodsListLoadApiSourceParameters:tableViewControl.lastDict
                               FinishBlock:^(BOOL isSuccess) {
                                   [self removeMBProgressHUDWithKey:@"goodsA006Myself"];
                               }];
}

#pragma mark - API
-(void)goodsListLoadApiSourceParameters:(NSDictionary *)parameters
                            FinishBlock:(void (^)(BOOL isSuccess))finishBlockToRun
{
    [ALProductTableViewCell stopAllPlayCell:mainTableView];
    [ALNetWorkAPI goodsA006Keyword:parameters[@"keyWord"]
                          Category:parameters[@"category"]
                             Order:parameters[@"Order"]
                          StartNum:parameters[@"StartNum"]
                            UserID:parameters[@"UserID"]
                   CompletionBlock:^(NSDictionary *responseObject) {
                       
                       BOOL isWiseData = YES;
                       if (![responseObject[@"resBody"] isKindOfClass:[NSDictionary class]]) {
                           isWiseData = NO;
                           
                           if (![responseObject[@"resBody"][@"GoodsList"] isKindOfClass:[NSArray class]]) {
                               isWiseData = NO;
                           }
                       }
                       
                       if (isWiseData) {
                           if (willReplaceWithNew) {
                               [objectListArray removeAllObjects];
                               willReplaceWithNew = NO;
                           }
                           
                           NSArray *addArray = [responseObject[@"resBody"][@"GoodsList"] mutableCopy];
                           if (addArray.count > 0) {
                               [objectListArray addObjectsFromArray:addArray];
                           }
                           [mainTableView reloadData];
                           
                           //至少一個才能播放
                           if (objectListArray.count > 0) {
                               [ALProductTableViewCell detectCenterCellToPlay:mainTableView];
                           }
                       }
                       else {
                           ALLog(@"goodsA006_resBody_GoodsList_格式錯誤");
                       }
                       finishBlockToRun(YES);
                   }];
}

@end
