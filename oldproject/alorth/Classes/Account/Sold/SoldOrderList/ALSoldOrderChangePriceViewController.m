//
//  ALSoldOrderChangePriceViewController.m
//  alorth
//
//  Created by John Hsu on 2015/6/30.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALSoldOrderChangePriceViewController.h"

@interface ALSoldOrderChangePriceViewController ()

@end

@implementation ALSoldOrderChangePriceViewController

#pragma mark - VC Life
-(instancetype)initWithInfoDict:(NSDictionary *)infoDict
{
    self = [super init];
    if (self) {
        _infoDict = infoDict;
        _cellsArray = [NSMutableArray array];
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.mainTableView.contentInset = UIEdgeInsetsMake(30, 0, 0, 0);
    [self alTableViewSetting:self.mainTableView];
    
    self.priceTextField.placeholder =
    [NSString stringWithFormat:@"%.2f", [self.infoDict[@"Price"] floatValue]];
    
    self.feeTextField.placeholder =
    [NSString stringWithFormat:@"%.2f", [self.infoDict[@"TransPrice"] floatValue]];
    
    if (self.cellsArray.count == 0) {
        [self.cellsArray addObject:self.cell1];
        [self.cellsArray addObject:self.cell2];
        [self.mainTableView reloadData];
    }
}

#pragma mark - UITableView Datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.cellsArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = self.cellsArray[indexPath.row];
    return cell.frame.size.height;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.cellsArray[indexPath.row];
}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    BOOL shouldBegin = [super textFieldShouldBeginEditing:textField];
    if (shouldBegin) {
        textField.inputAccessoryView = [textField keyboardReturnToolbar];
    }
    return shouldBegin;
}

#pragma mark - IBAction
-(IBAction)saveAction:(UIButton *)sender
{
    NSString *orderPrice =
    self.priceTextField.text.length == 0 ? self.priceTextField.placeholder : self.priceTextField.text;
    
    NSString *transPrice =
    self.feeTextField.text.length == 0 ? self.feeTextField.placeholder : self.feeTextField.text;
    
    if (self.priceTextField.text.length == 0 && self.feeTextField.text.length == 0) {
        //根本沒輸入
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    [YKBlockAlert alertWithTitle:ALLocalizedString(@"Confirm", nil)
                         message:ALLocalizedString(@"The price will be changed", nil)
                blocksAndButtons:
     ^{
         [self addMBProgressHUDWithKey:@"changeOrderPriceA010"];
         [ALNetWorkAPI changeOrderPriceA010OrderId:self.infoDict[@"OrderID"]
                                        OrderPrice:orderPrice
                                        TransPrice:transPrice
                                   CompletionBlock:^(NSDictionary *responseObject) {
                                       [self removeMBProgressHUDWithKey:@"changeOrderPriceA010"];
                                       
                                       if([responseObject.safe[@"resHeader"][@"TXID"] isEqualToString:@"A010"] &&
                                          [responseObject.safe[@"resBody"][@"ResultCode"] intValue] == 0) {
                                           
                                           NSMutableDictionary *new = [self.infoDict mutableCopy];
                                           new[@"Price"] = orderPrice;
                                           new[@"TransPrice"] = transPrice;
                                           
                                           [self.delegate changeInfoDict:new];
                                           [self.navigationController popViewControllerAnimated:YES];
                                           YKSimpleAlert(ALLocalizedString(@"Save",nil));
                                       }
                                       else {
                                           //@"網路連線錯誤，請稍後再試一次"
                                           YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
                                       }
                                   }];
     },
     ALLocalizedString(@"Yes",nil),//左邊
     ^{
         
     },
     ALLocalizedString(@"No",nil),//右邊
     nil];
}

@end
