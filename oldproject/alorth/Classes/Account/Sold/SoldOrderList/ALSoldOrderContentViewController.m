//
//  ALSoldOrderContentViewController.m
//  alorth
//
//  Created by John Hsu on 2015/6/30.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALSoldOrderContentViewController.h"
#import "ALSoldOrderChangePriceViewController.h"

@interface ALSoldOrderContentViewController ()

@end

@implementation ALSoldOrderContentViewController

#pragma mark - VC Life
-(instancetype)initWithInfoDict:(NSDictionary *)infoDict
{
    self = [super init];
    if (self) {
        _infoDict = infoDict;
        topCell = [ALSoldOrderListTableViewCell cell];
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.mainScrollViewContentView.superview == nil) {
        [self.mainScrollView masSetContentView:self.mainScrollViewContentView isVerticalFix:NO];
    }
    
    //上方cell
    {
        if (topCell.superview == nil) {
            topCell.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0);
            [topCell layoutIfNeeded];
            
            [self.cellContainer addSubview:topCell];
            [topCell mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(self.cellContainer);
            }];
            
            [topCell mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(topCell.middleHorizontalLine.mas_bottom);
            }];
        }
        [ALSoldOrderListTableViewCell performCell:topCell withDictionary:self.infoDict];
    }
    
    //商品型號
    {
        if ([[self.infoDict[apiKeyIsUsed] description] isEqualToString:@"N"]) {
            self.productConditionLabel.localText = @"New";
        }
        else {
            self.productConditionLabel.localText = @"Used";
        }
        
        self.productSizeLabel.text = self.infoDict[@"ProductSize"];
        self.productNumberLabel.text = self.infoDict[@"Amount"];
        
        self.productShippingFeeLabel.text =
        [NSString stringWithFormat:@"%.2f %@",
         [self.infoDict[@"TransPrice"] floatValue],
         self.infoDict[@"Currency"]];
        
        self.productTotalPriceLabel.text =
        [NSString stringWithFormat:@"%.2f %@",
         [self.infoDict[@"Price"] floatValue] +
         [self.infoDict[@"TransPrice"] floatValue],
         self.infoDict[@"Currency"]];
    }
    
    //買家資訊
    {
        self.buyerNameLabel.text = self.infoDict[@"BuyerName"];
        self.buyerAddressLabel.text = self.infoDict[@"BuyerAddress"];
        self.buyerTelephoneLabel.text = self.infoDict[@"BuyerPhone"];
    }
    
    //賣家資訊
    if (self.isSellerInfomationExist != YES) {
        self.isSellerInfomationExist = YES;
        
        [self addMBProgressHUDWithKey:@"addStockA020"];
        [ALNetWorkAPI personalProfile020CompletionBlock:^(NSDictionary *responseObject) {
            [self removeMBProgressHUDWithKey:@"addStockA020"];
            
            NSDictionary *dict = responseObject[@"resBody"];
            if ([ALNetWorkAPI checkSerialNumber:@"A020"
                                 ResponseObject:responseObject]) {
                
                self.sellerNameLabel.text =
                [ALFormatter preferredDisplayNameFirst:dict[@"FirstName"]
                                                  Last:dict[@"LastName"]
                                                UserID:[ALAppDelegate sharedAppDelegate].userID];
                
                self.sellerAddressLabel.text = dict[@"Address"];
                self.sellerTelephoneLabel.text = dict[@"Phone"];
                self.sellerPublicInfoLabel.text = dict[@"PublicInfo"];
                self.sellerPublicInfoLabel.numberOfLines = 0;
                
                NSDictionary *titleattributes = @{NSFontAttributeName:self.sellerPublicInfoLabel.font};
                
                CGSize titleSize =
                [self.sellerPublicInfoLabel.text boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, MAXFLOAT)
                                                              options:NSStringDrawingUsesLineFragmentOrigin
                                                           attributes:titleattributes
                                                              context:nil].size;
                
                [self.sellerPublicInfoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.height.equalTo(@(titleSize.height + 30));
                }];
            }
            else {
                YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
            }
        }];
    }
    
    //商品編號
    {
        self.sellerAccountLabel.text = [ALAppDelegate sharedAppDelegate].userID;
        self.sellerOrderNumberLabel.text = self.infoDict[@"OrderID"];
        
        NSDateFormatter *formatter =
        [ALFormatter dateFormatteWithTimeZone:[NSTimeZone systemTimeZone]
                                 FormatString:@"yyyy-MM-dd"];
        
        self.sellerOrderTimeLabel.text =
        [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:[self.infoDict[@"OrderTime"] intValue]]];
    }
    
    //按鈕設置
    if ([self.infoDict[@"OrderStatus"] intValue] == 1) {
        //正在移交
        self.buttonsView.hidden = NO;
    }
    else {
        self.buttonsView.hidden = YES;
        
        [self.buttonsView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@0);
        }];
    }
}

#pragma mark - IBAction
-(IBAction)orderCancelAction:(UIButton *)sender
{
    [YKBlockAlert alertWithTitle:ALLocalizedString(@"Confirm", nil)
                         message:ALLocalizedString(@"The deal will be canceled", nil)
                blocksAndButtons:
     ^{
         [self addMBProgressHUDWithKey:@"cancelOrderPriceA011O"];
         [ALNetWorkAPI cancelOrderPriceA011OrderId:self.infoDict[@"OrderID"]
                                   CompletionBlock:^(NSDictionary *responseObject) {
                                       [self removeMBProgressHUDWithKey:@"cancelOrderPriceA011O"];
                                       NSLog(@"%@",responseObject);
                                       if([ALNetWorkAPI checkSerialNumber:@"A011"
                                                           ResponseObject:responseObject]) {
                                           
                                           [self.tableDelegate checkNewData];
                                           [self.navigationController popViewControllerAnimated:NO];
                                           
                                           YKSimpleAlert(@"Canceled");
                                       }
                                       else {
                                           //@"網路連線錯誤，請稍後再試一次"
                                           YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A011", responseObject),nil));
                                       }
                                   }];
     },
     ALLocalizedString(@"Yes",nil),//左邊
     ^{
         
     },
     ALLocalizedString(@"No",nil),//右邊
     nil];
}

-(IBAction)editPriceAction:(UIButton *)sender
{
    ALSoldOrderChangePriceViewController *vc = [[ALSoldOrderChangePriceViewController alloc] initWithInfoDict:self.infoDict];
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)changeInfoDict:(NSDictionary *)infoDict
{
    self.infoDict = infoDict;
    [self.tableDelegate checkNewData];
}

@end
