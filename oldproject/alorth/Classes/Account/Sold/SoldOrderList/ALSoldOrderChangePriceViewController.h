//
//  ALSoldOrderChangePriceViewController.h
//  alorth
//
//  Created by John Hsu on 2015/6/30.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicViewController.h"
#import "ALSoldOrderContentViewController.h"

@interface ALSoldOrderChangePriceViewController : ALBasicSubViewController

-(instancetype)initWithInfoDict:(NSDictionary *)infoDict;

@property (nonatomic, strong) NSDictionary *infoDict;
@property (nonatomic, weak) ALSoldOrderContentViewController *delegate;

@property (nonatomic, strong) IBOutlet UITableView *mainTableView;

@property (nonatomic, strong) NSMutableArray *cellsArray;
@property (nonatomic, strong) IBOutlet UITableViewCell *cell1;
@property (nonatomic, strong) IBOutlet UITextField *priceTextField;

@property (nonatomic, strong) IBOutlet UITableViewCell *cell2;
@property (nonatomic, strong) IBOutlet UITextField *feeTextField;

@end
