//
//  ALSoldOrderContentViewController.h
//  alorth
//
//  Created by John Hsu on 2015/6/30.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicViewController.h"

#import "ALSoldOrderListTableViewCell.h"
#import "ALSoldOrderListViewController.h"

@interface ALSoldOrderContentViewController : ALBasicSubViewController
{
    ALSoldOrderListTableViewCell *topCell;
}

-(instancetype)initWithInfoDict:(NSDictionary *)infoDict;

@property (nonatomic, strong) NSDictionary *infoDict;
@property (nonatomic, weak) ALSoldOrderListViewController *tableDelegate;

@property (nonatomic, strong) IBOutlet UIScrollView *mainScrollView;
@property (nonatomic, strong) IBOutlet UIView *mainScrollViewContentView;

@property (nonatomic, strong) IBOutlet UIView *cellContainer;

@property (nonatomic, strong) IBOutlet UILabel *productConditionLabel;
@property (nonatomic, strong) IBOutlet UILabel *productSizeLabel;
@property (nonatomic, strong) IBOutlet UILabel *productNumberLabel;
@property (nonatomic, strong) IBOutlet UILabel *productShippingFeeLabel;
@property (nonatomic, strong) IBOutlet UILabel *productTotalPriceLabel;

@property (nonatomic, strong) IBOutlet UILabel *buyerNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *buyerAddressLabel;
@property (nonatomic, strong) IBOutlet UILabel *buyerTelephoneLabel;

@property (nonatomic) BOOL isSellerInfomationExist;
@property (nonatomic, strong) IBOutlet UILabel *sellerNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *sellerAddressLabel;
@property (nonatomic, strong) IBOutlet UILabel *sellerTelephoneLabel;
@property (nonatomic, strong) IBOutlet UILabel *sellerPublicInfoLabel;

@property (nonatomic, strong) IBOutlet UILabel *sellerAccountLabel;
@property (nonatomic, strong) IBOutlet UILabel *sellerOrderNumberLabel;
@property (nonatomic, strong) IBOutlet UILabel *sellerOrderTimeLabel;

@property (nonatomic, strong) IBOutlet UIView *buttonsView;
@property (nonatomic, strong) IBOutlet UIButton *cancelButton;
@property (nonatomic, strong) IBOutlet UIButton *editButton;

-(void)changeInfoDict:(NSDictionary *)infoDict;

@end
