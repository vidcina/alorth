//
//  ALSoldOrderListTableViewCell.m
//  alorth
//
//  Created by Chih-Ju Huang on 2015/7/16.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALSoldOrderListTableViewCell.h"

#define TEXT_GRAY_COLOR [UIColor colorWithRed:50/255.0f green:50/255.0f blue:50/255.0f alpha:1]
#define TEXT_RED_COLOR [UIColor redColor]

@implementation ALSoldOrderListTableViewCell

+ (ALSoldOrderListTableViewCell *)cell
{
    ALSoldOrderListTableViewCell *cell =
    [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] lastObject];
    return cell;
}

+ (CGFloat)performCell:(ALSoldOrderListTableViewCell *)cell withDictionary:(NSDictionary *)dictionary
{
    cell.soldOrderDictionary = dictionary;
    
    //商品資料
    {
        if ([dictionary[@"ProductImage"] isKindOfClass:[NSString class]]) {
            [cell.productImageView imageFromURLString:dictionary[@"ProductImage"]];
        }
        
        cell.productTitleLabel.text = dictionary[@"ProductTitle"];;
        cell.productTitleLabel.numberOfLines = 0;
        
        cell.priceLabel.text = [NSString stringWithFormat:@"%.2f %@",
                                [dictionary[@"Price"] floatValue],
                                dictionary[@"Currency"]];
    }
    
    //所剩時間
    {
        NSString *timestamp = nil;
        
        if ([dictionary[@"OrderStatus"] intValue] == 1) {
            timestamp = dictionary[@"PayDeadline"];
        }
        else if ([dictionary[@"OrderStatus"] intValue] == 0) {
            timestamp = dictionary[@"HideTime"];
        }
        else if ([dictionary[@"OrderStatus"] intValue] == 2) {
            NSString *commentID = dictionary.safe[@"CommentID"];
            if (commentID.length > 0) timestamp = dictionary[@"HideTime"];
        }

        if (timestamp) {
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:[timestamp doubleValue]];
            cell.restTimeLabel.localText = [ALFormatter localTextOrderPaymentCountdownFormatForDate:date];
            cell.restTimeLabel.hidden = NO;
        }
        else {
            cell.restTimeLabel.hidden = YES;
        }
    }
    
    //狀態表示
    cell.connectBuyerImageView.image =
    [UIImage reSizeImage:[UIImage imageNamed:@"RateIM"]
                  toSize:cell.connectBuyerImageView.frame.size];
    
    if ([dictionary[@"OrderStatus"] intValue] == 0) {
        //被取消訂單
        cell.statusImageView.image =
        [UIImage reSizeImage:[UIImage imageNamed:@"CancelClock"]
                      toSize:cell.statusImageView.frame.size];
        cell.statusLabel.localText = @"Cancel";
    }
    else if ([dictionary[@"OrderStatus"] intValue] == 1) {
        //正在等待付款
        cell.statusImageView.image =
        [UIImage reSizeImage:[UIImage imageNamed:@"RateClock"]
                      toSize:cell.statusImageView.frame.size];
        cell.statusLabel.localText = @"Waiting for payment";
    }
    else if ([dictionary[@"OrderStatus"] intValue] == 2) {
        //已經下單開始評論
        cell.statusImageView.image =
        [UIImage reSizeImage:[UIImage imageNamed:@"FinishClock"]
                      toSize:cell.statusImageView.frame.size];
        cell.statusLabel.localText = @"Transaction Completed";
    }
    else {
        cell.statusImageView.image = nil;
        cell.statusLabel.localText = @"";
        ALLog(@"Order error");
    }
    
    //聯絡買家
    
    //評論
    if ([dictionary[@"OrderStatus"] intValue] == 0) {
        //取消的東西
        cell.replyButton.hidden = YES;
    }
    else if ([dictionary[@"OrderStatus"] intValue] == 1) {
        //正在移交
        cell.replyButton.hidden = YES;
    }
    else if ([dictionary[@"OrderStatus"] intValue] == 2) {
        //互相評論
        cell.replyButton.hidden = NO;
    }
    
    //顯示背景
    #if TARGET_IPHONE_SIMULATOR && 0
    if ([dictionary[@"OrderStatus"] intValue] == 0) {
        //被取消訂單
        cell.contentView.backgroundColor = [UIColor redColor];
    }
    else if ([dictionary[@"OrderStatus"] intValue] == 1) {
        //正在等待付款
        cell.contentView.backgroundColor = [UIColor greenColor];
    }
    else if ([dictionary[@"OrderStatus"] intValue] == 2) {
        //已經下單開始評論
        cell.contentView.backgroundColor = [UIColor yellowColor];
    }
    #endif
    
    return cell.lowerHorizontalLine.frame.origin.y + 1;
}

- (IBAction)topAction:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(didPressedTopContext:)]) {
        [self.delegate didPressedTopContext:self];
    }
}

- (IBAction)chatAction:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(didPressedChatButton:)]) {
        [self.delegate didPressedChatButton:self];
    }
}

- (IBAction)rightBottomAction:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(didPressedRightBottomButton:)]) {
        [self.delegate didPressedRightBottomButton:self];
    }
}


@end
