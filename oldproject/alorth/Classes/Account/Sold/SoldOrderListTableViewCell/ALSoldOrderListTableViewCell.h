//
//  ALSoldOrderListTableViewCell.h
//  alorth
//
//  Created by Chih-Ju Huang on 2015/7/16.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ALSoldOrderListTableViewCellDelegate;
@interface ALSoldOrderListTableViewCell : UITableViewCell

@property (nonatomic, strong) NSDictionary *soldOrderDictionary;
@property (nonatomic, weak) id<ALSoldOrderListTableViewCellDelegate> delegate;

//上區
@property (nonatomic, weak) IBOutlet UIImageView *productImageView;
@property (nonatomic, weak) IBOutlet UILabel *productTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *priceLabel;

@property (nonatomic, weak) IBOutlet UILabel *restTimeLabel;
@property (nonatomic, weak) IBOutlet UIImageView *statusImageView;
@property (nonatomic, weak) IBOutlet UILabel *statusLabel;

@property (nonatomic, weak) IBOutlet UIButton *mainFakeButton;

//左下區
@property (nonatomic, weak) IBOutlet UIImageView *connectBuyerImageView;
@property (nonatomic, weak) IBOutlet UILabel *connectBuyerLabel;
@property (nonatomic, weak) IBOutlet UIButton *connectBuyerButton;

//右下區
@property (nonatomic, weak) IBOutlet UIButton *replyButton;

//底標線
@property (nonatomic, weak) IBOutlet UIView *middleHorizontalLine;
@property (nonatomic, weak) IBOutlet UIView *lowerHorizontalLine; //用來行量高度

+ (ALSoldOrderListTableViewCell *)cell;
+ (CGFloat)performCell:(ALSoldOrderListTableViewCell *)cell withDictionary:(NSDictionary *)dictionary;

@end

#pragma mark - delegate
@protocol ALSoldOrderListTableViewCellDelegate <NSObject>
@optional
- (void)didPressedTopContext:(ALSoldOrderListTableViewCell *)cell;
- (void)didPressedChatButton:(ALSoldOrderListTableViewCell *)cell;
- (void)didPressedRightBottomButton:(ALSoldOrderListTableViewCell *)cell;
@end

