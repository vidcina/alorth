//
//  ALSoldOrderListViewController.m
//  alorth
//
//  Created by John Hsu on 2015/6/30.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALSoldOrderListViewController.h"
#import "ALSoldOrderContentViewController.h"

#import "ALReplyViewController.h"

@interface ALSoldOrderListViewController ()

@end

@implementation ALSoldOrderListViewController

#pragma mark - VC Life
-(instancetype)init
{
    self = [super init];
    if (self) {
        goodsListArray = [NSMutableArray array];
        
        currentTableViewControl = [[ALTableViewControl alloc] init];
        currentTableViewControl.delegate = self;
        currentTableViewControl.apiDataArray = goodsListArray;
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self alTableViewSetting:self.tableView];
    if (goodsListArray.count == 0) {
        [self checkNewData:currentTableViewControl];
    }
}

- (void)testDictionary
{
    NSDictionary *dict = @{@"ProductID" : @"1234",
                           @"ProductTitle" : @"A018_模擬器測試資料",
                           @"StockID" : @"",
                           @"OrderID" : @"",
                           @"ProductClip" : @"",
                           @"ProductImage" : @"http://four.cdn.directmailmac.com/images/send_icon@2x.png",
                           @"ProductSize" : @"",
                           @"Price" : @"350",
                           @"Currency" : @"",
                           @"Location" : @"",
                           apiKeyIsUsed : @"",
                           @"Amount" : @"",
                           @"TransType" : @"",
                           @"TransPrice" : @"",
                           @"BuyerID" : @"",
                           @"BuyerName" : @"",
                           @"BuyerPhone" : @"",
                           @"BuyerAddress" : @"",
                           @"SellerPublicInfo" : @"",
                           @"PayType" : ALDealMySelfPayType,
                           @"OrderTime" : @"",
                           @"PayDeadline" : [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] + arc4random_uniform(24 * 60 * 60)]};
    [goodsListArray addObject:dict];
}

#pragma mark - UITableView Delegate & DataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (demoCell == nil) {
        demoCell = [ALSoldOrderListTableViewCell cell];
        demoCell.frame = CGRectMake(0, 0, tableView.contentSize.width, 0);
        [demoCell layoutIfNeeded];
    }
    return [ALSoldOrderListTableViewCell performCell:demoCell withDictionary:goodsListArray[indexPath.row]] + 15;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [goodsListArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reuseIdentifier = @"reuseIdentifier";
    ALSoldOrderListTableViewCell *cell =
    (ALSoldOrderListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (!cell) {
        cell = [ALSoldOrderListTableViewCell cell];
        cell.delegate = self;
    }
    [ALSoldOrderListTableViewCell performCell:cell withDictionary:goodsListArray[indexPath.row]];
    return cell;
}

#pragma mark - scrollView Delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == _tableView) {
        [currentTableViewControl scrollViewDidScroll:scrollView];
    }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView == _tableView) {
        [currentTableViewControl scrollViewDidEndDragging:scrollView willDecelerate:(BOOL)decelerate];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == _tableView) {
        [currentTableViewControl scrollViewDidEndDecelerating:(UIScrollView *)scrollView];
    }
}

#pragma mark - ALTableViewControlDelegate
-(void)checkNewData
{
    [self checkNewData:currentTableViewControl];
}

-(void)checkNewData:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    
    willReplaceWithNew = YES;
    tableViewControl.lastDict[@"StartNum"] = @"0";
    
    ALLog(@"重新整理");
    [self addMBProgressHUDWithKey:@"mySoldOrderListA018"];
    [self goodsListLoadApiSourceParameters:tableViewControl.lastDict
                               FinishBlock:^(BOOL isSuccess) {
                                   [self removeMBProgressHUDWithKey:@"mySoldOrderListA018"];
                               }];
}

-(void)loadMore:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    
    ALLog(@"載入更多:目前第 %@ 頁 %lu 筆資料",tableViewControl.lastDict[@"StartNum"],(unsigned long)goodsListArray.count);
    
    tableViewControl.lastDict[@"StartNum"] =
    [NSString stringWithFormat:@"%d",[tableViewControl.lastDict[@"StartNum"] intValue] + 1];
    
    [self addMBProgressHUDWithKey:@"mySoldOrderListA018"];
    [self goodsListLoadApiSourceParameters:tableViewControl.lastDict
                               FinishBlock:^(BOOL isSuccess) {
                                   [self removeMBProgressHUDWithKey:@"mySoldOrderListA018"];
                               }];
}

#pragma mark - API
-(void)goodsListLoadApiSourceParameters:(NSDictionary *)parameters
                            FinishBlock:(void (^)(BOOL isSuccess))finishBlockToRun
{
    [ALNetWorkAPI mySoldOrderListA018StartNum:parameters[@"StartNum"]
                              CompletionBlock:^(NSDictionary *responseObject) {

                                  BOOL isWiseData = YES;
                                  if (![responseObject[@"resBody"] isKindOfClass:[NSDictionary class]]) {
                                      isWiseData = NO;
                                      
                                      if (![responseObject[@"resBody"][@"OrderList"] isKindOfClass:[NSArray class]]) {
                                          isWiseData = NO;
                                      }
                                  }
                                  
                                  if (isWiseData) {
                                      if (willReplaceWithNew) {
                                          [goodsListArray removeAllObjects];
                                          willReplaceWithNew = NO;
                                      }
                                      
                                      NSArray *orderList = responseObject[@"resBody"][@"OrderList"];
                                      if ([orderList count] > 0) {
                                          [goodsListArray addObjectsFromArray:orderList];
                                      }
                                      else {
                                            #if TARGET_IPHONE_SIMULATOR && UseDevelopmentServer
                                          [self testDictionary];
                                            #endif
                                      }
                                      [self.tableView reloadData];
                                  }
                                  else {
                                      ALLog(@"A018_resBody_OrderList_格式錯誤");
                                  }
                                  finishBlockToRun(YES);
    }];
}

#pragma mark - ALSoldOrderListTableViewCell delegate
- (void)didPressedTopContext:(ALSoldOrderListTableViewCell *)cell
{
    NSDictionary *soldOrderDictionsry = cell.soldOrderDictionary;
    ALSoldOrderContentViewController *vc =
    [[ALSoldOrderContentViewController alloc] initWithInfoDict:soldOrderDictionsry];
    vc.tableDelegate = self;
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didPressedChatButton:(ALSoldOrderListTableViewCell *)cell
{
    NSDictionary *soldOrderDictionsry = cell.soldOrderDictionary;
    NSString *otherId = [soldOrderDictionsry[@"BuyerID"] description];
    [self chatWithOtherId:otherId];
}

- (void)didPressedRightBottomButton:(ALSoldOrderListTableViewCell *)cell
{
    if ([cell.soldOrderDictionary[@"OrderStatus"] intValue] == 2) {
    }
    else {
        ALLog(@"cell 動作設定錯誤");
        return;
    }
    
    NSString *commentID = nil;
    if ([cell.soldOrderDictionary[@"CommentID"] isKindOfClass:[NSString class]]) {
        commentID = cell.soldOrderDictionary[@"CommentID"];
    }
    
    if (commentID.length > 0) {
        [ALNetWorkAPI categoryCommentA031OrderID:cell.soldOrderDictionary[@"OrderID"]
                                        startNum:@"0"
                                 completionBlock:^(NSDictionary *responseObject) {
                                     
                                     BOOL isWiseData = YES;
                                     if (![responseObject[@"resBody"] isKindOfClass:[NSDictionary class]]) {
                                         isWiseData = NO;
                                         
                                         if (![responseObject[@"resBody"][@"CommentList"] isKindOfClass:[NSArray class]]) {
                                             isWiseData = NO;
                                         }
                                     }
                                     
                                     if (isWiseData) {
                                         [self startAddReply:responseObject[@"resBody"][@"CommentList"][0]];
                                     }
                                     else {
                                         ALLog(@"A031O回傳格式錯誤");
                                     }
                                 }];
    }
    else {
        YKSimpleAlert(ALLocalizedString(@"Waiting for comment",nil));
    }
}

-(void)startAddReply:(NSDictionary *)commentDict
{
    NSMutableDictionary *dataDict = [commentDict mutableCopy];
    dataDict[ALReplyCommentOwnerKey] = [ALAppDelegate sharedAppDelegate].userID;
    
    ALReplyViewController *next = [[ALReplyViewController alloc] initWithInfoDict:dataDict];
    [self.navigationController pushViewController:next animated:YES];
}

@end
