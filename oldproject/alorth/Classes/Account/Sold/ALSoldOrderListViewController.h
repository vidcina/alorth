//
//  ALSoldOrderListViewController.h
//  alorth
//
//  Created by John Hsu on 2015/6/30.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicViewController.h"
#import "ALSoldOrderListTableViewCell.h"

@interface ALSoldOrderListViewController : ALBasicSubViewController
<UITableViewDataSource, UITableViewDelegate, ALTableViewControlDelegate,
ALSoldOrderListTableViewCellDelegate>
{
    NSMutableArray *goodsListArray;
    
    BOOL willReplaceWithNew;
    ALTableViewControl *currentTableViewControl;
    
    ALSoldOrderListTableViewCell *demoCell;
}

@property (nonatomic,strong)IBOutlet UITableView *tableView;
-(void)checkNewData;

@end
