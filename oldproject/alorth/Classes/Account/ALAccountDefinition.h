//
//  ALAccountDefinition.h
//  alorth
//
//  Created by w91379137 on 2015/6/24.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#ifndef alorth_ALAccountDefinition_h
#define alorth_ALAccountDefinition_h

static NSString * const accountIconKey      = @"icon";
static NSString * const accountTitleKey     = @"title";
static NSString * const accountActionKey    = @"action";

#endif
