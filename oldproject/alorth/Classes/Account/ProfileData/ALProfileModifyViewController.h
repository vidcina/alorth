//
//  ALProfileModifyViewController.h
//  alorth
//
//  Created by w91379137 on 2015/8/15.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicSubViewController.h"
@protocol ALProfileModifyViewControllerDelegate;

//Few
static NSString *ALProfileModifyName        = @"ALProfileModifyName";
static NSString *ALProfileModifyAddress     = @"ALProfileModifyAddress";
static NSString *ALProfileModifyPublicInfo  = @"ALProfileModifyPublicInfo";
static NSString *ALProfileModifyPhone       = @"ALProfileModifyPhone";
static NSString *ALProfileModifyShippingFee = @"ALProfileModifyShippingFee";
static NSString *ALProfileModifyGender      = @"ALProfileModifyGender";
static NSString *ALProfileModifyBirthday    = @"ALProfileModifyBirthday";

//List
static NSString *ALProfileModifyLocation    = @"ALProfileModifyLocation";
static NSString *ALProfileModifyLanguage    = @"ALProfileModifyLanguage";

typedef NS_ENUM(NSUInteger, ALProfileModifySelectType) {
    ALProfileModifySelectTypeFew,
    ALProfileModifySelectTypeList,
    ALProfileModifySelectTypeEnd
};

static const NSInteger limitTextViewWordCount = 140;

@interface ALProfileModifyViewController : ALBasicSubViewController
<UITableViewDataSource, UITableViewDelegate>

+ (BOOL)isALProfileCanModifyType:(NSString *)key;

@property (nonatomic, weak) id<ALProfileModifyViewControllerDelegate> delegate;
@property (nonatomic, strong) NSDictionary *infoDict;
@property (nonatomic, strong) NSString *modifykey;

#pragma mark -
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic) ALProfileModifySelectType tableViewType;
@property (nonatomic, strong) IBOutlet UIButton *titleBarSaveButton;
@property (nonatomic, strong) IBOutlet UITableView *mainTableView;

#pragma mark - 1
@property (nonatomic, strong) NSMutableArray *cellsArray;

@property (nonatomic, strong) IBOutlet UITableViewCell *textField_1_Cell;
@property (nonatomic, strong) IBOutlet UILabel *textTitle_1;
@property (nonatomic, strong) IBOutlet UITextField *textField_1;

@property (nonatomic, strong) IBOutlet UITableViewCell *textField_2_Cell;
@property (nonatomic, strong) IBOutlet UILabel *textTitle_2;
@property (nonatomic, strong) IBOutlet UITextField *textField_2;

@property (nonatomic, strong) IBOutlet UITableViewCell *textView_1_Cell;
@property (nonatomic, strong) IBOutlet UILabel *textView_1_countLabel;
@property (nonatomic, strong) IBOutlet UITextView *textView_1;

@property (nonatomic, strong) IBOutlet UITableViewCell *saveButton_Cell;
@property (nonatomic, strong) IBOutlet UIButton *saveButton;

@property (nonatomic, strong) IBOutlet UIDatePicker *birthdayPicker;
@property (nonatomic, strong) NSDateFormatter *birthdayDateFormatter;

#pragma mark - 2
@property (nonatomic, strong) NSArray *dataArray;
@property (nonatomic, strong) IBOutlet UIView *modifyLocationHeaderView;
@property (nonatomic, strong) IBOutlet UILabel *selectLocationLabel;

@property (nonatomic, strong) IBOutlet UIView *modifyLanguageHeaderView;
@property (nonatomic, strong) IBOutlet UILabel *selectLanguageLabel;

@property (nonatomic, strong) IBOutlet UIView *modifyGenderHeaderView;
@property (nonatomic, strong) NSString *genderString;

@property (nonatomic, strong) IBOutlet UIButton *maleButton;
@property (nonatomic, strong) IBOutlet UIImageView *maleImageView;
@property (nonatomic, strong) IBOutlet UIButton *femaleButton;
@property (nonatomic, strong) IBOutlet UIImageView *femaleImageView;
@property (nonatomic, strong) IBOutlet UIButton *secretButton;
@property (nonatomic, strong) IBOutlet UIImageView *secretImageView;

@end

@protocol ALProfileModifyViewControllerDelegate <NSObject>

-(void)savaButtonAction:(ALProfileModifyViewController *)profileModifyViewController;

@end
