//
//  UIViewController+ALProfile.h
//  alorth
//
//  Created by w91379137 on 2015/9/10.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ALProfileModifyViewController.h"

@interface UIViewController (ALProfile)

-(void)saveUserProfile:(NSDictionary *)apiDataDict;

@end
