//
//  UIViewController+ALProfile.m
//  alorth
//
//  Created by w91379137 on 2015/9/10.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "UIViewController+ALProfile.h"

@implementation UIViewController (ALProfile)

#pragma mark - Save Funciotn
-(void)saveUserProfile:(NSDictionary *)apiDataDict
{
    [self addMBProgressHUDWithKey:@"PersonalProfile021"];
    [ALNetWorkAPI setPersonalProfile021HeadImage:apiDataDict[@"HeadImage"]
                                       FirstName:apiDataDict[@"FirstName"]
                                        LastName:apiDataDict[@"LastName"]
                                          Gender:apiDataDict[@"Gender"]
                                        Birthday:apiDataDict[@"Birthday"]
                                        Location:apiDataDict[@"Location"]
                                         Address:apiDataDict[@"Address"]
                                           Phone:apiDataDict[@"Phone"]
                                      TransPrice:apiDataDict[@"TransPrice"]
                                      PublicInfo:apiDataDict[@"PublicInfo"]
                                 CompletionBlock:^(NSDictionary *responseObject) {
                                     [self removeMBProgressHUDWithKey:@"PersonalProfile021"];
                                     
                                     if ([ALNetWorkAPI checkSerialNumber:@"A021"
                                                          ResponseObject:responseObject]) {
                                         
                                         //調整其他資料
                                         [[ALAppDelegate sharedAppDelegate] userInfomationRenewWithPersonalProfile021:apiDataDict];
                                         
                                         //調整使用貨幣
                                         [[ALAppDelegate sharedAppDelegate]
                                          userInfomationRenewWithPersonalProfile021:responseObject[@"resBody"]];
                                     }
                                     else {
                                         YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
                                     }
                                 }];
}

@end
