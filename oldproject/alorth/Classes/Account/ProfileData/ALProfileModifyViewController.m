//
//  ALProfileModifyViewController.m
//  alorth
//
//  Created by w91379137 on 2015/8/15.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALProfileModifyViewController.h"

@interface ALProfileModifyViewController ()

@end

@implementation ALProfileModifyViewController

+(BOOL)isALProfileCanModifyType:(NSString *)key
{
    NSArray *keys = @[ALProfileModifyName,
                      ALProfileModifyAddress,
                      ALProfileModifyPublicInfo,
                      ALProfileModifyPhone,
                      ALProfileModifyShippingFee,
                      ALProfileModifyGender,
                      ALProfileModifyBirthday,
                      
                      ALProfileModifyLocation,
                      ALProfileModifyLanguage];
    return [keys containsObject:key];
}

#pragma mark - VC Life
- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.birthdayDateFormatter =
        [ALFormatter dateFormatteWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:8 * 3600]
                                 FormatString:@"yyyy-MM-dd"];
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.mainTableView.tableFooterView = [[UIView alloc] init];
    self.saveButton.normalTitle = @"Save";
    
    NSMutableArray *cellsArray = [NSMutableArray array];
    
    if ([self.modifykey isEqualToString:ALProfileModifyName]) {
        
        self.tableViewType = ALProfileModifySelectTypeFew;
        self.titleLabel.localText = @"Name";
        
        {
            self.textTitle_1.localText = @"First Name";
            
            if ([self.infoDict[@"FirstName"] isKindOfClass:[NSString class]]) {
                self.textField_1.text = self.infoDict[@"FirstName"];
            }
            self.textField_1.localPlaceholder = @"First Name";
            
            [cellsArray addObject:self.textField_1_Cell];
        }
        {
            self.textTitle_2.localText = @"Last Name";
            
            if ([self.infoDict[@"LastName"] isKindOfClass:[NSString class]]) {
                self.textField_2.text = self.infoDict[@"LastName"];
            }
            self.textField_2.localPlaceholder = @"Last Name";
            
            [cellsArray addObject:self.textField_2_Cell];
        }
    }
    
    if ([self.modifykey isEqualToString:ALProfileModifyLocation]) {
        
        self.tableViewType = ALProfileModifySelectTypeList;
        self.titleLabel.localText = @"Location";
        
        if ([self.infoDict[@"Location"] isKindOfClass:[NSString class]]) {
            self.selectLocationLabel.text = self.infoDict[@"Location"];
        }
        
        self.dataArray = [ALBundleTextResources countryCodeArray];
        self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    }
    
    if ([self.modifykey isEqualToString:ALProfileModifyAddress]) {
        
        self.tableViewType = ALProfileModifySelectTypeFew;
        self.titleLabel.localText = @"Address";
        {
            if ([self.infoDict[@"Address"] isKindOfClass:[NSString class]]) {
                self.textView_1.text = self.infoDict[@"Address"];
            }
            [self textViewDidChange:self.textView_1];
            [cellsArray addObject:self.textView_1_Cell];
        }
    }
    
    if ([self.modifykey isEqualToString:ALProfileModifyPublicInfo]) {
        
        self.tableViewType = ALProfileModifySelectTypeFew;
        self.titleLabel.localText = @"My Public Info";
        {
            if ([self.infoDict[@"PublicInfo"] isKindOfClass:[NSString class]]) {
                self.textView_1.text = self.infoDict[@"PublicInfo"];
            }
            [self textViewDidChange:self.textView_1];
            [cellsArray addObject:self.textView_1_Cell];
        }
    }
    
    if ([self.modifykey isEqualToString:ALProfileModifyPhone]) {
        
        self.tableViewType = ALProfileModifySelectTypeFew;
        self.titleLabel.localText = @"Phone";
        
        {
            self.textTitle_1.localText = @"Phone";
            
            if ([self.infoDict[@"Phone"] isKindOfClass:[NSString class]]) {
                self.textField_1.text = self.infoDict[@"Phone"];
            }
            self.textField_1.localPlaceholder = @"Phone";
            
            [cellsArray addObject:self.textField_1_Cell];
        }
    }
    
    if ([self.modifykey isEqualToString:ALProfileModifyShippingFee]) {
        
        self.tableViewType = ALProfileModifySelectTypeFew;
        self.titleLabel.localText = @"Shipping fee";
        
        {
            self.textTitle_1.localText = @"Shipping fee";
            
            if ([self.infoDict[@"TransPrice"] isKindOfClass:[NSString class]]) {
                self.textField_1.text = self.infoDict[@"TransPrice"];
            }
            self.textField_1.localPlaceholder = @"Shipping fee";
            
            [cellsArray addObject:self.textField_1_Cell];
        }
    }
    
    if ([self.modifykey isEqualToString:ALProfileModifyGender]) {
        self.tableViewType = ALProfileModifySelectTypeFew;
        self.titleLabel.localText = @"Gender";
        self.genderString = self.infoDict[@"Gender"];
        
        self.mainTableView.tableHeaderView = self.modifyGenderHeaderView;
    }
    
    if ([self.modifykey isEqualToString:ALProfileModifyBirthday]) {
        
        self.tableViewType = ALProfileModifySelectTypeFew;
        self.titleLabel.localText = @"Birthday";
        
        {
            self.textTitle_1.localText = @"Birthday";
            
            if ([self.infoDict[@"Birthday"] isKindOfClass:[NSString class]]) {
                self.textField_1.text = self.infoDict[@"Birthday"];
            }
            self.textField_1.localPlaceholder = @"Birthday";
            
            [cellsArray addObject:self.textField_1_Cell];
            
            self.textField_1.inputView = self.birthdayPicker;
            
            NSDate *date =
            [self.birthdayDateFormatter dateFromString:self.textField_1.text];
            
            if (date == nil) {
                date = [NSDate date];
            }
            self.birthdayPicker.date =
            self.birthdayPicker.maximumDate = [NSDate date];
        }
    }
    
    if ([self.modifykey isEqualToString:ALProfileModifyLanguage]) {
        
        self.tableViewType = ALProfileModifySelectTypeList;
        self.titleLabel.localText = @"Language";
        
        self.selectLanguageLabel.localText = [ALLanguageCenter languageName:[ALLanguageCenter sharedInstance].managerLanguage];
        
        NSMutableArray *languages = [NSMutableArray array];
        for (NSInteger k = 0; k < LanguageCenterStatusEnd; k++) {
            [languages addObject:@(k)];
        }
        self.dataArray = languages;
        self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    }
    
    if (self.tableViewType == 0) {
        self.titleBarSaveButton.hidden = NO;
        //[cellsArray addObject:self.saveButton_Cell];
    }
    else {
        self.titleBarSaveButton.hidden = YES;
    }
    
    self.cellsArray = cellsArray;
}

-(void)viewDidDisappear:(BOOL)animated
{

    [super viewDidDisappear:animated];
}

#pragma mark - setter
-(void)setCellsArray:(NSMutableArray *)cellsArray
{
    _cellsArray = cellsArray;
    [self.mainTableView reloadData];
}

-(void)setGenderString:(NSString *)genderString
{
    _genderString = genderString;
    self.maleImageView.hidden = ![[_genderString lowercaseString] isEqualToString:@"male"];
    self.femaleImageView.hidden = ![[_genderString lowercaseString] isEqualToString:@"female"];
    self.secretImageView.hidden = ![[_genderString lowercaseString] isEqualToString:@"secret"];
}

#pragma mark - IBAction
- (IBAction)leftButtonAction:(id)sender
{
    [self keyboardReturn];
    if (self.tableViewType == 1) {
        if ([self.modifykey isEqualToString:ALProfileModifyLocation]) {
            if (![self.selectLocationLabel.text isEqualToString:self.infoDict[@"Location"]]) {
                [self savaButtonAction:sender];
                return;
            }
        }
    }
    [self back:sender];
}

- (IBAction)savaButtonAction:(id)sender
{
    [self keyboardReturn];
    [self.delegate savaButtonAction:self];
    [self back:sender];
}

- (IBAction)genderButtonAction:(id)sender
{
    if (sender == self.maleButton) {
        self.genderString = @"Male";
    }
    if (sender == self.femaleButton) {
        self.genderString = @"Female";
    }
    if (sender == self.secretButton) {
        self.genderString = @"Secret";
    }
}

#pragma mark - UITableView Datasource - Setion
//self.tableViewType

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ([self.modifykey isEqualToString:ALProfileModifyLocation]) {
        return self.modifyLocationHeaderView.frame.size.height;
    }
    if ([self.modifykey isEqualToString:ALProfileModifyLanguage]) {
        return self.modifyLanguageHeaderView.frame.size.height;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if ([self.modifykey isEqualToString:ALProfileModifyLocation]) {
        return self.modifyLocationHeaderView;
    }
    if ([self.modifykey isEqualToString:ALProfileModifyLanguage]) {
        return self.modifyLanguageHeaderView;
    }
    return [[UIView alloc] initWithFrame:CGRectZero];
}


#pragma mark - UITableView Datasource - Row
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (self.tableViewType) {
        case ALProfileModifySelectTypeList:
        {
            return self.dataArray.count;
        }
            break;
            
        default:
        {
            return self.cellsArray.count;
        }
            break;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (self.tableViewType) {
        case ALProfileModifySelectTypeList:
        {
            return 50;
        }
            break;
            
        default:
        {
            UITableViewCell *cell = self.cellsArray[indexPath.row];
            return cell.frame.size.height;
        }
            break;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (self.tableViewType) {
        case ALProfileModifySelectTypeList:
        {
            UITableViewCell *cell = nil;
            
            if ([self.modifykey isEqualToString:ALProfileModifyLocation] ||
                [self.modifykey isEqualToString:ALProfileModifyLanguage]) {
                
                UIFont *font = [UIFont systemFontOfSize:14];
                
                cell =
                [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"CELL"];
                
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                
                cell.textLabel.font = font;
                cell.textLabel.textColor = COMMON_GRAY4_COLOR;
                
                cell.detailTextLabel.font = font;
                cell.detailTextLabel.textColor = COMMON_GRAY4_COLOR;
                
                if ([self.modifykey isEqualToString:ALProfileModifyLocation]) {
                    cell.textLabel.text =
                    [NSString stringWithFormat:@"%@ (%@)",
                     self.dataArray[indexPath.row][@"name"],
                     self.dataArray[indexPath.row][@"alpha-2"]];
                }
                if ([self.modifykey isEqualToString:ALProfileModifyLanguage]) {
                    
                    NSMutableString *allText = [NSMutableString string];
                    
                    //以該語言 顯示
                    [allText appendFormat:@"%@", [ALLanguageCenter translationOfString:[ALLanguageCenter languageName:indexPath.row] language:indexPath.row]];
                    
                    if ([ALLanguageCenter sharedInstance].managerLanguage != indexPath.row) {
                        
                        //以現行語言 顯示
                        [allText appendFormat:@"($%@$)",[ALLanguageCenter languageName:indexPath.row]];
                    }
                    
                    cell.textLabel.localText = allText;
                }
            }
            
            if (cell == nil) {
                cell = [self defaultUITableViewCell];
            }
            return cell;
        }
            break;
            
        default:
        {
            return self.cellsArray[indexPath.row];
        }
            break;
    }
}

#pragma mark - UITableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([self.modifykey isEqualToString:ALProfileModifyLocation]) {
        self.selectLocationLabel.text = self.dataArray[indexPath.row][@"alpha-2"];
        [self leftButtonAction:nil];
    }
    
    if ([self.modifykey isEqualToString:ALProfileModifyLanguage]) {
        [PDSAccountManager sharedManager].userLanguage = [ALLanguageCenter languageISO639Key:indexPath.row];
        [self back:nil];
    }
}

#pragma mark - UITextView Delegate
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (textView.text.length >= limitTextViewWordCount && text.length > 0) {
        return NO;
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
    if (textView == self.textView_1) {
        self.textView_1_countLabel.text =
        [NSString stringWithFormat:@"%d/%ld",
         (int)MIN(limitTextViewWordCount, textView.text.length),
         (long)limitTextViewWordCount];
    }
}

#pragma mark - UIPickerView DataSource
-(IBAction)birthdayPickerAction:(UIDatePicker *)sender
{
    self.textField_1.text = [self.birthdayDateFormatter stringFromDate:sender.date];
}

@end
