//
//  ALProfileAdrressViewController.m
//  alorth
//
//  Created by ZZB on 2016/5/22.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALProfileAdrressViewController.h"
#import "ALNetWorkAPI+V3_0.h"
#import "ALAddAddressViewController.h"

static NSString *addressCellIdentifier = @"addressCellIdentifier";
static NSString *addAddressCellIdentifier = @"addNewAddressCellIdentifier";


@interface ALProfileAdrressViewController ()

@property (nonatomic, strong) NSArray *addresses;

@end

@implementation ALProfileAdrressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:addressCellIdentifier];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:addAddressCellIdentifier];
    
    self.navigationController.navigationBar.tintColor = [UIColor darkGrayColor];
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.title = ALLocalizedString(@"Shipping Address", nil);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];

    [self refreshAddresses];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Privates

- (void)refreshAddresses
{
    [self addMBProgressHUDWithKey:@"fetchAddressA110"];
    
    [ALNetWorkAPI fetchExistedAddressA110CompletionBlock:^(NSDictionary *responseObject) {
        [self removeMBProgressHUDWithKey:@"fetchAddressA110"];
        
        if ([ALNetWorkAPI checkSerialNumber:@"A110"
                             ResponseObject:responseObject]) {
            NSDictionary *resBody = responseObject[@"resBody"];
            self.addresses = resBody[@"AddressList"];
            [self.tableView reloadData];
        }
        else {
            [self.navigationController popViewControllerAnimated:YES];
            YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        }
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.addresses.count > 0) {
        return 2;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.addresses.count > 0) {
        if (section == 0) {
            return self.addresses.count;
        }
    }
    // Add new address
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.addresses.count > 0 && indexPath.section == 0) {
        NSDictionary *addressInfo = [self.addresses objectAtIndex:indexPath.row];
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:addressCellIdentifier];

        if ([addressInfo[@"IsPrimary"] boolValue]) {
            cell.textLabel.text = ALLocalizedString(@"Primary Shipping Address", nil);
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else {
            cell.textLabel.text = @"";
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        cell.textLabel.font = [UIFont systemFontOfSize:14.0];
        cell.textLabel.textColor = [UIColor darkGrayColor];
        
        cell.detailTextLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.detailTextLabel.numberOfLines = 0;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ %@\n%@ %@\n%@ %@ %@\n%@\n%@", addressInfo[@"UserFirstName"], addressInfo[@"UserLastName"], addressInfo[@"Street1"], addressInfo[@"Street2"], addressInfo[@"City"], addressInfo[@"State"], addressInfo[@"PostalCode"], addressInfo[@"Country"], addressInfo[@"Phone"]];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:14.0];
        cell.detailTextLabel.textColor = [UIColor colorWithHexString:@"1B317A"];
        cell.tintColor = [UIColor colorWithHexString:@"1B317A"];

        return cell;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:addAddressCellIdentifier forIndexPath:indexPath];
    cell.textLabel.text = ALLocalizedString(@"Enter a New Address", nil);
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.addresses.count > 0 && indexPath.section == 0) {
        NSDictionary *addressInfo = [self.addresses objectAtIndex:indexPath.row];

        NSString *address = [NSString stringWithFormat:@"%@ %@\n%@ %@\n%@ %@ %@\n%@\n%@", addressInfo[@"UserFirstName"], addressInfo[@"UserLastName"], addressInfo[@"Street1"], addressInfo[@"Street2"], addressInfo[@"City"], addressInfo[@"State"], addressInfo[@"PostalCode"], addressInfo[@"Country"], addressInfo[@"Phone"]];
        
        UIFont *cellFont = [UIFont systemFontOfSize:14.0];
        NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:address attributes:@{NSFontAttributeName: cellFont}];
        CGRect rect = [attributedText boundingRectWithSize:CGSizeMake(tableView.bounds.size.width, CGFLOAT_MAX)options:NSStringDrawingUsesLineFragmentOrigin context:nil];

        NSString *primaryString = [addressInfo[@"IsPrimary"] boolValue]? ALLocalizedString(@"Primary Shipping Address", nil): @"";
        UIFont *primaryFont = [UIFont systemFontOfSize:14.0];
        NSAttributedString *primaryAttributedText = [[NSAttributedString alloc] initWithString:primaryString attributes:@{NSFontAttributeName: primaryFont}];
        CGRect primaryRect = [primaryAttributedText boundingRectWithSize:CGSizeMake(tableView.bounds.size.width, CGFLOAT_MAX)options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        
        return CGRectGetHeight(rect) + CGRectGetHeight(primaryRect) + 8.0;
    }
    return 52.0;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.addresses.count > 0 && indexPath.section == 0) {
        return YES;
    }
    return NO;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        return ALLocalizedString(@"Add Another Address", nil);
    }
    return nil;
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        NSDictionary *addressInfo = [self.addresses objectAtIndex:indexPath.row];

        [self addMBProgressHUDWithKey:@"deleteAddressA201"];

        [ALNetWorkAPI deleteAddressA201WithId:addressInfo[@"AddressID"] completionBlock:^(NSDictionary *responseObject) {
            
            [self removeMBProgressHUDWithKey:@"deleteAddressA201"];
            
            if ([ALNetWorkAPI checkSerialNumber:@"A201"
                                 ResponseObject:responseObject]) {                
                NSMutableArray *mutableAddress = [NSMutableArray arrayWithArray:self.addresses];
                [mutableAddress removeObject:addressInfo];
                self.addresses = [mutableAddress copy];
                [tableView reloadData];
            }
            else {
                YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
            }
        }];
    }
}

#pragma mark - UITableViewDelegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here, for example:
    // Create the next view controller.
    if (self.addresses.count > 0 && indexPath.section == 0) {
        NSDictionary *addressInfo = [self.addresses objectAtIndex:indexPath.row];
        [self addMBProgressHUDWithKey:@"setPrimaryAddressA202"];

        [ALNetWorkAPI setPrimaryAddressA202WithId:addressInfo[@"AddressID"] completionBlock:^(NSDictionary *responseObject) {
            
            [self removeMBProgressHUDWithKey:@"setPrimaryAddressA202"];
            
            if ([ALNetWorkAPI checkSerialNumber:@"A202"
                                 ResponseObject:responseObject]) {
                [self refreshAddresses];
            }
            else {
                YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
            }
        }];
    }
    else {
        ALAddAddressViewController *viewController = [[ALAddAddressViewController alloc] initWithNibName:@"ALAddAddressViewController" bundle:nil];
        viewController.infoDict = self.infoDict;
        // Push the view controller.
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

@end
