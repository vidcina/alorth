//
//  ALAddAddressViewController.m
//  alorth
//
//  Created by ZZB on 2016/5/23.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALAddAddressViewController.h"
#import "ALNetWorkAPI+V3_0.h"
#import "ALProfileModifyViewController.h"

static NSString *addressCellIdentifier = @"addressCellIdentifier";

@interface ALAddAddressViewController ()<ALProfileModifyViewControllerDelegate>

@property (nonatomic, strong) UITextField *firstNameField;
@property (nonatomic, strong) UITextField *lastNameField;
@property (nonatomic, strong) UITextField *street1Field;
@property (nonatomic, strong) UITextField *street2Field;
@property (nonatomic, strong) UITextField *cityField;
@property (nonatomic, strong) UITextField *stateField;
@property (nonatomic, strong) UITextField *postalCodeField;
@property (nonatomic, strong) UILabel *countryLabel;
@property (nonatomic, strong) UITextField *phoneField;
@property (nonatomic, strong) UILabel *currentCountryLabel;
@property (nonatomic, strong) UILabel *primaryLabel;
@property (nonatomic, strong) UISwitch *primarySwitch;
@property (nonatomic, strong) UIButton *saveButton;

- (IBAction)saveAddress:(id)sender;

@end

@implementation ALAddAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:addressCellIdentifier];
    
    self.navigationController.navigationBar.tintColor = [UIColor darkGrayColor];
    
    self.title = ALLocalizedString(@"New Address", nil);

    UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeSystem];
    
    saveButton.layer.cornerRadius = 3.0;
    saveButton.backgroundColor = [UIColor colorWithHexString:@"FFF911"];
    [saveButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(saveAddress:) forControlEvents:UIControlEventTouchUpInside];
    [saveButton setTitle:ALLocalizedString(@"Save", nil) forState:UIControlStateNormal];
    self.saveButton = saveButton;
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, CGRectGetWidth([UIScreen mainScreen].bounds), 60)];
    [footerView addSubview:self.saveButton];
    self.tableView.tableFooterView = footerView;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.saveButton.frame = CGRectMake(16.0, 8.0, CGRectGetWidth(self.view.bounds) - 16*2, 44.0);
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

- (IBAction)saveAddress:(id)sender
{
    if (self.firstNameField.text.length == 0
        || self.lastNameField.text.length == 0
        || self.street1Field.text.length == 0
        || self.cityField.text.length == 0
        || self.postalCodeField.text.length == 0
        || self.currentCountryLabel.text.length == 0) {
        YKSimpleAlert(ALLocalizedString(@"Please fill all required fields.",nil));
        return;
    }

    [self addMBProgressHUDWithKey:@"addAddressA111"];

    [ALNetWorkAPI addAddressA111WithFirstName:self.firstNameField.text lastName:self.lastNameField.text street1:self.street1Field.text street2:self.street2Field.text state:self.stateField.text city:self.cityField.text postalCode:self.postalCodeField.text phone:self.phoneField.text country:self.currentCountryLabel.text primary:self.primarySwitch.on completionBlock:^(NSDictionary *responseObject) {
        [self removeMBProgressHUDWithKey:@"addAddressA111"];
        if ([ALNetWorkAPI checkSerialNumber:@"A111"
                             ResponseObject:responseObject]) {
            [self.navigationController popViewControllerAnimated:YES];
        }
        else {
            YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        }
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:addressCellIdentifier forIndexPath:indexPath];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryNone;
    for (UIView *subview in cell.contentView.subviews) {
        if ([subview isKindOfClass:[UITextField class]]
            || [subview isKindOfClass:[UILabel class]]
            || [subview isKindOfClass:[UISwitch class]]) {
            [subview removeFromSuperview];
        }
    }
    
    switch (indexPath.row) {
        case 0:
        {
            if (!self.firstNameField) {
                self.firstNameField = [[UITextField alloc] initWithFrame:CGRectMake(16.0, 0.0, CGRectGetWidth(self.view.frame), 44.0)];
                self.firstNameField.placeholder = ALLocalizedString(@"First Name", nil);
                self.firstNameField.keyboardType = UIKeyboardTypeNamePhonePad;
            }
            [cell.contentView addSubview:self.firstNameField];
            break;
        }
        case 1:
        {
            if (!self.lastNameField) {
                self.lastNameField = [[UITextField alloc] initWithFrame:CGRectMake(16.0, 0.0, CGRectGetWidth(self.view.frame), 44.0)];
                self.lastNameField.placeholder = ALLocalizedString(@"Last Name", nil);
                self.lastNameField.keyboardType = UIKeyboardTypeNamePhonePad;
            }
            [cell.contentView addSubview:self.lastNameField];
            break;
        }
        case 2:
        {
            if (!self.street1Field) {
                self.street1Field = [[UITextField alloc] initWithFrame:CGRectMake(16.0, 0.0, CGRectGetWidth(self.view.frame), 44.0)];
                self.street1Field.placeholder = ALLocalizedString(@"Street1", nil);
                self.street1Field.keyboardType = UIKeyboardTypeDefault;
            }
            [cell.contentView addSubview:self.street1Field];
            break;
        }
        case 3:
        {
            if (!self.street2Field) {
                self.street2Field = [[UITextField alloc] initWithFrame:CGRectMake(16.0, 0.0, CGRectGetWidth(self.view.frame), 44.0)];
                self.street2Field.placeholder = ALLocalizedString(@"Street2(optional)", nil);
                self.street2Field.keyboardType = UIKeyboardTypeDefault;
            }
            [cell.contentView addSubview:self.street2Field];
            break;
        }
        case 4:
        {
            if (!self.cityField) {
                self.cityField = [[UITextField alloc] initWithFrame:CGRectMake(16.0, 0.0, CGRectGetWidth(self.view.frame), 44.0)];
                self.cityField.placeholder = ALLocalizedString(@"City", nil);
                self.cityField.keyboardType = UIKeyboardTypeDefault;
            }
            [cell.contentView addSubview:self.cityField];
            break;
        }
        case 5:
        {
            if (!self.stateField) {
                self.stateField = [[UITextField alloc] initWithFrame:CGRectMake(16.0, 0.0, CGRectGetWidth(self.view.frame), 44.0)];
                self.stateField.placeholder = ALLocalizedString(@"State", nil);
                self.stateField.keyboardType = UIKeyboardTypeDefault;
            }
            [cell.contentView addSubview:self.stateField];
            break;
        }
        case 6:
        {
            if (!self.postalCodeField) {
                self.postalCodeField = [[UITextField alloc] initWithFrame:CGRectMake(16.0, 0.0, CGRectGetWidth(self.view.frame), 44.0)];
                self.postalCodeField.placeholder = ALLocalizedString(@"ZIP Code", nil);
                self.postalCodeField.keyboardType = UIKeyboardTypeNumberPad;
            }
            [cell.contentView addSubview:self.postalCodeField];
            break;
        }
        case 7:
        {
            if (!self.phoneField) {
                self.phoneField = [[UITextField alloc] initWithFrame:CGRectMake(16.0, 0.0, CGRectGetWidth(self.view.frame), 44.0)];
                self.phoneField.placeholder = ALLocalizedString(@"Phone", nil);
                self.phoneField.keyboardType = UIKeyboardTypePhonePad;
            }
            [cell.contentView addSubview:self.phoneField];
            break;
        }
        case 8:
        {
            if (!self.countryLabel || !self.currentCountryLabel) {
                self.countryLabel = [[UILabel alloc] init];
                self.currentCountryLabel = [[UILabel alloc] init];
            }
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            self.countryLabel.text = ALLocalizedString(@"Country", nil);
            [self.countryLabel sizeToFit];
            self.countryLabel.frame = CGRectMake(16.0, 0.0, CGRectGetWidth(self.countryLabel.frame), 44.0);
            self.currentCountryLabel.text = self.infoDict[@"Location"];
            [self.currentCountryLabel sizeToFit];
            self.currentCountryLabel.frame = CGRectMake(CGRectGetWidth(self.view.frame) - CGRectGetWidth(self.currentCountryLabel.frame) - 16.0 - 16.0, 0.0, CGRectGetWidth(self.currentCountryLabel.frame), 44.0);
            
            [cell.contentView addSubview:self.countryLabel];
            [cell.contentView addSubview:self.currentCountryLabel];
            break;
        }
        case 9:
        {
            if (!self.primaryLabel || !self.primarySwitch) {
                self.primaryLabel = [[UILabel alloc] init];
                self.primarySwitch = [[UISwitch alloc] init];
                self.primarySwitch.on = YES;
            }
            self.primaryLabel.text = ALLocalizedString(@"Make Primary", nil);
            [self.primaryLabel sizeToFit];
            self.primaryLabel.frame = CGRectMake(16.0, 0.0, CGRectGetWidth(self.primaryLabel.frame), 44.0);
            self.primarySwitch.frame = CGRectMake(CGRectGetWidth(cell.contentView.frame) - CGRectGetWidth(self.primarySwitch.frame) - 16.0, (CGRectGetHeight(cell.contentView.frame) - CGRectGetHeight(self.primarySwitch.frame))/2.0, CGRectGetWidth(self.primarySwitch.frame), CGRectGetHeight(self.primarySwitch.frame));
            [cell.contentView addSubview:self.primaryLabel];
            [cell.contentView addSubview:self.primarySwitch];
            break;
        }
        default:
            break;
    }
    
    
    return cell;
}

#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here, for example:
    // Create the next view controller.
    // <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    // [self.navigationController pushViewController:detailViewController animated:YES];
    if (indexPath.row == 8) {
        ALProfileModifyViewController *next = [[ALProfileModifyViewController alloc] init];
        next.delegate = self;
        next.infoDict = self.infoDict;
        next.modifykey = ALProfileModifyLocation;
        [self.navigationController pushViewController:next animated:YES];
    }
    else {
        [self.view endEditing:YES];
    }
}

#pragma mark - ALProfileModifyViewControllerDelegate

-(void)savaButtonAction:(ALProfileModifyViewController *)profileModifyViewController
{
    NSMutableDictionary *infoDict = [NSMutableDictionary dictionaryWithDictionary:self.infoDict];
    [infoDict setObject:profileModifyViewController.selectLocationLabel.text forKey:@"Location"];
    self.infoDict = [infoDict copy];
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:8 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
}

@end

