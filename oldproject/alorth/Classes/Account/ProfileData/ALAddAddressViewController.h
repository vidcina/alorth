//
//  ALAddAddressViewController.h
//  alorth
//
//  Created by ZZB on 2016/5/23.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ALAddAddressViewController : UITableViewController

@property (nonatomic, strong) NSDictionary *infoDict;

@end
