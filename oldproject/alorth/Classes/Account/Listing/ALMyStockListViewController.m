//
//  ALMyStockListViewController.m
//  alorth
//
//  Created by John Hsu on 2015/6/30.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALMyStockListViewController.h"

@interface ALMyStockListViewController ()
{
    float exampleProductDashboardHeight;
}

@end

@implementation ALMyStockListViewController

#pragma mark - VC Life
-(instancetype)init
{
    self = [super init];
    if (self) {
        goodsListArray = [NSMutableArray array];
        
        currentTableViewControl = [[ALTableViewControl alloc] init];
        currentTableViewControl.delegate = self;
        currentTableViewControl.apiDataArray = goodsListArray;
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self alTableViewSetting:mainTableView];
    
    if (goodsListArray.count == 0) {
        [self checkNewData:currentTableViewControl];
    }
}

#pragma mark - UITableView Datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return goodsListArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (exampleProductDashboardHeight == 0) {
        ALProductDashboard *example =
        [[ALProductDashboard alloc] initWithFrame:CGRectZero];
        example.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0);
        [example layoutIfNeeded];
        
        UIView *bottonView = example.stepButtonView;
        exampleProductDashboardHeight =
        bottonView.frame.origin.y +
        bottonView.frame.size.height +
        ALMyStockListViewGap;
    }
    return exampleProductDashboardHeight;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *products = goodsListArray[indexPath.row];
    
    UITableViewCell *cell =
    [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //調整視窗安裝
    ALProductDashboard *productDashboard = [[ALProductDashboard alloc] initWithFrame:CGRectZero];
    productDashboard.selectDelegate = self;
    
    [cell.contentView addSubview:productDashboard];
    [productDashboard mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(cell.contentView);
    }];
    
    //指定ID
    productDashboard.productID = products[@"ProductID"];
    
    //商品圖片
    [productDashboard.productImageView imageFromURLString:products[@"ProductImage"]];
    
    //商品名稱
    productDashboard.productNameLabel.text = products[@"ProductName"];
    
    //種類選擇條
    productDashboard.usedSelectTypeScrollView.infoObjectArray = [@[@"New",@"Used"] mutableCopy];
    
    //消除空白字元
    NSString *allSizeString = products[@"AllSize"];
    NSArray *allSize = [allSizeString componentsSeparatedByString:@","];
    NSMutableArray *allSizeCopy = [NSMutableArray array];
    for (int k = 0; k < allSize.count; k++) {
        NSString *sizeString = allSize[k];
        [allSizeCopy addObject:[sizeString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    }
    productDashboard.sizeSelectTypeScrollView.infoObjectArray = allSizeCopy;
    
    [self selectFirst:productDashboard Used:@"N" Size:nil];
    //選第一個新品庫存
    //NSDictionary *firstStock = products[@"StockList"][0];
    //[self selectFirst:productDashboard Used:firstStock[apiKeyIsUsed] Size:firstStock[apiKeySize]];
    
    return cell;
}

-(void)selectFirst:(ALProductDashboard *)productView Used:(NSString *)used Size:(NSString *)size
{
    {
        ALSelectTypeScrollView *usedSelect = productView.usedSelectTypeScrollView;
        if ([used isEqualToString:@"N"]) {
            [usedSelect selectView:usedSelect.typeViewArray.firstObject];
        }
        else {
            [usedSelect selectView:usedSelect.typeViewArray.lastObject];
        }
    }
    {
        ALSelectTypeScrollView *sizeSelect = productView.sizeSelectTypeScrollView;
        NSInteger index = 0;
        
        for (int k = 0; k < sizeSelect.infoObjectArray.count; k++) {
            NSString *key = sizeSelect.infoObjectArray[k];
            if ([key isEqualToString:size]) {
                index = k;
                break;
            }
        }
        [sizeSelect selectIndex:index];
    }
}

#pragma mark - scrollView Delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == mainTableView &&
        isKeyboardAnimating == NO &&
        _priceStockView != nil) {
        [self keyboardReturn];
    }
    
    if (scrollView == mainTableView) {
        [currentTableViewControl scrollViewDidScroll:scrollView];
    }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView == mainTableView) {
        [currentTableViewControl scrollViewDidEndDragging:scrollView willDecelerate:(BOOL)decelerate];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == mainTableView) {
        [currentTableViewControl scrollViewDidEndDecelerating:(UIScrollView *)scrollView];
    }
}

#pragma mark - ALTableViewControlDelegate
-(void)checkNewData:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    willReplaceWithNew = YES;
    tableViewControl.lastDict[@"StartNum"] = @"0";
    
    ALLog(@"重新整理");
    [self addMBProgressHUDWithKey:@"stockListA025CompletionBlock"];
    [self goodsListLoadApiSourceParameters:tableViewControl.lastDict
                               FinishBlock:^(BOOL isSuccess) {
                                   [self removeMBProgressHUDWithKey:@"stockListA025CompletionBlock"];
                               }];
}

-(void)loadMore:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    
    ALLog(@"載入更多:目前第 %@ 頁 %lu 筆資料",tableViewControl.lastDict[@"StartNum"],(unsigned long)goodsListArray.count);
    
    tableViewControl.lastDict[@"StartNum"] =
    [NSString stringWithFormat:@"%d",[tableViewControl.lastDict[@"StartNum"] intValue] + 1];
    
    [self addMBProgressHUDWithKey:@"stockListA025CompletionBlock"];
    [self goodsListLoadApiSourceParameters:tableViewControl.lastDict
                               FinishBlock:^(BOOL isSuccess) {
                                   [self removeMBProgressHUDWithKey:@"stockListA025CompletionBlock"];
                               }];
}

#pragma mark - API
-(void)goodsListLoadApiSourceParameters:(NSDictionary *)parameters
                            FinishBlock:(void (^)(BOOL isSuccess))finishBlockToRun
{
    [ALNetWorkAPI stockListA025StartNum:parameters[@"StartNum"]
                        CompletionBlock:^(NSDictionary *responseObject) {
                            
                            BOOL isWiseData = YES;
                            if (![responseObject[@"resBody"] isKindOfClass:[NSDictionary class]]) {
                                isWiseData = NO;
                                
                                if (![responseObject[@"resBody"][@"ProductList"] isKindOfClass:[NSArray class]]) {
                                    isWiseData = NO;
                                }
                            }
                            
                            if (isWiseData) {
                                if (willReplaceWithNew) {
                                    [goodsListArray removeAllObjects];
                                    willReplaceWithNew = NO;
                                }
                                
                                NSArray *addArray = [responseObject[@"resBody"][@"ProductList"] mutableCopy];
                                if (addArray.count > 0) {
                                    [goodsListArray addObjectsFromArray:addArray];
                                }
                                [mainTableView reloadData];
                            }
                            else {
                                ALLog(@"stockListA025_格式錯誤");
                            }
                            finishBlockToRun(YES);
                        }];
}

#pragma mark - ALProductViewDelegate
-(void)didSelectUsed:(NSString *)used Size:(NSString *)size sender:(ALProductDashboard *)sender
{
    ALLog(@"選中 select : %@, %@, %@",sender.productID,used,size);
    if (_priceStockView.superview == sender) {
        [self keyboardReturn];
    }
    
    NSString *productID = sender.productID;
    NSPredicate *productIDPredicate = [NSPredicate predicateWithFormat:@"%K == %@", @"ProductID", productID];
    NSArray *pruductArray = [goodsListArray filteredArrayUsingPredicate:productIDPredicate];
    
    if (pruductArray.count > 0) {
        NSDictionary *pruduct = pruductArray[0];
        
        NSPredicate *usedPredicate = [NSPredicate predicateWithFormat:@"%K == %@", apiKeyIsUsed, used];
        NSPredicate *sizePredicate = [NSPredicate predicateWithFormat:@"%K == %@", apiKeySize, size];
        NSPredicate *concatPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[sizePredicate, usedPredicate]];
        NSArray *stockArray = [pruduct[@"StockList"] filteredArrayUsingPredicate:concatPredicate];
        
        NSMutableDictionary *stockDict = nil;
        if (stockArray.count == 0) {
            
            //新增物品
            stockDict = [NSMutableDictionary dictionary];
            stockDict[@"Currency"] = [ALAppDelegate sharedAppDelegate].currency;
            stockDict[apiKeyIsUsed] = used;
            stockDict[apiKeySize] = size;
            stockDict[@"Amount"] = @(0);
            stockDict[@"Price"] = @(0.00);
        }
        else {
            stockDict = [stockArray[0] mutableCopy];
        }
        //ALLog(@"資料進入畫面: %@",fitDataDict);
        //ALLog(@"全部資料: %@",pruduct);
        sender.infoDict = stockDict;
    }
    else {
        ALLog(@"didSelectUsed 商品搜尋錯誤");
    }
}

-(void)dataDidChange:(NSDictionary *)infoDict sender:(ALProductDashboard *)sender
{
    ALLog(@"改變 dataDidChange : %@",infoDict);
    
    NSString *productID = sender.productID;
    NSPredicate *productIDPredicate = [NSPredicate predicateWithFormat:@"%K == %@", @"ProductID", productID];
    NSArray *pruductArray = [goodsListArray filteredArrayUsingPredicate:productIDPredicate];
    
    if (pruductArray.count > 0) {
        NSMutableDictionary *newPruduct = [pruductArray[0] mutableCopy];
        
        NSPredicate *usedPredicate = [NSPredicate predicateWithFormat:@"%K == %@", apiKeyIsUsed, infoDict[apiKeyIsUsed]];
        NSPredicate *sizePredicate = [NSPredicate predicateWithFormat:@"%K == %@", apiKeySize, infoDict[apiKeySize]];
        NSPredicate *concatPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[sizePredicate, usedPredicate]];
        
        NSMutableArray *newStockListArray = [newPruduct[@"StockList"] mutableCopy];
        NSArray *stockArray = [newStockListArray filteredArrayUsingPredicate:concatPredicate];
        
        if (stockArray.count == 0) {
            NSMutableDictionary *newStockDict = [NSMutableDictionary dictionary];
            
            newStockDict[apiKeyIsUsed] = infoDict[apiKeyIsUsed];
            newStockDict[apiKeySize] = infoDict[apiKeySize];
            newStockDict[@"Amount"] = @(0);
            newStockDict[@"Price"] = @(0.00);
            
            [newStockListArray addObject:newStockDict];
        }
        else {
            NSDictionary *oldStockDict = stockArray[0];
            NSMutableDictionary *newStockDict = [oldStockDict mutableCopy];

            newStockDict[@"Price"]  = infoDict[@"Price"];
            newStockDict[@"Amount"] = infoDict[@"Amount"];
            
            NSInteger replaceIndexOfStock = [newStockListArray indexOfObject:oldStockDict];
            [newStockListArray replaceObjectAtIndex:replaceIndexOfStock withObject:newStockDict];
        }
        
        newPruduct[@"StockList"] = newStockListArray;
        NSInteger replaceIndexOfPruduct = [goodsListArray indexOfObject:pruductArray[0]];
        [goodsListArray replaceObjectAtIndex:replaceIndexOfPruduct withObject:newPruduct];
    }
    else {
        ALLog(@"didSelectUsed 商品搜尋錯誤");
    }
}

-(void)errorMag:(NSString *)errorMag sender:(ALProductDashboard *)sender
{
    YKSimpleAlert(errorMag);
}

-(void)productDashboardRemoveAction:(ALProductDashboard *)sender
{
    ALLog(@"RemoveAction");
    [self keyboardReturn];
    
    [YKBlockAlert alertWithTitle:ALLocalizedString(@"Warning", nil)
                         message:ALLocalizedString(@"All stocks will be deleted", nil)
                blocksAndButtons:
     ^{
         [self addMBProgressHUDWithKey:@"deleteAllStockA027ProductID"];
         [ALNetWorkAPI deleteAllStockA027ProductID:sender.productID
                                   completionBlock:^(NSDictionary *responseObject) {
                                       [self removeMBProgressHUDWithKey:@"deleteAllStockA027ProductID"];
                                       if([responseObject.safe[@"resHeader"][@"TXID"] isEqualToString:@"A027"] &&
                                          [responseObject.safe[@"resBody"][@"ResultCode"] intValue] == 0) {
                                           ALLog(@"刪除所有存貨成功 %@",sender.productID);
                                           [self removeAllStockIn:sender.productID];
                                           [self selectFirst:sender Used:@"N" Size:nil];
                                       }
                                       else {
                                           ALLog(@"刪除所有存貨失敗 %@",sender.productID);
                                           [self checkNewData:currentTableViewControl];
                                       }
                                   }];
     },
     ALLocalizedString(@"Yes",nil),//左邊
     ^{
         
     },
     ALLocalizedString(@"No",nil),//右邊
     nil];
}

-(void)removeAllStockIn:(NSString *)productID
{
    NSPredicate *productIDPredicate = [NSPredicate predicateWithFormat:@"%K == %@", @"ProductID", productID];
    NSArray *pruductArray = [goodsListArray filteredArrayUsingPredicate:productIDPredicate];
    
    if (pruductArray.count > 0) {
        NSMutableDictionary *newPruduct = [pruductArray[0] mutableCopy];
        newPruduct[@"StockList"] = [NSArray array];
        
        NSInteger replaceIndexOfPruduct = [goodsListArray indexOfObject:pruductArray[0]];
        [goodsListArray replaceObjectAtIndex:replaceIndexOfPruduct withObject:newPruduct];
    }
    else {
        ALLog(@"removeAllStock 商品搜尋錯誤");
    }
}

-(void)productDashboardSaveAction:(ALProductDashboard *)sender
{
    ALLog(@"SaveAction");
    [self keyboardReturn];
    
    NSString *productID = sender.productID;
    NSPredicate *productIDPredicate = [NSPredicate predicateWithFormat:@"%K == %@", @"ProductID", productID];
    NSArray *pruductArray = [goodsListArray filteredArrayUsingPredicate:productIDPredicate];
    
    if (pruductArray.count > 0) {
        NSDictionary *pruduct = pruductArray[0];
        NSArray *stockList = pruduct[@"StockList"];
        
        [self addMBProgressHUDWithKey:@"ALMyStockListViewController saveAction"];
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            for (NSDictionary *stockDict in stockList) {
                ALLog(@"輸入%@,%@,%@,%@,%@",productID,stockDict[apiKeyIsUsed],stockDict[apiKeySize],stockDict[@"Amount"],stockDict[@"Price"]);
                [ALNetWorkAPI editStockA026ProductID:productID
                                             stockID:stockDict[@"StockID"]
                                              isUsed:stockDict[apiKeyIsUsed]
                                                size:stockDict[apiKeySize]
                                               price:stockDict[@"Price"]
                                              amount:stockDict[@"Amount"]
                                     completionBlock:^(NSDictionary *responseObject) {
                                         
                                         if([responseObject.safe[@"resHeader"][@"TXID"] isEqualToString:@"A026"] &&
                                            [responseObject.safe[@"resBody"][@"ResultCode"] intValue] == 0) {
                                             ALLog(@"輸出成功");
                                         }
                                         else {
                                             ALLog(@"輸出失敗%@",stockDict);
                                         }
                                         dispatch_semaphore_signal(sema);
                                         
                                     }];
                dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self removeMBProgressHUDWithKey:@"ALMyStockListViewController saveAction"];
                [YKBlockAlert alertWithTitle:ALLocalizedString(@"Success",nil)
                                     message:ALLocalizedString(@"",nil)
                            blocksAndButtons:^{
                                
                            },ALLocalizedString(@"OK",nil), nil];
            });
        });
    }
    else {
        ALLog(@"SaveAction 商品搜尋錯誤");
    }
}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    _priceStockView = textField.superview;
    return [super textFieldShouldBeginEditing:textField];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    _priceStockView = nil;
    return [super textFieldShouldReturn:textField];;
}

-(void)keyboardReturn
{
    _priceStockView = nil;
    [super keyboardReturn];
}

#pragma mark - Keyboard Auto System
-(void)keyboardDidShow:(NSNotification *)notification
{
    [super keyboardDidShow:notification];
    
    float keyboardHeight = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    float keyBoardYPosition = self.view.frame.size.height - keyboardHeight;
    
    CGRect windowRect = [_priceStockView convertRect:_priceStockView.bounds toView:self.view.window];
    
    float upShift = windowRect.origin.y + windowRect.size.height - keyBoardYPosition;
    
    if (upShift > 0) {
        //如果輸入框有一半在螢幕外面 就怪怪的
        isKeyboardAnimating = YES;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.25 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            isKeyboardAnimating = NO;
        });
        [UIView animateWithDuration:0.25
                         animations:^{
                             [mainTableView setContentOffset:CGPointMake(0, mainTableView.contentOffset.y + upShift)];
                         } completion:^(BOOL finished) {
                             
                         }];
    }
}

@end
