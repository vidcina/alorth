//
//  ALMyStockListViewController.h
//  alorth
//
//  Created by John Hsu on 2015/6/30.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicViewController.h"

#import "ALProductDashboard.h"

static const float ALMyStockListViewGap = 5.0;

@interface ALMyStockListViewController : ALBasicSubViewController
<UITableViewDataSource, UITableViewDelegate, ALTableViewControlDelegate,
ALProductDashboardSelectDelegate>
{
    NSMutableArray *goodsListArray;
    BOOL willReplaceWithNew;                    //是否清空之前資料
    __weak IBOutlet UITableView *mainTableView;
    ALTableViewControl *currentTableViewControl;
    
    BOOL isKeyboardAnimating;
}

@property (nonatomic, strong) UIView *priceStockView;

@end
