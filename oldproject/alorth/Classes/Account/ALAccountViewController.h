//
//  ALAccountViewController.h
//  alorth
//
//  Created by w91379137 on 2015/6/2.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALAccountDefinition.h"
#import "ALBasicMainViewController.h"

@interface ALAccountViewController : ALBasicMainViewController
<UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *settingCellInfoArray;
    
    IBOutlet UIImageView *userAvatarImageView;
    IBOutlet UILabel *userNameLabel;
    IBOutlet UIView *followView;
}

@end
