//
//  ALAboutListViewController.m
//  alorth
//
//  Created by w91379137 on 2015/10/13.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALAboutListViewController.h"

typedef NS_ENUM(NSUInteger, ALAboutListSection) {
    Section1,
    Section2,
    Section3,
    SectionMax
};

@interface ALAboutListViewController ()

@end

@implementation ALAboutListViewController

-(void)viewWillAppearFirstTime:(BOOL)animated
{
    [super viewWillAppearFirstTime:animated];
    [headerView.backButton addTarget:self
                              action:@selector(back:)
                    forControlEvents:UIControlEventTouchUpInside];
    
    [self alTableViewSetting:mainTableView];
}


#pragma mark - UITableViewDataSource Section
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return SectionMax;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

#pragma mark - UITableViewDataSource Row
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) return 2;
    if (section == 1) return 2;
    if (section == 2) return 5;
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell =
    [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@""];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

#pragma mark - UITableViewDelegate



@end
