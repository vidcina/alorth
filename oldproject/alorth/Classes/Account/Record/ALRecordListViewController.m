//
//  ALRecordListViewController.m
//  alorth
//
//  Created by w91379137 on 2015/10/13.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALRecordListViewController.h"
#import "ALStreamSmallCell.h"
#import "ALStreamHelper.h"

static const int cellCount = INT32_MAX;

@interface ALRecordListViewController ()
{
    
}

@end

@implementation ALRecordListViewController

#pragma mark - VC Life
-(void)viewDidLoad
{
    [super viewDidLoad];
    videoListArray = [NSMutableArray array];
    [self alTableViewSetting:self.mainTableView];
//    for (int k = 0; k < 10; k++) {
//        [videoListArray addObject:[NSString stringWithFormat:@"%d",k]];
//    }
//    [self.mainTableView setEditing:YES animated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (videoListArray.count == 0) {
        
        [self addMBProgressHUDWithKey:@"editStreamListA061"];
        [ALNetWorkAPI editStreamListA061CompletionBlock:^(NSDictionary *responseObject) {
            [self removeMBProgressHUDWithKey:@"editStreamListA061"];
            
            if ([ALNetWorkAPI checkSerialNumber:@"A061"
                                 ResponseObject:responseObject]) {
                
                videoListArray = [responseObject[@"resBody"][@"VideoList"] mutableCopy];
                
                [self.mainTableView reloadData];
                
            }
            else {
                YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
            }
        }];
    }
}

-(void)viewWillAppearFirstTime:(BOOL)animated
{
    [super viewWillAppearFirstTime:animated];
    [headerView.backButton addTarget:self
                              action:@selector(back:)
                    forControlEvents:UIControlEventTouchUpInside];
    
    [self.mainTableView setTableFooterView:[[UIView alloc] init]];
    self.mainTableView.editing = YES;
    self.mainTableView.allowsSelectionDuringEditing = YES;
}

#pragma mark - UITableViewDataSource Section
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        return 12.5;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

#pragma mark - UITableViewDataSource Row
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return MIN(videoListArray.count, cellCount);
    }
    else {
        if (videoListArray.count > cellCount) {
            return videoListArray.count - cellCount;
        }
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *infoDict = nil;
    if (indexPath.section == 0) {
        infoDict = videoListArray[indexPath.row];
    }
    else {
        if (videoListArray.count > cellCount) {
            NSInteger k = indexPath.row + cellCount;
            infoDict = videoListArray[k];
        }
    }
    
    ALStreamSmallCell *cell = [ALStreamSmallCell cellFromXib];
    if ([infoDict isKindOfClass:[NSDictionary class]]) {
        [cell performWithInfoDict:infoDict];
    }
    return cell;
}

#pragma mark - UITableViewDataSource Move Delete
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath
      toIndexPath:(NSIndexPath *)destinationIndexPath
{
    
    NSInteger from =
    sourceIndexPath.section * cellCount +
    sourceIndexPath.row;
    
    NSInteger to =
    destinationIndexPath.section * cellCount +
    destinationIndexPath.row -
    (sourceIndexPath.section < destinationIndexPath.section); //修正從前到後的誤差1個
    
    NSMutableArray *copyToRun = [videoListArray mutableCopy];
    
    NSObject *object = copyToRun[from];
    [copyToRun removeObjectAtIndex:from];
    
    if (to > copyToRun.count) {
        //影片數目並無超過 第一區容量 cellCount 故大於現存影片數量 videoListArray 將放置在最後一個
        [copyToRun addObject:object];
    }
    else {
        [copyToRun insertObject:object atIndex:to];
    }
    
    NSMutableArray *sendArray = [NSMutableArray array];
    for (NSInteger k = 0; k < copyToRun.count; k++) {
        NSDictionary *infoDict = copyToRun[k];
        NSMutableDictionary *aStream = [NSMutableDictionary dictionary];
        [aStream setObjectxNil:infoDict[@"StreamID"] forKey:@"StreamID"];
        [aStream setObjectxNil:[NSString stringWithFormat:@"%ld",copyToRun.count - k] forKey:@"Sequence"];
        
        [sendArray addObject:aStream];
    }
    
    [self addMBProgressHUDWithKey:@"rearrangeStreamListA067"];
    [ALNetWorkAPI rearrangeStreamListA067StreamList:sendArray
                                    CompletionBlock:^(NSDictionary *responseObject) {
                                        [self removeMBProgressHUDWithKey:@"rearrangeStreamListA067"];
                                        
                                        if ([ALNetWorkAPI checkSerialNumber:@"A067"
                                                             ResponseObject:responseObject]) {
                                            videoListArray = copyToRun;
                                        }
                                        else {
                                            YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
                                        }
                                        [tableView reloadData];
                                    }];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSInteger from =
        indexPath.section * cellCount +
        indexPath.row;
        
        NSDictionary *infoDict = videoListArray[from];
        
        [self addMBProgressHUDWithKey:@"deleteStreamA062"];
        [ALNetWorkAPI deleteStreamA062StreamID:infoDict[@"StreamID"]
                               CompletionBlock:^(NSDictionary *responseObject) {
                                   [self removeMBProgressHUDWithKey:@"deleteStreamA062"];
                                   
                                   if ([ALNetWorkAPI checkSerialNumber:@"A062"
                                                        ResponseObject:responseObject]) {
                                       [videoListArray removeObjectAtIndex:from];
                                   }
                                   else {
                                       YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
                                   }
                                   [tableView reloadData];
                               }];
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if([cell isKindOfClass:[ALCommonTableViewCell class]]) {
        ALCommonTableViewCell *cellx = (ALCommonTableViewCell *)cell;
        
        //補正伺服器其他資訊
        [self addMBProgressHUDWithKey:@"streamInfomationA066"];
        [ALNetWorkAPI streamInfomationA066StreamID:cellx.infoDict[@"StreamID"]
                                   CompletionBlock:^(NSDictionary *responseObject) {
                                       [self removeMBProgressHUDWithKey:@"streamInfomationA066"];
                                       
                                       if ([ALNetWorkAPI checkSerialNumber:@"A066"
                                                            ResponseObject:responseObject]) {
                                           
                                           NSMutableDictionary *newDict = [responseObject[@"resBody"] mutableCopy];
                                           newDict[@"StreamID"] = cellx.infoDict[@"StreamID"];
                                           
                                           [self playStreamVideo:newDict];
                                       }
                                       else {
                                           YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
                                       }
                                   }];
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView
           editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (BOOL)tableView:(UITableView *)tableview shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

@end
