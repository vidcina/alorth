//
//  ALRecordListViewController.h
//  alorth
//
//  Created by w91379137 on 2015/10/13.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicSubViewController.h"

@interface ALRecordListViewController : ALBasicSubViewController
<UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *videoListArray;
}

@property(nonatomic,strong) IBOutlet UITableView *mainTableView;

@end
