//
//  ALProfileDataViewController.m
//  alorth
//
//  Created by John Hsu on 2015/6/30.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALProfileDataViewController.h"
#import "ALAsset+Store.h"
#import "UIViewController+UIImagePickerControllerDelegate.h"
#import "UIViewController+ImageOnline.h"
#import "ALProfileModifyViewController.h"
#import "UIViewController+ALProfile.h"
#import "ALProfileAdrressViewController.h"

@interface ALProfileDataViewController ()
<ALProfileModifyViewControllerDelegate>

@end

@implementation ALProfileDataViewController

#pragma mark - VC Life
- (instancetype)init
{
    self = [super init];
    if (self) {

        dataKeyArray = @[
                         @{
                             @"fieldName" : @"Avatar",
                             @"apiKey" : @"HeadImage"
                             },
                         @{
                             @"fieldName" : @"Account",
                             @"apiKey" : @"Account"
                             },
                         @{
                             @"fieldName" : @"Name",
                             @"apiKey" : ALProfileModifyName
                             },
                         @{
                             @"fieldName" : @"Location",
                             @"apiKey" : ALProfileModifyLocation
                             },
                         @{
                             @"fieldName" : @"Address",
                             @"apiKey" : ALProfileModifyAddress
                             },
                         @{
                             @"fieldName" : @"My Public Info",
                             @"apiKey" : ALProfileModifyPublicInfo
                             },
                         @{
                             @"fieldName" : @"Phone",
                             @"apiKey" : ALProfileModifyPhone
                             },
                         @{
                             @"fieldName" : @"Shipping fee",
                             @"apiKey" : ALProfileModifyShippingFee
                             },
                         @{
                             @"fieldName" : @"Gender",
                             @"apiKey" : ALProfileModifyGender
                             },
                         @{
                             @"fieldName" : @"Birthday",
                             @"apiKey" : ALProfileModifyBirthday
                             }
                         ];
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tableView.contentInset = UIEdgeInsetsMake(15, 0, 0, 0);
    [self alTableViewSetting:self.tableView];
    
    userAvatarImageView.layer.masksToBounds = YES;
    userAvatarImageView.layer.cornerRadius = (userAvatarImageView.frame.size.height / 2);
    [userAvatarImageView.layer setBorderColor:[UIColor whiteColor].CGColor];
    [userAvatarImageView.layer setBorderWidth:2];
    
    avatarTitleLabel.text = ALLocalizedString(dataKeyArray[0][@"fieldName"], nil);
    
    if ((self.shouldReload == YES) || (apiDataDict == nil)) {
        self.shouldReload = NO;
        
        [self addMBProgressHUDWithKey:@"personalProfile020"];
        [ALNetWorkAPI personalProfile020CompletionBlock:^(NSDictionary *responseObject) {
            [self removeMBProgressHUDWithKey:@"personalProfile020"];
            
            if ([ALNetWorkAPI checkSerialNumber:@"A020"
                                 ResponseObject:responseObject]) {
                
                NSDictionary *dict = responseObject[@"resBody"];
                apiDataDict = [dict mutableCopy];
                [apiDataDict removeObjectForKey:@"ResultCode"];
                
                //[self.tableView reloadData];
                oldCountryCode = [responseObject[@"resBody"][@"Location"] description];
                NSString *path = dict[dataKeyArray[0][@"apiKey"]];
                [userAvatarImageView imageFromURLString:path];
            }
            else {
                [self.navigationController popViewControllerAnimated:YES];
                YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
            }
        }];
    }
}



#pragma mark - UITableView Data source
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataKeyArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return avatarCell.frame.size.height;
    }
    return 50;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *key = dataKeyArray[indexPath.row][@"apiKey"];
    if ([key isEqualToString:@"HeadImage"]) {
        return avatarCell;
    }
    
    UIFont *font = [UIFont systemFontOfSize:14];
    
    UITableViewCell *cell =
    [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"CELL"];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.localText = dataKeyArray[indexPath.row][@"fieldName"];
    
    cell.textLabel.font = font;
    cell.textLabel.textColor = COMMON_GRAY4_COLOR;
    
    cell.detailTextLabel.font = font;
    cell.detailTextLabel.textColor = COMMON_GRAY4_COLOR;
    
    if ([key isEqualToString:@"Account"]) {
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.detailTextLabel.text = [ALAppDelegate sharedAppDelegate].userID;
    }
    
    return cell;
}

#pragma mark - UITableView Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *key = dataKeyArray[indexPath.row][@"apiKey"];
    if ([key isEqualToString:@"HeadImage"]) {
        //更改頭像
        [self showChoosePickerControllerType];
    }
    else if([key isEqualToString:@"Account"])
    {
        
    }
    else if ([key isEqualToString:@"ALProfileModifyAddress"]) {
        ALProfileAdrressViewController *viewController = [[ALProfileAdrressViewController alloc] initWithNibName:@"ALProfileAdrressViewController" bundle:nil];
        viewController.infoDict = apiDataDict;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else {
        if ([ALProfileModifyViewController isALProfileCanModifyType:key]) {
            UIViewController *next = [self modifyViewControllerWithKey:key];
            [self.navigationController pushViewController:next animated:YES];
        }
        else {
            ALLog(@"請到修改畫面設定 新類型修改模式");
        }
    }
}

-(UIViewController *)modifyViewControllerWithKey:(NSString *)key
{
    ALProfileModifyViewController *next = [[ALProfileModifyViewController alloc] init];
    next.delegate = self;
    next.infoDict = apiDataDict;
    next.modifykey = key;
    return next;
}

#pragma mark - ALProfileModifyViewControllerDelegate
-(void)savaButtonAction:(ALProfileModifyViewController *)profileModifyViewController
{
    if ([profileModifyViewController.modifykey isEqualToString:ALProfileModifyName]) {
        if ([profileModifyViewController.textField_1.text isKindOfClass:[NSString class]]) {
            [apiDataDict setObject:profileModifyViewController.textField_1.text forKey:@"FirstName"];
        }
        else {
            [apiDataDict setObject:@"" forKey:@"FirstName"];
        }
        
        if ([profileModifyViewController.textField_2.text isKindOfClass:[NSString class]]) {
            [apiDataDict setObject:profileModifyViewController.textField_2.text forKey:@"LastName"];
        }
        else {
            [apiDataDict setObject:@"" forKey:@"LastName"];
        }
    }
    else if ([profileModifyViewController.modifykey isEqualToString:ALProfileModifyLocation]) {
        YKSimpleAlert(ALLocalizedString(@"$Warning: You've changed your country$, $Please remember to update your default shipping fees$", nil));
        if ([profileModifyViewController.selectLocationLabel.text isKindOfClass:[NSString class]]) {
            [apiDataDict setObject:profileModifyViewController.selectLocationLabel.text forKey:@"Location"];
        }
        else {
            [apiDataDict setObject:@"" forKey:@"Location"];
        }
    }
    else if ([profileModifyViewController.modifykey isEqualToString:ALProfileModifyAddress]) {
        if ([profileModifyViewController.textView_1.text isKindOfClass:[NSString class]]) {
            [apiDataDict setObject:profileModifyViewController.textView_1.text forKey:@"Address"];
        }
        else {
            [apiDataDict setObject:@"" forKey:@"Address"];
        }
    }
    else if ([profileModifyViewController.modifykey isEqualToString:ALProfileModifyPublicInfo]) {
        if ([profileModifyViewController.textView_1.text isKindOfClass:[NSString class]]) {
            [apiDataDict setObject:profileModifyViewController.textView_1.text forKey:@"PublicInfo"];
        }
        else {
            [apiDataDict setObject:@"" forKey:@"PublicInfo"];
        }
    }
    else if ([profileModifyViewController.modifykey isEqualToString:ALProfileModifyPhone]) {
        if ([profileModifyViewController.textField_1.text isKindOfClass:[NSString class]]) {
            [apiDataDict setObject:profileModifyViewController.textField_1.text forKey:@"Phone"];
        }
        else {
            [apiDataDict setObject:@"" forKey:@"Phone"];
        }
    }
    else if ([profileModifyViewController.modifykey isEqualToString:ALProfileModifyShippingFee]) {
        if ([profileModifyViewController.textField_1.text isKindOfClass:[NSString class]]) {
            [apiDataDict setObject:profileModifyViewController.textField_1.text forKey:@"TransPrice"];
        }
        else {
            [apiDataDict setObject:@"" forKey:@"TransPrice"];
        }
    }
    else if ([profileModifyViewController.modifykey isEqualToString:ALProfileModifyGender]) {
        if ([profileModifyViewController.genderString isKindOfClass:[NSString class]]) {
            [apiDataDict setObject:profileModifyViewController.genderString forKey:@"Gender"];
        }
        else {
            [apiDataDict setObject:@"" forKey:@"Gender"];
        }
    }
    else if ([profileModifyViewController.modifykey isEqualToString:ALProfileModifyBirthday]) {
        if ([profileModifyViewController.textField_1.text isKindOfClass:[NSString class]]) {
            [apiDataDict setObject:profileModifyViewController.textField_1.text forKey:@"Birthday"];
        }
        else {
            [apiDataDict setObject:@"" forKey:@"Birthday"];
        }
    }
    else {
        ALLog(@"\n\n\n並沒有儲存!!!!!\n\n\n");
    }
    
    [self saveUserProfile:apiDataDict];
}

#pragma mark - UIImagePickerController Delegate
- (void)didPickerControllerReturnImage:(UIImage *)avatarImage
{
    userAvatarImageView.image = avatarImage;
    [self uploadImage:userAvatarImageView.image];
}

#pragma mark - ImageOnline
- (void)didSaveImageAtURL:(NSString *)string
{
    if ([string isKindOfClass:[NSString class]]) {
        [apiDataDict setObject:string forKey:dataKeyArray[0][@"apiKey"]];
    }
    [self saveUserProfile:apiDataDict];
}

@end
