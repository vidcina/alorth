//
//  ALSettingViewController.h
//  alorth
//
//  Created by John Hsu on 2015/6/30.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicViewController.h"
#import <MessageUI/MessageUI.h>

@interface ALSettingViewController : ALBasicSubViewController
<UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate>
{
    NSMutableArray *cellTitleAndBlockArray;
}

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIButton *logOutButton;

@end
