//
//  ALBoughtOrderContentViewController.m
//  alorth
//
//  Created by John Hsu on 2015/6/30.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBoughtOrderContentViewController.h"

@interface ALBoughtOrderContentViewController ()
<ALOnlinePaymentViewControllerDelegate>

@end

@implementation ALBoughtOrderContentViewController

#pragma mark - VC Life
-(instancetype)initWithInfoDict:(NSDictionary *)infoDict
{
    self = [super init];
    if (self) {
        _infoDict = infoDict;
        
        NSArray *nib =
        [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([ALBoughtOrderListTableViewCell class]) owner:self options:nil];
        topCell = (ALBoughtOrderListTableViewCell *)[nib objectAtIndex:0];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (self.onlinePaymentVC == nil) {
        self.onlinePaymentVC = [[ALOnlinePaymentViewController alloc] init];
        self.onlinePaymentVC.delegate = self;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.mainScrollViewContentView.superview == nil) {
        [self.mainScrollView masSetContentView:self.mainScrollViewContentView isVerticalFix:NO];
    }
    
    //上方cell
    {
        if (topCell.superview == nil) {
            topCell.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0);
            [topCell layoutIfNeeded];
            
            [self.cellContainer addSubview:topCell];
            [topCell mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(self.cellContainer);
            }];
            
            [topCell mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(topCell.middleHorizontalLine.mas_bottom);
            }];
        }
        [ALBoughtOrderListTableViewCell performCell:topCell withDictionary:self.infoDict];
    }
    
    //商品型號
    {
        if ([[self.infoDict[apiKeyIsUsed] description] isEqualToString:@"N"]) {
            self.productConditionLabel.localText = @"New";
        }
        else {
            self.productConditionLabel.localText = @"Used";
        }
        
        self.productSizeLabel.text = self.infoDict[@"ProductSize"];
        self.productNumberLabel.text = self.infoDict[@"Amount"];
        
        self.productShippingFeeLabel.text =
        [NSString stringWithFormat:@"%.2f %@",
         [self.infoDict[@"TransPrice"] floatValue],
         self.infoDict[@"Currency"]];
        
        self.productTotalPriceLabel.text =
        [NSString stringWithFormat:@"%.2f %@",
         [self.infoDict[@"Price"] floatValue] +
         [self.infoDict[@"TransPrice"] floatValue],
         self.infoDict[@"Currency"]];
    }
    
    //買家資訊
    if (self.isBuyerInfomationExist != YES) {
        self.isBuyerInfomationExist = YES;
        
        [self addMBProgressHUDWithKey:@"addStockA020"];
        [ALNetWorkAPI personalProfile020CompletionBlock:^(NSDictionary *responseObject) {
            [self removeMBProgressHUDWithKey:@"addStockA020"];
            
            if ([ALNetWorkAPI checkSerialNumber:@"A020"
                                 ResponseObject:responseObject]) {
                
                NSDictionary *dict = responseObject[@"resBody"];
                self.buyerNameLabel.text =
                [ALFormatter preferredDisplayNameFirst:dict[@"FirstName"]
                                                  Last:dict[@"LastName"]
                                                UserID:[ALAppDelegate sharedAppDelegate].userID];
                
                self.buyerAddressLabel.text = dict[@"Address"];
                self.buyerTelephoneLabel.text = dict[@"Phone"];
            }
            else {
                YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
            }
        }];
    }
    
    //賣家資訊
    {
        self.sellerNameLabel.text = self.infoDict[@"SellerName"];
        self.sellerAddressLabel.text = self.infoDict[@"SellerAddress"];
        self.sellerTelephoneLabel.text = self.infoDict[@"SellerPhone"];
        self.sellerPublicInfoLabel.text = self.infoDict[@"SellerPublicInfo"];
        self.sellerPublicInfoLabel.numberOfLines = 0;
        
        NSDictionary *titleattributes = @{NSFontAttributeName:self.sellerPublicInfoLabel.font};
        
        CGSize titleSize =
        [self.sellerPublicInfoLabel.text boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, MAXFLOAT)
                                                      options:NSStringDrawingUsesLineFragmentOrigin
                                                   attributes:titleattributes
                                                      context:nil].size;
        
        [self.sellerPublicInfoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@(titleSize.height + 30));
            
        }];
    }
    
    
    //商品編號
    {
        self.sellerAccountLabel.text = self.infoDict[@"SellerID"];
        self.sellerOrderNumberLabel.text = self.infoDict[@"OrderID"];
        
        self.sellerOrderTimeLabel.text =
        [ALFormatter prettyLocelTimeFormat:dayTimeFormat
                                   ForDate:[NSDate dateWithTimeIntervalSince1970:[self.infoDict[@"OrderTime"] intValue]]];
    }
    
    //按鈕設置
    if ([self.infoDict[@"OrderStatus"] intValue] == 1) {
        //正在移交
        self.buttonsView.hidden = NO;
    }
    else {
        self.buttonsView.hidden = YES;
        
        [self.buttonsView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@0);
        }];
    }
    
    #warning 如果之前按信用卡 某些按鈕怎處理
}

#pragma mark - IBAction
-(IBAction)orderCancelAction:(UIButton *)sender
{
    [YKBlockAlert alertWithTitle:ALLocalizedString(@"Confirm", nil)
                         message:ALLocalizedString(@"The deal will be canceled", nil)
                blocksAndButtons:
     ^{
         [self addMBProgressHUDWithKey:@"cancelOrderPriceA011"];
         [ALNetWorkAPI cancelOrderPriceA011OrderId:self.infoDict[@"OrderID"]
                                   CompletionBlock:^(NSDictionary *responseObject) {
                                       [self removeMBProgressHUDWithKey:@"cancelOrderPriceA011"];
                                       
                                       if([ALNetWorkAPI checkSerialNumber:@"A011"
                                                           ResponseObject:responseObject]) {
                                           
                                           [self.tableDelegate checkNewData];
                                           [self.navigationController popViewControllerAnimated:NO];
                                           
                                           [self logAction:ALBuyStepBuyCancel];
                                           
                                           YKSimpleAlert(ALLocalizedString(@"Canceled",nil));
                                       }
                                       else {
                                           //@"網路連線錯誤，請稍後再試一次"
                                           YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
                                       }
                                   }];
     },
     ALLocalizedString(@"Yes",nil),//左邊
     ^{
         
     },
     ALLocalizedString(@"No",nil),//右邊
     nil];
}

-(IBAction)orderDealBySelfAction:(UIButton *)sender
{
    [self checkout:ALDealMySelfPayType];
}

-(IBAction)showOnlinePayAction:(id)sender
{
    if (self.onlinePaymentContainerView.superview == nil) {
        [self.buttonsView addSubview:self.onlinePaymentContainerView];
        [self.onlinePaymentContainerView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.buttonsView);
        }];
    }
    if (self.onlinePaymentVC.view.superview == nil) {
        [self.onlinePaymentContainerView addSubview:self.onlinePaymentVC.view];
        [self.onlinePaymentVC.view mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.onlinePaymentContainerView.mas_top);
            make.bottom.equalTo(self.onlinePaymentContainerView.mas_bottom);
            make.trailing.equalTo(self.onlinePaymentContainerView.mas_trailing);
            make.leading.equalTo(self.otherButton.mas_trailing);
            make.height.equalTo(@(self.buttonsView.frame.size.height));
        }];
    }
    self.onlinePaymentContainerView.hidden = NO;
}

-(IBAction)hiddenOnlinePayAction:(id)sender
{
    self.onlinePaymentContainerView.hidden = YES;
}

#pragma mark - ALOnlinePaymentViewControllerDelegate
- (void)totalPrice:(NSDecimalNumber **)decimalNumber currency:(NSString **)currency
{
    *decimalNumber = [NSDecimalNumber decimalNumberWithString:[self.infoDict.safe[@"Price"] description]];
    *currency = self.infoDict.safe[@"Currency"];
}

- (void)onlinePayAction
{
    [self checkout:ALCreditCardPayType];
}

- (void)touchOnlinePaymentButtonLog:(ALOnlinePaymentViewControllerLogType)logType
{
    ALBuyStep step = ALBuyStepInit;
    
    if (logType == ALOnlinePaymentLogApplePay) step = ALBuyStepBuyOnlinePayment_ApplePay;
    if (logType == ALOnlinePaymentLogStripe) step = ALBuyStepBuyOnlinePayment_Stripe;
    if (logType == ALOnlinePaymentLogAuthorize) step = ALBuyStepBuyOnlinePayment_Authorize;
    if (logType == ALOnlinePaymentLogChangeCard) step = ALBuyStepBuyOnlinePayment_ChangeCard;
    //if (logType == ALOnlinePaymentLogCheckout) step = ALBuyStepBuyOnlinePayment_Purchase; call api 那邊有
    
    if (step != ALBuyStepInit) {
        [[ALBuyProcessInspectorUnit logProductBuy:step
                                          Product:nil
                                          DevDict:self.infoDict] sendInspector];
    }
}

#pragma mark - payAction
- (void)checkout:(NSString *)type
{
    [YKBlockAlert alertWithTitle:ALLocalizedString(@"Confirm", nil)
                         message:ALLocalizedString(@"", nil)
                blocksAndButtons:
     ^{
         [ALNetWorkAPI confirmOrderPriceA012OrderId:self.infoDict[@"OrderID"]
                                            payType:type
                                    transactionData:nil
                                    CompletionBlock:^(NSDictionary *responseObject) {
                                        
                                        if([ALNetWorkAPI checkSerialNumber:@"A012"
                                                            ResponseObject:responseObject]) {
                                            
                                            [self.tableDelegate checkNewData];
                                            [self.navigationController popViewControllerAnimated:NO];
                                            
                                            ALBuyStep step = [type isEqualToString:ALDealMySelfPayType] ? ALBuyStepBuyDealMySelf_Purchase : ALBuyStepBuyOnlinePayment_Purchase;
                                            [self logAction:step];
                                            
                                            YKSimpleAlert(ALLocalizedString(@"Success", nil));
                                        }
                                        else {
                                            //@"網路連線錯誤，請稍後再試一次"
                                            
                                            BOOL isSellerIsConnected = YES;
                                            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                                                if ([responseObject[@"SellerIsConnected"] intValue] == 0) {
                                                    isSellerIsConnected = NO;
                                                }
                                            }
                                            
                                            if (isSellerIsConnected) {
                                                YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A012", responseObject),nil));
                                            }
                                            else {
                                                YKSimpleAlert(ALLocalizedString(@"The seller is unable to receive payments with credit card", nil));
                                            }
                                            
                                        }
                                    }];
     },
     ALLocalizedString(@"Yes",nil),//左邊
     ^{
         
     },
     ALLocalizedString(@"No",nil),//右邊
     nil];
}

- (void)logAction:(ALBuyStep)step
{
    ALEcommerceProduct *product = [[ALEcommerceProduct alloc] init];
    product.productID = self.infoDict.safe[@"ProductID"];
    product.productTitle = self.infoDict.safe[@"ProductTitle"];
    
    product.price = [self.infoDict.safe[@"Price"] floatValue];
    product.currency = self.infoDict.safe[@"Currency"];
    product.quantity = [self.infoDict.safe[@"Amount"] integerValue];
    
    [[ALBuyProcessInspectorUnit logProductBuy:step
                                      Product:product
                                      DevDict:self.infoDict] sendInspector];
}

@end
