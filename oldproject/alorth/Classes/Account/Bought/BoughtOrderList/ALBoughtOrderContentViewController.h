//
//  ALBoughtOrderContentViewController.h
//  alorth
//
//  Created by John Hsu on 2015/6/30.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicViewController.h"
#import "ALBoughtOrderListTableViewCell.h"

#import "ALBoughtOrderViewController.h"

@interface ALBoughtOrderContentViewController : ALBasicSubViewController
{
    ALBoughtOrderListTableViewCell *topCell;
}

-(instancetype)initWithInfoDict:(NSDictionary *)infoDict;

@property (nonatomic, strong) NSDictionary *infoDict;
@property (nonatomic, weak) ALBoughtOrderViewController *tableDelegate;

@property (nonatomic, strong) IBOutlet UIScrollView *mainScrollView;
@property (nonatomic, strong) IBOutlet UIView *mainScrollViewContentView;

@property (nonatomic, strong) IBOutlet UIView *cellContainer;

@property (nonatomic, strong) IBOutlet UILabel *productConditionLabel;
@property (nonatomic, strong) IBOutlet UILabel *productSizeLabel;
@property (nonatomic, strong) IBOutlet UILabel *productNumberLabel;
@property (nonatomic, strong) IBOutlet UILabel *productShippingFeeLabel;
@property (nonatomic, strong) IBOutlet UILabel *productTotalPriceLabel;

@property (nonatomic) BOOL isBuyerInfomationExist;
@property (nonatomic, strong) IBOutlet UILabel *buyerNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *buyerAddressLabel;
@property (nonatomic, strong) IBOutlet UILabel *buyerTelephoneLabel;

@property (nonatomic, strong) IBOutlet UILabel *sellerNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *sellerAddressLabel;
@property (nonatomic, strong) IBOutlet UILabel *sellerTelephoneLabel;
@property (nonatomic, strong) IBOutlet UILabel *sellerPublicInfoLabel;

@property (nonatomic, strong) IBOutlet UILabel *sellerAccountLabel;
@property (nonatomic, strong) IBOutlet UILabel *sellerOrderNumberLabel;
@property (nonatomic, strong) IBOutlet UILabel *sellerOrderTimeLabel;

@property (nonatomic, strong) IBOutlet UIView *buttonsView;
@property (nonatomic, strong) ALOnlinePaymentViewController *onlinePaymentVC;
@property (nonatomic, strong) IBOutlet UIView *onlinePaymentContainerView;
@property (nonatomic, strong) IBOutlet UIButton *otherButton;

@end
