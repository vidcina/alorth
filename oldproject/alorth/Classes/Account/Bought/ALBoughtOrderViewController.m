//
//  ALBoughtOrderViewController.m
//  alorth
//
//  Created by John Hsu on 2015/6/30.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBoughtOrderViewController.h"
#import "ALBoughtOrderContentViewController.h"

#import "ALCommentViewController.h"
#import "ALReplyViewController.h"

@interface ALBoughtOrderViewController ()

@end

@implementation ALBoughtOrderViewController

#pragma mark - VC Life
-(instancetype)init
{
    self = [super init];
    if (self) {
        goodsListArray = [NSMutableArray array];
        
        currentTableViewControl = [[ALTableViewControl alloc] init];
        currentTableViewControl.delegate = self;
        currentTableViewControl.apiDataArray = goodsListArray;
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self alTableViewSetting:self.tableView];
    
    if (goodsListArray.count == 0) {
        [self checkNewData:currentTableViewControl];
    }
}

- (void)testDictionary
{
    NSDictionary *dict = @{@"ProductID" : @"1234",
                           @"ProductTitle" : @"A019_模擬器測試資料\nA019_模擬器測試資料",
                           @"StockID" : @"",
                           @"OrderID" : @"A019_xxx_test_xxx_waxx",
                           @"ProductClip" : @"",
                           @"ProductImage" : @"http://www.case.edu/images/2014/icon-gearwheels.min.png",
                           @"ProductSize" : @"",
                           @"Price" : @"350",
                           @"Currency" : @"",
                           @"Location" : @"",
                           apiKeyIsUsed : @"",
                           @"Amount" : @"",
                           @"TransType" : @"",
                           @"TransPrice" : @"",
                           @"BuyerID" : @"",
                           @"BuyerName" : @"",
                           @"BuyerPhone" : @"",
                           @"BuyerAddress" : @"",
                           @"SellerPublicInfo" : @"",
                           @"PayType" : ALDealMySelfPayType,
                           @"OrderTime" : @"",
                           @"PayDeadline" : [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] + arc4random_uniform(24 * 60 * 60)]};
    [goodsListArray addObject:dict];
}

#pragma mark - UITableView Delegate & DataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (demoCell == nil) {
        demoCell = [ALBoughtOrderListTableViewCell cell];
        demoCell.frame = CGRectMake(0, 0, tableView.contentSize.width, 0);
        [demoCell layoutIfNeeded];
    }
    return [ALBoughtOrderListTableViewCell performCell:demoCell withDictionary:goodsListArray[indexPath.row]] + 15;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [goodsListArray count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reuseIdentifier = @"reuseIdentifier";
    ALBoughtOrderListTableViewCell *cell
    = (ALBoughtOrderListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (!cell) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([ALBoughtOrderListTableViewCell class]) owner:self options:nil];
        cell = (ALBoughtOrderListTableViewCell *)[nib objectAtIndex:0];
        cell.delegate = self;
    }
    [ALBoughtOrderListTableViewCell performCell:cell withDictionary:goodsListArray[indexPath.row]];
    
    return cell;
}

#pragma mark - scrollView Delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == _tableView) {
        [currentTableViewControl scrollViewDidScroll:scrollView];
    }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView == _tableView) {
        [currentTableViewControl scrollViewDidEndDragging:scrollView willDecelerate:(BOOL)decelerate];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == _tableView) {
        [currentTableViewControl scrollViewDidEndDecelerating:(UIScrollView *)scrollView];
    }
}

#pragma mark - ALTableViewControlDelegate
-(void)checkNewData
{
    [self checkNewData:currentTableViewControl];
}

-(void)checkNewData:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    
    willReplaceWithNew = YES;
    tableViewControl.lastDict[@"StartNum"] = @"0";
    
    ALLog(@"重新整理");
    [self addMBProgressHUDWithKey:@"myBoughtOrderListA019"];
    [self goodsListLoadApiSourceParameters:tableViewControl.lastDict
                               FinishBlock:^(BOOL isSuccess) {
                                   [self removeMBProgressHUDWithKey:@"myBoughtOrderListA019"];
                               }];
}

-(void)loadMore:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    
    ALLog(@"載入更多:目前第 %@ 頁 %lu 筆資料",tableViewControl.lastDict[@"StartNum"],(unsigned long)goodsListArray.count);
    
    tableViewControl.lastDict[@"StartNum"] =
    [NSString stringWithFormat:@"%d",[tableViewControl.lastDict[@"StartNum"] intValue] + 1];
    
    [self addMBProgressHUDWithKey:@"myBoughtOrderListA019"];
    [self goodsListLoadApiSourceParameters:tableViewControl.lastDict
                               FinishBlock:^(BOOL isSuccess) {
                                   [self removeMBProgressHUDWithKey:@"myBoughtOrderListA019"];
                               }];
}

#pragma mark - API
-(void)goodsListLoadApiSourceParameters:(NSDictionary *)parameters
                            FinishBlock:(void (^)(BOOL isSuccess))finishBlockToRun
{
    [ALNetWorkAPI myBoughtOrderListA019StartNum:parameters[@"StartNum"]
                                CompletionBlock:^(NSDictionary *responseObject) {
      
                                    BOOL isWiseData = YES;
                                    if (![responseObject[@"resBody"] isKindOfClass:[NSDictionary class]]) {
                                        isWiseData = NO;
                                        
                                        if (![responseObject[@"resBody"][@"OrderList"] isKindOfClass:[NSArray class]]) {
                                            isWiseData = NO;
                                        }
                                    }
                                    
                                    if (isWiseData) {
                                        if (willReplaceWithNew) {
                                            [goodsListArray removeAllObjects];
                                            willReplaceWithNew = NO;
                                        }
                                        
                                        NSArray *orderList = responseObject[@"resBody"][@"OrderList"];
                                        if ([orderList count] > 0) {
                                            [goodsListArray addObjectsFromArray:orderList];
                                        }
                                        else {
                                            #if TARGET_IPHONE_SIMULATOR && UseDevelopmentServer
                                            [self testDictionary];
                                            #endif
                                        }
                                        [self.tableView reloadData];
                                    }
                                    else {
                                        ALLog(@"A019_resBody_OrderList_格式錯誤");
                                    }
                                    finishBlockToRun(YES);
    }];
}

#pragma mark - ALBoughtOrderListTableViewCell delegate
- (void)didPressedCell:(ALBoughtOrderListTableViewCell *)cell
{
    NSDictionary *soldOrderDictionsry = cell.infoDict;
    ALBoughtOrderContentViewController *vc =
    [[ALBoughtOrderContentViewController alloc] initWithInfoDict:soldOrderDictionsry];
    vc.tableDelegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didPressedConnectToSellerButtonOnCell:(ALBoughtOrderListTableViewCell *)cell
{
    NSDictionary *soldOrderDictionsry = cell.infoDict;
    NSString *otherId = [soldOrderDictionsry[@"SellerID"] description];
    [self chatWithOtherId:otherId];
}

- (void)didPressedRatingButtonOnCell:(ALBoughtOrderListTableViewCell *)cell
{
    if ([cell.infoDict[@"OrderStatus"] intValue] == 2) {

    }
    else {
        ALLog(@"cell 動作設定錯誤");
        return;
    }

    
    NSDictionary *soldOrderDictionsry = cell.infoDict;
    
    NSString *commentID = soldOrderDictionsry[@"CommentID"];
    if (commentID.length > 0) {
        [ALNetWorkAPI categoryCommentA031OrderID:soldOrderDictionsry[@"OrderID"]
                                        startNum:@"0"
                                 completionBlock:^(NSDictionary *responseObject) {
                                     
                                     BOOL isWiseData = YES;
                                     if (![responseObject[@"resBody"] isKindOfClass:[NSDictionary class]]) {
                                         isWiseData = NO;
                                         
                                         if (![responseObject[@"resBody"][@"CommentList"] isKindOfClass:[NSArray class]]) {
                                             isWiseData = NO;
                                         }
                                     }
                                     
                                     if (isWiseData) {
                                         [self startAddReply:responseObject[@"resBody"][@"CommentList"][0]];
                                     }
                                     else {
                                         ALLog(@"A031O回傳格式錯誤");
                                     }
                                 }];
    }
    else {
        [self startCreateComment:soldOrderDictionsry];
    }
}

-(void)startCreateComment:(NSDictionary *)orderDict
{
    ALCommentViewController *next = [[ALCommentViewController alloc] initWithInfoDict:orderDict];
    next.tableDelegate = self;
    [self.navigationController pushViewController:next animated:YES];
}

-(void)startAddReply:(NSDictionary *)commentDict
{
    NSMutableDictionary *dataDict = [commentDict mutableCopy];
    dataDict[ALReplyCommentOwnerKey] = [ALAppDelegate sharedAppDelegate].userID;
    
    ALReplyViewController *next = [[ALReplyViewController alloc] initWithInfoDict:dataDict];
    [self.navigationController pushViewController:next animated:YES];
}


@end
