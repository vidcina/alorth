//
//  ALBoughtOrderViewController.h
//  alorth
//
//  Created by John Hsu on 2015/6/30.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicViewController.h"
#import "ALBoughtOrderListTableViewCell.h"

@interface ALBoughtOrderViewController : ALBasicSubViewController
<UITableViewDelegate, UITableViewDataSource, ALTableViewControlDelegate,
ALBoughtOrderListTableViewCellDelegate>
{
    NSMutableArray *goodsListArray;
    
    BOOL willReplaceWithNew;
    ALTableViewControl *currentTableViewControl;
    
    ALBoughtOrderListTableViewCell *demoCell;
}
@property (nonatomic,strong)IBOutlet UITableView *tableView;
-(void)checkNewData;

@end
