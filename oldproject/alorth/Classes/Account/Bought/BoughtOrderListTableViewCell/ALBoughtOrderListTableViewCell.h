//
//  ALBoughtOrderListTableViewCell.h
//  alorth
//
//  Created by Chih-Ju Huang on 2015/7/16.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ALBoughtOrderListTableViewCellDelegate;

@interface ALBoughtOrderListTableViewCell : UITableViewCell

@property (nonatomic, strong) NSDictionary *infoDict;
@property (nonatomic, weak) id<ALBoughtOrderListTableViewCellDelegate> delegate;

//上區
@property (nonatomic, weak) IBOutlet UIImageView *productImageView;
@property (nonatomic, weak) IBOutlet UILabel *productTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *priceLabel;

@property (nonatomic, weak) IBOutlet UILabel *restTimeLabel;
@property (nonatomic, weak) IBOutlet UIImageView *statusImageView;
@property (nonatomic, weak) IBOutlet UILabel *statusLabel;

@property (nonatomic, weak) IBOutlet UIButton *mainFakeButton;

//左下區
@property (nonatomic, weak) IBOutlet UIImageView *connectBuyerImageView;
@property (nonatomic, weak) IBOutlet UILabel *connectSellerLabel;

//右下區
@property (nonatomic, weak) IBOutlet UIButton *ratingButton;

//區域的分隔線(定位用)
@property (nonatomic, weak) IBOutlet UILabel *middleHorizontalLine;
@property (nonatomic, weak) IBOutlet UILabel *lowerHorizontalLine;

+ (ALBoughtOrderListTableViewCell *)cell;
+ (CGFloat)performCell:(ALBoughtOrderListTableViewCell *)cell withDictionary:(NSDictionary *)dictionary;

@end

#pragma mark - delegate
@protocol ALBoughtOrderListTableViewCellDelegate <NSObject>
@optional
- (void)didPressedCell:(ALBoughtOrderListTableViewCell *)cell;
- (void)didPressedConnectToSellerButtonOnCell:(ALBoughtOrderListTableViewCell *)cell;
- (void)didPressedRatingButtonOnCell:(ALBoughtOrderListTableViewCell *)cell;
@end