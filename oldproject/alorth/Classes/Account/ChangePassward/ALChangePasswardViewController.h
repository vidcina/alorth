//
//  ALChangePasswardViewController.h
//  alorth
//
//  Created by Chih-Ju Huang on 2015/7/17.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ALChangePasswardViewController : ALBasicSubViewController <UITextFieldDelegate, UIAlertViewDelegate>

@property (nonatomic, weak) IBOutlet UITextField *firstTextField;
@property (nonatomic, weak) IBOutlet UITextField *secondTextField;

@property (nonatomic, weak) IBOutlet UIButton *confirmButton;

@end
