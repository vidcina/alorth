//
//  ALChangePasswardViewController.m
//  alorth
//
//  Created by Chih-Ju Huang on 2015/7/17.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALChangePasswardViewController.h"
@interface ALChangePasswardViewController ()

@end

@implementation ALChangePasswardViewController

#pragma mark -
- (NSString *)checkIsPasswardIllegal
{
    if (![self.firstTextField.text isEqualToString:self.secondTextField.text]) {
        return ALLocalizedString(@"$The password you provided did not match$. $Please enter again$.",nil);
    }
    if (self.firstTextField.text.length < PASSWORD_MIN_LENGTH) {
        return [NSString stringWithFormat:ALLocalizedString(@"$Your password is too short$. $Please enter at least$ %@ $letters$",nil), @(PASSWORD_MIN_LENGTH)];
    }
    
    return nil;
}

- (void)performChangePassward
{
    NSString *string = self.firstTextField.text;
    
    [self addMBProgressHUDWithKey:@"chagePasswordA004"];
    [ALNetWorkAPI chagePasswordA004WithNewPassword:string
                                   CompletionBlock:^(NSDictionary *responseObject) {
        [self removeMBProgressHUDWithKey:@"chagePasswordA004"];
        
        if ([ALNetWorkAPI checkSerialNumber:@"A004"
                             ResponseObject:responseObject]) {
            
            [PDSAccountManager loginAccount:[PDSAccountManager userName]
                                   Password:string
                            CompletionBlock:^(NSDictionary *responseObject) {
                                if (![PDSAccountManager isLoggedIn]) {
                                    YKSimpleAlert(ALLocalizedString(@"Password change failed",nil));
                                }
                                else {
                                    [self.navigationController popViewControllerAnimated:YES];
//                                    self.firstTextField.text = @"";
//                                    self.secondTextField.text = @"";
                                    YKSimpleAlert(ALLocalizedString(@"Password changed",nil));
                                }
                            }];
        }
        else {
            YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        }
    }];
}

#pragma mark - IBAction
- (IBAction)confirmButtonAction:(UIButton *)sender
{
    NSString *alertTitleString = @"Warning";
    NSString *alertMessageString = [self checkIsPasswardIllegal];
    
    if (alertMessageString) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALLocalizedString(alertTitleString, nil)
                                                            message:ALLocalizedString(alertMessageString, nil)
                                                           delegate:self
                                                  cancelButtonTitle:ALLocalizedString(@"OK", nil)
                                                  otherButtonTitles:nil];
        [alertView show];
    }
    else {
        //NSLog(@"1234");
        [self performChangePassward];
    }
}

- (IBAction)textFieldDidChangeText:(UITextField *)textField
{
    if ([self checkIsPasswardIllegal]) {
        self.confirmButton.enabled = NO;
    }
    else {
        self.confirmButton.enabled = YES;
    }
}

#pragma mark - UIAlertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    self.firstTextField.text = @"";
    self.secondTextField.text = @"";
}

#pragma mark - UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end
