//
//  ALSettingViewController.m
//  alorth
//
//  Created by John Hsu on 2015/6/30.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALSettingViewController.h"
#import "ALChangePasswardViewController.h"
#import "ALWebViewController.h"
#import "ALProfileModifyViewController.h"
#import "ALAppDelegate.h"
#import <Intercom/Intercom.h>

//typedef NS_ENUM(NSUInteger, ALSettingViewCells) {
//    ALSettingViewCellChangePassword,
//    ALSettingViewCellChangeLanguage,
//    ALSettingViewCellPrivacyPolicy,
//    ALSettingViewCellReport,
//    ALSettingViewCellEnd
//};

//@interface ALSettingViewController ()
//
//@end

@implementation ALSettingViewController

#pragma mark - VC Life
-(instancetype)init
{
    self = [super init];
    if (self) {
        cellTitleAndBlockArray = [@[
                                   @{
                                       @"Title" : @"Change Password",
                                       @"Action" : ^{
                                           ALChangePasswardViewController *vc = [[ALChangePasswardViewController alloc] init];
                                           [self.navigationController pushViewController:vc animated:YES];

                                       },
                                       },
                                   @{
                                       @"Title" : @"Privacy Policy",
                                       @"Action" : ^{
                                           ALWebViewController *vc = [[ALWebViewController alloc] init];
                                           vc.titleString = @"Privacy Policy";
                                           vc.urlString = @"http://alorth.com/web/privacy.html";
                                           
                                           [self.navigationController pushViewController:vc animated:YES];

                                       },
                                       },
                                   @{
                                       @"Title" : @"Report",
                                       @"Action" : ^{
                                           if (![MFMailComposeViewController canSendMail]) {
                                               YKSimpleAlert(ALLocalizedString(@"$Unable to send email$. $Please set up your email account in system settings$.", nil));
                                               return;
                                           }
                                           MFMailComposeViewController *vc = [[MFMailComposeViewController alloc] init];
                                           [vc setToRecipients:@[@"jones@alorth.com"]];
                                           [vc setMailComposeDelegate:self];
                                           [self.view.window.rootViewController presentViewController:vc animated:YES completion:nil];
                                       },
                                       },
                                   @{
                                       @"Title" : @"Change Language",
                                       @"Action" : ^{
                                           ALProfileModifyViewController *next = [[ALProfileModifyViewController alloc] init];
                                           next.modifykey = ALProfileModifyLanguage;
                                           [self.navigationController pushViewController:next animated:YES];
                                           
                                       },
                                       },
                                   ] mutableCopy];
        if ([PDSAccountManager isThirdPartyLogin]) {
            [cellTitleAndBlockArray removeObjectAtIndex:0];
        }
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tableView.contentInset = UIEdgeInsetsMake(15, 0, 0, 0);
    [self alTableViewSetting:self.tableView];
}

#pragma mark - IBAction
- (IBAction)logOutButtonAction:(UIButton *)sender
{
    [self addMBProgressHUDWithKey:@"logoutA022"];
    [ALNetWorkAPI logoutA022CompletionBlock:^(NSDictionary *responseObject) {
        [self removeMBProgressHUDWithKey:@"logoutA022"];
        [[ALAppDelegate sharedAppDelegate] clearUserInformation];
        [[PDSAccountManager sharedManager] logout];
        [[GIDSignIn sharedInstance] signOut];
        [Intercom reset];
        ALNavigationViewController *nv = (ALNavigationViewController *)self.navigationController;
        [nv clearOtherController:ALNavigationSubClear
                            push:ALNavigationTabBarHome
                   subController:@[]];
    }];
}

#pragma mark - UITableViewCell delegate and date source
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [cellTitleAndBlockArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reuseIdentifier = @"CELL";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    NSString *title = cellTitleAndBlockArray[indexPath.row][@"Title"];
    cell.textLabel.localText = title;
    /*
    switch (indexPath.row) {
        case ALSettingViewCellChangePassword:
            cell.textLabel.localText = @"Change Password";
            break;
            
        case ALSettingViewCellPrivacyPolicy:
            cell.textLabel.localText = @"Privacy Policy";
            break;
            
        case ALSettingViewCellReport:
            cell.textLabel.localText = @"Report";
            break;
            
        case ALSettingViewCellChangeLanguage:
            cell.textLabel.localText = @"Change Language";
            break;
            
        default:
            break;
    }*/
    cell.textLabel.font = [UIFont systemFontOfSize:14.0f];
    cell.textLabel.textColor = COMMON_GRAY4_COLOR;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    void(^block)(void) = cellTitleAndBlockArray[indexPath.row][@"Action"];
    if (block) {
        block();
    }
    
    /*
    switch (indexPath.row) {
        case ALSettingViewCellChangePassword:
        {
            ALChangePasswardViewController *vc = [[ALChangePasswardViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case ALSettingViewCellPrivacyPolicy:
        {
            ALWebViewController *vc = [[ALWebViewController alloc] init];
            vc.titleString = @"Privacy Policy";
            vc.urlString = @"http://alorth.com/web/privacy.html";
            
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case ALSettingViewCellReport:
        {
            if (![MFMailComposeViewController canSendMail]) {
                YKSimpleAlert(ALLocalizedString(@"$Unable to send email$. $Please set up your email account in system settings$.", nil));
                return;
            }
            MFMailComposeViewController *vc = [[MFMailComposeViewController alloc] init];
            [vc setToRecipients:@[@"jones@Alorth.com"]];
            [vc setMailComposeDelegate:self];
            [self.view.window.rootViewController presentViewController:vc animated:YES completion:nil];
            
     
        }
            break;
        case ALSettingViewCellChangeLanguage:
        {
            ALProfileModifyViewController *next = [[ALProfileModifyViewController alloc] init];
            next.modifykey = ALProfileModifyLanguage;
            [self.navigationController pushViewController:next animated:YES];
        }
            break;
            
        default:
            break;
    }
     */
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
    if (error) {
        YKSimpleAlert(@"%@",[error localizedDescription]);
    }
}

@end
