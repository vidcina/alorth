//
//  ALEarnListViewController.h
//  alorth
//
//  Created by w91379137 on 2015/10/13.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicSubViewController.h"

@interface ALEarnListViewController : ALBasicSubViewController
<UITableViewDataSource, UITableViewDelegate>
{
    //清單
    NSMutableArray *objectListArray;
    BOOL willReplaceWithNew;                    //是否清空之前資料
    ALTableViewControl *currentTableViewControl;
}

@property(nonatomic, strong) IBOutlet UITableView *mainTableView;

@property(nonatomic, strong) IBOutlet UITableViewCell *sellingCommissionCell;
@property(nonatomic, strong) IBOutlet UITableViewCell *findGoodsToSaleCell;
@property(nonatomic, strong) IBOutlet UITableViewCell *agreeCommissionCell;

@end
