//
//  ALGrossSalesListController+PullMore.m
//  alorth
//
//  Created by w91379137 on 2015/12/1.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALGrossSalesListController+PullMore.h"
static NSString *earnGrossSalesListA048 = @"earnGrossSalesListA048";

@implementation ALGrossSalesListController (PullMore)

#pragma mark - scrollView Delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.mainTableView) {
        [currentTableViewControl scrollViewDidScroll:scrollView];
    }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView == self.mainTableView) {
        [currentTableViewControl scrollViewDidEndDragging:scrollView willDecelerate:(BOOL)decelerate];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.mainTableView) {
        [currentTableViewControl scrollViewDidEndDecelerating:(UIScrollView *)scrollView];
    }
}

#pragma mark - ALTableViewControlDelegate
-(void)checkNewData:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    willReplaceWithNew = YES;
    tableViewControl.lastDict[@"StartNum"] = @"0";
    
    ALLog(@"重新整理");
    [self addMBProgressHUDWithKey:earnGrossSalesListA048];
    [self goodsListLoadApiSourceParameters:tableViewControl.lastDict
                               FinishBlock:^(BOOL isSuccess) {
                                   [self removeMBProgressHUDWithKey:earnGrossSalesListA048];
                               }];
}

-(void)loadMore:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    
    ALLog(@"載入更多:目前第 %@ 頁 %lu 筆資料",
          tableViewControl.lastDict[@"StartNum"],
          (unsigned long)currentTableViewControl.apiDataArray.count);
    
    tableViewControl.lastDict[@"StartNum"] =
    [NSString stringWithFormat:@"%d",[tableViewControl.lastDict[@"StartNum"] intValue] + 1];
    
    [self addMBProgressHUDWithKey:earnGrossSalesListA048];
    [self goodsListLoadApiSourceParameters:tableViewControl.lastDict
                               FinishBlock:^(BOOL isSuccess) {
                                   [self removeMBProgressHUDWithKey:earnGrossSalesListA048];
                               }];
}

#pragma mark - API
-(void)goodsListLoadApiSourceParameters:(NSDictionary *)parameters
                            FinishBlock:(void (^)(BOOL isSuccess))finishBlockToRun
{
    [ALNetWorkAPI earnGrossSalesListA048ProductOwner:self.infoDict.safe[@"ProductOwner"]
                                            SellerID:self.infoDict.safe[@"SellerID"]
                                            StartNum:parameters[@"StartNum"]
                                     CompletionBlock:^(NSDictionary *responseObject) {
                                         
                                         BOOL isWiseData =
                                         [self checkListData:responseObject];
                                         
                                         if (!isWiseData) ALLog(@"%@ 錯誤",earnGrossSalesListA048);
                                         finishBlockToRun(isWiseData);
                                     }];
}

- (BOOL)checkListData:(NSDictionary *)responseObject
{
    BOOL isWiseData = YES;
    NSArray *addArray = nil;
    
    isWiseData =
    isWiseData && [ALNetWorkAPI checkSerialNumber:@"A048" ResponseObject:responseObject];
    
    isWiseData =
    isWiseData && responseObject.safe[@"resBody"][@"GrossSalesList"];
    
    isWiseData =
    isWiseData && [responseObject[@"resBody"][@"GrossSalesList"] isKindOfClass:[NSArray class]];
    
    if (isWiseData) {
        addArray = responseObject[@"resBody"][@"GrossSalesList"];
        
        #if TARGET_IPHONE_SIMULATOR
        if (addArray.count == 0) {
            addArray = @[[self testDictionary]];
        }
        #endif
        
        if (willReplaceWithNew) {
            [currentTableViewControl.apiDataArray removeAllObjects];
        }
        
        [currentTableViewControl.apiDataArray addObjectsFromArray:addArray];
        [self.mainTableView reloadData];
        
        if (willReplaceWithNew) {
            [self.mainTableView setContentOffset:CGPointZero animated:YES];
        }
        willReplaceWithNew = NO;
    }
    return isWiseData;
}

- (NSDictionary *)testDictionary
{
    NSDictionary *dict = @{@"ProductID" : @"1234",
                           @"ProductTitle" : @"A048_模擬器測試資料\nA048_模擬器測試資料",
                           @"ProductImage" : @"http://www.case.edu/images/2014/icon-gearwheels.min.png",
                           @"Price" : @"350",
                           @"Currency" : @"昆布",
                           @"OrderDateTime" : [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]]};
    return dict;
}

@end
