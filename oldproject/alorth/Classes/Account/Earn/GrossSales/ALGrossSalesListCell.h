//
//  ALGrossSalesListCell.h
//  alorth
//
//  Created by w91379137 on 2015/12/1.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALCommonTableViewCell.h"

@interface ALGrossSalesListCell : ALCommonTableViewCell

@property(nonatomic, strong) IBOutlet UIImageView *productImageView;
@property(nonatomic, strong) IBOutlet UILabel *productNameLabel;
@property(nonatomic, strong) IBOutlet UILabel *productPriceLabel;

@property(nonatomic, strong) IBOutlet UILabel *leftNameLabel;
@property(nonatomic, strong) IBOutlet UIView *leftRateView;

@property(nonatomic, strong) IBOutlet UILabel *rightNameLabel;
@property(nonatomic, strong) IBOutlet UIView *rightRateView;

@property(nonatomic, strong) IBOutlet UILabel *orderTimeLabel;

@end
