//
//  ALGrossSalesListController.m
//  alorth
//
//  Created by w91379137 on 2015/12/1.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALGrossSalesListController.h"
#import "ALGrossSalesListController+PullMore.h"

#import "ALGrossSalesListCell.h"

@interface ALGrossSalesListController ()
{
    ALGrossSalesListCell *demoCell;
}

@end

@implementation ALGrossSalesListController

#pragma mark - VC Life
- (instancetype)init
{
    self = [super init];
    if (self) {
        objectListArray = [NSMutableArray array];
        
        currentTableViewControl = [[ALTableViewControl alloc] init];
        currentTableViewControl.delegate = self;
        currentTableViewControl.apiDataArray = objectListArray;
    }
    return self;
}

-(void)viewWillAppearFirstTime:(BOOL)animated
{
    [super viewWillAppearFirstTime:animated];
    [headerView.backButton addTarget:self
                              action:@selector(back:)
                    forControlEvents:UIControlEventTouchUpInside];
    
    [self alTableViewSetting:self.mainTableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (objectListArray.count == 0) {
        [self checkNewData:currentTableViewControl];
    }
}

#pragma mark - UITableViewDataSource Row
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return objectListArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!demoCell) {
        demoCell = [ALGrossSalesListCell cellFromXib];
    }
    return demoCell.frame.size.height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ALGrossSalesListCell *cell = [ALGrossSalesListCell cellFromXib];
    [cell performWithInfoDict:objectListArray[indexPath.row]];
    
    cell.leftNameLabel.text =
    [ALFormatter preferredDisplayNameFirst:self.infoDict[@"ProductOwnerFirstName"]
                                      Last:self.infoDict[@"ProductOwnerLastName"]
                                    UserID:self.infoDict[@"ProductOwner"]];
    [cell.leftRateView rankStar:5 get:[self.infoDict.safe[@"ProductOwnerRating"] floatValue]];
    
    cell.rightNameLabel.text =
    [ALFormatter preferredDisplayNameFirst:self.infoDict[@"SellerFirstName"]
                                      Last:self.infoDict[@"SellerLastName"]
                                    UserID:self.infoDict[@"SellerID"]];
    [cell.rightRateView rankStar:5 get:[self.infoDict.safe[@"SellerRating"] floatValue]];
    
    return cell;
}

@end
