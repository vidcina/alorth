//
//  ALGrossSalesListController.h
//  alorth
//
//  Created by w91379137 on 2015/12/1.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALBasicSubViewController.h"

@interface ALGrossSalesListController : ALBasicSubViewController
<UITableViewDataSource, UITableViewDelegate>
{
    //清單
    NSMutableArray *objectListArray;
    BOOL willReplaceWithNew;                    //是否清空之前資料
    ALTableViewControl *currentTableViewControl;
}

@property(nonatomic, strong) NSDictionary *infoDict;
@property(nonatomic, strong) IBOutlet UITableView *mainTableView;

@end
