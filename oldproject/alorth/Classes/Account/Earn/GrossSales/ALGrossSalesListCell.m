//
//  ALGrossSalesListCell.m
//  alorth
//
//  Created by w91379137 on 2015/12/1.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALGrossSalesListCell.h"

@implementation ALGrossSalesListCell

-(void)cellFromXibSetting
{
    self.leftRateView.backgroundColor = [UIColor whiteColor];
    self.rightRateView.backgroundColor = [UIColor whiteColor];
}

- (CGFloat)performWithInfoDict:(NSDictionary *)infoDict
{
    self.infoDict = infoDict;
    
    [self.productImageView imageFromURLString:infoDict[@"ProductImage"]];
    
    self.productNameLabel.text = infoDict.safe[@"ProductTitle"];
    self.productPriceLabel.text =
    [NSString stringWithFormat:@"%@ %@",infoDict.safe[@"Price"],infoDict.safe[@"Currency"]];
    
    self.orderTimeLabel.text =
    [ALFormatter prettyLocelTimeFormat:fullTimeFormat
                               ForDate:[NSDate dateWithTimeIntervalSince1970:[self.infoDict.safe[@"OrderDateTime"] intValue]]];
    
    return self.frame.size.height;
}

@end
