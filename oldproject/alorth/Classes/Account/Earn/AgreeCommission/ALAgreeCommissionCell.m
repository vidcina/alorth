//
//  ALAgreeCommissionCell.m
//  alorth
//
//  Created by w91379137 on 2015/10/19.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALAgreeCommissionCell.h"

@implementation ALAgreeCommissionCell

- (void)cellFromXibSetting
{
    self.productTitleLabel.numberOfLines = 0;
    self.sellerRateView.backgroundColor = [UIColor clearColor];
}

#pragma mark - heightForRow
- (CGFloat)performWithInfoDict:(NSDictionary *)infoDict
{
    self.infoDict = infoDict;
    
    [self.productImageView imageFromURLString:infoDict[@"ProductImage"]];
    self.productTitleLabel.text = infoDict[@"ProductTitle"];
    
    self.sellerNameLabel.text =
    [ALFormatter preferredDisplayNameFirst:infoDict[@"SellerFirstName"]
                                      Last:infoDict[@"SellerLastName"]
                                    UserID:infoDict[@"SellerID"]];
    
    float rate = [infoDict.safe[@"SellerRating"] floatValue];
    [self.sellerRateView rankStar:5 get:rate];
    
    int earnPercentage = [infoDict.safe[@"EarnPercentage"] intValue];
    int oldEarnPercentage = [infoDict.safe[@"OldEarnPercentage"] intValue];
    
    if ([infoDict.safe[@"AgreeStatus"] intValue] == 1) {
        //等待中
        self.statusImageView.image = [UIImage imageNamed:@"RateClock"];
        self.statusLabel.localText = @"Waiting for agree";
        
        if (oldEarnPercentage == 0) {
            //只要顯示新的 只有移除
            self.percentLabel.text =
            [NSString stringWithFormat:@"%@ %%",infoDict[@"EarnPercentage"]];
        }
        else if (earnPercentage == oldEarnPercentage){
            //兩者相同 只有移除
            self.percentLabel.text =
            [NSString stringWithFormat:@"%@ %%",infoDict[@"EarnPercentage"]];
            [self.agreeButton mas_updateConstraints:^(MASConstraintMaker *make) {
                make.leading.equalTo(self.agreeButton.superview.mas_trailing);
            }];
        }
        else {
            //兩者不同 移除＋同意
            self.percentLabel.text =
            [NSString stringWithFormat:@"%@%%  ➤  %@ %%",infoDict[@"OldEarnPercentage"],infoDict[@"EarnPercentage"]];
        }
    }
    else if ([infoDict.safe[@"AgreeStatus"] intValue] == 2){
        //同意
        self.statusImageView.image = [UIImage imageNamed:@"FinishClock"];
        self.statusLabel.text = @"Agreed";
        
        self.percentLabel.text =
        [NSString stringWithFormat:@"%@ %%",infoDict[@"OldEarnPercentage"]];
        
        [self.agreeButton mas_updateConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(self.agreeButton.superview.mas_trailing);
        }];
    }
    else {
        self.statusImageView.image = nil;
        self.statusLabel.text = @"";
        
        self.percentLabel.hidden = YES;
    }
    
    return self.buttonContainerView.frame.origin.y + self.buttonContainerView.frame.size.height + 10;
}

#pragma mark - IBAction
-(IBAction)removeAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(removeActionCell:)]) {
        [self.delegate removeActionCell:self];
    }
}

-(IBAction)agreeAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(agreeActionCell:)]) {
        [self.delegate agreeActionCell:self];
    }
}

-(IBAction)chatAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(chatActionCell:)]) {
        [self.delegate chatActionCell:self];
    }
}


@end
