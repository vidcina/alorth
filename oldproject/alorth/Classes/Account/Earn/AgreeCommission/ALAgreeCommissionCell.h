//
//  ALAgreeCommissionCell.h
//  alorth
//
//  Created by w91379137 on 2015/10/19.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALCommonTableViewCell.h"
@protocol ALAgreeCommissionCellDelegate;

@interface ALAgreeCommissionCell : ALCommonTableViewCell

@property (nonatomic, weak) NSObject<ALAgreeCommissionCellDelegate> *delegate;
@property (nonatomic, strong) IBOutlet UIView *mainContainerView;
@property (nonatomic, strong) IBOutlet UIImageView *productImageView;
@property (nonatomic, strong) IBOutlet UILabel *productTitleLabel;

@property (nonatomic, strong) IBOutlet UILabel *sellerNameLabel;
@property (nonatomic, strong) IBOutlet UIView *sellerRateView;
@property (nonatomic, strong) IBOutlet UIImageView *statusImageView;
@property (nonatomic, strong) IBOutlet UILabel *statusLabel;
@property (nonatomic, strong) IBOutlet UILabel *percentLabel;

@property (nonatomic, strong) IBOutlet UIView *buttonContainerView;
@property (nonatomic, strong) IBOutlet UIButton *agreeButton;

@end

#pragma mark - ALAgreeCommissionCellDelegate
@protocol ALAgreeCommissionCellDelegate <NSObject>
@optional
- (void)removeActionCell:(ALAgreeCommissionCell *)cell;
- (void)agreeActionCell:(ALAgreeCommissionCell *)cell;
- (void)chatActionCell:(ALAgreeCommissionCell *)cell;
@end
