//
//  ALAgreeCommissionViewController.m
//  alorth
//
//  Created by w91379137 on 2015/10/18.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALAgreeCommissionViewController.h"
#import "ALAgreeCommissionViewController+PullMore.h"

@interface ALAgreeCommissionViewController ()
{
    ALAgreeCommissionCell *demoCell;
}
@end

@implementation ALAgreeCommissionViewController

#pragma mark - VC Life
- (instancetype)init
{
    self = [super init];
    if (self) {
        objectListArray = [NSMutableArray array];
        
        currentTableViewControl = [[ALTableViewControl alloc] init];
        currentTableViewControl.delegate = self;
        currentTableViewControl.apiDataArray = objectListArray;
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (objectListArray.count == 0) {
        [self checkNewData:currentTableViewControl];
    }
}

-(void)viewWillAppearFirstTime:(BOOL)animated
{
    [super viewWillAppearFirstTime:animated];
    [headerView.backButton addTarget:self
                              action:@selector(back:)
                    forControlEvents:UIControlEventTouchUpInside];
    
    [self alTableViewSetting:self.mainTableView];
}

#pragma mark - UITableViewDataSource Row
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return objectListArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (demoCell == nil) {
        demoCell = [ALAgreeCommissionCell cellFromXib];
        demoCell.frame = CGRectMake(0, 0, tableView.contentSize.width, 0);
        [demoCell layoutIfNeeded];
    }
    return [demoCell performWithInfoDict:nil];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self tableView:tableView heightForRowAtIndexPath:indexPath] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ALAgreeCommissionCell *cell = [ALAgreeCommissionCell cellFromXib];
    cell.delegate = self;
    [cell performWithInfoDict:objectListArray[indexPath.row]];
    return cell;
}

#pragma mark - ALAgreeCommissionCellDelegate
- (void)removeActionCell:(ALAgreeCommissionCell *)cell
{
    NSDictionary *infoDict = cell.infoDict;
    [ALNetWorkAPI checkAgreeCommissionA046ProductID:infoDict[@"ProductID"]
                                        ProductName:infoDict[@"ProductTitle"]
                                  ApplicationUserID:infoDict[@"SellerID"]
                                     EarnPercentage:infoDict[@"OldEarnPercentage"]
                                   CommissionStatus:@"0"
                                    CompletionBlock:^(NSDictionary *responseObject) {
                                        if ([ALNetWorkAPI checkSerialNumber:@"A046"
                                                             ResponseObject:responseObject]) {
                                            
                                            [self updateObjectListArray:infoDict
                                                                  Renew:responseObject[@"resBody"]];
                                        }
                                        else {
                                            YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
                                        }
                                    }];
}

- (void)agreeActionCell:(ALAgreeCommissionCell *)cell
{
    NSDictionary *infoDict = cell.infoDict;
    [ALNetWorkAPI checkAgreeCommissionA046ProductID:infoDict[@"ProductID"]
                                        ProductName:infoDict[@"ProductTitle"]
                                  ApplicationUserID:infoDict[@"SellerID"]
                                     EarnPercentage:infoDict[@"EarnPercentage"]
                                   CommissionStatus:@"2"
                                    CompletionBlock:^(NSDictionary *responseObject) {
                                        if ([ALNetWorkAPI checkSerialNumber:@"A046"
                                                             ResponseObject:responseObject]) {
                                            
                                            [self updateObjectListArray:infoDict
                                                                  Renew:responseObject[@"resBody"]];
                                        }
                                        else {
                                            YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
                                        }
                                    }];
}

- (void)updateObjectListArray:(NSDictionary *)infoDict Renew:(NSDictionary *)renewDict
{
    NSInteger index = [objectListArray indexOfObject:infoDict];
    
    if (index != NSNotFound) {
        
        //確認 是否移除
        if ([renewDict.safe[@"AgreeStatus"] intValue] == 0) {
            [objectListArray removeObject:infoDict];
        }
        else {
            //更新資料
            NSMutableDictionary *copy = [infoDict mutableCopy];
            NSDictionary *change = renewDict;
            for (NSString *key in change.allKeys) {
                if ([copy.allKeys containsObject:key]) {
                    copy[key] = change[key];
                }
            }
            [objectListArray replaceObjectAtIndex:index withObject:copy];
        }
    }
    else {
        NSLog(@"NSNotFound");
    }
    [self.mainTableView reloadData];
}

- (void)chatActionCell:(ALAgreeCommissionCell *)cell
{
    NSString *otherID = cell.infoDict[@"SellerID"];
    [self chatWithOtherId:otherID];
}

@end
