//
//  ALFindGoodsToSaleCell.h
//  alorth
//
//  Created by w91379137 on 2015/10/19.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALCommonTableViewCell.h"
#import "ALSelectGroup.h"
@class ALFindGoodsToSaleViewController;

@interface ALFindGoodsToSaleCell : ALCommonTableViewCell
{
    IBOutlet UIView *userListView;
}
@property (nonatomic, weak) ALSelectGroup *selectGroup;

@property(nonatomic, strong) IBOutlet UIImageView *productImageView;
@property(nonatomic, strong) IBOutlet UILabel *productNameLabel;

@end
