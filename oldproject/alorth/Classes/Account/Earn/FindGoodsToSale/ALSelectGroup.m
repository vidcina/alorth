//
//  ALSelectGroup.m
//  alorth
//
//  Created by w91379137 on 2015/12/3.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "NSObject+WeakReference.h"
#import "ALSelectGroup.h"

@implementation ALSelectGroup

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.weakSelectTabel = [NSMutableDictionary dictionary];
        self.selectKeysArray = [NSMutableArray array];
    }
    return self;
}

- (BOOL)isAcceptAdd:(NSObject<ALSelectGroupble> *)object
{
    if (![self keyOfObject:object]) {
        return NO;
    }
    [self.weakSelectTabel setObject:MakeWeakReference(object) forKey:[self keyOfObject:object]];
    
    [object select:[self.selectKeysArray containsObject:[self keyOfObject:object]]
             Group:self];
    
    return YES;//等有delegate 再實做如何拒絕加入
}

- (BOOL)select:(NSObject<ALSelectGroupble> *)object
{
    if (![self keyOfObject:object]) {
        return NO;
    }
    
    //目前是單選
    if (self.selectKeysArray.count == 0) {
        
    }
    else {
        NSString *key = self.selectKeysArray.firstObject;
        
        NSObject<ALSelectGroupble> *lastObject =
        WeakReferenceNonretainedObjectValue(self.weakSelectTabel[key]);
        
        [lastObject select:NO Group:self];
        [self.selectKeysArray removeObject:key];
    }
    
    [object select:YES Group:self];
    [self.selectKeysArray addObject:[self keyOfObject:object]];
    
    return YES;
}

- (NSString *)keyOfObject:(NSObject<ALSelectGroupble> *)object
{
    if (![object conformsToProtocol:@protocol(ALSelectGroupble)]) {
        return nil;
    }
    
    NSString *key = [object uuIDInGroup:self];
    if (![key isKindOfClass:[NSString class]]) {
        return nil;
    }
    return key;
}

- (id)didSelectobjectAt:(NSInteger)index
{
    if (index < 0 || index >= self.selectKeysArray.count) {
        return nil;
    }
    
    NSString *key = self.selectKeysArray[index];
    return WeakReferenceNonretainedObjectValue(self.weakSelectTabel[key]);
}

@end
