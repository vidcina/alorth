//
//  ALFindGoodsToSaleViewController.m
//  alorth
//
//  Created by w91379137 on 2015/10/18.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALFindGoodsToSaleViewController.h"
#import "ALFindGoodsToSaleViewController+PullMore.h"

#import "ALFindGoodsToSaleApplyViewController.h"
#import "ALFindGoodsToSaleRowView.h"

@interface ALFindGoodsToSaleViewController ()
{
    ALFindGoodsToSaleCell *demoCell;
    ALSelectGroup *selectGroup;
}

@end

@implementation ALFindGoodsToSaleViewController

#pragma mark - VC Life
- (instancetype)init
{
    self = [super init];
    if (self) {
        objectListArray = [NSMutableArray array];
        
        currentTableViewControl = [[ALTableViewControl alloc] init];
        currentTableViewControl.delegate = self;
        currentTableViewControl.apiDataArray = objectListArray;
        
        selectGroup = [[ALSelectGroup alloc] init];
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (objectListArray.count == 0) {
        [self startSearch];
    }
}

-(void)viewWillAppearFirstTime:(BOOL)animated
{
    [super viewWillAppearFirstTime:animated];
    [headerView.backButton addTarget:self
                              action:@selector(back:)
                    forControlEvents:UIControlEventTouchUpInside];
    
    [self alTableViewSetting:self.mainTableView];
    
    if (self.listStatus == ALGoodsSearchViewControllerStatusInit) {
        self.listStatus = ALGoodsSearchViewControllerStatusNewest;
    }
}

#pragma mark - Setter
- (void)setListStatus:(ALGoodsSearchViewControllerStatus)listStatus
{
    if (_listStatus == listStatus) {
        return;
    }
    _listStatus = listStatus;
    [self startSearch];
}

#pragma mark - ALSelectTypeScrollViewDelegate
-(UIView *)typeViewOfIndex:(NSInteger)index
                infoObject:(NSObject *)infoObject
      selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    if ([infoObject isKindOfClass:[NSNumber class]]) {
        
        NSString *title = nil;
        NSNumber *number = (NSNumber *)infoObject;
        ALGoodsSearchViewControllerStatus status = [number integerValue];
        if (status == ALGoodsSearchViewControllerStatusBestMatch) title = @"Best Match";
        if (status == ALGoodsSearchViewControllerStatusBestSell) title = @"Best sellers";
        if (status == ALGoodsSearchViewControllerStatusNewest) title = @"Newest";
        
        UIButton *aButton =
        [ALSelectTypeScrollView alSortStyleSelectButton:title];
        
        [aButton addTarget:selectTypeScrollView
                    action:@selector(selectView:)
          forControlEvents:UIControlEventTouchUpInside];
        
        [selectTypeScrollView addSubview:aButton]; //不先加上去 無法執行下一步
        [aButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(selectTypeScrollView.mas_width).multipliedBy(1.0 / selectTypeScrollView.infoObjectArray.count).offset(-1);
        }];
        
        return aButton;
    }
    else {
        return [[UIView alloc] initWithFrame:CGRectZero];
    }
}

-(void)didSelectIndex:(NSInteger)index
           infoObject:(NSObject *)infoObject
 selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    NSNumber *number = (NSNumber *)infoObject;
    self.listStatus = [number integerValue];
}

-(void)adjustSelectView:(UIView *)view
             infoObject:(NSObject *)infoObject
               selected:(BOOL)selected
   selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    [ALSelectTypeScrollView alSortStyleSelectButton:(UIButton *)view Select:selected];
}

#pragma mark -
- (void)startSearch
{
    NSMutableDictionary *newStart = [NSMutableDictionary dictionary];
    if (self.listStatus == ALGoodsSearchViewControllerStatusBestMatch) newStart[@"Order"] = @"4";
    if (self.listStatus == ALGoodsSearchViewControllerStatusBestSell) newStart[@"Order"] = @"2";
    if (self.listStatus == ALGoodsSearchViewControllerStatusNewest) newStart[@"Order"] = @"0";
    if (topSearchBar.text.length > 0) newStart[@"keyWord"] = topSearchBar.text;
    
    currentTableViewControl.lastDict = newStart;
    
    [self checkNewData:currentTableViewControl];
}

#pragma mark - UISearchBarDelegate
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [topSearchBar resignFirstResponder];
    [self startSearch];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = NO;
    [topSearchBar resignFirstResponder];
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = YES;
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = NO;
}

#pragma mark - UITableViewDataSource Row
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return objectListArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (demoCell == nil) {
        demoCell = [ALFindGoodsToSaleCell cellFromXib];
        demoCell.frame = CGRectMake(0, 0, tableView.contentSize.width, 0);
        [demoCell layoutIfNeeded];
    }
    return [demoCell performWithInfoDict:[self dataOfTableView:tableView AtIndexPath:indexPath]];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self tableView:tableView heightForRowAtIndexPath:indexPath] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ALFindGoodsToSaleCell *cell = [ALFindGoodsToSaleCell cellFromXib];
    cell.selectGroup = selectGroup;
    [cell performWithInfoDict:[self dataOfTableView:tableView AtIndexPath:indexPath]];
    return cell;
}

- (NSDictionary *)dataOfTableView:(UITableView *)tableView AtIndexPath:(NSIndexPath *)indexPath
{
    return objectListArray[indexPath.row];
}

#pragma mark - ALFindGoodsToSaleCell
- (IBAction)applyAction:(id)sender
{
    id obj = [selectGroup didSelectobjectAt:0];
    
    if (![obj isKindOfClass:[ALFindGoodsToSaleRowView class]]) {
        NSLog(@"沒有商品");
        return;
    }
    ALFindGoodsToSaleRowView *rowView = (ALFindGoodsToSaleRowView *)obj;
    
    ALFindGoodsToSaleApplyViewController *nextVC = [[ALFindGoodsToSaleApplyViewController alloc] init];
    nextVC.productInfoDict = rowView.delegate.infoDict;
    nextVC.userInfoDict = rowView.rowData;
    
    [self.navigationController pushViewController:nextVC animated:YES];
}

@end
