//
//  ALFindGoodsToSaleCell.m
//  alorth
//
//  Created by w91379137 on 2015/10/19.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALFindGoodsToSaleCell.h"
#import "ALFindGoodsToSaleRowView.h"

#import "ALFindGoodsToSaleViewController.h"

@implementation ALFindGoodsToSaleCell

- (void)cellFromXibSetting
{
    self.productNameLabel.numberOfLines = 0;
}

- (CGFloat)performWithInfoDict:(NSDictionary *)infoDict
{
    self.infoDict = infoDict;
    
    [self.productImageView imageFromURLString:infoDict[@"ProductImage"]];
    self.productNameLabel.text = infoDict[@"ProductTitle"];
    
    NSArray *userList = infoDict[@"UserList"];
    
    for (UIView *view in userListView.subviews) {
        [view removeFromSuperview];
    }
    
    UIView *lastOne = nil;
    for (int k = 0; k < userList.count; k++) {
        
        ALFindGoodsToSaleRowView *aRowView =
        [[ALFindGoodsToSaleRowView alloc] initWithFrame:CGRectZero];
        
        aRowView.rowData = userList[k];
        aRowView.delegate = self;
        [self.selectGroup isAcceptAdd:aRowView];
        
        [userListView addSubview:aRowView];
        [aRowView mas_makeConstraints:^(MASConstraintMaker *make) {
            
            if (lastOne == nil) {
                make.top.equalTo(userListView.mas_top).offset(10);
            }
            else {
                make.top.equalTo(lastOne.mas_bottom).offset(10);
            }
            make.leading.equalTo(userListView.mas_leading).offset(10);
            make.trailing.equalTo(userListView.mas_trailing).offset(-10);
            
            make.height.equalTo(@(35));
        }];
        lastOne = aRowView;
    }
    
    if (lastOne) {
        [lastOne mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(userListView.mas_bottom).offset(-10);
        }];
    }
    
    [self layoutIfNeeded];
    return userListView.frame.origin.y + userListView.frame.size.height;
}

@end
