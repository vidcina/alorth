//
//  ALFindGoodsToSaleApplyViewController.h
//  alorth
//
//  Created by w91379137 on 2015/10/19.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicSubViewController.h"
#import "ALSellingCommissionCell.h"

@interface ALFindGoodsToSaleApplyViewController : ALBasicSubViewController
<UITableViewDataSource, UITableViewDelegate, ALSellingCommissionCellDelegate>
{
    
}

@property(nonatomic, strong) IBOutlet UITableView *mainTableView;
@property(nonatomic, strong) NSDictionary *productInfoDict;
@property(nonatomic, strong) NSDictionary *userInfoDict;

@end
