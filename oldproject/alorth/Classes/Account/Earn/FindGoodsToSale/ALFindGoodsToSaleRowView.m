//
//  ALFindGoodsToSaleRowView.m
//  alorth
//
//  Created by w91379137 on 2015/10/19.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALFindGoodsToSaleRowView.h"
#import "ALFindGoodsToSaleCell.h"

@implementation ALFindGoodsToSaleRowView

#pragma mark - set
-(void)setRowData:(NSDictionary *)rowData
{
    _rowData = rowData;
    
    self.sellerName.text =
    [ALFormatter preferredDisplayNameFirst:rowData[@"FirstName"]
                                      Last:rowData[@"LastName"]
                                    UserID:rowData[@"UserID"]];
    
    for (UIView *view in self.sellerRate.subviews) {
        [view removeFromSuperview];
    }
    
    float rate = [rowData[@"AverageRate"] floatValue];
    [self.sellerRate rankStar:5 get:rate];
    
    self.location.text = rowData[@"Country"];
    self.priceLabel.text = @"";
    //[NSString stringWithFormat:@"%@ %@",rowData[@"Price"],rowData[@"Currency"]];
}

-(IBAction)didSelectProduct:(UIButton *)sender
{
    [self.delegate.selectGroup select:self];
}

#pragma mark - ALSelectGroupble
- (NSString *)uuIDInGroup:(ALSelectGroup *)group
{
    NSString *productID = self.delegate.infoDict.safe[@"ProductID"];
    NSString *userID = self.rowData.safe[@"UserID"];
    
    return [NSString stringWithFormat:@"%@_%@",productID,userID];
}

- (void)select:(BOOL)isSelect Group:(ALSelectGroup *)group
{
    if (isSelect) {
        self.clickButton.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    }
    else {
        self.clickButton.backgroundColor = [UIColor clearColor];
    }
}

@end
