//
//  ALSelectGroup.h
//  alorth
//
//  Created by w91379137 on 2015/12/3.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ALSelectGroup;
@protocol ALSelectGroupble

@required

- (NSString *)uuIDInGroup:(ALSelectGroup *)group;
- (void)select:(BOOL)isSelect Group:(ALSelectGroup *)group;

@end

@interface ALSelectGroup : NSObject

@property (nonatomic, strong) NSDictionary *infoDict;                       //辨識
@property (nonatomic, strong) NSMutableDictionary *weakSelectTabel;         //不會 retain 的 Dictionary
@property (nonatomic, strong) NSMutableArray *selectKeysArray;              //選中的key
//@property (nonatomic, retain) IBOutlet NSObject<ALSelectGroupble> *objects; //從xib載入 希望button 也可以從 xib檔 加入

- (BOOL)isAcceptAdd:(NSObject<ALSelectGroupble> *)object;   //加入並驅動狀態
- (BOOL)select:(NSObject<ALSelectGroupble> *)object;        //請求選起這物品
- (id)didSelectobjectAt:(NSInteger)index;                   //返回存在的選擇物件

@end
