//
//  ALFindGoodsToSaleApplyViewController.m
//  alorth
//
//  Created by w91379137 on 2015/10/19.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALFindGoodsToSaleApplyViewController.h"


@interface ALFindGoodsToSaleApplyViewController ()
{
    ALSellingCommissionCell *demoCell;
}

@end

@implementation ALFindGoodsToSaleApplyViewController

#pragma mark - VC Life
-(void)viewWillAppearFirstTime:(BOOL)animated
{
    [super viewWillAppearFirstTime:animated];
    [headerView.backButton addTarget:self
                              action:@selector(back:)
                    forControlEvents:UIControlEventTouchUpInside];
    
    [self alTableViewSetting:self.mainTableView];
}

#pragma mark - UITableViewDataSource Row
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.productInfoDict == nil ? 0 : 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (demoCell == nil) {
        demoCell = [ALSellingCommissionCell cellFromXib];
        demoCell.frame = CGRectMake(0, 0, tableView.contentSize.width, 0);
        [demoCell layoutIfNeeded];
    }
    return [demoCell performWithInfoDict:nil isHidden:NO];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self tableView:tableView heightForRowAtIndexPath:indexPath] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ALSellingCommissionCell *cell = [ALSellingCommissionCell cellFromXib];
    cell.delegate = self;
    
    NSMutableDictionary *copyToAdd = [NSMutableDictionary dictionaryWithDictionary:self.productInfoDict];
    [copyToAdd setObjectEmptyStringIfNil:self.userInfoDict[@"FirstName"] forKey:@"ProductOwnerFirstName"];
    [copyToAdd setObjectEmptyStringIfNil:self.userInfoDict[@"LastName"] forKey:@"ProductOwnerLastName"];
    [copyToAdd setObjectEmptyStringIfNil:self.userInfoDict[@"UserID"] forKey:@"ProductOwner"];
    [copyToAdd setObjectEmptyStringIfNil:self.userInfoDict[@"AverageRate"] forKey:@"ProductOwnerRating"];
    
    [cell performWithInfoDict:copyToAdd];
    return cell;
}

#pragma mark - ALSellingConnissionCellDelegate
- (void)removeActionCell:(ALSellingCommissionCell *)cell
{
    [self back:nil];
}

- (void)applyActionCell:(ALSellingCommissionCell *)cell
{
    [ALNetWorkAPI creatSellingCommissionA044ProductID:self.productInfoDict[@"ProductID"]
                                          ProductName:self.productInfoDict[@"ProductTitle"]
                                         ProductOwner:self.userInfoDict[@"UserID"]
                                       EarnPercentage:[NSString stringWithFormat:@"%d",cell.percent]
                                     CommissionStatus:@"1"
                                      CompletionBlock:^(NSDictionary *responseObject) {
                                          
                                          if ([ALNetWorkAPI checkSerialNumber:@"A044"
                                                               ResponseObject:responseObject]) {
                                              [self.navigationController popViewControllerAnimated:YES];
                                          }
                                          else {
                                              YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
                                          }
                                      }];
}

- (void)chatActionCell:(ALSellingCommissionCell *)cell
{
    NSString *otherID = cell.infoDict[@"ProductOwner"];
    [self chatWithOtherId:otherID];
}

@end
