//
//  ALFindGoodsToSaleViewController.h
//  alorth
//
//  Created by w91379137 on 2015/10/18.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicSubViewController.h"
#import "ALFindGoodsToSaleCell.h"

#import "ALGoodsSearchViewController.h" //取得種類

@interface ALFindGoodsToSaleViewController : ALBasicSubViewController
<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>
{
    IBOutlet UISearchBar *topSearchBar;
    
    //清單
    NSMutableArray *objectListArray;
    BOOL willReplaceWithNew;                    //是否清空之前資料
    ALTableViewControl *currentTableViewControl;
}

@property (nonatomic) ALGoodsSearchViewControllerStatus listStatus;

@property(nonatomic, strong) IBOutlet UITableView *mainTableView;

@end
