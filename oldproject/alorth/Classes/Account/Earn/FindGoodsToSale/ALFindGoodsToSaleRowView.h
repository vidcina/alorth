//
//  ALFindGoodsToSaleRowView.h
//  alorth
//
//  Created by w91379137 on 2015/10/19.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "PDSIBDesignABLEView.h"
#import "ALSelectGroup.h"

@class ALFindGoodsToSaleCell;

@interface ALFindGoodsToSaleRowView : PDSIBDesignABLEView
<ALSelectGroupble>

@property (nonatomic, weak) ALFindGoodsToSaleCell *delegate;
@property (nonatomic, strong) NSDictionary *rowData;

@property (nonatomic, strong) IBOutlet UILabel *sellerName;
@property (nonatomic, strong) IBOutlet UIView *sellerRate;

@property (nonatomic, strong) IBOutlet UILabel *location;
@property (nonatomic, strong) IBOutlet UILabel *priceLabel;

@property (nonatomic, strong) IBOutlet UIButton *clickButton;

@end
