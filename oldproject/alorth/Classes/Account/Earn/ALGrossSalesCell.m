//
//  ALGrossSalesCell.m
//  alorth
//
//  Created by w91379137 on 2015/12/1.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALGrossSalesCell.h"

@implementation ALGrossSalesCell

-(void)cellFromXibSetting
{
    self.leftRateView.backgroundColor = [UIColor whiteColor];
    self.rightRateView.backgroundColor = [UIColor whiteColor];
}

- (CGFloat)performWithInfoDict:(NSDictionary *)infoDict
{
    self.infoDict = infoDict;
    
    self.leftNameLabel.text = [ALFormatter preferredDisplayNameFirst:infoDict[@"ProductOwnerFirstName"]
                                                                Last:infoDict[@"ProductOwnerLastName"]
                                                              UserID:infoDict[@"ProductOwner"]];
    [self.leftRateView rankStar:5 get:[infoDict.safe[@"ProductOwnerRating"] floatValue]];
    
    self.rightNameLabel.text = [ALFormatter preferredDisplayNameFirst:infoDict[@"SellerFirstName"]
                                                                 Last:infoDict[@"SellerLastName"]
                                                               UserID:infoDict[@"SellerID"]];
    [self.rightRateView rankStar:5 get:[infoDict.safe[@"SellerRating"] floatValue]];
    
    return self.frame.size.height;
}

@end