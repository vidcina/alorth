//
//  ALSellingCommissionViewController.m
//  alorth
//
//  Created by w91379137 on 2015/10/18.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALSellingCommissionViewController.h"
#import "ALSellingCommissionViewController+PullMore.h"

@interface ALSellingCommissionViewController ()
{
    ALSellingCommissionCell *demoCell;
}

@end

@implementation ALSellingCommissionViewController

#pragma mark - VC Life
- (instancetype)init
{
    self = [super init];
    if (self) {
        objectListArray = [NSMutableArray array];
        
        currentTableViewControl = [[ALTableViewControl alloc] init];
        currentTableViewControl.delegate = self;
        currentTableViewControl.apiDataArray = objectListArray;
        
        clickPath = [NSIndexPath indexPathForItem:0 inSection:0];
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (objectListArray.count == 0) {
        [self checkNewData:currentTableViewControl];
    }
}

-(void)viewWillAppearFirstTime:(BOOL)animated
{
    [super viewWillAppearFirstTime:animated];
    [headerView.backButton addTarget:self
                              action:@selector(back:)
                    forControlEvents:UIControlEventTouchUpInside];
    
    [self alTableViewSetting:self.mainTableView];
}

#pragma mark - UITableViewDataSource Row
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return objectListArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (demoCell == nil) {
        demoCell = [ALSellingCommissionCell cellFromXib];
        demoCell.frame = CGRectMake(0, 0, tableView.contentSize.width, 0);
        [demoCell layoutIfNeeded];
    }
    return [demoCell performWithInfoDict:nil
                                isHidden:(clickPath.row != indexPath.row) || (clickPath.section != indexPath.section)];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self tableView:tableView heightForRowAtIndexPath:indexPath] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ALSellingCommissionCell *cell = [ALSellingCommissionCell cellFromXib];
    cell.delegate = self;
    [cell performWithInfoDict:objectListArray[indexPath.row]];
    return cell;
}

#pragma mark - ALSellingConnissionCellDelegate
- (void)didPressedCell:(ALSellingCommissionCell *)cell
{
    UITableView *tableView = self.mainTableView;
    NSIndexPath *path = [tableView indexPathForCell:cell];
    
    if (path == nil) {
        return;
    }
    
    NSMutableArray *reload = [NSMutableArray array];
    [reload addObject:path];
    
    if (clickPath &&
        (path.section == clickPath.section) &&
        (path.row == clickPath.row)) {
        
        //相同
    }
    else {
        if (clickPath && [tableView cellForRowAtIndexPath:clickPath]) {
            [reload addObject:[clickPath copy]];
        }
        clickPath = path;
        
        [tableView reloadRowsAtIndexPaths:reload
                         withRowAnimation:UITableViewRowAnimationNone];
    }
}

- (void)removeActionCell:(ALSellingCommissionCell *)cell
{
    NSDictionary *infoDict = cell.infoDict;
    [ALNetWorkAPI creatSellingCommissionA044ProductID:infoDict[@"ProductID"]
                                          ProductName:infoDict[@"ProductTitle"]
                                         ProductOwner:infoDict[@"ProductOwner"]
                                       EarnPercentage:infoDict[@"EarnPercentage"]
                                     CommissionStatus:@"0"
                                      CompletionBlock:^(NSDictionary *responseObject) {
                                          
                                          if ([ALNetWorkAPI checkSerialNumber:@"A044"
                                                               ResponseObject:responseObject]) {
                                              
                                              [self updateObjectListArray:infoDict
                                                                    Renew:responseObject[@"resBody"]];
                                          }
                                          else {
                                              YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
                                          }
                                      }];
}

- (void)applyActionCell:(ALSellingCommissionCell *)cell
{
    NSDictionary *infoDict = cell.infoDict;
    [ALNetWorkAPI creatSellingCommissionA044ProductID:infoDict[@"ProductID"]
                                          ProductName:infoDict[@"ProductTitle"]
                                         ProductOwner:infoDict[@"ProductOwner"]
                                       EarnPercentage:[NSString stringWithFormat:@"%d",cell.percent]
                                     CommissionStatus:@"1"
                                      CompletionBlock:^(NSDictionary *responseObject) {
                                          
                                          if ([ALNetWorkAPI checkSerialNumber:@"A044"
                                                               ResponseObject:responseObject]) {
                                              
                                              [self updateObjectListArray:infoDict
                                                                    Renew:responseObject[@"resBody"]];
                                          }
                                          else {
                                              YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
                                          }
                                      }];
}

- (void)updateObjectListArray:(NSDictionary *)infoDict Renew:(NSDictionary *)renewDict
{
    NSInteger index = [objectListArray indexOfObject:infoDict];
    
    if (index != NSNotFound) {
        
        //確認 是否移除
        if ([renewDict.safe[@"AgreeStatus"] intValue] == 0) {
            [objectListArray removeObject:infoDict];
        }
        else {
            //更新資料
            NSMutableDictionary *copy = [infoDict mutableCopy];
            NSDictionary *change = renewDict;
            for (NSString *key in change.allKeys) {
                if ([copy.allKeys containsObject:key]) {
                    copy[key] = change[key];
                }
            }
            [objectListArray replaceObjectAtIndex:index withObject:copy];
        }
    }
    else {
        NSLog(@"NSNotFound");
    }
    [self.mainTableView reloadData];
}

- (void)chatActionCell:(ALSellingCommissionCell *)cell
{
    NSString *otherID = cell.infoDict[@"ProductOwner"];
    [self chatWithOtherId:otherID];
}

@end
