//
//  ALSellingConnissionCell.h
//  alorth
//
//  Created by w91379137 on 2015/10/18.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALCommonTableViewCell.h"
@protocol ALSellingCommissionCellDelegate;

@interface ALSellingCommissionCell : ALCommonTableViewCell
<ALSelectTypeScrollViewDelegate>

@property (nonatomic, weak) NSObject<ALSellingCommissionCellDelegate> *delegate;
@property (nonatomic, strong) IBOutlet UIView *mainContainerView;
@property (nonatomic, strong) IBOutlet UIImageView *productImageView;
@property (nonatomic, strong) IBOutlet UILabel *productTitleLabel;

@property (nonatomic, strong) IBOutlet UILabel *sellerNameLabel;
@property (nonatomic, strong) IBOutlet UIView *sellerRateView;
@property (nonatomic, strong) IBOutlet UIImageView *statusImageView;
@property (nonatomic, strong) IBOutlet UILabel *statusLabel;

@property (nonatomic, strong) IBOutlet UIView *selectContainerView;
@property (nonatomic, strong) IBOutlet ALSelectTypeScrollView *percentSelectTypeScrollView;
@property (nonatomic) int percent;

@property (nonatomic, strong) IBOutlet UIView *buttonContainerView;
@property (nonatomic, strong) IBOutlet UIButton *agreeButton;

- (CGFloat)performWithInfoDict:(NSDictionary *)infoDict isHidden:(BOOL)isHidden;

@end

#pragma mark - ALSellingConnissionCellDelegate
@protocol ALSellingCommissionCellDelegate <NSObject>
@optional
- (void)didPressedCell:(ALSellingCommissionCell *)cell;
- (void)removeActionCell:(ALSellingCommissionCell *)cell;
- (void)applyActionCell:(ALSellingCommissionCell *)cell;
- (void)chatActionCell:(ALSellingCommissionCell *)cell;
@end