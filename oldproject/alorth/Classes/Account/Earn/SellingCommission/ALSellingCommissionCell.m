//
//  ALSellingConnissionCell.m
//  alorth
//
//  Created by w91379137 on 2015/10/18.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALSellingCommissionCell.h"

@interface ALSellingCommissionCell ()
{
    NSArray *applyButtonConstraints;
}

@end

@implementation ALSellingCommissionCell

- (void)cellFromXibSetting
{
    self.productTitleLabel.numberOfLines = 0;
    self.sellerRateView.backgroundColor = [UIColor clearColor];
}

#pragma mark - heightForRow
- (CGFloat)performWithInfoDict:(NSDictionary *)infoDict isHidden:(BOOL)isHidden
{
    if (isHidden) {
        return [self performWithInfoDict:infoDict];
    }
    else {
        [self performWithInfoDict:infoDict];
        return self.buttonContainerView.frame.origin.y + self.buttonContainerView.frame.size.height + 10;
    }
}

- (CGFloat)performWithInfoDict:(NSDictionary *)infoDict
{
    self.infoDict = infoDict;
    
    if (self.infoDict) {
        [self.productImageView imageFromURLString:infoDict[@"ProductImage"]];
        self.productTitleLabel.text = infoDict[@"ProductTitle"];
        
        self.sellerNameLabel.text =
        [ALFormatter preferredDisplayNameFirst:infoDict[@"ProductOwnerFirstName"]
                                          Last:infoDict[@"ProductOwnerLastName"]
                                        UserID:infoDict[@"ProductOwner"]];
        
        float rate = [infoDict[@"ProductOwnerRating"] floatValue];
        [self.sellerRateView rankStar:5 get:rate];
        
        
        if ([infoDict.safe[@"AgreeStatus"] intValue] == 1) {
            //等待中
            self.statusImageView.image = [UIImage imageNamed:@"RateClock"];
            self.statusLabel.localText = @"Waiting for agree";
        }
        else if ([infoDict.safe[@"AgreeStatus"] intValue] == 2){
            //同意
            self.statusImageView.image = [UIImage imageNamed:@"FinishClock"];
            self.statusLabel.localText = @"Agreed";
            [self adjustAgreeButton];
        }
        else {
            self.statusImageView.image = nil;
            self.statusLabel.localText = @"";
            
        }
        
        NSMutableArray *percents = [NSMutableArray array];
        for (NSInteger k = 0; k <= 20; k += 2) {
            [percents addObject:[NSString stringWithFormat:@"%ld",(long)k]];
        }
        
        self.percentSelectTypeScrollView.selectDelegate = self;
        self.percentSelectTypeScrollView.infoObjectArray = percents;
        
        if (self.infoDict[@"EarnPercentage"]) {
            //選到最初選擇
            int earnPercentage = [self.infoDict.safe[@"EarnPercentage"] intValue];
            [self.percentSelectTypeScrollView selectIndex:earnPercentage / 2];
            [self.percentSelectTypeScrollView scrollToSelect:NO];
        }
        else {
            [self.percentSelectTypeScrollView selectIndex:0];
        }
    }
    
    return self.selectContainerView.frame.origin.y + self.selectContainerView.frame.size.height;
}

#pragma mark - Constraint
- (void)adjustAgreeButton
{
    if (self.infoDict[@"EarnPercentage"]) {
        NSString *percentString =
        self.percentSelectTypeScrollView.infoObjectArray[self.percentSelectTypeScrollView.didSelectIndex];
        
        if ([self.infoDict.safe[@"EarnPercentage"] intValue] == [percentString intValue]) {
            applyButtonConstraints = [self.agreeButton mas_updateConstraints:^(MASConstraintMaker *make) {
                make.leading.equalTo(self.agreeButton.superview.mas_trailing);
            }];
        }
        else {
            for (MASConstraint *obj in applyButtonConstraints) {
                if([obj isKindOfClass:MASConstraint.class])
                {
                    [obj uninstall];
                }
            }
            applyButtonConstraints = nil;
        }
        
        [UIView animateWithDuration:0.1
                         animations:^{
                             [self.buttonContainerView layoutIfNeeded];
                         }];
    }
}

#pragma mark - ALSelectTypeScrollViewDelegate
-(UIView *)typeViewOfIndex:(NSInteger)index
                infoObject:(NSObject *)infoObject
      selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    if ([infoObject isKindOfClass:[NSString class]]) {
        
        NSString *title = (NSString *)infoObject;
        title = [NSString stringWithFormat:@"%@ %%",title];
        
        UIButton *aButton =
        [ALSelectTypeScrollView alSelectButton:title];
        
        [aButton addTarget:selectTypeScrollView
                    action:@selector(selectView:)
          forControlEvents:UIControlEventTouchUpInside];
        
        [aButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@(70));
        }];
        return aButton;
    }
    else {
        return [[UIView alloc] initWithFrame:CGRectZero];
    }
}

-(void)didSelectIndex:(NSInteger)index
           infoObject:(NSObject *)infoObject
 selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    NSString *percentString = (NSString *)infoObject;
    self.percent = [percentString intValue];
    [self clickCell:nil]; //展開此格按鈕
    [self adjustAgreeButton];
}

-(void)adjustSelectView:(UIView *)view
             infoObject:(NSObject *)infoObject
               selected:(BOOL)selected
   selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    [ALSelectTypeScrollView alSelectButton:(UIButton *)view Select:selected];
}

#pragma mark - IBAction
-(IBAction)clickCell:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didPressedCell:)]) {
        [self.delegate didPressedCell:self];
    }
}

-(IBAction)removeAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(removeActionCell:)]) {
        [self.delegate removeActionCell:self];
    }
}

-(IBAction)applyAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(applyActionCell:)]) {
        [self.delegate applyActionCell:self];
    }
}

-(IBAction)chatAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(chatActionCell:)]) {
        [self.delegate chatActionCell:self];
    }
}
@end
