//
//  ALEarnListViewController.m
//  alorth
//
//  Created by w91379137 on 2015/10/13.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALEarnListViewController.h"
#import "ALEarnListViewController+PullMore.h"
#import "ALGrossSalesCell.h"

#import "ALSellingCommissionViewController.h"
#import "ALFindGoodsToSaleViewController.h"
#import "ALAgreeCommissionViewController.h"
#import "ALGrossSalesListController.h"

typedef NS_ENUM(NSUInteger, ALEarnListViewControllerSection) {
    SellingCommissionSection,
    FindGoodsToSaleSection,
    AgreeCommissionSection,
    GrossSalesSection
};

@interface ALEarnListViewController ()
{
    ALGrossSalesCell *demoCell;
}

@end

@implementation ALEarnListViewController

#pragma mark - VC Life
- (instancetype)init
{
    self = [super init];
    if (self) {
        objectListArray = [NSMutableArray array];
        
        currentTableViewControl = [[ALTableViewControl alloc] init];
        currentTableViewControl.delegate = self;
        currentTableViewControl.apiDataArray = objectListArray;
    }
    return self;
}

-(void)viewWillAppearFirstTime:(BOOL)animated
{
    [super viewWillAppearFirstTime:animated];
    [headerView.backButton addTarget:self
                              action:@selector(back:)
                    forControlEvents:UIControlEventTouchUpInside];
    
    [self alTableViewSetting:self.mainTableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (objectListArray.count == 0) {
        [self checkNewData:currentTableViewControl];
    }
}

#pragma mark - UITableViewDataSource Section
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

#pragma mark - UITableViewDataSource Row
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case SellingCommissionSection:
        case FindGoodsToSaleSection:
        case AgreeCommissionSection:
            return 1;
            break;
        case GrossSalesSection:
            return objectListArray.count;
            break;
        default:
            return 0;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case SellingCommissionSection: return self.sellingCommissionCell.frame.size.height; break;
        case FindGoodsToSaleSection: return self.findGoodsToSaleCell.frame.size.height; break;
        case AgreeCommissionSection: return self.agreeCommissionCell.frame.size.height; break;
        case GrossSalesSection:
            if (!demoCell) {
                demoCell = [ALGrossSalesCell cellFromXib];
            }
            return demoCell.frame.size.height;
            break;
        default:
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case SellingCommissionSection:
            return self.sellingCommissionCell;
            break;
        case FindGoodsToSaleSection:
            return self.findGoodsToSaleCell;
            break;
        case AgreeCommissionSection:
            return self.agreeCommissionCell;
            break;
        case GrossSalesSection:
        {
            ALGrossSalesCell *cell = [ALGrossSalesCell cellFromXib];
            [cell performWithInfoDict:objectListArray[indexPath.row]];
            return cell;
        }
            break;
        default:
        {
            return [self defaultUITableViewCell];
        }
            break;
    }
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController *nextVc = nil;
    
    switch (indexPath.section) {
        case SellingCommissionSection:
            nextVc = [[ALSellingCommissionViewController alloc] init];
            break;
        case FindGoodsToSaleSection:
            nextVc = [[ALFindGoodsToSaleViewController alloc] init];
            break;
        case AgreeCommissionSection:
            nextVc = [[ALAgreeCommissionViewController alloc] init];
            break;
        case GrossSalesSection:
        {
            ALGrossSalesCell *cell = (ALGrossSalesCell *)[tableView cellForRowAtIndexPath:indexPath];
            
            ALGrossSalesListController *nextVcX = [[ALGrossSalesListController alloc] init];
            nextVcX.infoDict = cell.infoDict;
            nextVc = nextVcX;
        }
            break;
        default:

            break;
    }
    
    if (nextVc) {
        [self.navigationController pushViewController:nextVc animated:YES];
    }
}

@end
