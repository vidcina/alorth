//
//  ALAccountTableViewCell.m
//  alorth
//
//  Created by w91379137 on 2015/6/10.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALAccountTableViewCell.h"

@implementation ALAccountTableViewCell

#pragma mark - Cell Life
+(ALAccountTableViewCell *)cell
{
    ALAccountTableViewCell *cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class])
                                                                  owner:nil
                                                                options:nil] lastObject];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.settingTitleLabel.textColor = COMMON_GRAY3_COLOR;
    return cell;
}

@end
