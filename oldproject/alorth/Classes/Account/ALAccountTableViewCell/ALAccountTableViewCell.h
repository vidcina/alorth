//
//  ALAccountTableViewCell.h
//  alorth
//
//  Created by w91379137 on 2015/6/10.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ALAccountDefinition.h"

#define AccountTableViewCellHeight 50
@interface ALAccountTableViewCell : UITableViewCell
{
    
}

+(ALAccountTableViewCell *)cell;

@property (strong, nonatomic) IBOutlet UIImageView *iconImageView;
@property (strong, nonatomic) IBOutlet UILabel *settingTitleLabel;

@end
