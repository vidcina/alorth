//
//  ALProfileDataViewController.h
//  alorth
//
//  Created by John Hsu on 2015/6/30.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicViewController.h"

@interface ALProfileDataViewController : ALBasicSubViewController
<UITableViewDataSource, UITableViewDelegate,
UIAlertViewDelegate>

{
    NSArray *dataKeyArray;
    NSMutableDictionary *apiDataDict;
    
    IBOutlet UITableViewCell *avatarCell;
    IBOutlet UIImageView *userAvatarImageView;
    IBOutlet UILabel *avatarTitleLabel;
    
    NSString *editingApiKey;
    NSString *oldCountryCode;
}

@property(nonatomic) BOOL shouldReload;

@property(nonatomic,strong) IBOutlet UITableView *tableView;

@end
