//
//  ALAccountViewController.m
//  alorth
//
//  Created by w91379137 on 2015/6/2.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>

#import "ALAccountViewController.h"
#import "ALAccountTableViewCell.h"

#import "ALPersonFollowingListViewController.h"
#import "ALPersonFollowersListViewController.h"

#import "ALEarnListViewController.h"
#import "ALMyStockListViewController.h"
#import "ALRecordListViewController.h"
#import "ALSoldOrderListViewController.h"
#import "ALBoughtOrderViewController.h"
#import "ALProfileDataViewController.h"
#import "ALSettingViewController.h"
#import "ALAboutListViewController.h"

#import "ALStripeViewController.h"

//#import "PLViewController.h"
//#import "ALStripeEntryViewController.h"
#import "ALStripeConnectViewController.h"

#define ALAccountViewControllerImageUniqueCode @"ALAccountViewController.Image.Unique.Code"

@interface ALAccountViewController ()

@end

@implementation ALAccountViewController

#pragma mark - VC Life
-(void)viewDidLoad
{
    [super viewDidLoad];
    userAvatarImageView.layer.masksToBounds = YES;
    userAvatarImageView.layer.cornerRadius = (userAvatarImageView.frame.size.height / 2);
    [userAvatarImageView.layer setBorderColor:[UIColor whiteColor].CGColor];
    [userAvatarImageView.layer setBorderWidth:2];

    settingCellInfoArray = [NSMutableArray array];
    {
        NSDictionary *page =
        @{accountIconKey:@"PersonalPage",
          accountTitleKey:@"Personal Page",
          accountActionKey:^{
              UIViewController *vc = nil;
              if (UIInterfaceVersion == 1) {
                  vc = [[ALPersonDetailViewController alloc] init];
              }
              else {
                  vc = [[ALPersonIntroductionViewController alloc] init];
              }
              [self.navigationController pushViewController:vc animated:YES];
          }};
        [settingCellInfoArray addObject:page];
    }
    
    {
        NSDictionary *page =
        @{accountIconKey:@"Product_Box#acc",
          accountTitleKey:@"Bought",
          accountActionKey:^{
              ALBoughtOrderViewController *vc = [[ALBoughtOrderViewController alloc] init];
              [self.navigationController pushViewController:vc animated:YES];
          }};
        [settingCellInfoArray addObject:page];
    }
    
    //#warning v3 Shopping car
    //#warning v3 Recent view
    
    {
        NSDictionary *page =
        @{accountIconKey:@"Sold",
          accountTitleKey:@"Sold",
          accountActionKey:^{
              ALSoldOrderListViewController *vc = [[ALSoldOrderListViewController alloc] init];
              [self.navigationController pushViewController:vc animated:YES];
          }};
        [settingCellInfoArray addObject:page];
    }
    
    {
        NSDictionary *page =
        @{accountIconKey:@"Listing",
          accountTitleKey:@"Listing",
          accountActionKey:^{
              ALMyStockListViewController *vc = [[ALMyStockListViewController alloc] init];
              [self.navigationController pushViewController:vc animated:YES];
          }};
        [settingCellInfoArray addObject:page];
    }
    
    if (UIInterfaceVersion >= 2) {
        NSDictionary *page =
        @{accountIconKey:@"VODicon#acc",
          accountTitleKey:@"Broadcast list",
          accountActionKey:^{
              ALRecordListViewController *vc = [[ALRecordListViewController alloc] init];
              [self.navigationController pushViewController:vc animated:YES];
          }};
        [settingCellInfoArray addObject:page];
    }
    
    /*
    if (UIInterfaceVersion == 2) {
        NSDictionary *page =
        @{accountIconKey:@"ProductStatistics#acc",
          accountTitleKey:@"Partnership",
          accountActionKey:^{
              
              NSArray *target =
              @[@(QuestionStripe)];
              
              ALStripeViewController *stripeViewController = [[ALStripeViewController alloc] init];
              stripeViewController.questionTypeArray = target;
              stripeViewController.mainTitleString = ALStripeMainTitleString;
              stripeViewController.subTitleString = ALStripeSubTitleString;
              
              [[PDSEnvironmentViewController sharedInstance] submitViewController:stripeViewController
                                                                  CompletionBlock:^{
                                                                      
                                                                      if ([ALStripeViewController checkQuestionTypeArray:target]) {
                                                                          NSLog(@"Earn 檢查通過");
                                                                          ALEarnListViewController *vc = [[ALEarnListViewController alloc] init];
                                                                          [self.navigationController pushViewController:vc animated:YES];
                                                                      }
                                                                      else {
                                                                          NSLog(@"Earn 沒有通過");
                                                                      }
                                                                  }];
          }};
        [settingCellInfoArray addObject:page];
    }
     */
    
    {
        NSDictionary *page =
        @{accountIconKey:@"Profile",
          accountTitleKey:@"Profile",
          accountActionKey:^{
              ALProfileDataViewController *vc = [[ALProfileDataViewController alloc] init];
              [self.navigationController pushViewController:vc animated:YES];
          }};
        [settingCellInfoArray addObject:page];
    }

    {
        NSDictionary *page =
        @{accountIconKey:@"Settings",
          accountTitleKey:@"Settings",
          accountActionKey:^{
              ALSettingViewController *vc = [[ALSettingViewController alloc] init];
              [self.navigationController pushViewController:vc animated:YES];
          }};
        [settingCellInfoArray addObject:page];
    }
    
    /*
    if (UIInterfaceVersion == 2) {
        NSDictionary *page =
        @{accountIconKey:@"icon_info#acc",
          accountTitleKey:@"About",
          accountActionKey:^{
              ALAboutListViewController *vc = [[ALAboutListViewController alloc] init];
              [self.navigationController pushViewController:vc animated:YES];
          }};
        [settingCellInfoArray addObject:page];
    }
     */
    
    /*
    if (UIInterfaceVersion == 2) {
        NSDictionary *page =
        @{accountIconKey:@"Product_Rate#",
          accountTitleKey:@"Stripe 收款帳號綁定",
          accountActionKey:^{
              [self addMBProgressHUDWithKey:@"stripeConnectURLA071CompletionBlock"];
              [ALNetWorkAPI stripeConnectURLA071CompletionBlock:^(NSDictionary *responseObject) {
                  [self removeMBProgressHUDWithKey:@"stripeConnectURLA071CompletionBlock"];
                  
                  NSString *connectURLString = responseObject[@"resBody"][@"ConnectUrl"];
                  if ([connectURLString length] > 0) {
                      ALStripeConnectViewController *vc = [[ALStripeConnectViewController alloc] init];
                      vc.urlString = connectURLString;
                      vc.titleString = @"";
                      [self.navigationController pushViewController:vc animated:YES];
                  }
                  else {
                      YKSimpleAlert(@"無法綁定，請稍後再試");
                  }
              }];
          }};
        [settingCellInfoArray addObject:page];
    }
     */
    
    /*
    if (UIInterfaceVersion == 2) {
        NSDictionary *page =
        @{accountIconKey:@"Product_Rate#",
          accountTitleKey:@"付款信用卡綁定",
          accountActionKey:^{
              ALStripeEntryViewController *vc = [[ALStripeEntryViewController alloc] init];
              [self.navigationController pushViewController:vc animated:YES];
          }};
        [settingCellInfoArray addObject:page];
    }
     */
    
    /*
    if (UIInterfaceVersion == 2) {
        NSDictionary *page =
        @{accountIconKey:@"Product_Rate#",
          accountTitleKey:@"Broadcast 權限測試",
          accountActionKey:^{
              
              NSArray *target =
              @[@(QuestionCamera),@(QuestionMircrophone),@(QuestionLocation),@(QuestionStripe)];
              
              ALStripeViewController *stripeViewController = [[ALStripeViewController alloc] init];
              stripeViewController.questionTypeArray = target;
              stripeViewController.mainTitleString = ALBroadcastMainTitleString;
              stripeViewController.subTitleString = ALBroadcastSubTitleString;
              
              [[PDSEnvironmentViewController sharedInstance] submitViewController:stripeViewController
                                                                  CompletionBlock:^{
                                                                      
                                                                      if ([ALStripeViewController checkQuestionTypeArray:target]) {
                                                                          NSLog(@"Broadcast 權限測試 檢查通過");
                                                                      }
                                                                      else {
                                                                          NSLog(@"Broadcast 權限測試 沒有通過");
                                                                      }
                                                                  }];
          }};
        [settingCellInfoArray addObject:page];
    }
     */
    
//    [settingCellInfoArray
//     addObject:@{accountIconKey:@"Streaming Up Test(PL)",
//                 accountTitleKey:@"Streaming Up Test(PL)",
//                 accountActionKey:^{
//        PLViewController *vc = [[PLViewController alloc] init];
//        [self.navigationController pushViewController:vc animated:YES];
//    }
//                 }];
//    [settingCellInfoArray
//     addObject:@{accountIconKey:@"Streaming Play Test(JW)",
//                 accountTitleKey:@"Streaming Play Test(JW)",
//                 accountActionKey:^{
//        ALStreamingViewController *vc = [[ALStreamingViewController alloc] init];
//        [self.navigationController pushViewController:vc animated:YES];
//    }
//                 }];
//    
//    [settingCellInfoArray
//     addObject:@{accountIconKey:@"Streaming Play Test(MP)",
//                 accountTitleKey:@"Streaming Play Test(MP)",
//                 accountActionKey:^{
//        MPMoviePlayerViewController *mpvc = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:@"http://52.27.44.52:1935/live/myStream/playlist.m3u8"]];
//        [self.view.window.rootViewController presentMoviePlayerViewControllerAnimated:mpvc];
//    }
//                 }];
//    [settingCellInfoArray
//     addObject:@{accountIconKey:@"Developer Info",
//                 accountTitleKey:@"Developer Info",
//                 accountActionKey:^{
//        [[PDSEnvironmentViewController sharedEnvironmentManager] addLogManager];
//    }
//                 }];
}

- (void)viewWillAppearFirstTime:(BOOL)animated
{
    [super viewWillAppearFirstTime:animated];
    
    followView.layer.borderColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5].CGColor;
    followView.layer.borderWidth = 1;
    followView.layer.cornerRadius = 4;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    userNameLabel.text = [ALAppDelegate sharedAppDelegate].userNameString;
    [userAvatarImageView imageFromURLString:[[ALAppDelegate sharedAppDelegate] userAvatarURLString]];
}

#pragma mark - UITableView Datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return settingCellInfoArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 44;
    }
    return AccountTableViewCellHeight;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ALAccountTableViewCell *cell = [ALAccountTableViewCell cell];
    
    NSDictionary *infoDict = settingCellInfoArray[indexPath.row];
    
    {//圖片
        NSString *imageName = infoDict[accountIconKey];
        
        if ([imageName containsString:@"#"]) {
            
            UIImage *cacheImage = [ALImagekeeper tryImage:[self keyStringOfImage:imageName]];
            
            if (!cacheImage) {
                NSArray *array = [imageName componentsSeparatedByString:@"#"];
                NSString *newImageName = array.firstObject;
                UIImage *icon = [UIImage imageNamed:newImageName];
                
                if (icon) {
                    icon = [UIImage reSizeImage:icon maxLength:65.0f];
                    icon = [UIImage changeColor:icon color:[UIColor whiteColor]];
                }
                
                UIImage *clearBG =
                [UIImage makePureColorImage:CGSizeMake(128, 128) Color:[UIColor clearColor]];
                
                {
                    float length = 108.0f;
                    UIImage *grapCircle =
                    [UIImage makePureColorImage:CGSizeMake(length, length) Color:COMMON_GRAY_Circle_COLOR];
                    grapCircle = [grapCircle createRoundedRadius:length / 2];
                    
                    clearBG = [clearBG addImageCenter:grapCircle];
                    clearBG = [clearBG addImageCenter:icon];
                }
                
                cacheImage = clearBG;
                [ALImagekeeper cacheImage:[self keyStringOfImage:imageName] image:cacheImage];
            }
            
            cell.iconImageView.image = cacheImage;
        }
        else {
            cell.iconImageView.image = [UIImage imageNamed:imageName];
        }
    }
    
    cell.settingTitleLabel.localText = infoDict[accountTitleKey];
    
    if (indexPath.row == 0) {
        cell.backgroundColor = COMMON_GRAY1_COLOR;
    }
    
    return cell;
}

- (NSString *)keyStringOfImage:(NSString *)imageName
{
    return [NSString stringWithFormat:@"%@%@",ALAccountViewControllerImageUniqueCode,imageName];
}

#pragma mark - UITableView Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    NSDictionary *infoDict = settingCellInfoArray[indexPath.row];
    void(^actionBlock)(void)  = infoDict[accountActionKey];
    if (actionBlock != nil) {
        actionBlock();
    }
}

#pragma mark - IBAction
-(IBAction)showFollowerList:(id)sender
{
    ALPersonFollowersListViewController *next =
    [[ALPersonFollowersListViewController alloc] init];
    
    next.userID = [ALAppDelegate sharedAppDelegate].userID;
    [self.navigationController pushViewController:next animated:YES];
}

-(IBAction)showFollowingList:(id)sender
{
    ALPersonFollowingListViewController *next =
    [[ALPersonFollowingListViewController alloc] init];
    
    next.userID = [ALAppDelegate sharedAppDelegate].userID;
    [self.navigationController pushViewController:next animated:YES];
}

@end