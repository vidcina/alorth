//
//  ALUserRateListView.m
//  alorth
//
//  Created by w91379137 on 2015/10/20.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALUserRatingListView.h"

// cells
#import "ALOrderRateTableViewCell.h"
#import "ALMessageTableViewCell.h"

//reply
#import "ALReplyViewController.h"

@interface ALUserRatingListView()
{
    ALMessageTableViewCell *demoCell;
}
@end

@implementation ALUserRatingListView

#pragma mark - View Life
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        objectListArray = [NSMutableArray array];
        
        currentTableViewControl = [[ALTableViewControl alloc] init];
        currentTableViewControl.delegate = self;
        currentTableViewControl.apiDataArray = objectListArray;
        
        [self alTableViewSetting:self.mainTableView];
    }
    return self;
}

#pragma mark - UITableViewDataSource Row
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [objectListArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //NSDictionary *dataDict = listArray[section];
    //NSArray *replyList = dataDict[@"ReplyList"];
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dataDict = objectListArray[indexPath.section];
    if (indexPath.row == 0) {
        return 150;
    }
    
    //    ALRateTextTableViewCell *cell = [ALRateTextTableViewCell cell];
    //    NSDictionary *fontAttribute = @{NSFontAttributeName : cell.rateTextLabel.font};
    //    NSString *text = dataDict[@"Comment"];
    //    CGSize titleSize = [text boundingRectWithSize:CGSizeMake(tableView.frame.size.width - 100, MAXFLOAT)
    //                                          options:NSStringDrawingUsesLineFragmentOrigin
    //                                       attributes:fontAttribute
    //                                          context:nil].size;
    //    return 60 + titleSize.height;
    
    if (demoCell == nil) {
        demoCell = [ALMessageTableViewCell cellFromXib];
        demoCell.frame = CGRectMake(0, 0, tableView.contentSize.width, 0);
        [demoCell layoutIfNeeded];
    }
    return [ALMessageTableViewCell performCell:demoCell withCommentDict:dataDict];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dataDict = objectListArray[indexPath.section];
    
    if (indexPath.row == 0) {
        ALOrderRateTableViewCell *cell = [ALOrderRateTableViewCell cell];
        [ALOrderRateTableViewCell performCell:cell withDictionary:dataDict];
        return cell;
    }
    
    ALMessageTableViewCell *cell = [ALMessageTableViewCell cellFromXib];
    [ALMessageTableViewCell performCell:cell withCommentDict:dataDict];
    return cell;
}

#pragma mark - UITableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *dataDict = [objectListArray[indexPath.section] mutableCopy];
    
    //紀錄一下這是誰的列表
    dataDict[ALReplyCommentOwnerKey] = self.userID;
    
    ALReplyViewController *next = [[ALReplyViewController alloc] initWithInfoDict:dataDict];
    
    [self.delegate.navigationController pushViewController:next animated:YES];
}

@end

@implementation ALUserRatingListView (PullMore)
static NSString *reviewListA013 = @"reviewListA013";

#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.mainTableView) {
        [currentTableViewControl scrollViewDidScroll:scrollView];
    }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView == self.mainTableView) {
        [currentTableViewControl scrollViewDidEndDragging:scrollView willDecelerate:(BOOL)decelerate];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.mainTableView) {
        [currentTableViewControl scrollViewDidEndDecelerating:(UIScrollView *)scrollView];
    }
}

#pragma mark - ALTableViewControlDelegate
-(void)checkNewData
{
    [self checkNewData:currentTableViewControl];
}

-(void)checkNewData:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    willReplaceWithNew = YES;
    tableViewControl.lastDict[@"StartNum"] = @"0";
    
    ALLog(@"重新整理");
    [self addMBProgressHUDWithKey:reviewListA013];
    [self goodsListLoadApiSourceParameters:tableViewControl.lastDict
                               FinishBlock:^(BOOL isSuccess) {
                                   [self removeMBProgressHUDWithKey:reviewListA013];
                               }];
}

-(void)loadMore:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    
    ALLog(@"載入更多:目前第 %@ 頁 %lu 筆資料",
          tableViewControl.lastDict[@"StartNum"],
          (unsigned long)currentTableViewControl.apiDataArray.count);
    
    tableViewControl.lastDict[@"StartNum"] =
    [NSString stringWithFormat:@"%d",[tableViewControl.lastDict[@"StartNum"] intValue] + 1];
    
    [self addMBProgressHUDWithKey:reviewListA013];
    [self goodsListLoadApiSourceParameters:tableViewControl.lastDict
                               FinishBlock:^(BOOL isSuccess) {
                                   [self removeMBProgressHUDWithKey:reviewListA013];
                               }];
}

#pragma mark - API
-(void)goodsListLoadApiSourceParameters:(NSDictionary *)parameters
                            FinishBlock:(void (^)(BOOL isSuccess))finishBlockToRun
{
    [ALNetWorkAPI reviewListA013withUserID:self.userID
                                  startNum:parameters[@"StartNum"]
                           completionBlock:^(NSDictionary *responseObject) {
                               
                               BOOL isWiseData =
                               [self checkListData:responseObject];
                               
                               if (!isWiseData) ALLog(@"%@ 錯誤",reviewListA013);
                               finishBlockToRun(isWiseData);
                           }];
}

- (BOOL)checkListData:(NSDictionary *)responseObject
{
    BOOL isWiseData = YES;
    NSArray *addArray = nil;
    
    isWiseData =
    isWiseData && [ALNetWorkAPI checkSerialNumber:@"A013" ResponseObject:responseObject];
    
    isWiseData =
    isWiseData && responseObject.safe[@"resBody"][@"CommentList"];
    
    isWiseData =
    isWiseData && [responseObject[@"resBody"][@"CommentList"] isKindOfClass:[NSArray class]];
    
    if (isWiseData) {
        addArray = responseObject[@"resBody"][@"CommentList"];
        
        if (willReplaceWithNew) {
            [currentTableViewControl.apiDataArray removeAllObjects];
        }
        
        [currentTableViewControl.apiDataArray addObjectsFromArray:addArray];
        [self.mainTableView reloadData];
        if (currentTableViewControl.apiDataArray.count > 0) {
            [ALProductTableViewCell detectCenterCellToPlay:self.mainTableView];
        }
        
        if (willReplaceWithNew) {
            [self.mainTableView setContentOffset:CGPointZero animated:YES];
        }
        willReplaceWithNew = NO;
    }
    return isWiseData;
}

@end