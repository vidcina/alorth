//
//  ALUserProductListView.m
//  alorth
//
//  Created by w91379137 on 2015/10/20.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALUserProductListView.h"

@implementation ALUserProductListView

#pragma mark - View Life
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        objectListArray = [NSMutableArray array];
        
        currentTableViewControl = [[ALTableViewControl alloc] init];
        currentTableViewControl.delegate = self;
        currentTableViewControl.apiDataArray = objectListArray;
        
        [self alTableViewSetting:self.mainTableView];
    }
    return self;
}

- (void)removeFromSuperview
{
    [ALProductTableViewCell stopAllPlayCell:self.mainTableView];
    [super removeFromSuperview];
}

#pragma mark - UITableViewDataSource Row
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return objectListArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *infoDict = objectListArray[indexPath.row];
    return [ALProductTableViewCell settingCell:nil WithInfoDict:infoDict];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ALProductTableViewCell *cell = [ALProductTableViewCell cell];
    
    NSDictionary *infoDict = objectListArray[indexPath.row];
    [ALProductTableViewCell settingCell:cell WithInfoDict:infoDict];
    
    return cell;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([cell isKindOfClass:[ALProductTableViewCell class]]) {
        ALProductTableViewCell *cellx = (ALProductTableViewCell *)cell;

        NSString *event =
        [NSString stringWithFormat:@"%@ %@",NSStringFromClass([self.delegate class]), @"didSelectRowAtIndexPath"];
        
        [ALInspector logEventCategory:@"ProductDetail"                  // Event category (required)
                               Action:event
                                Label:NSStringFromClass([self class])   // Event label
                                Value:nil];                             // Event value
        
        ALProductDetailViewController *a = [[ALProductDetailViewController alloc] initWithInfoDict:cellx.infoDict];
        [self.delegate.navigationController pushViewController:a animated:YES];
    }
}

#pragma mark - Action
-(void)controlPlayCell:(BOOL)isPlay
{
    if (isPlay) {
        [ALProductTableViewCell detectCenterCellToPlay:self.mainTableView];
    }
    else {
        [ALProductTableViewCell stopAllPlayCell:self.mainTableView];
    }
}

@end

@implementation ALUserProductListView (PullMore)
static NSString *goodsA006 = @"goodsA006";

#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.mainTableView) {
        [currentTableViewControl scrollViewDidScroll:scrollView];
        [ALProductTableViewCell detectCenterCellToPlay:self.mainTableView];
    }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView == self.mainTableView) {
        [currentTableViewControl scrollViewDidEndDragging:scrollView willDecelerate:(BOOL)decelerate];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.mainTableView) {
        [currentTableViewControl scrollViewDidEndDecelerating:(UIScrollView *)scrollView];
    }
}

#pragma mark - ALTableViewControlDelegate
-(void)checkNewData
{
    [self checkNewData:currentTableViewControl];
}

-(void)checkNewData:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    willReplaceWithNew = YES;
    tableViewControl.lastDict[@"StartNum"] = @"0";
    tableViewControl.lastDict[@"UserID"] = self.userID;
    
    ALLog(@"重新整理");
    [self addMBProgressHUDWithKey:goodsA006];
    [self goodsListLoadApiSourceParameters:tableViewControl.lastDict
                               FinishBlock:^(BOOL isSuccess) {
                                   [self removeMBProgressHUDWithKey:goodsA006];
                               }];
}

-(void)loadMore:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    
    ALLog(@"載入更多:目前第 %@ 頁 %lu 筆資料",
          tableViewControl.lastDict[@"StartNum"],
          (unsigned long)currentTableViewControl.apiDataArray.count);
    
    tableViewControl.lastDict[@"StartNum"] =
    [NSString stringWithFormat:@"%d",[tableViewControl.lastDict[@"StartNum"] intValue] + 1];
    
    [self addMBProgressHUDWithKey:goodsA006];
    [self goodsListLoadApiSourceParameters:tableViewControl.lastDict
                               FinishBlock:^(BOOL isSuccess) {
                                   [self removeMBProgressHUDWithKey:goodsA006];
                               }];
}

#pragma mark - API
-(void)goodsListLoadApiSourceParameters:(NSDictionary *)parameters
                            FinishBlock:(void (^)(BOOL isSuccess))finishBlockToRun
{
    [ALProductTableViewCell stopAllPlayCell:self.mainTableView];
    [ALNetWorkAPI goodsA006Keyword:parameters[@"keyWord"]
                          Category:parameters[@"category"]
                             Order:parameters[@"Order"]
                          StartNum:parameters[@"StartNum"]
                            UserID:parameters[@"UserID"]
                   CompletionBlock:^(NSDictionary *responseObject) {
                       
                       BOOL isWiseData =
                       [self checkListData:responseObject];
                       
                       if (!isWiseData) ALLog(@"%@ 錯誤",goodsA006);
                       finishBlockToRun(isWiseData);
                   }];
}

- (BOOL)checkListData:(NSDictionary *)responseObject
{
    BOOL isWiseData = YES;
    NSArray *addArray = nil;
    
    isWiseData =
    isWiseData && [ALNetWorkAPI checkSerialNumber:@"A006" ResponseObject:responseObject];
    
    isWiseData =
    isWiseData && responseObject.safe[@"resBody"][@"GoodsList"];
    
    isWiseData =
    isWiseData && [responseObject[@"resBody"][@"GoodsList"] isKindOfClass:[NSArray class]];
    
    if (isWiseData) {
        addArray = responseObject[@"resBody"][@"GoodsList"];
        
        if (willReplaceWithNew) {
            [currentTableViewControl.apiDataArray removeAllObjects];
        }
        
        [currentTableViewControl.apiDataArray addObjectsFromArray:addArray];
        [self.mainTableView reloadData];
        if (currentTableViewControl.apiDataArray.count > 0) {
            [ALProductTableViewCell detectCenterCellToPlay:self.mainTableView];
        }
        
        if (willReplaceWithNew) {
            [self.mainTableView setContentOffset:CGPointZero animated:YES];
        }
        willReplaceWithNew = NO;
    }
    return isWiseData;
}

@end

