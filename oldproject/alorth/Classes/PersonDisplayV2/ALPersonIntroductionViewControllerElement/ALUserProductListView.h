//
//  ALUserProductListView.h
//  alorth
//
//  Created by w91379137 on 2015/10/20.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "PDSIBDesignABLEView.h"

@interface ALUserProductListView : PDSIBDesignABLEView
<UITableViewDataSource, UITableViewDelegate>
{
    //清單
    NSMutableArray *objectListArray;
    BOOL willReplaceWithNew;                    //是否清空之前資料
    ALTableViewControl *currentTableViewControl;
}

@property(nonatomic, weak) ALBasicViewController *delegate;
@property(nonatomic, strong) IBOutlet UITableView *mainTableView;

@property(nonatomic, strong) NSString *userID;
-(void)controlPlayCell:(BOOL)isPlay;

@end

@interface ALUserProductListView (PullMore)
<ALTableViewControlDelegate>

-(void)checkNewData;
-(void)checkNewData:(ALTableViewControl *)tableViewControl;
-(void)loadMore:(ALTableViewControl *)tableViewControl;

@end
