//
//  ALVodListView.m
//  alorth
//
//  Created by w91379137 on 2015/10/20.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALUserVodListView.h"
#import "ALStreamBigCell.h"

@interface ALUserVodListView()
{
    ALStreamBigCell *demoCell;
}

@end

@implementation ALUserVodListView

#pragma mark - View Life
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        objectListArray = [NSMutableArray array];
        
        currentTableViewControl = [[ALTableViewControl alloc] init];
        currentTableViewControl.delegate = self;
        currentTableViewControl.apiDataArray = objectListArray;
        
        [self alTableViewSetting:self.mainTableView];
    }
    return self;
}

#pragma mark - UITableViewDataSource Row
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return objectListArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (demoCell == nil) {
        demoCell = [ALStreamBigCell cellFromXib];
        [demoCell layoutAtWidth:tableView.frame.size.width];
    }
    return [demoCell performWithInfoDict:nil];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *infoDict = objectListArray[indexPath.row];
    
    ALStreamBigCell *cell = [ALStreamBigCell cellFromXib];
    [cell performWithInfoDict:infoDict];
    
    return cell;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if([cell isKindOfClass:[ALCommonTableViewCell class]]) {
        ALCommonTableViewCell *cellx = (ALCommonTableViewCell *)cell;
        [self.delegate playStreamVideo:cellx.infoDict];
    }
}

@end

@implementation ALUserVodListView (PullMore)
static NSString *personalStreamListA060 = @"personalStreamListA060";

#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.mainTableView) {
        [currentTableViewControl scrollViewDidScroll:scrollView];
    }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView == self.mainTableView) {
        [currentTableViewControl scrollViewDidEndDragging:scrollView willDecelerate:(BOOL)decelerate];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.mainTableView) {
        [currentTableViewControl scrollViewDidEndDecelerating:(UIScrollView *)scrollView];
    }
}

#pragma mark - ALTableViewControlDelegate
-(void)checkNewData
{
    [self checkNewData:currentTableViewControl];
}

-(void)checkNewData:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    willReplaceWithNew = YES;
    tableViewControl.lastDict[@"StartNum"] = @"0";
    
    ALLog(@"重新整理");
    [self addMBProgressHUDWithKey:personalStreamListA060];
    [self goodsListLoadApiSourceParameters:tableViewControl.lastDict
                               FinishBlock:^(BOOL isSuccess) {
                                   [self removeMBProgressHUDWithKey:personalStreamListA060];
                               }];
}

-(void)loadMore:(ALTableViewControl *)tableViewControl
{
    [self checkNewData:tableViewControl];
}

#pragma mark - API
-(void)goodsListLoadApiSourceParameters:(NSDictionary *)parameters
                            FinishBlock:(void (^)(BOOL isSuccess))finishBlockToRun
{
    [ALNetWorkAPI personalStreamListA060UserID:self.userID
                               CompletionBlock:^(NSDictionary *responseObject) {
                                   
                                   BOOL isWiseData = YES;
                                   NSArray *addArray = nil;
                                   
                                   isWiseData =
                                   isWiseData && [ALNetWorkAPI checkSerialNumber:@"A060" ResponseObject:responseObject];
                                   
                                   isWiseData =
                                   isWiseData && responseObject.safe[@"resBody"][@"VideoList"];
                                   
                                   isWiseData =
                                   isWiseData && [responseObject[@"resBody"][@"VideoList"] isKindOfClass:[NSArray class]];
                                   
                                   if (isWiseData) {
                                       addArray = responseObject[@"resBody"][@"VideoList"];
                                       
                                       objectListArray = [addArray mutableCopy];
                                       [self.mainTableView reloadData];
                                   }
                                   finishBlockToRun(isWiseData);
                               }];
}

@end
