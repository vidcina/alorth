//
//  ALPersonFollowCell.h
//
//  Created by w91379137 on 2015/10/27.
//

typedef NS_ENUM(NSUInteger, ALPersonFollowCellSelect) {
    ALPersonFollowCellSelectNone    = 0,
    ALPersonFollowCellSelectFollow  = 1
};

#import "ALCommonTableViewCell.h"

@interface ALPersonFollowCell : ALCommonTableViewCell

@property (nonatomic) ALPersonFollowCellSelect cellSelect;

@property (nonatomic, strong) IBOutlet UIImageView *userImageView;
@property (nonatomic, strong) IBOutlet UILabel *userLabel;
@property (nonatomic, strong) IBOutlet UIImageView *followImageView;
@property (nonatomic, strong) IBOutlet UIButton *followButton;

@end
