//
//  ALSearchPersonCell.h
//  alorth
//
//  Created by w91379137 on 2015/11/3.
//  Copyright © 2015年 w91379137. All rights reserved.
//

typedef NS_ENUM(NSUInteger, ALSearchPersonCellSelect) {
    ALSearchPersonCellSelecttNone       = 0,
    ALSearchPersonCellSelectFollow      = 1
};

#import "ALCommonTableViewCell.h"

@interface ALSearchPersonCell : ALCommonTableViewCell

@property (nonatomic) ALSearchPersonCellSelect cellSelect;

@property (nonatomic, strong) IBOutlet UIImageView *avatarImageView;
@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet UILabel *numberLabel;
@property (nonatomic, strong) IBOutlet UIImageView *followImageView;
@property (nonatomic, strong) IBOutlet UIButton *followButton;

@end
