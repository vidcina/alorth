//
//  ALSearchPersonCell.m
//  alorth
//
//  Created by w91379137 on 2015/11/3.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALSearchPersonCell.h"

@implementation ALSearchPersonCell

- (void)cellFromXibSetting
{
    self.avatarImageView.clipsToBounds = YES;
    self.avatarImageView.layer.cornerRadius = self.avatarImageView.frame.size.height / 2;
}

- (CGFloat)performWithInfoDict:(NSDictionary *)infoDict
{
    self.infoDict = infoDict;
    
    [self.avatarImageView imageFromURLString:self.infoDict[@"UsertHeadImage"]];
    
    self.nameLabel.text =
    [ALFormatter preferredDisplayNameFirst:self.infoDict[@"FirstName"]
                                      Last:self.infoDict[@"LastName"]
                                    UserID:self.infoDict[@"UserID"]];
    
    UILabel *object = self.numberLabel;
    object.localText = [NSString stringWithFormat:@"%@ $Followers$",self.infoDict[@"FollowerNumber"]];
    
    if ([[ALAppDelegate sharedAppDelegate].userID isEqualToString:infoDict[@"UserID"]]) {
        //自己的不能有按鈕
        self.followButton.hidden = YES;
    }
    else {
        self.followButton.hidden = NO;
    }
    
    //圖片跟隨
    self.followImageView.hidden = self.followButton.hidden;
    if (!self.followImageView.hidden) {
        NSString *imageName =
        [infoDict[@"FollowStatus"] intValue] == 0 ? @"YellowAdd" : @"YellowCheck";
        self.followImageView.image = [UIImage imageNamed:imageName];
    }
    
    return 50;
}

- (IBAction)followAction
{
    self.cellSelect = ALSearchPersonCellSelectFollow;
    [self tryTableViewSelectCell];
    self.cellSelect = ALSearchPersonCellSelecttNone;
}

@end
