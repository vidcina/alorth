//
//  ALPersonFollowCell.m
//
//  Created by w91379137 on 2015/10/27.
//

#import "ALPersonFollowCell.h"

@implementation ALPersonFollowCell

- (void)cellFromXibSetting
{
    self.userImageView.clipsToBounds = YES;
    self.userImageView.layer.cornerRadius = self.userImageView.frame.size.height / 2;
}

- (CGFloat)performWithInfoDict:(NSDictionary *)infoDict
{
    self.infoDict = infoDict;
    
    [self.userImageView imageFromURLString:infoDict[@"HeadImage"]];
    
    self.userLabel.text =
    [ALFormatter preferredDisplayNameFirst:infoDict[@"FirstName"]
                                      Last:infoDict[@"LastName"]
                                    UserID:infoDict[@"UserID"]];
    
    if ([[ALAppDelegate sharedAppDelegate].userID isEqualToString:infoDict[@"UserID"]]) {
        //自己的不能有按鈕
        self.followButton.hidden = YES;
    }
    else {
        self.followButton.hidden = NO;
    }
    
    //圖片跟隨
    self.followImageView.hidden = self.followButton.hidden;
    if (!self.followImageView.hidden) {
        NSString *imageName =
        [infoDict[@"FollowStatus"] intValue] == 0 ? @"YellowAdd" : @"YellowCheck";
        self.followImageView.image = [UIImage imageNamed:imageName];
    }
    
    return 50;
}

- (IBAction)followAction
{
    self.cellSelect = ALPersonFollowCellSelectFollow;
    [self tryTableViewSelectCell];
    self.cellSelect = ALPersonFollowCellSelectNone;
}

@end
