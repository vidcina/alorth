//
//  ALPersonIntroductionViewController.m
//  alorth
//
//  Created by w91379137 on 2015/10/20.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALPersonIntroductionViewController.h"

#import "ALUserVodListView.h"
#import "ALUserProductListView.h"
#import "ALUserRatingListView.h"

#import "PDSEnvironmentViewController+ALTestControl.h"

typedef NS_ENUM(NSUInteger, ALPersonIntroductionStatus) {
    ALPersonIntroductionStatusInit          = 0,
    ALPersonIntroductionStatusVod           = 1,
    ALPersonIntroductionStatusProduct       = 2,
    ALPersonIntroductionStatusRate          = 3
};

@interface ALPersonIntroductionViewController ()
{
    ALUserVodListView *vodListView;
    ALUserProductListView *productListView;
    ALUserRatingListView *ratingListView;
}
@property (nonatomic) ALPersonIntroductionStatus listStatus;

@end

@implementation ALPersonIntroductionViewController

#pragma mark - VC Life
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self defaultInitMethod];
        userID = [[ALAppDelegate sharedAppDelegate] userID];
    }
    return self;
}

- (instancetype)initWithUserId:(NSString *)anUserID
{
    self = [super init];
    if (self) {
        [self defaultInitMethod];
        userID = anUserID;
        if (!anUserID) {
            userID = [[ALAppDelegate sharedAppDelegate] userID];
        }
    }
    return self;
}

-(void)defaultInitMethod
{

}

-(void)viewWillAppearFirstTime:(BOOL)animated
{
    [super viewWillAppearFirstTime:animated];
    [headerView.backButton addTarget:self
                              action:@selector(back:)
                    forControlEvents:UIControlEventTouchUpInside];
    
    userAvatarImageView.layer.masksToBounds = YES;
    userAvatarImageView.layer.cornerRadius = (userAvatarImageView.frame.size.height / 2);
    [userAvatarImageView.layer setBorderColor:[UIColor whiteColor].CGColor];
    [userAvatarImageView.layer setBorderWidth:2];
    
    [ALUserInfoCacheManager getLocalUserInfomation:userID
                                   completionBlock:^(NSDictionary *responseObject) {
                                       
                                       if ([ALNetWorkAPI checkSerialNumber:@"A033"
                                                            ResponseObject:responseObject]) {
                                           
                                           userNameLabel.text =
                                           [ALFormatter preferredDisplayNameForUserInfomation:responseObject
                                                                                       UserID:userID];
                                           
                                           if (responseObject.safe[@"resBody"][@"HeadImage"]) {
                                               [userAvatarImageView imageFromURLString:responseObject.safe[@"resBody"][@"HeadImage"]];
                                           }
                                       }
                                       else {
                                           YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
                                       }
                                   }];
    
    //別人的列表才有
    followChatView.hidden = [userID isEqualToString:[ALAppDelegate sharedAppDelegate].userID];
    
    followActionButton.layer.borderWidth = 1;
    followActionButton.layer.borderColor = COMMON_GRAY3_COLOR.CGColor;
    followActionButton.layer.cornerRadius = 4;
    
    chatActionButton.layer.borderWidth = 1;
    chatActionButton.layer.borderColor = COMMON_GRAY3_COLOR.CGColor;
    chatActionButton.layer.cornerRadius = 4;
    
    //偵測追尋狀態
    if (![userID isEqualToString:[ALAppDelegate sharedAppDelegate].userID]) {
        
        followActionButton.enabled = NO;
        [ALNetWorkAPI followStatusA068UserID:userID
                             CompletionBlock:^(NSDictionary *responseObject) {
                                 followActionButton.enabled = YES;
                                 
                                 if ([ALNetWorkAPI checkSerialNumber:@"A068"
                                                      ResponseObject:responseObject]) {
                                     self.followStatus = responseObject[@"resBody"][@"FollowStatus"];
                                 }
                                 else {
                                     //followActionButton.enabled = NO;
                                     self.followStatus = @"0";
                                     ALLog(@"無法取得跟隨狀態");
                                 }
                             }];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (vodListView == nil) {
        vodListView = [[ALUserVodListView alloc] init];
        vodListView.delegate = self;
        vodListView.userID = userID;
        [vodListView checkNewData];
    }
    if (productListView == nil) {
        productListView = [[ALUserProductListView alloc] init];
        productListView.delegate = self;
        productListView.userID = userID;
        [productListView checkNewData];
    }
    if (ratingListView == nil) {
        ratingListView = [[ALUserRatingListView alloc] init];
        ratingListView.delegate = self;
        ratingListView.userID = userID;
        [ratingListView checkNewData];
    }
    
    NSMutableArray *views = [NSMutableArray array];
    if (vodListView && !vodListView.superview) [views addObject:vodListView];
    if (productListView && !productListView.superview) [views addObject:productListView];
    if (ratingListView && !ratingListView.superview) [views addObject:ratingListView];
    
    for (UIView *view in views) {
        view.hidden = YES;
        [listBaseView addSubview:view];
        [view mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(listBaseView);
        }];
    }
    
    if (self.listStatus == ALPersonIntroductionStatusInit) self.listStatus = ALPersonIntroductionStatusVod;
    [productListView controlPlayCell:(self.listStatus == ALPersonIntroductionStatusProduct)];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [productListView controlPlayCell:NO];
    [super viewDidDisappear:animated];
}

#pragma mark - Setter
-(void)setListStatus:(ALPersonIntroductionStatus)listStatus
{
    if (self.listStatus == listStatus) {
        ALLog(@"重複設定 listStatus");
        return;
    }
    _listStatus = listStatus;
    
    {//顯示畫面
        vodListView.hidden = (self.listStatus != ALPersonIntroductionStatusVod);
        
        productListView.hidden = (self.listStatus != ALPersonIntroductionStatusProduct);
        [productListView controlPlayCell:(self.listStatus == ALPersonIntroductionStatusProduct)];
        
        ratingListView.hidden = (self.listStatus != ALPersonIntroductionStatusRate);
    }
    
    {//取消群
        NSMutableArray *cancelButtons = [NSMutableArray array];
        if (vodListView.hidden && vodButton) [cancelButtons addObject:vodButton];
        if (productListView.hidden && productButton) [cancelButtons addObject:productButton];
        if (ratingListView.hidden && ratingButton) [cancelButtons addObject:ratingButton];
        for (UIButton *button in cancelButtons) {
            button.alpha = 0.5;
        }
    }
    
    {//選到群
        NSMutableArray *selectButtons = [NSMutableArray array];
        if (!vodListView.hidden && vodButton) [selectButtons addObject:vodButton];
        if (!productListView.hidden && productButton) [selectButtons addObject:productButton];
        if (!ratingListView.hidden && ratingButton) [selectButtons addObject:ratingButton];
        for (UIButton *button in selectButtons) {
            button.alpha = 1;
        }
    }
}

- (void)setFollowStatus:(NSString *)followStatus
{
    _followStatus = followStatus;
    addFollowLabel.hidden = !([self.followStatus intValue] == 0);
    if ([self.followStatus intValue] == 0) {
        followActionButton.normalTitle = @"    $Follow$";
    }
    else {
        followActionButton.normalTitle = @"Following";
    }
}

#pragma mark - IBAction
-(IBAction)showFollowerList:(id)sender
{
    ALPersonFollowersListViewController *next =
    [[ALPersonFollowersListViewController alloc] init];
    
    next.userID = userID;
    
    [self.navigationController pushViewController:next animated:YES];
}

-(IBAction)showFollowingList:(id)sender
{
    ALPersonFollowingListViewController *next =
    [[ALPersonFollowingListViewController alloc] init];
    
    next.userID = userID;
    
    [self.navigationController pushViewController:next animated:YES];
}

-(IBAction)followAction:(id)sender
{
    NSString *willChangeToStatus = [self.followStatus intValue] == 0 ? @"1" : @"0";

    followActionButton.enabled = NO;
    [ALNetWorkAPI changeFollowStatusA040UserID:userID
                                  FollowStatus:willChangeToStatus
                               CompletionBlock:^(NSDictionary *responseObject) {
                                   followActionButton.enabled = YES;
                                   
                                   if ([ALNetWorkAPI checkSerialNumber:@"A040"
                                                        ResponseObject:responseObject]) {
                                       self.followStatus = willChangeToStatus;
                                   }
                                   else {
                                       ALLog(@"%@",responseObject);
                                   }
                               }];
}

-(IBAction)chatAction:(id)sender
{
    [self chatWithOtherId:userID];
}

-(IBAction)listAction:(id)sender
{
    if (sender == vodButton) self.listStatus = ALPersonIntroductionStatusVod;
    if (sender == productButton) self.listStatus = ALPersonIntroductionStatusProduct;
    if (sender == ratingButton) self.listStatus = ALPersonIntroductionStatusRate;
}

#pragma mark - Test
- (IBAction)testButton1Action:(id)sender
{
    testButton1ClickCounter++;
    NSLog(@"%ld",(long)testButton1ClickCounter);
}

- (IBAction)testButton2Action:(id)sender
{
    if (testButton1ClickCounter == 8) {
        [[PDSEnvironmentViewController sharedInstance] addTestControl];
    }
}

@end
