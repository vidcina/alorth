//
//  ALPersonIntroductionViewController.h
//  alorth
//
//  Created by w91379137 on 2015/10/20.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicSubViewController.h"

@interface ALPersonIntroductionViewController : ALBasicSubViewController
{
    NSString *userID;
    
    IBOutlet UIImageView *userAvatarImageView;
    IBOutlet UILabel *userNameLabel;
    
    IBOutlet UIView *followShowView;
    
    IBOutlet UIView *followChatView;
    IBOutlet UIButton *followActionButton;
    IBOutlet UILabel *addFollowLabel;
    
    IBOutlet UIButton *chatActionButton;
    
    IBOutlet UIButton *vodButton;
    IBOutlet UIButton *productButton;
    IBOutlet UIButton *ratingButton;
    
    IBOutlet UIView *listBaseView;
    
    NSInteger testButton1ClickCounter;
}

-(instancetype)initWithUserId:(NSString *)anUserID;

@property (nonatomic, strong) NSString *followStatus;

@end
