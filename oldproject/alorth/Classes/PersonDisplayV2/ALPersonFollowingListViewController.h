//
//  ALPersonFollowingListViewController.h
//  alorth
//
//  Created by w91379137 on 2015/10/20.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicSubViewController.h"

@interface ALPersonFollowingListViewController : ALBasicSubViewController
<UITableViewDataSource, UITableViewDelegate>
{
    //清單
    NSMutableArray *objectListArray;
    BOOL willReplaceWithNew;                    //是否清空之前資料
    ALTableViewControl *currentTableViewControl;
}

@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) IBOutlet UITableView *mainTableView;

@end
