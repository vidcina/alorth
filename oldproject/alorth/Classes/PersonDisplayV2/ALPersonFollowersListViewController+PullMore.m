//
//  ALPersonFollowersListViewController+PullMore.m
//  alorth
//
//  Created by w91379137 on 2015/10/20.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALPersonFollowersListViewController+PullMore.h"
static NSString *accountFollowersListA070 = @"accountFollowersListA070";

@implementation ALPersonFollowersListViewController (PullMore)
#pragma mark - scrollView Delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.mainTableView) {
        [currentTableViewControl scrollViewDidScroll:scrollView];
    }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView == self.mainTableView) {
        [currentTableViewControl scrollViewDidEndDragging:scrollView willDecelerate:(BOOL)decelerate];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.mainTableView) {
        [currentTableViewControl scrollViewDidEndDecelerating:(UIScrollView *)scrollView];
    }
}

#pragma mark - ALTableViewControlDelegate
-(void)checkNewData:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    willReplaceWithNew = YES;
    tableViewControl.lastDict[@"StartNum"] = @"0";
    
    ALLog(@"重新整理");
    [self addMBProgressHUDWithKey:accountFollowersListA070];
    [self goodsListLoadApiSourceParameters:tableViewControl.lastDict
                               FinishBlock:^(BOOL isSuccess) {
                                   [self removeMBProgressHUDWithKey:accountFollowersListA070];
                               }];
}

-(void)loadMore:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    
    ALLog(@"載入更多:目前第 %@ 頁 %lu 筆資料",
          tableViewControl.lastDict[@"StartNum"],
          (unsigned long)currentTableViewControl.apiDataArray.count);
    
    tableViewControl.lastDict[@"StartNum"] =
    [NSString stringWithFormat:@"%d",[tableViewControl.lastDict[@"StartNum"] intValue] + 1];
    
    [self addMBProgressHUDWithKey:accountFollowersListA070];
    [self goodsListLoadApiSourceParameters:tableViewControl.lastDict
                               FinishBlock:^(BOOL isSuccess) {
                                   [self removeMBProgressHUDWithKey:accountFollowersListA070];
                               }];
}

#pragma mark - API
-(void)goodsListLoadApiSourceParameters:(NSDictionary *)parameters
                            FinishBlock:(void (^)(BOOL isSuccess))finishBlockToRun
{
    [ALNetWorkAPI accountFollowersListA070UserID:self.userID
                                        StartNum:parameters[@"StartNum"]
                                 CompletionBlock:^(NSDictionary *responseObject) {
                                     
                                     BOOL isWiseData =
                                     [self checkListData:responseObject];
                                     
                                     if (!isWiseData) ALLog(@"%@ 錯誤",accountFollowersListA070);
                                     finishBlockToRun(isWiseData);
                                     
                                 }];
}

- (BOOL)checkListData:(NSDictionary *)responseObject
{
    BOOL isWiseData = YES;
    NSArray *addArray = nil;
    
    isWiseData =
    isWiseData && [ALNetWorkAPI checkSerialNumber:@"A070" ResponseObject:responseObject];
    
    isWiseData =
    isWiseData && responseObject.safe[@"resBody"][@"FollowersList"];
    
    isWiseData =
    isWiseData && [responseObject[@"resBody"][@"FollowersList"] isKindOfClass:[NSArray class]];
    
    if (isWiseData) {
        addArray = responseObject[@"resBody"][@"FollowersList"];
        
        if (willReplaceWithNew) {
            [currentTableViewControl.apiDataArray removeAllObjects];
        }
        
        [currentTableViewControl.apiDataArray addObjectsFromArray:addArray];
        [self.mainTableView reloadData];
        
        if (willReplaceWithNew) {
            [self.mainTableView setContentOffset:CGPointZero animated:YES];
        }
        willReplaceWithNew = NO;
    }
    return isWiseData;
}


@end
