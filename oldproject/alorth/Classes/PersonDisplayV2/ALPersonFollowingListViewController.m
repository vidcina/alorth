//
//  ALPersonFollowingListViewController.m
//  alorth
//
//  Created by w91379137 on 2015/10/20.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALPersonFollowingListViewController.h"
#import "ALPersonFollowingListViewController+PullMore.h"

#import "ALPersonFollowCell.h"

@interface ALPersonFollowingListViewController ()

@end

@implementation ALPersonFollowingListViewController

#pragma mark - VC Life
- (instancetype)init
{
    self = [super init];
    if (self) {
        objectListArray = [NSMutableArray array];
        
        currentTableViewControl = [[ALTableViewControl alloc] init];
        currentTableViewControl.delegate = self;
        currentTableViewControl.apiDataArray = objectListArray;
    }
    return self;
}

- (void)viewWillAppearFirstTime:(BOOL)animated
{
    [super viewWillAppearFirstTime:animated];
    [headerView.backButton addTarget:self
                              action:@selector(back:)
                    forControlEvents:UIControlEventTouchUpInside];
    
    [self alTableViewSetting:self.mainTableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (objectListArray.count == 0) {
        [self checkNewData:currentTableViewControl];
    }
}

#pragma mark - UITableViewDataSource Row
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return objectListArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ALPersonFollowCell *cell = [ALPersonFollowCell cellFromXib];
    [cell performWithInfoDict:objectListArray[indexPath.row]];
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ALPersonFollowCell *cell = (ALPersonFollowCell *)[tableView cellForRowAtIndexPath:indexPath];
    NSDictionary *infoDict = cell.infoDict;
    
    switch (cell.cellSelect) {
        case ALPersonFollowCellSelectNone:
        {
            ALPersonIntroductionViewController *next =
            [[ALPersonIntroductionViewController alloc] initWithUserId:infoDict[@"UserID"]];
            [self.navigationController pushViewController:next animated:YES];
        }
            break;
            
        case ALPersonFollowCellSelectFollow:
        {
            if (![PDSAccountManager isLoggedIn]) {
                ALLog(@"沒有登入");
                return;
            }
            
            NSString *followStatus = infoDict[@"FollowStatus"];
            
            NSString *changeToStatus = [followStatus intValue] == 0 ? @"1" : @"0";
            
            [self addMBProgressHUDWithKey:@"changeFollowStatusA040"];
            [ALNetWorkAPI changeFollowStatusA040UserID:infoDict[@"UserID"]
                                          FollowStatus:changeToStatus
                                       CompletionBlock:^(NSDictionary *responseObject) {
                                           [self removeMBProgressHUDWithKey:@"changeFollowStatusA040"];
                                           
                                           if ([ALNetWorkAPI checkSerialNumber:@"A040"
                                                                ResponseObject:responseObject]) {
                                               
                                               NSMutableDictionary *newDict = [infoDict mutableCopy];
                                               [newDict setObject:changeToStatus forKey:@"FollowStatus"];
                                               
                                               NSInteger index = [objectListArray indexOfObject:infoDict];
                                               if (index != NSNotFound) {
                                                   [objectListArray removeObjectAtIndex:index];
                                                   [objectListArray insertObject:newDict atIndex:index];
                                                   [self.mainTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                                               }
                                               else {
                                                   NSLog(@"NSNotFound");
                                               }
                                           }
                                           else {
                                               YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
                                           }
                                       }];
        }
            break;
            
        default:
            break;
    }
}

@end
