//
//  ALPersonFollowingListViewController+PullMore.h
//  alorth
//
//  Created by w91379137 on 2015/10/20.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALPersonFollowingListViewController.h"

@interface ALPersonFollowingListViewController (PullMore)
<ALTableViewControlDelegate>

-(void)checkNewData:(ALTableViewControl *)tableViewControl;
-(void)loadMore:(ALTableViewControl *)tableViewControl;

@end
