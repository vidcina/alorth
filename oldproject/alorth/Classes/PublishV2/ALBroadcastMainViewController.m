//
//  ALBroadcastMainViewController.m
//  alorth
//
//  Created by w91379137 on 2015/11/1.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALBroadcastMainViewController.h"

@interface ALBroadcastMainViewController ()

@end

@implementation ALBroadcastMainViewController

#pragma mark - VC Life
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (selectViewController == nil) {
        selectViewController = [[ALBroadcastSelectViewController alloc] init];
        selectViewController.delegate = self;
        [self addChildViewController:selectViewController];
    }
}

- (void)viewWillAppearFirstTime:(BOOL)animated
{
    [super viewWillAppearFirstTime:animated];
    
    if (selectViewController.view.superview == nil) {
        [self.view addSubview:selectViewController.view];
        [selectViewController didMoveToParentViewController:self];
        [selectViewController.view mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
    }
}

- (void)viewDidAppearFirstTime:(BOOL)animated
{
    [super viewDidAppearFirstTime:animated];
    
    if (broadcastViewController == nil) {
        broadcastViewController = [[PLViewController alloc] init];
        broadcastViewController.delegate = self;
        [self addChildViewController:broadcastViewController];
    }
    
    [self performSelector:@selector(startPreview) withObject:nil afterDelay:BroadcastCameraDelayTime];
}

- (void)startPreview
{
    if (broadcastViewController.view.superview == nil) {
        [self.view insertSubview:broadcastViewController.view belowSubview:selectViewController.view];
        [broadcastViewController didMoveToParentViewController:self];
        [broadcastViewController.view mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
    }
    [broadcastViewController.view layoutIfNeeded];
    [broadcastViewController startPreview];
    [selectViewController startPreview];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(startPreview) object:nil];
    [super viewWillDisappear:animated];
}

#pragma mark -
- (void)setStreamPrefix:(NSString *)streamPrefix
{
    broadcastViewController.streamPrefix = streamPrefix;
}

- (void)setStreamID:(NSString *)streamID
{
    broadcastViewController.streamID = streamID;
}

- (void)completeStream
{
    [self addMBProgressHUDWithKey:@"streamInfomationA066StreamID"];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [ALNetWorkAPI streamInfomationA066StreamID:broadcastViewController.streamID
                                   CompletionBlock:^(NSDictionary *responseObject) {
                                       [self removeMBProgressHUDWithKey:@"streamInfomationA066StreamID"];
                                       
                                       if ([ALNetWorkAPI checkSerialNumber:@"A066"
                                                            ResponseObject:responseObject]) {
                                           
                                           UIViewController *vc = [self playStreamVideoController:responseObject[@"resBody"]];
                                           [[PDSEnvironmentViewController sharedInstance] submitViewController:vc
                                                                                               CompletionBlock:nil];
                                       }
                                       
                                       [self back:nil];
                                   }];
    });
}

@end
