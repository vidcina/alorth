//
//  ALBroadcastSelectViewController+PullMore.m
//  alorth
//
//  Created by w91379137 on 2015/10/15.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBroadcastSelectViewController+PullMore.h"
static NSString *getStreamProductA055 = @"getStreamProductA055";

@implementation ALBroadcastSelectViewController (PullMore)

#pragma mark - scrollView Delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.readyCollectionView) {
        [currentTableViewControl scrollViewDidScroll:scrollView];
    }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView == self.readyCollectionView) {
        [currentTableViewControl scrollViewDidEndDragging:scrollView willDecelerate:(BOOL)decelerate];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.readyCollectionView) {
        [currentTableViewControl scrollViewDidEndDecelerating:(UIScrollView *)scrollView];
    }
}

#pragma mark - ALTableViewControlDelegate
-(void)checkNewData:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    willReplaceWithNew = YES;
    tableViewControl.lastDict[@"StartNum"] = @"0";
    
    ALLog(@"重新整理");
    [self addMBProgressHUDWithKey:getStreamProductA055];
    [self listLoadApiSourceParameters:tableViewControl.lastDict
                          FinishBlock:^(BOOL isSuccess) {
                              [self removeMBProgressHUDWithKey:getStreamProductA055];
                          }];
}

-(void)loadMore:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    
    ALLog(@"載入更多:目前第 %@ 頁 %lu 筆資料",tableViewControl.lastDict[@"StartNum"],(unsigned long)objectListArray.count);
    
    tableViewControl.lastDict[@"StartNum"] =
    [NSString stringWithFormat:@"%d",[tableViewControl.lastDict[@"StartNum"] intValue] + 1];
    
    [self addMBProgressHUDWithKey:getStreamProductA055];
    [self listLoadApiSourceParameters:tableViewControl.lastDict
                          FinishBlock:^(BOOL isSuccess) {
                              [self removeMBProgressHUDWithKey:getStreamProductA055];
                          }];
}

#pragma mark - API
-(void)listLoadApiSourceParameters:(NSDictionary *)parameters
                       FinishBlock:(void (^)(BOOL isSuccess))finishBlockToRun
{
    [ALNetWorkAPI getStreamProductA055Type:@"0"
                                  StartNum:parameters[@"StartNum"]
                           CompletionBlock:^(NSDictionary *responseObject) {
                               
                               BOOL isWiseData =
                               [self checkListData:responseObject];
                               
                               if (!isWiseData) ALLog(@"%@ 錯誤",getStreamProductA055);
                               finishBlockToRun(isWiseData);
                           }];
}

- (BOOL)checkListData:(NSDictionary *)responseObject
{
    BOOL isWiseData = YES;
    NSArray *addArray = nil;
    
    isWiseData =
    isWiseData && [ALNetWorkAPI checkSerialNumber:@"A055" ResponseObject:responseObject];
    
    isWiseData =
    isWiseData && responseObject.safe[@"resBody"][@"ProductList"];
    
    isWiseData =
    isWiseData && [responseObject[@"resBody"][@"ProductList"] isKindOfClass:[NSArray class]];
    
    if (isWiseData) {
        addArray = responseObject[@"resBody"][@"ProductList"];
        
        if (willReplaceWithNew) {
            [currentTableViewControl.apiDataArray removeAllObjects];
        }
        
        [currentTableViewControl.apiDataArray addObjectsFromArray:addArray];
        [self spliteObjectListArrayToReadyArray];
        
        if (willReplaceWithNew) {
            [self.readyCollectionView setContentOffset:CGPointZero animated:YES];
        }
        willReplaceWithNew = NO;
    }
    return isWiseData;
}

- (void)spliteObjectListArrayToReadyArray
{
    [readyArray removeAllObjects];
    
    NSMutableSet *didSelectID = [NSMutableSet set];
    for (NSDictionary *aObject in didSelectArray) {
        [didSelectID addObject:aObject[@"ProductID"]];
    }
    
    for (NSInteger k = 0; k < objectListArray.count; k++) {
        NSDictionary *aObject = objectListArray[k];
        if (![didSelectID containsObject:aObject[@"ProductID"]]) {
            [readyArray addObject:aObject];
        }
    }
    [self.readyCollectionView reloadData];
}

@end
