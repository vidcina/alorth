//
//  ALLifeSellSelectViewController.h
//  alorth
//
//  Created by w91379137 on 2015/10/7.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicSubViewController.h"
#import "LXReorderableCollectionViewFlowLayout.h"
@class ALBroadcastMainViewController;

@interface ALBroadcastSelectViewController : ALBasicSubViewController
<UICollectionViewDataSource, UICollectionViewDelegate,
LXReorderableCollectionViewDataSource, LXReorderableCollectionViewDelegateFlowLayout>
{
    int cellLenth;
    
    NSMutableArray *didSelectArray;
    NSMutableArray *readyArray;
    
    NSMutableArray *objectListArray;            //列表
    BOOL willReplaceWithNew;                    //是否清空之前資料
    ALTableViewControl *currentTableViewControl;//控制器
}

-(void)startPreview;

@property (nonatomic, weak) ALBroadcastMainViewController *delegate;
@property (strong, nonatomic) IBOutlet UIView *mainScrollContainerView;

@property (strong, nonatomic) IBOutlet UICollectionView *didSelectCollectionView;
@property (strong, nonatomic) IBOutlet UIButton *startBroadcastButton;
@property (strong, nonatomic) IBOutlet UICollectionView *readyCollectionView;

@property (strong, nonatomic) IBOutlet UITextView *titleTextView;

@property (nonatomic, strong) IBOutlet ALSelectTypeScrollView *selectTypeScrollView;
@property (nonatomic, strong) NSString *selectedLanguageIndex;

@property (strong, nonatomic) IBOutlet UIView *startBroadcastView;

@end
