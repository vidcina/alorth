//
//  ALLifeSellSelectViewController.m
//  alorth
//
//  Created by w91379137 on 2015/10/7.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBroadcastSelectViewController.h"
#import "ALBroadcastSelectViewController+PullMore.h"

#import "ALBroadcastMainViewController.h"

#import "ALPublishViewController.h"
#import "ALBroadcastCollectionViewCell.h"
#import "PLViewController.h"

#define DidSelectMaximumAmount 3
static NSString *kCollectionViewCellID = @"CollectionViewCellID";
static NSString *kCollectionViewReuseIdentifierID = @"kCollectionViewReuseIdentifierID";

@interface ALBroadcastSelectViewController ()

@end

@implementation ALBroadcastSelectViewController

#pragma mark - VC Life
-(instancetype)init
{
    self = [super init];
    if (self) {
        objectListArray = [NSMutableArray array];
        didSelectArray = [NSMutableArray array];
        readyArray = [NSMutableArray array];
        
        currentTableViewControl = [[ALTableViewControl alloc] init];
        currentTableViewControl.delegate = self;
        currentTableViewControl.apiDataArray = objectListArray;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSArray *collectionViews = @[self.didSelectCollectionView,self.readyCollectionView];
    cellLenth = ([UIScreen mainScreen].bounds.size.width - 20) / 3;
    
    for (UICollectionView *collectionView in collectionViews) {
        [collectionView registerNib:
         [UINib nibWithNibName:NSStringFromClass([ALBroadcastCollectionViewCell class])
                        bundle:nil]
                       forCellWithReuseIdentifier:kCollectionViewCellID];
        
        [collectionView registerClass:[UICollectionReusableView class]
                         forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                                withReuseIdentifier:kCollectionViewReuseIdentifierID];
        
        [collectionView registerClass:[UICollectionReusableView class]
                         forSupplementaryViewOfKind:UICollectionElementKindSectionFooter
                                withReuseIdentifier:kCollectionViewReuseIdentifierID];
        
        LXReorderableCollectionViewFlowLayout *flowLayout =
        (LXReorderableCollectionViewFlowLayout *)collectionView.collectionViewLayout;
        flowLayout.panGestureRecognizer.enabled = YES;
        flowLayout.longPressGestureRecognizer.enabled = YES;
        flowLayout.longPressGestureRecognizer.minimumPressDuration = 0.25;
        
        flowLayout.itemSize = CGSizeMake(cellLenth, cellLenth);
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self didSelectCollectionViewUpdateHeight:NO];
    UIView *switchView = [ALPublishSwitchView sharedInstance];
    if (switchView.superview != self.view) {
        [self.view addSubview:switchView];
        [switchView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view.mas_top);
            make.leading.equalTo(self.view.mas_leading);
        }];
        
        [switchView addSafeObserver:self
                         forKeyPath:NSStringFromSelector(@selector(publishStatus))
                            options:NSKeyValueObservingOptionNew
                            context:NULL];
    }
    
    if (objectListArray.count == 0) {
        [self checkNewData:currentTableViewControl];
    }
    
    self.selectTypeScrollView.infoObjectArray = [ALBundleTextResources.languageArray mutableCopy];
    [self.selectTypeScrollView selectIndex:0];
}

- (void)back:(UIButton *)sender
{
    if (self.delegate) {
        [self.delegate back:sender];
    }
    else {
        [super back:sender];
    }
}

#pragma mark - Preview
- (void)startPreview
{
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.startBroadcastButton.alpha = 1;
                     } completion:^(BOOL finished) {
                         self.startBroadcastButton.enabled = YES;
                     }];
}

#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if (object == [ALPublishSwitchView sharedInstance] &&
        [keyPath isEqualToString:NSStringFromSelector(@selector(publishStatus))]) {
        
        if ([ALPublishSwitchView sharedInstance].publishStatus == ListingSell) {
            ALPublishViewController *vc =
            [[ALPublishViewController alloc] init];
            
            UINavigationController *nv = nil;
            if (self.delegate) {
                nv = self.delegate.navigationController;
            }
            else {
                nv = self.navigationController;
            }
            
            NSMutableArray *viewControllers = [nv.viewControllers mutableCopy];
            [viewControllers removeLastObject];
            [viewControllers addObject:vc];
            [nv setViewControllers:viewControllers];
        }
    }
}

#pragma mark - IBAction
-(IBAction)startBroadcast:(id)sender
{
    if (self.titleTextView.text.length == 0) {
        YKSimpleAlert(ALLocalizedString(@"Please enter a video description",nil));
        return;
    }
    
    if (didSelectArray.count == 0) {
        YKSimpleAlert(ALLocalizedString(@"Please select merchandise",nil));
        return;
    }
    
    NSMutableArray *sendList = [NSMutableArray array];
    
    for (NSInteger k = 0; k < didSelectArray.count; k++) {
        NSMutableDictionary *aObject = [didSelectArray[k] mutableCopy];
        [aObject setObject:[NSString stringWithFormat:@"%ld",(long)k + 1] forKey:@"Priority"];
        [sendList addObject:aObject];
    }
    
    CLLocation *nowLocation = [ALAppDelegate sharedAppDelegate].nowLocation;
    
    [self keyboardReturn];
    [self addMBProgressHUDWithKey:@"createStreamA056"];
    [ALNetWorkAPI createStreamA056Name:self.titleTextView.text
                             Longitude:[NSString stringWithFormat:@"%.3f",nowLocation.coordinate.longitude]
                               Laitude:[NSString stringWithFormat:@"%.3f",nowLocation.coordinate.latitude]
                           ProductList:sendList
                        StreamLanguage:self.selectedLanguageIndex
                       CompletionBlock:^(NSDictionary *responseObject) {
                           [self removeMBProgressHUDWithKey:@"createStreamA056"];
                           
                           NSString *streamPrefix = responseObject.safe[@"resBody"][@"StreamPrefix"];
                           NSString *streamID = responseObject.safe[@"resBody"][@"StreamID"];
                           
                           if (streamPrefix && streamID) {
                               
                               if (self.delegate != nil) {
                                   [self.delegate setStreamPrefix:streamPrefix];
                                   [self.delegate setStreamID:streamID];
                                   self.view.hidden = YES;
                               }
                               else {
                                   //舊版還未組合畫面之前
                                   PLViewController *vc = [[PLViewController alloc] init];
                                   vc.streamPrefix = streamPrefix;
                                   vc.streamID = streamID;
                                   [self.navigationController pushViewController:vc animated:YES];
                               }
                               
                           }
                           else {
                               YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
                           }
                       }];
}

#pragma mark - UICollectionViewDataSource Section
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake([UIScreen mainScreen].bounds.size.width, 1);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeMake([UIScreen mainScreen].bounds.size.width, 1);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview =
    [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                       withReuseIdentifier:kCollectionViewReuseIdentifierID
                                              forIndexPath:indexPath];
    reusableview.backgroundColor = COMMON_GRAY4_COLOR;
    return reusableview;
}

#pragma mark - UICollectionViewDataSource Cell
- (NSInteger)collectionView:(UICollectionView *)theCollectionView
     numberOfItemsInSection:(NSInteger)theSectionIndex
{
    if (theCollectionView == self.didSelectCollectionView) {
        return didSelectArray.count;
    }
    else if (theCollectionView == self.readyCollectionView) {
        return readyArray.count;
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSArray *infoArray = [self targetArrayOfCollectionView:collectionView];
    NSDictionary *infoDict = infoArray[indexPath.row];
    
    ALBroadcastCollectionViewCell *cell =
    [collectionView dequeueReusableCellWithReuseIdentifier:kCollectionViewCellID
                                              forIndexPath:indexPath];
    [cell performCellDictionary:infoDict];
    return cell;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionView *fromCollectionView = collectionView;
    UICollectionView *toCollectionView = (collectionView == self.didSelectCollectionView ?
                                          self.readyCollectionView : self.didSelectCollectionView);
    
    NSMutableArray *fromArray =
    [self targetArrayOfCollectionView:fromCollectionView];
    
    NSMutableArray *toArray =
    [self targetArrayOfCollectionView:toCollectionView];
    
    if (toArray == didSelectArray && didSelectArray.count >= DidSelectMaximumAmount) {
        ALLog(@"didselectArray 已滿");
    }
    else {
        //取出
        NSObject *object = fromArray[indexPath.item];
        
        //從來源移除
        [fromArray removeObjectAtIndex:indexPath.item];
        [fromCollectionView performBatchUpdates:^{
            [collectionView deleteItemsAtIndexPaths:@[indexPath]];
        } completion:^(BOOL finished) {}];
        
        //加入
        NSInteger insertIndex = (toArray == didSelectArray) ? didSelectArray.count : 0;
        [toArray insertObject:object atIndex:insertIndex];
        [toCollectionView performBatchUpdates:^{
            [toCollectionView insertItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:insertIndex inSection:0]]];
        } completion:^(BOOL finished) {}];
    }
    [self didSelectCollectionViewUpdateHeight:YES];
}

-(void)didSelectCollectionViewUpdateHeight:(BOOL)animated
{
    if (didSelectArray.count > 0) {
        [self.didSelectCollectionView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@(cellLenth + 5));
        }];
    }
    else {
        [self.didSelectCollectionView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@1);
        }];
    }
    
    if (animated) {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [self.view layoutIfNeeded];
                         }];
    }
}

#pragma mark - LXReorderableCollectionViewDataSource
- (void)collectionView:(UICollectionView *)collectionView
       itemAtIndexPath:(NSIndexPath *)fromIndexPath
   willMoveToIndexPath:(NSIndexPath *)toIndexPath
{
    NSObject *object = nil;
    
    NSMutableArray *array = [self targetArrayOfCollectionView:collectionView];
    object = array[fromIndexPath.item];
    [array removeObjectAtIndex:fromIndexPath.item];
    [array insertObject:object atIndex:toIndexPath.item];
}

- (NSMutableArray *)targetArrayOfCollectionView:(UICollectionView *)collectionView
{
    if (collectionView == self.didSelectCollectionView) {
        return didSelectArray;
    }
    else if (collectionView == self.readyCollectionView) {
        return readyArray;
    }
    return nil;
}

#pragma mark - ALSelectTypeScrollViewDelegate
-(UIView *)typeViewOfIndex:(NSInteger)index
                infoObject:(NSObject *)infoObject
      selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    if ([infoObject isKindOfClass:[NSDictionary class]]) {
        
        NSDictionary *info = (NSDictionary *)infoObject;
        
        UIButton *aButton =
        [ALSelectTypeScrollView alSelectButton:info[@"LanguageMap"]];
        
        [aButton addTarget:selectTypeScrollView
                    action:@selector(selectView:)
          forControlEvents:UIControlEventTouchUpInside];
        
        CGSize titleSize =
        [ALSelectTypeScrollView alSelectButtonSize:info[@"LanguageMap"]];
        
        [aButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@(titleSize.width + 40));
        }];
        return aButton;
    }
    else {
        return [[UIView alloc] initWithFrame:CGRectZero];
    }
}

-(void)didSelectIndex:(NSInteger)index
           infoObject:(NSObject *)infoObject
 selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    NSDictionary *info = (NSDictionary *)infoObject;
    self.selectedLanguageIndex = info[@"LanguageIndex"];
}

-(void)adjustSelectView:(UIView *)view
             infoObject:(NSObject *)infoObject
               selected:(BOOL)selected
   selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    //這個有其他地方重複
    UIButton *button = (UIButton *)view;
    if (selected) {
        //選到
        button.selected = YES;
        button.backgroundColor = kALProductDetailDidselectBgColor;
        [button setTitleColor:kALProductDetailDidselectFontColor forState:UIControlStateNormal];
    }
    else {
        //沒
        button.selected = NO;
        button.backgroundColor = [UIColor clearColor];
        [button setTitleColor:kALProductDetailUnselectFontColor forState:UIControlStateNormal];
    }
}

#pragma mark - DZNEmptyDataSetSource
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    return [[NSMutableAttributedString alloc] initWithString:ALLocalizedString(@"Nothing to see", nil)];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    return [[NSMutableAttributedString alloc] initWithString:ALLocalizedString(@"$You can create some merchandise$ $at$ $Listing sales$ $or$ $find merchandises to press sell$.", nil)];
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    return nil;
}

@end
