//
//  ALBroadcastSelectViewController+PullMore.h
//  alorth
//
//  Created by w91379137 on 2015/10/15.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBroadcastSelectViewController.h"

@interface ALBroadcastSelectViewController (PullMore)
<ALTableViewControlDelegate>

-(void)checkNewData:(ALTableViewControl *)tableViewControl;
-(void)loadMore:(ALTableViewControl *)tableViewControl;

@end
