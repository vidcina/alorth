//
//  ALBroadcastCollectionViewCell.h
//  alorth
//
//  Created by w91379137 on 2015/10/7.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ALBroadcastCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) NSDictionary *infoDict;

@property (nonatomic, weak) IBOutlet UILabel *ownerNameLabel;
@property (nonatomic, weak) IBOutlet UIImageView *productImageView;
@property (nonatomic, weak) IBOutlet UILabel *productTitleLabel;

- (void)performCellDictionary:(NSDictionary *)dictionary;

@end
