//
//  PLViewController.h
//  PLCameraStreamingKit
//
//  Created on 01/10/2015.
//  Copyright (c) Pili Engineering, Qiniu Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ALBroadcastMainViewController;
#define reloadPeriodGap 15

@interface PLViewController : UIViewController
{
    float reloadPeriod;
    NSTimer *reloadTimer;
    NSInteger isUpdateImageStatus; //0 沒動作 //1 動作中 //2上傳完成
}
-(void)startPreview;

@property (nonatomic, weak) ALBroadcastMainViewController *delegate;

//連線資料
@property (nonatomic, strong) NSString *streamPrefix;
@property (nonatomic, strong) NSString *streamID;
@property (nonatomic, strong) NSString *currentPriority;
@property (nonatomic, strong) NSMutableArray *timeStampArray;

//模擬器返回按鈕
@property (nonatomic, strong) IBOutlet UIButton *backButton;

//上方顯示區
@property (nonatomic, strong) IBOutlet UIView *topControlView;
@property (nonatomic, strong) IBOutlet UILabel *productName;
@property (nonatomic, strong) IBOutlet UIButton *muteButton;
@property (nonatomic, strong) IBOutlet UILabel *streamPeopleNumber;

//下方顯示區
@property (nonatomic, strong) IBOutlet UIView *bottomControlView;
@property (nonatomic, strong) IBOutlet UIView *nextControlView;
@property (nonatomic, strong) IBOutlet UIImageView *nextProductImageView;
@property (nonatomic, strong) IBOutlet UIButton *actionButton;
@property (nonatomic, strong) IBOutlet UIButton *toggleCameraButton;
@property (nonatomic, strong) IBOutlet UIButton *torchButton;

@end
