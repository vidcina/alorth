//
//  PLViewController.m
//  PLCameraStreamingKit
//
//  Created on 01/10/2015.
//  Copyright (c) Pili Engineering, Qiniu Inc. All rights reserved.
//

#import "PLViewController.h"
#import "Reachability.h"
#import "PLCameraStreamingKit.h"

#import "ALBroadcastMainViewController.h"

#import "UIViewController+ImageOnline.h"
#import "LivePublisher.h"


@interface PLViewController ()
<LivePublisherDelegate>
@property (nonatomic, strong) Reachability *internetReachability;
@property (weak, nonatomic) IBOutlet CamPreviewView *cameraPreviewView;
@property (nonatomic) LivePublisher *lp;
@property (nonatomic) bool isStarting;
@property (nonatomic) bool isFlashEnable;

@end


@implementation PLViewController

#pragma mark - VC Life
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.currentPriority = @"1";
        self.timeStampArray = [NSMutableArray array];
        self.internetReachability = [Reachability reachabilityForInternetConnection];
        [self.internetReachability startNotifier];
        

        // 网络状态监控
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(reachabilityChanged:)
                                                     name:kReachabilityChangedNotification
                                                   object:nil];
        
        NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"tmp"];
        BOOL isDir = NO;
        BOOL isExist = [[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:&isDir];
        if (!isExist || !isDir) {
            [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
        }

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    

    //屏幕常亮
    [ [ UIApplication sharedApplication] setIdleTimerDisabled:YES ];
    
    _lp = [[LivePublisher alloc] init]; // 1.
    [_lp setLivePublisherDelegate:self]; // 2.设置事件delegate
    
    //3.设置音频参数，码率32kbps ,HE-AAC
    [_lp setAudioParamBitrate:32*1000 aacProfile:AAC_PROFILE_HE];
    
    /**
     *4.设置视频参数 宽568 高320 fps 15 码率300kbps，main profile
     *  高宽比例推荐使用16:9的分辨率
     *  320X180@15 ~~ 200kbps
     480X272@15 ~~ 250kbps
     568x320@15 ~~ 300kbps
     640X360@15 ~~ 400kbps
     720x405@15 ~~ 500kbps
     854x480@15 ~~ 600kbps
     960x540@15 ~~ 700kbps
     1024x576@15 ~~ 800kbps
     1280x720@15 ~~ 1000kbps
     *  自适应横竖屏发布分辨率，不用反转此处的高宽值
     *  目前为软编码，fps对CPU消耗影响较大，不宜过高
     */
    [_lp setVideoParamWidth:1280 height:720 fps:15 bitrate:400*1000 avcProfile:AVC_PROFILE_MAIN];
    
    //5. 开启背景噪音消除，软件消除算法，有一定CPU消耗
    [_lp setDenoiseEnable:YES];
    
    /*
     * 6. 开始预览摄像头画面，
     * _cameraPreviewView   传入CamPreviewView视图对象
     * [self interfaceOrientation]  传入当前屏幕方向 视频发布的初始方向由此参数确定.也就是说,初始化视图是竖屏,发布的视频既是9:16的竖屏;横屏视图,视频就是16:9的横屏
     * CAMERA_BACK 初始使用后置摄像头, CAMERA_FRONT:前置
     */
    
    
    
    self.actionButton.layer.cornerRadius = 20;
}

-(void)startPreview
{
    [_lp startPreview:_cameraPreviewView interfaceOrientation:[self interfaceOrientation] camId:CAMERA_BACK];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
#if !TARGET_IPHONE_SIMULATOR
    self.backButton.hidden = YES;
#else
    self.backButton.hidden = NO;
#endif
    [self isStreamReady];
}

- (void)viewWillDisappear:(BOOL)animated
{
    if (_lp) {
        [self stopSession];
    }
    
    [super viewWillDisappear:animated];
}

- (void)dealloc
{
    
    [self.internetReachability stopNotifier];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kReachabilityChangedNotification
                                                  object:nil];
}


#pragma mark - Setter
- (void)setStreamPrefix:(NSString *)streamPrefix
{
    _streamPrefix = streamPrefix;
    [self isStreamReady];
}

- (void)setStreamID:(NSString *)streamID
{
    _streamID = streamID;
    [self isStreamReady];
}

- (void)isStreamReady
{
    BOOL isStreamReady = self.streamPrefix && self.streamID;
    self.topControlView.hidden = !isStreamReady;
    self.bottomControlView.hidden = !isStreamReady;
    if (isStreamReady) [self startSession];
}

#pragma mark - timerEvent
- (void)timerEvent:(NSTimer *)sender
{
    reloadPeriod += sender.timeInterval;
    if (reloadPeriod > reloadPeriodGap && self.timeStampArray.count > 0) {
        reloadPeriod = 0;
        
        [ALNetWorkAPI getStreamStatusA057StreamID:self.streamID
                                  CompletionBlock:^(NSDictionary *responseObject) {
                                      [self showAPIInfomation:responseObject];
                                  }];
        
        
    }
}

- (void)didSaveImageAtURL:(NSString *)string
{
    if (string) {
        [ALNetWorkAPI uploadStreamImageA049StreamID:self.streamID
                                        StreamImage:string
                                    CompletionBlock:^(NSDictionary *responseObject) {
                                        isUpdateImageStatus = 0;
                                        if ([ALNetWorkAPI checkSerialNumber:@"A049"
                                                             ResponseObject:responseObject]) {
                                            isUpdateImageStatus = 2;
                                            //ALLog(@"A049 串流圖片成功 %@",responseObject);
                                        }
                                        else {
                                            ALLog(@"A049 更換圖片失敗 %@",responseObject);
                                        }
                                    }];
    }
    else {
        isUpdateImageStatus = 0;
        ALLog(@"上傳圖片失敗");
    }
}

-(void)showAPIInfomation:(NSDictionary *)responseObject
{
    NSString *nextProductImage = responseObject.safe[@"resBody"][@"NextProductImage"];
    
    BOOL isNext = YES;

    if ([nextProductImage isKindOfClass:[NSString class]]) {
        if (nextProductImage.length == 0) {
            isNext = NO;
        }
    }
    else {
        isNext = NO;
    }
    
    if (isNext) {
        [self.nextProductImageView imageFromURLString:nextProductImage];
        self.nextControlView.hidden = NO;;
    }
    else {
        self.nextControlView.hidden = YES;
    }
    
    id productName = responseObject.safe[@"resBody"][@"ProductName"];
    if ([productName isKindOfClass:[NSString class]]) {
        self.productName.numberOfLines = 0;
        self.productName.text = productName;
    }
    
    id viewerNumber = responseObject.safe[@"resBody"][@"ViewerNumber"];
    if ([viewerNumber isKindOfClass:[NSString class]]) {
        self.streamPeopleNumber.text = viewerNumber;
    }
}

#pragma mark - Notification Handler
- (void)reachabilityChanged:(NSNotification *)notif
{
    Reachability *curReach = [notif object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    NetworkStatus status = [curReach currentReachabilityStatus];
    
    if (NotReachable == status) {
        // 对断网情况做处理
        [_lp stopPublish];
    }
    
    const char *networkStatus[] = {
        "Not Reachable",
        "Reachable via WiFi",
        "Reachable via CELL"
    };
    NSLog(@"Networkt Status: %s", networkStatus[status]);
}

#pragma mark - Operation
- (void)startSession
{
    //可能重新連線 會經過一次以上
    NSString *urlString = [[self.streamPrefix stringByReplacingOccurrencesOfString:@"http" withString:@"rtmp"] stringByAppendingString:self.streamID];
    //@"rtmp://52.27.44.52:1935/live/";
    ALLog(@"StartSession:%@",urlString);
    if (!self.streamID) {
        YKSimpleAlert(@"Error");
        return;
    }
    
    self.actionButton.enabled = NO;
    if(_isStarting) {
        return;
//        [_lp stopPublish];
    } else {
        //设置发布视频方向
        //如果不调用，则视频方向为调用预览方法时的界面方向，如果需要指定固定的发布方向，则在开始发布之前调用此方法 (可选方法)
        //        [_lp setVideoOrientation:VIDEO_ORI_PORTRAIT];
        
        //也可用在明确需要横屏16:9 的视频发布 但用户锁定了手机方向旋转，设置参数为：VIDEO_ORI_LANDSCAPE 或 VIDEO_ORI_LANDSCAPE_REVERSE 并提示用户横屏握手机
        //        [_lp setVideoOrientation:VIDEO_ORI_LANDSCAPE];
        
        
        //开始发布 普通模式
        [_lp setMicEnable:YES];
        [_lp startPublish:urlString];
        
        
        //开始发布 需要验证pageUrl swfUrl的发布模式
        //        [_lp startPublishRtmpUrl:[[DefConfig sharedInstance] getPublishUrl] pageUrl:@"http://www.example.com/pageurl" swfUrl:@"http://http://www.exmple.com/pageurl/swfurl.swf"];
    }
    [self tryCaptureImage];
    if (!reloadTimer) {
        reloadTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                       target:self
                                                     selector:@selector(timerEvent:)
                                                     userInfo:nil
                                                      repeats:true];
        reloadPeriod = reloadPeriodGap;
    }
}

- (void)stopSession
{
    [_lp stopPublish];
    [_lp stopPreview];
    _lp = nil;
}

-(void)tryCaptureImage
{
    if (_isStarting) {
        return;
    }
    if (isUpdateImageStatus == 1) {
        return;
    }
    UIImage *capturedScreenshot = nil;
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"tmp"];
    
    NSString *filePath = [path stringByAppendingPathComponent:@"scrnshot"];
    BOOL ret = [_lp capturePicture:filePath];
    if (ret) {
        capturedScreenshot = [UIImage imageWithContentsOfFile:filePath];
    }
    if (capturedScreenshot) {
        [self uploadImage:capturedScreenshot DisplayMB:NO];
        isUpdateImageStatus = 1;
    }
    else {
        ALLog(@"截圖失敗");
        isUpdateImageStatus = 0;
        [self performSelector:@selector(tryCaptureImage) withObject:nil afterDelay:3];
        
    }

}


#pragma mark - IBAction
- (IBAction)back:(id)sender
{
    [reloadTimer invalidate];
    
    if (self.delegate) {
        [self.delegate completeStream];
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)next:(id)sender
{
    if (self.timeStampArray.count == 0) {
        return;
    }
    
    double nowTime = [[NSDate date] timeIntervalSince1970];
    double lastTime = [[self.timeStampArray lastObject] doubleValue];
    
    [self addMBProgressHUDWithKey:@"nextStreamA059"];
    [ALNetWorkAPI nextStreamA059StreamID:self.streamID
                         CurrentPriority:self.currentPriority
                              ElapseTime:[NSString stringWithFormat:@"%.3f",nowTime - lastTime]
                         CompletionBlock:^(NSDictionary *responseObject) {
                             [self removeMBProgressHUDWithKey:@"nextStreamA059"];
                             
                             if ([responseObject.safe[@"resBody"][@"ResultCode"] intValue] == 0) {
                                 self.currentPriority = [NSString stringWithFormat:@"%d",[self.currentPriority intValue] + 1];
                                 [self.timeStampArray addObject:@(nowTime)];
                                 
                                 reloadPeriod += reloadPeriodGap;
                                 [reloadTimer fire];
                                 
                                 ALLog(@"目前播放第 %@ 段 前一段%@秒 %@",self.currentPriority,[NSString stringWithFormat:@"%.3f",nowTime - lastTime],self.timeStampArray);
                             }
                             else {
                                 
                             }
                             
                         }];
}

- (IBAction)actionButtonPressed:(id)sender
{
    if(_isStarting) {
        [self stopSession];
    }
    double nowTime = [[NSDate date] timeIntervalSince1970];
    double startTime = [[self.timeStampArray firstObject] doubleValue];
    
    [self addMBProgressHUDWithKey:@"closeStreamA058"];
    [ALNetWorkAPI closeStreamA058StreamID:self.streamID
                               ElapseTime:[NSString stringWithFormat:@"%.3f",nowTime - startTime]
                          CompletionBlock:^(NSDictionary *responseObject) {
                              [self removeMBProgressHUDWithKey:@"closeStreamA058"];
                              [self back:nil];
                          }];
}

- (IBAction)toggleCameraButtonPressed:(id)sender
{
    [_lp switchCamera];
    
    //切换摄像头操作的同时关闭闪关灯,因为打开前置摄像头无法开闪光灯
    [_lp setFlashEnable:NO];
}

- (IBAction)torchButtonPressed:(id)sender
{
    int ret = 0;
    if(_isFlashEnable) {
        ret =[_lp setFlashEnable:NO];
    }else {
        ret =[_lp setFlashEnable:YES];
    }
    
    if(ret == 1 ) {
        //闪光灯开启
        [sender setImage:[UIImage imageNamed:@"SwitchFlash_on"] forState:UIControlStateNormal];
        _isFlashEnable = YES;
    }else if(ret == 0) {
        //闪光灯关闭
        [sender setImage:[UIImage imageNamed:@"SwitchFlash_off"] forState:UIControlStateNormal];
        _isFlashEnable = NO;
    }else {
        //不支持开关闪光灯
    }
}

- (IBAction)muteButtonPressed:(UIButton *)sender
{
    sender.selected = !sender.selected;
    [_lp setMicEnable:!sender.selected];
}


-(void) onEventCallback:(int)event msg:(NSString *)msg {
    self.actionButton.enabled = YES;
    NSLog(@"onEventCallback:%d %@",event,msg);
    dispatch_async(dispatch_get_main_queue(), ^{
        switch (event) {
            case 2000:
                //发布流开始连接
                break;
            case 2001:
                //发布流连接成功 开始发布
                //_startBtn.selected = YES;
                _isStarting = YES;
                
                if (self.timeStampArray.count == 0) {
                    [self.timeStampArray addObject:@([[NSDate date] timeIntervalSince1970])];
                }
                

                break;
            case 2002:
                //发布流连接失败
                break;
            case 2004:
                //停止发布
                //_startBtn.selected = NO;
                _isStarting = NO;
                break;
            case 2005:
                //发布中遇到网络异常
                break;
            default:
                break;
        }
    });
}
@end
