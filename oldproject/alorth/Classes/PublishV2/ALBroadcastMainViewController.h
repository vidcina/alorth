//
//  ALBroadcastMainViewController.h
//  alorth
//
//  Created by w91379137 on 2015/11/1.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALBasicSubViewController.h"
#import "ALBroadcastSelectViewController.h"
#import "PLViewController.h"

static float BroadcastCameraDelayTime = 1.5f;

@interface ALBroadcastMainViewController : ALBasicSubViewController
{
    ALBroadcastSelectViewController *selectViewController;
    PLViewController *broadcastViewController;
}

- (void)setStreamPrefix:(NSString *)streamPrefix;
- (void)setStreamID:(NSString *)streamID;

- (void)completeStream;

@end
