//
//  ALPublishSwitch.h
//  alorth
//
//  Created by w91379137 on 2015/10/7.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "PDSIBDesignABLEView.h"
typedef NS_OPTIONS(NSUInteger, ALPublishStatus) {
    ListingSell     = 0,
    LiveSell        = 1
};

@interface ALPublishSwitchView : PDSIBDesignABLEView
<ALSelectTypeScrollViewDelegate>

+ (instancetype)sharedInstance;

@property (nonatomic) ALPublishStatus publishStatus;
@property (nonatomic, strong) IBOutlet ALSelectTypeScrollView *selectScrollView;

@end
