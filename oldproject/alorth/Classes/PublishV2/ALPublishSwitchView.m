//
//  ALPublishSwitch.m
//  alorth
//
//  Created by w91379137 on 2015/10/7.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALPublishSwitchView.h"

@implementation ALPublishSwitchView

#pragma mark - Init
+ (instancetype)sharedInstance
{
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] initWithFrame:CGRectZero];
        [sharedInstance initSetting];
    });
    return sharedInstance;
}

- (void)initSetting
{
    _publishStatus = ListingSell;
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@([UIScreen mainScreen].bounds.size.width - 50));
        make.height.equalTo(@64);
    }];
    self.selectScrollView.infoObjectArray = [@[@"Listing sales"
                                               //,@"Broadcast sales"
                                               ] mutableCopy];
}

#pragma mark - ALSelectTypeScrollViewDelegate
-(UIView *)typeViewOfIndex:(NSInteger)index
                infoObject:(NSObject *)infoObject
      selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    if ([infoObject isKindOfClass:[NSString class]]) {
        
        UIButton *aButton =
        [ALSelectTypeScrollView alSelectButton:(NSString *)infoObject];
        
        [aButton addTarget:selectTypeScrollView
                    action:@selector(selectView:)
          forControlEvents:UIControlEventTouchUpInside];
        
        [selectTypeScrollView addSubview:aButton]; //不先加上去 無法執行下一步
        [aButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(selectTypeScrollView.mas_width).multipliedBy(0.5).offset(-1);
        }];

        return aButton;
    }
    return nil;
}

-(void)didSelectIndex:(NSInteger)index
           infoObject:(NSObject *)infoObject
 selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    if (index == 0) {
        self.publishStatus = ListingSell;
    }
    else {
        
        NSArray *target =
        @[@(QuestionCamera),@(QuestionMircrophone),@(QuestionLocation),@(QuestionStripe)];
        
        ALStripeViewController *stripeViewController = [[ALStripeViewController alloc] init];
        stripeViewController.questionTypeArray = target;
        stripeViewController.mainTitleString = ALBroadcastMainTitleString;
        stripeViewController.subTitleString = ALBroadcastSubTitleString;
        
        [[PDSEnvironmentViewController sharedInstance] submitViewController:stripeViewController
                                                            CompletionBlock:^{
                                                                
                                                                if ([ALStripeViewController checkQuestionTypeArray:target]) {
                                                                    [self shouldStartBroadcast];
                                                                }
                                                                else {
                                                                    NSLog(@"沒有通過");
                                                                    [selectTypeScrollView selectIndex:0];
                                                                }
                                                            }];
    }
}

-(void)adjustSelectView:(UIView *)view
             infoObject:(NSObject *)infoObject
               selected:(BOOL)selected
   selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    UIButton *button = (UIButton *)view;
    if (selected) {
        //選到
        button.selected = YES;
        button.backgroundColor = kALProductDetailDidselectBgColor;
        [button setTitleColor:kALProductDetailDidselectFontColor forState:UIControlStateNormal];
    }
    else {
        //沒
        button.selected = NO;
        button.backgroundColor = [UIColor clearColor];
        [button setTitleColor:kALProductDetailUnselectFontColor forState:UIControlStateNormal];
    }
}

#pragma mark - check video count
- (void)shouldStartBroadcast
{
    [self addMBProgressHUDWithKey:@"userStreamVideoStatusA078"];
    [ALNetWorkAPI userStreamVideoStatusA078CompletionBlock:^(NSDictionary *responseObject) {
        [self removeMBProgressHUDWithKey:@"userStreamVideoStatusA078"];
        
        if ([ALNetWorkAPI checkSerialNumber:@"A078"
                             ResponseObject:responseObject]) {
            
            ALLog(@"isBroadcast : %@ ,CountStream : %@",
                  responseObject.safe[@"resBody"][@"isBroadcast"],
                  responseObject.safe[@"resBody"][@"CountStream"]);
            
            if ([responseObject.safe[@"resBody"][@"isBroadcast"] intValue] == 1) {
                self.publishStatus = LiveSell;
            }
            else {
                
                //這邊已經沒用了 kobe 只要數目超過 上限 就會 errorCode 81
                [self.selectScrollView selectIndex:0];
                //YKSimpleAlert(@"You have too many videos, delete some to continue.");
                YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A078", responseObject),nil));
            }
        }
        else {
            [self.selectScrollView selectIndex:0];
            YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A078", responseObject),nil));
        }
    }];
}

@end
