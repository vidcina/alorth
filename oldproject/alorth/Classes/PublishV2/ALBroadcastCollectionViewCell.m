//
//  ALBroadcastCollectionViewCell.m
//  alorth
//
//  Created by w91379137 on 2015/10/7.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBroadcastCollectionViewCell.h"

@implementation ALBroadcastCollectionViewCell

- (void)performCellDictionary:(NSDictionary *)dictionary
{
    _infoDict = dictionary;
    
    {//名稱
        self.ownerNameLabel.text =
        [ALFormatter preferredDisplayNameFirst:dictionary[@"ProductOwnerFirstName"]
                                          Last:dictionary[@"ProductOwnerLastName"]
                                        UserID:dictionary[@"ProductOwner"]];
        
        self.ownerNameLabel.layer.shadowOpacity = 0.2;
        self.ownerNameLabel.layer.shadowRadius = 2;
        self.ownerNameLabel.layer.shadowOffset = CGSizeMake(1, 1);
    }
    
    [self.productImageView imageFromURLString:self.infoDict[@"ProductImage"]];
    
    if ([self.infoDict[@"ProductName"] isKindOfClass:[NSString class]]) {
        self.productTitleLabel.text = self.infoDict[@"ProductName"];
    }
    else {
        self.productTitleLabel.text = @"";
    }
}

@end
