//
//  ALComplexStreamProductViewController+PullMore.h
//  alorth
//
//  Created by w91379137 on 2016/2/4.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALStreamProductListViewController.h"

@interface ALStreamProductListViewController (PullMore)
<ALTableViewControlDelegate>

- (BOOL)isSameParameters;
- (void)checkNewData;
- (void)checkNewData:(ALTableViewControl *)tableViewControl;
- (void)loadMore:(ALTableViewControl *)tableViewControl;

@end
