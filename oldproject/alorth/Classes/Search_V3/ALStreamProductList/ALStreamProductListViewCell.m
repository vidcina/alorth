//
//  ALComplexStreamProductViewCell.m
//  alorth
//
//  Created by w91379137 on 2016/2/4.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALStreamProductListViewCell.h"
#import "ALProductTableViewCell.h"
#import "ALStreamBigCell.h"

@interface ALStreamProductListViewCell()
{
    UITableViewCell *cellView;
    UIView *contentViewFromOther;
}

@property (nonatomic, strong) ALProductTableViewCell *productCell;
@property (nonatomic, strong) ALStreamBigCell *streamBigCell;

@end

@implementation ALStreamProductListViewCell

#pragma mark - Setter / Getter
- (ALProductTableViewCell *)productCell
{
    if (_productCell == nil) {
        _productCell = [ALProductTableViewCell cell];
    }
    return _productCell;
}

- (ALStreamBigCell *)streamBigCell
{
    if (_streamBigCell == nil) {
        _streamBigCell = [ALStreamBigCell cellFromXib];
    }
    return _streamBigCell;
}

- (void)setInfoDict:(NSDictionary *)infoDict
{
    _infoDict = infoDict;
    
    if (self.delegate.typeMode != ALFilterTypeModeProduct) _productCell = nil;
    if (self.delegate.typeMode != ALFilterTypeModeStreaming) _streamBigCell = nil;
    
    [contentViewFromOther removeFromSuperview];
    if (infoDict) {
    
        switch (self.delegate.typeMode) {
            case ALFilterTypeModeProduct:
            {
                [ALProductTableViewCell settingCell:self.productCell WithInfoDict:infoDict];
                cellView = self.productCell;
            }break;
                
            case ALFilterTypeModeStreaming:
            {
                [self.streamBigCell performWithInfoDict:infoDict];
                cellView = self.streamBigCell;
            }break;
                
            default:break;
        }
    }
    
    if (contentViewFromOther != cellView.contentView) {
        [contentViewFromOther removeFromSuperview];
    }
    contentViewFromOther = cellView.contentView;
    if (contentViewFromOther) {
        [self.contentView addSubview:contentViewFromOther];
        [contentViewFromOther mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView);
        }];
    }
}

- (void)setIsPlay:(BOOL)isPlay
{
    ALProductTableViewCell *cell = maybe(cellView, ALProductTableViewCell);
    cell.isPlay = isPlay;
}

#pragma mark - PrepareForReuse
- (void)prepareForReuse
{
    _productCell.isPlay = NO;
    _productCell.isVoice = NO;
    _productCell.goodImageView.hidden = NO;
    
    for (UIView *view in self.contentView.subviews) {
        [view removeFromSuperview];
    }
}

#pragma mark - Size
- (CGSize)sizeForRow
{
    int numbers = 1;
    switch (self.delegate.sizeType) {
        case ALComplexSPVCSize1: numbers = 1; break;
        case ALComplexSPVCSize2: numbers = 2; break;
        default: break;
    }
    
    int cellLenth = [UIScreen mainScreen].bounds.size.width / numbers;
    
    switch (self.delegate.typeMode) {
        case ALFilterTypeModeProduct:
        {
            //float height = [ALProductTableViewCell settingCell:nil WithInfoDict:nil];
            return CGSizeMake(cellLenth, cellLenth + 50);
        }
        case ALFilterTypeModeStreaming:
        {
            switch (self.delegate.sizeType) {
                case ALComplexSPVCSize1:
                    return CGSizeMake (cellLenth ,[self.streamBigCell performWithInfoDict:self.infoDict] / numbers);
                case ALComplexSPVCSize2:
                    return CGSizeMake (cellLenth ,cellLenth * 16 / 9);
                default: break;
            }
        }
        default:break;
    }
    return CGSizeZero;
}

#pragma mark - All Video Control
+ (void)detectCenterCellToPlay:(UICollectionView *)collectionView
{
    float centerY = collectionView.frame.size.height / 2 + collectionView.contentOffset.y;
    
    UICollectionViewCell *theCenterCell;
    for(UICollectionViewCell *cell in collectionView.visibleCells) {
        //NSLog(@"%@",NSStringFromCGPoint(CGPointMake(view.frame.origin.x, view.frame.origin.y)));
        //NSLog(@"%@",NSStringFromCGPoint(CGPointMake(scrollView.contentOffset.x, scrollView.contentOffset.y)));
        //NSLog(@"%@",NSStringFromCGPoint(CGPointMake(view.frame.origin.x, view.frame.origin.y)));
        
        if (theCenterCell != nil) {
            float theLast = fabs(theCenterCell.center.y - centerY);
            float theNext = fabs(cell.center.y - centerY);
            if (theLast > theNext) {
                theCenterCell = cell;
            }
        }
        else {
            theCenterCell = cell;
        }
    }
    
    for (UICollectionViewCell *cell in collectionView.visibleCells) {
        if ([cell isKindOfClass:[ALStreamProductListViewCell class]]) {
            ALStreamProductListViewCell *cellx = (ALStreamProductListViewCell *)cell;
            if (cell == theCenterCell) {
                cellx.isPlay = YES;
            }
            else {
                cellx.isPlay = NO;
            }
        }
    }
}

+ (void)stopAllPlayCell:(UICollectionView *)collectionView
{
    for (UICollectionViewCell *cell in collectionView.visibleCells) {
        if ([cell isKindOfClass:[ALStreamProductListViewCell class]]) {
            ALStreamProductListViewCell *cellx = (ALStreamProductListViewCell *)cell;
            cellx.isPlay = NO;
        }
    }
}

@end
