//
//  ALComplexStreamProductViewController.h
//  alorth
//
//  Created by w91379137 on 2016/2/4.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "BFBasicPartViewController.h"
#import "ALFilter.h"
#import "PinterestLayout.h"
#import "ALStreamProductListViewCell.h"

@protocol ALStreamProductListViewControllerDelegate <NSObject>

- (NSString *)keyword;
- (NSInteger)typeMode;
- (NSInteger)sortMode;
- (NSArray *)languages;
- (NSArray *)categories;
- (NSArray *)locations;

@end

@interface ALStreamProductListViewController : BFBasicPartViewController
<UICollectionViewDataSource, UICollectionViewDelegate,
PinterestLayoutDelegate>
{
    NSMutableArray *objectListArray;            //列表
    BOOL willReplaceWithNew;                    //是否清空之前資料
    ALTableViewControl *currentTableViewControl;//控制器
}
@property (nonatomic, weak) id<ALStreamProductListViewControllerDelegate> delegate;
@property (nonatomic, strong) IBOutlet UICollectionView *mainCollectionView;

@property (nonatomic) ALFilterTypeMode typeMode;
@property (nonatomic) ALComplexSPVCSize sizeType;

- (NSArray *)objectListArray;

@end
