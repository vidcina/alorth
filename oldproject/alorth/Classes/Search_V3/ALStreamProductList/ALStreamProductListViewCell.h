//
//  ALComplexStreamProductViewCell.h
//  alorth
//
//  Created by w91379137 on 2016/2/4.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ALFilter.h"

@protocol ALStreamProductListViewCellDelegate <NSObject>

- (ALFilterTypeMode)typeMode;
- (ALComplexSPVCSize)sizeType;

@end

@interface ALStreamProductListViewCell : UICollectionViewCell
{
    
}

@property (nonatomic, weak) id<ALStreamProductListViewCellDelegate> delegate;
@property (nonatomic, strong) NSDictionary *infoDict;
@property (nonatomic) BOOL isPlay;

- (CGSize)sizeForRow;

#pragma mark - All Video Control
+ (void)detectCenterCellToPlay:(UICollectionView *)collectionView;
+ (void)stopAllPlayCell:(UICollectionView *)collectionView;

@end
