//
//  ALComplexStreamProductViewController+PullMore.m
//  alorth
//
//  Created by w91379137 on 2016/2/4.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALStreamProductListViewController+PullMore.h"
#define APITypeModeKey @"TypeMode"
#define APISortModeKey @"SortMode"

@implementation ALStreamProductListViewController (PullMore)

static NSString *streamProductListViewLoading = @"streamProductListViewLoading";
static NSString *searchStreamingA090 = @"searchStreamingA090";
static NSString *searchProductA091 = @"searchProductA091";

#pragma mark - Parameter Method
- (BOOL)isSameParameters
{
    if (![self.delegate.keyword isEqualToString:currentTableViewControl.lastDict[@"Keyword"]]) return NO;
    if (self.delegate.typeMode != [currentTableViewControl.lastDict[APITypeModeKey] integerValue]) return NO;
    if (self.delegate.sortMode != [currentTableViewControl.lastDict[APISortModeKey] integerValue]) return NO;
    if (![self isSameContainOfArray1:self.delegate.languages Array2:currentTableViewControl.lastDict[@"Language"]]) return NO;
    if (![self isSameContainOfArray1:self.delegate.categories Array2:currentTableViewControl.lastDict[@"Category"]]) return NO;
    if (![self isSameContainOfArray1:self.delegate.locations Array2:currentTableViewControl.lastDict[@"Location"]]) return NO;
    return YES;
}

- (BOOL)isSameContainOfArray1:(NSArray *)array1 Array2:(NSArray *)array2
{
    NSMutableArray *test1 = [array1 mutableCopy];
    [test1 removeObjectsInArray:array2];
    if (test1.count > 0) return NO;
    
    NSMutableArray *test2 = [array2 mutableCopy];
    [test2 removeObjectsInArray:array1];
    if (test2.count > 0) return NO;
    
    return YES;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.mainCollectionView) {
        [currentTableViewControl scrollViewDidScroll:scrollView];
        [ALStreamProductListViewCell detectCenterCellToPlay:self.mainCollectionView];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView == self.mainCollectionView) {
        [currentTableViewControl scrollViewDidEndDragging:scrollView willDecelerate:(BOOL)decelerate];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.mainCollectionView) {
        [currentTableViewControl scrollViewDidEndDecelerating:(UIScrollView *)scrollView];
    }
}

#pragma mark - ALTableViewControlDelegate
- (void)checkNewData
{
    [self checkNewData:currentTableViewControl];
}

- (void)checkNewData:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    willReplaceWithNew = YES;
    tableViewControl.lastDict[@"StartNum"] = @"0";
    
    if (self.delegate.keyword) {
        tableViewControl.lastDict[@"Keyword"] = [self.delegate.keyword copy];
    }
    else {
        tableViewControl.lastDict[@"Keyword"] = @"";
    }
    tableViewControl.lastDict[APITypeModeKey] = @(self.delegate.typeMode);
    tableViewControl.lastDict[APISortModeKey] = @(self.delegate.sortMode);
    
    tableViewControl.lastDict[@"Language"] = [self.delegate.languages copy];
    tableViewControl.lastDict[@"Category"] = [self.delegate.categories copy];
    tableViewControl.lastDict[@"Location"] = [self.delegate.locations copy];
    
    ALLog(@"重新整理");
    [self addMBProgressHUDWithKey:streamProductListViewLoading];
    [self listLoadApiSourceParameters:tableViewControl.lastDict
                          FinishBlock:^(BOOL isSuccess) {
                              [self removeMBProgressHUDWithKey:streamProductListViewLoading];
                          }];
}

- (void)loadMore:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    
    ALLog(@"載入更多:目前第 %@ 頁 %lu 筆資料",tableViewControl.lastDict[@"StartNum"],(unsigned long)objectListArray.count);
    
    tableViewControl.lastDict[@"StartNum"] =
    [NSString stringWithFormat:@"%d",[tableViewControl.lastDict[@"StartNum"] intValue] + 1];
    
    [self addMBProgressHUDWithKey:streamProductListViewLoading];
    [self listLoadApiSourceParameters:tableViewControl.lastDict
                          FinishBlock:^(BOOL isSuccess) {
                              [self removeMBProgressHUDWithKey:streamProductListViewLoading];
                          }];
}

#pragma mark - API
- (void)listLoadApiSourceParameters:(NSDictionary *)parameters
                       FinishBlock:(void (^)(BOOL isSuccess))finishBlockToRun
{    
    [ALStreamProductListViewCell stopAllPlayCell:self.mainCollectionView];
    
    if ([parameters[APITypeModeKey] integerValue] == ALFilterTypeModeStreaming) {
        [ALNetWorkAPI searchStreamingA090StartNum:parameters[@"StartNum"]
                                          Keyword:parameters[@"Keyword"]
                                             Sort:[NSString stringWithFormat:@"%ld",[parameters[APISortModeKey] integerValue]]
                                         Language:parameters[@"Language"]
                                         Category:parameters[@"Category"]
                                         Location:parameters[@"Location"]
                                  CompletionBlock:^(NSDictionary *responseObject) {
                                      BOOL isValid =
                                      [self checkListDataA090:responseObject];
                                      
                                      if (!isValid) ALLog(@"%@ 錯誤",searchStreamingA090);
                                      finishBlockToRun(isValid);
                                  }];
    }
    else if ([parameters[APITypeModeKey] integerValue] == ALFilterTypeModeProduct) {
        [ALNetWorkAPI searchProductA091StartNum:parameters[@"StartNum"]
                                        Keyword:parameters[@"Keyword"]
                                           Sort:[NSString stringWithFormat:@"%ld",[parameters[APISortModeKey] integerValue]]
                                       Category:parameters[@"Category"]
                                CompletionBlock:^(NSDictionary *responseObject) {
                                    BOOL isValid =
                                    [self checkListDataA091:responseObject];
                                    
                                    if (!isValid) ALLog(@"%@ 錯誤",searchProductA091);
                                    finishBlockToRun(isValid);
                                    
                                    [ALStreamProductListViewCell detectCenterCellToPlay:self.mainCollectionView];
                                }];
    }
}

- (BOOL)checkListDataA090:(NSDictionary *)responseObject
{
    NSArray *addArray = nil;
    if ([ALNetWorkAPI checkSerialNumber:@"A090"
                         ResponseObject:responseObject]) {
        if (responseObject.safe[@"resBody"][@"AllList"])
            addArray = maybe(responseObject[@"resBody"][@"AllList"], NSArray);
    }
    
    if (willReplaceWithNew) {
        [self.mainCollectionView setContentOffset:CGPointZero animated:NO];
        [currentTableViewControl.apiDataArray removeAllObjects];
        willReplaceWithNew = NO;
    }
    
    {
        [currentTableViewControl.apiDataArray addObjectsFromArray:addArray];
        self.typeMode = [currentTableViewControl.lastDict[APITypeModeKey] integerValue];
        [self.mainCollectionView reloadData];
    }
    return addArray != nil;
}

- (BOOL)checkListDataA091:(NSDictionary *)responseObject
{
    NSArray *addArray = nil;
    if ([ALNetWorkAPI checkSerialNumber:@"A091"
                         ResponseObject:responseObject]) {
        
        if (responseObject.safe[@"resBody"][@"AllList"])
            addArray = maybe(responseObject[@"resBody"][@"AllList"], NSArray);
    }
    
    if (willReplaceWithNew) {
        [self.mainCollectionView setContentOffset:CGPointZero animated:NO];
        [currentTableViewControl.apiDataArray removeAllObjects];
        willReplaceWithNew = NO;
    }
    
    {
        [currentTableViewControl.apiDataArray addObjectsFromArray:addArray];
        self.typeMode = [currentTableViewControl.lastDict[APITypeModeKey] integerValue];
        [self.mainCollectionView reloadData];
    }
    
    return addArray != nil;
}

@end
