//
//  ALComplexStreamProductViewController.m
//  alorth
//
//  Created by w91379137 on 2016/2/4.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALStreamProductListViewController.h"
#import "ALStreamProductListViewController+PullMore.h"

#import "ALProductDetailViewController.h"
#import <ImageEdit/UIImage+SimpleEdit.h>

static NSString *kCollectionViewCellID = @"CollectionViewCellID";
static NSString *kCollectionViewCellID2 = @"CollectionViewCellID2";

@interface ALStreamProductListViewController ()
<ALStreamProductListViewCellDelegate>
{
    ALStreamProductListViewCell *demoCell;
}

@end

@implementation ALStreamProductListViewController

#pragma mark - Init
- (instancetype)init
{
    self = [super init];
    if (self) {
        objectListArray = [NSMutableArray array];
        
        currentTableViewControl = [[ALTableViewControl alloc] init];
        currentTableViewControl.delegate = self;
        currentTableViewControl.apiDataArray = objectListArray;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSArray *collectionViews = @[self.mainCollectionView];
    for (UICollectionView *collectionView in collectionViews) {
        [collectionView registerNib:
         [UINib nibWithNibName:NSStringFromClass([ALStreamProductListViewCell class])
                        bundle:nil]
         forCellWithReuseIdentifier:kCollectionViewCellID];
        [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:kCollectionViewCellID2];
    }
    
    self.typeMode = ALFilterTypeModeProduct;
    self.sizeType = ALComplexSPVCSize2;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [ALStreamProductListViewCell detectCenterCellToPlay:self.mainCollectionView];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [ALStreamProductListViewCell stopAllPlayCell:self.mainCollectionView];
    [super viewDidDisappear:animated];
}

#pragma mark - Setter / Getter
- (void)setTypeMode:(ALFilterTypeMode)typeMode
{
    _typeMode = typeMode;
    [self computeSize];
}

- (void)setSizeType:(ALComplexSPVCSize)sizeType
{
    _sizeType = sizeType;
    
    [ALStreamProductListViewCell stopAllPlayCell:self.mainCollectionView];
    [self computeSize];
    [ALStreamProductListViewCell detectCenterCellToPlay:self.mainCollectionView];
}

- (NSArray *)objectListArray
{
    return objectListArray;
}

- (void)computeSize
{
    PinterestLayout *layout =
    (PinterestLayout *)self.mainCollectionView.collectionViewLayout;
    
    switch (self.sizeType) {
        case ALComplexSPVCSize1: {
            layout.numberOfColumns = 1;
            layout.cellPadding = 0.0;
        } break;
            
        case ALComplexSPVCSize2: {
            layout.numberOfColumns = 2;
            layout.cellPadding = 1.0;
        } break;
            
        default: break;
    }
    
    [layout.cache removeAllObjects];
    [self.mainCollectionView reloadData];
//    [UIView animateKeyframesWithDuration:0.25
//                                   delay:0.0
//                                 options:UIViewKeyframeAnimationOptionBeginFromCurrentState
//                              animations:^{
//                                  [self.mainCollectionView layoutIfNeeded];
//                                  //[self.mainCollectionView reloadData];
//                              } completion:^(BOOL finished) {
//                                  
//                              }];
}

#pragma mark - UICollectionViewDataSource Cell
- (NSInteger)collectionView:(UICollectionView *)theCollectionView
     numberOfItemsInSection:(NSInteger)theSectionIndex
{
    switch (self.sizeType) {
        case ALComplexSPVCSize1: return objectListArray.count;
        case ALComplexSPVCSize2: return objectListArray.count > 0 ? objectListArray.count + 2 : objectListArray.count; //零個不加
        default: break;
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = 0;
    if (self.sizeType == ALComplexSPVCSize1) {
        index = indexPath.row;
    }
    else if (self.sizeType == ALComplexSPVCSize2) {
        
        if ([self isEmptyBlackCell:indexPath.row]) {
            UICollectionViewCell *cell =
            [collectionView dequeueReusableCellWithReuseIdentifier:kCollectionViewCellID2 forIndexPath:indexPath];
            cell.backgroundColor = [UIColor blackColor];

            static int kBlankCellImageViewTag = 2;
            [[cell viewWithTag:kBlankCellImageViewTag] removeFromSuperview];
            //if (![cell viewWithTag:kBlankCellImageViewTag]) {
#pragma mark V1商品與V2商品cell大小不一樣
                if (self.typeMode == ALFilterTypeModeStreaming) {
                    UIImage *image = [UIImage imageNamed:@"blankBlock"];
                    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
                    imageView.contentMode = UIViewContentModeScaleAspectFill;
                    [cell.contentView addSubview:imageView];
                    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.edges.equalTo(cell.contentView);
                    }];
                }
                else {
                    UIImage *image = [UIImage imageNamed:@"blankBlock"];
                    UIImage *croppedImage = [image subImageInRect:CGRectMake(0, 84, 656, 450)];
                    UIImageView *imageView = [[UIImageView alloc] initWithImage:croppedImage];
                    imageView.backgroundColor = [UIColor whiteColor];
                    imageView.contentMode = UIViewContentModeScaleAspectFit;
                    [cell.contentView addSubview:imageView];
                    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.edges.equalTo(cell.contentView);
                    }];
                }
                
            //}
            
            return cell;
        }
        
        index = indexPath.row == 0 ? 0 : indexPath.row - 1;
    }
    
    ALStreamProductListViewCell *cell =
    [collectionView dequeueReusableCellWithReuseIdentifier:kCollectionViewCellID
                                              forIndexPath:indexPath];
    
    cell.delegate = self;
    cell.infoDict = objectListArray[index];
    
    [cell layoutSubviews];
    return cell;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (![[collectionView cellForItemAtIndexPath:indexPath] isKindOfClass:[ALStreamProductListViewCell class]]) {
        return;
    }
    
    ALStreamProductListViewCell *cell =
    (ALStreamProductListViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    switch (self.typeMode) {
        case ALFilterTypeModeProduct:
        {
            //v1
            ALProductDetailViewController *vc =
            [[ALProductDetailViewController alloc] initWithInfoDict:cell.infoDict];
            [self.navigationController pushViewController:vc animated:YES];
        }
            
            break;
        case ALFilterTypeModeStreaming:
        {
            //v2
            [self addMBProgressHUDWithKey:@"streamInfomationA066"];
            [ALNetWorkAPI streamInfomationA066StreamID:cell.infoDict.safe[@"StreamID"]
                                       CompletionBlock:^(NSDictionary *responseObject) {
                                           [self removeMBProgressHUDWithKey:@"streamInfomationA066"];
                                           
                                           if ([ALNetWorkAPI checkSerialNumber:@"A066"
                                                                ResponseObject:responseObject]) {
                                               [self playStreamVideo:responseObject[@"resBody"]];
                                           }
                                           else {
                                               YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A066", responseObject),nil));
                                           }
                                       }];
        }
            
            break;
            
        default:
            break;
    }
}

#pragma mark - PinterestLayoutDelegate
- (CGFloat)collectionView:(UICollectionView *)collectionView
       HeightForIndexPath:(NSIndexPath *)indexPath
                    Width:(CGFloat)width
{
    if (!demoCell) {
        demoCell = [[ALStreamProductListViewCell alloc] init];
        demoCell.delegate = self;
    }
    
    if ([self isEmptyBlackCell:indexPath.item]) return [demoCell sizeForRow].height / 2;
    return [demoCell sizeForRow].height;
}

- (BOOL)isEmptyBlackCell:(NSInteger)index
{
    if (self.sizeType == ALComplexSPVCSize2) {
        if (self.sizeType == ALComplexSPVCSize2) {
            if (index == 1) {
                return YES;
            }
            if (index == objectListArray.count + 1) {
                return YES;
            }
        }
    }
    return NO;
}

@end
