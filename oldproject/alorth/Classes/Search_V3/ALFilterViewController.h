//
//  ALFilterViewController.h
//  alorth
//
//  Created by w91379137 on 2016/2/3.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALAlertViewController.h"
#import "ALFilter.h"
typedef NS_ENUM(NSUInteger, ALFilterSection) {
    ALFilterSectionType,
    ALFilterSectionSort,
    ALFilterSectionLanguage,
    ALFilterSectionCategory,
    ALFilterSectionLocation,
    ALFilterSectionEnd
};

@protocol ALFilterViewControllerDelegate <NSObject>

- (BOOL)isEnabledSection:(ALFilterSection)section;

- (BOOL)isSelectOfSection:(ALFilterSection)section
                    Index:(NSInteger)index
                KeyString:(NSString *)keyString;

- (void)clickOfSection:(ALFilterSection)section
                 Index:(NSInteger)index
             KeyString:(NSString *)keyString;

//全選相關
- (void)selectAllSection:(ALFilterSection)section IsSelect:(BOOL)isSelect;
- (BOOL)isAllSelect:(ALFilterSection)section;

@end

@interface ALFilterViewController : ALAlertViewController
<UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UIView *baseView;
}

@property(nonatomic, weak) id<ALFilterViewControllerDelegate> delegate;
@property(nonatomic, strong) IBOutlet UITableView *mainTableView;

@end

@interface ALFilterViewControllerCell : UITableViewCell

@property(nonatomic, strong) NSString *keyString;

@end
