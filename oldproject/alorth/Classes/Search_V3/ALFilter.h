//
//  ALFilter.h
//  alorth
//
//  Created by w91379137 on 2016/2/4.
//  Copyright © 2016年 w91379137. All rights reserved.
//

typedef NS_ENUM(NSUInteger, ALFilterTypeMode) {
    ALFilterTypeModeStreaming,
    ALFilterTypeModeProduct,
    ALFilterTypeModeEnd
};

typedef NS_ENUM(NSUInteger, ALFilterSortMode) {
    ALFilterSortModeRelevance,
    ALFilterSortModeLowToHigh,
    ALFilterSortModeHighToLow,
    ALFilterSortModeEnd
};

typedef NS_ENUM(NSUInteger, ALComplexSPVCSize) {
    ALComplexSPVCSize1,
    ALComplexSPVCSize2
};
