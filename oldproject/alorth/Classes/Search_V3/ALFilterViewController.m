//
//  ALFilterViewController.m
//  alorth
//
//  Created by w91379137 on 2016/2/3.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALFilterViewController.h"

@interface ALFilterViewController ()

@end

@implementation ALFilterViewController

#pragma mark - VC Life
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.type = ALAlertViewTypeRight;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self alTableViewSetting:self.mainTableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    backButton.backgroundColor = [UIColor clearColor];
    [baseView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@([UIScreen mainScreen].bounds.size.height));
        make.width.equalTo(@([UIScreen mainScreen].bounds.size.width * 0.6));
    }];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (backButton.enabled == NO) {
        [UIView animateWithDuration:ALAlertDuration
                              delay:0.0
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             backButton.alpha = 1;
                         } completion:^(BOOL finished) {
                             backButton.enabled = YES;
                             [self sendViewToCenter:baseView];
                         }];
    }
}

#pragma mark - IBAction
- (IBAction)backAction:(id)sender
{
    if (backButton.enabled == YES) {
        backButton.enabled = NO;
        [self sendViewToCenter:nil];
        [UIView animateWithDuration:ALAlertDuration
                              delay:0.0
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             backButton.alpha = 0;
                         } completion:^(BOOL finished) {
                             [self.navigationController popViewControllerAnimated:YES];
                         }];
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)tableViewSelectAll:(UIButton *)sender
{
    ALFilterSection section = sender.tag;
    
    //實作把那個區域 全選 反選
    BOOL isMark = [self.delegate isAllSelect:section];
    [self.delegate selectAllSection:section IsSelect:!isMark];
    
    //表現狀態
    [self.mainTableView reloadData];
}

#pragma mark - UITableViewDataSource Section
static float const ALFilterSectionHeight = 40.0f;

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return ALFilterSectionEnd;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (![self.delegate isEnabledSection:section]) return 0;
    switch ((ALFilterSection)section) {
        case ALFilterSectionType:
        case ALFilterSectionSort:
            return ALFilterSectionHeight;
        case ALFilterSectionLanguage:
        case ALFilterSectionCategory:
        case ALFilterSectionLocation:
            return ALFilterSectionHeight + ALFilterRowHeight;
        default:break;
    }
    
    return ALFilterSectionHeight;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = COMMON_GRAY2_COLOR;
    
    {
        UILabel *label = [[UILabel alloc] init];
        label.textColor = [UIColor whiteColor];
        switch ((ALFilterSection)section) {
            case ALFilterSectionType: label.localText = @"Type"; break;
            case ALFilterSectionSort: label.localText = @"Sort"; break;
            case ALFilterSectionLanguage: label.localText = @"Language"; break;
            case ALFilterSectionCategory: label.localText = @"Category"; break;
            case ALFilterSectionLocation: label.localText = @"Location"; break;
            default:break;
        }
        
        [view addSubview:label];
        [label mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(view.mas_top);
            make.leading.equalTo(view.mas_leading).offset(15);
            make.trailing.equalTo(view.mas_trailing);
            make.height.equalTo(@(ALFilterSectionHeight));
        }];
    }
    
    if (section == ALFilterSectionLanguage ||
        section == ALFilterSectionCategory ||
        section == ALFilterSectionLocation ) {
        
        UIButton *whiteButton =
        [[UIButton alloc] initWithFrame:CGRectZero];
        whiteButton.backgroundColor = [UIColor whiteColor];
        [view addSubview:whiteButton];
        [whiteButton mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(view.mas_bottom).offset(-1);
            make.leading.equalTo(view.mas_leading);
            make.trailing.equalTo(view.mas_trailing);
            make.height.equalTo(@(ALFilterRowHeight - 1));
        }];
        whiteButton.tag = section;
        [whiteButton addTarget:self action:@selector(tableViewSelectAll:) forControlEvents:UIControlEventTouchUpInside];
        
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@""];
        
        [view addSubview:cell];
        [cell mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(view.mas_bottom);
            make.leading.equalTo(view.mas_leading);
            make.trailing.equalTo(view.mas_trailing).offset(-6);
            make.height.equalTo(@(ALFilterRowHeight));
        }];
        
        cell.textLabel.localText = @"Select all";
        cell.userInteractionEnabled = NO;
        cell.accessoryType = [self.delegate isAllSelect:section] ?
        UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    }
    return view;
}

#pragma mark - UITableViewDataSource Row
static float const ALFilterRowHeight = 40.0f;

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (![self.delegate isEnabledSection:section]) return 0;
    
    switch ((ALFilterSection)section) {
        case ALFilterSectionType: return ALFilterTypeModeEnd;
        case ALFilterSectionSort: return ALFilterSortModeEnd;
        case ALFilterSectionLanguage: return ALBundleTextResources.languageArray.count;
        case ALFilterSectionCategory: return ALBundleTextResources.categoryArray.count;
        case ALFilterSectionLocation: return ALBundleTextResources.locationArray.count;
        default: return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellTitle = nil;
    NSString *cellKeyString = nil;
    UIImage *cellImage = nil;
    
    switch ((ALFilterSection)indexPath.section) {
        case ALFilterSectionType: {
            switch ((ALFilterTypeMode)indexPath.row) {
                case ALFilterTypeModeStreaming:
                {
                    cellTitle = @"Streaming";
                    cellImage = [UIImage imageNamed:@"home_stream"];
                }
                    break;
                case ALFilterTypeModeProduct: {
                    cellTitle = @"Product";
                    cellImage = [UIImage imageNamed:@"Product_Box"];
                }
                     break;
                default: break;
            }
        }break;
            
        case ALFilterSectionSort: {
            switch ((ALFilterSortMode)indexPath.row) {
                case ALFilterSortModeRelevance: cellTitle = @"Relevance"; break;
                case ALFilterSortModeLowToHigh: cellTitle = @"Price(low to high)"; break;
                case ALFilterSortModeHighToLow: cellTitle = @"Price(high to low)"; break;
                default: break;
            }
        }break;
            
        case ALFilterSectionLanguage: {
            cellTitle = ALBundleTextResources.languageArray.safe[indexPath.row][@"LanguageMap"];
            cellKeyString = ALBundleTextResources.languageArray.safe[indexPath.row][@"LanguageIndex"];
        }break;
            
        case ALFilterSectionCategory: {
            cellTitle = ALBundleTextResources.categoryArray.safe[indexPath.row][@"CategoryEn"];
            cellKeyString = ALBundleTextResources.categoryArray.safe[indexPath.row][@"CategoryIndex"];
        }break;
            
        case ALFilterSectionLocation: {
            cellTitle = ALBundleTextResources.locationArray.safe[indexPath.row][@"LocationCountry"];
            cellKeyString = ALBundleTextResources.locationArray.safe[indexPath.row][@"LocationIndex"];
        }break;
            
        default:break;
    }
    
    ALFilterViewControllerCell *cell =
    [[ALFilterViewControllerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@""];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.textLabel.localText = cellTitle;
    cell.keyString = cellKeyString;
    if (cellImage) {
        cell.imageView.image = cellImage;
    }
    cell.accessoryType =
    [self.delegate isSelectOfSection:indexPath.section Index:indexPath.row KeyString:cell.keyString] ?
    UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return ALFilterRowHeight;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ALFilterViewControllerCell *cell =
    (ALFilterViewControllerCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    [self.delegate clickOfSection:indexPath.section
                            Index:indexPath.row
                        KeyString:cell.keyString];
    
    [tableView reloadData];
}

@end

@implementation ALFilterViewControllerCell

@end
