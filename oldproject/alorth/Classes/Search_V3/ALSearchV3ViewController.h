//
//  ALSearchV3ViewController.h
//  alorth
//
//  Created by w91379137 on 2016/3/5.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALBasicSubViewController.h"
#import "ALFilter.h"
#import "ALFilterViewController.h"
#import "ALStreamProductListViewController.h"
#import "ALStreamProductListViewController+PullMore.h"

@interface ALSearchV3ViewController : ALBasicSubViewController
<UISearchBarDelegate,
ALFilterViewControllerDelegate,
ALStreamProductListViewControllerDelegate>

@property (nonatomic, strong) NSString *keyword;
@property (nonatomic, strong) IBOutlet UISearchBar *topSearchBar;  //搜尋條
@property (nonatomic, strong) IBOutlet UIButton *typeModeButton;

@property (nonatomic, strong) IBOutlet UIView *streamProductContainerView;

@property (nonatomic) ALFilterTypeMode typeMode;
@property (nonatomic) ALFilterSortMode sortMode;

@property (nonatomic, strong) NSMutableArray *languages;
@property (nonatomic, strong) NSMutableArray *categories;
@property (nonatomic, strong) NSMutableArray *locations;

@end
