//
//  ALCommonTableViewCell.h
//  alorth
//
//  Created by w91379137 on 2015/9/23.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ALCommonTableViewCell : UITableViewCell

+ (instancetype)cellFromXib;
- (void)cellFromXibSetting;//overwrite

@property (nonatomic, strong) NSDictionary *infoDict;

- (void)layoutAtWidth:(CGFloat)width;
- (CGFloat)performWithInfoDict:(NSDictionary *)infoDict;//overwrite

- (UITableView *)tryTableView;
- (void)tryTableViewSelectCell;

@end
