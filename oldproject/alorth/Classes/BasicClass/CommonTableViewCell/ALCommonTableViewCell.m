//
//  ALCommonTableViewCell.m
//  alorth
//
//  Created by w91379137 on 2015/9/23.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALCommonTableViewCell.h"

@implementation ALCommonTableViewCell

+ (id)cellFromXib
{
    if ([[NSBundle mainBundle] pathForResource:NSStringFromClass([self class]) ofType:@"nib"] == nil) {
        NSLog(@"不存在 xib : %@",NSStringFromClass([self class]));
        return nil;
    }
    
    id obj =
    [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class])
                                   owner:nil
                                 options:nil] lastObject];
    
    if (![obj isKindOfClass:self]) {
        NSLog(@"型態不合 xib : %@",NSStringFromClass([self class]));
        return nil;
    }
    [obj cellFromXibSetting];
    
    return obj;
}

- (void)cellFromXibSetting
{
    //overwrite
}

- (void)layoutAtWidth:(CGFloat)width
{
    self.frame = CGRectMake(0, 0, width, self.frame.size.height);
    [self layoutIfNeeded];
}

- (CGFloat)performWithInfoDict:(NSDictionary *)infoDict
{
    self.infoDict = infoDict;
    return self.frame.size.height;
}

- (UITableView *)tryTableView
{
    id view = [self superview];
    while (view && [view isKindOfClass:[UITableView class]] == NO) {
        view = [view superview];
    }
    return view;
}

- (void)tryTableViewSelectCell
{
    UITableView *tableView = self.tryTableView;
    NSIndexPath *index = [tableView indexPathForCell:self];
    if (tableView && index) {
        if ([tableView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)]) {
            [self.tryTableView.delegate tableView:tableView
                          didSelectRowAtIndexPath:index];
        }
    }
}

@end
