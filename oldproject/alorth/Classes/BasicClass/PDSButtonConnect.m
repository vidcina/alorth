//
//  CCButtonConnect.m
//  ButtonConnect
//
//  Created by w91379137 on 2015/11/24.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "PDSButtonConnect.h"

@implementation PDSButtonConnect
- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self addKVO];
}

- (void)setDelegate:(UIButton *)delegate
{
    NSArray *keys = @[@"enabled",@"selected",@"highlighted"];
    
    //取消前一個
    if (self.delegate) {
        for (NSString *key in keys) {
            [self.delegate removeSafeObserver:self
                                   forKeyPath:key];
        }
    }
    
    //規格符合才加入
    if ([delegate isKindOfClass:[UIButton class]]) {
        _delegate = delegate;
    }
    else {
        _delegate = nil;
    }
    
    [self addKVO];
}

- (void)addKVO
{
    NSArray *keys = @[@"enabled",@"selected",@"highlighted"];
    //註冊
    if (self.delegate) {
        for (NSString *key in keys) {
            [self.delegate addSafeObserver:self
                                forKeyPath:key
                                   options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                                   context:NULL];
        }
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    for (UIButton *button in self.controlButtons) {
        button.enabled = self.delegate.enabled;
        button.highlighted = self.delegate.highlighted;
        button.selected = self.delegate.highlighted ? NO : self.delegate.selected;
    }
}

@end
