//
//  NSObject+ALMethod.m
//  alorth
//
//  Created by w91379137 on 2015/12/11.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "NSObject+ALMethod.h"

@implementation NSObject (ALMethod)

#pragma mark -
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    return [ALNothingToSeeImage clearNothingToSeeImage];
}

-(void)alTableViewSetting:(UITableView *)tableView
{
    tableView.tableFooterView = [[UIView alloc] init];
    tableView.emptyDataSetSource = self;
    tableView.emptyDataSetDelegate = self;
}

- (UIImage *)limitImageLength:(UIImage *)image
{
    float length = 1000 / [UIScreen mainScreen].scale;
    if (image.size.width > length ||
        image.size.height > length ) {
        return [UIImage reSizeImage:image maxLength:length];
    }
    return image;
}

@end
