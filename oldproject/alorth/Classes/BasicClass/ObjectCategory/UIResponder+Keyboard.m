//
//  UIResponder+Keyboard.m
//  alorth
//
//  Created by w91379137 on 2016/2/3.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "UIResponder+Keyboard.h"

@implementation UIResponder (Keyboard)

- (UIToolbar *)keyboardReturnToolbar
{
    UIToolbar *doneToolbar =
    [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
    
    doneToolbar.barStyle = UIBarStyleDefault;
    doneToolbar.items =
    [NSArray arrayWithObjects:
     [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                  target:nil action:nil],
     
     [[UIBarButtonItem alloc]initWithTitle:ALLocalizedString(@"Done", nil)
                                     style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(resignFirstResponder)],nil];
    [doneToolbar sizeToFit];
    return doneToolbar;
}

@end
