//
//  CCButtonConnect.h
//  ButtonConnect
//
//  Created by w91379137 on 2015/11/24.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PDSButtonConnect : NSObject

@property (nonatomic, weak) IBOutlet UIButton *delegate;
@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *controlButtons;

@end
