//
//  ALBasicViewController.h
//  alorth
//
//  Created by w91379137 on 2015/5/25.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"
#import "ALCommonHeaderView.h"

@interface ALBasicViewController : GAITrackedViewController
<UITextFieldDelegate, UITextViewDelegate>
{
    IBOutlet ALCommonHeaderView *headerView;
    
    BOOL isViewWillAppearAgain;
    BOOL isViewDidAppearAgain;
    
    id editFirstResponder;
    
    IBOutletCollection(NSObject) NSArray *xibRetainObjectArray;
}

- (void)viewWillAppearFirstTime:(BOOL)animated;
- (void)viewDidAppearFirstTime:(BOOL)animated;

#pragma mark - Keyboard Auto System
- (IBAction)keyboardReturn;
- (void)keyboardDidShow:(NSNotification *)notification;
- (void)keyboardWillHide:(NSNotification *)notification;

@end
