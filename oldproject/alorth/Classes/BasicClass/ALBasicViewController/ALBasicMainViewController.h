//
//  ALMainViewController.h
//  alorth
//
//  Created by w91379137 on 2015/6/26.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicViewController.h"

//佔滿整版有下方列 會被nv所查找復用
@interface ALBasicMainViewController : ALBasicViewController
{
    IBOutlet UIView *barSpace;
}

@end
