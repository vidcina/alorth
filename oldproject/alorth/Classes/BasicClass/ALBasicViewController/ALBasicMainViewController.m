//
//  ALMainViewController.m
//  alorth
//
//  Created by w91379137 on 2015/6/26.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicMainViewController.h"

@interface ALBasicMainViewController ()

@end

@implementation ALBasicMainViewController

#pragma mark - Init
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //判斷是否位於主要的 navigationController
    if (self.navigationController ==
        [PDSEnvironmentViewController sharedInstance].mainNavigationController ) {
        [[PDSEnvironmentViewController sharedInstance] showBar];
    }
}

- (void)viewWillAppearFirstTime:(BOOL)animated
{
    [super viewWillAppearFirstTime:animated];
    [barSpace mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@([PDSEnvironmentViewController sharedInstance].tabbarHeight));
    }];
}

@end
