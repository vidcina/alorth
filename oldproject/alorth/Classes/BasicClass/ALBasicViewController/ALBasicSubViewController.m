//
//  ALBasicSubViewController.m
//  alorth
//
//  Created by w91379137 on 2015/6/23.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicSubViewController.h"
#import "ALBasicMainViewController.h"

@interface ALBasicSubViewController ()

@end

@implementation ALBasicSubViewController

#pragma mark - Init
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //判斷是否位於主要的 navigationController
    if (self.navigationController ==
        [PDSEnvironmentViewController sharedInstance].mainNavigationController ) {
        [[PDSEnvironmentViewController sharedInstance] hiddenBar];
    }
}

#pragma mark - IBAction
- (IBAction)back:(UIButton *)sender
{
    sender.enabled = NO;
    if ([PDSEnvironmentViewController sharedInstance].isBarAnimation) {
        [self performSelector:@selector(back:) withObject:nil afterDelay:0.3];
        return;
    }
    
    NSInteger previous = 0;
    if (self.navigationController.viewControllers.count > 2) {
        previous = self.navigationController.viewControllers.count - 2;
    }
    
    BOOL isMain =
    [self.navigationController.viewControllers[previous]
     isKindOfClass:[ALBasicMainViewController class]];
    [self.navigationController popViewControllerAnimated:!isMain];
}


@end
