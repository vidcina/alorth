//
//  ALBasicSubViewController.h
//  alorth
//
//  Created by w91379137 on 2015/6/23.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicViewController.h"

//佔滿整版沒有下方列 會被nv移除的
@interface ALBasicSubViewController : ALBasicViewController

- (IBAction)back:(UIButton *)sender;

@end
