//
//  ALBasicViewController.m
//  alorth
//
//  Created by w91379137 on 2015/5/25.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicViewController.h"
@interface ALBasicViewController ()

@end

@implementation ALBasicViewController

- (NSString *)screenName
{
    return [[self class] description];
}

#pragma mark - Init
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (isViewWillAppearAgain == NO) {
        isViewWillAppearAgain = YES;
        [self viewWillAppearFirstTime:animated];
    }
}

- (void)viewWillAppearFirstTime:(BOOL)animated
{
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (isViewDidAppearAgain == NO) {
        isViewDidAppearAgain = YES;
        [self viewDidAppearFirstTime:animated];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidChangeFrameNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
}

- (void)viewDidAppearFirstTime:(BOOL)animated
{
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self keyboardReturn];
    [super viewWillDisappear:animated];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    ALLog(@"%@ dealloc",NSStringFromClass([self class]));
}

#pragma mark - Keyboard Auto System
- (void)keyboardDidShow:(NSNotification *)notification
{
    //請SubClass自己實行
    //float keyboardHeight = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
    //float keyBoardYPosition = self.view.frame.size.height - keyboardHeight;
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    //請SubClass自己實行
}

- (IBAction)keyboardReturn
{
    if ([editFirstResponder respondsToSelector:@selector(resignFirstResponder)]) {
        [editFirstResponder performSelector:@selector(resignFirstResponder) withObject:nil];
        //WNLog(@"%@ 收鍵盤",obj);
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    editFirstResponder = textField;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    editFirstResponder = nil;
}

#pragma mark - UITextViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if (textView.inputAccessoryView == nil) {
        textView.inputAccessoryView = [textView keyboardReturnToolbar];
    }
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    editFirstResponder = textView;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    editFirstResponder = nil;
}

@end
