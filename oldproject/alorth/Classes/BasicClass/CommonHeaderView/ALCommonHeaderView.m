//
//  ALCommonHeaderView.m
//  alorth
//
//  Created by w91379137 on 2015/9/22.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALCommonHeaderView.h"

@implementation ALCommonHeaderView

#pragma mark - headerTitleText
-(void)setHeaderTitleText:(NSString *)headerTitleText
{
    self.titleLabel.localText = headerTitleText;
}

-(NSString *)headerTitleText
{
    return self.titleLabel.text;
}

#pragma mark - lineWidth
-(void)setLineWidth:(NSInteger)lineWidth
{
    self.baseView.lineWidth = lineWidth;
}

-(NSInteger)lineWidth
{
    return self.baseView.lineWidth;
}

#pragma mark - backgroundColorKey
-(void)setBackgroundColorKey:(NSString *)backgroundColorKey
{
    _backgroundColorKey = backgroundColorKey;
    UIColor *color = [ALColorDefinition alColorWithString:backgroundColorKey];
    if (color) {
        self.baseView.backgroundColor = color;
    }
}

@end
