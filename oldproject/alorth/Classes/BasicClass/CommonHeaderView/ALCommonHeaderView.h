//
//  ALCommonHeaderView.h
//  alorth
//
//  Created by w91379137 on 2015/9/22.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "PDSIBDesignABLEView.h"

IB_DESIGNABLE
@interface ALCommonHeaderView : PDSIBDesignABLEView

@property(nonatomic, strong) IBInspectable NSString *backButtonTypeKey;
@property(nonatomic, strong) IBInspectable NSString *headerTitleText;

@property(nonatomic, strong) IBInspectable NSString *backgroundColorKey;
@property(nonatomic) IBInspectable NSInteger lineWidth;

@property(nonatomic, strong) IBOutlet LineView *baseView;
@property(nonatomic, strong) IBOutlet UIButton *backButton;
@property(nonatomic, strong) IBOutlet UILabel *titleLabel;

@end
