//
//  ALSelectTypeScrollView.h
//  alorth
//
//  Created by w91379137 on 2015/7/17.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ALSelectTypeScrollView;
@protocol ALSelectTypeScrollViewDelegate <NSObject>

//回傳客製化view 可以把action 接到 selectIndex
-(UIView *)typeViewOfIndex:(NSInteger)index
                infoObject:(NSObject *)infoObject
      selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView;

//接收點到的訊息
-(void)didSelectIndex:(NSInteger)index
           infoObject:(NSObject *)infoObject
 selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView;

//修改 被選到 被反選 的客製化畫面
-(void)adjustSelectView:(UIView *)view
             infoObject:(NSObject *)infoObject
               selected:(BOOL)selected
   selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView;

@optional

@end

@interface ALSelectTypeScrollView : UIScrollView

@property (weak) IBOutlet NSObject <ALSelectTypeScrollViewDelegate> *selectDelegate;
@property (nonatomic, strong) UIView *typeViewContainer;

@property (nonatomic, strong) NSMutableArray *infoObjectArray; //保存訊息條
@property (nonatomic, readonly) NSMutableArray *typeViewArray; //用來判斷View的index
-(void)reloadData;
-(void)reloadSubView;

-(void)selectViewByGestureRecognizer:(UIGestureRecognizer *)sender;
-(void)selectView:(UIView *)view;
-(void)selectIndex:(NSInteger)index;
-(void)scrollToSelect:(BOOL)animated;
@property (nonatomic, readonly) NSInteger didSelectIndex;

@property (nonatomic) UIEdgeInsets contentTypeViewInset;
@property (nonatomic) float contentTypeViewGap;

//Default Button
//@property (nonatomic, strong) UIColor *didSelectColor;
//@property (nonatomic, strong) UIColor *unSelectColor;
//-(void)swipeTypeSelect:(UISwipeGestureRecognizer *)sender;

@end
