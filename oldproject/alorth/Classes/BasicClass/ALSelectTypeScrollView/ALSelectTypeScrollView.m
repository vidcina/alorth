//
//  ALSelectTypeScrollView.m
//  alorth
//
//  Created by w91379137 on 2015/7/17.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALSelectTypeScrollView.h"

@implementation ALSelectTypeScrollView

#pragma mark - init
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initMethod];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initMethod];
    }
    return self;
}

-(void)initMethod
{
    _typeViewArray = [NSMutableArray array];
    _didSelectIndex = 0;
    _contentTypeViewInset = UIEdgeInsetsMake(0, 0, 0, 0);
    _contentTypeViewGap = -1;
}

-(void)setInfoObjectArray:(NSMutableArray *)infoObjectArray
{
    _infoObjectArray = infoObjectArray;
    [self reloadData];
}

#pragma mark - reload
-(void)reloadData
{    
    //清空上次畫面
    for (UIView *view in _typeViewArray) {
        [view removeFromSuperview];
    }
    [_typeViewContainer removeFromSuperview];
    [_typeViewArray removeAllObjects];
    
    //初始化
    _typeViewContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    _typeViewContainer.backgroundColor = [UIColor clearColor];
    
    _typeViewContainer.clipsToBounds = YES;
    _typeViewContainer.layer.cornerRadius = 3;
    _typeViewContainer.layer.borderWidth = 1;
    _typeViewContainer.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    [self masSetContentView:_typeViewContainer isVerticalFix:YES];
    
    //預先複製一份
    NSArray *sizeArray = [NSArray arrayWithArray:_infoObjectArray];
    
    //排列起來
    UIView *firstOne;
    UIView *lastOne;
    for (NSInteger k = 0; k < sizeArray.count; k++) {
        
        NSObject *obj = sizeArray[k];
        UIView *aView = [self.selectDelegate typeViewOfIndex:k
                                                  infoObject:obj
                                        selectTypeScrollView:self];
        if (aView == nil) {
            aView = [[UIView alloc] initWithFrame:CGRectZero];
            aView.backgroundColor = [UIColor whiteColor];
        }
        
        [_typeViewArray addObject:aView];
        
        [_typeViewContainer addSubview:aView];
        
        [aView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_typeViewContainer.mas_top).offset(_contentTypeViewInset.top);
            make.bottom.equalTo(_typeViewContainer.mas_bottom).offset(-_contentTypeViewInset.bottom);
            
            if (lastOne == nil) {
                make.leading.equalTo(_typeViewContainer.mas_leading).offset(_contentTypeViewInset.left);
            }
            else {
                make.leading.equalTo(lastOne.mas_trailing).offset(_contentTypeViewGap);
            }
        }];
        lastOne = aView;
        
        if (firstOne == nil) {
            firstOne = aView;
        }
    }
    
    if (lastOne) {
        [lastOne mas_makeConstraints:^(MASConstraintMaker *make) {
            make.trailing.lessThanOrEqualTo(_typeViewContainer.mas_trailing).offset(-_contentTypeViewInset.right);
        }];
    }
    [self reloadSubView];
}

-(void)reloadSubView
{
    //預選設定
    for (NSInteger k = 0; k < _typeViewArray.count; k++) {
        [self.selectDelegate adjustSelectView:_typeViewArray[k]
                                   infoObject:_infoObjectArray[k]
                                     selected:(k == _didSelectIndex)
                         selectTypeScrollView:self];
    }
}

#pragma mark - select
-(void)selectViewByGestureRecognizer:(UIGestureRecognizer *)sender
{
    [self selectView:sender.view];
}

-(void)selectView:(UIView *)view
{
    _didSelectIndex = [_typeViewArray indexOfObject:view];
    if (_didSelectIndex == NSNotFound) {
        _didSelectIndex = 0;
        NSLog(@"selectView not found");
    }
    
    if (_didSelectIndex >= 0 && _didSelectIndex < _infoObjectArray.count) {
        [self.selectDelegate didSelectIndex:_didSelectIndex
                                 infoObject:_infoObjectArray[_didSelectIndex]
                       selectTypeScrollView:self];
    }
    
    [self reloadSubView];
}

-(void)selectIndex:(NSInteger)index
{
    if (index >= 0 && index < _typeViewArray.count) {
        [self selectView:_typeViewArray[index]];
    }
    else {
        NSLog(@"index invalid");
    }
}

-(void)scrollToSelect:(BOOL)animated
{
    [self layoutIfNeeded];
    UIView *selectView = self.typeViewArray[self.didSelectIndex];
    [self scrollRectToVisible:selectView.frame animated:animated];
}

@end
