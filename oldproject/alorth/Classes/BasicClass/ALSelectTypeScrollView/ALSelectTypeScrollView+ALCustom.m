//
//  ALSelectTypeScrollView+ALCustom.m
//  alorth
//
//  Created by w91379137 on 2015/10/19.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALSelectTypeScrollView+ALCustom.h"

@implementation ALSelectTypeScrollView (ALCustom)

//正常黑白框
+(UIButton *)alSelectButton:(NSString *)title
{
    UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
    aButton.layer.borderWidth = 1;
    aButton.layer.borderColor = kALProductDetailDidselectBgColor.CGColor;
    
    aButton.normalTitle = title;
    [aButton.titleLabel setFont:[self alSelectButtonFont]];
    
    return aButton;
}


+(CGSize)alSelectButtonSize:(NSString *)title
{
    NSDictionary *titleattributes = @{NSFontAttributeName:[self alSelectButtonFont]};
    CGSize titleSize =
    [title boundingRectWithSize:CGSizeMake(MAXFLOAT, 45)
                        options:NSStringDrawingUsesLineFragmentOrigin
                     attributes:titleattributes
                        context:nil].size;
    return titleSize;
}

+ (UIFont *)alSelectButtonFont
{
    return [UIFont systemFontOfSize:13];
}

+(void)alSelectButton:(UIButton *)button Select:(BOOL)selected
{
    if (selected) {
        //選到
        button.selected = YES;
        button.backgroundColor = kALProductDetailDidselectBgColor;
        [button setTitleColor:kALProductDetailDidselectFontColor forState:UIControlStateNormal];
    }
    else {
        //沒
        button.selected = NO;
        button.backgroundColor = kALProductDetailUnselectBgColor;
        [button setTitleColor:kALProductDetailUnselectFontColor forState:UIControlStateNormal];
    }
}

//無框藍白選項
+(UIButton *)alSortStyleSelectButton:(NSString *)title
{
    UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
    aButton.backgroundColor = [UIColor whiteColor];
    
    aButton.normalTitle = title;
    aButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    aButton.titleLabel.minimumScaleFactor = 0.5;
    
    return aButton;
}

+ (void)alSortStyleSelectButton:(UIButton *)button Select:(BOOL)selected
{
    if (selected) {
        //選到
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
    }
    else {
        //沒
        [button setTitleColor:[UIColor colorWithHexString:@"818181"] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:14.0];
    }
}

@end
