//
//  ALSelectTypeScrollView+ALCustom.h
//  alorth
//
//  Created by w91379137 on 2015/10/19.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALSelectTypeScrollView.h"

@interface ALSelectTypeScrollView (ALCustom)

+(UIButton *)alSelectButton:(NSString *)title;
+(CGSize)alSelectButtonSize:(NSString *)title;
+(void)alSelectButton:(UIButton *)button Select:(BOOL)selected;

//無框藍白選項
+(UIButton *)alSortStyleSelectButton:(NSString *)title;
+ (void)alSortStyleSelectButton:(UIButton *)button Select:(BOOL)selected;

@end
