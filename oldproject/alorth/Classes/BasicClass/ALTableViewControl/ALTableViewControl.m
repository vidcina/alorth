//
//  ALTableViewControl.m
//  alorth
//
//  Created by w91379137 on 2015/7/18.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALTableViewControl.h"
static CGFloat refreshDragLimit = 30.0f;

@interface ALTableViewControl()

//尚無用處 先放裡面
@property (nonatomic) BOOL isLoading;                           // 是否已呼叫API載入中, yes時顯示loading cell

@end

@implementation ALTableViewControl

-(instancetype)init
{
    self = [super init];
    if (self) {
        _lastDict = [NSMutableDictionary dictionary];
    }
    return self;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.dragIndicator == nil) {
        self.dragIndicator = [[UILabel alloc] initWithFrame:CGRectMake(0,
                                                                       0,
                                                                       scrollView.frame.size.width,
                                                                       refreshDragLimit)];
        self.dragIndicator.textAlignment = NSTextAlignmentCenter;
        self.dragIndicator.backgroundColor = [UIColor clearColor];
        self.dragIndicator.textColor = [UIColor blackColor];
    }
    
    if (!self.dragIndicator.superview) {
        [scrollView addSubview:self.dragIndicator];
    }
    
    if (scrollView.contentOffset.y < 0 && scrollView.contentOffset.y > -refreshDragLimit) {
        self.dragIndicator.center =
        CGPointMake(self.dragIndicator.frame.size.width / 2,- refreshDragLimit / 2);
        self.dragIndicator.localText = self.isLoading ? @"Loading" : @"Pull down to refresh";
        self.dragIndicator.alpha = 1.0;
    }
    else if (scrollView.contentOffset.y <= -refreshDragLimit)
    {
        self.dragIndicator.center =
        CGPointMake(self.dragIndicator.frame.size.width / 2,- refreshDragLimit / 2);
        self.dragIndicator.localText = self.isLoading ? @"Loading" : @"Release to refresh";
        self.dragIndicator.alpha = 1.0;
    }
    else if (scrollView.contentOffset.y > MAX(0, scrollView.contentSize.height - scrollView.frame.size.height) &&
             scrollView.contentOffset.y < MAX(0, scrollView.contentSize.height - scrollView.frame.size.height) + refreshDragLimit &&
             [self.apiDataArray count] > 0) {
        
        self.dragIndicator.center =
        CGPointMake(self.dragIndicator.frame.size.width / 2,scrollView.contentSize.height + refreshDragLimit / 2);
        self.dragIndicator.localText = self.isLoading ? @"Loading" : @"Pull up to load more";
        self.dragIndicator.alpha = 1.0;
    }
    else if (scrollView.contentOffset.y >= MAX(0, scrollView.contentSize.height - scrollView.frame.size.height) + refreshDragLimit &&
             [self.apiDataArray count] > 0) {
        self.dragIndicator.center =
        CGPointMake(self.dragIndicator.frame.size.width / 2,scrollView.contentSize.height + refreshDragLimit / 2);
        self.dragIndicator.localText = self.isLoading ? @"Loading" : @"Release to load more";
        self.dragIndicator.alpha = 1.0;
    }
    else {
        self.dragIndicator.alpha = 0.0;
    }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView.contentOffset.y <= -refreshDragLimit)
    {
        [self.delegate checkNewData:self];
    }
    else if (scrollView.contentOffset.y > 0 &&
             scrollView.contentOffset.y >= MAX(0, scrollView.contentSize.height - scrollView.frame.size.height) + refreshDragLimit) {
        
        [self.delegate loadMore:self];
    }
    if (!decelerate) {
        self.dragIndicator.alpha = 0.0;
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (!self.isLoading) {
        self.dragIndicator.alpha = 0.0;
    }
}

@end
