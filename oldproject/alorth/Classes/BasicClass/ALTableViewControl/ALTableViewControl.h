//
//  ALTableViewControl.h
//  alorth
//
//  Created by w91379137 on 2015/7/18.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ALTableViewControl;
@protocol ALTableViewControlDelegate <NSObject>

-(void)checkNewData:(ALTableViewControl *)tableViewControl;
-(void)loadMore:(ALTableViewControl *)tableViewControl;

@end

@interface ALTableViewControl : NSObject

@property (weak) IBOutlet NSObject <ALTableViewControlDelegate> *delegate;

@property (nonatomic, strong) NSMutableDictionary *lastDict;    // 作為下次呼叫api的計算
@property (nonatomic, strong) UILabel *dragIndicator;           // 提示往下/往上 拉 讀取更多資料 / 最新資料
@property (nonatomic, weak) NSMutableArray *apiDataArray;

-(void)scrollViewDidScroll:(UIScrollView *)scrollView;
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView;

@end
