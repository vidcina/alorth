//
//  ALBasicViewController+UIImagePickerControllerDelegate.h
//  alorth
//
//  Created by w91379137 on 2015/9/8.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (UIImagePickerControllerDelegate)
<UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIActionSheetDelegate>

- (void)showChoosePickerControllerType;//輸入
- (void)didPickerControllerReturnImage:(UIImage *)avatarImage;//輸出

@end
