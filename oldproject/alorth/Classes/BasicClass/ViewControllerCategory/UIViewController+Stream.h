//
//  UIViewController+Stream.h
//  alorth
//
//  Created by w91379137 on 2015/10/14.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Stream)

- (UIViewController *)playStreamVideoController:(NSDictionary *)infoDict;
- (void)playStreamVideo:(NSDictionary *)infoDict;

@end
