//
//  UIViewController+AVIMConversation.h
//  alorth
//
//  Created by w91379137 on 2015/10/19.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (AVIMConversation)

-(void)chatWithOtherId:(NSString *)otherId;

@end
