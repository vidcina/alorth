//
//  UIViewController+Stream.m
//  alorth
//
//  Created by w91379137 on 2015/10/14.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "UIViewController+Stream.h"
#import "ALVLCPlayerViewController.h"
@import MediaPlayer;

@implementation UIViewController (Stream)

- (UIViewController *)playStreamVideoController:(NSDictionary *)infoDict
{
    //    MPMoviePlayerViewController *playerVC =
    //    [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:string]];
    //    playerVC.moviePlayer.movieSourceType = MPMovieSourceTypeStreaming;
    //    playerVC.moviePlayer.fullscreen = YES;
    //    playerVC.moviePlayer.allowsAirPlay = YES;
    //    playerVC.moviePlayer.controlStyle = MPMovieControlStyleFullscreen;
    
    ALVLCPlayerViewController *playerVC =
    [[ALVLCPlayerViewController alloc] init];
    playerVC.infoDict = infoDict;
    return playerVC;
}

- (void)playStreamVideo:(NSDictionary *)infoDict
{
    [self.navigationController pushViewController:[self playStreamVideoController:infoDict] animated:YES];
}

@end
