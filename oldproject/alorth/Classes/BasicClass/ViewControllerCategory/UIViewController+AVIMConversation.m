//
//  UIViewController+AVIMConversation.m
//  alorth
//
//  Created by w91379137 on 2015/10/19.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "UIViewController+AVIMConversation.h"

@implementation UIViewController (AVIMConversation)

-(void)chatWithOtherId:(NSString *)otherId
{
    if (![PDSAccountManager isLoggedIn]) {
        ALLog(@"沒有登入");
        return;
    }
    
    if (![[PDSAccountManager sharedManager] imClientID]) {
        [[PDSAccountManager sharedManager] loginLeanCloud];
        YKSimpleAlert(ALLocalizedString(@"$Chat service is disconnected$. $Please try again later$.", nil));
        return;
    }
    
    if (otherId.length == 0 || [ALAppDelegate sharedAppDelegate].userID.length == 0) {
        ALLog(@"這是個無效的 名稱");
        return;
    }
    
    for (AVIMConversation *conversation in [[PDSAccountManager sharedManager] conversationArray]) {
        if ([[conversation members] count] == 2 &&
            [[conversation members] containsObject:otherId] &&
            [[conversation members] containsObject:[[ALAppDelegate sharedAppDelegate] userID]]) {
            ALChatRoomController *controller = [[ALChatRoomController alloc] init];
            controller.currentConversation = conversation;
            [self.navigationController pushViewController:controller animated:YES];
            return ;
        }
    }
    // no conversation can be reused. create new one
    [[[PDSAccountManager sharedManager] imClient] createConversationWithName:@"Chat"
                                                                   clientIds:@[otherId]
                                                                    callback:^(AVIMConversation *conversation, NSError *error) {
        if (conversation) {
            ALChatRoomController *controller = [[ALChatRoomController alloc] init];
            controller.currentConversation = conversation;
            [[[PDSAccountManager sharedManager] conversationArray] insertObject:conversation atIndex:0];
            [[NSNotificationCenter defaultCenter] postNotificationName:kPDSIMDidLoadConversationList object:nil];
            
            [self.navigationController pushViewController:controller animated:YES];
        }
        else {
            YKSimpleAlert(ALLocalizedString(@"$Server temporarily not responding$. $Please try again later$.",nil));
        }
    }];
}

@end
