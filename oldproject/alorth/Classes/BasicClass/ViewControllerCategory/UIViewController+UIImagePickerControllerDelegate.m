//
//  ALBasicViewController+UIImagePickerControllerDelegate.m
//  alorth
//
//  Created by w91379137 on 2015/9/8.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "UIViewController+UIImagePickerControllerDelegate.h"
#import <AssetsLibrary/AssetsLibrary.h>

@implementation UIViewController (UIImagePickerControllerDelegate)

#pragma mark -
- (void)showChoosePickerControllerType
{
    UIActionSheet *actionSheet =
    [[UIActionSheet alloc]initWithTitle:ALLocalizedString(@"Choose",nil)
                               delegate:self
                      cancelButtonTitle:ALLocalizedString(@"Cancel",nil)
                 destructiveButtonTitle:nil
                      otherButtonTitles:[self sheetButtonTitle:NO],
     [self sheetButtonTitle:YES],nil];
    [actionSheet showInView:self.view];
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    if ([title isEqualToString:[self sheetButtonTitle:NO]] ||
        [title isEqualToString:[self sheetButtonTitle:YES]]) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.allowsEditing = YES;
        picker.delegate = self;
        
        if ([title isEqualToString:[self sheetButtonTitle:NO]]) {
            picker.mediaTypes = @[(NSString *)kUTTypeImage];
        }
        else {
#if TARGET_IPHONE_SIMULATOR
            ALLog(@"模擬器無法載入相機");
            picker.mediaTypes = @[(NSString *)kUTTypeImage];
#else
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
#endif
        }
        {
            [self addChildViewController:picker];
            picker.view.frame = self.view.bounds;
            [self.view addSubview:picker.view];
            [picker didMoveToParentViewController:self];
        }
    }
}

-(void)imageFromAssetURL:(NSURL *)assetURL CompletionBlock:(void (^)(UIImage *image, NSError *error))block
{
    ALAssetsLibrary *assetLibrary  = [[ALAssetsLibrary alloc] init];
    [assetLibrary assetForURL:assetURL resultBlock:^(ALAsset *asset) {
        ALAssetRepresentation *rep = [asset defaultRepresentation];
        CGImageRef iref = [rep fullResolutionImage];
        if (iref) {
            UIImage *largeimage = [UIImage imageWithCGImage:iref];
            block(largeimage,nil);
        }
        else {
            block(nil,[NSError errorWithDomain:NSStringFromClass([self class])
                                          code:1
                                      userInfo:nil]);
        }
    } failureBlock:^(NSError *error) {
        block(nil,[NSError errorWithDomain:NSStringFromClass([self class])
                                      code:2
                                  userInfo:nil]);
    }];
}

-(NSString *)sheetButtonTitle:(BOOL)isTakeNewOne
{
    if (isTakeNewOne) {
        return ALLocalizedString(@"Camera",nil);
    }
    return ALLocalizedString(@"Album",nil);
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self imagePickerRemove:picker];
    UIImage *editImage = [info objectForKey:UIImagePickerControllerEditedImage];
    if (!editImage) {
        editImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    }
    if (editImage) {
        [self didPickerControllerReturnImage:editImage];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self imagePickerRemove:picker];
}

-(void)imagePickerRemove:(UIImagePickerController *)picker
{
    [picker.view removeFromSuperview];
    [picker removeFromParentViewController];
    picker = nil;
}

#pragma mark -
- (void)didPickerControllerReturnImage:(UIImage *)avatarImage
{
    
}

@end
