//
//  ALBasicViewController+ImageOnline.m
//  alorth
//
//  Created by w91379137 on 2015/9/8.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "UIViewController+ImageOnline.h"

@implementation UIViewController (ImageOnline)

- (void)uploadImage:(UIImage *)image
{
    [self uploadImage:image DisplayMB:YES];
}

- (void)uploadImage:(UIImage *)image DisplayMB:(BOOL)displayMB
{
    //檢查上載暫存資料夾
    NSString *cachesImageFileDirPath =
    [NSHomeDirectory() stringByAppendingPathComponent:@"/Library/Caches/upload/"];
    [ALNetWorkAPI createCachesImageFileDir:cachesImageFileDirPath];
    
    NSString *localPath =
    [NSString stringWithFormat:@"%@/%u.jpg",cachesImageFileDirPath, arc4random_uniform(9999)];
    
    NSData *imageData = UIImageJPEGRepresentation(image, 0.1);
    
    if ([imageData writeToFile:localPath atomically:YES]) {
        [self productMediaUploadStep:localPath DisplayMB:displayMB];
    }
    else {
        ALLog(@"本地端檔案儲存失敗");
        [self didSaveImageAtURL:nil];
    }
}

-(void)productMediaUploadStep:(NSString *)localPath DisplayMB:(BOOL)displayMB
{
    __block typeof(self) __weak weakself = self;
    
    //初始化
    __block NSString *imageURLString = nil;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        //連續api 初始設定
        __block int apiTryTimes = 0;
        __block BOOL isSuccess = YES;
        __block double defaultDelayInSeconds = 0.5;                 //失敗延遲
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        
        if (isSuccess) {
            apiTryTimes = 0;
            while (apiTryTimes < 2) {
                
                double delayInSeconds = defaultDelayInSeconds;
                if (apiTryTimes == 0) {
                    delayInSeconds = 0;
                }
                
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    
                    if (displayMB) [self addMBProgressHUDWithKey:@"productMediaUploadStep"];
                    // Amazon
                    [[ALAmazonUploadManager sharedManager] uploadImageFilePath:localPath
                                                              progressCallback:nil
                                                               completionBlock:^(NSString *uploadedURLString) {
                                                                   if (displayMB) [self removeMBProgressHUDWithKey:@"productMediaUploadStep"];
                                                                   
                                                                   if (uploadedURLString.length > 0) {
                                                                       [self deleteFile:localPath];
                                                                       imageURLString = uploadedURLString;
                                                                       isSuccess = YES;
                                                                   }
                                                                   else {
                                                                       isSuccess = NO;
                                                                   }
                                                                   dispatch_semaphore_signal(sema);
                                                               }];
                    
                    // 阿里巴巴
                    /*
                    [[ALMediaUploadManager sharedManager] uploadImageFilePath:localPath
                                                             progressCallback:^(float progress) {}
                                                              completionBlock:^(NSString *uploadedURLString) {
                                                                  [self removeMBProgressHUDWithKey:@"productMediaUploadStep"];

                                                                  if (uploadedURLString.length > 0) {
                                                                      [self deleteFile:localPath];
                                                                      imageURLString = uploadedURLString;
                                                                      isSuccess = YES;
                                                                  }
                                                                  else {
                                                                      isSuccess = NO;
                                                                  }
                                                                  dispatch_semaphore_signal(sema);
                                                              }];
                     */
                });
                dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
                
                if (isSuccess == YES) {
                    break;
                }
                else {
                    apiTryTimes ++;
                    ALLog(@"uploadImage 重試%d",apiTryTimes);
                }
            }
        }
        
        if (isSuccess) {
            [weakself didSaveImageAtURL:imageURLString];
        }
        else {
            //@"網路連線錯誤，請稍後再試一次"
            //YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
            [weakself didSaveImageAtURL:nil];
        }
    });
}

//-(void)avatarUploadStepWithLocalPath:(NSString *)localPath
//{
//    if (localPath) {
//        //阻擋準備
//        [self addMBProgressHUDWithKey:@"avatarUploadStepWithLocalPath"];
//        [[ALMediaUploadManager sharedManager] uploadImageFilePath:localPath
//                                                 progressCallback:^(float progress) {}
//                                                  completionBlock:^(NSString *uploadedURLString) {
//                                                      [self removeMBProgressHUDWithKey:@"avatarUploadStepWithLocalPath"];
//                                                      
//                                                      [self deleteFile:localPath];
//                                                      [self imageOnlineURLString:uploadedURLString];
//                                                  }];
//    }
//    
//}

-(void)deleteFile:(NSString *)aPath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:aPath]) {
        NSError *error;
        if ([[NSFileManager defaultManager] isDeletableFileAtPath:aPath]) {
            BOOL success = [[NSFileManager defaultManager] removeItemAtPath:aPath error:&error];
            if (!success) {
                NSLog(@"Error removing file at path: %@", error.localizedDescription);
            }
        }
    }
}

#pragma mark -
- (void)didSaveImageAtURL:(NSString *)string
{
    
}

@end
