//
//  UIViewController+ALMethod.m
//  alorth
//
//  Created by w91379137 on 2015/11/19.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "UIViewController+ALMethod.h"

@implementation UIViewController (ALMethod)

#pragma mark -
- (UITableViewCell *)defaultUITableViewCell
{
    UITableViewCell *cell =
    [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@""];
    
    return cell;
}

#pragma mark -
-(void)showBigImageGesture:(UITapGestureRecognizer *)sender
{
    PDSEnvironmentViewController *evc = [PDSEnvironmentViewController sharedInstance];
    [evc showBigImageGesture:sender];
}

@end
