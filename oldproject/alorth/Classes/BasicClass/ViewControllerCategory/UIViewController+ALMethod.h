//
//  UIViewController+ALMethod.h
//  alorth
//
//  Created by w91379137 on 2015/11/19.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (ALMethod)

#pragma mark -
- (UITableViewCell *)defaultUITableViewCell;

#pragma mark - Show BigImage Method
-(void)showBigImageGesture:(UITapGestureRecognizer *)sender;

@end
