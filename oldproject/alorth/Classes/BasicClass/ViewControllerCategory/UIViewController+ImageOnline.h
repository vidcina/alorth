//
//  ALBasicViewController+ImageOnline.h
//  alorth
//
//  Created by w91379137 on 2015/9/8.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (ImageOnline)

- (void)uploadImage:(UIImage *)image;//輸入
- (void)didSaveImageAtURL:(NSString *)string;//輸出

//新版
- (void)uploadImage:(UIImage *)image DisplayMB:(BOOL)displayMB; //輸入

@end
