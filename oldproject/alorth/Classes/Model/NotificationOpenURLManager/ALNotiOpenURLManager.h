//
//  ALNotiOpenURLManager.h
//
//  Created by w91379137 on 2015/12/30.
//

#import <Foundation/Foundation.h>
#import "ALNotiOpenURLAction.h"

static NSString *ALNotiOpenURLManagerNewAction = @"ALNotiOpenURLManagerNewAction";

@interface ALNotiOpenURLManager : NSObject

#pragma mark - check
+ (BOOL)shouldHandleURL:(NSURL *)url;

#pragma mark - Init
+ (instancetype)sharedInstance;
@property (nonatomic, strong) NSMutableArray *actionArray;

- (ALNotiOpenURLAction *)lastNewAction;

#pragma mark - HandleURL
- (void)handleURL:(NSURL *)url;

@end
