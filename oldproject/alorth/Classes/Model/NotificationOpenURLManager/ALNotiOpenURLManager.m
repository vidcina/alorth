//
//  ALNotiOpenURLManager.m
//
//  Created by w91379137 on 2015/12/30.
//

#import "ALNotiOpenURLManager.h"

@implementation ALNotiOpenURLManager

#pragma mark - check
+ (BOOL)shouldHandleURL:(NSURL *)url
{
    return ([self streamIDFromURL:url] != nil);
}

+ (NSString *)streamIDFromURL:(NSURL *)url
{
    if ([[url scheme] isEqualToString:@"alorth"] || [[url scheme] isEqualToString:@"aimcue"]) {
        NSString *streamID = [url lastPathComponent];
        if ([streamID length]) {
            return streamID;
        }
    }
    return nil;
}

#pragma mark - Init
+ (instancetype)sharedInstance
{
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

#pragma mark - Getter / Setter
- (NSMutableArray *)actionArray
{
    if (_actionArray == nil) {
        _actionArray = [NSMutableArray array];
    }
    return _actionArray;
}

#pragma mark - Search
- (ALNotiOpenURLAction *)lastNewAction
{
    for (NSInteger k = 0; k < self.actionArray.count; k++) {
        ALNotiOpenURLAction *action = self.actionArray[k];
        if (action.status == ALNotiOpenURLActionTypeNew) {
            return action;
        }
    }
    return nil;
}

#pragma mark - HandleURL
- (void)handleURL:(NSURL *)url
{
    NSString *streamID = [ALNotiOpenURLManager streamIDFromURL:url];
    
    if (streamID) {
        ALNotiOpenURLAction *action = [[ALNotiOpenURLAction alloc] init];
        action.type = ALNotiOpenURLActionTypeURL;
        action.url = url;
        action.streamID = streamID;
        [self.actionArray addObject:action];
        [[NSNotificationCenter defaultCenter] postNotificationName:ALNotiOpenURLManagerNewAction object:action];
    }
    else {
        ALLog(@"streamID is nil");
    }
}

@end
