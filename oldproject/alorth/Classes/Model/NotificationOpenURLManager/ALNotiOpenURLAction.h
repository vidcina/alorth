//
//  ALNotiOpenURLAction.h
//  alorth
//
//  Created by w91379137 on 2015/12/30.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_OPTIONS(NSUInteger, ALNotiOpenURLActionType) {
    ALNotiOpenURLActionTypeNone,
    ALNotiOpenURLActionTypeURL,
    ALNotiOpenURLActionTypeNotification
};

typedef NS_OPTIONS(NSUInteger, ALNotiOpenURLActionStatus) {
    ALNotiOpenURLActionTypeNew,
    ALNotiOpenURLActionTypeProcessing,
    ALNotiOpenURLActionTypeComplete,
    ALNotiOpenURLActionTypeFailure
};

@interface ALNotiOpenURLAction : NSObject

@property (nonatomic) ALNotiOpenURLActionType type;
@property (nonatomic) ALNotiOpenURLActionStatus status;
@property (nonatomic, strong) NSString *failureMsg;

//ALNotiOpenURLActionTypeURL
@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) NSString *streamID;

@end
