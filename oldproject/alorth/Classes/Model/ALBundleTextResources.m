//
//  ALBundleTextResources.m
//  alorth
//
//  Created by w91379137 on 2015/11/12.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALBundleTextResources.h"
#import "CHCSVParser.h"

static NSString const *KVersion = @"version";
static NSString const *KDataList = @"dataList";

@interface ALBundleTextResources()
{
    
}

@end

@implementation ALBundleTextResources

#pragma mark - init
+ (instancetype)sharedInstance
{
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

#pragma mark - 文字資料版本更新
- (void)versionCheck
{
    //考慮把一次快取 做到api裡面 時間太近不會再呼叫一次
    [self addMBProgressHUDWithKey:@"versionInformationA077"];
    [ALNetWorkAPI versionInformationA077CompletionBlock:^(NSDictionary *responseObject) {
        [self removeMBProgressHUDWithKey:@"versionInformationA077"];
        
        if ([ALNetWorkAPI checkSerialNumber:@"A077"
                             ResponseObject:responseObject]) {
            
            //讀取 目前 app 版本
            NSString *currentAppVersion = responseObject.safe[@"resBody"][@"Version"];
            NSString *InstallAppVersion =
            [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
            
            if ([currentAppVersion compare:InstallAppVersion options:NSNumericSearch] == NSOrderedDescending) {
                
                //已經有新版本
                NSLog(@"Current_AppVersion %@ is higher than Install_AppVersion %@\n\n",currentAppVersion,InstallAppVersion);
                if (ALitunesAppID > 0) {
                    
                    static NSString *const iOS7AppStoreURLFormat = @"itms-apps://itunes.apple.com/app/id%d";
                    static NSString *const iOSAppStoreURLFormat = @"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%d";
                    NSURL *url =
                    [NSURL URLWithString:[NSString stringWithFormat:([[UIDevice currentDevice].systemVersion floatValue] >= 7.0f) ? iOS7AppStoreURLFormat: iOSAppStoreURLFormat, ALitunesAppID]];
                    
                    if ([[UIApplication sharedApplication] canOpenURL:url]) {
                        [YKBlockAlert alertWithTitle:@""
                                             message:ALLocalizedString(@"New Version",nil)
                                    blocksAndButtons:^{
                                        
                                        [[UIApplication sharedApplication] openURL:url];
                                        
                                    },ALLocalizedString(@"OK",nil), nil];
                    }
                    else {
                        NSLog(@"該裝置無法 打開app store\n\n");
                    }
                }
                else {
                    NSLog(@"沒有設置 appID\n\n");
                }
            }
            else {
                //目前已經是最新版
            }
        }
    }];
}

- (void)textDataInitialAndUpdate
{
    //尚未與上部分合併
    {//Category
        NSString *cachePath = [self categoryCachePath];
        if (!self.categoryDict) {
            self.categoryDict = [self dictionaryOfPath:cachePath];
        }
        if (!self.categoryDict) {
            NSLog(@"載入原始檔案 Category");
            cachePath = [[NSBundle mainBundle] pathForResource:cachedCategoryDict ofType:@"plist"];
            self.categoryDict = [self dictionaryOfPath:cachePath];
            [self.categoryDict writeToFile:[self categoryCachePath] atomically:YES];
        }
        if (!self.categoryDict) {
            self.categoryDict =
            @{KVersion : @"0",
              KDataList : @[]};
        }
        [ALBundleTextResources updateCategoryToLanguageCenter];
    }
    {
        NSString *cachePath = [self languageCachePath];
        if (!self.languageDict) {
            self.languageDict = [self dictionaryOfPath:cachePath];
        }
        if (!self.languageDict) {
            NSLog(@"載入原始檔案 Language");
            cachePath = [[NSBundle mainBundle] pathForResource:cachedLanguageDict ofType:@"plist"];
            self.languageDict = [self dictionaryOfPath:cachePath];
            [self.languageDict writeToFile:[self languageCachePath] atomically:YES];
        }
        if (!self.languageDict) {
            self.languageDict =
            @{KVersion : @"0",
              KDataList : @[]};
        }
        [ALBundleTextResources updateLanguageToLanguageCenter];
    }
    {
        NSString *cachePath = [self locationCachePath];
        if (!self.locationDict) {
            self.locationDict = [self dictionaryOfPath:cachePath];
        }
        if (!self.locationDict) {
            NSLog(@"載入原始檔案 Location");
            cachePath = [[NSBundle mainBundle] pathForResource:cachedLocationDict ofType:@"plist"];
            self.locationDict = [self dictionaryOfPath:cachePath];
            [self.locationDict writeToFile:[self locationCachePath] atomically:YES];
        }
        if (!self.locationDict) {
            self.locationDict =
            @{KVersion : @"0",
              KDataList : @[]};
        }
        [ALBundleTextResources updateLocationToLanguageCenter];
    }
    
    __block NSString *categoryVersion = @"0";
    __block NSString *languageVersion = @"0";
    __block NSString *locationVersion = @"0";
    
    dispatch_group_t waitToCheckVersion = dispatch_group_create();
    
    dispatch_group_enter(waitToCheckVersion);
    [ALNetWorkAPI versionInformationA077CompletionBlock:^(NSDictionary *responseObject) {
       
        if ([ALNetWorkAPI checkSerialNumber:@"A077"
                             ResponseObject:responseObject]) {
            
            categoryVersion = responseObject.safe[@"resBody"][@"CategoryVersion"];
            languageVersion = responseObject.safe[@"resBody"][@"LanguageVersion"];
            locationVersion = responseObject.safe[@"resBody"][@"LocationVersion"];
        }
        
        dispatch_group_leave(waitToCheckVersion);
    }];
    
    dispatch_group_notify(waitToCheckVersion, dispatch_get_main_queue(), ^{
        [self categoryUpdate:categoryVersion];
        [self languageUpdate:languageVersion];
        [self locationUpdate:locationVersion];
    });
}

- (NSDictionary *)dictionaryOfPath:(NSString *)cachePath
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:cachePath]) {
        NSObject *data = [NSDictionary dictionaryWithContentsOfFile:cachePath];
        if ([data isKindOfClass:[NSDictionary class]]) {
            return (NSDictionary *)data;
        }
    }
    return nil;
}

#pragma mark - Category
static NSString *cachedCategoryDict = @"cachedCategoryDict";
- (NSString *)categoryCachePath
{
    return [cacheDataPath() stringByAppendingPathComponent:cachedCategoryDict];
}

- (void)categoryUpdate:(NSString *)serverVersion
{
    NSString *currentVersion = self.categoryDict[KVersion];
    if ([serverVersion compare:currentVersion options:NSNumericSearch] == NSOrderedDescending) {
        ALLog(@"Category 版本更新 %@ >> %@",currentVersion,serverVersion);
        
        [ALNetWorkAPI categoryListA030CompletionBlock:^(NSDictionary *responseObject) {
            
            if ([ALNetWorkAPI checkSerialNumber:@"A030"
                                 ResponseObject:responseObject]) {
                NSArray *categoryArray = responseObject[@"resBody"][@"CategoryList"];
                
                [ALBundleTextResources detailCategoryArray:categoryArray
                                           CompletionBlock:^(NSMutableArray *newCategoryArray) {
                                               
                                               //排序
                                               NSArray *sortCategoryArray =
                                               [newCategoryArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
                                                   
                                                   return [obj1[@"CategoryIndex"] compare:obj2[@"CategoryIndex"]
                                                                                  options:NSNumericSearch];
                                               }];
                                               
                                               //假設其中一筆沒有取得 是為沒更新
                                               NSString *version = serverVersion;
                                               if (categoryArray.count > sortCategoryArray.count) {
                                                   version = currentVersion;
                                               }
                                               
                                               self.categoryDict =
                                               @{KVersion : version,
                                                 KDataList : sortCategoryArray};
                                               [ALBundleTextResources updateCategoryToLanguageCenter];
                                               
                                               [self.categoryDict writeToFile:[self categoryCachePath] atomically:YES];
                                           }];
            }
            else {
                ALLog(@"Category 版本更新 讀取錯誤 %s",__func__);
            }
        }];
    }
    else {
        ALLog(@"Category 目前版本:%@ API:%@",currentVersion,serverVersion);
    }
}

//http://www.raywenderlich.com/63338/grand-central-dispatch-in-depth-part-2
+ (void)detailCategoryArray:(NSArray *)categoryArray
            CompletionBlock:(void (^)(NSMutableArray *categoryDictionary))completionBlock
{
    __block NSMutableArray *newCategoryArray = [NSMutableArray array];
    
    dispatch_group_t downloadGroup = dispatch_group_create();
    
    for (NSInteger i = 0; i < categoryArray.count; i++) {
        
        dispatch_group_enter(downloadGroup);
        NSString *categoryName = categoryArray[i][@"CategoryIndex"];
        
        [ALNetWorkAPI subCategoryListA029ByCategoryIndex:categoryName
                                         CompletionBlock:^(NSDictionary *responseObject) {
                                             
                                             if ([ALNetWorkAPI checkSerialNumber:@"A029"
                                                                  ResponseObject:responseObject]) {
                                                 NSArray *goodsSize = responseObject[@"resBody"][@"GoodsSize"];
                                                 
                                                 NSMutableDictionary *combinedCategoryDictionary = [categoryArray[i] mutableCopy];
                                                 combinedCategoryDictionary[@"GoodsSize"] = goodsSize;
                                                 
                                                 [newCategoryArray addObject:combinedCategoryDictionary];
                                             }
                                             else {
                                                 ALLog(@"來源:%@ \n回傳錯誤:%@",categoryArray[i],responseObject)
                                             }
                                             
                                             dispatch_group_leave(downloadGroup);
                                         }];
    }
    
    dispatch_group_notify(downloadGroup, dispatch_get_main_queue(), ^{
        completionBlock(newCategoryArray);
    });
}

#pragma mark - Language
static NSString *cachedLanguageDict = @"cachedLanguageDict";
- (NSString *)languageCachePath
{
    return [cacheDataPath() stringByAppendingPathComponent:cachedLanguageDict];
}

- (void)languageUpdate:(NSString *)serverVersion
{
    NSString *currentVersion = self.languageDict[KVersion];
    if ([serverVersion compare:currentVersion options:NSNumericSearch] == NSOrderedDescending) {
        ALLog(@"Language 版本更新 %@ >> %@",currentVersion,serverVersion);
        
        [ALNetWorkAPI allLanguageListA092CompletionBlock:^(NSDictionary *responseObject) {
            
            if ([ALNetWorkAPI checkSerialNumber:@"A092"
                                 ResponseObject:responseObject]) {
                
                NSArray *newLanguageArray = responseObject[@"resBody"][@"LanguageList"];
                
                //排序
                NSArray *sortLanguageArray =
                [newLanguageArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
                    
                    return [obj1[@"LanguageIndex"] compare:obj2[@"LanguageIndex"]
                                                   options:NSNumericSearch];
                }];
                
                self.languageDict =
                @{KVersion : serverVersion,
                  KDataList : sortLanguageArray};
                
                [ALBundleTextResources updateLanguageToLanguageCenter];
                [self.languageDict writeToFile:[self languageCachePath] atomically:YES];
            }
            else {
                ALLog(@"Language 版本更新 讀取錯誤 %s",__func__);
            }
        }];
    }
    else {
        ALLog(@"Language 目前版本:%@ API:%@",currentVersion,serverVersion);
    }
}

#pragma mark - Location
static NSString *cachedLocationDict = @"cachedLocationDict";
- (NSString *)locationCachePath
{
    return [cacheDataPath() stringByAppendingPathComponent:cachedLocationDict];
}

- (void)locationUpdate:(NSString *)serverVersion
{
    NSString *currentVersion = self.locationDict[KVersion];
    if ([serverVersion compare:currentVersion options:NSNumericSearch] == NSOrderedDescending) {
        ALLog(@"Location 版本更新 %@ >> %@",currentVersion,serverVersion);
        
        [ALNetWorkAPI allLocationListA093CompletionBlock:^(NSDictionary *responseObject) {
            
            if ([ALNetWorkAPI checkSerialNumber:@"A093"
                                 ResponseObject:responseObject]) {
                
                NSArray *newLocationArray = responseObject[@"resBody"][@"LocationList"];
                
                //排序
                NSArray *sortLocationArray =
                [newLocationArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
                    
                    return [obj1[@"LocationRank"] compare:obj2[@"LocationRank"]
                                                  options:NSNumericSearch];
                }];
                
                self.locationDict =
                @{KVersion : serverVersion,
                  KDataList : sortLocationArray};
                
                [ALBundleTextResources updateLocationToLanguageCenter];
                [self.locationDict writeToFile:[self locationCachePath] atomically:YES];
            }
            else {
                ALLog(@"Location 版本更新 讀取錯誤 %s",__func__);
            }
        }];
    }
    else {
        ALLog(@"Location 目前版本:%@ API:%@",currentVersion,serverVersion);
    }
}

#pragma mark - Getter
+ (NSArray *)categoryArray
{
    return [ALBundleTextResources sharedInstance].categoryDict[KDataList];
}

+ (NSArray *)languageArray
{
    return [ALBundleTextResources sharedInstance].languageDict[KDataList];
}

+ (NSArray *)locationArray
{
    return [ALBundleTextResources sharedInstance].locationDict[KDataList];
}

#pragma mark - Update Language
+ (void)updateCategoryToLanguageCenter
{
    for (NSDictionary *dict in self.categoryArray) {
        NSString *chinese = dict.safe[@"CategoryChinese"];
        NSString *english = dict.safe[@"CategoryEn"];
        
        if (chinese && english) {
            [ALLanguageCenter addTranslationObjects:@[english,chinese]
                                            forKeys:@[@"en",@"zh-Hant"]];
        }
    }
}

+ (void)updateLanguageToLanguageCenter
{
    for (NSDictionary *dict in self.languageArray) {
        NSString *chinese = dict.safe[@"LanguageChinese"];
        NSString *english = dict.safe[@"LanguageMap"];
        
        if (chinese && english) {
            [ALLanguageCenter addTranslationObjects:@[english,chinese]
                                            forKeys:@[@"en",@"zh-Hant"]];
        }
    }
}

+ (void)updateLocationToLanguageCenter
{
    //無翻譯
}

#pragma mark - 國家清單
+ (NSArray *)countryCodeArray
{
    NSString *countryCodeJsonFile = [[NSBundle mainBundle] pathForResource:@"slim-2" ofType:@"json"];
    NSData *jsonData = [NSData dataWithContentsOfFile:countryCodeJsonFile];
    NSArray *countryCodeArray =
    [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:nil];
    return countryCodeArray;
}

/*
+ (NSArray *)countryArray
{
    return @[
             @"Afghanistan",
             @"Aland Islands",
             @"Albania",
             @"Algeria",
             @"American Samoa",
             @"Andorra",
             @"Angola",
             @"Anguilla",
             @"Antigua and Barbuda",
             @"Argentina",
             @"Armenia",
             @"Aruba",
             @"Australia",
             @"Austria",
             @"Azerbaijan",
             @"Bangladesh",
             @"Bahrain",
             @"Bahamas",
             @"Barbados",
             @"Belarus",
             @"Belgium",
             @"Belize",
             @"Benin",
             @"Bermuda",
             @"Bhutan",
             @"Bolivia",
             @"Bosnia and Herzegovina",
             @"Botswana",
             @"Bouvet Island",
             @"Brazil",
             @"Brunei",
             @"Bulgaria",
             @"Burkina Faso",
             @"Burundi",
             @"Cambodia",
             @"Cameroon",
             @"Canada",
             @"Cape Verde",
             @"Central African Republic",
             @"Chad",
             @"Chile",
             @"Christmas Islands",
             @"Cocos (keeling) Islands",
             @"Colombia",
             @"Comoros",
             @"Congo (Congo-Kinshasa)",
             @"Congo",
             @"Cook Islands",
             @"Costa Rica",
             @"Cote D'Ivoire",
             @"China",
             @"Croatia",
             @"Cuba",
             @"Czech",
             @"Cyprus",
             @"Denmark",
             @"Djibouti",
             @"Dominica",
             @"East Timor",
             @"Ecuador",
             @"Egypt",
             @"Equatorial Guinea",
             @"Eritrea",
             @"Estonia",
             @"Ethiopia",
             @"Faroe Islands",
             @"Fiji",
             @"Finland",
             @"France",
             @"Franch Metropolitan",
             @"Franch Guiana",
             @"French Polynesia",
             @"Gabon",
             @"Gambia",
             @"Georgia",
             @"Germany",
             @"Ghana",
             @"Gibraltar",
             @"Greece",
             @"Grenada",
             @"Guadeloupe",
             @"Guam",
             @"Guatemala",
             @"Guernsey",
             @"Guinea-Bissau",
             @"Guinea",
             @"Guyana",
             @"Hong Kong",
             @"Haiti",
             @"Honduras",
             @"Hungary",
             @"Iceland",
             @"India",
             @"Indonesia",
             @"Iran",
             @"Iraq",
             @"Ireland",
             @"Isle of Man",
             @"Israel",
             @"Italy",
             @"Jamaica",
             @"Japan",
             @"Jersey",
             @"Jordan",
             @"Kazakstan",
             @"Kenya",
             @"Kiribati",
             @"Korea (South)",
             @"Korea (North)",
             @"Kuwait",
             @"Kyrgyzstan",
             @"Laos",
             @"Latvia",
             @"Lebanon",
             @"Lesotho",
             @"Liberia",
             @"Libya",
             @"Liechtenstein",
             @"Lithuania",
             @"Luxembourg",
             @"Macau",
             @"Macedonia",
             @"Malawi",
             @"Malaysia",
             @"Madagascar",
             @"Maldives",
             @"Mali",
             @"Malta",
             @"Marshall Islands",
             @"Martinique",
             @"Mauritania",
             @"Mauritius",
             @"Mayotte",
             @"Mexico",
             @"Micronesia",
             @"Moldova",
             @"Monaco",
             @"Mongolia",
             @"Montenegro",
             @"Montserrat",
             @"Morocco",
             @"Mozambique",
             @"Myanmar",
             @"Namibia",
             @"Nauru",
             @"Nepal",
             @"Netherlands",
             @"New Caledonia",
             @"New Zealand",
             @"Nicaragua",
             @"Niger",
             @"Nigeria",
             @"Niue",
             @"Norfolk Island",
             @"Norway",
             @"Oman",
             @"Pakistan",
             @"Palau",
             @"Palestine",
             @"Panama",
             @"Papua New Guinea",
             @"Paraguay",
             @"Peru",
             @"Philippines",
             @"Pitcairn Islands",
             @"Poland",
             @"Portugal",
             @"Puerto Rico",
             @"Qatar",
             @"Reunion",
             @"Romania",
             @"Rwanda",
             @"Russian Federation",
             @"Saint Helena",
             @"Saint Kitts-Nevis",
             @"Saint Lucia",
             @"Saint Vincent and the Grenadines",
             @"El Salvador",
             @"Samoa",
             @"San Marino",
             @"Sao Tome and Principe",
             @"Saudi Arabia",
             @"Senegal",
             @"Seychelles",
             @"Sierra Leone",
             @"Singapore",
             @"Serbia",
             @"Slovakia",
             @"Slovenia",
             @"Solomon Islands",
             @"Somalia",
             @"South Africa",
             @"Spain",
             @"Sri Lanka",
             @"Sudan",
             @"Suriname",
             @"Swaziland",
             @"Sweden",
             @"Switzerland",
             @"Syria",
             @"Tajikistan",
             @"Tanzania",
             @"Taiwan",
             @"Thailand",
             @"Trinidad and Tobago",
             @"Timor-Leste",
             @"Togo",
             @"Tokelau",
             @"Tonga",
             @"Tunisia",
             @"Turkey",
             @"Turkmenistan",
             @"Tuvalu",
             @"Uganda",
             @"Ukraine",
             @"United Arab Emirates",
             @"United Kingdom",
             @"United States",
             @"Uruguay",
             @"Uzbekistan",
             @"Vanuatu",
             @"Vatican City",
             @"Venezuela",
             @"Vietnam",
             @"Wallis and Futuna",
             @"Western Sahara",
             @"Yemen",
             @"Yugoslavia",
             @"Zambia",
             @"Zimbabwe"
             ];
}
 */

@end
