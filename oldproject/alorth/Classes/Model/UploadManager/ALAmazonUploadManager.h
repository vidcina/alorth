//
//  AmazonGettingCredentialsHelper.h
//  AmazonGettingCredtialsHelper
//
//  Created by Chih-Ju Huang on 2015/9/14.
//  Copyright (c) 2015年 Chih-Ju Huang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ALAmazonUploadManager : NSObject

+(ALAmazonUploadManager *)sharedManager;
-(void)uploadImageFilePath:(NSString *)localPath
          progressCallback:(void (^)(float))progressCallback
           completionBlock:(void (^)(NSString *uploadedURLString))completionBlock;

-(void)uploadVideoFilePath:(NSString *)localPath
          progressCallback:(void (^)(float))progressCallback
           completionBlock:(void (^)(NSString *uploadedURLString))completionBlock;

+(NSString *)timeStampString;

-(void)example;

@end
