//
//  AmazonGettingCredentialsHelper.m
//  AmazonGettingCredtialsHelper
//
//  Created by Chih-Ju Huang on 2015/9/14.
//  Copyright (c) 2015年 Chih-Ju Huang. All rights reserved.
//

#import "ALAmazonUploadManager.h"
#import <AWSS3/AWSS3.h>
#import <AWSCore/AWSCore.h>

static NSString *OSS_ACCESS_ID = @"us-east-1:55c8c671-bc1d-447e-81cd-0e378503dfdf";
static NSString *kALBucketName = @"alorth";
static NSString *kALVideoDirName = @"video";
static NSString *kALImageDirName = @"image";

#define kTimeFormat @"yyyy'-'MM'-'dd'_'HHmmssSSS"

@implementation ALAmazonUploadManager

+(ALAmazonUploadManager *)sharedManager
{
    static ALAmazonUploadManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[ALAmazonUploadManager alloc] init];
    });
    return sharedManager;
}

-(void)example
{    
    NSString *localPath = [[NSBundle mainBundle]pathForResource:@"test" ofType:@"txt"];
    NSString *targetPath = [self timeStampName:@"test" fileType:@"txt"];
    [self uploadMedia:targetPath localPath:localPath progressCallback:nil completionBlock:^(NSString *uploadedURLString) {
        
    }];
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        [self initOSSFile];
    }
    return self;
}

- (void)initOSSFile
{
    AWSCognitoCredentialsProvider *credentialsProvider
    = [[AWSCognitoCredentialsProvider alloc] initWithRegionType:AWSRegionUSEast1
                                                 identityPoolId:@"us-east-1:55c8c671-bc1d-447e-81cd-0e378503dfdf"];
    
    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSEast1
                                                                         credentialsProvider:credentialsProvider];
    
    [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;
}

-(void)uploadImageFilePath:(NSString *)localPath
          progressCallback:(void (^)(float))progressCallback
           completionBlock:(void (^)(NSString *uploadedURLString))completionBlock
{
    NSString *targetKeyPath = [self timeStampName:kALImageDirName fileType:@"jpg"];
    [self uploadMedia:targetKeyPath localPath:localPath progressCallback:progressCallback completionBlock:completionBlock];
}

-(void)uploadVideoFilePath:(NSString *)localPath
          progressCallback:(void (^)(float))progressCallback
           completionBlock:(void (^)(NSString *uploadedURLString))completionBlock
{
    NSString *targetKeyPath = [self timeStampName:kALVideoDirName fileType:@"mp4"];
    [self uploadMedia:targetKeyPath localPath:localPath progressCallback:progressCallback completionBlock:completionBlock];
}

-(void)uploadMedia:(NSString *)targetKeyPath
         localPath:(NSString *)localPath
  progressCallback:(void (^)(float))progressCallback
   completionBlock:(void (^)(NSString *uploadedURLString))completionBlock
{
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
    AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
    uploadRequest.bucket = kALBucketName;
    uploadRequest.key = targetKeyPath;
    uploadRequest.body = [NSURL fileURLWithPath:localPath];
    
    [[transferManager upload:uploadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor]
                                                       withBlock:^id(AWSTask *task) {
        if (task.error) {
            if ([task.error.domain isEqualToString:AWSS3TransferManagerErrorDomain]) {
                switch (task.error.code) {
                    case AWSS3TransferManagerErrorCancelled:
                    case AWSS3TransferManagerErrorPaused:
                        completionBlock(nil);
                        break;
                        
                    default:
                        NSLog(@"Error: %@", task.error);
                        completionBlock(nil);
                        break;
                }
            } else {
                // Unknown error.
                NSLog(@"Error: %@", task.error);
                completionBlock(nil);
            }
        }
        
        if (task.result) {
//            AWSS3TransferManagerUploadOutput *uploadOutput = task.result;
            // The file uploaded successfully.
//            NSString *uploadedPath = [NSString stringWithFormat:@"https://s3.amazonaws.com/alorth/%@", targetKeyPath];
            NSString *uploadedPath = [NSString stringWithFormat:@"https://daj84lxobzfcg.cloudfront.net/%@", targetKeyPath];
            completionBlock(uploadedPath);
            return nil;
        }
        completionBlock(nil);
        return nil;
    }];
}

-(NSString *)timeStampName:(NSString *)dirName fileType:(NSString *)type
{
    //使用者ID
    NSString *userID = [ALAppDelegate sharedAppDelegate].userID;
    NSString *timeStampString = [[self class] timeStampString];
    return [NSString stringWithFormat:@"Inventory/%@/%@/%@.%@",dirName,userID,timeStampString,type];
}

+(NSString *)timeStampString
{
    //上傳時間
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setCalendar:[[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian]];
    dateFormater.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    [dateFormater setDateFormat:[NSString stringWithFormat:kTimeFormat]];
    NSString *timeString = [dateFormater stringFromDate:[NSDate date]];
    
    //隨機數字
    int arcNumber = arc4random_uniform(9999);
    
    return [NSString stringWithFormat:@"%@_%4d",timeString,arcNumber];
}

@end

