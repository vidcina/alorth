//
//  ALStripeInfo.h
//  alorth
//
//  Created by w91379137 on 2015/12/17.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PDSDeviceAuthorized.h"

typedef NS_ENUM(NSInteger, StripeStatus) {
    StripeStatusUnknow = 0,
    StripeStatusNo = 1,
    StripeStatusAuthorize = 2
};

typedef NS_ENUM(NSInteger, CreditCardStatus) {
    CreditCardStatusUnknow = 0,
    CreditCardStatusNo = 1,
    CreditCardStatusAuthorize = 2 //至少一張
};

@interface ALStripeInfo : NSObject

#pragma mark - 賣家
- (void)detectStripeStatusCompletionBlock:(void (^)())block;
@property (nonatomic) StripeStatus stripeStatus;
+ (PDSDeviceAuthorizedStatus)stripeStatus;//轉格式

#pragma mark - 買家
- (void)detectCreditCardStatusCompletionBlock:(void (^)())block;
@property (nonatomic) CreditCardStatus creditCardStatus;
@property (nonatomic, strong) NSArray *creditCardInfoArray;
- (NSString *)firstCardType;
//+ (PDSDeviceAuthorizedStatus)creditStatus;//轉格式

@end
