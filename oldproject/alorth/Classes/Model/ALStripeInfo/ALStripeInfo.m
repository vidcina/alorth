//
//  ALStripeInfo.m
//  alorth
//
//  Created by w91379137 on 2015/12/17.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALStripeInfo.h"

@implementation ALStripeInfo

#pragma mark - init
- (instancetype)init
{
    self = [super init];
    if (self) {
        NSLog(@"%s",__func__);
    }
    return self;
}

-(void)dealloc
{
    NSLog(@"%s",__func__);
}

#pragma mark - 賣家
- (void)detectStripeStatusCompletionBlock:(void (^)())block
{
    if (!block) {
        block = ^{};
    }
    
    [ALNetWorkAPI stripeIsAuthorizeA073CompletionBlock:^(NSDictionary *responseObject) {
       
        if ([ALNetWorkAPI checkSerialNumber:@"A073"
                             ResponseObject:responseObject]) {
            if ([responseObject[@"resBody"][@"isConnected"] intValue] == 0) {
                self.stripeStatus = StripeStatusNo;
            }
            else {
                self.stripeStatus = StripeStatusAuthorize;
            }
        }
        else {
            self.stripeStatus = StripeStatusNo;
        }
        
        block();
    }];
}

+ (PDSDeviceAuthorizedStatus)stripeStatus
{
    switch ([PDSAccountManager sharedManager].stripeInfo.stripeStatus) {
        case StripeStatusUnknow: return Unknown;
        case StripeStatusNo: return Unauthorized;
        case StripeStatusAuthorize: return Authorized;
    }
    return Unknown;
}

#pragma mark - 買家
- (void)detectCreditCardStatusCompletionBlock:(void (^)())block
{
    if (!block) {
        block = ^{};
    }
    
    [ALNetWorkAPI creditCardIsAuthorizeA076CompletionBlock:^(NSDictionary *responseObject) {
        
        if ([ALNetWorkAPI checkSerialNumber:@"A076"
                             ResponseObject:responseObject]) {
            
            BOOL isWishData = YES;
            
            if (isWishData &&
                [responseObject[@"resBody"][@"isConnected"] intValue] == 0) {
                isWishData = NO;
            }
            if (isWishData &&
                ![responseObject[@"resBody"][@"CardList"] isKindOfClass:[NSArray class]]) {
                isWishData = NO;
            }
            
            if (isWishData) {
                self.creditCardInfoArray = responseObject[@"resBody"][@"CardList"];
                self.creditCardStatus = CreditCardStatusAuthorize;//這個會觸發kvo抓資料 其他資訊應該 優先載入
            }
            else {
                self.creditCardStatus = CreditCardStatusNo;
            }
        }
        else {
            self.creditCardStatus = CreditCardStatusNo;
        }
        
        block();
    }];
}

- (NSString *)firstCardType
{
    if ([self.creditCardInfoArray isKindOfClass:[NSArray class]]) {
        NSDictionary *firstCard = self.creditCardInfoArray.firstObject;
        return firstCard[@"CardType"];
    }
    return nil;
}

//+ (PDSDeviceAuthorizedStatus)creditStatus
//{
//    switch ([PDSAccountManager sharedManager].stripeInfo.creditCardStatus) {
//        case CreditCardStatusUnknow: return Unknown;
//        case CreditCardStatusNo: return Unauthorized;
//        case CreditCardStatusAuthorize: return Authorized;
//    }
//    return Unknown;
//}

@end
