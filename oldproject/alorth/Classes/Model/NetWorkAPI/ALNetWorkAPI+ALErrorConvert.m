//
//  ALNetWorkAPI+ALErrorConvert.m
//  alorth
//
//  Created by w91379137 on 2015/11/17.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALNetWorkAPI+ALErrorConvert.h"

@implementation ALNetWorkAPI (ALErrorConvert)

+ (NSString *)phpHeaderError:(NSString *)inputString
{
    while ([inputString rangeOfString:@"<div"].length &&
        [inputString rangeOfString:@"</div>"].length ) {
        
        NSInteger k = [inputString rangeOfString:@"</div>"].length + [inputString rangeOfString:@"</div>"].location;
        inputString = [inputString substringFromIndex:k];
        //inputString = [inputString stringByReplacingOccurrencesOfString:@"\n" withString:@""]; //非必要
    }
    return inputString;
}

@end
