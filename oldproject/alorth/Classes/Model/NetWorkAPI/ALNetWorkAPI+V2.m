//
//  ALNetWorkAPI+V2.m
//  alorth
//
//  Created by w91379137 on 2015/9/20.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALNetWorkAPI+V2.h"
#import "ALNetWorkAPI+PostBasic.h"

@implementation ALNetWorkAPI (V2)

// [A034] 取得商品修改資訊
+(void)getEditProductInformationA034ProductID:(NSString *)productID
                             CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:productID forKey:@"ProductID"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/goods/api"]
               Header:[self reqHeaderDict:@"A034"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A035] 修改商品
+(void)editProductA035ProductID:(NSString *)productID
                   ProductImage:(NSString *)productImage
             ProductDescription:(NSString *)productDescription
                           Tags:(NSArray *)tags
                CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:productID forKey:@"ProductID"];
    [reqBodyDict setObjectEmptyStringIfNil:productImage forKey:@"ProductImage"];
    [reqBodyDict setObjectEmptyStringIfNil:productDescription forKey:@"ProductDescription"];
    [reqBodyDict setObjectEmptyStringIfNil:tags forKey:@"Tags"];
 
    [self postAddress:[API_domain stringByAppendingString:@"/goods/api"]
               Header:[self reqHeaderDict:@"A035"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A036] 檢視編輯過此商品資訊
+(void)reviewEditProductListA036ProductID:(NSString *)productID
                             StartNum:(NSString *)startNum
                      CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:productID forKey:@"ProductID"];
    [reqBodyDict setObjectEmptyStringIfNil:startNum forKey:@"StartNum"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/goods/api"]
               Header:[self reqHeaderDict:@"A036"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A037] 此商品賣掉統計
+(void)reportProductSalesListA037ProductID:(NSString *)productID
                                  StartNum:(NSString *)startNum
                           CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:productID forKey:@"ProductID"];
    [reqBodyDict setObjectEmptyStringIfNil:startNum forKey:@"StartNum"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/order/api"]
               Header:[self reqHeaderDict:@"A037"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

@end
