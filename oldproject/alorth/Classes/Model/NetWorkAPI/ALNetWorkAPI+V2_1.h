//
//  ALNetWorkAPI+V2_1.h
//  alorth
//
//  Created by w91379137 on 2015/10/8.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALNetWorkAPI.h"

//標準錯誤回應
#define ALNetworkErrorLocalizedString(serialNumber, responseObject) [ALNetWorkAPI errorMsgSerialNumber:serialNumber ResponseObject:responseObject]

//客製化錯誤回應
#define ALErrorLocalizedString(errorMag ,serialNumber, responseObject) [NSString stringWithFormat:@"%@(%@)",errorMag,[ALNetWorkAPI errorMsgSerialNumber:serialNumber ResponseObject:responseObject]]

@interface ALNetWorkAPI (V2_1)

+ (BOOL)checkSerialNumber:(NSString *)serialNumber
           ResponseObject:(NSDictionary *)responseObject;

+ (NSString *)errorMsgSerialNumber:(NSString *)serialNumber
                    ResponseObject:(NSDictionary *)responseObject;

#pragma mark -
+ (void)accountFollowingListA038StartNum:(NSString *)startNum
                         CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)accountFollowersListA039StartNum:(NSString *)startNum
                         CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)changeFollowStatusA040UserID:(NSString *)userID
                        FollowStatus:(NSString *)followStatus
                     CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)earnGrossSalesListA041StartNum:(NSString *)startNum
                       CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)findGoodsToSaleListA042Keyword:(NSString *)keyword
                                 Order:(NSString *)order
                              StartNum:(NSString *)startNum
                       CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)sellingCommissionListA043StartNum:(NSString *)startNum
                          CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)creatSellingCommissionA044ProductID:(NSString *)productID
                                ProductName:(NSString *)productName
                               ProductOwner:(NSString *)productOwner
                             EarnPercentage:(NSString *)earnPercentage
                           CommissionStatus:(NSString *)commissionStatus
                          CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)agreeCommissionListA045StartNum:(NSString *)startNum
                        CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)checkAgreeCommissionA046ProductID:(NSString *)productID
                              ProductName:(NSString *)productName
                        ApplicationUserID:(NSString *)applicationUserID
                           EarnPercentage:(NSString *)earnPercentage
                         CommissionStatus:(NSString *)commissionStatus
                          CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

#pragma mark - A047 暫時不用
+ (void)earnGrossSalesListA048ProductOwner:(NSString *)productOwner
                                  SellerID:(NSString *)sellerID
                                  StartNum:(NSString *)startNum
                           CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)uploadStreamImageA049StreamID:(NSString *)streamID
                          StreamImage:(NSString *)streamImage
                      CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)getStreamListA050StartNum:(NSString *)startNum
                  CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)getStreamListA051GPSLongitude1:(NSString *)longitude1
                          GPSLatitude1:(NSString *)latitude1
                         GPSLongitude2:(NSString *)longitude2
                          GPSLatitude2:(NSString *)longtide1
                       CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)getStreamProductA052ProductID:(NSString *)productID
                             StreamID:(NSString *)streamID
                      CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)reportStreamA053StreamID:(NSString *)streamID
                 CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

/*
//廢棄
+ (void)createStreamOrderA054UserID:(NSString *)userID
                           StreamID:(NSString *)streamID
                          ProductID:(NSString *)productID
                            StockID:(NSString *)stockID
                           SellerID:(NSString *)sellerID
                        OrderAmount:(NSString *)orderAmount
                            PayType:(NSString *)payType
                    CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;
 */

+ (void)getStreamProductA055Type:(NSString *)type
                        StartNum:(NSString *)startNum
                 CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)createStreamA056Name:(NSString *)name
                   Longitude:(NSString *)longitude
                     Laitude:(NSString *)laitude
                 ProductList:(NSArray *)productList
              StreamLanguage:(NSString *)streamLanguage
             CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)getStreamStatusA057StreamID:(NSString *)streamID
                    CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)closeStreamA058StreamID:(NSString *)streamID
                     ElapseTime:(NSString *)elapseTime
                CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)nextStreamA059StreamID:(NSString *)streamID
               CurrentPriority:(NSString *)currentPriority
                    ElapseTime:(NSString *)elapseTime
               CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)personalStreamListA060UserID:(NSString *)userID
                     CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)editStreamListA061CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)deleteStreamA062StreamID:(NSString *)streamID
                 CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)searchPersonA063Keyword:(NSString *)keyword
                       StartNum:(NSString *)startNum
                CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;
#pragma mark - A064
#pragma mark - A065 暫時不用

+ (void)streamInfomationA066StreamID:(NSString *)streamID
                     CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)rearrangeStreamListA067StreamList:(NSArray *)streamList
                          CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)followStatusA068UserID:(NSString *)userID
               CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)accountFollowingListA069UserID:(NSString *)userID
                              StartNum:(NSString *)startNum
                         CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)accountFollowersListA070UserID:(NSString *)userID
                              StartNum:(NSString *)startNum
                         CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)stripeConnectURLA071CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)finishStripeConnect:(NSDictionary *)stripeResponseJSON
            CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)stripeIsAuthorizeA073CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)streamCheckoutA074ApplicationID:(NSString *)applicationID
                               StreamID:(NSString *)streamID
                              ProductID:(NSString *)productID
                                StockID:(NSString *)stockID
                               SellerID:(NSString *)sellerID
                            OrderAmount:(NSString *)orderAmount
                             TotalPrice:(NSString *)totalPrice
                        CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)creditCardTokenA075StripeToken:(NSString *)stripeToken
                            CardNumber:(NSString *)cardNumber
                              CardType:(NSString *)cardType
                                  UUID:(NSString *)uuID
                                Stauts:(NSString *)stauts
                       CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)creditCardIsAuthorizeA076CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)versionInformationA077CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)userStreamVideoStatusA078CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;


@end
