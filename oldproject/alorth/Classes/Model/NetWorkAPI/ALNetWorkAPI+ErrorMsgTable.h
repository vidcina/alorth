//
//  ALNetWorkAPI+ErrorMsgTable.h
//  alorth
//
//  Created by w91379137 on 2016/2/29.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALNetWorkAPI.h"

#define StripeLowPriceAlert @"Total price is too low to checkout by Stripe."
#define StockoutString @"There is not much stock in the shop now."

@interface ALNetWorkAPI (ErrorMsgTable)

+ (NSString *)errorMsgTableSerialNumber:(NSString *)serialNumber
                             ResultCode:(NSString *)resultCode;

@end
