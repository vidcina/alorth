//
//  ALNetWorkAPI+V3_0.h
//  alorth
//
//  Created by w91379137 on 2016/1/31.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALNetWorkAPI.h"

typedef NS_ENUM(NSUInteger, ALShippingMethod) {
    ALShippingMethodFree = 1,
    ALShippingMethodFlateRate,
    ALShippingMethodCalcuated
};

@interface ALNetWorkAPI (V3_0)

+ (void)cartAddV1ProductA079ProductID:(NSString *)productID
                              StockID:(NSString *)stockID
                             SellerID:(NSString *)sellerID
                          OrderAmount:(NSString *)orderAmount
                      CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)cartAddV2ProductA080UserID:(NSString *)userID
                          StreamID:(NSString *)streamID
                         ProductID:(NSString *)productID
                           StockID:(NSString *)stockID
                          SellerID:(NSString *)sellerID
                       OrderAmount:(NSString *)orderAmount
                   CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)inCartListA081CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)deleteInCartItemA082CartItemID:(NSString *)cartItemID
                       CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)changeInCartToSavedA083CartItemID:(NSString *)cartItemID
                          CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)checkoutInCartItemA084CartItems:(NSArray *)cartItems
                        CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)checkoutCartItemA085CartItemID:(NSString *)cartItemID
                       CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)detailCartItemA086CartItemID:(NSString *)cartItemID
                     CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)changeCartItemInfomationA087CartItemID:(NSString *)cartItemID
                                        IsUsed:(NSString *)isUsed
                                          Size:(NSString *)size
                                        Amount:(NSString *)amount
                               CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)savedListA088CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)changeSavedToInCartA089CartItemID:(NSString *)cartItemID
                          CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)searchStreamingA090StartNum:(NSString *)startNum
                            Keyword:(NSString *)keyword
                               Sort:(NSString *)sort
                           Language:(NSArray *)language
                           Category:(NSArray *)category
                           Location:(NSArray *)location
                    CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)searchProductA091StartNum:(NSString *)startNum
                          Keyword:(NSString *)keyword
                             Sort:(NSString *)sort
                         Category:(NSArray *)category
                  CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)allLanguageListA092CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)allLocationListA093CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)renewCartA094CartStatus:(NSString *)cartStatus
                CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)streamBuyCountA118StreamID:(NSString *)streamID
                   CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)fetchExistedAddressA110CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)addAddressA111WithFirstName:(NSString *)firstName
                           lastName:(NSString *)lastName
                            street1:(NSString *)street1
                            street2:(NSString *)street2
                              state:(NSString *)state
                               city:(NSString *)city
                         postalCode:(NSString *)postalCode
                              phone:(NSString *)phoneNumber
                            country:(NSString *)country
                            primary:(BOOL)isPrimary
                    completionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)fetchShipFeeA112CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)updateDomesticShipFeeA113WithMethod:(ALShippingMethod)method
                                   flatRate:(NSString *)rate
                            completionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)updateInternationalShipFeeA113WithMethod:(ALShippingMethod)method
                                   flatRate:(NSString *)rate
                                  customLocation:(NSString *)location
                            completionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)fetchTaxA114CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)updateTaxA115WithToBeEnable:(BOOL)enabled withUSTax:(BOOL)enableUSTax withEUTax:(BOOL)enableEUTax completionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)fetchProductCurrencyA116CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)updateProductCurrencyA117WithString:(NSString *)currency completionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)deleteAddressA201WithId:(NSString *)addressId completionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+ (void)setPrimaryAddressA202WithId:(NSString *)addressId completionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

@end
