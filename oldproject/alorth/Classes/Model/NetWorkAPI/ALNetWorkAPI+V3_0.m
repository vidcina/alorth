//
//  ALNetWorkAPI+V3_0.m
//  alorth
//
//  Created by w91379137 on 2016/1/31.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALNetWorkAPI+V3_0.h"
#import "ALNetWorkAPI+PostBasic.h"

@implementation ALNetWorkAPI (V3_0)

+ (void)cartAddV1ProductA079ProductID:(NSString *)productID
                              StockID:(NSString *)stockID
                             SellerID:(NSString *)sellerID
                          OrderAmount:(NSString *)orderAmount
                      CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:productID forKey:@"ProductID"];
    [reqBodyDict setObjectEmptyStringIfNil:stockID forKey:@"StockID"];
    [reqBodyDict setObjectEmptyStringIfNil:sellerID forKey:@"SellerID"];
    [reqBodyDict setObjectEmptyStringIfNil:orderAmount forKey:@"OrderAmount"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/cart/api"]
               Header:[self reqHeaderDict:@"A079"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)cartAddV2ProductA080UserID:(NSString *)userID
                          StreamID:(NSString *)streamID
                         ProductID:(NSString *)productID
                           StockID:(NSString *)stockID
                          SellerID:(NSString *)sellerID
                       OrderAmount:(NSString *)orderAmount
                   CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:userID forKey:@"UserID"];
    [reqBodyDict setObjectEmptyStringIfNil:streamID forKey:@"StreamID"];
    [reqBodyDict setObjectEmptyStringIfNil:productID forKey:@"ProductID"];
    [reqBodyDict setObjectEmptyStringIfNil:stockID forKey:@"StockID"];
    [reqBodyDict setObjectEmptyStringIfNil:sellerID forKey:@"SellerID"];
    [reqBodyDict setObjectEmptyStringIfNil:orderAmount forKey:@"OrderAmount"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/cart/api"]
               Header:[self reqHeaderDict:@"A080"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)inCartListA081CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    
    [self postAddress:[API_domain stringByAppendingString:@"/cart/api"]
               Header:[self reqHeaderDict:@"A081"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)deleteInCartItemA082CartItemID:(NSString *)cartItemID
                       CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:cartItemID forKey:@"CartItemID"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/cart/api"]
               Header:[self reqHeaderDict:@"A082"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)changeInCartToSavedA083CartItemID:(NSString *)cartItemID
                          CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:cartItemID forKey:@"CartItemID"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/cart/api"]
               Header:[self reqHeaderDict:@"A083"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)checkoutInCartItemA084CartItems:(NSArray *)cartItems
                        CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:cartItems forKey:@"CartItems"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/cart/api"]
               Header:[self reqHeaderDict:@"A084"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)checkoutCartItemA085CartItemID:(NSString *)cartItemID
                       CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:cartItemID forKey:@"CartItemID"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/cart/api"]
               Header:[self reqHeaderDict:@"A085"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)detailCartItemA086CartItemID:(NSString *)cartItemID
                     CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:cartItemID forKey:@"CartItemID"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/cart/api"]
               Header:[self reqHeaderDict:@"A086"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)changeCartItemInfomationA087CartItemID:(NSString *)cartItemID
                                        IsUsed:(NSString *)isUsed
                                          Size:(NSString *)size
                                        Amount:(NSString *)amount
                               CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:cartItemID forKey:@"CartItemID"];
    [reqBodyDict setObjectEmptyStringIfNil:isUsed forKey:@"IsUsed"];
    [reqBodyDict setObjectEmptyStringIfNil:size forKey:@"Size"];
    [reqBodyDict setObjectEmptyStringIfNil:amount forKey:@"Amount"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/cart/api"]
               Header:[self reqHeaderDict:@"A087"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)savedListA088CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    
    [self postAddress:[API_domain stringByAppendingString:@"/cart/api"]
               Header:[self reqHeaderDict:@"A088"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)changeSavedToInCartA089CartItemID:(NSString *)cartItemID
                          CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:cartItemID forKey:@"CartItemID"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/cart/api"]
               Header:[self reqHeaderDict:@"A089"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)searchStreamingA090StartNum:(NSString *)startNum
                            Keyword:(NSString *)keyword
                               Sort:(NSString *)sort
                           Language:(NSArray *)language
                           Category:(NSArray *)category
                           Location:(NSArray *)location
                    CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:startNum forKey:@"StartNum"];
    if (keyword)[reqBodyDict setObjectEmptyStringIfNil:keyword forKey:@"Keyword"];
    [reqBodyDict setObjectEmptyStringIfNil:sort forKey:@"Sort"];
    [reqBodyDict setObjectEmptyStringIfNil:language forKey:@"Language"];
    [reqBodyDict setObjectEmptyStringIfNil:category forKey:@"Category"];
    [reqBodyDict setObjectEmptyStringIfNil:location forKey:@"Location"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/search/api"]
               Header:[self reqHeaderDict:@"A090"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)searchProductA091StartNum:(NSString *)startNum
                          Keyword:(NSString *)keyword
                             Sort:(NSString *)sort
                         Category:(NSArray *)category
                  CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:startNum forKey:@"StartNum"];
    if (keyword)[reqBodyDict setObjectEmptyStringIfNil:keyword forKey:@"Keyword"];
    [reqBodyDict setObjectEmptyStringIfNil:sort forKey:@"Sort"];
    [reqBodyDict setObjectEmptyStringIfNil:category forKey:@"Category"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/search/api"]
               Header:[self reqHeaderDict:@"A091"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)allLanguageListA092CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    
    [self postAddress:[API_domain stringByAppendingString:@"/search/api"]
               Header:[self reqHeaderDict:@"A092"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)allLocationListA093CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    
    [self postAddress:[API_domain stringByAppendingString:@"/search/api"]
               Header:[self reqHeaderDict:@"A093"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)renewCartA094CartStatus:(NSString *)cartStatus
                CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:cartStatus forKey:@"CartStatus"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/cart/api"]
               Header:[self reqHeaderDict:@"A094"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)streamBuyCountA118StreamID:(NSString *)streamID
                   CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:streamID forKey:@"StreamID"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/stream/api"]
               Header:[self reqHeaderDict:@"A118"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)fetchExistedAddressA110CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSDictionary *reqBodyDict = [NSDictionary dictionary];
    
    // (howard): Success response
    //        City = "";
    //        Country = "";
    //        Phone = "";
    //        PostalCode = "";
    //        ResultCode = 0;
    //        State = "";
    //        Street1 = "";
    //        Street2 = "";
    //        UserFirstName = "";
    //        UserLastName = "";
    //        FullAddress = "";
    //        IsPrimary = 0 or 1;
    
    [self postAddress:[API_domain stringByAppendingString:@"/setting/api"]
               Header:[self reqHeaderDict:@"A110"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)addAddressA111WithFirstName:(NSString *)firstName
                           lastName:(NSString *)lastName
                            street1:(NSString *)street1
                            street2:(NSString *)street2
                              state:(NSString *)state
                               city:(NSString *)city
                         postalCode:(NSString *)postalCode
                              phone:(NSString *)phoneNumber
                            country:(NSString *)country
                            primary:(BOOL)isPrimary
                    completionBlock:(void (^)(NSDictionary *))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:firstName forKey:@"UserFirstName"];
    [reqBodyDict setObjectEmptyStringIfNil:lastName forKey:@"UserLastName"];
    [reqBodyDict setObjectEmptyStringIfNil:street1 forKey:@"Street1"];
    [reqBodyDict setObjectEmptyStringIfNil:street2 forKey:@"Street2"];
    [reqBodyDict setObjectEmptyStringIfNil:city forKey:@"City"];
    [reqBodyDict setObjectEmptyStringIfNil:postalCode forKey:@"PostalCode"];
    [reqBodyDict setObjectEmptyStringIfNil:phoneNumber forKey:@"Phone"];
    [reqBodyDict setObjectEmptyStringIfNil:country forKey:@"Country"];
    [reqBodyDict setObject:@(isPrimary) forKey:@"IsPrimary"];

    [self postAddress:[API_domain stringByAppendingString:@"/setting/api"]
               Header:[self reqHeaderDict:@"A111"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)fetchShipFeeA112CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSDictionary *reqBodyDict = [NSDictionary dictionary];
    
    // (howard): Success response
    //        DomesticShipping = 0;
    //        InternationalShipping = 0;
    //        ResultCode = 0;
    
    [self postAddress:[API_domain stringByAppendingString:@"/setting/api"]
               Header:[self reqHeaderDict:@"A112"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)updateDomesticShipFeeA113WithMethod:(ALShippingMethod)method
                                   flatRate:(NSString *)rate
                            completionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObject:@"1" forKey:@"DomesticShipping"];
    [reqBodyDict setObject:@"0" forKey:@"InternationalShipping"];
    [reqBodyDict setObject:[NSString stringWithFormat:@"%zd", method] forKey:@"DomesticMethod"];
    if (method == ALShippingMethodFlateRate) {
        [reqBodyDict setObjectEmptyStringIfNil:rate forKey:@"DomesticFlatRate"];
    }
    
    [self postAddress:[API_domain stringByAppendingString:@"/setting/api"]
               Header:[self reqHeaderDict:@"A113"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)updateInternationalShipFeeA113WithMethod:(ALShippingMethod)method
                                        flatRate:(NSString *)rate
                                  customLocation:(NSString *)location
                                 completionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObject:@"1" forKey:@"InternationalShipping"];
    [reqBodyDict setObject:@"0" forKey:@"DomesticShipping"];
    [reqBodyDict setObject:[NSString stringWithFormat:@"%zd", method] forKey:@"InternationalMethod"];
    if (method == ALShippingMethodFlateRate) {
        [reqBodyDict setObjectEmptyStringIfNil:rate forKey:@"InternationalFlatRate"];
    }
    if (location.length == 0) {
        [reqBodyDict setObject:@"1" forKey:@"Region"];
    }
    else {
        [reqBodyDict setObject:@"2" forKey:@"Region"];
        [reqBodyDict setObjectEmptyStringIfNil:location forKey:@"Location"];
    }
    
    [self postAddress:[API_domain stringByAppendingString:@"/setting/api"]
               Header:[self reqHeaderDict:@"A113"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)fetchTaxA114CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSDictionary *reqBodyDict = [NSDictionary dictionary];
    
    // (howard): Success response
    //        AutoTaxSetting = 0;
    //        ResultCode = 0;
    
    [self postAddress:[API_domain stringByAppendingString:@"/setting/api"]
               Header:[self reqHeaderDict:@"A114"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)updateTaxA115WithToBeEnable:(BOOL)enabled withUSTax:(BOOL)enableUSTax withEUTax:(BOOL)enableEUTax completionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObject:[NSString stringWithFormat:@"%d", enabled] forKey:@"AutoTaxSetting"];
    
    if (enabled) {
        [reqBodyDict setObject:[NSString stringWithFormat:@"%d", enableUSTax] forKey:@"UsaTax"];
        [reqBodyDict setObject:[NSString stringWithFormat:@"%d", enableEUTax] forKey:@"EuTax"];
    }
    
    [self postAddress:[API_domain stringByAppendingString:@"/setting/api"]
               Header:[self reqHeaderDict:@"A115"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)fetchProductCurrencyA116CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSDictionary *reqBodyDict = [NSDictionary dictionary];
    
    // (howard): Success response
    //        CurrencyList =         (
    //                                USD,
    //                                EUR,
    //                                GBP
    //                                );
    //        ResultCode = 0;
    //        UserCurrency = "";
    
    [self postAddress:[API_domain stringByAppendingString:@"/setting/api"]
               Header:[self reqHeaderDict:@"A116"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)updateProductCurrencyA117WithString:(NSString *)currency completionBlock:(void (^)(NSDictionary *responseObject))completionBlock;
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:currency forKey:@"UserCurrency"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/setting/api"]
               Header:[self reqHeaderDict:@"A117"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)deleteAddressA201WithId:(NSString *)addressId completionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:addressId forKey:@"AddressID"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/setting/api"]
               Header:[self reqHeaderDict:@"A201"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)setPrimaryAddressA202WithId:(NSString *)addressId completionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:addressId forKey:@"AddressID"];

    [self postAddress:[API_domain stringByAppendingString:@"/setting/api"]
               Header:[self reqHeaderDict:@"A202"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

@end
