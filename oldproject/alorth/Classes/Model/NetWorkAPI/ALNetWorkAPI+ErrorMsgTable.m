//
//  ALNetWorkAPI+ErrorMsgTable.m
//  alorth
//
//  Created by w91379137 on 2016/2/29.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALNetWorkAPI+ErrorMsgTable.h"

@implementation ALNetWorkAPI (ErrorMsgTable)

+ (NSString *)errorMsgTableSerialNumber:(NSString *)serialNumber
                             ResultCode:(NSString *)resultCode
{
    BOOL isLoginAPI = ([serialNumber isEqualToString:@"A123"] || [serialNumber isEqualToString:@"A002"] || [serialNumber isEqualToString:@"A001"]);
    NSInteger errorCode = [maybe(resultCode, NSString) integerValue];
    switch (errorCode) {
        case 0x10:
            if (isLoginAPI) [[PDSEnvironmentViewController sharedInstance] presentLoginViewController];
            return ALLocalizedString(@"The account has been registered", nil);
            break;
        case 0x11:
            if (isLoginAPI) {
                return ALLocalizedString(@"Account not found",nil);
            }
            else {
                [[PDSEnvironmentViewController sharedInstance] presentLoginViewController];
                return @"You have logged on other device, please login again";
            }
            break;
        case 0x12:
            if (isLoginAPI) [[PDSEnvironmentViewController sharedInstance] presentLoginViewController];
            return ALLocalizedString(@"No operation permission",nil);
            break;
        case 0x13:
            if (isLoginAPI) [[PDSEnvironmentViewController sharedInstance] presentLoginViewController];
            return ALLocalizedString(@"Wrong password",nil);
            break;
        case 0x14:
            if (isLoginAPI) [[PDSEnvironmentViewController sharedInstance] presentLoginViewController];
            return ALLocalizedString(@"Invalid Token, please login again",nil);
            break;
        case 0x20:
            if (isLoginAPI) [[PDSEnvironmentViewController sharedInstance] presentLoginViewController];
            return ALLocalizedString(@"No such Product",nil);
            break;
        case 0x21:
            if (isLoginAPI) [[PDSEnvironmentViewController sharedInstance] presentLoginViewController];
            return ALLocalizedString(@"Same product already exists",nil);
            break;
        case 0x30:
            if (isLoginAPI) [[PDSEnvironmentViewController sharedInstance] presentLoginViewController];
            return ALLocalizedString(@"Parameter error",nil);
            break;
        case 0xA0:
            if (isLoginAPI) [[PDSEnvironmentViewController sharedInstance] presentLoginViewController];
            return ALLocalizedString(@"API operation error",nil);
            break;
        default:
            break;
    }
    if ([serialNumber isEqualToString:@"A009"]) {
        switch ([maybe(resultCode, NSString) integerValue]) {
            case 82:return StockoutString;
            default:return nil;
        }
    }
    else if ([serialNumber isEqualToString:@"A012"]) {
        switch ([maybe(resultCode, NSString) integerValue]) {
            case 83:return StripeLowPriceAlert; //kobe還沒附上文件
            default:return nil;
        }
    }
    else if ([serialNumber isEqualToString:@"A074"]) {
        switch ([maybe(resultCode, NSString) integerValue]) {
            case 82:return StockoutString;
            case 83:return StripeLowPriceAlert; //kobe還沒附上文件
            default:return nil;
        }
    }
    else if ([serialNumber isEqualToString:@"A078"]) {
        switch ([maybe(resultCode, NSString) integerValue]) {
            case 81:return @"You have too many videos, delete some to continue.";
            default:return nil;
        }
    }
    else {
        return nil;
    }
}

@end
