//
//  ALNetWorkAPI.h
//  alorth
//
//  Created by w91379137 on 2015/5/25.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ALNetWorkAPI : NSObject

// [A001] 註冊使用者
+(void)registerA001Account:(NSString *)account
                  password:(NSString *)password
                     email:(NSString *)email
                   country:(NSString *)country
           CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A002] 使用者登入
+(void)loginA002Account:(NSString *)account
               password:(NSString *)password
        CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A003] 使用者位置
+(void)setGPSA003Longitide:(double)lon
                  latitude:(double)lat
           CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A004] 變更密碼
+(void)chagePasswordA004WithNewPassword:(NSString *)newPassword
                        CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A005] 刊登新增庫存
+(void)addStockA005ProductID:(NSString *)productIdOrNil
                 productClip:(NSString *)productClip
                productImage:(NSString *)productImage
                 productSize:(NSString *)productSize
          productDescription:(NSString *)productDescription
                    Category:(NSString *)Category
                ProductTitle:(NSString *)ProductTitle
                        Tags:(NSArray *)Tags
             CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;


// [A006] 顯示商品列表
// Order: latest=0, BestSell=2, BestMatch=4
+(void)goodsA006Keyword:(NSString *)keywordOrNil
               Category:(NSString *)categoryOrNil
                  Order:(NSString *)orderOrNil
               StartNum:(NSString *)startNumOrNil
                 UserID:(NSString *)userIDOrNil
        CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A007] 取得商品詳細資料
+(void)getStockListA007ProductID:(NSString *)productID
                 CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A008] 檢查商品刊登狀況
+(void)checkProductExistA008ProductTitle:(NSString *)ProductTitle
                             ProductSize:(NSString *)ProductSize
                                Category:(NSString *)Category
                         CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A009] 訂購商品
+(void)orderStockA009ProductId:(NSString *)ProductId
                       StockID:(NSString *)StockID
                      SellerID:(NSString *)SellerID
                   OrderAmount:(NSString *)OrderAmount
                       PayType:(NSString *)payType
               CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A010] 修改訂單價格
+(void)changeOrderPriceA010OrderId:(NSString *)orderId
                        OrderPrice:(NSString *)OrderPrice
                        TransPrice:(NSString *)TransPrice
                   CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A011] 取消交易
+(void)cancelOrderPriceA011OrderId:(NSString *)orderId
                   CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A012] 確認交易
+(void)confirmOrderPriceA012OrderId:(NSString *)orderId
                            payType:(NSString *)payTypeOrNil
                    transactionData:(NSString *)transactionDataOrNil
                    CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A013] 評價列表
+(void)reviewListA013withUserID:(NSString *)userID
                       startNum:(NSString *)startNum
                completionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A014] 評價商品
+(void)commentStockA014StockID:(NSString *)stockID
                       orderID:(NSString *)orderID
                          Rate:(NSString *)rate
                       Comment:(NSString *)comment
                   ReviewImage:(NSString *)reviewImageOrNil
               CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A015] 回覆評價
+(void)replyCommentA015CommentID:(NSString *)commentID
                    ReplyComment:(NSString *)ReplyComment
                 CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A016] 我的公開資訊
+(void)myPublicInfoA016CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A017] 我的公開資訊
+(void)setMyPublicInfoA017PublicInfo:(NSString *)infoString
                     CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A018] 販售訂單列表
+(void)mySoldOrderListA018StartNum:(NSString *)startNumOrNil
                   CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A019] 購買訂單列表
+(void)myBoughtOrderListA019StartNum:(NSString *)startNum
                     CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A020] 個人資料
+(void)personalProfile020CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A021] 設定個人資料
+(void)setPersonalProfile021HeadImage:(NSString *)headImage
                            FirstName:(NSString *)firstName
                             LastName:(NSString *)lastName
                               Gender:(NSString *)gender
                             Birthday:(NSString *)birthday
                             Location:(NSString *)location
                              Address:(NSString *)address
                                Phone:(NSString *)phone
                           TransPrice:(NSString *)transPrice
                           PublicInfo:(NSString *)publicInfo
                      CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A022] 登出
+(void)logoutA022CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A023] 取得聊天室ID
+(void)getIMRoomIDA023ForUser1:(NSString *)userID1
                         User2:(NSString *)userID2
               completionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A024] 新增聊天室ID
+(void)createIMRoomIDA024ForUser1:(NSString *)userID1
                            User2:(NSString *)userID2
                           roomID:(NSString *)roomID
                  completionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A025] 庫存列表
+(void)stockListA025StartNum:(NSString *)startNumOrNil
             CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A026] 修改庫存
+(void)editStockA026ProductID:(NSString *)productID
                      stockID:(NSString *)stockID
                       isUsed:(NSString *)isUsed
                         size:(NSString *)size
                        price:(NSString *)price
                       amount:(NSString *)amount
              completionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A027] 刪除所有庫存
+(void)deleteAllStockA027ProductID:(NSString *)productID
                   completionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A028] 忘記密碼
+(void)forgetPasswordA028UserInfo:(NSString *)emailAddress
                  completionBlock:(void (^)(NSDictionary *responseObject))completionBlock;


// [A029] 取得分類子規格
+(void)subCategoryListA029ByCategoryIndex:(NSString *)categoryIndex
                   CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A030] 取得商品分類
+(void)categoryListA030CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A031] 取得商品評價
+(void)categoryCommentA031OrderID:(NSString *)orderID
                         startNum:(NSString *)startNumOrNil
                  completionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A032] 取得商品某評價的全部回覆
+(void)categoryCommentReplyA032:(NSString *)commentID startNum:(NSString *)startNumOrNil completionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A033] 查詢使用者名稱和頭像
+(void)getUserInfomation:(NSString *)userID
         completionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A123] FB/Google on-click login
+(void)loginA123AccountType:(NSString *)accountType
            withAccountInfo:(NSDictionary *)accountInfo
                    country:(NSString *)country
            completionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A124] FB/Google fetch in-use token
+(void)getTokenA124AccountType:(NSString *)accountType
               withAccountInfo:(NSDictionary *)accountInfo
               completionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

#pragma mark - cache Image
+(NSData *)cachedImageFromURL:(NSString *)urlStr;
+(BOOL)isCachedImageFromURL:(NSString *)urlStr;
+(NSString *)cacheImagePath;

#pragma mark - cache Video
+(NSString *)targetVideoFromURL:(NSString *)urlStr;
+(BOOL)cachedVideoFromURL:(NSString *)urlStr;
+(NSString *)cacheVideoPath;

#pragma mark - cache
+(void)createCachesImageFileDir:(NSString *)dir;

#pragma mark - 測試用
//+(void)listProduct:(NSString *)keyword
//          category:(NSString *)category
//             order:(NSString *)order
//          startNum:(NSString *)startNum
//   completionBlock:(void (^)(NSDictionary *responseObject))block;

//+(void)testCompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

@end
