//
//  ALNetWorkAPI+PostBasic.h
//  alorth
//
//  Created by w91379137 on 2015/6/2.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALNetWorkAPI.h"

@interface ALNetWorkAPI (PostBasic)

/*
+(void)postAlorthParam:(NSDictionary *)parameters
           toUrlString:(NSString *)urlString
       completionBlock:(void (^)(NSDictionary *responseObject))block;
 */

+(void)postAddress:(NSString *)addressString
            Header:(NSDictionary *)reqHeader
              Body:(NSDictionary *)reqBody
   CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

+(NSDictionary *)reqHeaderDict:(NSString *)aTXID;

@end
