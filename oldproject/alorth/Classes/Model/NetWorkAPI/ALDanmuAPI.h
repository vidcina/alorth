//
//  ALDanmuAPI.h
//  alorth
//
//  Created by John Hsu on 2016/3/10.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

typedef void (^ALDanmuCompleteBlock)(NSURLSessionDataTask *task, id responseObject, NSError *error);

@interface ALDanmuAPI : NSObject
{
    AFHTTPSessionManager *sessionManager;
}
+(instancetype)sharedInstance;

-(void)getPoints:(NSString *)streamID completeBlock:(ALDanmuCompleteBlock)completeBlock;

-(void)getComments:(NSString *)streamID completeBlock:(ALDanmuCompleteBlock)completeBlock;

-(void)addComment:(NSString *)comment atTime:(NSTimeInterval)second forUserID:(NSString *)userID userName:(NSString *)userName toStream:(NSString *)streamID completeBlock:(ALDanmuCompleteBlock)completeBlock;

-(void)voteComment:(int)score commentID:(NSString *)commentID forUserID:(NSString *)userID userName:(NSString *)userName completeBlock:(ALDanmuCompleteBlock)completeBlock;

-(void)getVoteForUserID:(NSString *)userID forComment:(NSString *)commentID completeBlock:(ALDanmuCompleteBlock)completeBlock;

@end
