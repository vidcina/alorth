//
//  ALNetWorkAPI+V2_1.m
//  alorth
//
//  Created by w91379137 on 2015/10/8.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALNetWorkAPI+V2_1.h"
#import "ALNetWorkAPI+PostBasic.h"

@implementation ALNetWorkAPI (V2_1)

+ (BOOL)checkSerialNumber:(NSString *)serialNumber
           ResponseObject:(NSDictionary *)responseObject
{
    return [self checkSerialNumber:serialNumber
                    ResponseObject:responseObject
                  ErrorCodePointer:nil];
}

+ (NSString *)errorMsgSerialNumber:(NSString *)serialNumber
                    ResponseObject:(NSDictionary *)responseObject
{
    NSString *errorCode = @"";
    
    //取得錯誤代碼
    [self checkSerialNumber:serialNumber
             ResponseObject:responseObject
           ErrorCodePointer:&errorCode];
    
    //查詢已知錯誤訊息列表
    NSString *errorMsg =
    maybe([ALNetWorkAPI errorMsgTableSerialNumber:serialNumber
                                       ResultCode:errorCode], NSString);
    
    //查詢失敗 顯示預設 訊息
    if (!errorMsg) {
        errorMsg = [NSString stringWithFormat:@"%@(%@_%@)",ALNetworkErrorMsg,serialNumber,errorCode];
    }
    
    return errorMsg;
}

+ (BOOL)checkSerialNumber:(NSString *)serialNumber
           ResponseObject:(NSDictionary *)responseObject
         ErrorCodePointer:(NSString **)errorCodePointer
{
    BOOL isSuccess = YES;
    NSMutableString *errorMsg = [NSMutableString string];
    id debugMsg = nil;
    
    //架構
    if (isSuccess &&
        ![responseObject isKindOfClass:[NSDictionary class]]) {
        [errorMsg appendString:@"NoDi"];
        isSuccess = NO;
    }
    else {
        debugMsg = responseObject[@"debug"];
    }
    
    if (isSuccess &&
        (![responseObject[@"resBody"] isKindOfClass:[NSDictionary class]] ||
        ![responseObject[@"resHeader"] isKindOfClass:[NSDictionary class]])) {
        [errorMsg appendString:@"NoDi"];
        isSuccess = NO;
    }
    
    //號碼
    if (isSuccess &&
        ![serialNumber isEqualToString:responseObject[@"resHeader"][@"TXID"]]) {
        [errorMsg appendString:@"NotID"];
        isSuccess = NO;
    }
    
    if (isSuccess) {
        //代碼
        NSDictionary *resBody = responseObject[@"resBody"];
        if (isSuccess &&
            ![resBody.allKeys containsObject:@"ResultCode"]) {
            ALLog(@"NO ResultCode : %@",serialNumber);
            [errorMsg appendString:@"NoNum"];
            isSuccess = NO;
        }
        
        if (isSuccess &&
            [responseObject[@"resBody"][@"ResultCode"] isKindOfClass:[NSNull class]]) {
            ALLog(@"ResultCode is Null : %@",serialNumber);
            [errorMsg appendString:@"ReNull"];
            isSuccess = NO;
        }
        
        if (isSuccess &&
            !([responseObject[@"resBody"][@"ResultCode"] intValue] == 0)) {
            ALLog(@"ResultCode is %@ : %@",responseObject[@"resBody"][@"ResultCode"],serialNumber);
            [errorMsg appendFormat:@"%@",responseObject[@"resBody"][@"ResultCode"]];
            isSuccess = NO;
        }
    }
    
    if (errorCodePointer) *errorCodePointer = errorMsg;
    
    if (!isSuccess) {
        ALLog(@"debugMsg : %@",debugMsg);
    }
    
    return isSuccess;
}

#pragma mark -
+ (void)accountFollowingListA038StartNum:(NSString *)startNum
                         CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:startNum forKey:@"StartNum"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/user/api"]
               Header:[self reqHeaderDict:@"A038"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)accountFollowersListA039StartNum:(NSString *)startNum
                         CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:startNum forKey:@"StartNum"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/user/api"]
               Header:[self reqHeaderDict:@"A039"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)changeFollowStatusA040UserID:(NSString *)userID
                        FollowStatus:(NSString *)followStatus
                     CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    if (![PDSAccountManager isLoggedIn]) {
        completionBlock(@{});
        return;
    }
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:userID forKey:@"UserID"];
    [reqBodyDict setObjectEmptyStringIfNil:followStatus forKey:@"FollowStatus"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/user/api"]
               Header:[self reqHeaderDict:@"A040"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)earnGrossSalesListA041StartNum:(NSString *)startNum
                       CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:startNum forKey:@"StartNum"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/earn/api"]
               Header:[self reqHeaderDict:@"A041"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)findGoodsToSaleListA042Keyword:(NSString *)keyword
                                 Order:(NSString *)order
                              StartNum:(NSString *)startNum
                       CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:keyword forKey:@"Keyword"];
    [reqBodyDict setObjectEmptyStringIfNil:order forKey:@"Order"];
    [reqBodyDict setObjectEmptyStringIfNil:startNum forKey:@"StartNum"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/earn/api"]
               Header:[self reqHeaderDict:@"A042"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)sellingCommissionListA043StartNum:(NSString *)startNum
                          CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:startNum forKey:@"StartNum"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/earn/api"]
               Header:[self reqHeaderDict:@"A043"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)creatSellingCommissionA044ProductID:(NSString *)productID
                                ProductName:(NSString *)productName
                               ProductOwner:(NSString *)productOwner
                             EarnPercentage:(NSString *)earnPercentage
                           CommissionStatus:(NSString *)commissionStatus
                            CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:productID forKey:@"ProductID"];
    [reqBodyDict setObjectEmptyStringIfNil:productName forKey:@"ProductName"];
    [reqBodyDict setObjectEmptyStringIfNil:productOwner forKey:@"ProductOwner"];
    [reqBodyDict setObjectEmptyStringIfNil:earnPercentage forKey:@"EarnPercentage"];
    [reqBodyDict setObjectEmptyStringIfNil:commissionStatus forKey:@"CommissionStatus"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/earn/api"]
               Header:[self reqHeaderDict:@"A044"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)agreeCommissionListA045StartNum:(NSString *)startNum
                        CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:startNum forKey:@"StartNum"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/earn/api"]
               Header:[self reqHeaderDict:@"A045"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)checkAgreeCommissionA046ProductID:(NSString *)productID
                              ProductName:(NSString *)productName
                        ApplicationUserID:(NSString *)applicationUserID
                           EarnPercentage:(NSString *)earnPercentage
                         CommissionStatus:(NSString *)commissionStatus
                          CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:productID forKey:@"ProductID"];
    [reqBodyDict setObjectEmptyStringIfNil:productName forKey:@"ProductName"];
    [reqBodyDict setObjectEmptyStringIfNil:applicationUserID forKey:@"ApplicationUserID"];
    [reqBodyDict setObjectEmptyStringIfNil:earnPercentage forKey:@"EarnPercentage"];
    [reqBodyDict setObjectEmptyStringIfNil:commissionStatus forKey:@"CommissionStatus"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/earn/api"]
               Header:[self reqHeaderDict:@"A046"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

#pragma mark - A047 暫時不用
+ (void)earnGrossSalesListA048ProductOwner:(NSString *)productOwner
                                  SellerID:(NSString *)sellerID
                                  StartNum:(NSString *)startNum
                           CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:productOwner forKey:@"ProductOwner"];
    [reqBodyDict setObjectEmptyStringIfNil:sellerID forKey:@"SellerID"];
    [reqBodyDict setObjectEmptyStringIfNil:startNum forKey:@"StartNum"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/earn/api"]
               Header:[self reqHeaderDict:@"A048"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)uploadStreamImageA049StreamID:(NSString *)streamID
                          StreamImage:(NSString *)streamImage
                      CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:streamID forKey:@"StreamID"];
    [reqBodyDict setObjectEmptyStringIfNil:streamImage forKey:@"StreamImage"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/stream/api"]
               Header:[self reqHeaderDict:@"A049"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)getStreamListA050StartNum:(NSString *)startNum
                  CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:startNum forKey:@"StartNum"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/stream/api"]
               Header:[self reqHeaderDict:@"A050"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)getStreamListA051GPSLongitude1:(NSString *)longitude1
                          GPSLatitude1:(NSString *)latitude1
                         GPSLongitude2:(NSString *)longitude2
                          GPSLatitude2:(NSString *)longtide1
                       CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:longitude1 forKey:@"GPSLongitude1"];
    [reqBodyDict setObjectEmptyStringIfNil:latitude1 forKey:@"GPSLatitude1"];
    [reqBodyDict setObjectEmptyStringIfNil:longitude2 forKey:@"GPSLongitude2"];
    [reqBodyDict setObjectEmptyStringIfNil:longtide1 forKey:@"GPSLatitude2"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/stream/api"]
               Header:[self reqHeaderDict:@"A051"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)getStreamProductA052ProductID:(NSString *)productID
                             StreamID:(NSString *)streamID
                      CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:productID forKey:@"ProductID"];
    [reqBodyDict setObjectEmptyStringIfNil:streamID forKey:@"StreamID"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/stream/api"]
               Header:[self reqHeaderDict:@"A052"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)reportStreamA053StreamID:(NSString *)streamID
                 CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:streamID forKey:@"StreamID"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/stream/api"]
               Header:[self reqHeaderDict:@"A053"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

/*
+ (void)createStreamOrderA054UserID:(NSString *)userID
                           StreamID:(NSString *)streamID
                          ProductID:(NSString *)productID
                            StockID:(NSString *)stockID
                           SellerID:(NSString *)sellerID
                        OrderAmount:(NSString *)orderAmount
                            PayType:(NSString *)payType
                    CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:userID forKey:@"UserID"];
    [reqBodyDict setObjectEmptyStringIfNil:streamID forKey:@"StreamID"];
    [reqBodyDict setObjectEmptyStringIfNil:productID forKey:@"ProductID"];
    [reqBodyDict setObjectEmptyStringIfNil:stockID forKey:@"StockID"];
    [reqBodyDict setObjectEmptyStringIfNil:sellerID forKey:@"SellerID"];
    [reqBodyDict setObjectEmptyStringIfNil:orderAmount forKey:@"OrderAmount"];
    [reqBodyDict setObjectEmptyStringIfNil:payType forKey:@"PayType"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/order/api"]
               Header:[self reqHeaderDict:@"A054"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}
 */

+(void)getStreamProductA055Type:(NSString *)type
                       StartNum:(NSString *)startNum
                CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:type forKey:@"StreamingProductType"];
    [reqBodyDict setObjectEmptyStringIfNil:startNum forKey:@"StartNum"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/stream/api"]
               Header:[self reqHeaderDict:@"A055"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+(void)createStreamA056Name:(NSString *)name
                  Longitude:(NSString *)longitude
                    Laitude:(NSString *)laitude
                ProductList:(NSArray *)productList
             StreamLanguage:(NSString *)streamLanguage
            CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:name forKey:@"StreamingName"];
    
    [reqBodyDict setObjectEmptyStringIfNil:longitude forKey:@"StreamGPSLongitude"];
    [reqBodyDict setObjectEmptyStringIfNil:laitude forKey:@"StreamGPSLatitude"];
    
    [reqBodyDict setObjectEmptyStringIfNil:productList forKey:@"StreamingList"];
    
    [reqBodyDict setObjectEmptyStringIfNil:[ALAppDelegate sharedAppDelegate].countryCode forKey:@"StreamCountryCode"];
    [reqBodyDict setObjectEmptyStringIfNil:streamLanguage forKey:@"StreamLanguage"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/stream/api"]
               Header:[self reqHeaderDict:@"A056"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+(void)getStreamStatusA057StreamID:(NSString *)streamID
                   CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:streamID forKey:@"StreamID"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/stream/api"]
               Header:[self reqHeaderDict:@"A057"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+(void)closeStreamA058StreamID:(NSString *)streamID
                    ElapseTime:(NSString *)elapseTime
               CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:streamID forKey:@"StreamID"];
    [reqBodyDict setObjectEmptyStringIfNil:elapseTime forKey:@"ElapseTime"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/stream/api"]
               Header:[self reqHeaderDict:@"A058"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+(void)nextStreamA059StreamID:(NSString *)streamID
              CurrentPriority:(NSString *)currentPriority
                   ElapseTime:(NSString *)elapseTime
              CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:streamID forKey:@"StreamID"];
    [reqBodyDict setObjectEmptyStringIfNil:currentPriority forKey:@"CurrentPriority"];
    [reqBodyDict setObjectEmptyStringIfNil:elapseTime forKey:@"ElapseTime"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/stream/api"]
               Header:[self reqHeaderDict:@"A059"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)personalStreamListA060UserID:(NSString *)userID
                     CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:userID forKey:@"UserID"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/stream/api"]
               Header:[self reqHeaderDict:@"A060"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)editStreamListA061CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    
    [self postAddress:[API_domain stringByAppendingString:@"/stream/api"]
               Header:[self reqHeaderDict:@"A061"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)deleteStreamA062StreamID:(NSString *)streamID
                 CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:streamID forKey:@"StreamID"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/stream/api"]
               Header:[self reqHeaderDict:@"A062"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)searchPersonA063Keyword:(NSString *)keyword
                       StartNum:(NSString *)startNum
                CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:keyword forKey:@"Keyword"];
    [reqBodyDict setObjectEmptyStringIfNil:startNum forKey:@"StartNum"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/user/api"]
               Header:[self reqHeaderDict:@"A063"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}
#pragma mark - A064
#pragma mark - A065 暫時不用

+ (void)streamInfomationA066StreamID:(NSString *)streamID
                     CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:streamID forKey:@"StreamID"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/stream/api"]
               Header:[self reqHeaderDict:@"A066"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)rearrangeStreamListA067StreamList:(NSArray *)streamList
                          CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:streamList forKey:@"StreamList"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/stream/api"]
               Header:[self reqHeaderDict:@"A067"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)followStatusA068UserID:(NSString *)userID
               CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:userID forKey:@"UserID"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/user/api"]
               Header:[self reqHeaderDict:@"A068"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)accountFollowingListA069UserID:(NSString *)userID
                              StartNum:(NSString *)startNum
                       CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:userID forKey:@"UserID"];
    [reqBodyDict setObjectEmptyStringIfNil:startNum forKey:@"StartNum"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/user/api"]
               Header:[self reqHeaderDict:@"A069"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)accountFollowersListA070UserID:(NSString *)userID
                              StartNum:(NSString *)startNum
                       CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:userID forKey:@"UserID"];
    [reqBodyDict setObjectEmptyStringIfNil:startNum forKey:@"StartNum"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/user/api"]
               Header:[self reqHeaderDict:@"A070"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)stripeConnectURLA071CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    
    [self postAddress:[API_domain stringByAppendingString:@"/oauth/api"]
               Header:[self reqHeaderDict:@"A071"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)finishStripeConnect:(NSDictionary *)stripeResponseJSON
            CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:stripeResponseJSON forKey:@"StripeObject"];

    [self postAddress:[API_domain stringByAppendingString:@"/oauth/api"]
               Header:[self reqHeaderDict:@"A072"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)stripeIsAuthorizeA073CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    
    [self postAddress:[API_domain stringByAppendingString:@"/oauth/api"]
               Header:[self reqHeaderDict:@"A073"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)streamCheckoutA074ApplicationID:(NSString *)applicationID
                               StreamID:(NSString *)streamID
                              ProductID:(NSString *)productID
                                StockID:(NSString *)stockID
                               SellerID:(NSString *)sellerID
                            OrderAmount:(NSString *)orderAmount
                             TotalPrice:(NSString *)totalPrice
                        CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectxNil:applicationID forKey:@"ApplicationID"];
    [reqBodyDict setObjectxNil:streamID forKey:@"StreamID"];
    [reqBodyDict setObjectxNil:productID forKey:@"ProductID"];
    [reqBodyDict setObjectxNil:stockID forKey:@"StockID"];
    [reqBodyDict setObjectxNil:sellerID forKey:@"SellerID"];
    [reqBodyDict setObjectxNil:orderAmount forKey:@"OrderAmount"];
    [reqBodyDict setObjectxNil:totalPrice forKey:@"TotalPrice"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/oauth/api"]
               Header:[self reqHeaderDict:@"A074"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)creditCardTokenA075StripeToken:(NSString *)stripeToken
                            CardNumber:(NSString *)cardNumber
                              CardType:(NSString *)cardType
                                  UUID:(NSString *)uuID
                                Stauts:(NSString *)stauts
                       CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    /*
     stauts
     0:inset and update
     1:delete
     */
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectxNil:stripeToken forKey:@"StripeToken"];
    [reqBodyDict setObjectxNil:cardNumber forKey:@"CardNumber"];
    [reqBodyDict setObjectxNil:cardType forKey:@"CardType"];
    [reqBodyDict setObjectxNil:uuID forKey:@"UUID"];
    [reqBodyDict setObjectxNil:stauts forKey:@"Stauts"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/oauth/api"]
               Header:[self reqHeaderDict:@"A075"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)creditCardIsAuthorizeA076CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    
    [self postAddress:[API_domain stringByAppendingString:@"/oauth/api"]
               Header:[self reqHeaderDict:@"A076"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)versionInformationA077CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    
    [self postAddress:[API_domain stringByAppendingString:@"/about/api"]
               Header:[self reqHeaderDict:@"A077"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

+ (void)userStreamVideoStatusA078CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    
    [self postAddress:[API_domain stringByAppendingString:@"/stream/api"]
               Header:[self reqHeaderDict:@"A078"]
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

@end
