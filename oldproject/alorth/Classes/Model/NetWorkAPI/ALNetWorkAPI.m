//
//  ALNetWorkAPI.m
//  alorth
//
//  Created by w91379137 on 2015/5/25.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALNetWorkAPI.h"
#import "ALNetWorkAPI+PostBasic.h"
#import "NSData+CommonCrypto.h"

@implementation ALNetWorkAPI
#pragma mark - Header

+(NSString *)aesAndBase64StringFromString:(NSString *)originalString
{
    NSMutableData *data = [[originalString dataUsingEncoding:NSUTF8StringEncoding] mutableCopy];
    
    if (data.length % 16 != 0) {
        int k = 16 - (data.length % 16);
        [data increaseLengthBy:k];
    }
    
    NSString *key = @"14a65l8o0r783t6h";
    NSString *iv = @"0000000000000000";
    NSData *encryptedData = nil;

    encryptedData = [data dataEncryptedUsingAlgorithm:kCCAlgorithmAES
                                                  key:key
                                 initializationVector:iv
                                              options:0
                                                error:nil];
    NSString *base64EncryptedData = [encryptedData base64EncodedStringWithOptions:kNilOptions];
    return base64EncryptedData;
}

#pragma mark -
// [A001] 註冊使用者
+(void)registerA001Account:(NSString *)account
                  password:(NSString *)password
                     email:(NSString *)email
                   country:(NSString *)country
           CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A001" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:account forKey:@"UserAccount"];
    NSString *encryptedPassword = [self aesAndBase64StringFromString:password];
    [reqBodyDict setObjectEmptyStringIfNil:encryptedPassword forKey:@"UserPassword"];
    [reqBodyDict setObjectEmptyStringIfNil:email forKey:@"UserEmail"];
    [reqBodyDict setObjectEmptyStringIfNil:@"16" forKey:@"UserCharecter"];
    [reqBodyDict setObjectEmptyStringIfNil:country forKey:@"UserCountry"];

    [self postAddress:[API_domain stringByAppendingString:@"/user/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A002] 使用者登入
+(void)loginA002Account:(NSString *)account
               password:(NSString *)password
        CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A002" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:account forKey:@"UserAccount"];
    NSString *encryptedPassword = [self aesAndBase64StringFromString:password];
    [reqBodyDict setObjectEmptyStringIfNil:encryptedPassword forKey:@"UserPassword"];
    [reqBodyDict setObjectEmptyStringIfNil:[UIDevice currentDevice].identifierForVendor.UUIDString forKey:@"DeviceID"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/user/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A003] 使用者位置
+(void)setGPSA003Longitide:(double)lon
                  latitude:(double)lat
           CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A003" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:[NSString stringWithFormat:@"%f",lon] forKey:@"UserLongitude"];
    [reqBodyDict setObjectEmptyStringIfNil:[NSString stringWithFormat:@"%f",lat] forKey:@"UserLatitude"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/user/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A004] 變更密碼
+(void)chagePasswordA004WithNewPassword:(NSString *)newPassword
              CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A004" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    NSString *encryptedNewPassword = [self aesAndBase64StringFromString:newPassword];

    [reqBodyDict setObjectEmptyStringIfNil:encryptedNewPassword forKey:@"NewPassword"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/user/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A005] 刊登新增庫存
+(void)addStockA005ProductID:(NSString *)productIdOrNil
                 productClip:(NSString *)productClip
                productImage:(NSString *)productImage
                 productSize:(NSString *)productSize
          productDescription:(NSString *)productDescription
                    Category:(NSString *)Category
                ProductTitle:(NSString *)ProductTitle
                        Tags:(NSArray *)Tags
             CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A005" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:productIdOrNil forKey:@"ProductID"];
    [reqBodyDict setObjectEmptyStringIfNil:productClip forKey:@"ProductClip"];
    [reqBodyDict setObjectEmptyStringIfNil:productImage forKey:@"ProductImage"];
    [reqBodyDict setObjectEmptyStringIfNil:productSize forKey:@"ProductSize"];
    [reqBodyDict setObjectEmptyStringIfNil:productDescription forKey:@"ProductDescription"];
    [reqBodyDict setObjectEmptyStringIfNil:Category forKey:@"Category"];
    [reqBodyDict setObjectEmptyStringIfNil:ProductTitle forKey:@"ProductTitle"];
    [reqBodyDict setObjectEmptyStringIfNil:Tags forKey:@"Tags"];

    [self postAddress:[API_domain stringByAppendingString:@"/seller/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}


// [A006] 顯示商品列表
+(void)goodsA006Keyword:(NSString *)keywordOrNil
               Category:(NSString *)categoryOrNil
                  Order:(NSString *)orderOrNil
               StartNum:(NSString *)startNumOrNil
                 UserID:(NSString *)userIDOrNil
        CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A006" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:keywordOrNil forKey:@"Keyword"];
    [reqBodyDict setObjectEmptyStringIfNil:categoryOrNil forKey:@"Category"];
    [reqBodyDict setObjectEmptyStringIfNil:userIDOrNil forKey:@"UserID"];
    [reqBodyDict setObjectEmptyStringIfNil:orderOrNil forKey:@"Order"];
    [reqBodyDict setObjectEmptyStringIfNil:startNumOrNil forKey:@"StartNum"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/goods/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A007] 取得商品詳細資料
+(void)getStockListA007ProductID:(NSString *)productID
                 CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A007" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:productID forKey:@"ProductID"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/goods/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A008] 檢查商品刊登狀況
+(void)checkProductExistA008ProductTitle:(NSString *)ProductTitle
                             ProductSize:(NSString *)ProductSize
                                Category:(NSString *)Category
                         CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A008" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:ProductTitle forKey:@"ProductTitle"];
    [reqBodyDict setObjectEmptyStringIfNil:ProductSize forKey:@"ProductSize"];
    [reqBodyDict setObjectEmptyStringIfNil:Category forKey:@"Category"];

    [self postAddress:[API_domain stringByAppendingString:@"/seller/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A009] 訂購商品
+(void)orderStockA009ProductId:(NSString *)ProductId
                       StockID:(NSString *)StockID
                      SellerID:(NSString *)SellerID
                   OrderAmount:(NSString *)OrderAmount
                       PayType:(NSString *)payType
               CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A009" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:ProductId forKey:@"ProductID"];
    [reqBodyDict setObjectEmptyStringIfNil:StockID forKey:@"StockID"];
    [reqBodyDict setObjectEmptyStringIfNil:SellerID forKey:@"SellerID"];
    [reqBodyDict setObjectEmptyStringIfNil:OrderAmount forKey:@"OrderAmount"];
    [reqBodyDict setObjectEmptyStringIfNil:payType forKey:@"PayType"];

    [self postAddress:[API_domain stringByAppendingString:@"/order/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A010] 修改訂單價格
+(void)changeOrderPriceA010OrderId:(NSString *)orderId
                        OrderPrice:(NSString *)OrderPrice
                        TransPrice:(NSString *)TransPrice
                   CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A010" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:orderId forKey:@"OrderID"];
    [reqBodyDict setObjectEmptyStringIfNil:OrderPrice forKey:@"OrderPrice"];
    [reqBodyDict setObjectEmptyStringIfNil:TransPrice forKey:@"TransPrice"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/order/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A011] 取消交易
+(void)cancelOrderPriceA011OrderId:(NSString *)orderId
                   CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A011" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:orderId forKey:@"OrderID"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/order/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}


// [A012] 確認交易
+(void)confirmOrderPriceA012OrderId:(NSString *)orderId
                            payType:(NSString *)payTypeOrNil
                    transactionData:(NSString *)transactionDataOrNil
                    CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A012" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:orderId forKey:@"OrderID"];
    [reqBodyDict setObjectEmptyStringIfNil:payTypeOrNil forKey:@"PayType"];
    [reqBodyDict setObjectEmptyStringIfNil:transactionDataOrNil forKey:@"TransactionData"];

    [self postAddress:[API_domain stringByAppendingString:@"/order/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A013] 評價列表
+(void)reviewListA013withUserID:(NSString *)userID
                       startNum:(NSString *)startNum
                completionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A013" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:userID forKey:@"UserID"];
    [reqBodyDict setObjectEmptyStringIfNil:startNum forKey:@"StartNum"];

    [self postAddress:[API_domain stringByAppendingString:@"/comment/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A014] 評價商品
+(void)commentStockA014StockID:(NSString *)stockID
                       orderID:(NSString *)orderID
                          Rate:(NSString *)rate
                       Comment:(NSString *)comment
                   ReviewImage:(NSString *)reviewImageOrNil
               CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A014" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:stockID forKey:@"StockID"];
    [reqBodyDict setObjectEmptyStringIfNil:orderID forKey:@"OrderID"];
    [reqBodyDict setObjectEmptyStringIfNil:rate forKey:@"Rate"];
    [reqBodyDict setObjectEmptyStringIfNil:comment forKey:@"Comment"];
    [reqBodyDict setObjectEmptyStringIfNil:reviewImageOrNil forKey:@"ReviewImage"];

    [self postAddress:[API_domain stringByAppendingString:@"/comment/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A015] 回覆評價
+(void)replyCommentA015CommentID:(NSString *)commentID
                    ReplyComment:(NSString *)ReplyComment
                 CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A015" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:commentID forKey:@"CommentID"];
    [reqBodyDict setObjectEmptyStringIfNil:ReplyComment forKey:@"ReplyComment"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/comment/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A016] 我的公開資訊
+(void)myPublicInfoA016CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A016" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    
    [self postAddress:[API_domain stringByAppendingString:@"/user/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A017] 我的公開資訊
+(void)setMyPublicInfoA017PublicInfo:(NSString *)infoString
                     CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A017" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:infoString forKey:@"PublicInfo"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/user/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A018] 販售訂單列表
+(void)mySoldOrderListA018StartNum:(NSString *)startNumOrNil
                   CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A018" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:startNumOrNil forKey:@"StartNum"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/order/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A019] 購買訂單列表
+(void)myBoughtOrderListA019StartNum:(NSString *)startNum
                     CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A019" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:startNum forKey:@"StartNum"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/order/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A020] 個人資料
+(void)personalProfile020CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A020" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    
    [self postAddress:[API_domain stringByAppendingString:@"/user/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A021] 設定個人資料
+(void)setPersonalProfile021HeadImage:(NSString *)headImage
                            FirstName:(NSString *)firstName
                             LastName:(NSString *)lastName
                               Gender:(NSString *)gender
                             Birthday:(NSString *)birthday
                             Location:(NSString *)location
                              Address:(NSString *)address
                                Phone:(NSString *)phone
                           TransPrice:(NSString *)transPrice
                           PublicInfo:(NSString *)publicInfo
                      CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A021" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:headImage forKey:@"HeadImage"];
    [reqBodyDict setObjectEmptyStringIfNil:firstName forKey:@"FirstName"];
    [reqBodyDict setObjectEmptyStringIfNil:lastName forKey:@"LastName"];
    [reqBodyDict setObjectEmptyStringIfNil:gender forKey:@"Gender"];
    [reqBodyDict setObjectEmptyStringIfNil:birthday forKey:@"Birthday"];
    [reqBodyDict setObjectEmptyStringIfNil:location forKey:@"Location"];
    [reqBodyDict setObjectEmptyStringIfNil:address forKey:@"Address"];
    [reqBodyDict setObjectEmptyStringIfNil:phone forKey:@"Phone"];
    [reqBodyDict setObjectEmptyStringIfNil:transPrice forKey:@"TransPrice"];
    [reqBodyDict setObjectEmptyStringIfNil:publicInfo forKey:@"PublicInfo"];

    [self postAddress:[API_domain stringByAppendingString:@"/user/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A022] 登出
+(void)logoutA022CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A022" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];

    [self postAddress:[API_domain stringByAppendingString:@"/user/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A023] 取得聊天室ID
+(void)getIMRoomIDA023ForUser1:(NSString *)userID1 User2:(NSString *)userID2 completionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A023" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:userID1 forKey:@"FirstUserID"];
    [reqBodyDict setObjectEmptyStringIfNil:userID2 forKey:@"SecondUserID"];

    [self postAddress:[API_domain stringByAppendingString:@"/webim/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A024] 新增聊天室ID
+(void)createIMRoomIDA024ForUser1:(NSString *)userID1 User2:(NSString *)userID2 roomID:(NSString *)roomID completionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A024" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:userID1 forKey:@"FirstUserID"];
    [reqBodyDict setObjectEmptyStringIfNil:userID2 forKey:@"SecondUserID"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/webim/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A025] 庫存列表
+(void)stockListA025StartNum:(NSString *)startNumOrNil
             CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A025" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:startNumOrNil forKey:@"StartNum"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/inventory/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];

}

// [A026] 修改庫存
+(void)editStockA026ProductID:(NSString *)productID
                      stockID:(NSString *)stockID
                       isUsed:(NSString *)isUsed
                         size:(NSString *)size
                        price:(NSString *)price
                       amount:(NSString *)amount
              completionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A026" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:productID forKey:@"ProductID"];
    [reqBodyDict setObjectEmptyStringIfNil:stockID forKey:@"StockID"];
    [reqBodyDict setObjectEmptyStringIfNil:isUsed forKey:apiKeyIsUsed];
    [reqBodyDict setObjectEmptyStringIfNil:size forKey:apiKeySize];
    [reqBodyDict setObjectEmptyStringIfNil:price forKey:@"Price"];
    [reqBodyDict setObjectEmptyStringIfNil:amount forKey:@"Amount"];

    
    [self postAddress:[API_domain stringByAppendingString:@"/inventory/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A027] 刪除所有庫存
+(void)deleteAllStockA027ProductID:(NSString *)productID completionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A027" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:productID forKey:@"ProductID"];

    [self postAddress:[API_domain stringByAppendingString:@"/inventory/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A028] 忘記密碼
+(void)forgetPasswordA028UserInfo:(NSString *)emailAddress completionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A028" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:emailAddress forKey:@"UserInfo"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/user/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];

}

// [A029] 取得分類子規格
+(void)subCategoryListA029ByCategoryIndex:(NSString *)categoryIndex
                   CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A029" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:categoryIndex forKey:@"Category"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/inventory/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A030] 取得商品分類
+(void)categoryListA030CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A030" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    
    [self postAddress:[API_domain stringByAppendingString:@"/inventory/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A031] 取得商品評價
+(void)categoryCommentA031OrderID:(NSString *)orderID
                         startNum:(NSString *)startNumOrNil
                  completionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A031" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:orderID forKey:@"OrderID"];
    [reqBodyDict setObjectEmptyStringIfNil:startNumOrNil forKey:@"StartNum"];

    [self postAddress:[API_domain stringByAppendingString:@"/comment/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A032] 取得商品某評價的全部回覆
+(void)categoryCommentReplyA032:(NSString *)commentID startNum:(NSString *)startNumOrNil completionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A032" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:commentID forKey:@"CommentID"];
    [reqBodyDict setObjectEmptyStringIfNil:startNumOrNil forKey:@"StartNum"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/comment/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A033] 查詢使用者名稱和頭像
+(void)getUserInfomation:(NSString *)userID
         completionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A033" forKey:@"TXID"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    [reqBodyDict setObjectEmptyStringIfNil:userID forKey:@"UserID"];
    
    [self postAddress:[API_domain stringByAppendingString:@"/user/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

// [A123] FB/Google on-click login
+(void)loginA123AccountType:(NSString *)accountType
            withAccountInfo:(NSDictionary *)accountInfo
                    country:(NSString *)country
            completionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A123" forKey:@"TXID"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    if ([accountType isEqualToString:@"facebook"]) {
        // Required
        [reqBodyDict setObjectEmptyStringIfNil:accountInfo[@"token"] forKey:@"FbAccessToken"];
        [reqBodyDict setObjectEmptyStringIfNil:@"1" forKey:@"Type"];
        
        // Optional
        [reqBodyDict setObjectEmptyStringIfNil:accountInfo[@"userID"] forKey:@"UserID"];
    }
    else if ([accountType isEqualToString:@"google"]) {
        // Required
        [reqBodyDict setObjectEmptyStringIfNil:accountInfo[@"token"] forKey:@"GoogleIDToken"];
        [reqBodyDict setObjectEmptyStringIfNil:@"2" forKey:@"Type"];
        
        // Optional
        [reqBodyDict setObjectEmptyStringIfNil:accountInfo[@"userID"] forKey:@"UserID"];
    }
    
    // Required
    [reqBodyDict setObjectEmptyStringIfNil:accountInfo[@"avatar"] forKey:@"HeadImage"];
    [reqBodyDict setObjectEmptyStringIfNil:accountInfo[@"email"] forKey:@"Email"];

    // Optional
    
    // TODO: need time format
    [reqBodyDict setObjectEmptyStringIfNil:accountInfo[@"expiredTime"] forKey:@"ExpireTime"];
    [reqBodyDict setObjectEmptyStringIfNil:accountInfo[@"firstName"] forKey:@"FirstName"];
    [reqBodyDict setObjectEmptyStringIfNil:accountInfo[@"lastName"] forKey:@"LastName"];
    [reqBodyDict setObjectEmptyStringIfNil:accountInfo[@"displayName"] forKey:@"DisplayName"];
    [reqBodyDict setObjectEmptyStringIfNil:[UIDevice currentDevice].identifierForVendor.UUIDString forKey:@"DeviceID"];
    [reqBodyDict setObjectEmptyStringIfNil:country forKey:@"UserCountry"];

    [self postAddress:[API_domain stringByAppendingString:@"/user/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
    
}

+(void)getTokenA124AccountType:(NSString *)accountType
               withAccountInfo:(NSDictionary *)accountInfo
               completionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    [reqHeaderDict setObjectEmptyStringIfNil:@"A124" forKey:@"TXID"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager userToken] forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    
    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
    
    if ([accountType isEqualToString:@"facebook"]) {
        // Required
        [reqBodyDict setObjectEmptyStringIfNil:accountInfo[@"token"] forKey:@"FbAccessToken"];
        [reqBodyDict setObjectEmptyStringIfNil:@"1" forKey:@"Type"];
        
        // Optional
        [reqBodyDict setObjectEmptyStringIfNil:accountInfo[@"userID"] forKey:@"UserID"];
    }
    else if ([accountType isEqualToString:@"google"]) {
        // Required
        [reqBodyDict setObjectEmptyStringIfNil:accountInfo[@"token"] forKey:@"GoogleIDToken"];
        [reqBodyDict setObjectEmptyStringIfNil:@"2" forKey:@"Type"];
        
        // Optional
        [reqBodyDict setObjectEmptyStringIfNil:accountInfo[@"userID"] forKey:@"UserID"];
    }

    [self postAddress:[API_domain stringByAppendingString:@"/user/api"]
               Header:reqHeaderDict
                 Body:reqBodyDict
      CompletionBlock:completionBlock];
}

#pragma mark - cache Image
+(NSData *)cachedImageFromURL:(NSString *)urlStr
{
    if (![urlStr isKindOfClass:[NSString class]]) {
        return nil;
    }
    
    NSString *md5Str = [[urlStr dataUsingEncoding:NSUTF8StringEncoding] md5];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *cachePath = [[[self cacheImagePath] stringByAppendingPathComponent:md5Str] stringByAppendingString:@".jpg"];
    if ([fileManager fileExistsAtPath:cachePath]) {
        return [NSData dataWithContentsOfFile:cachePath];
    }
    else {
        NSURL *url = [NSURL URLWithBugString:urlStr];
        
        NSData *data = [NSData dataWithContentsOfURL:url];
        [data writeToFile:cachePath atomically:YES];
        return data;
    }
}

+(BOOL)isCachedImageFromURL:(NSString *)urlStr
{
    if (![urlStr isKindOfClass:[NSString class]]) {
        return NO;
    }
    
    NSString *md5Str = [[urlStr dataUsingEncoding:NSUTF8StringEncoding] md5];
    NSString *cachePath = [[[self cacheImagePath] stringByAppendingPathComponent:md5Str] stringByAppendingString:@".jpg"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:cachePath]) {
        return YES;
    }
    else {
        return NO;
    }
}

+(NSString *)cacheImagePath
{
    NSString *cachesImageFileDirPath = [NSHomeDirectory() stringByAppendingPathComponent:@"/Library/Caches/imgcache/"];
    [self createCachesImageFileDir:cachesImageFileDirPath];
    return cachesImageFileDirPath;
}

#pragma mark - cache Video
+(NSString *)targetVideoFromURL:(NSString *)urlStr
{
    if (![urlStr isKindOfClass:[NSString class]]) {
        return nil;
    }
    
    NSString *md5Str = [[urlStr dataUsingEncoding:NSUTF8StringEncoding] md5];
    NSString *cachePath = [[[self cacheVideoPath] stringByAppendingPathComponent:md5Str] stringByAppendingString:@".mp4"];
    return cachePath;
}

+(BOOL)cachedVideoFromURL:(NSString *)urlStr
{
    NSString *cachePath = [self targetVideoFromURL:urlStr];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:cachePath]) {
        return YES;
    }
    else {
        //不存在影片 請影片下載機制下載
        return NO;
    }
}

+(NSString *)cacheVideoPath
{
    NSString *cachesVideoFileDirPath = [NSHomeDirectory() stringByAppendingPathComponent:@"/Library/Caches/vidcache/"];
    [self createCachesImageFileDir:cachesVideoFileDirPath];
    return cachesVideoFileDirPath;
}

#pragma mark - cache
+(void)createCachesImageFileDir:(NSString *)dir
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL isDir;
    BOOL isExist = [fileManager fileExistsAtPath:dir isDirectory:&isDir];
    
    if (isExist == NO || isDir == NO) {
        BOOL createSuccess = [fileManager createDirectoryAtPath:dir withIntermediateDirectories:YES attributes:nil error:nil];
        if (createSuccess == NO) {
            return;
        }
    }
}

#pragma mark - test
/*
 +(void)listProduct:(NSString *)keyword category:(NSString *)category order:(NSString *)order startNum:(NSString *)startNum completionBlock:(void (^)(NSDictionary *responseObject))block
 {
 NSString *relativeURLString = [API_domain stringByAppendingString:@"/goods/api"];
 NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
 [reqBodyDict setObjectEmptyStringIfNil:keyword forKey:@"Keyword"];
 [reqBodyDict setObjectEmptyStringIfNil:category forKey:@"Category"];
 [reqBodyDict setObjectEmptyStringIfNil:order forKey:@"Order"];
 [reqBodyDict setObjectEmptyStringIfNil:startNum forKey:@"StartNum"];
 
 NSDictionary *parameter = @{
 @"reqHeader" : @{
 @"UserToken" : [PDSAccountManager userToken],
 @"AuthTime" : [PDSAccountManager authTime],
 @"TXID" : @"A006"
 },
 @"reqBody" : reqBodyDict
 };
 [self postAlorthParam:parameter toUrlString:relativeURLString completionBlock:block];
 }
 */

//+(void)testCompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
//{
//    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
//    [reqHeaderDict setObjectEmptyStringIfNil:nil forKey:@"UserToken"];
//    [reqHeaderDict setObjectEmptyStringIfNil:nil forKey:@"AuthTime"];
//    [reqHeaderDict setObjectEmptyStringIfNil:@"A006" forKey:@"TXID"];
//    
//    NSMutableDictionary *reqBodyDict = [NSMutableDictionary dictionary];
//    [reqBodyDict setObjectEmptyStringIfNil:nil forKey:@"Keyword"];
//    [reqBodyDict setObjectEmptyStringIfNil:nil forKey:@"Category"];
//    [reqBodyDict setObjectEmptyStringIfNil:nil forKey:@"Order"];
//    [reqBodyDict setObjectEmptyStringIfNil:nil forKey:@"StartNum"];
//    
//    [self postAddress:[API_domain stringByAppendingString:@"/goods/api"]
//               Header:reqHeaderDict
//                 Body:reqBodyDict
//      CompletionBlock:completionBlock];
//}

@end
