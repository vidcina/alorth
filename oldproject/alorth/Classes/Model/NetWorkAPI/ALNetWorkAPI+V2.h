//
//  ALNetWorkAPI+V2.h
//  alorth
//
//  Created by w91379137 on 2015/9/20.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALNetWorkAPI.h"

@interface ALNetWorkAPI (V2)

// [A034] 取得商品修改資訊
+(void)getEditProductInformationA034ProductID:(NSString *)productID
                             CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A035] 修改商品
+(void)editProductA035ProductID:(NSString *)productID
                   ProductImage:(NSString *)productImage
             ProductDescription:(NSString *)productDescription
                           Tags:(NSArray *)tags
                CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A036] 檢視編輯過此商品資訊
+(void)reviewEditProductListA036ProductID:(NSString *)productID
                                 StartNum:(NSString *)startNum
                          CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

// [A037] 此商品賣掉統計
+(void)reportProductSalesListA037ProductID:(NSString *)productID
                                  StartNum:(NSString *)startNum
                           CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

@end
