//
//  ALDanmuAPI.m
//  alorth
//
//  Created by John Hsu on 2016/3/10.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALDanmuAPI.h"

@implementation ALDanmuAPI
+(instancetype)sharedInstance
{
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        sessionConfiguration.requestCachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
        
        sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:Danmu_API_domain] sessionConfiguration:sessionConfiguration];
        sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
        sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
        [[sessionManager responseSerializer] setAcceptableStatusCodes:nil];
        [[sessionManager responseSerializer] setAcceptableContentTypes:nil];
        
    }
    return self;
}

-(void)handleResponseTask:(NSURLSessionDataTask *)task
           ResponseObject:(id)responseObject
                    Error:(NSError *)error
            CompleteBlock:(ALDanmuCompleteBlock)completeBlock
{
    if (!task || error) {
        //錯誤處理LOG, 但不阻擋往下進行
#ifdef DEBUG
        if (task.response) {
            NSLog(@"error: %@,\n task response: %@",error,task.response);
        }
#endif
    }
    if (error) {
        completeBlock(task, nil, error);
    }
    else {
        if ([responseObject isKindOfClass:[NSDictionary class]] || [responseObject isKindOfClass:[NSArray class]]) {
            completeBlock(task, responseObject, nil);
        }
        else {
            NSString *bundleID = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleIdentifierKey];  //@"CFBundleIdentifier"
            
            NSError *error = [NSError errorWithDomain:[bundleID stringByAppendingString:@"NetworkAPI"] code:0 userInfo:nil];
            completeBlock(task, responseObject, error);
        }
    }
}

-(void)getPoints:(NSString *)streamID completeBlock:(ALDanmuCompleteBlock)completeBlock
{
    NSString *path = [NSString stringWithFormat:@"videos/%@/points", streamID];
    
    [sessionManager GET:path parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [self handleResponseTask:task
                  ResponseObject:responseObject
                           Error:nil
                   CompleteBlock:completeBlock];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self handleResponseTask:task
                  ResponseObject:nil
                           Error:error
                   CompleteBlock:completeBlock];
    }];
}

-(void)getComments:(NSString *)streamID completeBlock:(ALDanmuCompleteBlock)completeBlock
{
    NSString *path = [NSString stringWithFormat:@"videos/%@/comments?limit=10000&skip=0&sort=time%%20asc", streamID];
 
    [sessionManager GET:path parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [self handleResponseTask:task
                  ResponseObject:responseObject
                           Error:nil
                   CompleteBlock:completeBlock];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self handleResponseTask:task
                  ResponseObject:nil
                           Error:error
                   CompleteBlock:completeBlock];
    }];
}

-(void)addComment:(NSString *)comment atTime:(NSTimeInterval)second forUserID:(NSString *)userID userName:(NSString *)userName toStream:(NSString *)streamID completeBlock:(ALDanmuCompleteBlock)completeBlock
{
    NSString *path = [NSString stringWithFormat:@"comments"];
    NSDictionary *param = @{
                            @"time" : [NSString stringWithFormat:@"%.0f",second],
                            @"message" : comment,
                            @"video" : @{ @"id" : streamID },
                            @"user" : @{ @"id" : userID, @"name" : userName}
                            };
    [sessionManager POST:path parameters:param success:^(NSURLSessionDataTask *task, id responseObject) {
        [self handleResponseTask:task
                  ResponseObject:responseObject
                           Error:nil
                   CompleteBlock:completeBlock];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self handleResponseTask:task
                  ResponseObject:nil
                           Error:error
                   CompleteBlock:completeBlock];
    }];
    
}

-(void)voteComment:(int)score commentID:(NSString *)commentID forUserID:(NSString *)userID userName:(NSString *)userName completeBlock:(ALDanmuCompleteBlock)completeBlock
{
    NSString *path = [NSString stringWithFormat:@"votes"];
    NSDictionary *param = @{
                            @"score" : [NSString stringWithFormat:@"%d",score],
                            @"comment" : @{ @"id" : commentID },
                            @"user" : @{ @"id" : userID, @"name" : userName}
                            };
    [sessionManager POST:path parameters:param success:^(NSURLSessionDataTask *task, id responseObject) {
        [self handleResponseTask:task
                  ResponseObject:responseObject
                           Error:nil
                   CompleteBlock:completeBlock];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self handleResponseTask:task
                  ResponseObject:nil
                           Error:error
                   CompleteBlock:completeBlock];
    }];
}

-(void)getVoteForUserID:(NSString *)userID forComment:(NSString *)commentID completeBlock:(ALDanmuCompleteBlock)completeBlock
{
    NSString *path = [NSString stringWithFormat:@"comments/%@/votes?user=%@", commentID,userID];
    
    [sessionManager GET:path parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [self handleResponseTask:task
                  ResponseObject:responseObject
                           Error:nil
                   CompleteBlock:completeBlock];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self handleResponseTask:task
                  ResponseObject:nil
                           Error:error
                   CompleteBlock:completeBlock];
    }];
}
@end
