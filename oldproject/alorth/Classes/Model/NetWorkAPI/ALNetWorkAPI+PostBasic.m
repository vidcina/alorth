//
//  ALNetWorkAPI+PostBasic.m
//  alorth
//
//  Created by w91379137 on 2015/6/2.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALNetWorkAPI+PostBasic.h"
#import "ALNetWorkAPI+ALErrorConvert.h"

@implementation ALNetWorkAPI (PostBasic)

/*
+(void)postAlorthParam:(NSDictionary *)parameters
           toUrlString:(NSString *)urlString
       completionBlock:(void (^)(NSDictionary *responseObject))block
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json", nil];
    [manager POST:urlString
       parameters:@{@"data":[self aIdTojsonString:parameters]}
          success:^(AFHTTPRequestOperation *operation, id responseObject){
              if ([responseObject isKindOfClass:[NSDictionary class]]) {
                  block(responseObject);
              }
              else {
                  block(@{@"status":@"error not a Dictionary"});
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error){
              //ALLog(@"送出失敗: %@",address);
              ALLog(@"Header: %@",manager.requestSerializer.HTTPRequestHeaders);
              [self deBUG:operation parameters:parameters pt:nil];
              block(@{@"status":@"error connect failure"});
          }
     ];
     
}
 */

+(void)postAddress:(NSString *)addressString
            Header:(NSDictionary *)reqHeader
              Body:(NSDictionary *)reqBody
   CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;
{
    //Request 管理者
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json", nil];
    
    //包成參數
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObjectxNil:reqHeader forKey:@"reqHeader"];
    [parameters setObjectxNil:reqBody forKey:@"reqBody"];
    
    //產生POST
    [manager POST:addressString
       parameters:@{@"data":[self aIdTojsonString:parameters]}
          success:^(AFHTTPRequestOperation *operation, id responseObject){
              [self handleParameters:parameters
                           Operation:operation
                              Object:responseObject
                               Error:nil
                     CompletionBlock:completionBlock];
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error){
              [self handleParameters:parameters
                           Operation:operation
                              Object:nil
                               Error:error
                     CompletionBlock:completionBlock];
          }
     ];
}

+ (void)handleParameters:(NSMutableDictionary *)parameters
               Operation:(AFHTTPRequestOperation *)operation
                  Object:(id)responseObject
                   Error:(NSError *)error
         CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock;
{
    if (error) {
        NSString *apiID = parameters.safe[@"reqHeader"][@"TXID"];
        if (![apiID isKindOfClass:[NSString class]]) apiID = @"";
        
        NSString *inputString = operation.responseString;
        
        if (responseObject == nil && inputString) {
            //型一錯誤處理
            inputString = [self phpHeaderError:inputString];
            NSDictionary *reportx =
            [NSJSONSerialization JSONObjectWithData:[inputString dataUsingEncoding:NSUTF8StringEncoding]
                                            options:0
                                              error:&error];
            if (reportx) {
                NSLog(@"型一錯誤修正 %@ ***************************************",apiID);
                responseObject = reportx;
            }
        }
    }
    
    if ([responseObject isKindOfClass:[NSDictionary class]]) {
        NSDictionary *responseDictionary = (NSDictionary *)responseObject;
        
        int responseServerCode = [responseDictionary.safe[@"resBody"][@"ResultCode"] intValue];
        if (responseServerCode != 0) {
            NSMutableDictionary *dictMutableCopy = [responseObject mutableCopy];
            dictMutableCopy[@"debug"] = [self errorMessageFromServerErrorCode:responseServerCode];
            responseObject = dictMutableCopy;
        }
    }
    else {
        [self deBUG:operation parameters:parameters pt:nil];
        
        NSMutableDictionary *reportx = [NSMutableDictionary dictionary];
        [reportx setObjectxNil:@"error connection failure" forKey:@"debug"];
        [reportx setObjectxNil:[operation.responseString description] forKey:@"responseString"];
        responseObject = reportx;
    }
    
    #ifdef DEBUG
    if (parameters) {
        NSMutableDictionary *dictMutableCopy = [responseObject mutableCopy];
        dictMutableCopy[@"parameters"] = parameters;
        responseObject = dictMutableCopy;
        
        if (![[ALAppDelegate sharedAppDelegate].userID isEqualToString:@"w913791379137"] &&
            ![[ALAppDelegate sharedAppDelegate].userID isEqualToString:@"john"]) {
            ALLog(@"api %@紀錄\n%@",parameters.safe[@"reqHeader"][@"TXID"],responseObject);
        }
    }
    #endif
    
    completionBlock(responseObject);
}

+(NSDictionary *)reqHeaderDict:(NSString *)aTXID
{
    NSMutableDictionary *reqHeaderDict = [NSMutableDictionary dictionary];
    
    //kobe 要求 帶入 -1
    NSString *sendUserToken = [PDSAccountManager userToken].length == 0 ? @"-1" : [PDSAccountManager userToken];
    [reqHeaderDict setObjectEmptyStringIfNil:sendUserToken forKey:@"UserToken"];
    [reqHeaderDict setObjectEmptyStringIfNil:[PDSAccountManager authTime] forKey:@"AuthTime"];
    [reqHeaderDict setObjectEmptyStringIfNil:aTXID forKey:@"TXID"];
    return reqHeaderDict;
}

#pragma mark - 轉格式工具
+(NSString *)errorMessageFromServerErrorCode:(int)errCode
{
    switch (errCode) {
        case 0x10:
            return @"帳號名稱重複";
            break;
        case 0x11:
            return @"沒有此帳號";
            break;
        case 0x12:
            return @"沒有操作權限";
            break;
        case 0x13:
            return @"密碼錯誤";
            break;
        case 0x14:
            return @"無效的token";
            break;
        case 0x20:
            return @"沒有此商品";
            break;
        case 0x21:
            return @"已有商品刊登";
            break;
        case 0x30:
            return @"參數錯誤";
            break;
        case 0xA0:
            return @"API操作錯誤";
            break;
        default:
            break;
    }
    return @"未預期的錯誤";
}

+(NSData *)aIdTojsonData:(id)obj
{
    NSError * error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:obj
                                                       options:kNilOptions
                                                         error:&error];
    return jsonData;
}

+(NSString *)aIdTojsonString:(id)obj
{
    NSData *jsonData = [self aIdTojsonData:obj];
    if (! jsonData) {
        return @"{}";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}

+(id)aJsonDataToId:(NSData *)jsonData
{
    id obj = [NSJSONSerialization JSONObjectWithData:jsonData
                                             options:NSJSONReadingAllowFragments
                                               error:nil];
    return obj;
}

#pragma mark - DEBUG
+(void)deBUG:(AFHTTPRequestOperation *)operation
  parameters:(NSDictionary *)parameters
          pt:(NSDictionary *)pt
{
#ifdef DEBUG
    NSString *log = @"\n";
    log = [NSString stringWithFormat:@"%@request:\n  %@\n",log,operation.request.URL.absoluteString];
    log = [NSString stringWithFormat:@"%@pt:\n%@\n",log,pt];
    log = [NSString stringWithFormat:@"%@parameters:\n%@\n",log,parameters];
    log = [NSString stringWithFormat:@"%@status:\n  %d\n",log,(int)operation.response.statusCode];
    log = [NSString stringWithFormat:@"%@response:\n  %@\n",log,operation.response.URL.absoluteString];
    log = [NSString stringWithFormat:@"%@responseString:\n  %@\n",log,operation.responseString];
    ALLog(@"%@",log);
#endif
}

@end
