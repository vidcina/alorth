//
//  ALNetWorkAPI+ALErrorConvert.h
//  alorth
//
//  Created by w91379137 on 2015/11/17.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALNetWorkAPI.h"

@interface ALNetWorkAPI (ALErrorConvert)

+ (NSString *)phpHeaderError:(NSString *)inputString;

@end
