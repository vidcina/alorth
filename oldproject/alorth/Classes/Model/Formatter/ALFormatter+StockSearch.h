//
//  ALFormatter+StockSearch.h
//  alorth
//
//  Created by w91379137 on 2016/1/30.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALFormatter.h"

@interface ALFormatter (StockSearch)

+ (NSArray *)indexOfSortValueArray1:(NSArray *)sortValueArray1
                               Key1:(NSString *)key1
                    SortValueArray2:(NSArray *)sortValueArray2
                               Key2:(NSString *)key2
                        SourceArray:(NSArray *)sourceArray;

+ (NSArray *)indexOfSortValueArray1:(NSArray *)sortValueArray1
                               Key1:(NSString *)key1
                        SourceArray:(NSArray *)sourceArray;

@end
