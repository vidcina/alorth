//
//  ALFormatter+StockSearch.m
//  alorth
//
//  Created by w91379137 on 2016/1/30.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "ALFormatter+StockSearch.h"

@implementation ALFormatter (StockSearch)

+ (NSArray *)indexOfSortValueArray1:(NSArray *)sortValueArray1
                               Key1:(NSString *)key1
                    SortValueArray2:(NSArray *)sortValueArray2
                               Key2:(NSString *)key2
                        SourceArray:(NSArray *)sourceArray
{
    BOOL isFind = NO;
    NSInteger fit_x = 0;
    NSInteger fit_y = 0;
    
    for (NSInteger x = 0; x < sortValueArray1.count; x++) {
        
        NSPredicate *predicate1 =
        [NSPredicate predicateWithFormat:@"%K == %@", key1, sortValueArray1[x]];
        
        for (NSInteger y = 0; y < sortValueArray2.count; y++) {
            
            NSPredicate *predicate2 =
            [NSPredicate predicateWithFormat:@"%K == %@", key2, sortValueArray2[y]];
            
            {
                NSPredicate *concatPredicate =
                [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2]];
                
                NSArray *fitArray =
                [sourceArray filteredArrayUsingPredicate:concatPredicate];
                
                if (fitArray.count > 0) {
                    isFind = YES;
                }
            }
            
            if (isFind) {
                fit_y = y;
                break;
            }
        }
        if (isFind) {
            fit_x = x;
            break;
        }
    }
    
    return @[@(fit_x),@(fit_y)];
}

+ (NSArray *)indexOfSortValueArray1:(NSArray *)sortValueArray1
                               Key1:(NSString *)key1
                        SourceArray:(NSArray *)sourceArray
{
    BOOL isFind = NO;
    NSInteger fit_x = 0;
    
    for (NSInteger x = 0; x < sortValueArray1.count; x++) {
        
        NSPredicate *predicate1 =
        [NSPredicate predicateWithFormat:@"%K == %@", key1, sortValueArray1[x]];
        
        {
            NSPredicate *concatPredicate =
            [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1]];
            
            NSArray *fitArray =
            [sourceArray filteredArrayUsingPredicate:concatPredicate];
            
            if (fitArray.count > 0) {
                isFind = YES;
            }
        }
        
        if (isFind) {
            fit_x = x;
            break;
        }
    }
    
    return @[@(fit_x)];
}

@end
