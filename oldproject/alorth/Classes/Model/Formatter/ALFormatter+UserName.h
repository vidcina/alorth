//
//  ALFormatter+UserName.h
//  alorth
//
//  Created by w91379137 on 2015/9/24.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALFormatter.h"

@interface ALFormatter (UserName)

+ (NSString *)preferredDisplayNameFirst:(id)first Last:(id)last UserID:(id)userID;

/**
 *  @return returns full name by default, returns userID if fullname is blank or null
 */
+(NSString *)preferredDisplayNameForFullName:(NSString *)fullName UserID:(NSString *)userID;

/**
 *  @return returns full name by default, returns userID if fullname is blank or null
 */
+(NSString *)preferredDisplayNameForUserInfomation:(NSDictionary *)userInfomation UserID:(NSString *)userID;

@end
