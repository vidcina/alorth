//
//  ALDateFormatter.h
//  alorth
//
//  Created by John Hsu on 2015/7/16.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <Foundation/Foundation.h>
#define fullTimeFormat @"yyyy-MM-dd' 'HH:mm:ss"
#define dayTimeFormat @"yyyy-MM-dd"

#define maybe(object,classType) ((classType *)([object isKindOfClass:[classType class]] ? object : nil))

@interface ALFormatter : NSObject
/**
 *  @return localized @"xx hours ago", or @"xx minutes ago", or @"xx days ago"
 */
+(NSString *)localTextIntervalAgoTextForDate:(NSDate *)date;

/**
 *  @return @"HH:mm" for payment countdown in order list
 */
+(NSString *)localTextOrderPaymentCountdownFormatForDate:(NSDate *)date;

/**
 *  @return Any type for date
 */
+(NSString *)prettyLocelTimeFormat:(NSString *)format ForDate:(NSDate *)date;

/**
 *  @return @[days, hours, minites, seconds]
 */
+(NSArray *)timeDiffComponentsForDate:(NSDate *)date;

+(NSDateFormatter *)dateFormatteWithTimeZone:(NSTimeZone *)timeZone
                                FormatString:(NSString *)formatString;

@end
