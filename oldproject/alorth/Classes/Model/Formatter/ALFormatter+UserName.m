//
//  ALFormatter+UserName.m
//  alorth
//
//  Created by w91379137 on 2015/9/24.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALFormatter+UserName.h"

@implementation ALFormatter (UserName)

+(NSString *)preferredDisplayNameForFullName:(NSString *)fullName UserID:(NSString *)userID
{
    if (fullName && [fullName isKindOfClass:[NSString class]] && [fullName length] > 0 && [[fullName stringByReplacingOccurrencesOfString:@" " withString:@""] length] > 0) {
        return fullName;
    }
    if (userID && [userID isKindOfClass:[NSString class]] && [userID length] > 0 && [[userID stringByReplacingOccurrencesOfString:@" " withString:@""] length] > 0) {
        return userID;
    }
    return @"(No Name)";
}

+ (NSString *)preferredDisplayNameFirst:(id)first Last:(id)last UserID:(id)userID
{
    if ([first isKindOfClass:[NSString class]] && [last isKindOfClass:[NSString class]]) {
        NSString *fullName = [NSString stringWithFormat:@"%@ %@",first,last];
        if (fullName.length > 1) {
           return fullName;
        }
    }
    
    if ([userID isKindOfClass:[NSString class]]) {
        return [NSString stringWithFormat:@"%@",userID];
    }
    return @"";
}

+(NSString *)preferredDisplayNameForUserInfomation:(NSDictionary *)userInfomation UserID:(NSString *)userID
{
    NSString *fullName = nil;
    if ([userInfomation isKindOfClass:[NSDictionary class]]) {
        if ([userInfomation[@"resBody"] isKindOfClass:[NSDictionary class]]) {
            fullName =
            [NSString stringWithFormat:@"%@ %@",
             userInfomation[@"resBody"][@"FirstName"],
             userInfomation[@"resBody"][@"LastName"]];
        }
    }
    return [self preferredDisplayNameForFullName:fullName UserID:userID];
}

@end
