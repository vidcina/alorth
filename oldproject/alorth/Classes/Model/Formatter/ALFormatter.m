//
//  ALDateFormatter.m
//  alorth
//
//  Created by John Hsu on 2015/7/16.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALFormatter.h"

@implementation ALFormatter

+(NSString *)localTextIntervalAgoTextForDate:(NSDate *)date
{
    NSTimeInterval timeDiff = [[NSDate date] timeIntervalSinceDate:date];
    if (timeDiff > 24 * 3600) {
        return [NSString stringWithFormat:@"%.0f $days ago$",timeDiff / 24 / 3600];
    }
    else if (timeDiff > 3600) {
        return [NSString stringWithFormat:@"%.0f $hours ago$",timeDiff / 3600];
    }
    else if (timeDiff > 60){
        return [NSString stringWithFormat:@"%.0f $minutes ago$",timeDiff / 60];
    }
    else {
        return [NSString stringWithFormat:@"%.0f $seconds ago$",timeDiff];
    }
}

+(NSString *)localTextOrderPaymentCountdownFormatForDate:(NSDate *)date
{
    NSTimeInterval timeDiff = [date timeIntervalSinceDate:[NSDate date]];
    if (timeDiff < 0) {
        return @"Expired";
    }
    int hour = timeDiff / 3600;
    int minute = (timeDiff - (hour * 3600)) / 60;
    return [NSString stringWithFormat:@"$$%02d:%02d",hour,minute];
}

+(NSString *)prettyLocelTimeFormat:(NSString *)format ForDate:(NSDate *)date
{
    NSDateFormatter *formatter =
    [ALFormatter dateFormatteWithTimeZone:[NSTimeZone systemTimeZone]
                             FormatString:format];
    return [formatter stringFromDate:date];
}

+ (NSArray *)timeDiffComponentsForDate:(NSDate *)date
{
    NSMutableArray *aArray = [NSMutableArray array];
    NSTimeInterval timeDiff = [date timeIntervalSinceDate:[NSDate date]];
    
    int days = floorf(timeDiff / (24 * 60 * 60));
    [aArray addObject:@(days)];
    
    timeDiff = (timeDiff - days * (24 * 60 * 60));
    int hours = floorf(timeDiff / (60 * 60));
    [aArray addObject:@(hours)];
    
    timeDiff = timeDiff - hours * (60 * 60);
    int minutes = floorf(timeDiff / 60);
    [aArray addObject:@(minutes)];
    
    timeDiff = timeDiff - minutes * 60;
    int seconds = floorf(timeDiff);
    [aArray addObject:@(seconds)];
    
    return aArray;
}

+(NSDateFormatter *)dateFormatteWithTimeZone:(NSTimeZone *)timeZone
                                FormatString:(NSString *)formatString
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.calendar =
    [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    formatter.timeZone = timeZone;
    [formatter setDateFormat:formatString];
    
    return formatter;
}

@end
