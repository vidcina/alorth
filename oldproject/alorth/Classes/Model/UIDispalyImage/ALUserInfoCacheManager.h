//
//  ALUserInfoCacheManager.h
//  alorth
//
//  Created by w91379137 on 2015/9/11.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <Foundation/Foundation.h>

static const NSInteger ALUserInfoCacheTime = 300;
static NSString *ALUserInfoCacheManagerTimeKey = @"ALUserInfoCacheManagerTimeKey";

@interface ALUserInfoCacheManager : NSObject
{
    NSMutableDictionary *cacheUserInfomation;
}

#pragma mark -
+(void)getLocalUserInfomation:(NSString *)userID
              completionBlock:(void (^)(NSDictionary *responseObject))completionBlock;

@end
