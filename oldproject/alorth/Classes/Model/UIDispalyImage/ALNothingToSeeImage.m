//
//  ALNothingToSeeImage.m
//  alorth
//
//  Created by w91379137 on 2015/12/11.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALNothingToSeeImage.h"
#import "ALImagekeeper.h"
static NSString *ALclearNothingToSeeImage = @"clearNothingToSeeImage";
static NSString *ALwhiteNothingToSeeImage = @"whiteNothingToSeeImage";

@implementation ALNothingToSeeImage

+ (UIImage *)clearNothingToSeeImage
{
    return [self nothingToSeeImageWithKey:ALclearNothingToSeeImage];
}

+ (UIImage *)whiteNothingToSeeImage
{
    return [self nothingToSeeImageWithKey:ALwhiteNothingToSeeImage];
}

+ (UIImage *)nothingToSeeImageWithKey:(NSString *)key
{
    UIImage *image = [ALImagekeeper tryImage:key];
    
    if (image == nil) {
        
        UIColor *color = nil;
        if ([key isEqualToString:ALclearNothingToSeeImage]) color = [UIColor clearColor];
        if ([key isEqualToString:ALwhiteNothingToSeeImage]) color = [UIColor whiteColor];
        
        if (color) {
            image = [self generatorNothingToSee:color];
        }
        
        if (image) {
            [ALImagekeeper cacheImage:key image:image];
        }
    }
    
    return image;
}

#pragma mark -
+ (UIImage *)generatorNothingToSee:(UIColor *)color
{
    float upSpace = 10;
    
    UIImage *image = [UIImage imageNamed:@"defaultProductImage"];
    
    float length = image.size.height * 1.3 + upSpace;
    CGSize size = CGSizeMake(length, length);
    UIImage *base = [UIImage makePureColorImage:size Color:color];
    
    base = [base addImage:image atPoint:CGPointMake((length - image.size.width) / 2, upSpace)];
    
    {
        NSString *title = ALLocalizedString(@"Nothing to see", nil);
        
        NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
        [attributes setValue:[UIFont systemFontOfSize:15 * [UIScreen mainScreen].scale] forKey:NSFontAttributeName];
        [attributes setValue:COMMON_GRAY4_COLOR forKey:NSForegroundColorAttributeName];
        
        CGSize titleSize =
        [title boundingRectWithSize:CGSizeMake(size.width - 10, MAXFLOAT)
                            options:NSStringDrawingUsesLineFragmentOrigin
                         attributes:attributes
                            context:nil].size;
        
        CGPoint point = CGPointMake((size.width - titleSize.width) / 2,
                                    image.size.height * 1.05 + upSpace);
        
        base = [base addString:title
                      withfont:attributes
                       atPoint:CGPointMake(point.x, point.y)];
    }
    return base;
}


@end
