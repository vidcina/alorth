//
//  UIView+ALRotate.h
//  alorth
//
//  Created by w91379137 on 2015/9/11.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (ALRotate)

@property(nonatomic) IBInspectable float degrees;

@end
