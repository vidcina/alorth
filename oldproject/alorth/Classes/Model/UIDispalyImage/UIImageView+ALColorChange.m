//
//  UIImageView+ALColorChange.m
//  alorth
//
//  Created by w91379137 on 2015/9/11.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "UIImageView+ALColorChange.h"

@implementation UIImageView (ALColorChange)

-(void)setImageColorKey:(NSString *)imageColorKey
{
    if ([imageColorKey isEqualToString:@"COMMON_GRAY4_COLOR"]) {
        [self setImageColor:COMMON_GRAY4_COLOR];
    }
}

-(NSString *)imageColorKey
{
    return nil;
}

- (void)setImageColor:(UIColor *)imageColor
{
    #if !TARGET_INTERFACE_BUILDER
    if (self.image) {
        self.image = [UIImage changeColor:self.image
                                    color:imageColor];
    }
    #endif
}

- (UIColor *)imageColor
{
    return nil;
}

-(void)setResize:(CGSize)resize
{
    if (self.image) {
        self.image = [UIImage reSizeImage:self.image
                                   toSize:resize];
    }
}

- (CGSize)resize
{
    return CGSizeZero;
}

@end
