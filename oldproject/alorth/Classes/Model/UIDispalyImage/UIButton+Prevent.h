//
//  UIButton+Prevent.h
//  alorth
//
//  Created by w91379137 on 2016/2/3.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Prevent)

@property(nonatomic) IBInspectable float delayduobleClickTime;

@end
