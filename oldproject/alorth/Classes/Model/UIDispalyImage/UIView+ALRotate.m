//
//  UIView+ALRotate.m
//  alorth
//
//  Created by w91379137 on 2015/9/11.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "UIView+ALRotate.h"

@implementation UIView (ALRotate)

-(void)setDegrees:(float)degrees
{
    self.layer.transform =
    CATransform3DRotate(CATransform3DIdentity, degrees / 180.0f * M_PI, 0, 0, 1);
}

-(float)degrees
{
    return 0;
}

@end
