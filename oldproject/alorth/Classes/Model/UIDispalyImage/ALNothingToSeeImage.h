//
//  ALNothingToSeeImage.h
//  alorth
//
//  Created by w91379137 on 2015/12/11.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ALNothingToSeeImage : NSObject

+ (UIImage *)clearNothingToSeeImage;
+ (UIImage *)whiteNothingToSeeImage;

@end
