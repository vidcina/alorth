//
//  UIImageView+ALColorChange.h
//  alorth
//
//  Created by w91379137 on 2015/9/11.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (ALColorChange)

@property(nonatomic, strong) IBInspectable NSString *imageColorKey;
@property(nonatomic, strong) IBInspectable UIColor *imageColor;
@property(nonatomic) IBInspectable CGSize resize;

@end
