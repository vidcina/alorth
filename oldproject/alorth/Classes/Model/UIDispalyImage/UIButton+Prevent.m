//
//  UIButton+Prevent.m
//  alorth
//
//  Created by w91379137 on 2016/2/3.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "UIButton+Prevent.h"
static char kDelayduobleClickTimeKey;

@implementation UIButton (Prevent)

- (void)setDelayduobleClickTime:(float)delayduobleClickTime
{
    [self removeTarget:self
                action:@selector(userInteractionEnabledControl)
      forControlEvents:UIControlEventTouchDown];
    
    [self addTarget:self
             action:@selector(userInteractionEnabledControl)
   forControlEvents:UIControlEventTouchDown];
    
    objc_setAssociatedObject (self, &kDelayduobleClickTimeKey, @(delayduobleClickTime), OBJC_ASSOCIATION_RETAIN);
}

- (float)delayduobleClickTime
{
    return [objc_getAssociatedObject(self, &kDelayduobleClickTimeKey) floatValue];
}

- (void)userInteractionEnabledControl
{
    //NSLog(@"Delay start %f",self.delayduobleClickTime);
    self.enabled = NO;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(self.delayduobleClickTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        //NSLog(@"Delay end %f",self.delayduobleClickTime);
        self.enabled = YES;
    });
}

@end
