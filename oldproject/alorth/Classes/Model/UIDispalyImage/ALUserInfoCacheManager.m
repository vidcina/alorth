//
//  ALUserInfoCacheManager.m
//  alorth
//
//  Created by w91379137 on 2015/9/11.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALUserInfoCacheManager.h"

@implementation ALUserInfoCacheManager

#pragma mark - init
+(ALUserInfoCacheManager *)sharedInstance
{
    static ALUserInfoCacheManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[[self class] alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        cacheUserInfomation = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -
+(void)getLocalUserInfomation:(NSString *)userID
              completionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    [[ALUserInfoCacheManager sharedInstance] getLocalUserInfomation:userID
                                                    completionBlock:completionBlock];
}

-(void)getLocalUserInfomation:(NSString *)userID
              completionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    NSDictionary *cacheDict = cacheUserInfomation[userID];
    
    BOOL shouldCallAPI = NO;
    
    //檢查資料存在
    if (![cacheDict isKindOfClass:[NSDictionary class]]) {
        shouldCallAPI = YES;
    }
    
    if (!shouldCallAPI) {
        NSDate *cacheTime = cacheDict[ALUserInfoCacheManagerTimeKey];
        
        if (![cacheTime isKindOfClass:[NSDate class]]) {
            shouldCallAPI = YES;
        }
        else {
            if ([[NSDate date] timeIntervalSinceDate:cacheTime] > ALUserInfoCacheTime) {
                shouldCallAPI = YES;
            }
        }
    }
    
    if (shouldCallAPI) {
        [ALNetWorkAPI getUserInfomation:userID
                        completionBlock:^(NSDictionary *responseObject) {
                            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                                NSMutableDictionary *responseObjectCopy = [responseObject mutableCopy];
                                [responseObjectCopy setObject:[NSDate date] forKey:ALUserInfoCacheManagerTimeKey];
                                [cacheUserInfomation setObject:responseObjectCopy forKey:userID];
                                
                                completionBlock(responseObjectCopy);
                            }
                            else {
                                ALLog(@"A033 異常 %@",userID);
                                completionBlock(nil);
                            }
                        }];
    }
    else {
        //ALLog(@"A033 舊 %@",userID);
        completionBlock(cacheDict);
    }
}

@end
