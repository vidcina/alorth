//
//  ALImagekeeper.m
//
//  Created by w91379137 on 2015/9/11.
//

#import "ALImagekeeper.h"

@implementation ALImagekeeper

#pragma mark - init
+ (ALImagekeeper *)sharedInstance
{
    static ALImagekeeper *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[[self class] alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didReceiveMemoryWarning)
                                                     name:UIApplicationDidReceiveMemoryWarningNotification
                                                   object:nil];
        [self newImageCache];
    }
    return self;
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)didReceiveMemoryWarning
{
    [imageCache removeAllObjects];
    [self newImageCache];
}

#pragma mark - 
-(void)newImageCache
{
    imageCache = [[NSCache alloc] init];
    imageCache.countLimit = 10;
}

#pragma mark -
+(UIImage *)tryImage:(NSString *)key
{
    return [[ALImagekeeper sharedInstance] tryImage:key];
}

-(UIImage *)tryImage:(NSString *)key
{
    return [imageCache objectForKey:key];
}

+(void)cacheImage:(NSString *)key image:(UIImage *)image
{
    [[ALImagekeeper sharedInstance] cacheImage:key image:image];
}

-(void)cacheImage:(NSString *)key image:(UIImage *)image
{
    [imageCache setObject:image forKey:key];
}

@end
