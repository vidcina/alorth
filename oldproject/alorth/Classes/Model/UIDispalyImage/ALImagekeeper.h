//
//  ALImagekeeper.h
//
//  Created by w91379137 on 2015/9/11.
//

#import <Foundation/Foundation.h>

@interface ALImagekeeper : NSObject
{
    NSCache *imageCache;
}

+ (UIImage *)tryImage:(NSString *)key;

+ (void)cacheImage:(NSString *)key
             image:(UIImage *)image;

@end
