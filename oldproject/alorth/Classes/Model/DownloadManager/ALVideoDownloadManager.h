//
//  ALVideoDownloadManager.h
//  alorth
//
//  Created by w91379137 on 2015/6/9.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ALVideoDownloadManager : NSObject

+(ALVideoDownloadManager *)sharedManager;
@property (nonatomic, strong) NSMutableArray *curDownloadArray;
@property (nonatomic, strong) NSMutableArray *waitToDownloadArray;
@property (nonatomic) int maxDownload;

-(BOOL)isExistVideo:(NSString *)urlStr;

@end
