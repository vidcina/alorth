//
//  ALVideoDownloadManager.m
//  alorth
//
//  Created by w91379137 on 2015/6/9.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALVideoDownloadManager.h"

@implementation ALVideoDownloadManager

+(ALVideoDownloadManager *)sharedManager
{
    static ALVideoDownloadManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[[self class] alloc] init];
    });
    return sharedManager;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        _maxDownload = 3;
        _curDownloadArray = [NSMutableArray array];
        _waitToDownloadArray = [NSMutableArray array];
    }
    return self;
}

-(BOOL)isExistVideo:(NSString *)urlStr
{
    if (urlStr == nil) {
        return NO;
    }

    if ([ALNetWorkAPI cachedVideoFromURL:urlStr]) {
        return YES;
    }
    else {
        if (![_curDownloadArray containsObject:urlStr]) {
            if (![_waitToDownloadArray containsObject:urlStr]) {
                [_waitToDownloadArray addObject:urlStr];
            }
        }
        if (_waitToDownloadArray.count > 0) {
            [self checkToDownload];
        }
    }
    return NO;
}

-(void)checkToDownload
{
    if (_curDownloadArray.count >= _maxDownload) {
        ALLog(@"%@ 下載流程滿載",NSStringFromClass([self class]));
        return;
    }
    if (_waitToDownloadArray.count == 0) {
        ALLog(@"%@ 下載流程完結",NSStringFromClass([self class]));
        return;
    }

    NSString *aURLString = [_waitToDownloadArray firstObject];
    [_curDownloadArray addObject:aURLString];
    [_waitToDownloadArray removeObject:aURLString];
    
    ALLog(@"%@ 開始下載 \n%@",NSStringFromClass([self class]),aURLString);
    //http://stackoverflow.com/questions/13207335/downloading-large-files-with-afnetworking
    //https://github.com/AFNetworking/AFNetworking
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *url = [NSURL URLWithBugString:aURLString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSProgress *progress;
    NSURLSessionDownloadTask *downloadTask =
    [manager downloadTaskWithRequest:request
                            progress:&progress
                         destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
                             
        return [NSURL fileURLWithPath:[ALNetWorkAPI targetVideoFromURL:aURLString]];
                             
    }
                   completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
                       
        dispatch_async(dispatch_get_main_queue(), ^{
            ALLog(@"%@ 下載完成 : \n%@",NSStringFromClass([self class]),aURLString);
            [_curDownloadArray removeObject:aURLString];
            [self checkToDownload];
        });
                       
    }];
    
    [downloadTask resume];
    [progress addObserver:self
               forKeyPath:@"fractionCompleted"
                  options:NSKeyValueObservingOptionNew
                  context:NULL];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"fractionCompleted"]) {
        NSProgress *progress = (NSProgress *)object;
        ALLog(@"Progress… %f", progress.fractionCompleted);
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

@end
