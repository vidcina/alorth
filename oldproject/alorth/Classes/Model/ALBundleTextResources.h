//
//  ALBundleTextResources.h
//  alorth
//
//  Created by w91379137 on 2015/11/12.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ALBundleTextResources : NSObject

+ (instancetype)sharedInstance;

#pragma mark - 文字資料版本更新
- (void)versionCheck;
- (void)textDataInitialAndUpdate;
- (NSString *)categoryCachePath;
- (NSString *)languageCachePath;
- (NSString *)locationCachePath;

@property(nonatomic, strong) NSDictionary *categoryDict;
+ (NSArray *)categoryArray;

@property(nonatomic, strong) NSDictionary *languageDict;
+ (NSArray *)languageArray;

@property(nonatomic, strong) NSDictionary *locationDict;
+ (NSArray *)locationArray;

#pragma mark - 國家清單
+ (NSArray *)countryCodeArray;
//+ (NSArray *)countryArray;

@end
