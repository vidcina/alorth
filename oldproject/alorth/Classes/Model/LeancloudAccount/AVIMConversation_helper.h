//
//  AVIMConversation1.h
//  alorth
//
//  Created by John Hsu on 2015/8/16.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "AVIMConversation.h"

@interface AVIMConversation (OccupantHelper)
-(NSString *)otherId;
@end
