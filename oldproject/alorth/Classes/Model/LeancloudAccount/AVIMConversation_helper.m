//
//  AVIMConversation1.m
//  alorth
//
//  Created by John Hsu on 2015/8/16.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "AVIMConversation_helper.h"

@implementation AVIMConversation (OccupantHelper)
-(NSString *)otherId
{
    if ([self.members count] < 2) {
        return nil;
    }
    NSMutableArray *membersCopy = [self.members mutableCopy];
    [membersCopy removeObject:[[ALAppDelegate sharedAppDelegate] userID]];
    return [membersCopy objectAtIndex:0];
}
@end
