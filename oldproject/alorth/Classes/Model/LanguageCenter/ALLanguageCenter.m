//
//  ATLanguageCenter.m
//
//  Created by w91379137 on 2015/10/5.
//

#import "ALLanguageCenter.h"
#import "CHCSVParser.h"
#import "ALLanguageFormat.h"

static NSString *iso639Key = @"ISO 639-2 Key";
static NSString *languageName = @"languageName";

@interface ALLanguageCenter()
{
    CHCSVParser *parser;
    NSMutableArray *array;
}
@end

@implementation ALLanguageCenter

#pragma mark - Init
+ (instancetype)sharedInstance
{
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSURL *url = [[NSBundle mainBundle] URLForResource:@"AL - 輸入表" withExtension:@"csv"];
        parser = [[CHCSVParser alloc] initWithContentsOfCSVURL:url];
        array = [[NSArray arrayWithContentsOfDelimitedURL:url
                                                  options:CHCSVParserOptionsUsesFirstLineAsKeys | CHCSVParserOptionsRecognizesComments
                                                delimiter:','
                                                    error:nil] mutableCopy];
    }
    return self;
}

+ (void)autoConnectLocalizableString
{
    ALLanguageCenter *center = [ALLanguageCenter sharedInstance];
    NSString *currentLanguageKey = [PDSAccountManager sharedManager].userLanguage;
    
    if (!currentLanguageKey) {
        //使用者還未選擇過語言
        //抓取系統偏好語言
        currentLanguageKey = NSLocalizedString(kLanguageKey,nil);
    }
    
    if ([currentLanguageKey isEqualToString:
         [ALLanguageCenter languageISO639Key:LanguageCenterStatusEnglish]]) {
        center.managerLanguage = LanguageCenterStatusEnglish;
    }
    else if ([currentLanguageKey isEqualToString:
              [ALLanguageCenter languageISO639Key:LanguageCenterStatusChineseTraditional]]) {
        center.managerLanguage = LanguageCenterStatusChineseTraditional;
    }
    else {
        ALLog(@"找不到語言");
        center.managerLanguage = LanguageCenterStatusEnglish;
    }
}

#pragma mark - 
+ (NSString *)translationOfString:(NSString *)string
{
#if TARGET_INTERFACE_BUILDER
    return string;
#endif
    
    ALLanguageCenter *center = [ALLanguageCenter sharedInstance];
    return [center translateStringForLocKey:string
                               languageCode:[ALLanguageCenter languageISO639Key:center.managerLanguage]];
}

+ (NSString *)translationOfString:(NSString *)string language:(LanguageCenterStatus)aStatus
{
    ALLanguageCenter *center = [ALLanguageCenter sharedInstance];
    return [center translateStringForLocKey:string
                               languageCode:[ALLanguageCenter languageISO639Key:aStatus]];
}

#pragma mark -
+ (NSString *)languageISO639Key:(LanguageCenterStatus)aStatus
{
    return [self languageInfoDictionary:aStatus][iso639Key];
}

+ (NSString *)languageName:(LanguageCenterStatus)aStatus
{
    return [self languageInfoDictionary:aStatus][languageName];
}

+ (NSDictionary *)languageInfoDictionary:(LanguageCenterStatus)aStatus
{
    switch (aStatus) {
        case LanguageCenterStatusChineseTraditional:
            return @{iso639Key:@"zh-Hant",
                     languageName:@"Traditional Chinese"};
            
        //case LanguageCenterStatusEnglish:
        default:
            return @{iso639Key:@"en",
                     languageName:@"English"};
    }
}

#pragma mark - Input
+ (void)addTranslationObjects:(NSArray *)objects
                      forKeys:(NSArray *)keys
{
    CHCSVOrderedDictionary *line =
    [[CHCSVOrderedDictionary alloc] initWithObjects:objects
                                            forKeys:keys];
    
    [[ALLanguageCenter sharedInstance] addTranslation:line];
}

- (void)addTranslation:(CHCSVOrderedDictionary *)dict
{
    //取代或是新增
    NSPredicate *predicate =
    [NSPredicate predicateWithFormat:@"%K == %@",[ALLanguageCenter languageISO639Key:LanguageCenterStatusEnglish],dict[@"en"]];
    NSArray *filteredArray = [array filteredArrayUsingPredicate:predicate];
    
    if (filteredArray.count > 0) {
        [array removeObject:filteredArray];
    }
    [array addObject:dict];
}

+ (NSString *)translationOfFormatString:(NSString *)string
{
    NSArray *translationWords;
    NSArray *periodWords;
    
    [ALLanguageFormat translationWords:&translationWords
                           periodWords:&periodWords
                          sourceString:string];
    
    NSMutableString *completeString = [NSMutableString string];
    
    for (NSInteger k = 0; k < translationWords.count || k < periodWords.count; k++) {
        
        if (k < periodWords.count) {
            [completeString appendString:periodWords[k]];
        }
        
        if (k < translationWords.count) {
            [completeString appendString:[ALLanguageCenter translationOfString:translationWords[k]]];
        }
    }
    return completeString;
}

- (NSString *)translateStringForLocKey:(NSString *)locKey
                          languageCode:(NSString *)languageCode
{
    if (locKey.length == 0) {
        return @"";
    }
    
    NSString *complete = nil;
    
    //特殊版本
    if ([locKey rangeOfString:@"#"].length > 0) {
        complete = [self searchTranslateOfKey:locKey
                                 languageCode:languageCode];
        
        //去除符號
        {
            NSArray *split = [locKey componentsSeparatedByString:@"#"];
            locKey = split.firstObject;
        }
        
        //去除符號
        if (complete) {
            NSArray *split = [complete componentsSeparatedByString:@"#"];
            complete = split.firstObject;
        }
    }
    
    if (!complete) {
        complete = [self searchTranslateOfKey:locKey
                                 languageCode:languageCode];
    }
    
    if (complete) {
        return complete;
    }
    
    ALLog(@"%@ has no translation",locKey);
    return NSLocalizedString(locKey,nil);
}

- (NSString *)searchTranslateOfKey:(NSString *)key
                      languageCode:(NSString *)languageCode
{
    NSPredicate *predicate =
    [NSPredicate predicateWithFormat:@"%K == %@",[ALLanguageCenter languageISO639Key:LanguageCenterStatusEnglish],key];
    NSArray *filteredArray = [array filteredArrayUsingPredicate:predicate];
    if ([filteredArray count] > 0) {
        if ([filteredArray count] > 1) NSLog(@"%@ has many translation",key);
        
        NSString *translatedString = filteredArray.lastObject[languageCode];
        if (!translatedString || translatedString.length == 0) {
            NSLog(@"%@ was not translated to %@",key, languageCode);
            return nil;
        }
        return translatedString;
    }
    return nil;
}

@end
