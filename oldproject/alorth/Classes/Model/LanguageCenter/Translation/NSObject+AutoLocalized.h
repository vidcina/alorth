//
//  NSObject+AutoLocalized.h
//
//  Created by w91379137 on 2015/10/31.
//

#import <Foundation/Foundation.h>
#import "NSObject+SafeKVO.h"
#import "ALLanguageCenter.h"
#import "ATTranslationElement.h"

@interface NSObject (AutoLocalized)

- (NSMutableDictionary *)nonnullAranslationActionDict;
- (BOOL)executeTranslationElement:(id)anyElement;

@end
