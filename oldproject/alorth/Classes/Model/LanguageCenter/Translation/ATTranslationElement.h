//
//  ATTranslationElement.h
//
//  Created by w91379137 on 2015/10/31.
//

#import <Foundation/Foundation.h>

@interface ATTranslationElement : NSObject
{
    NSArray *translationWords;
    NSArray *periodWords;
}

#pragma mark - Class Setting Method
+ (void)setNoTranslate:(BOOL)isNoTranslate;
+ (BOOL)isNoTranslate;

#pragma mark - Input
- (void)inputString:(NSString *)aString;
- (void)inputFormatString:(NSString *)format, ...NS_FORMAT_FUNCTION(1,2);
- (void)inputTranslationWords:(NSArray *)translationWords
                  PeriodWords:(NSArray *)periodWords;

#pragma mark - Output
- (NSString *)translationString:(BOOL)isTranslate;

#pragma mark - Action
- (void)createAndRunTranslationBlockTarget:(NSObject *)target
                                    Action:(SEL)action
                                  EventKey:(NSString *)eventKey;

@property (copy) void(^translationBlock)();

@end
