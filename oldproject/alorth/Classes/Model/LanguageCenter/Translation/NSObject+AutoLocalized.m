//
//  NSObject+AutoLocalized.m
//
//  Created by w91379137 on 2015/10/31.
//

#import "NSObject+AutoLocalized.h"
static NSString *translationActionDictKey = @"translationActionDict";

@interface NSObject()

@property(nonatomic, strong) NSMutableDictionary *translationActionDict;

@end

@implementation NSObject (AutoLocalized)

- (void)setTranslationActionDict:(NSMutableDictionary *)translationActionDict
{
    NSString *key = @"translationActionDict";
    [self willChangeValueForKey:key];
    objc_setAssociatedObject (self, &translationActionDictKey, translationActionDict, OBJC_ASSOCIATION_RETAIN);
    [self didChangeValueForKey:key];
}

- (NSMutableDictionary *)translationActionDict
{
    id obj = objc_getAssociatedObject(self,&translationActionDictKey);
    return obj;
}

- (NSMutableDictionary *)nonnullAranslationActionDict
{
    NSMutableDictionary *dict = self.translationActionDict;
    if (![dict isKindOfClass:[NSMutableDictionary class]]) {
        dict = [NSMutableDictionary dictionary];
        self.translationActionDict = dict;
        [self connectLanguageCenter];
    }
    return dict;
}

#pragma mark - init connect
- (void)connectLanguageCenter
{
    [[ALLanguageCenter sharedInstance] addSafeObserver:self
                                            forKeyPath:@"managerLanguage"
                                               options:NSKeyValueObservingOptionInitial
                                               context:NULL];
}

#pragma mark - observe
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if (object == [ALLanguageCenter sharedInstance] &&
        [keyPath isEqualToString:@"managerLanguage"]) {
        [self executeTranslationActionDict];
    }
}

#pragma mark - loop Block
- (void)executeTranslationActionDict
{
    for (NSString *key in self.translationActionDict.allKeys) {
        [self executeTranslationElement:self.translationActionDict[key]];
    }
}

- (BOOL)executeTranslationElement:(id)anyElement
{
    if ([anyElement isKindOfClass:[ATTranslationElement class]]) {
        ATTranslationElement *element = (ATTranslationElement *)anyElement;
        if (element.translationBlock) {
            element.translationBlock();
        }
        return YES;
    }
    return NO;
}

@end
