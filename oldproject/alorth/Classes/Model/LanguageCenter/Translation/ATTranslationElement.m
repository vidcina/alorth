//
//  ATTranslationElement.m
//
//  Created by w91379137 on 2015/10/31.
//

#import "ATTranslationElement.h"
#import "ALLanguageCenter.h"
#import "ALLanguageFormat.h"

static BOOL ATTranslationNoTranslate;

@implementation ATTranslationElement

#pragma mark - Class Setting Method
+ (void)setNoTranslate:(BOOL)isNoTranslate;
{
    ATTranslationNoTranslate = isNoTranslate;
}

+ (BOOL)isNoTranslate
{
    return ATTranslationNoTranslate;
}

#pragma mark - Input
- (void)inputString:(NSString *)aString
{
    NSArray *aTranslationWords;
    NSArray *aPeriodWords;
    
    [ALLanguageFormat translationWords:&aTranslationWords
                           periodWords:&aPeriodWords
                          sourceString:aString];
    
    [self inputTranslationWords:aTranslationWords
                    PeriodWords:aPeriodWords];
}

- (void)inputFormatString:(NSString *)format, ...NS_FORMAT_FUNCTION(1,2)
{
    NSMutableArray *translationWordsx = [NSMutableArray array];
    NSArray *periodWordsx = [format componentsSeparatedByString:@"%@"];
    NSArray *testArray = [format componentsSeparatedByString:@"%"];
    if (testArray.count != periodWordsx.count) {
        NSLog(@"error format");
        return;
    }
    
    va_list argList;
    if (format)
    {
        va_start(argList, format);
        NSString *arg;
        for (NSInteger k = 0; k < periodWords.count - 1; k++) {
            arg = va_arg(argList, id);
            if ([arg isKindOfClass:[NSString class]]) {
                [translationWordsx addObject:arg];
            }
            else {
                [translationWordsx addObject:arg.description];
            }
        }
        va_end(argList);
    }
    [self inputTranslationWords:translationWordsx
                    PeriodWords:periodWordsx];
}

- (void)inputTranslationWords:(NSArray *)aTranslationWords
                  PeriodWords:(NSArray *)aPeriodWords
{
    translationWords = aTranslationWords;
    periodWords = aPeriodWords;
}

#pragma mark - Output
- (NSString *)translationString:(BOOL)isTranslate
{
    NSMutableString *string = [NSMutableString string];
    
    for (NSInteger k = 0; k < translationWords.count || k < periodWords.count; k++) {
        
        if (k < periodWords.count) {
            [string appendString:periodWords[k]];
        }
        
        if (k < translationWords.count) {
            if (isTranslate && !ATTranslationNoTranslate) {
                [string appendString:[ALLanguageCenter translationOfString:translationWords[k]]];
            }
            else {
                [string appendString:@"$"];
                [string appendString:translationWords[k]];
                [string appendString:@"$"];
            }
        }
    }
    return string;
}

#pragma mark - Action
- (void)createAndRunTranslationBlockTarget:(NSObject *)target
                                    Action:(SEL)action
                                  EventKey:(NSString *)eventKey
{
    __block typeof(target) __weak weakTarget = target;
    __block typeof(self) __weak weakElemet = self;
    self.translationBlock =
    ^{
        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [weakTarget performSelector:action withObject:[weakElemet translationString:YES]];
        #pragma clang diagnostic pop
    };
    self.translationBlock();
    
    if (!eventKey) eventKey = NSStringFromSelector(action);
    [target.nonnullAranslationActionDict setObject:self forKey:eventKey];
}

@end