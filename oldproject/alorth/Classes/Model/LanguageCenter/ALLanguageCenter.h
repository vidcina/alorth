//
//  ATLanguageCenter.h
//
//  Created by w91379137 on 2015/10/5.
//

#import <Foundation/Foundation.h>

#define kLanguageKey @"LanguageKey" //僅是在自動抓取現在系統選擇語言

typedef NS_OPTIONS(NSUInteger, LanguageCenterStatus) {
    LanguageCenterStatusEnglish,                //英文
    LanguageCenterStatusChineseTraditional,     //繁體中文
    LanguageCenterStatusEnd                     //結束符號
};

@interface ALLanguageCenter : NSObject

#pragma mark - Init
+ (instancetype)sharedInstance;
+ (void)autoConnectLocalizableString;
@property (nonatomic) LanguageCenterStatus managerLanguage;

+ (NSString *)languageISO639Key:(LanguageCenterStatus)aStatus;
+ (NSString *)languageName:(LanguageCenterStatus)aStatus;

#pragma mark - Input
+ (void)addTranslationObjects:(NSArray *)objects forKeys:(NSArray *)keys;
+ (NSString *)translationOfFormatString:(NSString *)string;

+ (NSString *)translationOfString:(NSString *)string;
+ (NSString *)translationOfString:(NSString *)string language:(LanguageCenterStatus)aStatus;

@end

//NSLocalizedString
#define ALLocalizedString(key, comment) [ALLanguageCenter translationOfFormatString:key]