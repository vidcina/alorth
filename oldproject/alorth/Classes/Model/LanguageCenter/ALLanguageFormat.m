//
//  ALLanguageFormate.m
//
//  Created by w91379137 on 2015/12/28.
//

#import "ALLanguageFormat.h"

@implementation ALLanguageFormat

+ (void)translationWords:(NSArray **)translationWordsPointer
             periodWords:(NSArray **)periodWordsPointer
            sourceString:(NSString *)sourceString
{
    if ([sourceString rangeOfString:@"$"].length > 0) {
        NSArray *comp = [sourceString componentsSeparatedByString:@"$"];
        
        NSMutableArray *aTranslationWords = [NSMutableArray array];
        NSMutableArray *aPeriodWords = [NSMutableArray array];
        
        for (int i = 0; i < comp.count; i++) {
            if (i % 2 == 0) {
                [aPeriodWords addObject:comp[i]];
            }
            else {
                [aTranslationWords addObject:comp[i]];
            }
        }
        *translationWordsPointer = aTranslationWords;
        *periodWordsPointer = aPeriodWords;
    }
    else {
        
        if (sourceString) {
            *translationWordsPointer = @[sourceString];
        }
        else {
            *translationWordsPointer = @[];
        }
        
        *periodWordsPointer = @[];
    }
}

@end
