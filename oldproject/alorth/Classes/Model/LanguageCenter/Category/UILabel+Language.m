//
//  UILabel+XIB.m
//  alorth
//
//  Created by w91379137 on 2015/11/4.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "UILabel+Language.h"
#import "NSObject+AutoLocalized.h"
#import "ATTranslationElement.h"

@implementation UILabel (Language)

-(void)setLocalText:(NSString *)localText
{
#if TARGET_INTERFACE_BUILDER
    self.text = localText;
#else
    ATTranslationElement *elemet = [[ATTranslationElement alloc] init];
    [elemet inputString:localText];
    [elemet createAndRunTranslationBlockTarget:self
                                        Action:@selector(setText:)
                                      EventKey:nil];
#endif
}

-(NSString *)localText
{
    ATTranslationElement *elemet =
    self.nonnullAranslationActionDict[NSStringFromSelector(@selector(text))];
    return [elemet translationString:NO];
}

@end
