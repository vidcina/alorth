//
//  UISegmentedControl+AL.h
//  alorth
//
//  Created by w91379137 on 2015/12/29.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UISegmentedControl (AL)

@property(nonatomic, strong) IBInspectable NSString *localText0;
@property(nonatomic, strong) IBInspectable NSString *localText1;

@end
