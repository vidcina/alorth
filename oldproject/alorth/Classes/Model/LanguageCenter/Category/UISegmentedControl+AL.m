//
//  UISegmentedControl+AL.m
//  alorth
//
//  Created by w91379137 on 2015/12/29.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "UISegmentedControl+AL.h"

@implementation UISegmentedControl (AL)

-(void)setLocalText0:(NSString *)localText0
{
#if TARGET_INTERFACE_BUILDER
    [self setTitle:localText0 forSegmentAtIndex:0];
#else
    ATTranslationElement *elemet = [[ATTranslationElement alloc] init];
    [elemet inputString:localText0];
    
    __block typeof(self) __weak weakTarget = self;
    __block typeof(elemet) __weak weakElemet = elemet;
    elemet.translationBlock =
    ^{
        [weakTarget setTitle:[weakElemet translationString:YES] forSegmentAtIndex:0];
    };
    elemet.translationBlock();
    [self.nonnullAranslationActionDict setObject:elemet forKey:@"localText0"];
#endif
}

- (NSString *)localText0
{
    return nil;
}

-(void)setLocalText1:(NSString *)localText1
{
#if TARGET_INTERFACE_BUILDER
    [self setTitle:localText1 forSegmentAtIndex:1];
#else
    ATTranslationElement *elemet = [[ATTranslationElement alloc] init];
    [elemet inputString:localText1];
    
    __block typeof(self) __weak weakTarget = self;
    __block typeof(elemet) __weak weakElemet = elemet;
    elemet.translationBlock =
    ^{
        [weakTarget setTitle:[weakElemet translationString:YES] forSegmentAtIndex:1];
    };
    elemet.translationBlock();
    [self.nonnullAranslationActionDict setObject:elemet forKey:@"localText1"];
#endif
}

- (NSString *)localText1
{
    return nil;
}

@end
