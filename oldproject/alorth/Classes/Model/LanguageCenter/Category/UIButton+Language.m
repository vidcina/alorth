//
//  UIButton+Language.m
//  alorth
//
//  Created by w91379137 on 2015/11/4.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "UIButton+Language.h"
#import "NSObject+AutoLocalized.h"
#import "ATTranslationElement.h"

@implementation UIButton (Language)

-(void)setNormalTitle:(NSString *)normalTitle
{
    self.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.titleLabel.minimumScaleFactor = 0.5;
    self.titleLabel.text = @"";//防止動畫
#if TARGET_INTERFACE_BUILDER
    [self setTitle:normalTitle forState:UIControlStateNormal];
#else
    ATTranslationElement *elemet = [[ATTranslationElement alloc] init];
    [elemet inputString:normalTitle];
    
    __block typeof(self) __weak weakTarget = self;
    __block typeof(elemet) __weak weakElemet = elemet;
    elemet.translationBlock =
    ^{
        [weakTarget setTitle:@"" forState:UIControlStateNormal];//如果沒有變換 可能會出現空白標題
        [weakTarget setTitle:[weakElemet translationString:YES] forState:UIControlStateNormal];
    };
    elemet.translationBlock();
    [self.nonnullAranslationActionDict setObject:elemet forKey:@"normalTitle"];
#endif
}

-(NSString *)normalTitle
{
    ATTranslationElement *elemet =
    self.nonnullAranslationActionDict[@"normalTitle"];
    return [elemet translationString:NO];
}

@end
