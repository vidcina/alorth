//
//  UITextView+Language.m
//  alorth
//
//  Created by w91379137 on 2015/12/29.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "UITextView+Language.h"

static char kPlaceholderLayerKey;
static char kPlaceholderKey;

@interface UITextView ()
<ALPlaceholderLayerDelegate>

@property(nonatomic) ALPlaceholderLayer *placeholderLayer;


@end

@implementation UITextView (Language)

#pragma mark - Getter / Setter
-(void)setPlaceholder:(NSString *)placeholder
{
    [self willChangeValueForKey:NSStringFromSelector(@selector(placeholder))];
    objc_setAssociatedObject (self, &kPlaceholderKey, placeholder, OBJC_ASSOCIATION_RETAIN);
    
    if (self.placeholderLayer == nil) {
        self.placeholderLayer = [[ALPlaceholderLayer alloc] init];
        [self.layer insertSublayer:self.placeholderLayer atIndex:0];
        [self.placeholderLayer setAlignmentMode:kCAAlignmentLeft];
        [self.placeholderLayer setForegroundColor:COMMON_GRAY4_COLOR.CGColor];
        self.placeholderLayer.contentsScale = [[UIScreen mainScreen] scale];
        
        self.placeholderLayer.textDelegate = self;
        [self.placeholderLayer checkText];
    }
    
    [self.placeholderLayer setString:placeholder];
    
    [self.placeholderLayer setFont:(__bridge CFTypeRef _Nullable)(self.font)];
    [self.placeholderLayer setFontSize:self.font.pointSize];
    [self.placeholderLayer setFrame:CGRectMake(5,
                                               10,
                                               self.bounds.size.width,
                                               self.bounds.size.height)];
    
    [self didChangeValueForKey:NSStringFromSelector(@selector(placeholder))];
}

-(NSString *)placeholder
{
    return objc_getAssociatedObject(self, &kPlaceholderKey);
}

-(void)setPlaceholderLayer:(ALPlaceholderLayer *)placeholderLayer
{
    [self willChangeValueForKey:NSStringFromSelector(@selector(placeholderLayer))];
    objc_setAssociatedObject (self, &kPlaceholderLayerKey, placeholderLayer, OBJC_ASSOCIATION_RETAIN);
    [self didChangeValueForKey:NSStringFromSelector(@selector(placeholderLayer))];
}

-(ALPlaceholderLayer *)placeholderLayer
{
    return objc_getAssociatedObject(self, &kPlaceholderLayerKey);
}

#pragma mark -
-(void)setLocalPlaceholder:(NSString *)localPlaceholder
{
#if TARGET_INTERFACE_BUILDER
    self.placeholder = localPlaceholder;
#else
    ATTranslationElement *elemet = [[ATTranslationElement alloc] init];
    [elemet inputString:localPlaceholder];
    [elemet createAndRunTranslationBlockTarget:self
                                        Action:@selector(setPlaceholder:)
                                      EventKey:nil];
#endif
}

-(NSString *)localPlaceholder
{
    ATTranslationElement *elemet =
    self.nonnullAranslationActionDict[NSStringFromSelector(@selector(placeholder))];
    return [elemet translationString:NO];
}

@end

#pragma mark - ALPlaceholderLayer
@implementation ALPlaceholderLayer

- (instancetype)init
{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(textViewTextDidBeginEditing:)
                                                     name:UITextViewTextDidBeginEditingNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(textViewTextDidEndEditing:)
                                                     name:UITextViewTextDidEndEditingNotification
                                                   object:nil];
    }
    return self;
}

- (void)textViewTextDidBeginEditing:(NSNotification *)notification
{
    if (self.textDelegate == notification.object) {
        self.opacity = 0;
    }
}

- (void)textViewTextDidEndEditing:(NSNotification *)notification
{
    if (self.textDelegate == notification.object) {
        [self checkText];
    }
}

- (void)checkText
{
    self.opacity = (self.textDelegate.text.length == 0) ? 0.7 : 0;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
