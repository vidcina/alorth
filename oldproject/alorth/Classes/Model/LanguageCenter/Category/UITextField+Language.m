//
//  UITextField+Language.m
//  alorth
//
//  Created by w91379137 on 2015/11/5.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "UITextField+Language.h"
#import "NSObject+AutoLocalized.h"
#import "ATTranslationElement.h"

@implementation UITextField (Language)

-(void)setLocalPlaceholder:(NSString *)localPlaceholder
{
#if TARGET_INTERFACE_BUILDER
    self.placeholder = localPlaceholder;
#else
    ATTranslationElement *elemet = [[ATTranslationElement alloc] init];
    [elemet inputString:localPlaceholder];
    [elemet createAndRunTranslationBlockTarget:self
                                        Action:@selector(setPlaceholder:)
                                      EventKey:nil];
#endif
}

-(NSString *)localPlaceholder
{
    ATTranslationElement *elemet =
    self.nonnullAranslationActionDict[NSStringFromSelector(@selector(placeholder))];
    return [elemet translationString:NO];
}

@end
