//
//  UITextField+Language.h
//  alorth
//
//  Created by w91379137 on 2015/11/5.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Language)

@property(nonatomic, strong) IBInspectable NSString *localPlaceholder;

@end
