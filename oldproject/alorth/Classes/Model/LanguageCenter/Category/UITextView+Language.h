//
//  UITextView+Language.h
//  alorth
//
//  Created by w91379137 on 2015/12/29.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (Language)

@property(nonatomic, strong) NSString *placeholder;
@property(nonatomic, strong) IBInspectable NSString *localPlaceholder;

@end

#pragma mark - ALPlaceholderLayer

@protocol ALPlaceholderLayerDelegate<NSObject>

-(NSString *)text;

@end

@interface ALPlaceholderLayer : CATextLayer

@property(nonatomic, weak) NSObject<ALPlaceholderLayerDelegate> *textDelegate;
- (void)checkText;

@end