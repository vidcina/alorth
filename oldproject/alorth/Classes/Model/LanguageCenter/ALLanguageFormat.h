//
//  ALLanguageFormat.h
//
//  Created by w91379137 on 2015/12/28.
//

#import <Foundation/Foundation.h>

@interface ALLanguageFormat : NSObject

+ (void)translationWords:(NSArray **)translationWordsPointer
             periodWords:(NSArray **)periodWordsPointer
            sourceString:(NSString *)sourceString;

@end
