//
//  ALWebViewController.m
//  alorth
//
//  Created by w91379137 on 2015/6/25.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALWebViewController.h"

@interface ALWebViewController ()

@end

@implementation ALWebViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    headerView.headerTitleText = self.titleString;
    
    NSURLRequest *aURLRequest =
    [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:self.urlString]];
    [webView loadRequest:aURLRequest];
}

-(void)viewWillAppearFirstTime:(BOOL)animated
{
    [super viewWillAppearFirstTime:animated];
    [headerView.backButton addTarget:self
                              action:@selector(back:)
                    forControlEvents:UIControlEventTouchUpInside];
}
@end
