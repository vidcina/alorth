//
//  ALWebViewController.h
//  alorth
//
//  Created by w91379137 on 2015/6/25.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicSubViewController.h"

@interface ALWebViewController : ALBasicSubViewController <UIWebViewDelegate>
{
    IBOutlet UIWebView *webView;
}

@property (nonatomic, strong) NSString *titleString;
@property (nonatomic, strong) NSString *urlString;

@end
