//
//  ALComplexSearchViewController.m
//  alorth
//
//  Created by w91379137 on 2015/11/2.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALComplexSearchViewController.h"

#import "ALGoodsSearchViewController.h"
#import "ALPersonSearchViewController.h"

@interface ALComplexSearchViewController ()
{
    ALGoodsSearchViewController *goodsSearchVC;
    ALPersonSearchViewController *personSearchVC;
}

@property (nonatomic, strong) NSString *searchingText;

@end

@implementation ALComplexSearchViewController

#pragma mark - VC Life
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (goodsSearchVC == nil) {
        goodsSearchVC = [[ALGoodsSearchViewController alloc] init];
        goodsSearchVC.delegateVC = self;
        [self addChildViewController:goodsSearchVC];
    }
    if (personSearchVC == nil) {
        personSearchVC = [[ALPersonSearchViewController alloc] init];
        personSearchVC.delegateVC = self;
        [self addChildViewController:personSearchVC];
    }
}

-(void)viewWillAppearFirstTime:(BOOL)animated
{
    [super viewWillAppearFirstTime:animated];
    [headerView.backButton addTarget:self
                              action:@selector(back:)
                    forControlEvents:UIControlEventTouchUpInside];
    
    self.selectTypeScrollView.infoObjectArray =
    [@[@(ALComplexSearchViewControllerStatusGoods),
       @(ALComplexSearchViewControllerStatusPerson)] mutableCopy];
    
    if ([topSearchBar.text length] == 0) {
        topSearchBar.text = self.searchingText;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (goodsSearchVC.view.superview == nil) {
        [self.searchResultContainerView addSubview:goodsSearchVC.view];
        [goodsSearchVC didMoveToParentViewController:self];
        [goodsSearchVC.view mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(goodsSearchVC.view.superview);
        }];
    }
    
    if (personSearchVC.view.superview == nil) {
        [self.searchResultContainerView addSubview:personSearchVC.view];
        [personSearchVC didMoveToParentViewController:self];
        [personSearchVC.view mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(personSearchVC.view.superview);
        }];
    }
    
    [topSearchBar.superview bringSubviewToFront:topSearchBar];
    
    switch (self.listStatus) {
        case ALComplexSearchViewControllerStatusInit:
        {
            self.listStatus = ALComplexSearchViewControllerStatusGoods; //驅動第一次搜尋
        }
            break;
        case ALComplexSearchViewControllerStatusGoods:
        {
            [goodsSearchVC controlPlayCell:YES];
        }
            break;
        default:
            break;
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [goodsSearchVC controlPlayCell:NO];
    [super viewWillDisappear:animated];
}

#pragma mark - Setter
- (void)firstSearchingText:(NSString *)aString
{
    _searchingText = aString;
    [self visibleListStartSearch];
}

- (void)setSearchingText:(NSString *)searchingText
{
    _searchingText = searchingText;
    [self visibleListStartSearch];
}

-(void)setListStatus:(ALComplexSearchViewControllerStatus)listStatus
{
    if (_listStatus == listStatus) {
        return;
    }
    
    float animationTime = _listStatus == ALComplexSearchViewControllerStatusInit ? 0 : 0.25;
    float windowWidth = [UIScreen mainScreen].bounds.size.width;
    
    _listStatus = listStatus;
    [UIView animateWithDuration:animationTime
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         goodsSearchVC.view.alpha = 1;
                         goodsSearchVC.view.layer.transform =
                         self.listStatus == ALComplexSearchViewControllerStatusGoods ?
                         CATransform3DIdentity : CATransform3DTranslate(CATransform3DIdentity, -windowWidth, 0, 0);
                         
                         personSearchVC.view.alpha = 1;
                         personSearchVC.view.layer.transform =
                         self.listStatus == ALComplexSearchViewControllerStatusPerson ?
                         CATransform3DIdentity : CATransform3DTranslate(CATransform3DIdentity, windowWidth, 0, 0);
                         
                     } completion:^(BOOL finished) {
                         
                     }];
    [self visibleListStartSearch];
}

- (void)visibleListStartSearch
{
    [goodsSearchVC controlPlayCell:self.listStatus == ALComplexSearchViewControllerStatusGoods];
    
    switch (self.listStatus) {
        case ALComplexSearchViewControllerStatusGoods:
        {
            goodsSearchVC.keyWord = self.searchingText;
            if (goodsSearchVC.listStatus == ALGoodsSearchViewControllerStatusInit) {
                goodsSearchVC.listStatus = ALGoodsSearchViewControllerStatusBestMatch;
                goodsSearchVC.shouldAutoSearch = YES;
            }
        }
            break;
        case ALComplexSearchViewControllerStatusPerson:
        {
            personSearchVC.searchingText = self.searchingText;
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - ALSelectTypeScrollViewDelegate
-(UIView *)typeViewOfIndex:(NSInteger)index
                infoObject:(NSObject *)infoObject
      selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    if ([infoObject isKindOfClass:[NSNumber class]]) {
        
        NSString *title = nil;
        NSNumber *number = (NSNumber *)infoObject;
        ALComplexSearchViewControllerStatus status = [number integerValue];
        if (status == ALComplexSearchViewControllerStatusGoods) title = @"Goods";
        if (status == ALComplexSearchViewControllerStatusPerson) title = @"Person";
        
        UIButton *aButton =
        [ALSelectTypeScrollView alSelectButton:title];
        
        [aButton addTarget:selectTypeScrollView
                    action:@selector(selectView:)
          forControlEvents:UIControlEventTouchUpInside];
        
        [selectTypeScrollView addSubview:aButton]; //不先加上去 無法執行下一步
        [aButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(selectTypeScrollView.mas_width).multipliedBy(0.5).offset(-1);
        }];
        
        return aButton;
    }
    else {
        return [[UIView alloc] initWithFrame:CGRectZero];
    }
}

-(void)didSelectIndex:(NSInteger)index
           infoObject:(NSObject *)infoObject
 selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    NSNumber *number = (NSNumber *)infoObject;
    self.listStatus = [number integerValue];
}

-(void)adjustSelectView:(UIView *)view
             infoObject:(NSObject *)infoObject
               selected:(BOOL)selected
   selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    [ALSelectTypeScrollView alSelectButton:(UIButton *)view Select:selected];
}

#pragma mark - UISearchBarDelegate
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [topSearchBar resignFirstResponder];
    self.searchingText = searchBar.text;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = NO;
    [topSearchBar resignFirstResponder];
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = YES;
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = NO;
}

@end
