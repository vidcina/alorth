//
//  ALGoodsSearchViewController.h
//  alorth
//
//  Created by w91379137 on 2015/11/2.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSUInteger, ALGoodsSearchViewControllerStatus) {
    ALGoodsSearchViewControllerStatusInit       = 0,
    ALGoodsSearchViewControllerStatusBestMatch  = 1,
    ALGoodsSearchViewControllerStatusBestSell   = 2,
    ALGoodsSearchViewControllerStatusNewest     = 3
};

@interface ALGoodsSearchViewController : UIViewController //因為不是整版畫面 不需要繼承Basic
<ALSelectTypeScrollViewDelegate, UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UIView *upbarView;
    IBOutlet UIButton *countryCodeButton;
    
    //商品列表
    NSMutableArray *goodsListArray;
    BOOL willReplaceWithNew;                    //是否清空之前資料
    __weak IBOutlet UITableView *mainTableView;
    ALTableViewControl *currentTableViewControl;
}

/*
 使用方式參考
 ALHomeViewController
 ALBasicSubViewController
 
 1.viewDidLoad 跟著 init 設定 selectSortArray
 2.viewWillAppear 移動到view上面
 3.設定好初始化參數 啟動 shouldAutoSearch
 
 */

@property (nonatomic, weak) ALBasicViewController *delegateVC;                      //連結主要視窗 通知push相關

@property (nonatomic) BOOL isUpbarDisplay;                                          //開關是否啟用上方列
@property (nonatomic, strong) NSArray *selectSortArray;                             //調整上方按鈕
@property (nonatomic, strong) IBOutlet ALSelectTypeScrollView *selectTypeScrollView;//切換排序狀態 橫向選單

@property (nonatomic, strong) NSString *keyWord;                                    //A006控制參數
@property (nonatomic) ALGoodsSearchViewControllerStatus listStatus;                 //A006控制參數
@property (nonatomic, strong) NSString *category;                                   //A006控制參數
@property (nonatomic, strong) NSString *userID;                                     //A006控制參數
@property (nonatomic) BOOL shouldAutoSearch;                                        //控制是否以上參數改變時自動查詢

-(void)controlPlayCell:(BOOL)isPlay;                                                //控制影片播放

@end
