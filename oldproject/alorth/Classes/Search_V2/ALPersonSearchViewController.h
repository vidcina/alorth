//
//  ALPersonSearchViewController.h
//  alorth
//
//  Created by w91379137 on 2015/11/2.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ALPersonSearchViewController : UIViewController //因為不是整版畫面 不需要繼承Basic
<UITableViewDataSource, UITableViewDelegate>
{
    //商品列表
    NSMutableArray *goodsListArray;
    BOOL willReplaceWithNew;                    //是否清空之前資料
    ALTableViewControl *currentTableViewControl;
}

@property (nonatomic, weak) ALBasicViewController *delegateVC;
@property (nonatomic, strong) UITableView *mainTableView;

@property (nonatomic, strong) NSString *searchingText;

@end
