//
//  ALPersonSearchViewController+PullMore.m
//  alorth
//
//  Created by w91379137 on 2015/11/3.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALPersonSearchViewController+PullMore.h"

static NSString *searchPersonA063 = @"searchPersonA063";

@implementation ALPersonSearchViewController (PullMore)

#pragma mark - scrollView Delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.mainTableView) {
        [currentTableViewControl scrollViewDidScroll:scrollView];
    }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView == self.mainTableView) {
        [currentTableViewControl scrollViewDidEndDragging:scrollView willDecelerate:(BOOL)decelerate];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.mainTableView) {
        [currentTableViewControl scrollViewDidEndDecelerating:(UIScrollView *)scrollView];
    }
}

#pragma mark - ALTableViewControlDelegate
-(void)checkNewData:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    willReplaceWithNew = YES;
    tableViewControl.lastDict[@"StartNum"] = @"0";
    
    ALLog(@"重新整理");
    [self addMBProgressHUDWithKey:searchPersonA063];
    [self goodsListLoadApiSourceParameters:tableViewControl.lastDict
                               FinishBlock:^(BOOL isSuccess) {
                                   [self removeMBProgressHUDWithKey:searchPersonA063];
                               }];
}

-(void)loadMore:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    
    ALLog(@"載入更多:目前第 %@ 頁 %lu 筆資料",
          tableViewControl.lastDict[@"StartNum"],
          (unsigned long)currentTableViewControl.apiDataArray.count);
    
    tableViewControl.lastDict[@"StartNum"] =
    [NSString stringWithFormat:@"%d",[tableViewControl.lastDict[@"StartNum"] intValue] + 1];
    
    [self addMBProgressHUDWithKey:searchPersonA063];
    [self goodsListLoadApiSourceParameters:tableViewControl.lastDict
                               FinishBlock:^(BOOL isSuccess) {
                                   [self removeMBProgressHUDWithKey:searchPersonA063];
                               }];
}

#pragma mark - API
-(void)goodsListLoadApiSourceParameters:(NSDictionary *)parameters
                            FinishBlock:(void (^)(BOOL isSuccess))finishBlockToRun
{
    [ALNetWorkAPI searchPersonA063Keyword:self.searchingText
                                 StartNum:parameters[@"StartNum"]
                          CompletionBlock:^(NSDictionary *responseObject) {
       
                              BOOL isWiseData =
                              [self checkListData:responseObject];
                              
                              if (!isWiseData) ALLog(@"%@ 錯誤",searchPersonA063);
                              finishBlockToRun(isWiseData);
                              
                          }];
}

- (BOOL)checkListData:(NSDictionary *)responseObject
{
    BOOL isWiseData = YES;
    NSArray *addArray = nil;
    
    isWiseData =
    isWiseData && [ALNetWorkAPI checkSerialNumber:@"A063" ResponseObject:responseObject];
    
    isWiseData =
    isWiseData && responseObject.safe[@"resBody"][@"AccountList"];
    
    isWiseData =
    isWiseData && [responseObject[@"resBody"][@"AccountList"] isKindOfClass:[NSArray class]];
    
    if (isWiseData) {
        addArray = responseObject[@"resBody"][@"AccountList"];
        
        if (willReplaceWithNew) {
            [currentTableViewControl.apiDataArray removeAllObjects];
        }
        
        if (addArray.count > 0) {
            [currentTableViewControl.apiDataArray addObjectsFromArray:addArray];
        }
        
        [self.mainTableView reloadData];
        
        if (willReplaceWithNew) {
            [self.mainTableView setContentOffset:CGPointZero animated:YES];
        }
        willReplaceWithNew = NO;
    }
    return isWiseData;
}

@end
