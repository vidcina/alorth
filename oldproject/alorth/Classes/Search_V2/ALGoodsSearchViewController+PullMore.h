//
//  ALGoodsSearchViewController+PullMore.h
//  alorth
//
//  Created by w91379137 on 2015/11/2.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALGoodsSearchViewController.h"

@interface ALGoodsSearchViewController (PullMore)
<ALTableViewControlDelegate>

-(void)checkNewData:(ALTableViewControl *)tableViewControl;
-(void)loadMore:(ALTableViewControl *)tableViewControl;

@end
