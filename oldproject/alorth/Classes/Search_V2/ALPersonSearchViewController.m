//
//  ALPersonSearchViewController.m
//  alorth
//
//  Created by w91379137 on 2015/11/2.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALPersonSearchViewController.h"
#import "ALPersonSearchViewController+PullMore.h"

#import "ALSearchPersonCell.h"

#import "ALPersonIntroductionViewController.h"

@interface ALPersonSearchViewController ()
{
}

@end

@implementation ALPersonSearchViewController

#pragma mark - VC Life
-(instancetype)init
{
    self = [super init];
    if (self) {
        goodsListArray = [NSMutableArray array];
        
        currentTableViewControl = [[ALTableViewControl alloc] init];
        currentTableViewControl.delegate = self;
        currentTableViewControl.apiDataArray = goodsListArray;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self alTableViewSetting:self.mainTableView];
}

#pragma mark - Setter
- (void)setSearchingText:(NSString *)searchingText
{
    if ([_searchingText isEqualToString:searchingText]) {
        return;
    }
    _searchingText = searchingText;
    
    [self checkNewData:currentTableViewControl];
}

#pragma mark - UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [goodsListArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 85;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ALSearchPersonCell *cell = [ALSearchPersonCell cellFromXib];
    
    NSDictionary *infoDict = goodsListArray[indexPath.row];
    [cell performWithInfoDict:infoDict];
    
    return cell;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([cell isKindOfClass:[ALSearchPersonCell class]]) {
        ALSearchPersonCell *cellx = (ALSearchPersonCell *)cell;
        NSDictionary *infoDict = cellx.infoDict;
        
        switch (cellx.cellSelect) {
            case ALSearchPersonCellSelecttNone:
            {
                ALPersonIntroductionViewController *next =
                [[ALPersonIntroductionViewController alloc] initWithUserId:infoDict[@"UserID"]];
                
                [self.delegateVC.navigationController pushViewController:next animated:YES];
            }
                break;
            case ALSearchPersonCellSelectFollow:
            {
                NSString *followStatus = infoDict[@"FollowStatus"];
                
                NSString *changeToStatus = [followStatus intValue] == 0 ? @"1" : @"0";
                
                [self addMBProgressHUDWithKey:@"changeFollowStatusA040"];
                [ALNetWorkAPI changeFollowStatusA040UserID:infoDict[@"UserID"]
                                              FollowStatus:changeToStatus
                                           CompletionBlock:^(NSDictionary *responseObject) {
                                               [self removeMBProgressHUDWithKey:@"changeFollowStatusA040"];
                                               
                                               if ([ALNetWorkAPI checkSerialNumber:@"A040"
                                                                    ResponseObject:responseObject]) {
                                                   
                                                   NSMutableDictionary *newDict = [infoDict mutableCopy];
                                                   [newDict setObject:changeToStatus forKey:@"FollowStatus"];
                                                   int number = [newDict[@"FollowerNumber"] intValue];
                                                   if ([changeToStatus isEqualToString:@"1"]) number++;
                                                   if ([changeToStatus isEqualToString:@"0"]) number--;
                                                   [newDict setObject:[NSString stringWithFormat:@"%d",number] forKey:@"FollowerNumber"];
                                                   
                                                   NSInteger index = [goodsListArray indexOfObject:infoDict];
                                                   if (index != NSNotFound) {
                                                       [goodsListArray removeObjectAtIndex:index];
                                                       [goodsListArray insertObject:newDict atIndex:index];
                                                       [self.mainTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                                                   }
                                                   else {
                                                       NSLog(@"NSNotFound");
                                                   }
                                               }
                                               else {
                                                   YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
                                               }
                                           }];
            }
                break;
                
            default:
                break;
        }
    }
}

@end
