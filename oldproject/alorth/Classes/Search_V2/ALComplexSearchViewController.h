//
//  ALComplexSearchViewController.h
//  alorth
//
//  Created by w91379137 on 2015/11/2.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALBasicSubViewController.h"
typedef NS_ENUM(NSUInteger, ALComplexSearchViewControllerStatus) {
    ALComplexSearchViewControllerStatusInit     = 0,
    ALComplexSearchViewControllerStatusGoods    = 1,
    ALComplexSearchViewControllerStatusPerson   = 2
};

@interface ALComplexSearchViewController : ALBasicSubViewController
<ALSelectTypeScrollViewDelegate>
{
    IBOutlet UISearchBar *topSearchBar;
}

- (void)firstSearchingText:(NSString *)aString;
@property (nonatomic) ALComplexSearchViewControllerStatus listStatus;
@property (nonatomic, strong) IBOutlet ALSelectTypeScrollView *selectTypeScrollView;
@property (nonatomic, strong) IBOutlet UIView *searchResultContainerView;

@end
