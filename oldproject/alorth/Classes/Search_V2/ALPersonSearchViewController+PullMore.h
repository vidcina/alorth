//
//  ALPersonSearchViewController+PullMore.h
//  alorth
//
//  Created by w91379137 on 2015/11/3.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALPersonSearchViewController.h"

@interface ALPersonSearchViewController (PullMore)
<ALTableViewControlDelegate>

-(void)checkNewData:(ALTableViewControl *)tableViewControl;
-(void)loadMore:(ALTableViewControl *)tableViewControl;

@end
