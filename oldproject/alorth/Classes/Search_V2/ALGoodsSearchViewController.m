//
//  ALGoodsSearchViewController.m
//  alorth
//
//  Created by w91379137 on 2015/11/2.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALGoodsSearchViewController.h"
#import "ALGoodsSearchViewController+PullMore.h"

@interface ALGoodsSearchViewController ()
{
    NSArray *upbarViewConstraints;
}

@end

@implementation ALGoodsSearchViewController

#pragma mark - VC Life
-(instancetype)init
{
    self = [super init];
    if (self) {
        goodsListArray = [NSMutableArray array];
        _isUpbarDisplay = YES;
        
        currentTableViewControl = [[ALTableViewControl alloc] init];
        currentTableViewControl.delegate = self;
        currentTableViewControl.apiDataArray = goodsListArray;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.clipsToBounds = YES;
    [[ALAppDelegate sharedAppDelegate] addSafeObserver:self
                                            forKeyPath:@"countryCode"
                                               options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionInitial
                                               context:nil];
    [self alTableViewSetting:mainTableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.selectTypeScrollView.infoObjectArray == nil) {
        self.selectTypeScrollView.contentTypeViewInset = UIEdgeInsetsMake(0, 0, 0, 0);
        self.selectTypeScrollView.contentTypeViewGap = 1;
        
        if (self.selectSortArray == nil) {
            //預設三個全開
            self.selectTypeScrollView.infoObjectArray =
            [@[@(ALGoodsSearchViewControllerStatusBestMatch),
               @(ALGoodsSearchViewControllerStatusBestSell),
               @(ALGoodsSearchViewControllerStatusNewest)] mutableCopy];
        }
        else {
            //使用選擇的
            self.selectTypeScrollView.infoObjectArray = [self.selectSortArray mutableCopy];
        }
        
        self.selectTypeScrollView.typeViewContainer.layer.borderColor = [UIColor clearColor].CGColor;
        self.selectTypeScrollView.typeViewContainer.layer.borderWidth = 0;
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
        view.backgroundColor = [UIColor colorWithHexString:@"818181"];
        [self.selectTypeScrollView insertSubview:view atIndex:0];
        
        [view mas_updateConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(self.selectTypeScrollView.mas_leading).offset(3);
            make.trailing.equalTo(self.selectTypeScrollView.mas_trailing).offset(-3);
            make.centerY.equalTo(self.selectTypeScrollView.mas_centerY);
            make.height.equalTo(@20);
        }];
        
        [self checkSelectTypeScrollView];
    }
    
    [self checkUpbarViewConstraints];
}

#pragma mark - Action
-(void)controlPlayCell:(BOOL)isPlay
{
    if (isPlay) {
        [ALProductTableViewCell detectCenterCellToPlay:mainTableView];
    }
    else {
        [ALProductTableViewCell stopAllPlayCell:mainTableView];
    }
}

-(void)checkUpbarViewConstraints
{
    if (self.isUpbarDisplay == NO && upbarViewConstraints == nil) {
        upbarViewConstraints =
        [upbarView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.view.mas_top);
        }];
    }
    
    if (self.isUpbarDisplay == YES && upbarViewConstraints != nil) {
        for (id obj in upbarViewConstraints)
        {
            if([obj isKindOfClass:MASConstraint.class])
            {
                [(MASConstraint *)obj uninstall];
            }
        }
        upbarViewConstraints = nil;
    }
}

#pragma mark - Setter
-(void)setIsUpbarDisplay:(BOOL)isUpbarDisplay
{
    _isUpbarDisplay = isUpbarDisplay;
    [self checkUpbarViewConstraints];
}

-(void)setKeyWord:(NSString *)keyWord
{
    if ([_keyWord isEqualToString:keyWord]) {
        return;
    }
    _keyWord = keyWord;
    [self startSearch];
}

- (void)setListStatus:(ALGoodsSearchViewControllerStatus)listStatus
{
    if (_listStatus == listStatus) {
        return;
    }
    _listStatus = listStatus;
    [self checkSelectTypeScrollView];
    
    [self startSearch];
}

- (void)checkSelectTypeScrollView
{
    //檢查橫選框是否 一致
    ALSelectTypeScrollView *selectView = self.selectTypeScrollView;
    if ([selectView.infoObjectArray[selectView.didSelectIndex] integerValue] != self.listStatus) {
        NSInteger newIndex = [selectView.infoObjectArray indexOfObject:@(self.listStatus)];
        if (newIndex != NSNotFound) {
            [selectView selectIndex:newIndex];
        }
    }
}

- (void)setShouldAutoSearch:(BOOL)shouldAutoSearch
{
    _shouldAutoSearch = shouldAutoSearch;
    if (shouldAutoSearch) {
        [self startSearch];
    }
}

- (void)startSearch
{
    if (self.shouldAutoSearch != YES) {
        return;
    }
    
    NSMutableDictionary *newStart = [NSMutableDictionary dictionary];
    willReplaceWithNew = YES;
    newStart[@"StartNum"] = @"0";
    if (self.listStatus == ALGoodsSearchViewControllerStatusBestMatch) newStart[@"Order"] = @"4";
    if (self.listStatus == ALGoodsSearchViewControllerStatusBestSell) newStart[@"Order"] = @"2";
    if (self.listStatus == ALGoodsSearchViewControllerStatusNewest) newStart[@"Order"] = @"0";
    if (self.keyWord) newStart[@"keyWord"] = self.keyWord;
    if (self.category) newStart[@"category"] = self.category;
    if (self.userID) newStart[@"UserID"] = self.userID;
    
    currentTableViewControl.lastDict = newStart;
    
    [mainTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    [self checkNewData:currentTableViewControl];
}

#pragma mark - ALSelectTypeScrollViewDelegate
-(UIView *)typeViewOfIndex:(NSInteger)index
                infoObject:(NSObject *)infoObject
      selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    if ([infoObject isKindOfClass:[NSNumber class]]) {
        
        NSString *title = nil;
        NSNumber *number = (NSNumber *)infoObject;
        ALGoodsSearchViewControllerStatus status = [number integerValue];
        if (status == ALGoodsSearchViewControllerStatusBestMatch) title = @"Best Match";
        if (status == ALGoodsSearchViewControllerStatusBestSell) title = @"Best sellers";
        if (status == ALGoodsSearchViewControllerStatusNewest) title = @"Newest";
        
        UIButton *aButton =
        [ALSelectTypeScrollView alSortStyleSelectButton:title];
        
        [aButton addTarget:selectTypeScrollView
                    action:@selector(selectView:)
          forControlEvents:UIControlEventTouchUpInside];
        
        [selectTypeScrollView addSubview:aButton]; //不先加上去 無法執行下一步
        [aButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(selectTypeScrollView.mas_width).multipliedBy(1.0 / selectTypeScrollView.infoObjectArray.count).offset(-1);
        }];
        
        return aButton;
    }
    else {
        return [[UIView alloc] initWithFrame:CGRectZero];
    }
}

-(void)didSelectIndex:(NSInteger)index
           infoObject:(NSObject *)infoObject
 selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    NSNumber *number = (NSNumber *)infoObject;
    self.listStatus = [number integerValue];
}

-(void)adjustSelectView:(UIView *)view
             infoObject:(NSObject *)infoObject
               selected:(BOOL)selected
   selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    [ALSelectTypeScrollView alSortStyleSelectButton:(UIButton *)view Select:selected];
}

#pragma mark - UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [goodsListArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *infoDict = goodsListArray[indexPath.row];
    return [ALProductTableViewCell settingCell:nil WithInfoDict:infoDict];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ALProductTableViewCell *cell = [ALProductTableViewCell cell];
    
    NSDictionary *infoDict = goodsListArray[indexPath.row];
    [ALProductTableViewCell settingCell:cell WithInfoDict:infoDict];
    
    return cell;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([cell isKindOfClass:[ALProductTableViewCell class]]) {
        ALProductTableViewCell *cellx = (ALProductTableViewCell *)cell;
        
        if (self.delegateVC) {
            NSString *action =
            [NSString stringWithFormat:@"%@ %@",NSStringFromClass([self.delegateVC class]),@"didSelectRowAtIndexPath"];
            
            [ALInspector logEventCategory:@"ProductDetail"          // Event category (required)
                                   Action:action                    // Event action (required)
                                    Label:self.keyWord              // Event label
                                    Value:nil];                     // Event value
            
            ALProductDetailViewController *next =
            [[ALProductDetailViewController alloc] initWithInfoDict:cellx.infoDict];
            [self.delegateVC.navigationController pushViewController:next animated:YES];
        }
    }
}

#pragma mark - observe
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if ([keyPath isEqualToString:@"countryCode"] && object == [ALAppDelegate sharedAppDelegate]) {
        [countryCodeButton setTitle:[ALAppDelegate sharedAppDelegate].countryCode forState:UIControlStateNormal];
    }
}

@end
