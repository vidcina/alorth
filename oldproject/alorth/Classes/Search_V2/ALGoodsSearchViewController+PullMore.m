//
//  ALGoodsSearchViewController+PullMore.m
//  alorth
//
//  Created by w91379137 on 2015/11/2.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALGoodsSearchViewController+PullMore.h"

static NSString *goodsA006 = @"goodsA006";

@implementation ALGoodsSearchViewController (PullMore)

#pragma mark - scrollView Delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == mainTableView) {
        [currentTableViewControl scrollViewDidScroll:scrollView];
        [ALProductTableViewCell detectCenterCellToPlay:mainTableView];
    }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView == mainTableView) {
        [currentTableViewControl scrollViewDidEndDragging:scrollView willDecelerate:(BOOL)decelerate];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == mainTableView) {
        [currentTableViewControl scrollViewDidEndDecelerating:(UIScrollView *)scrollView];
    }
}

#pragma mark - ALTableViewControlDelegate
-(void)checkNewData:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    willReplaceWithNew = YES;
    tableViewControl.lastDict[@"StartNum"] = @"0";
    
    ALLog(@"重新整理");
    [self addMBProgressHUDWithKey:goodsA006];
    [self goodsListLoadApiSourceParameters:tableViewControl.lastDict
                               FinishBlock:^(BOOL isSuccess) {
                                   [self removeMBProgressHUDWithKey:goodsA006];
                               }];
}

-(void)loadMore:(ALTableViewControl *)tableViewControl
{
    if (![YKSystemInfo isNetworkAvailable]) {
        YKSimpleAlert(ALLocalizedString(ALNetworkErrorMsg,nil));
        return;
    }
    
    ALLog(@"載入更多:目前第 %@ 頁 %lu 筆資料",tableViewControl.lastDict[@"StartNum"],(unsigned long)goodsListArray.count);
    
    tableViewControl.lastDict[@"StartNum"] =
    [NSString stringWithFormat:@"%d",[tableViewControl.lastDict[@"StartNum"] intValue] + 1];
    
    [self addMBProgressHUDWithKey:goodsA006];
    [self goodsListLoadApiSourceParameters:tableViewControl.lastDict
                               FinishBlock:^(BOOL isSuccess) {
                                   [self removeMBProgressHUDWithKey:goodsA006];
                               }];
}

#pragma mark - API
-(void)goodsListLoadApiSourceParameters:(NSDictionary *)parameters
                            FinishBlock:(void (^)(BOOL isSuccess))finishBlockToRun
{
    [ALProductTableViewCell stopAllPlayCell:mainTableView];
    [ALNetWorkAPI goodsA006Keyword:parameters[@"keyWord"]
                          Category:parameters[@"category"]
                             Order:parameters[@"Order"]
                          StartNum:parameters[@"StartNum"]
                            UserID:parameters[@"UserID"]
                   CompletionBlock:^(NSDictionary *responseObject) {
                       
                       BOOL isWiseData =
                       [self checkListData:responseObject];
                       
                       if (!isWiseData) ALLog(@"%@ 錯誤",goodsA006);
                       [ALProductTableViewCell detectCenterCellToPlay:mainTableView];
                       finishBlockToRun(isWiseData);
                   }];
}

- (BOOL)checkListData:(NSDictionary *)responseObject
{
    BOOL isWiseData = YES;
    NSArray *addArray = nil;
    
    isWiseData =
    isWiseData && [ALNetWorkAPI checkSerialNumber:@"A006" ResponseObject:responseObject];
    
    isWiseData =
    isWiseData && responseObject.safe[@"resBody"][@"GoodsList"];
    
    isWiseData =
    isWiseData && [responseObject[@"resBody"][@"GoodsList"] isKindOfClass:[NSArray class]];
    
    if (isWiseData) {
        addArray = responseObject[@"resBody"][@"GoodsList"];
        
        if (willReplaceWithNew) {
            [currentTableViewControl.apiDataArray removeAllObjects];
        }
        
        if (addArray.count > 0) {
            [currentTableViewControl.apiDataArray addObjectsFromArray:addArray];
        }
        
        [mainTableView reloadData];
        
        if (willReplaceWithNew) {
            [mainTableView setContentOffset:CGPointZero animated:NO];
        }
        willReplaceWithNew = NO;
    }
    return isWiseData;
}

@end
