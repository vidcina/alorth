//
//  PDSExtraViewController.m
//
//  Created by w91379137 on 2015/12/15.
//

#import "PDSExtraNavigationViewController.h"

@interface PDSExtraNavigationViewController ()

@end

@implementation PDSExtraNavigationViewController

- (nullable UIViewController *)popViewControllerAnimated:(BOOL)animated
{
    if (self.viewControllers.count > 1) {
        return [super popViewControllerAnimated:animated];
    }
    else {
        if (self.completeBlock) {
            self.completeBlock();
            self.completeBlock = nil;
        }
        return self.viewControllers.firstObject;
    }
}

- (nullable NSArray<__kindof UIViewController *> *)popToRootViewControllerAnimated:(BOOL)animated
{
    if (self.completeBlock) {
        self.completeBlock();
        self.completeBlock = nil;
    }
    return self.viewControllers;
}

@end
