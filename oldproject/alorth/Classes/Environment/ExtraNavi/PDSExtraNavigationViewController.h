//
//  PDSExtraNavigationViewController.h
//
//  Created by w91379137 on 2015/12/15.
//

#import <UIKit/UIKit.h>

@interface PDSExtraNavigationViewController : UINavigationController

@property(nonatomic,copy) void(^completeBlock)();

@end
