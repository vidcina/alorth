//
//  UIViewController+PDSMB.h
//  alorth
//
//  Created by w91379137 on 2015/9/10.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSObject (PDSMB)

#pragma mark -
-(void)addMBProgressHUDWithKey:(NSString *)key;
-(void)removeMBProgressHUDWithKey:(NSString *)key;

@end
