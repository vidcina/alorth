//
//  ALLogManager.h
//  alorth
//
//  Created by w91379137 on 2015/6/26.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ALLogManager : NSObject
{
}

+(ALLogManager *)sharedInstance;
-(void)writeLog:(NSString *)string;
@property (nonatomic, strong) NSMutableArray *recordLogArr;

@end
