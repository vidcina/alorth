//
//  ALLogViewController.h
//  alorth
//
//  Created by w91379137 on 2015/6/26.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ALLogViewController : UIViewController
<UITextFieldDelegate, UITextViewDelegate>
{
    id editFirstResponder;
    IBOutlet UITextView *displayTextView;
}

+(ALLogViewController *)sharedInstance;

@end
