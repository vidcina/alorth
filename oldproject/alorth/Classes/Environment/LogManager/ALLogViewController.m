//
//  ALLogViewController.m
//  alorth
//
//  Created by w91379137 on 2015/6/26.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALLogViewController.h"
#import "ALLogManager.h"
#import "UIView+Move.h"

@interface ALLogViewController ()

@end

@implementation ALLogViewController

#pragma mark - Init
+(ALLogViewController *)sharedInstance
{
    static ALLogViewController *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ALLogViewController alloc] init];
    });
    return sharedInstance;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    [self.view canMove];
    self.view.alpha = 0.7;
}

#pragma mark -
-(IBAction)renew:(UIButton *)sender
{
    NSString *str = @"";
    NSMutableArray *a = [ALLogManager sharedInstance].recordLogArr;
    for (NSUInteger k = 0; k < a.count; k++) {
        NSString *t = [a objectAtIndex:k];
        
        str = [NSString stringWithFormat:@"%@\n%@",t,str];
    }
    displayTextView.text = str;
}

-(IBAction)remove:(UIButton *)sender
{
    [self.view removeFromSuperview];
}

#pragma mark - UITextViewDelegate
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    editFirstResponder = textView;
    return YES;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        editFirstResponder = nil;
        return NO;
    }
    return YES;
}

@end
