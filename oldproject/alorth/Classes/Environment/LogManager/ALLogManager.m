//
//  ALLogManager.m
//  alorth
//
//  Created by w91379137 on 2015/6/26.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALLogManager.h"

@implementation ALLogManager

+ (instancetype)sharedInstance
{
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(void)writeLog:(NSString *)string
{
    if (self.recordLogArr == nil) {
        self.recordLogArr = [NSMutableArray array];
    }
    
    if ([self.recordLogArr count] >= 200) {
        [self.recordLogArr removeObjectAtIndex:0];
    }
    
    [self.recordLogArr addObject:string];
}

@end
