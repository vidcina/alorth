//
//  PDSEnvironmentViewController.m
//  ProphetDevelopSystem
//
//  Created by w91379137 on 2015/5/9.
//  Copyright (c) 2015年 w91379137. All rights reserved.
//

#import "PDSEnvironmentViewController.h"
#import "ALMediaMakerViewController.h"

//參數設定
#import "UIView+Move.h"
#import "ALLogViewController.h"
#import "ALLanguageCenter.h"

//附加功能
#import "PDSExtraNavigationViewController.h"
#import "ALLoginViewController.h"

@interface PDSEnvironmentViewController ()
{
    NSNumber *lastReturn;
}

@end

@implementation PDSEnvironmentViewController

#pragma mark - VC Life
+ (instancetype)sharedInstance
{
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    for (UIButton *button in tabbarButton) {
        UIImage *image = [button imageForState:UIControlStateNormal];
        image = [UIImage changeColor:image
                               color:[UIColor colorWithHexString:@"f8ec00"]];
        [button setImage:image forState:UIControlStateSelected];
        
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (isViewWillAppearAgain == NO) {
        isViewWillAppearAgain = YES;
        [self viewWillAppearFirstTime:animated];
    }
}

-(void)viewWillAppearFirstTime:(BOOL)animated
{
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (isViewDidAppearAgain == NO) {
        isViewDidAppearAgain = YES;
        [self viewDidAppearFirstTime:animated];
    }
    [self.view bringSubviewToFront:self.progressHUD];
}

-(void)viewDidAppearFirstTime:(BOOL)animated
{
    
#if TARGET_IPHONE_SIMULATOR && UseDevelopmentServer
    [self showTestTranslateButton];
#endif
}

#pragma mark - Rerange
- (void)rerangeSubViews
{
    if (testTranslateButton) {
        [self.view bringSubviewToFront:testTranslateButton];
    }
    if (self.progressHUD) {
        [self.view bringSubviewToFront:self.progressHUD];
    }
}

#pragma mark - Translate
- (void)showTestTranslateButton
{
    if (testTranslateButton == nil) {
        testTranslateButton = [UIButton buttonWithType:UIButtonTypeCustom];
        testTranslateButton.frame = CGRectMake(44, 20, 100, 44);
        testTranslateButton.backgroundColor = [UIColor cyanColor];
        testTranslateButton.alpha = 0.5;
        [self.view insertSubview:testTranslateButton aboveSubview:barContainer];
        
        
        testTranslateButton.normalTitle = @"英文";
        [testTranslateButton addTarget:self
                                action:@selector(changeTranslate)
                      forControlEvents:UIControlEventTouchUpInside];
        
        UIPanGestureRecognizer *panGestureRecognizer =
        [[UIPanGestureRecognizer alloc] initWithTarget:testTranslateButton
                                                action:@selector(panAction:)];
        
        [testTranslateButton addGestureRecognizer:panGestureRecognizer];
    }
    [self rerangeSubViews];
}

- (void)changeTranslate
{
    NSUInteger number = [ALLanguageCenter sharedInstance].managerLanguage;
    number++;
    if (number > LanguageCenterStatusEnd) {
        number = 0;
    }
    
    if (number < LanguageCenterStatusEnd) {
        [ATTranslationElement setNoTranslate:NO];
    }
    else {
        [ATTranslationElement setNoTranslate:YES];
    }
    
    [ALLanguageCenter sharedInstance].managerLanguage = number;
}

#pragma mark - Setter / Getter
-(void)setMainNavigationController:(ALNavigationViewController *)mainNavigationController
{
    _mainNavigationController = mainNavigationController;
    
    //加入內容
    //[self addChildViewController:_mainNavigationController];
    
    [self.view insertSubview:_mainNavigationController.view belowSubview:barContainer];
    [_mainNavigationController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top);
        make.bottom.equalTo(self.view.mas_bottom);
        make.leading.equalTo(self.view.mas_leading);
        make.trailing.equalTo(self.view.mas_trailing);
    }];
}

#pragma mark - Tab Bar
- (void)showBar
{
    [self changeBar:@(YES)];
}

- (void)hiddenBar
{
    [self changeBar:@(NO)];
}

- (void)changeBar:(NSNumber *)openNumber
{
    if (self.isBarAnimation == YES) {
        //NSLog(@"動畫中 等待中");
        
        if (lastReturn) {
            //註銷上一個等待中
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(changeBar:) object:lastReturn];
            lastReturn = nil;
        }
        
        //註冊等待中
        [self performSelector:@selector(changeBar:) withObject:openNumber afterDelay:0.25f];
        lastReturn = openNumber;
    }
    else {
        self.isBarAnimation = YES;
        //NSLog(@"開始 動畫中");
        
        BOOL shouldOpen = [openNumber boolValue];
        if (self.isBarShow == !shouldOpen) {
            //NSLog(@"狀態改變");
            
            [self barContainerDisplayStatus:!shouldOpen];
            
            [UIView animateWithDuration:0.25
                                  delay:0.0
                                options:UIViewAnimationOptionBeginFromCurrentState
                             animations:^{
                                 
                                 [self barContainerDisplayStatus:shouldOpen];
                                 
                             } completion:^(BOOL finished) {
                                 self.isBarAnimation = NO;//NSLog(@"動畫結束");
                                 self.isBarShow = shouldOpen;
                             }];
        }
        else {
            //NSLog(@"狀態相同 不需改變");
            self.isBarAnimation = NO;//NSLog(@"動畫結束");
        }
    }
}

- (void)barContainerDisplayStatus:(BOOL)isDisplayStatus
{
    if (isDisplayStatus) {
        barContainer.layer.transform = CATransform3DIdentity;
        barContainer.alpha = 1;
    }
    else {
        barContainer.layer.transform = CATransform3DTranslate(CATransform3DIdentity, 0, barContainer.frame.size.height, 0);
        barContainer.alpha = 0;
    }
}

-(float)tabbarHeight
{
    //ALLog(@"tabbarHeight %f",barContainer.frame.size.height);
    return barContainer.frame.size.height;
}

-(void)selectItemAction:(NSInteger)index
{
    for (UIButton *button in tabbarButton) {
        button.selected = (button.tag == index);
    }
}

-(IBAction)itemAction:(UIButton *)sender
{
    [self.mainNavigationController tabbarItemSelectOnIndex:[sender tag]];
}

#pragma mark - MBProgressHUD Setter / Getter
- (MBProgressHUD *)progressHUD
{
    if (_progressHUD == nil){
        _progressHUD = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:_progressHUD];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapMBAction:)];
        [_progressHUD addGestureRecognizer:tap];
        
        UITapGestureRecognizer *doubletap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleMBAction:)];
        doubletap.numberOfTapsRequired = 3;
        [_progressHUD addGestureRecognizer:doubletap];
    }
    return _progressHUD;
}

#pragma mark - MBProgressHUD
- (void)tapMBAction:(UITapGestureRecognizer *)sender
{
    ALLog(@"%@",self.mbKeyArray);
}

- (void)doubleMBAction:(UITapGestureRecognizer *)sender
{
    if (UseDevelopmentServer) {
        ALLog(@"強制移除 api :%@",self.mbKeyArray.firstObject);
        [self removeMBProgressHUDWithKey:self.mbKeyArray.firstObject];
    }
}

-(void)addMBProgressHUDWithKey:(NSString *)key
{
    if (![NSThread isMainThread]) {
        ALLog(@"not MainThread %@",key);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self addMBProgressHUDWithKey:key];
        });
        return;
    }
    
    if (!self.mbKeyArray) {
        _mbKeyArray = [NSMutableArray array];
    }
    
    if (_mbKeyArray.count == 0) {
        [self.progressHUD show:NO];
    }
    
    if (![_mbKeyArray containsObject:key]) {
        [_mbKeyArray addObject:key];
    }
}

-(void)removeMBProgressHUDWithKey:(NSString *)key
{
    if (![NSThread isMainThread]) {
        ALLog(@"not MainThread %@",key);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self removeMBProgressHUDWithKey:key];
        });
        return;
    }
    
    if ([_mbKeyArray containsObject:key]) {
        [_mbKeyArray removeObject:key];
    }
    
    if (_mbKeyArray.count == 0) {
        [self.progressHUD hide:NO];
    }
}

#pragma mark - MediaMaker
-(void)requestMakePhotoCompletionBlock:(void (^)(NSURL *assetURL, NSError *error))block
{
    ALMediaMakerViewController *mediaMakerViewController =
    [[ALMediaMakerViewController alloc] init];
    
    [self addChildViewController:mediaMakerViewController];
    mediaMakerViewController.view.frame = self.view.bounds;
    [self.view addSubview:mediaMakerViewController.view];
    [mediaMakerViewController didMoveToParentViewController:self];
    
    mediaMakerViewController.isVideoAvailable = NO;
    mediaMakerViewController.mediaType = ALTakePublishPhoto;
    
    __block typeof(mediaMakerViewController) __weak weakvc = mediaMakerViewController;
    [mediaMakerViewController setACompletionBlock:^(NSURL *assetURLx, NSError *errorx) {
        if (errorx.code != kALMediaMakerCancelErrorCode) {
            [[[UIAlertView alloc] initWithTitle:ALLocalizedString(@"Error",nil) message:[errorx localizedFailureReason] delegate:nil cancelButtonTitle:ALLocalizedString(@"OK",nil) otherButtonTitles:nil] show];
        }
        block(assetURLx,errorx);
        [weakvc.view removeFromSuperview];
        [weakvc removeFromParentViewController];
    }];
    [self rerangeSubViews];
}

-(void)requestMakeMediaCompletionBlock:(void (^)(NSURL *assetURL, NSError *error))block
{
    ALMediaMakerViewController *mediaMakerViewController =
    [[ALMediaMakerViewController alloc] init];
    
    [self addChildViewController:mediaMakerViewController];
    mediaMakerViewController.view.frame = self.view.bounds;
    [self.view addSubview:mediaMakerViewController.view];
    [mediaMakerViewController didMoveToParentViewController:self];

    mediaMakerViewController.isVideoAvailable = YES;
    mediaMakerViewController.mediaType = ALTakePublishPhoto;
    
    __block typeof(mediaMakerViewController) __weak weakvc = mediaMakerViewController;
    [mediaMakerViewController setACompletionBlock:^(NSURL *assetURLx, NSError *errorx) {
        if (errorx.code != kALMediaMakerCancelErrorCode) {
            [[[UIAlertView alloc] initWithTitle:ALLocalizedString(@"Error",nil) message:[errorx localizedFailureReason] delegate:nil cancelButtonTitle:ALLocalizedString(@"OK",nil) otherButtonTitles:nil] show];
        }
        block(assetURLx,errorx);
        [weakvc.view removeFromSuperview];
        [weakvc removeFromParentViewController];
    }];
    [self rerangeSubViews];
}

#pragma mark - Log
- (void)addLogManager
{
    ALLogViewController *aLogViewController = [ALLogViewController sharedInstance];
    [self.view addSubview:aLogViewController.view];
}

#pragma mark - Login
- (UINavigationController *)presentLoginViewController
{
    return [self submitViewController:[[ALLoginViewController alloc] init]];
}

#pragma mark - SubmitViewController
- (NSMutableDictionary *)submitViewControllerKeyDict
{
    if (_submitViewControllerKeyDict == nil) {
        _submitViewControllerKeyDict = [NSMutableDictionary dictionary];
    }
    return _submitViewControllerKeyDict;
}

- (UINavigationController *)submitViewController:(UIViewController *)viewController
{
    return
    [self submitViewController:viewController
               CompletionBlock:nil];
}

- (UINavigationController *)submitViewController:(UIViewController *)viewController
                                 CompletionBlock:(void (^)())completionBlock
{
    return
    [self submitViewController:viewController
             ViewControllerkey:nil
               CompletionBlock:completionBlock];
}

- (UINavigationController *)submitViewController:(UIViewController *)viewController
                               ViewControllerkey:(NSString *)viewControllerkey
                                 CompletionBlock:(void (^)())completionBlock
{
    if (![viewControllerkey isKindOfClass:[NSString class]]) {
        viewControllerkey = NSStringFromClass([viewController class]);
    }
    
    //組裝
    PDSExtraNavigationViewController *addNavi =
    [[PDSExtraNavigationViewController alloc] initWithRootViewController:viewController];
    addNavi.navigationBarHidden = YES;
    
    [self addChildViewController:addNavi];
    addNavi.view.frame = self.view.bounds;
    [self.view addSubview:addNavi.view];
    [addNavi.view mas_updateConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    //[addNavi didMoveToParentViewController:self];
    [self rerangeSubViews];
    
    __block typeof(addNavi) __weak weakNavi = addNavi;
    
    //登記
    [self.submitViewControllerKeyDict setObject:addNavi forKey:viewControllerkey];
    
    //結束
    addNavi.completeBlock = ^{
        
        //反登記
        [self.submitViewControllerKeyDict removeObjectForKey:viewControllerkey];
        
        //反組裝
        [weakNavi.view removeFromSuperview];
        [weakNavi removeFromParentViewController];
        NSLog(@"移除畫面");
        
        //通知
        if (completionBlock) {
            completionBlock();
        }
    };
    
    return addNavi;
}

@end
