//
//  PDSEnvironmentViewController+ALTestControl.m
//  alorth
//
//  Created by w91379137 on 2015/11/16.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "PDSEnvironmentViewController+ALTestControl.h"
#import "ALTestControlViewController.h"

@implementation PDSEnvironmentViewController (ALTestControl)

-(void)addTestControl
{
    ALTestControlViewController *vc = [ALTestControlViewController sharedInstance];
    [self.view addSubview:vc.view];
}

@end
