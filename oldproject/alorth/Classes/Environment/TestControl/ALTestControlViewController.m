//
//  ALTestControlViewController.m
//  alorth
//
//  Created by w91379137 on 2015/11/16.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALTestControlViewController.h"
#import "UIView+Move.h"
#import "SimplePingClient.h"

@implementation ALTestControlViewController

#pragma mark - Init
+ (instancetype)sharedInstance
{
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    [self.view canMove];
    self.view.alpha = 0.7;
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *build = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    
    versionLabel.text =
    [NSString stringWithFormat:@"version:%@,build:%@",version,build];
}

#pragma mark - IBAction
- (IBAction)showTranslateButton:(id)sender
{
    [[PDSEnvironmentViewController sharedInstance] showTestTranslateButton];
}

- (IBAction)showLogManager:(id)sender
{
    [[PDSEnvironmentViewController sharedInstance] addLogManager];
}

//http://stackoverflow.com/questions/22469810/get-ping-latency-from-host-in-objective-c/25843488#25843488
- (IBAction)pingNow:(id)sender
{
    [SimplePingClient pingHostname:[API_domain stringByReplacingOccurrencesOfString:@"https://"
                                                                         withString:@""]
                 andResultCallback:^(NSString *latency) {
                     //NSLog(@"your latency is: %@", latency ? latency : @"unknown");
                     showPingNow.text = latency ? latency : @"unknown";
                 }];
}

- (IBAction)pingA:(id)sender
{
    [SimplePingClient pingHostname:[@"https://test.alorth.com" stringByReplacingOccurrencesOfString:@"https://"
                                                                                         withString:@""]
                 andResultCallback:^(NSString *latency) {
                     //NSLog(@"your latency is: %@", latency ? latency : @"unknown");
                     showPingA.text = latency ? latency : @"unknown";
                 }];
}

- (IBAction)pingB:(id)sender
{
    [SimplePingClient pingHostname:[@"https://service.alorth.com" stringByReplacingOccurrencesOfString:@"https://"
                                                                                            withString:@""]
                 andResultCallback:^(NSString *latency) {
                     //NSLog(@"your latency is: %@", latency ? latency : @"unknown");
                     showPingB.text = latency ? latency : @"unknown";
                 }];
}

- (IBAction)remove:(UIButton *)sender
{
    [self.view removeFromSuperview];
}

- (IBAction)categoryCache:(UIButton *)sender
{
    [self airdrop:[[ALBundleTextResources sharedInstance] categoryCachePath]];
}

- (IBAction)languageCache:(UIButton *)sender
{
    [self airdrop:[[ALBundleTextResources sharedInstance] languageCachePath]];
}

- (IBAction)locationCache:(UIButton *)sender
{
    [self airdrop:[[ALBundleTextResources sharedInstance] locationCachePath]];
}

- (void)airdrop:(NSString *)path
{
    //NSString *fileName = [[path lastPathComponent] stringByDeletingPathExtension];
    
    
    DXURLItemSource *urlItemSource =
    [[DXURLItemSource alloc] initWithURL:[NSURL fileURLWithPath:path]
                                 subject:@"123"];
    
    UIActivityViewController *controller =
    [[UIActivityViewController alloc] initWithActivityItems:@[urlItemSource] applicationActivities:nil];
    
    controller.excludedActivityTypes =
    @[UIActivityTypePostToTwitter, UIActivityTypePostToFacebook,
      UIActivityTypePostToWeibo, UIActivityTypeMessage,
      UIActivityTypePrint, UIActivityTypeCopyToPasteboard,
      UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll,
      UIActivityTypeAddToReadingList, UIActivityTypePostToFlickr,
      UIActivityTypePostToVimeo, UIActivityTypePostToTencentWeibo,UIActivityTypeMail];
    
    // Present the controller
    if ([controller respondsToSelector:@selector(popoverPresentationController)])
    {
        // iOS 8+
        UIPopoverPresentationController *presentationController = [controller popoverPresentationController];
        presentationController.sourceView = self.view.window; // if button or change to self.view.
        presentationController.sourceRect = self.view.window.bounds;
        [[PDSEnvironmentViewController sharedInstance] presentViewController:controller animated:YES completion:nil];
    }
    else {
        [self presentViewController:controller animated:YES completion:nil];
    }
}

@end

@implementation DXURLItemSource
- (id)initWithURL:(NSURL *)url subject:(NSString *)subject;
{
    self = [super init];
    if (self != nil) {
        self.url = url;
        self.subject = subject;
    }
    return self;
}

- (id)activityViewControllerPlaceholderItem:(UIActivityViewController *)activityViewController
{
    return self.url;
}

- (id)activityViewController:(UIActivityViewController *)activityViewController itemForActivityType:(NSString *)activityType
{
    return self.url;
}

- (NSString *)activityViewController:(UIActivityViewController *)activityViewController subjectForActivityType:(NSString *)activityType
{
    // This will be the subject of the email, if the user chooses to send an email.
    return self.subject;
}

- (UIImage *)activityViewController:(UIActivityViewController *)activityViewController thumbnailImageForActivityType:(NSString *)activityType suggestedSize:(CGSize)size
{
    if ([activityType isEqualToString:UIActivityTypeAirDrop]) {
        // this is the preview image in the "Accept airdrop" dialog. We're using the
        // app icon here (there is no app icon in this bundle, so it will still be
        // blank) but you can use anything, even customize to the content.
        return [UIImage imageNamed:@"AppIcon"];
    }
    return nil;
}
@end
