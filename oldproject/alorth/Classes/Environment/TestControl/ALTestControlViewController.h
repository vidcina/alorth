//
//  ALTestControlViewController.h
//  alorth
//
//  Created by w91379137 on 2015/11/16.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ALTestControlViewController : UIViewController
{
    IBOutlet UILabel *showPingNow;
    IBOutlet UILabel *showPingA;
    IBOutlet UILabel *showPingB;
    
    IBOutlet UILabel *versionLabel;
}

+ (instancetype)sharedInstance;


@end

@interface DXURLItemSource : NSObject <UIActivityItemSource>
@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) NSString *subject;

- (id)initWithURL:(NSURL *)url subject:(NSString *)subject;
@end