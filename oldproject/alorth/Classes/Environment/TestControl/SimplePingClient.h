//
//  SimplePingClient.h
//  alorth
//
//  Created by w91379137 on 2015/11/17.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SimplePing.h"

@interface SimplePingClient : NSObject<SimplePingDelegate>

+(void)pingHostname:(NSString*)hostName andResultCallback:(void(^)(NSString* latency))result;

@end
