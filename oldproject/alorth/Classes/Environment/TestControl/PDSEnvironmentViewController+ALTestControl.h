//
//  PDSEnvironmentViewController+ALTestControl.h
//  alorth
//
//  Created by w91379137 on 2015/11/16.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "PDSEnvironmentViewController.h"

@interface PDSEnvironmentViewController (ALTestControl)

-(void)addTestControl;

@end
