//
//  PDSEnvironmentViewController.h
//  ProphetDevelopSystem
//
//  Created by w91379137 on 2015/5/9.
//  Copyright (c) 2015年 w91379137. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDSEnvironmentViewController : UIViewController
{
    BOOL isViewWillAppearAgain;
    BOOL isViewDidAppearAgain;
    
    IBOutletCollection(NSObject) NSArray *buttonConnect;
    IBOutletCollection(UIButton) NSArray *tabbarButton;
    
    IBOutlet UIView *barContainer;
    
    UIButton *testTranslateButton;
}

+ (instancetype)sharedInstance;
@property (strong, nonatomic) ALNavigationViewController *mainNavigationController;

#pragma mark -
- (void)showTestTranslateButton;

#pragma mark - Tab Bar
@property (nonatomic) BOOL isBarAnimation;
@property (nonatomic) BOOL isBarShow;
@property (nonatomic, readonly) float tabbarHeight;

- (void)showBar;
- (void)hiddenBar;
- (void)selectItemAction:(NSInteger)index;

#pragma mark - MBProgressHUD
@property (strong, strong, readonly) NSMutableArray *mbKeyArray;
@property (nonatomic, strong) MBProgressHUD *progressHUD;
- (void)addMBProgressHUDWithKey:(NSString *)key;
- (void)removeMBProgressHUDWithKey:(NSString *)key;

#pragma mark - MediaMaker
- (void)requestMakeMediaCompletionBlock:(void (^)(NSURL *assetURL, NSError *error))block;
- (void)requestMakePhotoCompletionBlock:(void (^)(NSURL *assetURL, NSError *error))block;

#pragma mark - LogManager
- (void)addLogManager;

#pragma mark - Login
- (UINavigationController *)presentLoginViewController;

#pragma mark - SubmitViewController
@property(nonatomic, strong) NSMutableDictionary *submitViewControllerKeyDict;

- (UINavigationController *)submitViewController:(UIViewController *)viewController;
- (UINavigationController *)submitViewController:(UIViewController *)viewController
                                 CompletionBlock:(void (^)())completionBlock;
- (UINavigationController *)submitViewController:(UIViewController *)viewController
                               ViewControllerkey:(NSString *)viewControllerkey
                                 CompletionBlock:(void (^)())completionBlock;

//- (void)submitViewController:(UIViewController *)viewController
//           ViewControllerkey:(NSString *)viewControllerkey
//             CompletionBlock:(void (^)())completionBlock;

@end
