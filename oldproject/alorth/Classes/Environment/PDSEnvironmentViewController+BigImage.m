//
//  PDSEnvironmentViewController+BigImage.m
//
//  Created by Chih-Ju Huang on 2015/9/15.
//

#import "PDSEnvironmentViewController+BigImage.h"
#import "NSObject+AssociatedObject.h"

static char keyBigImage;
static char keyBigImageBaseView;

@interface PDSEnvironmentViewController ()

@property (nonatomic, strong) UIImageView *bigImageView;
@property (nonatomic, strong) UIView *bigImageBaseView;

@end

@implementation PDSEnvironmentViewController (BigImage)

#pragma mark - property getter setter
- (void)setBigImageView:(UIImageView *)bigImageView
{
    [self setAssociatedObject:bigImageView forKey:&keyBigImage];
}

- (UIImageView *)bigImageView
{
    return (UIImageView *)[self associatedObject:&keyBigImage];
}

- (void)setBigImageBaseView:(UIView *)bigImageBaseView
{
    [self setAssociatedObject:bigImageBaseView forKey:&keyBigImageBaseView];
}

-(UIView *)bigImageBaseView
{
    return (UIView *)[self associatedObject:&keyBigImageBaseView];
}

#pragma mark -
-(void)showBigImageGesture:(UITapGestureRecognizer *)sender
{
    //檢查是否可以取得圖片
    UIImageView *tapImageView = (UIImageView *)sender.view;
    if (tapImageView.image == nil) {
        return;
    }
    
    [self showBigImage:tapImageView.image];
}

-(void)showBigImage:(UIImage *)aImage
{
    if (aImage == nil) {
        return;
    }
    
    //產生基底
    self.bigImageBaseView = [[UIView alloc] initWithFrame:self.view.bounds];
    self.bigImageBaseView.alpha = 0;
    self.bigImageBaseView.backgroundColor = [UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:0.8];
    [self.view addSubview:self.bigImageBaseView];
    
    //隱藏手勢
    UITapGestureRecognizer *aTapGestureRecognizer =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenBigImage:)];
    [self.bigImageBaseView addGestureRecognizer:aTapGestureRecognizer];
    
    
    //產生圖片
    self.bigImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    self.bigImageView.userInteractionEnabled = YES;
    
    //移動手勢
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] init];
    [pan addTarget:self action:@selector(panView:)];
    [self.bigImageView addGestureRecognizer:pan];
    
    //縮放手勢
    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] init];
    [pinch addTarget:self action:@selector(pinchView:)];
    [self.bigImageView addGestureRecognizer:pinch];
    
    //載入圖片
    self.bigImageView.image = aImage;
    self.bigImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    //顯示
    float scaleMinZoom =
    MIN(self.view.bounds.size.width / aImage.size.width,
        self.view.bounds.size.height / aImage.size.height);
    
    self.bigImageView.frame = CGRectMake(0, 0, aImage.size.width * scaleMinZoom, aImage.size.height * scaleMinZoom);
    
    self.bigImageView.center = self.bigImageBaseView.center;
    [self.bigImageBaseView addSubview:self.bigImageView];
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.bigImageBaseView.alpha = 1;
                     } completion:^(BOOL finished) {
                         
                     }];
}

-(void)hiddenBigImage:(UITapGestureRecognizer *)sender
{
    //隱藏
    UIImageView *tapView = (UIImageView *)sender.view;
    [UIView animateWithDuration:0.25
                     animations:^{
                         tapView.alpha = 0;
                     } completion:^(BOOL finished) {
                         [tapView removeFromSuperview];
                         self.bigImageBaseView = nil;
                         self.bigImageView = nil;
                     }];
}

-(void)panView:(UIPanGestureRecognizer *)pan
{
    CGPoint point = [pan translationInView:pan.view];
    
    pan.view.frame = CGRectOffset(pan.view.frame, point.x, point.y);
    [pan setTranslation:CGPointZero inView:pan.view];
    
    if (pan.state == UIGestureRecognizerStateEnded) {
        [self handleOverflowView:self.bigImageView Bounds:self.bigImageBaseView.bounds];
    }
}

-(void)pinchView:(UIPinchGestureRecognizer *)pinch
{
    float w = pinch.view.frame.size.width * pinch.scale;
    float h = pinch.view.frame.size.height * pinch.scale;
    CGPoint center = pinch.view.center;
    pinch.view.frame = CGRectMake(center.x - w / 2, center.y - h / 2, w, h);
    pinch.scale = 1.0;
    
    if (pinch.state == UIGestureRecognizerStateEnded) {
        [self handleOverflowView:self.bigImageView Bounds:self.bigImageBaseView.bounds];
    }
}

-(void)handleOverflowView:(UIView *)view Bounds:(CGRect)bounds
{
    //檢查放大倍率
    {
        //最小檢查
        float scaleMinZoom =
        MIN(bounds.size.width / view.frame.size.width,
            bounds.size.height / view.frame.size.height);
        
        if (scaleMinZoom > 1) {
            //必要放大
            float w = view.frame.size.width * scaleMinZoom;
            float h = view.frame.size.height * scaleMinZoom;
            CGPoint center = view.center;
            view.frame = CGRectMake(center.x - w / 2, center.y - h / 2, w, h);
        }
        
        //最大檢查
        float scaleMaxZoom =
        MAX(bounds.size.width / view.frame.size.width,
            bounds.size.height / view.frame.size.height);
        if (scaleMaxZoom < 1) {
            //必要縮小
            float w = view.frame.size.width * scaleMaxZoom;
            float h = view.frame.size.height * scaleMaxZoom;
            CGPoint center = view.center;
            view.frame = CGRectMake(center.x - w / 2, center.y - h / 2, w, h);
        }
    }
    
    //檢查邊界值
    {
        // horizontally
        float shift_x = 0;
        {
            if (view.frame.size.width < bounds.size.width) {
                //小邊步驟
                shift_x = self.view.center.x - view.center.x;
            }
            else {
                //大邊步驟
                if (view.frame.origin.x > 0) {
                    //左側違規
                    shift_x = -view.frame.origin.x;
                }
                else if (view.frame.origin.x + view.frame.size.width < bounds.size.width) {
                    //右側違規
                    shift_x = bounds.size.width - (view.frame.origin.x + view.frame.size.width);
                }
            }
        }
        
        // vertically
        float shift_y = 0;
        {
            if (view.frame.size.height < bounds.size.height) {
                //小邊步驟
                shift_y = self.view.center.y - view.center.y;
            }
            else {
                //大邊步驟
                if (view.frame.origin.y > 0) {
                    //上側違規
                    shift_y = -view.frame.origin.y;
                }
                else if (view.frame.origin.y + view.frame.size.height < bounds.size.height) {
                    //下側違規
                    shift_y = bounds.size.height - (view.frame.origin.y + view.frame.size.height);
                }
            }
        }
        
        [UIView animateWithDuration:0.1
                         animations:^{
                             view.frame = CGRectOffset(view.frame, shift_x, shift_y);
                         } completion:^(BOOL finished) {
                             
                         }];
    }
}

@end
