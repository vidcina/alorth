//
//  PDSEnvironmentViewController+BigImage.h
//
//  Created by Chih-Ju Huang on 2015/9/15.
//

#import "PDSEnvironmentViewController.h"

@interface PDSEnvironmentViewController (BigImage)

-(void)showBigImageGesture:(UITapGestureRecognizer *)sender;
-(void)showBigImage:(UIImage *)sender;
@end
