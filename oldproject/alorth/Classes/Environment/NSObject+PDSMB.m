//
//  UIViewController+PDSMB.m
//  alorth
//
//  Created by w91379137 on 2015/9/10.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "NSObject+PDSMB.h"

@implementation NSObject (PDSMB)

-(void)addMBProgressHUDWithKey:(NSString *)key
{
    NSString *newKey =
    [NSString stringWithFormat:@"%@ - %@",NSStringFromClass([self class]),key];
    [[PDSEnvironmentViewController sharedInstance] addMBProgressHUDWithKey:newKey];
}

-(void)removeMBProgressHUDWithKey:(NSString *)key
{
    NSString *newKey =
    [NSString stringWithFormat:@"%@ - %@",NSStringFromClass([self class]),key];
    [[PDSEnvironmentViewController sharedInstance] removeMBProgressHUDWithKey:newKey];
}

@end
