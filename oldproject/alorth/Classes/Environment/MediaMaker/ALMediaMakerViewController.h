//
//  ALMovieMakerViewController.h
//  alorth
//
//  Created by w91379137 on 2015/6/2.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PBJVision.h"
@import AssetsLibrary;

static float const max_videoTime = 15.0f;
static int kALMediaMakerCancelErrorCode = 0;
@interface ALMediaMakerViewController : UIViewController
<PBJVisionDelegate,
UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    AVCaptureVideoPreviewLayer *previewLayer;
    ALAssetsLibrary *assetLibrary;
    UIImagePickerController *picker;
    
    BOOL haveFrontCamera;
    BOOL haveBackCamera;
    
    NSTimer *videoTimer;
    IBOutlet UILabel *kvo_videoTimeLabel; //自動參照videoTime
}

//設定
@property (nonatomic) ALTakePublishType mediaType;
@property (nonatomic) BOOL isVideoAvailable;
@property (copy) void(^aCompletionBlock)(NSURL *assetURL, NSError *error);

//共用
@property (nonatomic, strong) IBOutlet UIView *upBarView;
@property (nonatomic, strong) IBOutlet UIView *upBarV1;
@property (nonatomic, strong) IBOutlet UIView *upBarV2;

@property (nonatomic, strong) IBOutlet UIView *previewView;
@property (nonatomic, strong) IBOutlet UIView *controlSpaceView;

//拍照系統
@property (nonatomic, strong) IBOutlet UIView *photoControlView;

@property (nonatomic, strong) IBOutlet UIButton *photoCaptureButton;

@property (nonatomic, strong) IBOutlet UIButton *photo_changeMediaTypeButton;
@property (nonatomic, strong) IBOutlet UIButton *photo_PreviewButton;

//錄影系統
@property (nonatomic, strong) IBOutlet UIView *videoControlView;

@property (nonatomic, strong) IBOutlet UIButton *videoStartButton;
@property (nonatomic, strong) IBOutlet UIButton *videoStopButton;

@property (nonatomic, strong) IBOutlet UIButton *video_changeMediaTypeButton;
@property (nonatomic, strong) IBOutlet UIButton *video_PreviewButton;

@property (nonatomic) float videoTime;

@end
