//
//  ALMovieMakerViewController.m
//  alorth
//
//  Created by w91379137 on 2015/6/2.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALMediaMakerViewController.h"

@interface ALMediaMakerViewController ()

@end

@implementation ALMediaMakerViewController

#pragma mark - VC Life

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    assetLibrary = [[ALAssetsLibrary alloc] init];
    [self renewLibraryPreview];
    //[self createSimulatorVideo];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIView *upBar = nil;
    #if (UIInterfaceVersion == 1)
    upBar = self.upBarV1;
    #endif
    
    #if (UIInterfaceVersion >= 2)
    upBar = self.upBarV2;
    ALPublishSwitchView *switchView = [ALPublishSwitchView sharedInstance];
    if (switchView.superview != upBar) {
        [self.upBarView addSubview:upBar];
        [upBar addSubview:switchView];
        [switchView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view.mas_top);
            make.leading.equalTo(self.view.mas_leading);
        }];
        
        [switchView addSafeObserver:self
                         forKeyPath:NSStringFromSelector(@selector(publishStatus))
                            options:NSKeyValueObservingOptionNew
                            context:NULL];
    }
    #endif
    [self.upBarView addSubview:upBar];
    [upBar mas_updateConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.upBarView);
    }];
    
    if (self.photoControlView.superview == nil) {
        self.photoControlView.hidden = YES;
        [self.controlSpaceView addSubview:self.photoControlView];
        [self.photoControlView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.controlSpaceView);
        }];
    }
    
    if (self.videoControlView.superview == nil) {
        self.videoControlView.hidden = YES;
        [self.controlSpaceView addSubview:self.videoControlView];
        [self.videoControlView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.controlSpaceView);
        }];
    }
    
    self.previewView.alpha = 0;
    
    NSArray *buttons = @[self.photoCaptureButton,
                         self.videoStartButton,
                         self.videoStopButton];
    for (UIButton *button in buttons) {
        
        float r1 = 110;
        float r2 = r1 - 10;
        float r3 = r2 - 25;
        
        UIImage *circle1 = [UIImage makePureColorImage:CGSizeMake(r1, r1) Color:[UIColor whiteColor]];
        circle1 = [circle1 createRoundedRadius:r1 / 2];
        
        UIImage *circle2 = [UIImage makePureColorImage:CGSizeMake(r2, r2) Color:[UIColor blackColor]];
        circle2 = [circle2 createRoundedRadius:r2 / 2];
        
        UIImage *circle3 = [UIImage makePureColorImage:CGSizeMake(r3, r3) Color:[button backgroundColor]];
        circle3 = [circle3 createRoundedRadius:r3 / 2];
        
        circle1 = [circle1 addImageCenter:circle2];
        circle1 = [circle1 addImageCenter:circle3];
        
        [button setBackgroundImage:circle1 forState:UIControlStateNormal];
    }
    
    for (UIView *view in buttons) {
        view.layer.cornerRadius = view.frame.size.width / 2;
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    {
        previewLayer = [[PBJVision sharedInstance] previewLayer];
        previewLayer.frame = _previewView.bounds;
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        [self.previewView.layer addSublayer:previewLayer];
    }
    
    #if !TARGET_IPHONE_SIMULATOR
    [[PBJVision sharedInstance] startPreview];
    #endif
    
    self.mediaType = _mediaType; //使刷新
    
    CGAffineTransform transform = CGAffineTransformMakeScale(0.1, 0.1);
    transform = CGAffineTransformRotate(transform, 2);
    self.previewView.transform = transform;
    
    [UIView animateWithDuration:0.02
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.previewView.transform = transform;
                     } completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.15
                                          animations:^{
                                              self.previewView.alpha = 1;
                                              self.previewView.transform = CGAffineTransformIdentity;
                                          }];
                     }];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [[PBJVision sharedInstance] stopPreview];
    [super viewDidDisappear:animated];
}

-(void)dealloc
{
    ALLog(@"%@ dealloc",NSStringFromClass([self class]));
}

-(IBAction)backAction:(UIButton *)sender
{
    NSError *error = [NSError errorWithDomain:@"com.alorth" code:kALMediaMakerCancelErrorCode userInfo:nil];
    self.aCompletionBlock(nil,error);
}

-(void)createSimulatorVideo
{
    #if TARGET_IPHONE_SIMULATOR
    NSString *dirPath = [ALNetWorkAPI cacheVideoPath];
    NSString *videoPath = [NSString stringWithFormat:@"%@/96e396b590f5c29146f3240a0f0146fc.mp4",dirPath];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:videoPath]) {
        
        [assetLibrary writeVideoAtPathToSavedPhotosAlbum:[NSURL URLWithString:videoPath] completionBlock:^(NSURL *assetURL, NSError *error1) {
            ALLog(@"測試影片載入 模擬器相簿");
        }];
        
    }
    else {
        ALLog(@"測試影片載入 模擬器相簿 失敗 檔案不存在");
    }
    #endif
}

#pragma mark - KVO
-(void)setMediaType:(ALTakePublishType)mediaType
{
    _mediaType = mediaType;
    
    PBJVision *vision = [PBJVision sharedInstance];
    vision.delegate = self;
    vision.cameraOrientation = PBJCameraOrientationPortrait;
    vision.focusMode = PBJFocusModeContinuousAutoFocus;
    vision.outputFormat = PBJOutputFormatSquare;
    
    self.photoControlView.hidden = !(mediaType == ALTakePublishPhoto);
    self.photo_changeMediaTypeButton.hidden = !self.isVideoAvailable;
    
    self.videoControlView.hidden = !(mediaType == ALTakePublishVideo);
    switch (mediaType) {
        case ALTakePublishPhoto:
        {
            vision.cameraMode = PBJCameraModePhoto;
        }
            break;
        case ALTakePublishVideo:
        {
            vision.cameraMode = PBJCameraModeVideo;
            vision.videoBitRate = PBJVideoBitRate480x360;
        }
            break;
            
        default:
        {
            
        }
            break;
    }
    [self initCaptureDevice];
}

-(void)initCaptureDevice
{
    //建立 AVCaptureDeviceInput
    NSArray *myDevices = [AVCaptureDevice devices];
    PBJVision *vision = [PBJVision sharedInstance];
    
    for (AVCaptureDevice *device in myDevices) {
        
        if ([device position] == AVCaptureDevicePositionFront) {
            ALLog(@"前攝影機硬體名稱: %@", [device localizedName]);
            haveFrontCamera = YES;
        }
        
        if ([device position] == AVCaptureDevicePositionBack) {
            ALLog(@"後攝影機硬體名稱: %@", [device localizedName]);
            haveBackCamera = YES;
        }
    }
    
    if (haveBackCamera == YES) {
        [vision setCameraDevice:PBJCameraDeviceBack];
    }
    else {
        [vision setCameraDevice:PBJCameraDeviceFront];
    }
}

-(void)setVideoTime:(float)videoTime
{
    _videoTime = videoTime;
    
    if (self.videoTime < 0) {
        kvo_videoTimeLabel.text = @"";
    }
    else {
        kvo_videoTimeLabel.text = [NSString stringWithFormat:@"%.2f",max_videoTime - _videoTime];
    }
}

-(void)observeValueForKeyPath:(NSString *)keyPath
                     ofObject:(id)object
                       change:(NSDictionary *)change
                      context:(void *)context
{
    if (object == [ALPublishSwitchView sharedInstance] &&
        [keyPath isEqualToString:NSStringFromSelector(@selector(publishStatus))]) {
        if ([ALPublishSwitchView sharedInstance].publishStatus == LiveSell) {
            [self backAction:nil];
        }
    }
}

#pragma mark - IBAction_共用
-(IBAction)cameraFlipAction:(id)sender
{
    if (haveFrontCamera == NO || haveBackCamera == NO) {
        return;
    }
    
    PBJVision *vision = [PBJVision sharedInstance];
    
    if (vision.cameraDevice == PBJCameraDeviceBack) {
        //focusButton.hidden = YES;
        [vision setCameraDevice:PBJCameraDeviceFront];
    }
    else {
        //focusButton.hidden = NO;
        [vision setCameraDevice:PBJCameraDeviceBack];
    }
}

-(IBAction)changeMediaTypeAction:(UIButton *)sender
{
    if (self.mediaType == ALTakePublishPhoto) {
        self.mediaType = ALTakePublishVideo;
    }
    else if (self.mediaType == ALTakePublishVideo) {
        self.mediaType = ALTakePublishPhoto;
    }
}

#pragma mark - IBAction_拍照
-(IBAction)photoCapture
{
    self.photoCaptureButton.enabled = NO;
    [[PBJVision sharedInstance] capturePhoto];
}

#pragma mark - IBAction_錄影
-(IBAction)startCaptureVideo
{
    self.videoStartButton.hidden = YES;
    self.videoStopButton.hidden = NO;
    
    self.video_changeMediaTypeButton.enabled = NO;
    self.video_PreviewButton.enabled = NO;
    
    [[PBJVision sharedInstance] startVideoCapture];
    
    if (videoTimer == nil) {
        self.videoTime = 0;
        videoTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                      target:self
                                                    selector:@selector(videoTimerScan:)
                                                    userInfo:nil
                                                     repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:videoTimer forMode:NSRunLoopCommonModes];
    }
}

-(void)videoTimerScan:(NSTimer *)sneder
{
    self.videoTime += sneder.timeInterval;
    
    if (self.videoTime >= max_videoTime) {
        [self stopCaptureVideo];
    }
}

-(IBAction)stopCaptureVideo
{
    if (videoTimer != nil) {
        [videoTimer invalidate];
        videoTimer = nil;
        self.videoTime = -1;
    }
    
    _videoStartButton.hidden = NO;
    _videoStopButton.hidden = YES;
    
    _video_changeMediaTypeButton.enabled = YES;
    _video_PreviewButton.enabled = YES;
    
    [[PBJVision sharedInstance] endVideoCapture];
}

#pragma mark - IBAction_載入相簿
-(IBAction)pickerAction
{
    if (picker == nil) {
        picker = [[UIImagePickerController alloc] init];
        picker.mediaTypes = @[(NSString *)kUTTypeImage];
        picker.delegate = self;
    }
    [self.view addSubview:picker.view];
}

#pragma mark - imagePicker
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self imagePickerRemove];
    self.aCompletionBlock([info objectForKey:UIImagePickerControllerReferenceURL],nil);
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self imagePickerRemove];
}

-(void)imagePickerRemove
{
    [picker.view removeFromSuperview];
    picker = nil;
}

#pragma mark - PBJVisionDelegate
-(void)vision:(PBJVision *)vision capturedVideo:(NSDictionary *)videoDict error:(NSError *)error
{
    if (error && [error.domain isEqual:PBJVisionErrorDomain] && error.code == PBJVisionErrorCancelled) {
        ALLog(@"recording session cancelled");
        return;
    }
    else if (error) {
        ALLog(@"encounted an error in video capture (%@)", error);
        return;
    }
    
    NSDictionary *aMediaDict = videoDict;
    NSString *aPath = [aMediaDict  objectForKey:PBJVisionVideoPathKey];
    
    [assetLibrary writeVideoAtPathToSavedPhotosAlbum:[NSURL URLWithString:aPath]
                                     completionBlock:^(NSURL *assetURL, NSError *error1) {
                                         self.aCompletionBlock(assetURL, error?error:error1);
    }];
}

-(void)vision:(PBJVision *)vision capturedPhoto:(nullable NSDictionary *)photoDict error:(nullable NSError *)error
{
    if (error && [error.domain isEqual:PBJVisionErrorDomain] &&
        error.code == PBJVisionErrorCancelled) {
        ALLog(@"recording session cancelled");
        self.photoCaptureButton.enabled = YES;
        return;
    }
    else if (error) {
        ALLog(@"encounted an error in video capture (%@)", error);
        self.photoCaptureButton.enabled = YES;
        return;
    }
    
    NSDictionary *aMediaDict = photoDict;
//    NSData *photoData = [aMediaDict objectForKey:PBJVisionPhotoJPEGKey];
//    NSDictionary *metadata = [aMediaDict objectForKey:PBJVisionPhotoMetadataKey];
    UIImage *image = [aMediaDict objectForKey:PBJVisionPhotoImageKey];
    
    float minSideLength = MIN(image.size.width, image.size.height);
    
    CGSize newSize2 = CGSizeMake(minSideLength, minSideLength);
    UIGraphicsBeginImageContext(newSize2);
    
    [image drawInRect:CGRectMake(-(image.size.width - newSize2.width) / 2,
                                 -(image.size.height - newSize2.height) / 2,
                                 image.size.width ,
                                 image.size.height)];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    image = [[UIImage alloc] initWithCGImage:image.CGImage scale:image.scale orientation:image.imageOrientation];
    /*
    if (vision.cameraDevice != PBJCameraDeviceBack) {
        
        // 修正 metadata orientation
        {
            NSMutableDictionary *new = [metadata mutableCopy];
            new[@"Orientation"] = @(UIImageOrientationUp);
            metadata = new;
        }
        
        // 修正前鏡頭的圖片
        {
            CGSize size = image.size;
            // 修正旋轉時的照片長寬
            if ([[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeLeft ||
                [[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeRight) {
                size = CGSizeMake(size.height, size.width);
            }
            
            // 修正前鏡頭照片旋轉
            UIGraphicsBeginImageContext(CGSizeMake(size.width, size.height));
            [[UIImage imageWithCGImage:[image CGImage]
                                 scale:image.scale
                           orientation:UIImageOrientationLeft]
             drawInRect:CGRectMake(0,0, size.width,size.height)];
            UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            image = newImage;
        }
        
        // 前鏡頭照片水平翻轉
        {
            CGSize size = image.size;
            UIGraphicsBeginImageContext(CGSizeMake(size.width, size.height));
            [[UIImage imageWithCGImage:[image CGImage]
                                 scale:image.scale
                           orientation:UIImageOrientationDownMirrored]
             drawInRect:CGRectMake(0,0, size.width,size.height)];
            UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            image = newImage;
        }
    }
    
    switch ([[UIDevice currentDevice] orientation]) {
        case UIDeviceOrientationLandscapeLeft: {
            CGSize size = image.size;
            UIGraphicsBeginImageContext(CGSizeMake(size.width, size.height));
            [[UIImage imageWithCGImage:[image CGImage]
                                 scale:image.scale
                           orientation:UIImageOrientationLeft]
             drawInRect:CGRectMake(0,0,size.width ,size.height)];
            UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            image = newImage;
            
        } break;
        case UIDeviceOrientationLandscapeRight: {
            CGSize size = image.size;
            UIGraphicsBeginImageContext(CGSizeMake(size.width, size.height));
            [[UIImage imageWithCGImage:[image CGImage]
                                 scale:image.scale
                           orientation:UIImageOrientationRight]
             drawInRect:CGRectMake(0,0,size.width ,size.height)];
            UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            image = newImage;
            
        } break;
        default:
            break;
    }
     */
    [assetLibrary writeImageToSavedPhotosAlbum:image.CGImage
                                      metadata:nil
                               completionBlock:^(NSURL *assetURL, NSError *error1) {
                                   self.aCompletionBlock(assetURL, error ? error : error1);
                                   self.photoCaptureButton.enabled = YES;
                                   
    }];
}

#pragma mark - Image
-(NSData *)squareImageOutput:(UIImage *)image
{
    float minSideLength = MIN(image.size.width, image.size.height);
    
    CGSize newSize2 = CGSizeMake(minSideLength, minSideLength);
    UIGraphicsBeginImageContext(newSize2);
    
    [image drawInRect:CGRectMake(-(image.size.width - newSize2.width) / 2,
                                 -(image.size.height - newSize2.height) / 2,
                                 image.size.width ,
                                 image.size.height)];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    image = [self limitImageLength:image];
    NSData *data = UIImageJPEGRepresentation(image, 0.1);
    
    return data;
}

-(void)renewLibraryPreview
{
    [assetLibrary enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos//ALAssetsGroupAll
                       usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                           //find my custom photo album and if it's there, enumerate through it.
                           if (group != nil) {
                               if (group.numberOfAssets != 0) {
                                   [group enumerateAssetsWithOptions:NSEnumerationReverse usingBlock:^(ALAsset *result, NSUInteger index, BOOL *innerStop) {
                                       if (result != nil) {
                                           CGImageRef imgRef = [result thumbnail];
                                           UIImage *img = [UIImage imageWithCGImage:imgRef];
                                           [self.video_PreviewButton setBackgroundImage:img forState:UIControlStateNormal];
                                           [self.photo_PreviewButton setBackgroundImage:img forState:UIControlStateNormal];
                                           *innerStop = YES;
                                           *stop = YES;
                                       } else if (result == nil){
                                           NSLog(@"enumeration of assets ended");
                                       }
                                   }];
                               }
                           }
                       }
                     failureBlock:^(NSError *error) {
                     }];
}

@end
