//
//  ALNavigationViewController.m
//  alorth
//
//  Created by w91379137 on 2015/6/26.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALNavigationViewController.h"

#import "ALHomeViewController.h"
#import "ALHomeV3Controller.H"

#import "ALFavoriteViewController.h"
#import "ALCartViewController.h"

#import "ALPublishViewController.h"
#import "ALChatTableViewController.h"
#import "ALAccountViewController.h"

@interface ALNavigationViewController ()

@end

@implementation ALNavigationViewController

#pragma mark - VC Life
- (instancetype)initHomeALNavigationViewController
{
    NSString *className = [ALNavigationViewController classNameInTabBar:ALNavigationTabBarHome];
    
    self = [super initWithRootViewController:[[NSClassFromString(className) alloc] init]];
    if (self) {
        self.navigationBarHidden = YES;
        self.delegate = self;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(checkReadAction:)
                                                     name:ALNotiOpenURLManagerNewAction
                                                   object:NULL];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - TabBar Setting
+ (NSString *)classNameInTabBar:(ALNavigationTabBarType)tabBarType
{
    switch ((ALNavigationTabBarType)tabBarType) {
        case ALNavigationTabBarHome: {
            if (UIInterfaceVersion < 3) return NSStringFromClass([ALHomeViewController class]);
            if (UIInterfaceVersion >= 3) return NSStringFromClass([ALHomeV3Controller class]);
        }
        case ALNavigationTabBarCart: {
            if (UIInterfaceVersion < 3) return NSStringFromClass([ALFavoriteViewController class]);
            if (UIInterfaceVersion >= 3) return NSStringFromClass([ALCartViewController class]);
        }
        case ALNavigationTabBarPublish: return NSStringFromClass([ALPublishViewController class]);
        case ALNavigationTabBarChat: return NSStringFromClass([ALChatTableViewController class]);
        case ALNavigationTabBarSetting: return NSStringFromClass([ALAccountViewController class]);
        default:
            break;
    }
    return nil;
}

+ (NSMutableArray *)tabbarClassesArray
{
    NSMutableArray *tabbarClassesArray = [NSMutableArray array];
    for (NSInteger k = 0; k < ALNavigationTabBarEnd; k++) {
        [tabbarClassesArray addObject:[self classNameInTabBar:(ALNavigationTabBarType)k]];
    }
    return tabbarClassesArray;
}

#pragma mark - UINavigationControllerDelegate
-(void)navigationController:(UINavigationController *)navigationController
     willShowViewController:(UIViewController *)viewController
                   animated:(BOOL)animated
{
    NSMutableArray *tabbarClassesArray = [ALNavigationViewController tabbarClassesArray];
    if ([tabbarClassesArray containsObject:NSStringFromClass([viewController class])]) {
        NSInteger select = [tabbarClassesArray indexOfObject:NSStringFromClass([viewController class])];
        [[PDSEnvironmentViewController sharedInstance] selectItemAction:select];
    }
    
    if (navigationController.viewControllers.count > 15) {
        ALLog(@"迴圈產生 %@",navigationController.viewControllers);
    }
}

- (void)navigationController:(UINavigationController *)navigationController
       didShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated
{
    [self checkReadAction:nil];
}

#pragma mark - Select Action
-(void)tabbarItemSelectOnIndex:(NSInteger)index
{
    [self clearOtherController:ALNavigationNone
                          push:(ALNavigationTabBarType)index
                 subController:nil];
}

- (void)clearOtherController:(ALNavigationAction)clearType
                        push:(ALNavigationTabBarType)index
               subController:(NSArray *)subControllerArray
{
    [self clearOtherController:clearType
                          push:index
                 subController:subControllerArray
               CompletionBlock:nil];
}

- (void)clearOtherController:(ALNavigationAction)clearType
                        push:(ALNavigationTabBarType)index
               subController:(NSArray *)subControllerArray
             CompletionBlock:(void (^)(ALNavigationViewController *nc))completionBlock
{
    //取出即將跳入畫面
    NSString *className = [ALNavigationViewController classNameInTabBar:index];
    Class displayClass = NSClassFromString(className);
    
    //檢查型態
    if (![displayClass isSubclassOfClass:[UIViewController class]]) {
        ALLog(@"Not a UIViewController Class : %@",NSStringFromClass(displayClass));
        return;
    }
    
    //原始畫面群
    NSMutableArray *controllers = [self.viewControllers mutableCopy];
    
    //清除殘留畫面
    switch ((ALNavigationAction)clearType) {
        case ALNavigationSubClear:
        {
            for (NSObject *obj in self.viewControllers) {
                if (![obj isKindOfClass:[ALBasicMainViewController class]]) {
                    ALLog(@"%@ 清空換頁移除",NSStringFromClass([obj class]));
                    [controllers removeObject:obj];
                }
            }
        }
            break;
        default:
        {
            
        }
            break;
    }
    
    //要檢查是否登入
    if (index == ALNavigationTabBarCart ||
        index == ALNavigationTabBarPublish ||
        index == ALNavigationTabBarChat ||
        index == ALNavigationTabBarSetting) {
        
        if ([PDSAccountManager isLoggedIn]) {
            //有登入
            //ALLog(@"%@ 進入設定",[ALLoginViewController userName]);
        }
        else {
            ALLog(@"刊登 聊天 設定 需要登入 導入登入頁");
            [[PDSEnvironmentViewController sharedInstance] presentLoginViewController];
            return;
        }
    }
    
    //Log
    [ALInspector logEventCategory:@"Tab"        // Event category (required)
                           Action:@"Click"      // Event action (required)
                            Label:className     // Event label
                            Value:nil];         // Event value
    
    [[[ALTabbarInspectorUnit create] readClassName:className] sendInspector];
    
    //基礎頁面
    UIViewController *newController = [controllers searchClass:displayClass];
    if (newController) {
        [controllers removeObject:newController];
    }
    else {
        newController = [[displayClass alloc] init];
    }
    [controllers addObject:newController];

    //新增其他子畫面
    if ([subControllerArray isKindOfClass:[NSArray class]]) {
        [controllers addObjectsFromArray:subControllerArray];
    }
    
    //進入畫面
    [self setViewControllers:controllers animated:NO];
    //ALLog(@"%@",controllers);
    
    if (completionBlock) {
        completionBlock(self);
    }
}

#pragma mark - Handle Action
- (void)checkReadAction:(NSNotification *)notification
{
    ALNotiOpenURLAction *action = nil;
    if (notification) {
        //剛剛收到新的
        if ([notification.object isKindOfClass:[ALNotiOpenURLAction class]]) {
            action = notification.object;
        }
    }
    else {
        //翻頁時檢查
        action = [[ALNotiOpenURLManager sharedInstance] lastNewAction];
    }
    
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        
//    });
    //正式處理
    if (action) {
        if (action.type == ALNotiOpenURLActionTypeURL) {
            if (action.streamID != nil) {
                
                action.status = ALNotiOpenURLActionTypeProcessing;
                
                //檢查是否 有另外一個影片的navi存在
                static NSString *videoSubmitToFront = @"videoSubmitToFront";
                
                UINavigationController *navi =
                [[PDSEnvironmentViewController sharedInstance].submitViewControllerKeyDict objectForKey:videoSubmitToFront];
                
                if (navi) {
                    [navi popToRootViewControllerAnimated:NO];
                }
                
                [self addMBProgressHUDWithKey:@"streamInfomationA066StreamID"];
                [ALNetWorkAPI streamInfomationA066StreamID:action.streamID
                                           CompletionBlock:^(NSDictionary *responseObject) {
                                               [self removeMBProgressHUDWithKey:@"streamInfomationA066StreamID"];
                                               
                                               BOOL isValid = NO;
                                               
                                               if ([ALNetWorkAPI checkSerialNumber:@"A066"
                                                                    ResponseObject:responseObject]) {
                                                   if (responseObject.safe[@"resBody"][@"StreamName"]) {
                                                       isValid = YES;
                                                   }
                                               }
                                               
                                               if (isValid) {
                                                   action.status = ALNotiOpenURLActionTypeComplete;
                                                   UIViewController *vc = [self playStreamVideoController:responseObject[@"resBody"]];
                                                   [[PDSEnvironmentViewController sharedInstance] submitViewController:vc
                                                                                                     ViewControllerkey:videoSubmitToFront
                                                                                                       CompletionBlock:nil];
                                               }
                                               else {
                                                   action.status = ALNotiOpenURLActionTypeFailure;
                                                   action.failureMsg = @"網路有問題 or 資料錯誤";
                                                   ALLog(@"%@",responseObject);
                                               }
                                           }];
            }
        }
    }
    
    
}

#pragma mark - Memory Warning
-(void)didReceiveMemoryWarning
{
    ALLog(@"will change vcs%@",self.viewControllers);
    NSMutableArray *saveControllers = [NSMutableArray array];
    BOOL startDelete = NO;
    BOOL savePrevious = YES;
    
    for (NSInteger k = self.viewControllers.count - 1; k >= 0; k--) {
        
        UIViewController *vc = self.viewControllers[k];
        
        if (startDelete == NO) {
            //第一個 ALProductDetailViewController
            if ([vc isKindOfClass:[ALProductDetailViewController class]]) {
                startDelete = YES;
            }
            [saveControllers insertObject:vc atIndex:0];
        }
        else {
            //除了前一個 其他 看是不是main 要不要刪
            if (savePrevious || [vc isKindOfClass:[ALBasicMainViewController class]]) {
                savePrevious = NO;
                [saveControllers insertObject:vc atIndex:0];
            }
            else {
                //放棄
            }
        }
    }
    ALLog(@"did change vcs%@",saveControllers);
    [self setViewControllers:saveControllers animated:NO];

    [super didReceiveMemoryWarning];
    
}

@end
