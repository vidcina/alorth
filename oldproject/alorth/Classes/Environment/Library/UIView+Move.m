//
//  UIView+Move.m
//  alorth
//
//  Created by w91379137 on 2015/11/16.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "UIView+Move.h"

@implementation UIView (Move)

- (void)canMove
{
    UIPanGestureRecognizer *panGestureRecognizer =
    [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panAction:)];
    [self addGestureRecognizer:panGestureRecognizer];
}

- (void)panAction:(UIPanGestureRecognizer *)gestureRecognizer
{
    UIView *piece = [gestureRecognizer view];
    
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan ||
        [gestureRecognizer state] == UIGestureRecognizerStateChanged) {
        
        CGPoint translation = [gestureRecognizer translationInView:[piece superview]];
        CGPoint newCenter = CGPointMake([piece center].x + translation.x, [piece center].y + translation.y);
        
        [piece setCenter:newCenter];
        [gestureRecognizer setTranslation:CGPointZero inView:[piece superview]];
    }
}

@end
