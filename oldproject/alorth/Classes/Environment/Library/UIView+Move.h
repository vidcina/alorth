//
//  UIView+Move.h
//  alorth
//
//  Created by w91379137 on 2015/11/16.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Move)

- (void)canMove;
- (void)panAction:(UIPanGestureRecognizer *)gestureRecognizer;

@end
