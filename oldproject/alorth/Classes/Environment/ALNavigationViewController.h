//
//  ALNavigationViewController.h
//  alorth
//
//  Created by w91379137 on 2015/6/26.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ALNavigationAction) {
    ALNavigationNone,
    ALNavigationSubClear,
    ALNavigationEnd
};

typedef NS_ENUM(NSInteger, ALNavigationTabBarType) {
    ALNavigationTabBarHome,
    ALNavigationTabBarPublish,
    ALNavigationTabBarChat,
    ALNavigationTabBarCart,
    ALNavigationTabBarSetting,
    ALNavigationTabBarEnd
};

@interface ALNavigationViewController : UINavigationController
<UINavigationControllerDelegate>
{

}

//初始化 在 home
- (instancetype)initHomeALNavigationViewController;

//紀錄的 tabbar Classes Name
+ (NSString *)classNameInTabBar:(ALNavigationTabBarType)tabBarType;
+ (NSMutableArray *)tabbarClassesArray;

//選中 tabbar
- (void)tabbarItemSelectOnIndex:(NSInteger)index;

//執行跳頁 並 清掉某些Controllers ＋ 跳到那一頁 ＋ 加入其他Controllers
- (void)clearOtherController:(ALNavigationAction)clearType
                        push:(ALNavigationTabBarType)index
               subController:(NSArray *)subControllerArray;

- (void)clearOtherController:(ALNavigationAction)clearType
                        push:(ALNavigationTabBarType)index
               subController:(NSArray *)subControllerArray
             CompletionBlock:(void (^)(ALNavigationViewController *nc))completionBlock;

#pragma mark - Handle Action
- (void)checkReadAction:(NSNotification *)notification;

@end
