//
//  ALLoginViewController.m
//  alorth
//
//  Created by John Hsu on 2015/7/18.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "JHFacebookLogin.h"
#import "ALProfileModifyViewController.h"

#import "ALLoginViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface ALLoginViewController()
<ALProfileModifyViewControllerDelegate, GIDSignInUIDelegate,FBSDKLoginButtonDelegate>

@property (nonatomic, strong) ACAccountStore *accountStore;
@property (nonatomic, strong) ACAccount *facebookAccount;

@end

@implementation ALLoginViewController
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardDidShow:)
                                                     name:UIKeyboardDidShowNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardDidShow:)
                                                     name:UIKeyboardDidChangeFrameNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillHide:)
                                                     name:UIKeyboardDidHideNotification
                                                   object:nil];

    }
    return self;
}
#pragma mark - VC Life
- (void)viewDidLoad
{
    [super viewDidLoad];
    [GIDSignIn sharedInstance].uiDelegate = self;
    loginButton.layer.cornerRadius = 4;
    signUpButton.layer.cornerRadius = 4;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(googleSignInSuccess:) name:@"googleSignInSuccess" object:nil];
    facebookLoginButton.readPermissions = @[@"public_profile", @"email", @"user_friends"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([[PDSAccountManager userName] length] &&
        [[PDSAccountManager password] length] && ![PDSAccountManager isThirdPartyLogin]) {
        usernameField.text = [PDSAccountManager userName];
        passwordField.text = [PDSAccountManager password];
    }
    
    [[ALAppDelegate sharedAppDelegate] addSafeObserver:self
                                            forKeyPath:@"countryCode"
                                               options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionInitial
                                               context:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [usernameField becomeFirstResponder];
}

#pragma mark - NSNotificationCenter

- (void)googleSignInSuccess:(NSNotification *)notification
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - ALProfileModifyViewControllerDelegate
- (void)savaButtonAction:(ALProfileModifyViewController *)profileModifyViewController
{
    if ([profileModifyViewController.modifykey isEqualToString:ALProfileModifyLocation]) {
        if ([profileModifyViewController.selectLocationLabel.text isKindOfClass:[NSString class]]) {
            countryField.text = profileModifyViewController.selectLocationLabel.text;
        }
        else {
            countryField.text = profileModifyViewController.selectLocationLabel.text;
        }
    }
    
    //回復tabbar
    BOOL isSub =
    [[PDSEnvironmentViewController sharedInstance].mainNavigationController.topViewController isKindOfClass:[ALBasicSubViewController class]];
    if (!isSub) {
        [[PDSEnvironmentViewController sharedInstance] showBar];
    }
}

#pragma mark - IBAction
- (IBAction)segmentChangeAction:(UISegmentedControl *)sender
{
    [self.tableView reloadData];
    [usernameField becomeFirstResponder];
}

- (IBAction)signupAction:(id)sender
{
    NSString *errorString = nil;
    if ([usernameField.text length] < 1) {
        errorString = @"Please enter your name";
    }
    if ([passwordField.text length] < 1) {
        errorString = @"Please enter your password";
    }
    if ([countryField.text length] < 1) {
        errorString = @"Please select your country";
    }
    if ([emailField.text length] < 1) {
        errorString = @"Please enter your email";
    }
    
    NSString *regex = @"^[a-z0-9A-Z]*$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    if (![predicate evaluateWithObject:usernameField.text]) {
        errorString = @"Usernames can only contain letters (a-z) and numbers (0-9)";
    }
    
    if (errorString) {
        [[[UIAlertView alloc] initWithTitle:ALLocalizedString(@"Error", nil)
                                    message:ALLocalizedString(errorString, nil)
                                   delegate:nil
                          cancelButtonTitle:ALLocalizedString(@"OK",nil)
                          otherButtonTitles:nil] show];
        return;
    }
    
    weakSelfMake(weakself)
    
    [self addMBProgressHUDWithKey:@"registerA001"];
    [ALNetWorkAPI registerA001Account:usernameField.text
                             password:passwordField.text
                                email:emailField.text
                              country:countryField.text
                      CompletionBlock:^(NSDictionary *responseObject) {
                          [self removeMBProgressHUDWithKey:@"registerA001"];
                          
                          if ([ALNetWorkAPI checkSerialNumber:@"A001"
                                               ResponseObject:responseObject]) {
                              [weakself loginAction:nil];
                          }
                          else {
                              YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A001", responseObject),nil));
//                              YKSimpleAlert(ALLocalizedString(ALErrorLocalizedString(@"$Registration failed$", @"A001", responseObject), nil));
                          }
    }];
}

- (IBAction)loginAction:(id)sender
{
    NSString *errorString = nil;
    if ([usernameField.text length] < 1) {
        errorString = @"Please enter your name";
    }
    if ([passwordField.text length] < 1) {
        errorString = @"Please enter your password";
    }
    if (errorString) {
        [[[UIAlertView alloc] initWithTitle:ALLocalizedString(@"Error", nil)
                                    message:ALLocalizedString(errorString, nil)
                                   delegate:nil
                          cancelButtonTitle:ALLocalizedString(@"OK",nil)
                          otherButtonTitles:nil] show];
        return;
    }
    [PDSAccountManager loginAccount:usernameField.text
                           Password:passwordField.text
                    CompletionBlock:^(NSDictionary *responseObject) {
                        if ([PDSAccountManager isLoggedIn]) {
                            [self.navigationController popViewControllerAnimated:YES];
                        }
                        else {
                            YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A002", responseObject),nil));
//                            YKSimpleAlert(ALLocalizedString(@"Login Failed",nil));
                        }
                    }];
}

- (IBAction)forgotPasswordAction:(id)sender
{
    [YKBlockAlert alertWithTitle:ALLocalizedString(@"Please enter your email",nil)
                defaultInputText:nil
                         button1:ALLocalizedString(@"Recover",nil)
                         button2:ALLocalizedString(@"Cancel",nil)
                          block1:^(NSString *emailText) {
                              
        [ALNetWorkAPI forgetPasswordA028UserInfo:emailText
                                 completionBlock:^(NSDictionary *responseObject) {
                                     if (responseObject[@"resBody"][@"ResultCode"] &&
                                         [responseObject[@"resBody"][@"ResultCode"] intValue] == 0) {
                                         YKSimpleAlert(ALLocalizedString(@"Please check your email",nil));
                                     }
                                     else {
                                         YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A028", responseObject),nil));
                                     }
        }];

    } block2:^(NSString *emailText){}];
}

- (IBAction)selectCountryAction
{
    ALProfileModifyViewController *next = [[ALProfileModifyViewController alloc] init];
    next.delegate = self;
    next.infoDict = countryField.text.length > 0 ? @{@"Location" : countryField.text} : @{};
    next.modifykey = ALProfileModifyLocation;
    [self.navigationController pushViewController:next animated:YES];
}

/* 20160518: wants to use facebook SDK login
- (IBAction)loginWithFacebook
{
    [JHFacebookLogin loginFacebook:^(NSDictionary *meObject, NSError *err) {
        if (!err && meObject) {
            NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:meObject];
            [userInfo setObject:@"facebook" forKey:@"type"];
            [PDSAccountManager loginWithSocialAccount:[userInfo copy] country:countryField.text completionBlock:^(NSDictionary *responseObject) {
                if ([PDSAccountManager isLoggedIn]) {
                    [self.navigationController popViewControllerAnimated:YES];
                }
                else {
                YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A123", responseObject),nil));
                }
            }];
        }
        else if ([err.domain isEqualToString:@"com.apple.accounts"] && err.code == ACErrorAccountNotFound) {
            UIAlertController *ac = [UIAlertController alertControllerWithTitle:ALLocalizedString(@"Error", nil) message:@"Please login facebook in system first" preferredStyle:UIAlertControllerStyleAlert];
            [ac addAction:[UIAlertAction actionWithTitle:ALLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
            }]];
            [ac addAction:[UIAlertAction actionWithTitle:ALLocalizedString(@"Proceed", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                NSURL *url = [NSURL URLWithString:@"prefs:root=FACEBOOK"];
                [[UIApplication sharedApplication] openURL:url];

            }]];
            [self presentViewController:ac animated:YES completion:nil];
        }
        else {
            YKSimpleAlert(@"Facbook login failed. Please try again later.");
        }
    }];
}
*/
- (void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error {
    if (error) {
        return;
    }
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@"id, name, email" forKey:@"fields"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]initWithGraphPath:@"me" parameters:parameters];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, NSDictionary *result, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (!error && [result isKindOfClass:[NSDictionary class]]) {
            NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:result];
            [userInfo setObject:@"facebook" forKey:@"type"];
            [userInfo setObject:FBSDKAccessToken.currentAccessToken.tokenString forKey:@"fbAccessToken"];
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [PDSAccountManager loginWithSocialAccount:[userInfo copy] country:countryField.text completionBlock:^(NSDictionary *responseObject) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                if ([PDSAccountManager isLoggedIn]) {
                    [self.navigationController popViewControllerAnimated:YES];
                }
                else {
                    YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A123", responseObject),nil));
                }
            }];
        } else {
            YKSimpleAlert(@"Facbook login failed. Please try again later.");
        }
    }];
}

- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton
{
    YKSimpleAlert(@"You have logged out Facebook");
}

#pragma mark - tableview datasource and delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (loginOrSignUpSegmentControl.selectedSegmentIndex == 0) {
        if (indexPath.row == 4) {
            return signUpButtonCell.frame.size.height;
        }
    }
    else {
        if (indexPath.row == 2) {
            return loginButtonCell.frame.size.height;
        }
    }
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (loginOrSignUpSegmentControl.selectedSegmentIndex == 0) {
        switch (indexPath.row) {
            case 0:
                return usernameCell;
                break;
            case 1:
                return passwordCell;
                break;
            case 2:
                return emailCell;
                break;
            case 3:
                return countryCell;
                break;
            case 4:
                return signUpButtonCell;
                break;
            default:
                break;
        }
    }
    else {
        switch (indexPath.row) {
            case 0:
                return usernameCell;
                break;
            case 1:
                return passwordCell;
                break;
            case 2:
                return loginButtonCell;
                break;
            default:
                break;
        }
    }
    return [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@""];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (loginOrSignUpSegmentControl.selectedSegmentIndex == 0) {
        return 5;
    }
    else {
        return 3;
    }
}

#pragma mark - Back Action
- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - observe
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if ([keyPath isEqualToString:@"countryCode"] && object == [ALAppDelegate sharedAppDelegate]) {
        if (countryField.text.length == 0) {
            NSString *countryCode = [ALAppDelegate sharedAppDelegate].countryCode;
            if ([countryCode length]) {
                
                NSArray *countryCodeArray = [ALBundleTextResources countryCodeArray];
                NSInteger index = [[countryCodeArray valueForKey:@"alpha-2"] indexOfObject:countryCode];
                if (index != NSNotFound) {
                    countryField.text = countryCode;
                }
                return;
            }
        }
        else {
            ALLog(@"使用者填入");
        }
    }
}

#pragma mark - GIDSignInUIDelegate

- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error
{
    [[NSUserDefaults standardUserDefaults] setObject:countryField.text forKey:@"currnetCountryCode"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Keyboard Auto System
- (void)keyboardDidShow:(NSNotification *)notification
{
    float keyboardHeight = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0);
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    self.tableView.contentInset = UIEdgeInsetsZero;
}
@end
