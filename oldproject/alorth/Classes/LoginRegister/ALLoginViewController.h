//
//  ALLoginViewController.h
//  alorth
//
//  Created by John Hsu on 2015/7/18.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/SignIn.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface ALLoginViewController : UIViewController
<UITableViewDataSource, UITableViewDelegate, GIDSignInUIDelegate, UITextFieldDelegate>
{
    IBOutlet UIButton *loginButton;
    IBOutlet UIButton *signUpButton;
    IBOutlet FBSDKLoginButton *facebookLoginButton;
    IBOutlet GIDSignInButton *googleLoginButton;

    IBOutlet UISegmentedControl *loginOrSignUpSegmentControl;
    
    IBOutlet UITextField *usernameField;
    IBOutlet UITextField *passwordField;
    IBOutlet UITextField *emailField;
    IBOutlet UITextField *countryField;
    
    IBOutlet UITableViewCell *usernameCell;
    IBOutlet UITableViewCell *passwordCell;
    IBOutlet UITableViewCell *emailCell;
    IBOutlet UITableViewCell *countryCell;

    IBOutlet UITableViewCell *signUpButtonCell;
    IBOutlet UITableViewCell *loginButtonCell;
}

@property(nonatomic,strong) IBOutlet UITableView *tableView;

@end
