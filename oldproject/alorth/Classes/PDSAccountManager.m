//
//  PDSAccountManager.m
//  PDSLeacloudDemo
//
//  Created by w91379137 on 2015/6/30.
//  Copyright (c) 2015年 w91379137. All rights reserved.
//

#import "PDSAccountManager.h"

#import "NSString+SHA3.h"
#import <Mixpanel/Mixpanel.h>
#import <Intercom/Intercom.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

static NSString *const kALDefaultServiceName = @"ALDefaultServiceName";

static NSString *const kALUserName = @"ALUserName";
static NSString *const kALUserPassword = @"ALUserPassword";
static NSString *const kALUserToken = @"ALUserToken";
static NSString *const kALAuthTime = @"ALAuthTime";

static NSString *const kPDSAccountManagerUserLanguageKey = @"PDSAccountManagerUserLanguageKey";

static NSString *_userName;
static NSString *_password;
static NSString *_authToken;
static NSString *_authTime;

@interface PDSAccountManager()

@property (nonatomic, strong) NSMutableArray *users;//全部的人正常帳號不能讀取

@end

@implementation PDSAccountManager

#pragma mark - sharedInstance
+ (instancetype)sharedManager
{
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

#pragma mark - Account/User Infomation
+ (void)saveUserName:(NSString *)username andPassword:(NSString *)password
{
    if ([username length] == 0 ||
        [password length] == 0) {
        [SFHFKeychainUtils deleteItemForUsername:kALUserName
                                  andServiceName:kALDefaultServiceName
                                     accessGroup:nil
                                           error:nil];
        [SFHFKeychainUtils deleteItemForUsername:kALUserPassword
                                  andServiceName:kALDefaultServiceName
                                     accessGroup:nil
                                           error:nil];
    }
    else {
        [SFHFKeychainUtils storeUsername:kALUserName
                             andPassword:username
                          forServiceName:kALDefaultServiceName
                             accessGroup:nil
                          updateExisting:YES
                                   error:nil];
        
        [SFHFKeychainUtils storeUsername:kALUserPassword
                             andPassword:password
                          forServiceName:kALDefaultServiceName
                             accessGroup:nil
                          updateExisting:YES
                                   error:nil];
    }
    
    _userName = username;
    _password = password;
}

+ (NSString *)userName
{
    if (!_userName) {
        _userName =
        [SFHFKeychainUtils getPasswordForUsername:kALUserName
                                   andServiceName:kALDefaultServiceName
                                      accessGroup:nil
                                            error:nil];
    }
    if (_userName) {
        return _userName;
    }
    return @"";
}

+ (NSString *)password
{
    if (!_password) {
        _password =
        [SFHFKeychainUtils getPasswordForUsername:kALUserPassword
                                   andServiceName:kALDefaultServiceName
                                      accessGroup:nil
                                            error:nil];
    }
    if (_password) {
        return _password;
    }
    return @"";
}

+ (void)saveAccessToken:(NSString *)token andAuthTime:(NSString *)authTime
{
    if ([token length] == 0 ||
        [authTime length] == 0) {
        [SFHFKeychainUtils deleteItemForUsername:kALUserToken
                                  andServiceName:kALDefaultServiceName
                                     accessGroup:nil
                                           error:nil];
        [SFHFKeychainUtils deleteItemForUsername:kALAuthTime
                                  andServiceName:kALDefaultServiceName
                                     accessGroup:nil
                                           error:nil];
    }
    else {
        [SFHFKeychainUtils storeUsername:kALUserToken
                             andPassword:token
                          forServiceName:kALDefaultServiceName
                             accessGroup:nil
                          updateExisting:YES
                                   error:nil];
        
        [SFHFKeychainUtils storeUsername:kALAuthTime
                             andPassword:authTime
                          forServiceName:kALDefaultServiceName
                             accessGroup:nil
                          updateExisting:YES
                                   error:nil];
    }
    
    
    _authToken = token;
    _authTime = authTime;
}

+(NSString *)userToken
{
    if (!_authToken) {
        _authToken = [SFHFKeychainUtils getPasswordForUsername:kALUserToken andServiceName:kALDefaultServiceName accessGroup:nil error:nil];
    }
    if (_authToken != nil) {
        return _authToken;
    }
    ALLog(@"token 為空");
    return @"";
}

+(NSString *)authTime
{
    if (!_authTime) {
        _authTime = [SFHFKeychainUtils getPasswordForUsername:kALAuthTime andServiceName:kALDefaultServiceName accessGroup:nil error:nil];
    }
    if (_authTime != nil) {
        return _authTime;
    }
    return @"";
}

+ (BOOL)isThirdPartyLogin
{
    return [[self password] isEqualToString:kThirdPartyLoginPassword];
}

- (void)setUserLanguage:(NSString *)userLanguage
{
    [[NSUserDefaults standardUserDefaults] setObject:userLanguage forKey:kPDSAccountManagerUserLanguageKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [ALLanguageCenter autoConnectLocalizableString];
}

- (NSString *)userLanguage
{
    NSString *userLanguage =
    [[NSUserDefaults standardUserDefaults] objectForKey:kPDSAccountManagerUserLanguageKey];
    
    return userLanguage;
}

#pragma mark - Account/User Stripe
- (ALStripeInfo *)stripeInfo
{
    if (_stripeInfo == nil) {
        _stripeInfo = [[ALStripeInfo alloc] init];
    }
    return _stripeInfo;
}

#pragma mark - Account/User Login/Logout
+ (void)loginAccount:(NSString *)account
            Password:(NSString *)password
     CompletionBlock:(void (^)(NSDictionary *responseObject))completionBlock
{
    if (![account isKindOfClass:[NSString class]] && [[PDSAccountManager userName] length]) account = [PDSAccountManager userName];
    if (![password isKindOfClass:[NSString class]] && [[PDSAccountManager password] length]) password = [PDSAccountManager password];
    
    if (![account length] || ![password length]) {
        return;
    }
    [self addMBProgressHUDWithKey:@"loginA002Account"];
    [ALNetWorkAPI loginA002Account:account
                          password:password
                   CompletionBlock:^(NSDictionary *responseObject) {
                       [self removeMBProgressHUDWithKey:@"loginA002Account"];
                       
                       NSString *token = nil;
                       NSString *authTime = nil;
                       if ([ALNetWorkAPI checkSerialNumber:@"A002"
                                            ResponseObject:responseObject]) {
                           token = responseObject[@"resBody"][@"UserToken"];
                           authTime = responseObject[@"resBody"][@"AuthTime"];
                       }
                       [PDSAccountManager saveAccessToken:token andAuthTime:authTime];
                       
                       if ([token length] && [authTime length]) {
                           [PDSAccountManager saveUserName:account andPassword:password];
                           
                           //更新使用者資訊
                           [[ALAppDelegate sharedAppDelegate] userInfomationRenewWithLoginAccount002:responseObject[@"resBody"]];
                           
                           [PDSAccountManager afterLoginAction];
                       }
                       completionBlock(responseObject);
                   }];
}

+ (void)loginWithSocialAccount:(NSDictionary *)accountInfo country:(NSString *)countryCode completionBlock:(void (^)(NSDictionary *))completionBlock
{
    [self addMBProgressHUDWithKey:@"loginA123Account"];
    
    if ([accountInfo[@"type"] isEqualToString:@"facebook"]) {
        
        NSString *pictureURLString = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", accountInfo[@"id"]];
        NSString *displayName = accountInfo[@"name"];
        NSArray *names = [displayName componentsSeparatedByString:@" "];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithDictionary:@{
                                   @"avatar": pictureURLString,
                                   @"userID": [NSString stringWithFormat:@"fb@%@",  accountInfo[@"id"]],
                                   @"email": [accountInfo[@"email"] description],
                                   @"displayName": displayName,
                                   @"type": accountInfo[@"type"],
                                   @"token": accountInfo[@"fbAccessToken"]
                                   }];
        if (names.count > 1) {
            [parameters setObject:[names firstObject] forKey:@"firstName"];
            [parameters setObject:[names lastObject] forKey:@"lastName"];
        }
        
        [ALNetWorkAPI loginA123AccountType:accountInfo[@"type"] withAccountInfo:[parameters copy] country:countryCode completionBlock:^(NSDictionary *responseObject) {
            
            [self removeMBProgressHUDWithKey:@"loginA123Account"];
            
            NSString *token = nil;
            NSString *authTime = nil;
            if ([ALNetWorkAPI checkSerialNumber:@"A123"
                                 ResponseObject:responseObject]) {
                token = responseObject[@"resBody"][@"UserToken"];
                authTime = responseObject[@"resBody"][@"AuthTime"];
            }
            
            [PDSAccountManager saveAccessToken:token andAuthTime:authTime];
            [PDSAccountManager saveUserName:parameters[@"userID"] andPassword:kThirdPartyLoginPassword];
            if ([token length] && [authTime length]) {
                //更新使用者資訊
                NSMutableDictionary *responseBody = [responseObject[@"resBody"] mutableCopy];
                responseBody[@"FirstName"] = parameters[@"firstName"];
                responseBody[@"LastName"] = parameters[@"lastName"];
                responseBody[@"HeadImage"] = pictureURLString;
                responseBody[@"UserID"] = parameters[@"userID"];
                responseBody[@"email"] = accountInfo[@"email"];
                [[ALAppDelegate sharedAppDelegate] userInfomationRenewWithLoginAccount002:responseBody];
                
                [PDSAccountManager afterLoginAction];
                
                completionBlock(responseObject);
            }
            /*
            else if ([responseObject[@"resBody"][@"ResultCode"] isEqualToString:@"ERR_ACCOUNT_ALREADY_IN_USE"]) {
                
                NSDictionary *parameters = @{
                                             @"userID": [NSString stringWithFormat:@"fb@%@",  accountInfo[@"id"]],
                                             @"type": accountInfo[@"type"],
                                             @"token": accountInfo[@"fbAccessToken"]
                                             };
                
                [ALNetWorkAPI getTokenA124AccountType:accountInfo[@"type"] withAccountInfo:parameters completionBlock:^(NSDictionary *responseObject) {
                    [self removeMBProgressHUDWithKey:@"loginA123Account"];

                    NSString *token = nil;
                    NSString *authTime = nil;
                    if ([ALNetWorkAPI checkSerialNumber:@"A124"
                                         ResponseObject:responseObject]) {
                        token = responseObject[@"resBody"][@"UserToken"];
                        authTime = responseObject[@"resBody"][@"AuthTime"];
                    }
                    
                    [PDSAccountManager saveAccessToken:token andAuthTime:authTime];
                    
                    if ([token length] && [authTime length]) {                        
                        // TODO: 更新使用者資訊
//                        [[ALAppDelegate sharedAppDelegate] userInfomationRenewWithLoginAccount002:responseObject[@"resBody"]];
                        [PDSAccountManager afterLoginAction];
                        
                    }
                    completionBlock(responseObject);
                }];
            }
             */
        }];
    }
    else if ([accountInfo[@"type"] isEqualToString:@"google"]) {
        [ALNetWorkAPI loginA123AccountType:accountInfo[@"type"] withAccountInfo:accountInfo country:countryCode completionBlock:^(NSDictionary *responseObject) {
            [self removeMBProgressHUDWithKey:@"loginA123Account"];
            
            NSString *token = nil;
            NSString *authTime = nil;
            if ([ALNetWorkAPI checkSerialNumber:@"A123"
                                 ResponseObject:responseObject]) {
                token = responseObject[@"resBody"][@"UserToken"];
                authTime = responseObject[@"resBody"][@"AuthTime"];
            }
            
            [PDSAccountManager saveAccessToken:token andAuthTime:authTime];
            [PDSAccountManager saveUserName:accountInfo[@"userID"] andPassword:kThirdPartyLoginPassword];
            if ([token length] && [authTime length]) {
                //更新使用者資訊
                NSMutableDictionary *responseBody = [responseObject[@"resBody"] mutableCopy];
                responseBody[@"FirstName"] = accountInfo[@"firstName"];
                responseBody[@"LastName"] = accountInfo[@"lastName"];
                responseBody[@"HeadImage"] = accountInfo[@"avatar"];
                responseBody[@"UserID"] = accountInfo[@"userID"];
                
                [[ALAppDelegate sharedAppDelegate] userInfomationRenewWithLoginAccount002:responseBody];
                
                [PDSAccountManager afterLoginAction];
                
            }            
            completionBlock(responseObject);
        }];
    }
}

+ (void)afterLoginAction
{
    //LeanCloud
    [[PDSAccountManager sharedManager] loginLeanCloud];
    
    //Stripe
    [[PDSAccountManager sharedManager].stripeInfo detectStripeStatusCompletionBlock:^{
        ALLog(@"Stripe 偵測完結 %ld",(long)[PDSAccountManager sharedManager].stripeInfo.stripeStatus);
    }];
    
    //CreditCard
    [[PDSAccountManager sharedManager].stripeInfo detectCreditCardStatusCompletionBlock:^{
        ALLog(@"CreditCard 偵測完結 %ld",(long)[PDSAccountManager sharedManager].stripeInfo.creditCardStatus);
    }];
    
    //Mixpanel
    [[Mixpanel sharedInstance] createAlias:[ALAppDelegate sharedAppDelegate].userID
                             forDistinctID:[Mixpanel sharedInstance].distinctId];
    [[Mixpanel sharedInstance] identify:[ALAppDelegate sharedAppDelegate].userID];
    ALLog(@"Mixpanel ID %@",[ALAppDelegate sharedAppDelegate].userID);
    
    // intercom
    NSString *email = [[ALAppDelegate sharedAppDelegate] userInfomation][@"email"];
    if ([email length]) {
        [Intercom registerUserWithUserId:[ALAppDelegate sharedAppDelegate].userID email:email];
    }
    else {
        [Intercom registerUserWithUserId:[ALAppDelegate sharedAppDelegate].userID];
    }

    //個人資料
    [ALNetWorkAPI personalProfile020CompletionBlock:^(NSDictionary *responseObject) {
        if ([ALNetWorkAPI checkSerialNumber:@"A020"
                             ResponseObject:responseObject]) {
//            [PDSAccountManager sharedManager].profileDictionary = responseObject[@"resBody"];
            NSMutableDictionary *dict = [[ALAppDelegate sharedAppDelegate] userInfomation];
            for (NSString *key in [dict allKeys]) {
                if (responseObject[@"resBody"][key]) {
                    dict[key] = responseObject[@"resBody"][key];
                }
            }
        }
    }];
    
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerForRemoteNotifications)]) {
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:
         UIUserNotificationTypeBadge |
         UIUserNotificationTypeSound |
         UIUserNotificationTypeAlert categories:nil];
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
    else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         UIRemoteNotificationTypeBadge |
         UIRemoteNotificationTypeAlert |
         UIRemoteNotificationTypeSound];
    }
}

+ (BOOL)isLoggedIn
{
    return ([[self userToken] length] && [[self authTime] length]);
}

- (void)logout
{
    [PDSAccountManager saveUserName:nil andPassword:nil];
    [PDSAccountManager saveAccessToken:nil andAuthTime:nil];
    
    //Stripe
    self.stripeInfo = nil;
    [ALAppDelegate sharedAppDelegate].userInfomation = nil;
    
    //LeanCloud
    [self logoutLeanCloud];
    
    // intercom
    [Intercom reset];

    // self.profileDictionary = nil;
    // Facebook
    [[FBSDKLoginManager new] logOut];
}

-(NSDictionary *)profileDictionary
{
    return [[ALAppDelegate sharedAppDelegate] userInfomation];
}

@end
