//
//  PDSAccountManager+LeanCloud.m
//  alorth
//
//  Created by w91379137 on 2016/1/5.
//  Copyright © 2016年 w91379137. All rights reserved.
//

#import "PDSAccountManager+LeanCloud.h"

@implementation PDSAccountManager (LeanCloud)

- (void)loginLeanCloud
{
    if (!self.imClient) {
        self.imClient = [[AVIMClient alloc] init];
        self.imClient.delegate = self;
    }
    if (self.imClientID && ![self.imClientID isEqualToString:[ALAppDelegate sharedAppDelegate].userID]) {
        [self.imClient closeWithCallback:^(BOOL succeeded, NSError *error) {
            if (succeeded) {
                self.imClientID = nil;
            }
        }];
        
    }
    [self.imClient openWithClientId:[ALAppDelegate sharedAppDelegate].userID callback:^(BOOL succeeded, NSError *error) {
        if (!succeeded) {
            self.imClientID = nil;
            return;
        }
        AVIMConversationQuery *query = [self.imClient conversationQuery];
        query.cacheMaxAge = 1;
        [query whereKey:@"m" containsString:[ALAppDelegate sharedAppDelegate].userID];
        [query findConversationsWithCallback:^(NSArray *objects, NSError *error) {
            if (error) {
                self.imClientID = nil;
                [self.imClient closeWithCallback:^(BOOL succeeded, NSError *error) {
                    
                }];
            }
            self.imClientID = [ALAppDelegate sharedAppDelegate].userID;
            NSMutableArray *array = [objects mutableCopy];
            for (AVIMConversation *conversation in [array copy]) {
                if ([conversation.members count] < 2 || ![conversation.members containsObject:[ALAppDelegate sharedAppDelegate].userID]) {
                    [array removeObject:conversation];
                }
            }
            self.conversationArray = array;
            [[NSNotificationCenter defaultCenter] postNotificationName:kPDSIMDidLoadConversationList object:nil];
        }];
        
    }];
}

- (void)logoutLeanCloud
{
    self.imClientID = nil;
    self.imClient.delegate = nil;
    self.conversationArray = nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:kPDSIMDidLoadConversationList object:nil];
    [self.imClient closeWithCallback:^(BOOL succeeded, NSError *error) {}];
    self.imClient = nil;
}

#pragma mark - LeanCloud
- (void)imClientPaused:(AVIMClient *)imClient error:(NSError *)error
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kPDSIMDidDisconnect object:imClient];
}

- (void)imClientResumed:(AVIMClient *)imClient
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kPDSIMDidReconnect object:imClient];
}

- (void)conversation:(AVIMConversation *)conversation didReceiveTypedMessage:(AVIMTypedMessage *)message
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kPDSIMDidReceiveIM object:@{@"conversation":conversation,@"message":message}];
}

- (void)conversation:(AVIMConversation *)conversation messageDelivered:(AVIMMessage *)message
{
    NSLog(@"message delivered:%@",message.content);
}

- (void)conversation:(AVIMConversation *)conversation membersRemoved:(NSArray *)clientIds byClientId:(NSString *)clientId
{
    NSLog(@"conversation membersRemoved:%@ byClientId:%@",clientIds,clientId);
}

@end
