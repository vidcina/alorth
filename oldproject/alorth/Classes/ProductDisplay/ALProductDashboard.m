//
//  ALProductView.m
//  alorth
//
//  Created by w91379137 on 2015/7/17.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALProductDashboard.h"

@implementation ALProductDashboard
#pragma mark - setter
-(void)setInfoDict:(NSMutableDictionary *)infoDict
{
    _infoDict = [infoDict mutableCopy];
    
    self.stockTextField.layer.borderWidth = 1.0;
    self.stockTextField.layer.borderColor = COMMON_GRAY3_COLOR.CGColor;
    
    if ([self currency].length > 0) {
        self.stockLabel.localText = [NSString stringWithFormat:@"$Stock$ (%@)",
                                     [self currency]];
        self.priceLabel.localText = [NSString stringWithFormat:@"$Price$ (%@)",
                                     [self currency]];
    }
    else {
        self.stockLabel.localText = @"Stock";
        self.priceLabel.localText = @"Price";
    }
    
    [self performData];
}

#pragma mark - 類型選擇
-(NSString *)selectedIsUsed
{
    NSString *used = _usedSelectTypeScrollView.infoObjectArray[_usedSelectTypeScrollView.didSelectIndex];
    NSString *selectedIsUsedx = nil;
    if ([used isEqualToString:@"New"]) {
        selectedIsUsedx = @"N";
    }
    if ([used isEqualToString:@"Used"]) {
        selectedIsUsedx = @"Y";
    }
    return selectedIsUsedx;
}

-(NSString *)selectedSizeString
{
    return _sizeSelectTypeScrollView.infoObjectArray[_sizeSelectTypeScrollView.didSelectIndex];
}

#pragma mark - ALSelectTypeScrollViewDelegate
-(UIView *)typeViewOfIndex:(NSInteger)index
                infoObject:(NSObject *)infoObject
      selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    if ([infoObject isKindOfClass:[NSString class]]) {
        
        UIButton *aButton =
        [ALSelectTypeScrollView alSelectButton:(NSString *)infoObject];
        
        [aButton addTarget:selectTypeScrollView
                    action:@selector(selectView:)
          forControlEvents:UIControlEventTouchUpInside];
        
        if (selectTypeScrollView == _usedSelectTypeScrollView) {
            [_usedSelectTypeScrollView addSubview:aButton]; //不先加上去 無法執行下一步
            [aButton mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(_usedSelectTypeScrollView.mas_width).multipliedBy(0.5).offset(-1);
            }];
        }
        else {
            CGSize titleSize =
            [ALSelectTypeScrollView alSelectButtonSize:(NSString *)infoObject];
            
            [aButton mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(@(titleSize.width + 40));
            }];
        }
        return aButton;
    }
    else {
        return [[UIView alloc] initWithFrame:CGRectZero];
    }
}

-(void)didSelectIndex:(NSInteger)index
           infoObject:(NSObject *)infoObject
 selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    [self.selectDelegate didSelectUsed:self.selectedIsUsed Size:self.selectedSizeString sender:self];
}

-(void)adjustSelectView:(UIView *)view
             infoObject:(NSObject *)infoObject
               selected:(BOOL)selected
   selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    [ALSelectTypeScrollView alSelectButton:(UIButton *)view Select:selected];
}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (_infoDict == nil) {
        [self.selectDelegate errorMag:@"Please select size" sender:self]; //有預設應該用不到
        return NO;
    }
    return [self.selectDelegate textFieldShouldBeginEditing:textField];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return [self.selectDelegate textFieldShouldReturn:textField];;
}

#pragma mark - IBAction_價格數目
-(IBAction)addStockAction:(UIButton *)sender
{
    if (_infoDict == nil) {
        [self.selectDelegate errorMag:@"Please select size"  sender:self]; //有預設應該用不到
        return;
    }
    int stock = [_stockTextField.text intValue];
    _stockTextField.text = [NSString stringWithFormat:@"%d",stock + 1];
    [self writeChange];
}

-(IBAction)minusStockAction:(UIButton *)sender
{
    if (_infoDict == nil) {
        [self.selectDelegate errorMag:@"Please select size" sender:self]; //有預設應該用不到
        return;
    }
    int stock = [_stockTextField.text intValue];
    if (stock > 0) {
        _stockTextField.text = [NSString stringWithFormat:@"%d",stock - 1];
    }
    [self writeChange];
}

-(IBAction)writeChange
{
    if (self.infoDict == nil) {
        [self.selectDelegate errorMag:@"Please select size" sender:self]; //有預設應該用不到
        return;
    }
    
    int stockIntValue = [self.stockTextField.text intValue];
    if (![[NSString stringWithFormat:@"%d",stockIntValue] isEqualToString:self.stockTextField.text]) {
        // 不正常的存貨量，嘗試修正看看，不行就改零
        self.stockTextField.text = [NSString stringWithFormat:@"%d",stockIntValue];
    }
    
    BOOL isFormatChange = NO;
    
    NSString *priceString =
    [self.priceTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    double priceDoubleValue = [priceString doubleValue];
    if (priceDoubleValue <= 0 && stockIntValue != 0) {
        //不正常的價格，存貨量又不等於零，不能賣
        priceDoubleValue = 0.00;
        
        NSString *test = [self.priceTextField.text copy];
        test = [test stringByReplacingOccurrencesOfString:@"0" withString:@""];
        test = [test stringByReplacingOccurrencesOfString:@"." withString:@""];
        if (test.length > 0) {
           isFormatChange = YES;
        }
    }
    
    NSArray *priceComponents = [self.priceTextField.text componentsSeparatedByString:@"."];
    if ([priceComponents count] > 1 &&
        [priceComponents[1] length] > 2) {
        // 小數位大於兩位
        isFormatChange = YES;
    }
    if (priceString.length > 1 &&
        [priceString hasPrefix:@"0"] &&
        ![priceString hasPrefix:@"0."]) {
        //有多餘的0
        priceString = [priceString substringFromIndex:1];
        self.priceTextField.text = priceString;
    }

    self.infoDict[apiKeyIsUsed] = self.selectedIsUsed;
    self.infoDict[apiKeySize] = self.selectedSizeString;
    self.infoDict[@"Amount"] = [NSString stringWithFormat:@"%d",stockIntValue];
    self.infoDict[@"Price"] = [NSString stringWithFormat:@"%.2f",priceDoubleValue];
    
    if (isFormatChange) {
        [self performData];
    }
    
    [self.selectDelegate dataDidChange:_infoDict sender:self];
}

-(void)performData
{
    self.stockTextField.text = [self.infoDict[@"Amount"] description];
    self.priceTextField.text = [self.infoDict[@"Price"] description];
}

-(NSString *)currency
{
    NSString *currency = @"";
    if ([self.infoDict[@"Currency"] isKindOfClass:[NSString class]]) {
        currency = self.infoDict[@"Currency"];
    }
    return currency;
}

#pragma mark - IBAction_步驟
-(IBAction)removeAction:(UIButton *)sender
{
    if ([self.selectDelegate respondsToSelector:@selector(productDashboardRemoveAction:)]) {
        [self.selectDelegate productDashboardRemoveAction:self];
    }
}

-(IBAction)saveAction:(UIButton *)sender
{
    if ([self.selectDelegate respondsToSelector:@selector(productDashboardSaveAction:)]) {
        [self.selectDelegate productDashboardSaveAction:self];
    }
}

@end
