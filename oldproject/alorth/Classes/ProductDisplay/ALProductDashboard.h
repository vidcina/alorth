//
//  ALProductView.h
//  alorth
//
//  Created by w91379137 on 2015/7/17.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "PDSIBDesignABLEView.h"

@class ALProductDashboard;

@protocol ALProductDashboardSelectDelegate <NSObject>

//告知vc 請vc給予此種類的 價格 以及數量
-(void)didSelectUsed:(NSString *)used Size:(NSString *)size sender:(ALProductDashboard *)sender;

//告知vc 資料已經改變
-(void)dataDidChange:(NSDictionary *)infoDict sender:(ALProductDashboard *)sender;

//告知vc 發生錯誤
-(void)errorMag:(NSString *)errorMag sender:(ALProductDashboard *)sender;

@optional

//告知vc
-(void)productDashboardRemoveAction:(ALProductDashboard *)sender;

//告知vc
-(void)productDashboardSaveAction:(ALProductDashboard *)sender;

@end

@interface ALProductDashboard : PDSIBDesignABLEView
<ALSelectTypeScrollViewDelegate, UITextFieldDelegate>

@property (weak) IBOutlet NSObject <ALProductDashboardSelectDelegate,UITextFieldDelegate> *selectDelegate;
-(void)writeChange;

//這次調整 ProductID
@property (nonatomic, strong) NSString *productID;

//這次調整的某個 stock 的資訊
@property (nonatomic, strong) NSMutableDictionary *infoDict;

#pragma mark - 資料
@property (nonatomic, strong) IBOutlet UIImageView *productImageView;
@property (nonatomic, strong) IBOutlet UILabel *productNameLabel;

#pragma mark - 類型選擇
@property (nonatomic, strong) IBOutlet ALSelectTypeScrollView *usedSelectTypeScrollView;
@property (nonatomic, readonly) NSString *selectedIsUsed;

@property (nonatomic, strong) IBOutlet ALSelectTypeScrollView *sizeSelectTypeScrollView;
@property (nonatomic, readonly) NSString *selectedSizeString;

#pragma mark - 價格數目
@property (nonatomic, strong) IBOutlet UIView *priceStockView;

@property (nonatomic, strong) IBOutlet UILabel *stockLabel;
@property (nonatomic, strong) IBOutlet UIButton *addButton;
@property (nonatomic, strong) IBOutlet UIButton *minusButton;
@property (nonatomic, strong) IBOutlet UITextField *stockTextField;

@property (nonatomic, strong) IBOutlet UILabel *priceLabel;
@property (nonatomic, strong) IBOutlet UITextField *priceTextField;

#pragma mark - 步驟區
@property (nonatomic, strong) IBOutlet UIView *stepButtonView;

@end
