//
//  ALPlaceOrderViewController.m
//  alorth
//
//  Created by John Hsu on 2015/7/1.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALPlaceOrderViewController.h"
#import "ALBoughtOrderViewController.h"

#import "ALProfileModifyViewController.h"
#import "UIViewController+ALProfile.h"

@interface ALPlaceOrderViewController ()
<ALProfileModifyViewControllerDelegate, ALOnlinePaymentViewControllerDelegate>

@end

@implementation ALPlaceOrderViewController

#pragma mark - init
- (instancetype)initWithProductID:(NSString *)aProductID stockInfo:(NSDictionary *)aStockInfo;
{
    self = [super init];
    if (self) {
        productID = aProductID;
        stockInfo = aStockInfo;
    }
    return self;
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.mainContainerView.superview == nil) {
        [self.mainScrollView masSetContentView:self.mainContainerView isVerticalFix:NO];
        
        NSDictionary *logDictionary = [self logDictionary];
        
        ALEcommerceProduct *product = [[ALEcommerceProduct alloc] init];
        
        product.productID = logDictionary.safe[@"ProductID"];
        product.productTitle = self.productTitle;
        
        product.price = [logDictionary.safe[@"Price"] floatValue];
        product.currency = logDictionary.safe[@"Currency"];
        
        [[ALBuyProcessInspectorUnit logProductBuy:ALBuyStepOrderShow
                                          Product:product
                                          DevDict:logDictionary]sendInspector];
        
        if (self.onlinePaymentVC == nil) {
            self.onlinePaymentVC = [[ALOnlinePaymentViewController alloc] init];
            self.onlinePaymentVC.delegate = self;
        }
        
        
    }
    
    if (self.receiveAddressLabel.userInteractionEnabled != YES) {
        UITapGestureRecognizer *tap =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(modifyAddress)];
        self.receiveAddressLabel.userInteractionEnabled = YES;
        [self.receiveAddressLabel addGestureRecognizer:tap];
    }
    
    [self.productImageView imageFromURLString:self.productImageURL];
    
    self.productTitleLabel.text = self.productTitle;
    self.productTitleLabel.numberOfLines = 0;

    double productFeeValue = [stockInfo[@"Price"] doubleValue];
    self.productPriceLabel.text = [NSString stringWithFormat:@"%.2f %@",
                                   productFeeValue,
                                   stockInfo[@"Currency"]];
    
    self.productAmountLabel.text = [NSString stringWithFormat:@"%@ %d",
                                    ALLocalizedString(@"Stock", nil) ,
                                    [stockInfo[@"Amount"] intValue]] ;
    
    if (apiDataDict == nil) {
        [ALNetWorkAPI personalProfile020CompletionBlock:^(NSDictionary *responseObject) {
            NSDictionary *dict = responseObject[@"resBody"];
            if (dict && dict[@"ResultCode"] && [dict[@"ResultCode"] intValue] == 0) {
                
                apiDataDict = [dict mutableCopy];
                [apiDataDict removeObjectForKey:@"ResultCode"];
                
                self.receiveAddressLabel.text = apiDataDict[@"Address"];
            }
        }];
    }
    else {
        self.receiveAddressLabel.text = apiDataDict[@"Address"];
    }
    
    self.stockTextField.layer.borderWidth = 1.0;
    self.stockTextField.layer.borderColor = COMMON_GRAY3_COLOR.CGColor;
    
    if (self.selectScrollView.infoObjectArray == nil) {
        self.selectScrollView.infoObjectArray = [@[@(PlaceOrderCreditCard),@(PlaceOrderSelf)] mutableCopy];
        [self.selectScrollView selectIndex:0];
    }
    
    double transFeeValue = [stockInfo[@"TransPrice"] doubleValue];
    self.transFeeLabel.text = [NSString stringWithFormat:@"%.2f %@",
                               transFeeValue,
                               stockInfo[@"Currency"]];
    
    //價格初始化
    [self writeChange];
}

- (NSMutableDictionary *)logDictionary
{
    NSMutableDictionary *logDictionary = [stockInfo mutableCopy];
    logDictionary[@"ProductID"] = productID;
    return logDictionary;
}

#pragma mark - ALSelectTypeScrollViewDelegate
-(UIView *)typeViewOfIndex:(NSInteger)index
                infoObject:(NSObject *)infoObject
      selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    NSNumber *number = (NSNumber *)infoObject;
    ALPlaceOrderStatus status = [number integerValue];
    
    NSString *title = @"";
    switch (status) {
        case PlaceOrderCreditCard:
        {
            title = @"Online payment";
        }
            break;
            
        case PlaceOrderSelf:
        {
            title = @"Transfer by myself";
        }
            break;
            
        default:
            break;
    }
    
    UIButton *aButton =
    [ALSelectTypeScrollView alSelectButton:title];
    
    [aButton addTarget:selectTypeScrollView
                action:@selector(selectView:)
      forControlEvents:UIControlEventTouchUpInside];
    
    [selectTypeScrollView addSubview:aButton]; //不先加上去 無法執行下一步
    [aButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(selectTypeScrollView.mas_width).multipliedBy(0.5).offset(-1);
    }];
    
    return aButton;
}

-(void)didSelectIndex:(NSInteger)index
           infoObject:(NSObject *)infoObject
 selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    NSNumber *number = (NSNumber *)infoObject;
    self.status = [number integerValue];
}

- (void)adjustSelectView:(UIView *)view
              infoObject:(NSObject *)infoObject
                selected:(BOOL)selected
    selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    UIButton *button = (UIButton *)view;
    if (selected) {
        //選到
        button.selected = YES;
        button.backgroundColor = kALProductDetailDidselectBgColor;
        [button setTitleColor:kALProductDetailDidselectFontColor forState:UIControlStateNormal];
    }
    else {
        //沒
        button.selected = NO;
        button.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5];
        [button setTitleColor:kALProductDetailUnselectFontColor forState:UIControlStateNormal];
    }
}

#pragma mark - Setter / Getter
- (void)setStatus:(ALPlaceOrderStatus)status
{
    _status = status;
    
    NSArray *views = @[self.dealMyselfButton,self.onlinePaymentVC.view];
    
    for (UIView *view in views) {
        if (view.superview == nil) {
            view.hidden = YES;
            
            [self.view addSubview:view];
            [view mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.view.mas_bottom).offset(-2);
                make.leading.equalTo(self.view.mas_leading).offset(2);
                make.trailing.equalTo(self.view.mas_trailing).offset(-2);
                make.height.equalTo(@50);
            }];
        }
    }
    
    self.dealMyselfButton.hidden = !(status == PlaceOrderSelf);
    self.onlinePaymentVC.view.hidden = !(status == PlaceOrderCreditCard);
}

#pragma mark - IBAction
- (void)modifyAddress
{
    if (apiDataDict != nil) {
        UIViewController *next = [self modifyViewControllerWithKey:ALProfileModifyAddress];
        [self.navigationController pushViewController:next animated:YES];
    }
}

- (IBAction)addStockAction:(UIButton *)sender
{
    [self.stockTextField resignFirstResponder];
    int stock = [self.stockTextField.text intValue];
    self.stockTextField.text = [NSString stringWithFormat:@"%d",stock + 1];
    [self writeChange];
}

- (IBAction)minusStockAction:(UIButton *)sender
{
    [self.stockTextField resignFirstResponder];
    int stock = [self.stockTextField.text intValue];
    if (stock > 0) {
        self.stockTextField.text = [NSString stringWithFormat:@"%d",stock - 1];
    }
    [self writeChange];
}

- (IBAction)sendOrderAction:(UIButton *)sender
{
    if (sender == self.dealMyselfButton) {
        payType = ALDealMySelfPayType;
    }
    else {
        payType = ALCreditCardPayType;
    }
    NSLog(@"類別 %@",payType);
    
    int buyAmount = [_stockTextField.text intValue];
    if (buyAmount <= 0) {
        YKSimpleAlert(ALLocalizedString(@"Please enter a valid quantity",nil));
        return;
    }
    
    if (self.receiveAddressLabel.text.length == 0) {
        [YKBlockAlert alertWithTitle:ALLocalizedString(@"Please enter an address",nil)
                             message:ALLocalizedString(@"",nil)
                    blocksAndButtons:^{
                        [self modifyAddress];
                    },ALLocalizedString(@"OK",nil), nil];
        return;
    }
    
    //    if (buyAmount > [stockInfo[@"Amount"] intValue]) {
    //        YKSimpleAlert(ALLocalizedString(@"Please enter a valid quantity",nil));
    //        return;
    //    }
    
    dispatch_group_t orderStockA009Group = dispatch_group_create();
    
    __block NSString *orderID = nil;
    NSMutableDictionary *logDictionary = [self logDictionary];
    logDictionary[@"OrderAmount"] = [NSString stringWithFormat:@"%d",buyAmount];
    
    dispatch_group_enter(orderStockA009Group);
    [self addMBProgressHUDWithKey:@"orderStockA009"];
    [ALNetWorkAPI orderStockA009ProductId:productID
                                  StockID:stockInfo[@"StockID"]
                                 SellerID:stockInfo[@"SellerID"]
                              OrderAmount:[NSString stringWithFormat:@"%d",buyAmount]
                                  PayType:payType
                          CompletionBlock:^(NSDictionary *responseObject) {
                              [self removeMBProgressHUDWithKey:@"orderStockA009"];
       
                              if ([ALNetWorkAPI checkSerialNumber:@"A009"
                                                   ResponseObject:responseObject]) {
                                  orderID = responseObject.safe[@"resBody"][@"OrderID"];
                              }
                              else {
                                  YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A009", responseObject),nil));
                              }
                              dispatch_group_leave(orderStockA009Group);
    }];
    
    dispatch_group_notify(orderStockA009Group, dispatch_get_main_queue(), ^{
        
        void (^finishBlock)(NSString *orderID) = ^(NSString *orderID) {
            if (orderID) {
                
                ALBuyStep step = [payType isEqualToString:ALDealMySelfPayType] ? ALBuyStepBuyDealMySelf_Order : ALBuyStepBuyOnlinePayment_Purchase;
                
                ALEcommerceProduct *product = [[ALEcommerceProduct alloc] init];
                product.productID = logDictionary.safe[@"ProductID"];
                product.productTitle = self.productTitle;
                
                product.price = [logDictionary.safe[@"Price"] floatValue];
                product.currency = logDictionary.safe[@"Currency"];
                product.quantity = [logDictionary.safe[@"OrderAmount"] integerValue];
                
                [[ALBuyProcessInspectorUnit logProductBuy:step
                                                  Product:product
                                                  DevDict:logDictionary] sendInspector];
                
                //YKSimpleAlert(ALLocalizedString(@"已下單成功",nil));
                [YKBlockAlert alertWithTitle:ALLocalizedString(@"Success",nil)
                                     message:ALLocalizedString(@"",nil)
                            blocksAndButtons:^{
                                self.delegate.infoDetailDict = nil; //使得前一頁更新(雖然沒用 先刷掉)
                                ALNavigationViewController *nv = (ALNavigationViewController *)self.navigationController;
                                [nv clearOtherController:ALNavigationSubClear
                                                    push:ALNavigationTabBarSetting
                                           subController:@[[[ALBoughtOrderViewController alloc] init]]];
                            },ALLocalizedString(@"OK",nil), nil];
            }
        };
        
        if (payType == ALCreditCardPayType && orderID) {
            
            //嘗試直接結帳
            [self addMBProgressHUDWithKey:@"confirmOrderPriceA012O"];
            [ALNetWorkAPI confirmOrderPriceA012OrderId:orderID
                                               payType:payType
                                       transactionData:nil
                                       CompletionBlock:^(NSDictionary *responseObject) {
                                           [self removeMBProgressHUDWithKey:@"confirmOrderPriceA012O"];
                                           
                                           if ([ALNetWorkAPI checkSerialNumber:@"A012"
                                                                ResponseObject:responseObject]) {
                                               
                                           }
                                           else {
                                               ALLog(@"A012 error :%@",responseObject);
                                           }
                                           finishBlock(orderID);
                                       }];
        }
        else {
            finishBlock(orderID);
        }
    });
}

#pragma mark - ALOnlinePaymentViewControllerDelegate
- (void)totalPrice:(NSDecimalNumber **)decimalNumber currency:(NSString **)currency
{
    *decimalNumber = [NSDecimalNumber decimalNumberWithString:self.totalFeeLabel.text];
    *currency = stockInfo[@"Currency"];
}

- (void)onlinePayAction
{
    [self sendOrderAction:nil];
}

- (void)touchOnlinePaymentButtonLog:(ALOnlinePaymentViewControllerLogType)logType
{
    ALBuyStep step = ALBuyStepInit;
    
    if (logType == ALOnlinePaymentLogApplePay) step = ALBuyStepBuyOnlinePayment_ApplePay;
    if (logType == ALOnlinePaymentLogStripe) step = ALBuyStepBuyOnlinePayment_Stripe;
    if (logType == ALOnlinePaymentLogAuthorize) step = ALBuyStepBuyOnlinePayment_Authorize;
    if (logType == ALOnlinePaymentLogChangeCard) step = ALBuyStepBuyOnlinePayment_ChangeCard;
    //if (logType == ALOnlinePaymentLogCheckout) step = ALBuyStepBuyOnlinePayment_Purchase; call api 那邊有
    
    if (step != ALBuyStepInit) {
        [[ALBuyProcessInspectorUnit logProductBuy:step
                                          Product:nil
                                          DevDict:[self logDictionary]] sendInspector];
    }
}

#pragma mark - Data change
-(IBAction)textFieldChangeAction:(UITextField *)sender
{
    [self writeChange];
}

-(BOOL)writeChange
{
    int stock = [_stockTextField.text intValue];
    if (stock < 1) {
        stock = 1;
        _stockTextField.text = @"1";
    }
    if (stock > [stockInfo[@"Amount"] intValue]) {
        stock = [stockInfo[@"Amount"] intValue];
        _stockTextField.text = stockInfo[@"Amount"];
    }
    
    double productFeeValue = [stockInfo[@"Price"] doubleValue];
    double transFeeValue = [stockInfo[@"TransPrice"] doubleValue];
    double totalPrice = productFeeValue * stock + transFeeValue;
    
    self.totalFeeLabel.text = [NSString stringWithFormat:@"%.2f %@",
                               totalPrice,
                               stockInfo[@"Currency"]];
    return YES;
}

#pragma mark - ALProfileModifyViewControllerDelegate
-(void)savaButtonAction:(ALProfileModifyViewController *)profileModifyViewController
{
    if ([profileModifyViewController.modifykey isEqualToString:ALProfileModifyAddress]) {
        if ([profileModifyViewController.textView_1.text isKindOfClass:[NSString class]]) {
            [apiDataDict setObject:profileModifyViewController.textView_1.text forKey:@"Address"];
        }
        else {
            [apiDataDict setObject:@"" forKey:@"Address"];
        }
    }
    [self saveUserProfile:apiDataDict];
}

-(UIViewController *)modifyViewControllerWithKey:(NSString *)key
{
    ALProfileModifyViewController *next = [[ALProfileModifyViewController alloc] init];
    next.delegate = self;
    next.infoDict = apiDataDict;
    next.modifykey = key;
    return next;
}

#pragma mark - Keyboard Auto System
-(void)keyboardDidShow:(NSNotification *)notification
{
    float keyboardHeight =
    [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    keyboardHeight -= 70;
    
    [UIView animateWithDuration:0.15
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.view.transform =
                         CGAffineTransformTranslate(CGAffineTransformIdentity, 0, -keyboardHeight);
                     } completion:nil];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.15
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.view.transform = CGAffineTransformIdentity;
                     } completion:nil];
}

@end
