//
//  ALProductDetailViewController.h
//  alorth
//
//  Created by w91379137 on 2015/6/25.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicSubViewController.h"

#import "SKTagView.h"
#import "SKTag.h"

#define kALProductDetailDidselectBgColor COMMON_GRAY3_COLOR
#define kALProductDetailDidselectFontColor [UIColor whiteColor]

#define kALProductDetailUnselectBgColor [UIColor whiteColor]
#define kALProductDetailUnselectFontColor COMMON_GRAY3_COLOR


@interface ALProductDetailViewController : ALBasicSubViewController

-(instancetype)initWithInfoDict:(NSDictionary *)infoDict;

@property (nonatomic, strong) NSDictionary *infoDict;
@property (nonatomic, strong) NSDictionary *infoDetailDict;
@property (nonatomic, strong) IBOutlet UIScrollView *mainScrollView;
@property (nonatomic, strong) IBOutlet UIView *mainScrollViewContentView;

//圖樣名稱
@property (nonatomic, strong) IBOutlet UIView *productView;
@property (nonatomic, strong) ALProductTableViewCell *productCell;

//新舊樣式
@property (nonatomic, strong) IBOutlet ALSelectTypeScrollView *usedSelectTypeScrollView;
@property (nonatomic, readonly) NSString *selectedIsUsed;

@property (nonatomic, strong) IBOutlet ALSelectTypeScrollView *sizeSelectTypeScrollView;
@property (nonatomic, readonly) NSString *selectedSizeString;

//庫存數量
@property (nonatomic, strong) IBOutlet UIView *displayProductsView;
@property (nonatomic, strong) NSDictionary *selectStock;

//銷售按鈕
@property (nonatomic, strong) IBOutlet UIButton *sellButton;
@property (nonatomic, strong) IBOutlet UIButton *dataButton;
@property (nonatomic, strong) IBOutlet UIButton *trendButton;
@property (nonatomic, strong) IBOutlet UIButton *editButton;

//tag
@property (nonatomic, strong) IBOutlet SKTagView *tagView;


@end
