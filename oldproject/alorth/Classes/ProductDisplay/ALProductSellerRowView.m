//
//  ALProductRowView.m
//  alorth
//
//  Created by w91379137 on 2015/6/29.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALProductSellerRowView.h"

@implementation ALProductSellerRowView

#pragma mark - set
-(void)setRowData:(NSDictionary *)rowData
{
    _rowData = rowData;
    
    self.sellerName.text =
    [ALFormatter preferredDisplayNameForFullName:self.rowData[@"SellerName"] UserID:self.rowData[@"SellerID"]];
      
    for (UIView *view in self.sellerRate.subviews) {
        [view removeFromSuperview];
    }
    
    float rate = [rowData[@"SellerRate"] floatValue];
    [self.sellerRate rankStar:5 get:rate];

    self.location.text = rowData[@"Location"];
    self.priceLabel.text =
    [NSString stringWithFormat:@"%@ %@",rowData[@"Price"],rowData[@"Currency"]];
}

-(IBAction)didSelectProduct:(UIButton *)sender
{
    self.delegate.selectStock = self.rowData;
}

-(void)performSelectColor:(NSDictionary *)rowData
{
    if (rowData == self.rowData) {
        self.clickButton.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    }
    else {
        self.clickButton.backgroundColor = [UIColor clearColor];
    }
}

@end
