//
//  ALPlaceOrderViewController.h
//  alorth
//
//  Created by John Hsu on 2015/7/1.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALBasicViewController.h"
#import "ALProductDetailViewController.h"

typedef NS_OPTIONS(NSUInteger, ALPlaceOrderStatus) {
    PlaceOrderInit,
    PlaceOrderCreditCard,
    PlaceOrderSelf
};

@interface ALPlaceOrderViewController : ALBasicSubViewController
{
    NSString *productID;
    NSDictionary *stockInfo;
    
    int amount;
    NSString *payType;
    
    NSMutableDictionary *apiDataDict;
}
- (instancetype)initWithProductID:(NSString *)aProductID stockInfo:(NSDictionary *)aStockInfo;

@property (nonatomic,strong) ALProductDetailViewController *delegate;

@property (nonatomic,strong) IBOutlet UIScrollView *mainScrollView;
@property (nonatomic,strong) IBOutlet UIView *mainContainerView;

@property (nonatomic,strong) NSString *productTitle;
@property (nonatomic,strong) NSString *productImageURL;

@property (nonatomic, strong) IBOutlet UIImageView *productImageView;
@property (nonatomic, strong) IBOutlet UILabel *productTitleLabel;
@property (nonatomic, strong) IBOutlet UILabel *productPriceLabel;
@property (nonatomic, strong) IBOutlet UILabel *productAmountLabel;

@property (nonatomic, strong) IBOutlet UILabel *receiveAddressLabel;
@property (nonatomic, strong) IBOutlet UITextField *stockTextField;

//付款選擇
@property (nonatomic, strong) IBOutlet ALSelectTypeScrollView *selectScrollView;
@property (nonatomic) ALPlaceOrderStatus status;
@property (nonatomic, strong) IBOutlet UIButton *dealMyselfButton;
@property (nonatomic, strong) ALOnlinePaymentViewController *onlinePaymentVC;

//金額
@property (nonatomic, strong) IBOutlet UILabel *transFeeLabel;
@property (nonatomic, strong) IBOutlet UILabel *totalFeeLabel;

@end
