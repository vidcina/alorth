//
//  ALRankTableViewCell.h
//  alorth
//
//  Created by papayabird on 2015/5/26.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface ALProductTableViewCell : UITableViewCell
{
    AVPlayer *player;
    AVPlayerLayer *playerLayer;
}

+(ALProductTableViewCell *)cell;
+(float)settingCell:(ALProductTableViewCell *)cell
       WithInfoDict:(NSDictionary *)infoDict;

@property (strong, nonatomic) NSDictionary *infoDict;
@property (nonatomic) float productTableViewCellWidth;

@property (nonatomic) BOOL isPlay;
@property (nonatomic, strong) IBOutlet UIButton *isPlayButton;

@property (nonatomic) BOOL isVoice;
@property (nonatomic, strong) IBOutlet UIButton *isVoiceButton;

@property (strong, nonatomic) NSString *productID;
@property (strong, nonatomic) NSString *productClip;

@property (strong, nonatomic) IBOutlet UIView *videoView;
@property (strong, nonatomic) IBOutlet UIImageView *goodImageView;
@property (strong, nonatomic) IBOutlet UILabel *productTitleLabel;

#pragma mark - All Video Control
+(void)detectCenterCellToPlay:(UITableView *)tableView;
+(void)stopAllPlayCell:(UITableView *)tableView;

@end
