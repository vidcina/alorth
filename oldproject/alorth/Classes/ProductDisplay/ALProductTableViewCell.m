//
//  ALRankTableViewCell.m
//  alorth
//
//  Created by papayabird on 2015/5/26.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALProductTableViewCell.h"

@implementation ALProductTableViewCell

#pragma mark - Cell Life
+(ALProductTableViewCell *)cell
{
    ALProductTableViewCell *cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class])
                                                               owner:nil
                                                             options:nil] lastObject];
    cell.isVoice = NO;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    return cell;
}
    
#pragma mark - custom
+(float)settingCell:(ALProductTableViewCell *)cell
       WithInfoDict:(NSDictionary *)infoDict
{
    cell.infoDict = infoDict;
    
    float cellAccumulationHeight = [UIScreen mainScreen].bounds.size.width + 50;
    
    [cell.goodImageView imageFromURLString:infoDict.safe[@"ProductImage"]];
    
    NSString *clipURL = infoDict.safe[@"ProductClip"];
    cell.productClip = (clipURL.length > 0) ? clipURL : nil;
    
    cell.productTitleLabel.text = infoDict.safe[@"ProductTitle"];
    cell.productTitleLabel.numberOfLines = 2;
    
    return cellAccumulationHeight;
}

-(void)setIsPlay:(BOOL)isPlay
{
    [self willChangeValueForKey:NSStringFromSelector(@selector(isPlay))];
    if (isPlay == YES) {
        [self.isPlayButton setTitle:@"播" forState:UIControlStateNormal];
    }
    else {
        [self.isPlayButton setTitle:@"停" forState:UIControlStateNormal];
    }
    
    if (self.isPlay == isPlay) {
        return;
    }
    
    _isPlay = isPlay;
    if (self.isPlay == YES) {
        
        if ([[ALVideoDownloadManager sharedManager] isExistVideo:self.productClip]) {
            if (player == nil) {
                
                NSString *cachePath = [ALNetWorkAPI targetVideoFromURL:self.productClip];
                player = [AVPlayer playerWithURL:[NSURL fileURLWithPath:cachePath]];
                player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
                //ALLog(@"影片位置 : \n%@",cachePath);
                if (_isVoice) {
                    player.volume = 1;
                }
                else {
                    player.volume = 0;
                }
            }
            if (playerLayer == nil) {
                playerLayer = [AVPlayerLayer playerLayerWithPlayer:player];
                playerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
                playerLayer.needsDisplayOnBoundsChange = YES;
                float width = self.productTableViewCellWidth != 0 ?
                self.productTableViewCellWidth : [UIScreen mainScreen].bounds.size.width;
                playerLayer.frame = CGRectMake(0, 0, width, width);
                
                [self.videoView.layer addSublayer:playerLayer];
                
                //註冊結束事件
                [[NSNotificationCenter defaultCenter] addObserver:self
                                                         selector:@selector(playerItemDidReachEnd:)
                                                             name:AVPlayerItemDidPlayToEndTimeNotification
                                                           object:[player currentItem]];
            }
            [player play];
            self.goodImageView.hidden = YES;
        }
        else {
            self.goodImageView.hidden = NO;
            _isPlay = NO;
            [_isPlayButton setTitle:@"載" forState:UIControlStateNormal];
        }
    }
    else if (_isPlay == NO) {
        [player pause];
        //_goodImageView.hidden = NO;
    }
    [self didChangeValueForKey:NSStringFromSelector(@selector(isPlay))];
}

-(void)setIsVoice:(BOOL)isVoice
{
    if (isVoice == YES) {
        [self.isVoiceButton setImage:[UIImage imageNamed:@"soundOn.png"]
                            forState:UIControlStateNormal];
    }
    else {
        [self.isVoiceButton setImage:[UIImage imageNamed:@"soundOff.png"]
                            forState:UIControlStateNormal];
    }
    
    if (self.isVoice == isVoice) {
        return;
    }
    _isVoice = isVoice;
    if (player) {
        if (self.isVoice) {
            player.volume = 1;
        }
        else {
            player.volume = 0;
        }
    }
}

//結束事件
-(void)playerItemDidReachEnd:(NSNotification *)notification
{
    AVPlayerItem *aPlayerItem = [notification object];
    [aPlayerItem seekToTime:kCMTimeZero];
}

#pragma mark -
-(void)observeValueForKeyPath:(NSString *)keyPath
                     ofObject:(id)object change:(NSDictionary *)change
                      context:(void *)context
{
    if (object == player && [keyPath isEqualToString:@"status"]) {
        if (player.status == AVPlayerStatusReadyToPlay) {

        } else if (player.status == AVPlayerStatusFailed) {
            // something went wrong. player.error should contain some information
        }
    }
}

#pragma mark - IBAction
-(IBAction)switchButton:(UIButton *)sender
{
    self.isPlay = !_isPlay;
}

-(IBAction)switchVoiceButton:(UIButton *)sender
{
    self.isVoice = !_isVoice;
}

#pragma mark - All Video Control
+(void)detectCenterCellToPlay:(UITableView *)tableView
{
    float centerY = tableView.frame.size.height / 2 + tableView.contentOffset.y;
    
    UITableViewCell *theCenterCell;
    for(UITableViewCell *cell in tableView.visibleCells) {
        //NSLog(@"%@",NSStringFromCGPoint(CGPointMake(view.frame.origin.x, view.frame.origin.y)));
        //NSLog(@"%@",NSStringFromCGPoint(CGPointMake(scrollView.contentOffset.x, scrollView.contentOffset.y)));
        //NSLog(@"%@",NSStringFromCGPoint(CGPointMake(view.frame.origin.x, view.frame.origin.y)));
        
        if (theCenterCell != nil) {
            float theLast = fabs(theCenterCell.center.y - centerY);
            float theNext = fabs(cell.center.y - centerY);
            if (theLast > theNext) {
                theCenterCell = cell;
            }
        }
        else {
            theCenterCell = cell;
        }
    }
    
    for (UITableViewCell *cell in tableView.visibleCells) {
        if ([cell isKindOfClass:[ALProductTableViewCell class]]) {
            ALProductTableViewCell *cellx = (ALProductTableViewCell *)cell;
            if (cell == theCenterCell) {
                cellx.isPlay = YES;
            }
            else {
                cellx.isPlay = NO;
            }
        }
    }
}

+(void)stopAllPlayCell:(UITableView *)tableView
{
    for (ALProductTableViewCell *cell in tableView.visibleCells) {
        if ([cell isKindOfClass:[ALProductTableViewCell class]]) {
            ALProductTableViewCell *cellx = (ALProductTableViewCell *)cell;
            cellx.isPlay = NO;
        }
    }
}

@end
