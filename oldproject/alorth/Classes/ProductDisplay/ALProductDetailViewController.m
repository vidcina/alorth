//
//  ALProductDetailViewController.m
//  alorth
//
//  Created by w91379137 on 2015/6/25.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALProductDetailViewController.h"
#import "ALProductTableViewCell.h"

#import "ALProductSellerRowView.h"              //商品細目橫條
#import "ALPlaceOrderViewController.h"          //訂購商品

#import "ALPublishConfirmViewController.h"      //出售商品
//#import "ALWebViewController.h"               //舊版的不需要了
#import "ALProductEditHistoryViewController.h"  //產品資訊
#import "ALProductStatisticsViewController.h"   //銷量變化
#import "ALProductEditViewController.h"         //修改商品

#import "ALSearchV3ViewController.h"

@interface ALProductDetailViewController ()

@end

@implementation ALProductDetailViewController

#pragma mark - VC Life
-(instancetype)initWithInfoDict:(NSDictionary *)infoDict
{
    self = [super init];
    if (self) {
        _infoDict = infoDict;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    ALEcommerceProduct *product = [[ALEcommerceProduct alloc] init];
    product.productID = self.infoDict.safe[@"ProductID"];
    product.productTitle = self.infoDict.safe[@"ProductTitle"];
    
    [[ALBuyProcessInspectorUnit logProductBuy:ALBuyStepDetailPage
                                      Product:product
                                      DevDict:self.infoDict] sendInspector];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    ALLog(@"商品資訊infoDict %@",self.infoDict);
    
    [_mainScrollView masSetContentView:_mainScrollViewContentView isVerticalFix:NO];
    
    if (self.productCell == nil) {
        self.productCell = [ALProductTableViewCell cell];
        
        [self.productView addSubview:self.productCell];
        [self.productCell mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.productView);
            make.height.equalTo(@([ALProductTableViewCell settingCell:nil WithInfoDict:self.infoDict]));
        }];
        [ALProductTableViewCell settingCell:self.productCell WithInfoDict:self.infoDict];
    }
    self.productCell.isPlay = YES;
    
    if (self.infoDetailDict == nil) {
        
        __block typeof(self) __weak weakself = self;
        self.tagView.didClickTagAtIndex = ^(NSUInteger index){
            [weakself tagSearchButtonClicked:weakself.infoDetailDict[@"Tags"][index]];
        };
        
        [self addMBProgressHUDWithKey:@"StockList - A007"];
        [ALNetWorkAPI getStockListA007ProductID:_infoDict[@"ProductID"]
                                CompletionBlock:^(NSDictionary *responseObject) {
                                    [self removeMBProgressHUDWithKey:@"StockList - A007"];
                                    //ALLog(@"%@",responseObject);
                                    
                                    if ([responseObject.safe[@"resBody"][@"ResultCode"] intValue] == 0) {
                                        
                                        //成功
                                        _infoDetailDict = responseObject[@"resBody"];
                                        
                                        //新舊樣式
                                        [self prepareSizeSelectButton];
                                        
                                        //讀取tag
                                        self.tagView.padding    = UIEdgeInsetsMake(3, 15, 3, 15);
                                        self.tagView.insets     = 5;
                                        self.tagView.lineSpace  = 8;
                                        
                                        //清除
                                        [self.tagView removeAllTags];
                                        
                                        //加入
                                        NSArray *tags = responseObject[@"resBody"][@"Tags"];
                                        for (int k = 0; k < tags.count; k++) {
                                            SKTag *tag = [SKTag tagWithText:tags[k]];
                                            
                                            NSMutableAttributedString *string =
                                            [[NSMutableAttributedString alloc] initWithString:
                                             [NSString stringWithFormat:@"%@",tags[k]]];
                                            
                                            tag.attributedText = string;
                                            
                                            tag.textColor = [UIColor blackColor];
                                            tag.bgColor = [UIColor whiteColor];
                                            
                                            tag.borderColor = [UIColor lightGrayColor];
                                            tag.borderWidth = 1;
                                            tag.cornerRadius = 3;
                                            
                                            tag.fontSize = 15;
                                            tag.padding = UIEdgeInsetsMake(4.5, 8.0, 6.0, 8.0);
                                            [self.tagView addTag:tag];
                                        }
                                    }
                                }];
    }
    else {
       ALLog(@"已記載infoDetailDict %@",_infoDetailDict);
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.productCell.isPlay = NO;
    [super viewWillDisappear:animated];
}

#pragma mark - 類型選擇 顯示
-(void)prepareSizeSelectButton
{
    NSMutableArray *usedArray = [@[@"New",@"Used"] mutableCopy];
    NSMutableArray *sizeArray = [self.infoDetailDict[@"ModelList"] mutableCopy];
    
    NSMutableArray *usedKeyArray = [NSMutableArray array];
    for (NSInteger k = 0; k < usedArray.count; k++) {
        [usedKeyArray addObject:[@"New" isEqualToString:usedArray[k]] ? @"N" : @"Y"];
    }
    
    NSArray *indexArray =
    [ALFormatter indexOfSortValueArray1:usedKeyArray
                                   Key1:apiKeyIsUsed
                        SortValueArray2:sizeArray
                                   Key2:apiKeySize
                            SourceArray:self.infoDetailDict[@"StockList"]];
    
    self.usedSelectTypeScrollView.infoObjectArray = usedArray;
    self.sizeSelectTypeScrollView.infoObjectArray = sizeArray;
    
    [self.usedSelectTypeScrollView selectIndex:[indexArray[0] integerValue]];
    [self.sizeSelectTypeScrollView selectIndex:[indexArray[1] integerValue]];
}

#pragma mark - ALSelectTypeScrollViewDelegate
-(UIView *)typeViewOfIndex:(NSInteger)index
                infoObject:(NSObject *)infoObject
      selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    if ([infoObject isKindOfClass:[NSString class]]) {
        
        UIButton *aButton =
        [ALSelectTypeScrollView alSelectButton:(NSString *)infoObject];
        
        [aButton addTarget:selectTypeScrollView
                    action:@selector(selectView:)
          forControlEvents:UIControlEventTouchUpInside];
        
        if (selectTypeScrollView == _usedSelectTypeScrollView) {
            [_usedSelectTypeScrollView addSubview:aButton]; //不先加上去 無法執行下一步
            [aButton mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(_usedSelectTypeScrollView.mas_width).multipliedBy(0.5).offset(-1);
            }];
        }
        else {
            CGSize titleSize =
            [ALSelectTypeScrollView alSelectButtonSize:(NSString *)infoObject];
            
            [aButton mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(@(titleSize.width + 40));
            }];
        }
        return aButton;
    }
    else {
        return [[UIView alloc] initWithFrame:CGRectZero];
    }
}

-(void)didSelectIndex:(NSInteger)index
           infoObject:(NSObject *)infoObject
 selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    if (selectTypeScrollView == _usedSelectTypeScrollView) {
        [_sizeSelectTypeScrollView reloadSubView];
    }
    [self didSelectProductSizeType];
}

-(void)adjustSelectView:(UIView *)view
             infoObject:(NSObject *)infoObject
               selected:(BOOL)selected
   selectTypeScrollView:(ALSelectTypeScrollView *)selectTypeScrollView
{
    UIButton *button = (UIButton *)view;
    [ALSelectTypeScrollView alSelectButton:(UIButton *)view Select:selected];
    
    if (selectTypeScrollView == _sizeSelectTypeScrollView) {
        BOOL isAnyFit = NO;
        
        NSArray *stockList = _infoDetailDict[@"StockList"];
        for (int t = 0 ; t < stockList.count; t++) {
            NSDictionary *aProduct = stockList[t];
            
            if ([aProduct[apiKeyIsUsed] isEqualToString:self.selectedIsUsed] &&
                [aProduct[apiKeySize] isEqualToString:(NSString *)infoObject]) {
                
                isAnyFit = YES;
                break;
            }
        }
        if (isAnyFit == NO) {
            button.enabled = NO;
            button.alpha = 0.5;
        }
        else {
            button.enabled = YES;
            button.alpha = 1;
        }
    }
}

#pragma mark - IBAction_類型選擇
-(NSString *)selectedIsUsed
{
    NSString *used = _usedSelectTypeScrollView.infoObjectArray[_usedSelectTypeScrollView.didSelectIndex];
    NSString *selectedIsUsedx = nil;
    if ([used isEqualToString:@"New"]) {
        selectedIsUsedx = @"N";
    }
    if ([used isEqualToString:@"Used"]) {
        selectedIsUsedx = @"Y";
    }
    return selectedIsUsedx;
}

-(NSString *)selectedSizeString
{
    return _sizeSelectTypeScrollView.infoObjectArray[_sizeSelectTypeScrollView.didSelectIndex];
}

#pragma mark - 顯示符合商品
-(void)didSelectProductSizeType
{
    if (_infoDetailDict == nil) {
        return;
    }
    
    self.selectStock = nil;
    
    NSMutableArray *fitProductArray = [NSMutableArray array];
    
    NSArray *stockList = self.infoDetailDict[@"StockList"];
    if ([stockList isKindOfClass:[NSArray class]]) {
        for (int k = 0; k < stockList.count; k++) {
            NSDictionary *aProduct = stockList[k];
            
            BOOL isFit = YES;
            if (![aProduct[apiKeyIsUsed] isEqualToString:self.selectedIsUsed]) {
                isFit = NO;
            }
            if (![aProduct[apiKeySize] isEqualToString:self.selectedSizeString]) {
                isFit = NO;
            }
            if (isFit) {
                [fitProductArray addObject:aProduct];
            }
        }
    }
    
//    //檢查
//    fitProductArray =
//    [@[
//      @{@"Currency":@"XCCCC",
//        @"Price":@2},
//      @{@"Currency":@"ABCCC",
//        @"Price":@1},
//      @{@"Currency":[ALAppDelegate sharedAppDelegate].currency,
//        @"Price":@1},
//      
//      @{@"Currency":@"XCCCC",
//        @"Price":@1},
//      
//      @{@"Currency":[ALAppDelegate sharedAppDelegate].currency,
//        @"Price":@2},
//      @{@"Currency":@"ABCCC",
//        @"Price":@2}
//      ] mutableCopy];

    /* //伺服器排好了
    NSArray *sortedFitProductArray =
    [fitProductArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
        
        
        if ([obj1[@"Currency"] isEqualToString:obj2[@"Currency"]]) {
            //貨幣相同比數字
            return [[NSString stringWithFormat:@"%@",obj1[@"Price"]] compare:
                    [NSString stringWithFormat:@"%@",obj2[@"Price"]] options:NSNumericSearch];
        }
        else {
            //貨幣不同先取同國
            if ([obj1[@"Currency"] isEqualToString:[ALAppDelegate sharedAppDelegate].currency]) {
                return NSOrderedAscending;
            }
            if ([obj2[@"Currency"] isEqualToString:[ALAppDelegate sharedAppDelegate].currency]) {
                return NSOrderedDescending;
            }
            return [(NSString *)obj1[@"Currency"] compare:(NSString *)obj2[@"Currency"] options:NSLiteralSearch];
        }
        //NSComparisonResult
        //NSOrderedAscending , NSOrderedSame, NSOrderedDescending
        return NSOrderedSame;
    }];
    
    fitProductArray = [sortedFitProductArray mutableCopy];
     */
    
    for (UIView *view in _displayProductsView.subviews) {
        [view removeFromSuperview];
    }
    
    UIView *lastOne = nil;
    for (int k = 0; k < fitProductArray.count; k++) {
        
        ALProductSellerRowView *aProductRow =
        [[ALProductSellerRowView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        
        aProductRow.rowData = fitProductArray[k];
        aProductRow.delegate = self;
        
        [self.displayProductsView addSubview:aProductRow];
        [aProductRow mas_makeConstraints:^(MASConstraintMaker *make) {
            
            if (lastOne == nil) {
                make.top.equalTo(self.displayProductsView.mas_top);
            }
            else {
                make.top.equalTo(lastOne.mas_bottom).offset(3);
            }
            make.leading.equalTo(self.displayProductsView.mas_leading).offset(10);
            make.trailing.equalTo(self.displayProductsView.mas_trailing).offset(-10);
            
            make.height.equalTo(@(35));
        }];
        lastOne = aProductRow;
    }
    
    if (lastOne) {
        [lastOne mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.displayProductsView.mas_bottom);
        }];
    }
    
    if (fitProductArray.count > 0) {
        self.selectStock = fitProductArray.firstObject;
    }
}

#pragma mark - 選中商品
-(void)setSelectStock:(NSDictionary *)selectStock
{
    _selectStock = selectStock;
    for (UIView *view in _displayProductsView.subviews) {
        if ([view isKindOfClass:[ALProductSellerRowView class]]) {
            ALProductSellerRowView *productSellerRowView = (ALProductSellerRowView *)view;
            [productSellerRowView performSelectColor:_selectStock];
        }
    }
}

#pragma mark - IBAction_銷售按鈕
-(IBAction)sellAction:(UIButton *)sneder
{
    if (![PDSAccountManager isLoggedIn]) {
        [[PDSEnvironmentViewController sharedInstance] presentLoginViewController];
        return;
    }
    //賣
    if (!self.selectedIsUsed) {
        YKSimpleAlert(@"請先選擇新/舊");//已經用不到 都會自動帶入
        return;
    }
    if (!self.selectedSizeString) {
        YKSimpleAlert(@"請先選擇尺寸");//已經用不到 都會自動帶入
        return;
    }
    
    ALPublishConfirmViewController *vc =
    [[ALPublishConfirmViewController alloc] initWithProductID:_infoDict[@"ProductID"]
                                                  productSize:_infoDetailDict[@"ModelList"]
                                                  productName:_infoDict[@"ProductTitle"]
                                                 productImage:nil
                                              productImageURL:_infoDict[@"ProductImage"]];
    vc.presetIsUsed = self.selectedIsUsed;
    vc.presetSize = self.selectedSizeString;
    [self.navigationController pushViewController:vc animated:YES];
    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//        
//        NSData *imageData = [ALNetWorkAPI cachedImageFromURL:_infoDict[@"ProductImage"]];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self removeMBProgressHUDWithKey:@"ProductDetailLoadProductImage"];
//            
//            ALPublishConfirmViewController *vc =
//            [[ALPublishConfirmViewController alloc] initWithProductID:_infoDict[@"ProductID"]
//                                                          productSize:_infoDetailDict[@"ModelList"]
//                                                          productName:_infoDict[@"ProductTitle"]
//                                                         productImage:[UIImage imageWithData:imageData]];
//            vc.presetIsUsed = self.selectedIsUsed;
//            vc.presetSize = self.selectedSizeString;
//            [self.navigationController pushViewController:vc animated:YES];
//        });
//    });
}

-(IBAction)webProductDataActiona:(UIButton *)sneder
{
    //產品資料
    ALProductEditHistoryViewController *next = [[ALProductEditHistoryViewController alloc] init];
    next.infoDict = self.infoDict;
    
    [self.navigationController pushViewController:next animated:YES];
}

-(IBAction)webProductSellActiond:(UIButton *)sneder
{
    //銷量變化
    ALProductStatisticsViewController *next = [[ALProductStatisticsViewController alloc] init];
    next.infoDict = self.infoDict;
    
    [self.navigationController pushViewController:next animated:YES];
}

-(IBAction)webProductEditActionc:(UIButton *)sneder
{
    if (![PDSAccountManager isLoggedIn]) {
        return;
    }
    
    //修改商品
    ALProductEditViewController *next = [[ALProductEditViewController alloc] init];
    next.infoDict = self.infoDict;
    
    self.infoDetailDict = nil; //修改之後這部分需要重新讀取
    [self.navigationController pushViewController:next animated:YES];
}

#pragma mark - tag 按鈕
-(void)tagSearchButtonClicked:(NSString *)keyword
{    
    ALSearchV3ViewController *vc = [[ALSearchV3ViewController alloc] init];
    vc.keyword = keyword;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - IBAction_購買通訊
-(IBAction)imAction:(UIButton *)sender
{
    if (![PDSAccountManager isLoggedIn]) {
        [[PDSEnvironmentViewController sharedInstance] presentLoginViewController];
        return;
    }
    //聊天
    if (!self.selectStock) {
        YKSimpleAlert(ALLocalizedString(@"Please select merchandise", nil));
        return;
    }
    if ([self.selectStock[@"SellerID"] isEqualToString:[ALAppDelegate sharedAppDelegate].userID]) {
        YKSimpleAlert(ALLocalizedString(@"This is your own merchandise", nil));
        return;
    }
    
    NSString *sellerID = self.selectStock[@"SellerID"];
    [self startChatPerson:sellerID];
    
    
    //    __block typeof(self) __weak weakself = self;
    //    [self addMBProgressHUDWithKey:@"getIMRoomIDA023"];
    //    [ALNetWorkAPI getIMRoomIDA023ForUser1:[ALAppDelegate sharedAppDelegate].userID
    //                                    User2:sellerID
    //                          completionBlock:^(NSDictionary *responseObject) {
    //                              [self removeMBProgressHUDWithKey:@"getIMRoomIDA023"];
    //
    //                              NSString *roomID = responseObject[@"RoomID"];
    //                              if (![roomID isKindOfClass:[NSString class]]) {
    //                                  roomID = @"";
    //                              }
    //
    //                              if (roomID.length == 0) {
    //                                  [self addMBProgressHUDWithKey:@"createIMRoomIDA024"];
    //                                  [ALNetWorkAPI createIMRoomIDA024ForUser1:[ALAppDelegate sharedAppDelegate].userID
    //                                                                     User2:sellerID
    //                                                           completionBlock:^(NSDictionary *responseObject) {
    //                                                               [self removeMBProgressHUDWithKey:@"createIMRoomIDA024"];
    //
    //                                                               //[weakself startChat:@""];
    //
    //
    //                                                           }];
    //
    //                              }
    //                              else {
    //                                  //[weakself startChat:@""];
    //                              }
    //                          }];
}

-(void)startChatPerson:(NSString *)otherId
{
    if (![[PDSAccountManager sharedManager] imClientID]) {
        [[PDSAccountManager sharedManager] loginLeanCloud];
        YKSimpleAlert(ALLocalizedString(@"$Chat service is disconnected$. $Please try again later$.", nil));
        return;
    }
    for (AVIMConversation *conversation in [[PDSAccountManager sharedManager] conversationArray]) {
        if ([[conversation members] count] == 2 && [[conversation members] containsObject:otherId] && [[conversation members] containsObject:[[ALAppDelegate sharedAppDelegate] userID]]) {
            ALChatRoomController *controller = [[ALChatRoomController alloc] init];
            controller.currentConversation = conversation;
            [self.navigationController pushViewController:controller animated:YES];
            return ;
        }
    }
    // no conversation can be reused. create new one
    [[[PDSAccountManager sharedManager] imClient] createConversationWithName:@"Chat" clientIds:@[otherId] callback:^(AVIMConversation *conversation, NSError *error) {
        if (conversation) {
            [[[PDSAccountManager sharedManager] conversationArray] insertObject:conversation atIndex:0];
            [[NSNotificationCenter defaultCenter] postNotificationName:kPDSIMDidLoadConversationList object:nil];

            ALChatRoomController *controller = [[ALChatRoomController alloc] init];
            controller.currentConversation = conversation;
            [self.navigationController pushViewController:controller animated:YES];
        }
        else {
            YKSimpleAlert(ALLocalizedString(@"$Server temporarily not responding$. $Please try again later$.", nil));
        }
    }];
    
}

- (IBAction)cartAction:(UIButton *)sneder
{
    if (![PDSAccountManager isLoggedIn]) {
        [[PDSEnvironmentViewController sharedInstance] presentLoginViewController];
        return;
    }
    //買
    if (!self.selectStock) {
        YKSimpleAlert(ALLocalizedString(@"Please select merchandise", nil));
        return;
    }
    if ([self.selectStock[@"SellerID"] isEqualToString:[ALAppDelegate sharedAppDelegate].userID]) {
        YKSimpleAlert(ALLocalizedString(@"This is your own merchandise", nil));
        return;
    }
    
    [ALNetWorkAPI cartAddV1ProductA079ProductID:self.infoDict[@"ProductID"]
                                        StockID:self.selectStock[@"StockID"]
                                       SellerID:self.selectStock[@"SellerID"]
                                    OrderAmount:@"1"
                                CompletionBlock:^(NSDictionary *responseObject) {
                                    if ([ALNetWorkAPI checkSerialNumber:@"A079"
                                                         ResponseObject:responseObject]) {
                                        YKSimpleAlert(ALLocalizedString(@"Success",nil));
                                    }
                                    else {
                                        YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A079", responseObject),nil));
                                    }
                                }];
}

-(IBAction)buyAction:(UIButton *)sneder
{
    if (![PDSAccountManager isLoggedIn]) {
        [[PDSEnvironmentViewController sharedInstance] presentLoginViewController];
        return;
    }
    //買
    if (!self.selectStock) {
        YKSimpleAlert(ALLocalizedString(@"Please select merchandise", nil));
        return;
    }
    if ([self.selectStock[@"SellerID"] isEqualToString:[ALAppDelegate sharedAppDelegate].userID]) {
        YKSimpleAlert(ALLocalizedString(@"This is your own merchandise", nil));
        return;
    }
    
    ALPlaceOrderViewController *vc =
    [[ALPlaceOrderViewController alloc] initWithProductID:_infoDict[@"ProductID"] stockInfo:self.selectStock];
    vc.delegate = self;
    
    vc.productTitle = _infoDict[@"ProductTitle"];
    vc.productImageURL = _infoDict[@"ProductImage"];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
