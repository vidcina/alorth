//
//  ALColorDefinition.m
//  alorth
//
//  Created by w91379137 on 2015/10/2.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALColorDefinition.h"

@implementation ALColorDefinition

+(UIColor *)alColorWithString:(NSString *)alColorString
{
    if ([alColorString isEqualToString:@"COMMON_GRAY_LINE_COLOR"]) {
        return COMMON_GRAY_LINE_COLOR;
    }
    if ([alColorString isEqualToString:@"COMMON_GRAY1_COLOR"]) {
        return COMMON_GRAY1_COLOR;
    }
    if ([alColorString isEqualToString:@"COMMON_GRAY2_COLOR"]) {
        return COMMON_GRAY2_COLOR;
    }
    if ([alColorString isEqualToString:@"COMMON_GRAY3_COLOR"]) {
        return COMMON_GRAY3_COLOR;
    }
    if ([alColorString isEqualToString:@"COMMON_GRAY4_COLOR"]) {
        return COMMON_GRAY4_COLOR;
    }
    return nil;
}

@end
