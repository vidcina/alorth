//
//  AppDelegate.h
//  alorth
//
//  Created by John Hsu on 2015/5/25.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/SignIn.h>
@interface ALAppDelegate : UIResponder <UIApplicationDelegate,GIDSignInDelegate>
{
    
}

@property (strong, nonatomic) UIWindow *window;
@property (copy, nonatomic) NSData *pushToken;
#pragma mark - Custom Method
+(ALAppDelegate *)sharedAppDelegate;
-(void)sendPushTokenToServer;

@property (nonatomic, strong) NSMutableDictionary *userInfomation;
-(void)userInfomationRenewWithLoginAccount002:(NSDictionary *)dict;
-(void)userInfomationRenewWithPersonalProfile021:(NSDictionary *)dict;

@property (nonatomic, readonly, strong) NSString *userID;
@property (nonatomic, readonly, strong) NSString *userNameString;
@property (nonatomic, readonly, strong) NSString *userAvatarURLString;

@property (nonatomic, readonly, strong) NSString *countryCode;
@property (nonatomic, readonly, strong) NSString *currency;

@property (nonatomic, strong) CLLocation *nowLocation;//開啟後讀取 未讀取到就是nil
@property (nonatomic, strong) CLLocation *lastLocation;//上次紀錄 未讀取到就是(0,0)
-(void)clearUserInformation;
@end

