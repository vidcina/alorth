//
//  ALDefinition.h
//  alorth
//
//  Created by w91379137 on 2015/5/25.
//  Copyright (c) 2015年 miros. All rights reserved.
//

// 0:Production server, 1: testing server, 2: staging server
#define UseDevelopmentServer 2
#define UIInterfaceVersion 3


static NSString *kThirdPartyLoginPassword = @"ThirdPartyPassword";



#import "ALLogManager.h"

#define ALitunesAppID 1110341356
#define ALMerchantIdentifier @"merchant.com.aimcue.aimcue"



#if UseDevelopmentServer == 1
    #warning 注意■■■■■ You are using development mode 注意■■■■■
    #define API_domain @"https://test.alorth.com"//測試站

    #define AVOSAppID @"77cauc1xcrbprg78ju0foyyq6dg8blaz6tm6kprihhj1lcng"//測試AVOS
    #define AVOSAppKey @"r25tlhlc8m16tnwaspn4arjna3fufa2510yh73qc4ef6zhc7"//測試AVOS

    static NSString *const kStripePublishableKey = @"pk_test_rfo5QkAylDCrYl5fbEAVeVsh";//測試
    #define MIXPANEL_TOKEN @"780ad5e683c1d0497c20a7ac35220962"//測試log站
    #define StripeAuthURL API_domain"/oauth/check_oauth"
    #define Danmu_API_domain @"http://sa-video-comment.unisharp.net/api/v1"//測試站
    #define Danmu_Socket_Address @"http://sa-video-comment.unisharp.net:1337"

#elif UseDevelopmentServer == 2
    #warning 注意■■■■■ You are using staging mode 注意■■■■■
    #define API_domain @"https://staging.alorth.com"//staging站

    #define AVOSAppID @"cjdt67lu07a5aipevhcovsq3h3iwfrvqjob3mh9gbz1c90u9"//測試AVOS
    #define AVOSAppKey @"mr9ccm3m69btwadaynflw7f54xkrrzgf0g63h6b95b7a9c4i"//測試AVOS

    static NSString *const kStripePublishableKey = @"pk_live_6Le3yRCaPi79Yki7PzgpsYRd";//測試
    #define MIXPANEL_TOKEN @"780ad5e683c1d0497c20a7ac35220962"//測試log站
    #define StripeAuthURL @"https://service.alorth.com/oauth/check_oauth" // 20160309 Kobe:與正式站共用oauth網頁
    #define Danmu_API_domain @"http://danmu.alorth.com/api/v1"
    #define Danmu_Socket_Address @"http://danmu.alorth.com:80"
#else
    //#define API_domain @"http://114.215.170.238/alorth/index.php"
    #define API_domain @"https://service.alorth.com"//正式站

    #define AVOSAppID @"cjdt67lu07a5aipevhcovsq3h3iwfrvqjob3mh9gbz1c90u9"//正式AVOS
    #define AVOSAppKey @"mr9ccm3m69btwadaynflw7f54xkrrzgf0g63h6b95b7a9c4i"//正式AVOS

    static NSString *const kStripePublishableKey = @"pk_live_6Le3yRCaPi79Yki7PzgpsYRd";//正式
    #define MIXPANEL_TOKEN @"5d12e2b1225b1293e9803bd28d79e1c2"//正式log站
    #define StripeAuthURL API_domain"/oauth/check_oauth"
    #define Danmu_API_domain @"http://danmu.alorth.com/api/v1"//
    #define Danmu_Socket_Address @"http://danmu.alorth.com:80"
#endif


#define weakSelfMake(weakSelf) __weak __typeof(&*self)weakSelf = self;

//http://stackoverflow.com/questions/9619708/nslog-to-both-console-and-file/
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-function"

static inline void ALBLog(NSString *format, ...)NS_FORMAT_FUNCTION(1,2);

static inline void ALBLog(NSString *format, ...)
{
    va_list argList;
    va_start(argList, format);
    NSString* formattedMessage = [[NSString alloc] initWithFormat: format arguments: argList];
    va_end(argList);
    
#ifdef DEBUG
    fprintf(stderr,"%s\n",[formattedMessage UTF8String]);
#endif
    
    [[ALLogManager sharedInstance] writeLog:formattedMessage];
}
#pragma clang diagnostic pop

#define ALLog(fmt, ...) ALBLog((@"" fmt),##__VA_ARGS__);

//#define ALLog(fmt, ...) ALBLog((@"\n%s:%d:\n" fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);

//#define ALLog(fmt, ...) ALBLog((@"%s:%s:%d:\n" fmt),[[[NSString stringWithUTF8String:__FILE__] lastPathComponent] UTF8String], __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);

#if !DEBUG
    #define NSLog(FORMAT, ...) nil
#endif

typedef enum {
    ALTakePublishPhoto = 0,
    ALTakePublishVideo = 1,
} ALTakePublishType;

#define ALNetworkErrorMsg @"$There are some things wrong.$\n$Try again later.$"

//static NSString *cachesImageFileDirPath = [NSHomeDirectory() stringByAppendingPathComponent:@"/Library/Caches/upload/"];
static NSString *apiKeyIsUsed = @"IsUsed";
static NSString *apiKeySize = @"Size";

//伺服器 儲存key 不能任意修改
static NSString *ALCreditCardPayType = @"credit card";
static NSString *ALDealMySelfPayType = @"self";

//伺服器 儲存key 不能任意修改
static NSString *ALCreditCardPayWithStripe = @"Stripe";
static NSString *ALCreditCardPayWithApplePay = @"Apple Pay"; //與此符合將顯示 Apple Pay logo

