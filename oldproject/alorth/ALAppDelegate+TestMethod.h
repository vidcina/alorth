//
//  ALAppDelegate+TestMethod.h
//  alorth
//
//  Created by w91379137 on 2015/12/30.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALAppDelegate.h"

@interface ALAppDelegate (TestMethod)

- (NSURL *)testStreamURL;

@end
