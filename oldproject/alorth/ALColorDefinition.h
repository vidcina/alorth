//
//  ALColorDefinition.h
//  alorth
//
//  Created by w91379137 on 2015/10/2.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <Foundation/Foundation.h>

#define COMMON_GRAY_LINE_COLOR [UIColor colorWithHexString:@"DEDEDE"]
#define COMMON_GRAY_Circle_COLOR [UIColor colorWithHexString:@"8e8e93"]

#define COMMON_GRAY1_COLOR [UIColor colorWithHexString:@"EEEEEE"]
#define COMMON_GRAY2_COLOR [UIColor colorWithHexString:@"DEDEDE"]
#define COMMON_GRAY3_COLOR [UIColor colorWithHexString:@"AAAAAA"]
#define COMMON_GRAY4_COLOR [UIColor colorWithHexString:@"595959"]

#define COMMON_Yellow_COLOR [UIColor colorWithHexString:@"FFEF00"]
#define COMMON_PriceRed_COLOR [UIColor colorWithHexString:@"C10D23"]

@interface ALColorDefinition : NSObject

+(UIColor *)alColorWithString:(NSString *)alColorString;

@end
