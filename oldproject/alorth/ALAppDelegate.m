//
//  AppDelegate.m
//  alorth
//
//  Created by John Hsu on 2015/5/25.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "ALAppDelegate.h"
#import "ALAppDelegate+TestMethod.h"

#import "ALLanguageCenter.h"

#import <CoreLocation/CoreLocation.h>
#import "GAI.h"
#import <Stripe/Stripe.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Intercom/Intercom.h>
#import <AppsFlyer/AppsFlyer.h>
#import <AdSupport/ASIdentifierManager.h>
#import "PDSAccountManager.h"

static NSString *ALAppDelegateLastLocationKey = @"ALAppDelegateLastLocationKey";
static NSString *ALAppDelegateCountryCodeKey = @"ALAppDelegateCountryCodeKey";
static NSString *ALUserDefaultsUserInformationJsonKey = @"ALUserDefaultsUserInformationJsonKey";

@interface ALAppDelegate ()

@end

@implementation ALAppDelegate

#pragma mark - Application life
+(ALAppDelegate *)sharedAppDelegate
{
    return (id)[[UIApplication sharedApplication] delegate];
}

-(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self applicationFolderSetting];
    YKInitialize();
//    NSLog(@"idfa:%@",[[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString]);
    setupMixModeForAudioSession();
    [Stripe setDefaultPublishableKey:kStripePublishableKey];
    
    // Google Sign in
    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    [GIDSignIn sharedInstance].delegate = self;
    
    // Appsflyer SDK
    [AppsFlyerTracker sharedTracker].appsFlyerDevKey = @"zwEeibXtmUd2TmQSkLRrCP";
    [AppsFlyerTracker sharedTracker].appleAppID = @"1110341356";

    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:nil];
    [session setActive:YES error:nil];
    
#if !TARGET_IPHONE_SIMULATOR
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];

#endif
    [[ALBundleTextResources sharedInstance] textDataInitialAndUpdate];
    [ALLanguageCenter autoConnectLocalizableString]; //把現行csv語言 跟 Localizable String 設定相同
    
    //新生視窗
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window makeKeyAndVisible];
    
    //視窗設定
    PDSEnvironmentViewController *enVC = [PDSEnvironmentViewController sharedInstance];
    enVC.mainNavigationController = [[ALNavigationViewController alloc] initHomeALNavigationViewController];
    [self.window setRootViewController:enVC];
    
    [AVOSCloud setApplicationId:AVOSAppID
                      clientKey:AVOSAppKey];
    
    dispatch_group_t loginGroup = dispatch_group_create();
    
    // 第三方登入，但使用者資料不見，需要重新登入
    if ([PDSAccountManager isLoggedIn] && [PDSAccountManager isThirdPartyLogin] && ![[[self userInfomation] allKeys] count]) {
        [[PDSAccountManager sharedManager] logout];
        [[GIDSignIn sharedInstance] signOut];
        [Intercom reset];
    }
    
    [Intercom setApiKey:@"ios_sdk-1a523a2361c6121b8a44e7aed7d0cc38777b76bb" forAppId:@"tl3tc4o9"];
    if ([PDSAccountManager isLoggedIn] && ![PDSAccountManager isThirdPartyLogin]) {

        dispatch_group_enter(loginGroup);
        [PDSAccountManager loginAccount:nil
                               Password:nil
                        CompletionBlock:^(NSDictionary *responseObject) {
                            if ([PDSAccountManager isLoggedIn]) {
                                [Intercom registerUserWithUserId:self.userID];
                                ALLog(@"登入成功");
                            }
                            else {
                                // [Intercom registerUnidentifiedUser];
                                ALLog(@"登入失敗");
                            }
                            dispatch_group_leave(loginGroup);
                        }];
    }
    else if ([PDSAccountManager isLoggedIn]) { // 第三方登入
        [Intercom registerUserWithUserId:[PDSAccountManager userName]];
    }
    else {
        // [Intercom registerUnidentifiedUser];
        /* 取消詢問登入 2016/01/12 by 阿宅
        [[PDSEnvironmentViewController sharedInstance] presentLoginViewController];
         */
    }
    
    dispatch_group_notify(loginGroup,dispatch_get_main_queue(),^{
        [ALInspector startSession];
        [ALInspector logEventTrack:Track_Event_LaunchStaus
                        Parameters:nil];
        
        NSURL *launchUrl = [launchOptions objectForKey:UIApplicationLaunchOptionsURLKey];
        //url = [self testStreamURL]; //測試藉由URL 開啟的狀態
        if (launchUrl) {
            [self application:application handleOpenURL:launchUrl];
        }
        [FBSDKAppLinkUtility fetchDeferredAppLink:^(NSURL *url, NSError *error) {
            if (error) {
                NSLog(@"Received error while fetching deferred app link %@", error);
            }
            if (url) {
                [self application:application handleOpenURL:url];
            }
        }];
    });
    
    return YES;
}

#pragma mark - iCloud Setting
-(void)applicationFolderSetting
{
    //app主目錄
    ALLog(@"Path: %@",NSHomeDirectory());
    
    //防止上傳到雲端
    NSArray *documentsPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [documentsPaths objectAtIndex:0];
    
    NSFileManager *manager = [NSFileManager defaultManager];
    NSArray *fileList = [manager contentsOfDirectoryAtPath:documentsDirectory error:nil];
    for (NSString *subFolderPath in fileList){
        //ALLog(@"子資料夾名稱 %@", subFolderPath);
        NSString *documentsSubfolderPath = [documentsDirectory stringByAppendingPathComponent:subFolderPath];
        BOOL isDir;
        BOOL exists = [manager fileExistsAtPath:documentsSubfolderPath isDirectory:&isDir];
        if (exists) {
            /* file exists */
            if (isDir) {
                /* file is a directory */
                //NSLog(@"資料夾");
                NSURL *localURL = [NSURL fileURLWithPath:documentsSubfolderPath];
                [self addSkipBackupAttributeToItemAtURL:localURL];
            }
        }
    }
}

//http://productiosappbyxcode.blogspot.tw/2013/01/icloud.html
- (void)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    NSError *error = nil;
    BOOL success = [URL setResourceValue:[NSNumber numberWithBool: YES]
                                  forKey:NSURLIsExcludedFromBackupKey
                                   error:&error];
    if (success == NO) {
        NSLog(@"設定禁止上傳失敗");
    }
}

#pragma mark push notification
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    if (notificationSettings.types != UIUserNotificationTypeNone) {
        //register to receive notifications
        [application registerForRemoteNotifications];
    }
    else {
        // silent error
    }
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    self.pushToken = deviceToken;
    [self sendPushTokenToServer];
}

-(void)sendPushTokenToServer
{
    if (![PDSAccountManager isLoggedIn]) {
        return;
    }
    
    [Intercom setDeviceToken:self.pushToken];
    NSString *token = [NSString stringWithFormat:@"%@", [self.pushToken description]];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];//去左箭號
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];//去右鍵號
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];//去空白
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSString *alert = userInfo[@"aps"][@"alert"];
    if ([alert length]) {
        [[[UIAlertView alloc] initWithTitle:nil message:alert delegate:nil cancelButtonTitle:NSLocalizedString(@"button.OK", nil) otherButtonTitles: nil] show];
    }
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    // silent error
}

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    
}
#pragma mark - application Active / Background
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
#if !TARGET_IPHONE_SIMULATOR
    NSString *userID = [PDSAccountManager userName];
    if ([PDSAccountManager isLoggedIn] && userID) {
        [AppsFlyerTracker sharedTracker].customerUserID = userID;
        //個人資料
        [ALNetWorkAPI personalProfile020CompletionBlock:^(NSDictionary *responseObject) {
            if ([ALNetWorkAPI checkSerialNumber:@"A020"
                                 ResponseObject:responseObject]) {
                NSMutableDictionary *dict = [[ALAppDelegate sharedAppDelegate] userInfomation];
                for (NSString *key in [dict allKeys]) {
                    if (responseObject[@"resBody"][key]) {
                        dict[key] = responseObject[@"resBody"][key];
                    }
                }
//                [PDSAccountManager sharedManager].profileDictionary = responseObject[@"resBody"];
            }
        }];
    }
    [FBSDKAppEvents activateApp];
    [[AppsFlyerTracker sharedTracker] trackAppLaunch];
#endif
    [[ALBundleTextResources sharedInstance] versionCheck];
    [self locateCountry];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - handleOpenURL
-(BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    [[AppsFlyerTracker sharedTracker] handleOpenURL:url sourceApplication:nil];

    BOOL shouldHandle = [ALNotiOpenURLManager shouldHandleURL:url];
    if (shouldHandle) {
        [[ALNotiOpenURLManager sharedInstance] handleURL:url];
        return YES;
    }
    if ([[url scheme] hasPrefix:@"fb"]) {
        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url
                                                    sourceApplication:nil
                                                           annotation:nil
                ];
    }
    else {
        return [[GIDSignIn sharedInstance] handleURL:url
                                   sourceApplication:nil
                                          annotation:nil];
    }
}

-(BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options
{
    [[AppsFlyerTracker sharedTracker] handleOpenURL:url sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]];

    BOOL shouldHandle = [ALNotiOpenURLManager shouldHandleURL:url];
    if (shouldHandle) {
        [[ALNotiOpenURLManager sharedInstance] handleURL:url];
        return YES;
    }
    if ([[url scheme] hasPrefix:@"fb"]) {
        return [[FBSDKApplicationDelegate sharedInstance] application:app
                                                              openURL:url
                                                    sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                           annotation:nil
                ];
    }
    return [[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                      annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];

}

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    [[AppsFlyerTracker sharedTracker] handleOpenURL:url sourceApplication:sourceApplication];

    BOOL shouldHandle = [ALNotiOpenURLManager shouldHandleURL:url];
    if (shouldHandle) {
        [[ALNotiOpenURLManager sharedInstance] handleURL:url];
        return YES;
    }
    if ([[url scheme] hasPrefix:@"fb"]) {
        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url
                                                    sourceApplication:sourceApplication
                                                           annotation:annotation
                ];
    }
    return [[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:sourceApplication
                                      annotation:annotation];
}

// Reports app open from a Universal Link for iOS 9
- (BOOL) application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray *_Nullable))restorationHandler
{
    [[AppsFlyerTracker sharedTracker] continueUserActivity:userActivity restorationHandler:restorationHandler];
    return YES;
}


#pragma mark Google Sign in delegate
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations on signed in user here.
    if (!error) {
        NSString *currnetCountryCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"currnetCountryCode"];
        
        NSDateFormatter *formatter =
        [ALFormatter dateFormatteWithTimeZone:[NSTimeZone systemTimeZone]
                                 FormatString:@"yyyy-MM-dd' 'HH:mm:ss"];
        
        NSDictionary *accountInfo = @{
                                      @"avatar": [user.profile hasImage]? [[user.profile imageURLWithDimension:150] absoluteString]: @"",
                                      @"userID": [NSString stringWithFormat:@"google@%@",  user.userID],
                                      @"email": user.profile.email,
                                      @"displayName": user.profile.name,
                                      @"type": @"google",
                                      @"token": user.authentication.idToken,
                                      @"firstName": user.profile.givenName,
                                      @"lastName": user.profile.familyName,
                                      @"expiredTime": [formatter stringFromDate:user.authentication.idTokenExpirationDate]
                                      };
        
        [PDSAccountManager loginWithSocialAccount:accountInfo country:currnetCountryCode completionBlock:^(NSDictionary *responseObject) {
            if ([PDSAccountManager isLoggedIn]) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"googleSignInSuccess" object:nil];
            }
            else {
                YKSimpleAlert(ALLocalizedString(ALNetworkErrorLocalizedString(@"A123", responseObject),nil));
            }
        }];
    }
    else {
        YKSimpleAlert(@"Google login failed(%@). Please try again later.", [error localizedDescription]);
    }
}

- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    YKSimpleAlert(@"Disconnect failed(%@). Please try again later.", [error localizedDescription]);

}
#pragma mark - locateCountry
-(void)locateCountry
{
    YKWaitForLocationUpdate(5, ^(CLLocation *location) {
        
        self.nowLocation = location;
        
        //登入者 伺服器定位
        {
            NSString *locationString =
            [NSString stringWithFormat:@"%.3f,%.3f",location.coordinate.latitude,location.coordinate.longitude];
            [[NSUserDefaults standardUserDefaults] setObject:locationString forKey:ALAppDelegateLastLocationKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            if ([PDSAccountManager isLoggedIn]) {
                [ALNetWorkAPI setGPSA003Longitide:location.coordinate.longitude
                                         latitude:location.coordinate.latitude
                                  CompletionBlock:^(NSDictionary *responseObject) {
                                      
                                      if (![ALNetWorkAPI checkSerialNumber:@"A003"
                                                           ResponseObject:responseObject]) {
                                          ALLog(@"登入者 伺服器定位:%@",responseObject);
                                      }
                                      
                                  }];
            }
        }
        
        // result_type : https://developers.google.com/maps/documentation/geocoding/#Types
        dispatch_group_t downloadGroup = dispatch_group_create();
        
        __block NSData *data = nil;
        
        dispatch_group_enter(downloadGroup);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            data =
            [NSData dataWithContentsOfURL:
             [NSURL URLWithString:
              [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&key=AIzaSyBg0SsmuUYuwAPl1k3rikf1XBZYyYMH6cI&result_type=country&language=en",
               location.coordinate.latitude,
               location.coordinate.longitude]]];
            
            dispatch_group_leave(downloadGroup);
        });
        
        dispatch_group_notify(downloadGroup, dispatch_get_main_queue(), ^{
            if (data) {
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                //NSLog(@"dict:%@",dict);
                NSArray *foundAddressComponents = [dict[@"results"] valueForKey:@"address_components"];
                
                for (NSArray *result in foundAddressComponents) {
                    for (NSDictionary *component in result) {
                        if ([component[@"types"] indexOfObject:@"country"] != NSNotFound) {
                            NSString *countryCode = component[@"short_name"];
                            
                            ALLog(@"found short name:%@",countryCode);
                            if ([countryCode length]) {
                                self.countryCode = countryCode;
                                return;
                            }
                        }
                    }
                }
            }
        });
        //[self performSelector:@selector(locateCountry) withObject:nil afterDelay:3];
    }, ^{
        //[self performSelector:@selector(locateCountry) withObject:nil afterDelay:3];
    });
}

#pragma mark - userInfomation
-(NSMutableDictionary *)userInfomation
{
    if (_userInfomation == nil) {
        [self readUserInformation];
    }
    if (_userInfomation == nil) {
        _userInfomation = [NSMutableDictionary dictionary];
    }
    return _userInfomation;
}

-(void)userInfomationRenewWithLoginAccount002:(NSDictionary *)dict
{
    if (![dict isKindOfClass:[NSDictionary class]]) {
        NSLog(@"A002 輸入錯誤 %@",dict);
        return;
    }
    
    //登入必有
    /*
     AuthTime
     Currency
     FirstName
     HeadImage
     LastName
     ResultCode
     UserID
     UserToken
     */
    
    ALLog(@"A002 輸入 %@",dict);
    for (NSString *key in dict.allKeys) {
        if (![dict[key] isKindOfClass:[NSNull class]]) {
            self.userInfomation[key] = dict[key];
        }
    }
    [self saveUserInformation];
    ALLog(@"A002 輸出 %@",self.userInfomation);
}

-(void)userInfomationRenewWithPersonalProfile021:(NSDictionary *)dict
{
    if (![dict isKindOfClass:[NSDictionary class]]) {
        NSLog(@"A021 輸入錯誤 %@",dict);
        return;
    }
    
    ALLog(@"A021 輸入 %@",dict);
    for (NSString *key in dict.allKeys) {
        if (![dict[key] isKindOfClass:[NSNull class]]) {
            self.userInfomation[key] = dict[key];
        }
    }
    [self saveUserInformation];
    ALLog(@"A021 輸出 %@",self.userInfomation);
}

-(void)readUserInformation
{
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:ALUserDefaultsUserInformationJsonKey];
    if ([data length]) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        if (dict) {
            _userInfomation = [dict mutableCopy];
        }
    }
}

-(void)saveUserInformation
{
    NSData *data = [NSJSONSerialization dataWithJSONObject:self.userInfomation options:kNilOptions error:nil];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:ALUserDefaultsUserInformationJsonKey];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

-(void)clearUserInformation
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:ALUserDefaultsUserInformationJsonKey];
    [[NSUserDefaults standardUserDefaults] synchronize];

}
#pragma mark - Getter / Setter
-(NSString *)userID
{
    if ([PDSAccountManager isLoggedIn]) {
        return self.userInfomation[@"UserID"];
    }
    return nil;
}

-(NSString *)userNameString
{
    return [ALFormatter preferredDisplayNameFirst:self.userInfomation[@"FirstName"]
                                             Last:self.userInfomation[@"LastName"]
                                           UserID:self.userID];
}

-(NSString *)userAvatarURLString
{
    return self.userInfomation[@"HeadImage"];
}

-(void)setCountryCode:(NSString *)countryCode
{
    self.userInfomation[@"countryCode"] = countryCode;
    [[NSUserDefaults standardUserDefaults] setObject:countryCode forKey:ALAppDelegateCountryCodeKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString *)countryCode
{
    if (self.userInfomation[@"countryCode"]) {
        return self.userInfomation[@"countryCode"];
    }
    return [[NSUserDefaults standardUserDefaults] objectForKey:ALAppDelegateCountryCodeKey];
}

-(NSString *)currency
{
    return self.userInfomation[@"Currency"];
}

-(CLLocation *)nowLocation
{
    if (_nowLocation) {
        return _nowLocation;
    }
    return nil;
}

-(CLLocation *)lastLocation
{
    NSString *locationString = [[NSUserDefaults standardUserDefaults] objectForKey:ALAppDelegateLastLocationKey];
    
    NSArray *numbers = [locationString componentsSeparatedByString:@","];
    if (numbers.count > 1) {
        double latitude = [numbers[0] doubleValue];
        double longitude = [numbers[1] doubleValue];
        return [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    }
    else {
        return [[CLLocation alloc] initWithLatitude:0 longitude:0];
    }
}


@end
