//
//  ALAppDelegate+TestMethod.m
//  alorth
//
//  Created by w91379137 on 2015/12/30.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "ALAppDelegate+TestMethod.h"

@implementation ALAppDelegate (TestMethod)

- (NSURL *)testStreamURL
{
    //已經失效ID
    //f09b4a33ffa2e2756f2c163bf62e42ef1451453689
    
    //有效ID
    //098f6bcd4621d373cade4e832627b4f61452676724
    return [NSURL URLWithString:@"alorth://video/098f6bcd4621d373cade4e832627b4f61452676724"];
}

@end
