//
//  JHFacebookLogin.m
//
//  Created by John Hsu on 2016/2/19.
//

#import "JHFacebookLogin.h"

@implementation JHFacebookLogin
@synthesize accountStore, facebookAccount, handler;
+(void)loginFacebook:(void(^)(NSDictionary *meObject, NSError *err))handler
{
    [JHFacebookLogin sharedManager].handler = handler;
    [[JHFacebookLogin sharedManager] tryLogin];
   
    // call graph me
}

+(JHFacebookLogin *)sharedManager
{
    static JHFacebookLogin * instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[JHFacebookLogin alloc] init];
    });
    return instance;
}

-(void)tryLogin
{
    self.accountStore = [[ACAccountStore alloc] init];
    ACAccountType *FBaccountType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    
    NSString *key = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"FacebookAppID"];
    NSDictionary *dictFB = [NSDictionary dictionaryWithObjectsAndKeys:key,ACFacebookAppIdKey,@[@"email",@"public_profile"],ACFacebookPermissionsKey, nil];
    
    [self.accountStore requestAccessToAccountsWithType:FBaccountType options:dictFB completion:
     ^(BOOL granted, NSError *error) {
         if (granted)
         {
             NSArray *accounts = [self.accountStore accountsWithAccountType:FBaccountType];
             self.facebookAccount = [accounts lastObject];
             
             [self.accountStore renewCredentialsForAccount:self.facebookAccount completion:^(ACAccountCredentialRenewResult renewResult, NSError *error) {

                 switch (renewResult) {
                     case ACAccountCredentialRenewResultRejected:
                     {
                         dispatch_async(dispatch_get_main_queue(), ^{
                             self.handler(nil, error);
                         });
                     }
                         break;
                     case ACAccountCredentialRenewResultFailed:
                     {
                         dispatch_async(dispatch_get_main_queue(), ^{
                             self.handler(nil, error);
                         });
                     }
                         break;
                     case ACAccountCredentialRenewResultRenewed:
                         [self get];
                         break;
                         
                     default:
                         break;
                 }
             }];
             
//             ACAccountCredential *facebookCredential = [self.facebookAccount credential];
//             NSString *accessToken = [facebookCredential oauthToken];
         }
         else {
             // this means the user has not signed-on to Facebook via the OS
//             BOOL isUntosedDevice = (!granted && error.code == ACErrorAccountNotFound);
             dispatch_async(dispatch_get_main_queue(), ^{
                 self.handler(nil, error);
             });
         }
     }];
    
}


-(void)get
{
    
    NSURL *requestURL = [NSURL URLWithString:@"https://graph.facebook.com/me"];
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@"id,name,email" forKey:@"fields"];

    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeFacebook requestMethod:SLRequestMethodGET URL:requestURL parameters:parameters];
    request.account = self.facebookAccount;
    
    [request performRequestWithHandler:^(NSData *data, NSHTTPURLResponse *response, NSError *error) {
        if (!error && [data length])
        {
            NSDictionary *list = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:list];
            dict[@"fbAccessToken"] = self.facebookAccount.credential.oauthToken;
//            NSLog(@"oauthToken: %@", self.facebookAccount.credential.oauthToken );
            dispatch_async(dispatch_get_main_queue(), ^{
                self.handler(dict, error);
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.handler(nil, error);
            });
        }
        
    }];
}

@end
