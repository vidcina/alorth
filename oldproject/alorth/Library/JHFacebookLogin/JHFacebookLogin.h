//
//  JHFacebookLogin.h
//
//  Created by John Hsu on 2016/2/19.
//

#import <Foundation/Foundation.h>
#import <Accounts/Accounts.h>
#import <Social/Social.h>

@interface JHFacebookLogin : NSObject
+(void)loginFacebook:(void(^)(NSDictionary *meObject, NSError *err))handler;

@property (nonatomic, strong) ACAccountStore *accountStore;
@property (nonatomic, strong) ACAccount *facebookAccount;
@property (nonatomic, copy) void(^handler)(NSDictionary *meObject,NSError *err);
@end
