//
//  PLPlayerKitFramework.h
//  PLPlayerKitFramework
//
//  Created by 0dayZh on 15/12/16.
//  Copyright © 2015年 pili-engineering. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for PLPlayerKitFramework.
FOUNDATION_EXPORT double PLPlayerKitFrameworkVersionNumber;

//! Project version string for PLPlayerKitFramework.
FOUNDATION_EXPORT const unsigned char PLPlayerKitFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PLPlayerKitFramework/PublicHeader.h>


#import <PLPlayerKitFramework/PLPlayer.h>