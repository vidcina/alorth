//
//  NSNull+Lenth.m
//  alorth
//
//  Created by w91379137 on 2015/12/4.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "NSNull+Length.h"

@implementation NSNull (Length)

-(NSUInteger )length
{
    NSLog(@"**** NSNull length ****");
    return 0;
}

- (NSRange)rangeOfCharacterFromSet:(NSCharacterSet *)searchSet
{
    NSLog(@"**** NSNull rangeOfCharacterFromSet ****");
    return NSMakeRange(0, 0);
}

@end
