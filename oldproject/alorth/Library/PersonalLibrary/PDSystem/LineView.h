//
//  LineView.h
//  Draw
//
//  Created by w91379137 on 2015/1/26.
//  Copyright (c) 2015年 w91379137. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface LineView : UIView

@property(nonatomic, strong) IBInspectable UIColor *fillColor;
@property(nonatomic) IBInspectable NSInteger lineWidth;
@property(nonatomic, strong) IBInspectable NSString *lineColorKey;

@end
