//
//  LineView.m
//  Draw
//
//  Created by w91379137 on 2015/1/26.
//  Copyright (c) 2015年 w91379137. All rights reserved.
//

#import "LineView.h"

@implementation LineView

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    UIColor *drawColor = self.fillColor;
    if (self.lineColorKey.length > 0) {
        
        UIColor *color = [ALColorDefinition alColorWithString:self.lineColorKey];
        if (color) {
            drawColor = color;
        }
        //try to find color by key
    }
    
    CGRect line =
    CGRectMake(- self.lineWidth / 2,
               self.bounds.size.height - self.lineWidth / 2 - 1,
               self.bounds.size.width + self.lineWidth / 2,
               self.lineWidth);
    
    
    [drawColor setFill];
    UIRectFill(line);
}

@end
