//
//  PDSIBDesignABLEView.m
//
//  Created by w91379137 on 2015/8/27.
//

#import "PDSIBDesignABLEView.h"

@implementation PDSIBDesignABLEView

#pragma mark - IB_DESIGNABLE
-(instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self xibSetup];
        [self dataSetup];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self xibSetup];
        [self dataSetup];
    }
    return self;
}

#pragma mark - Xib Setup
- (void)xibSetup
{
    UIView *view = [self loadViewFromNib];
    [self addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
}

- (UIView *)loadViewFromNib
{
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    UINib *nib = [UINib nibWithNibName:NSStringFromClass([self class]) bundle:bundle];
    UIView *view = (UIView *)[nib instantiateWithOwner:self options:nil][0];
    return view;
}

#pragma mark - Data Setup
- (void)dataSetup
{
    
}

@end
