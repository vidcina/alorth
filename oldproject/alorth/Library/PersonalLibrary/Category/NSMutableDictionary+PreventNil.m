//
//  NSMutableDictionary+PreventNil.m
//  ToyotaOrder
//
//  Created by w91379137 on 2015/1/29.
//  Copyright (c) 2015年 w91379137. All rights reserved.
//

#import "NSMutableDictionary+PreventNil.h"

@implementation NSMutableDictionary (PreventNil)

-(void)setObjectPreventNil:(NSString *)key object:(id)obj
{
    if (obj == nil) {
        NSLog(@"注意 %@ is nil",key);
    }
    else {
        [self setObject:obj forKey:key];
    }
}

-(void)setObjectxNil:(id)obj forKey:(NSString *)key
{
    //[self setObjectPreventNil:key object:obj];
    [self setObjectEmptyStringIfNil:obj forKey:key];
}

-(void)setObjectEmptyStringIfNil:(id)obj forKey:(NSString *)key
{
    if (obj) {
        [self setObject:obj forKey:key];
    }
    else {
        [self setObject:@"" forKey:key];
    }
}
@end
