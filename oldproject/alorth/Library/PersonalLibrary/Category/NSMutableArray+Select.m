//
//  NSMutableArray+Select.m
//  alorth
//
//  Created by w91379137 on 2015/6/25.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "NSMutableArray+Select.h"

@implementation NSMutableArray (Select)

-(id)searchClass:(Class)class
{
    __block NSInteger foundIndex = NSNotFound;
    [self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:class]) {
            foundIndex = idx;
            *stop = YES;
        }
    }];
    
    if (foundIndex != NSNotFound) {
        return self[foundIndex];
    }
    return nil;
}

@end
