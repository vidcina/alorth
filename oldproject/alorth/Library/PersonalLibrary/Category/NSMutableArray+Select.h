//
//  NSMutableArray+Select.h
//  alorth
//
//  Created by w91379137 on 2015/6/25.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (Select)

-(id)searchClass:(Class)aClass;

@end
