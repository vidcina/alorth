//
//  UIScrollView+MasSetContextView.h
//  alorth
//
//  Created by w91379137 on 2015/6/25.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (MasSetContextView)

-(void)masSetContentView:(UIView *)view isVerticalFix:(BOOL)isVertical;

@end
