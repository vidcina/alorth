//
//  NSMutableDictionary+PreventNil.h
//  ToyotaOrder
//
//  Created by w91379137 on 2015/1/29.
//  Copyright (c) 2015年 w91379137. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (PreventNil)

-(void)setObjectPreventNil:(NSString *)key object:(id)obj;
-(void)setObjectxNil:(id)obj forKey:(NSString *)key;
-(void)setObjectEmptyStringIfNil:(id)obj forKey:(NSString *)key;

@end
