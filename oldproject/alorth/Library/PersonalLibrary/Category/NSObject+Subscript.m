//
//  NSObject+Subscript.m
//  alorth
//
//  Created by w91379137 on 2015/11/17.
//  Copyright © 2015年 w91379137. All rights reserved.
//

#import "NSObject+Subscript.h"

@implementation NSObject (Subscript)

- (id)objectAtIndexedSubscript:(NSUInteger)idx
{
    NSLog(@"Subscript Err %@[%lu]",NSStringFromClass([self class]),(unsigned long)idx);
    return nil;
}

- (id)objectForKeyedSubscript:(id)key
{
    NSLog(@"Subscript Err %@[%@]",NSStringFromClass([self class]),key);
    return nil;
}

@end
