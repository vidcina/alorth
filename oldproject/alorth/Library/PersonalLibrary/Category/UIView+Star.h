//
//  UIView+Star.h
//  alorth
//
//  Created by w91379137 on 2015/7/14.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Star)

-(void)rankStar:(int)allStar
            get:(float)rate;

-(void)rankStar:(int)allStar
            get:(float)rate
       interval:(float)interval
           edge:(UIEdgeInsets)edge;

@end
