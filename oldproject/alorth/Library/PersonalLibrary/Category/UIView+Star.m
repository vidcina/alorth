//
//  UIView+Star.m
//  alorth
//
//  Created by w91379137 on 2015/7/14.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "UIView+Star.h"

@implementation UIView (Star)

-(void)rankStar:(int)allStar
            get:(float)rate
{
    [self rankStar:allStar get:rate interval:2 edge:UIEdgeInsetsMake(0, 0, 0, 0)];
}

-(void)rankStar:(int)allStar
            get:(float)rate
       interval:(float)interval
           edge:(UIEdgeInsets)edge
{
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    
    if (rate > allStar) {
        rate = allStar;
    }
    
    UIView *lastOne = nil;
    for (int k = 0; k < allStar; k++) {
        
        UIView *alignView = [[UIView alloc] initWithFrame:CGRectZero];
        [self addSubview:alignView];
        
        {//追加影像
            UIImageView *star = [[UIImageView alloc] initWithFrame:CGRectZero];
            [self addSubview:star];
            // 0.75 ~ 1.24 : 1星
            // 1.25 ~ 1.74 : 1.5星
            // 1.75 ~ 2.24 : 2星
            
            NSString *imageName = @"NoStar";                //預設0
            if (k + 0.25 <= rate) imageName = @"HalfStar";  //如果多過0.25 則 先給半顆
            if (k + 0.75 <= rate) imageName = @"FullStar";  //如果多過0.75 則 給全顆
            
            UIImage *image = [UIImage imageNamed:imageName];
            star.image = image;
            star.contentMode = UIViewContentModeScaleAspectFit;//圖片的比例
            star.clipsToBounds = NO;
            
            [star mas_updateConstraints:^(MASConstraintMaker *make) {
                make.center.equalTo(alignView);
                make.height.equalTo(star.mas_width).multipliedBy(image.size.width / image.size.height);//圖片的比例
                
                make.width.lessThanOrEqualTo(alignView.mas_width);
                make.height.lessThanOrEqualTo(alignView.mas_height);
            }];
        }
        
        [alignView mas_updateConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(self.mas_top).offset(edge.top);
            make.bottom.equalTo(self.mas_bottom).offset(-edge.bottom);
            
            if (lastOne == nil) {
                make.leading.equalTo(self.mas_leading).offset(edge.left);
            }
            else {
                make.leading.equalTo(lastOne.mas_trailing).offset(interval);
                make.width.equalTo(lastOne.mas_width);
            }
        }];
        lastOne = alignView;
    }
    if (lastOne) {
        [lastOne mas_updateConstraints:^(MASConstraintMaker *make) {
            make.trailing.equalTo(self.mas_trailing).offset(-edge.right);
        }];
    }
}



@end
