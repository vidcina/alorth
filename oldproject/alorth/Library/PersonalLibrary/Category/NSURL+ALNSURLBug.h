//
//  NSURL+ALNSURLBug.h
//  alorth
//
//  Created by w91379137 on 2015/9/12.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (ALNSURLBug)

+(NSURL *)URLWithBugString:(NSString *)urlStr;

@end
