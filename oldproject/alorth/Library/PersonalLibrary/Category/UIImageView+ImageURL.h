//
//  UIImageView+ImageURL.h
//
//  Created by w91379137 on 2015/4/22.
//

#import <UIKit/UIKit.h>

@interface UIImageView (ImageURL)

-(void)imageFromURLString:(NSString *)aURLString;

@end
