//
//  NSURL+ALNSURLBug.m
//  alorth
//
//  Created by w91379137 on 2015/9/12.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "NSURL+ALNSURLBug.h"

@implementation NSURL (ALNSURLBug)

+(NSURL *)URLWithBugString:(NSString *)urlStr
{
    NSURL *url = [NSURL URLWithString:urlStr];
    if (url == nil) {
        NSString *rewriteURLString = [urlStr stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        url = [NSURL URLWithString:[rewriteURLString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    }
    return url;
}

@end
