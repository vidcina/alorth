//
//  UIImageView+ImageURL.m
//
//  Created by w91379137 on 2015/4/22.
//

#import "UIImageView+ImageURL.h"
#import "UIImageView+WebCache.h"
#import "NSObject+AssociatedObject.h"

static char downloadProcessActivityIndicatorKey;

@interface UIImageView ()

@property (nonatomic, strong) UIActivityIndicatorView *aDownloadProcessActivityIndicator;

@end

@implementation UIImageView (ImageURL)

#pragma mark - Setter Getter
-(void)setADownloadProcessActivityIndicator:(UIActivityIndicatorView *)aDownloadProcessActivityIndicator
{
    [self setAssociatedObject:aDownloadProcessActivityIndicator forKey:&downloadProcessActivityIndicatorKey];
}

-(UIActivityIndicatorView *)aDownloadProcessActivityIndicator
{
    return [self associatedObject:&downloadProcessActivityIndicatorKey];
}

-(void)imageFromURLString:(NSString *)aURLString
{
    if (![aURLString isKindOfClass:[NSString class]]) {
        aURLString = @"";
    }
    
    if (self.aDownloadProcessActivityIndicator == nil) {
        self.aDownloadProcessActivityIndicator =
        [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        self.aDownloadProcessActivityIndicator.transform = CGAffineTransformMakeScale(1.6, 1.6);
    }
    
    [self.aDownloadProcessActivityIndicator startAnimating];
    [self addSubview:self.aDownloadProcessActivityIndicator];
    [self.aDownloadProcessActivityIndicator mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY);
        make.width.equalTo(@20);
        make.height.equalTo(@20);
    }];
    [self.aDownloadProcessActivityIndicator layoutIfNeeded];
    
    [self sd_setImageWithURL:[NSURL URLWithBugString:aURLString]
                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                       
                       if (image == nil) {
                           self.image = [ALNothingToSeeImage whiteNothingToSeeImage];
                       }
                       [self.aDownloadProcessActivityIndicator stopAnimating];
                       [self.aDownloadProcessActivityIndicator removeFromSuperview];
                   }];
}

//-(void)imageFromURLString:(NSString *)aURLString
//{
//    if ([ALNetWorkAPI isCachedImageFromURL:aURLString]) {
//        NSData *imageData = [ALNetWorkAPI cachedImageFromURL:aURLString];
//        self.image = [UIImage imageWithData:imageData];
//    }
//    else {
//        UIActivityIndicatorView *activityIndicator =
//        [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//        activityIndicator.center = CGPointMake(self.bounds.size.width / 2, self.bounds.size.height / 2);
//        
//        [activityIndicator startAnimating];
//        [self addSubview:activityIndicator];
//        
//        __weak UIImageView *weakSelf = self;
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//            NSData *imageData = [ALNetWorkAPI cachedImageFromURL:aURLString];
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                UIImage *imageFromData;
//                if (imageData != nil) {
//                    imageFromData = [UIImage imageWithData:imageData];
//                }
//                if (imageFromData != nil) {
//                    weakSelf.image = imageFromData;
//                }
//                [activityIndicator stopAnimating];
//                [activityIndicator removeFromSuperview];
//            });
//        });
//    }
//}

@end
