//
//  UIScrollView+MasSetContextView.m
//  alorth
//
//  Created by w91379137 on 2015/6/25.
//  Copyright (c) 2015年 miros. All rights reserved.
//

#import "UIScrollView+MasSetContextView.h"

@implementation UIScrollView (MasSetContextView)

-(void)masSetContentView:(UIView *)view isVerticalFix:(BOOL)isVertical
{
    [self addSubview:view];
    
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        
        if (isVertical) {
            make.height.equalTo(self.mas_height);
        }
        else {
            make.width.equalTo(self.mas_width);
        }
        
        make.top.equalTo(self.mas_top);
        make.bottom.equalTo(self.mas_bottom);
        make.leading.equalTo(self.mas_leading);
        make.trailing.equalTo(self.mas_trailing);
    }];
}

@end
