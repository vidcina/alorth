//
//  NSObject+AssociatedObject.h
//
//  Created by w91379137 on 2015/9/4.
//

#import <Foundation/Foundation.h>
#include <objc/runtime.h>

@interface NSObject (AssociatedObject)

//http://www.yar2050.com/2012/06/adding-properties-to-class-you-dont.html
- (void)setAssociatedObject:(id)object
                     forKey:(char *)key;

- (id)associatedObject:(char *)key;

@end
