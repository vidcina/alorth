//
//  NSObject+SafeKVo.h
//
//  Created by w91379137 on 2015/8/25.
//

#import <Foundation/Foundation.h>
#include <objc/runtime.h>

@interface NSObject (SafeKVO)

@property (nonatomic, strong) NSMutableArray *safeObserverArray;

#pragma mark -
- (void)addSafeObserver:(NSObject *)observer
             forKeyPath:(NSString *)keyPath
                options:(NSKeyValueObservingOptions)options
                context:(void *)context;

- (void)removeSafeObserver:(NSObject *)observer
                forKeyPath:(NSString *)keyPath;

@end