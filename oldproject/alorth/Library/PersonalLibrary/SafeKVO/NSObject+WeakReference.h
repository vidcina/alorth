//
//  NSObject+WeakReference.h
//
//  Created by w91379137 on 2015/9/3.
//

#import <Foundation/Foundation.h>

#pragma mark - WeakReference

typedef id (^WeakReference)(void);

@interface NSObject (WeakReference)

id WeakReferenceNonretainedObjectValue (WeakReference ref);
WeakReference MakeWeakReference (id object);

@end
