//
//  NSObject+TargetActionSecret.h
//
//  Created by w91379137 on 2015/9/4.
//

#import <Foundation/Foundation.h>
#import "NSObject+TargetAction.h"

@interface NSObject (TargetActionSecret)

- (void)addTarget:(NSObject *)target
           Action:(SEL)action
           Events:(NSInteger)events;

- (void)removeTarget:(NSObject *)target
              Action:(SEL)action
              Events:(NSInteger)events;

@end
