//
//  NSObject+WeakReference.m
//
//  Created by w91379137 on 2015/9/3.
//

#import "NSObject+WeakReference.h"

@implementation NSObject (Weak)

WeakReference MakeWeakReference (id object)
{
    __weak id weakref = object;
    return ^{ return weakref; };
}

id WeakReferenceNonretainedObjectValue (WeakReference ref)
{
    if (ref == nil)
        return nil;
    else
        return ref ();
}

@end
