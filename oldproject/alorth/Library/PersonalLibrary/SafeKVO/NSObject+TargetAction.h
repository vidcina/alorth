//
//  NSObject+TargetAction.h
//
//  Created by w91379137 on 2015/9/4.
//

#import <Foundation/Foundation.h>

#define ktarget @"target"
#define kaction @"action"
#define kevent @"event"
#define khash @"hash"

@interface NSObject (TargetAction)

@property (nonatomic, strong) NSMutableArray *targetActionArray;
- (void)wakeEvent:(NSInteger)event;

@end