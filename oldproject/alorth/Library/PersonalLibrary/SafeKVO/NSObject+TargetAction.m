//
//  NSObject+TargetAction.m
//
//  Created by w91379137 on 2015/9/4.
//

#import "NSObject+TargetAction.h"
#import "TargetActionKVO.h"

static char kTargetActionArrayKey;
@implementation NSObject (TargetAction)

#pragma mark - Setter Getter
-(void)setTargetActionArray:(NSMutableArray *)targetActionArray
{
    [self setAssociatedObject:targetActionArray forKey:&kTargetActionArrayKey];
}

-(NSMutableArray *)targetActionArray
{
    NSMutableArray *array = (NSMutableArray *)[self associatedObject:&kTargetActionArrayKey];
    if (![array isKindOfClass:[NSMutableArray class]]) {
        array = [NSMutableArray array];
        self.targetActionArray = array;
    }
    return array;
}

#pragma mark - Work
- (BOOL)isEventA:(NSInteger)eventAInteger containEventB:(NSInteger )eventBInteger
{
    //如果規則不一樣 請覆寫
    return eventAInteger == eventBInteger;
}

- (void)wakeEvent:(NSInteger)event
{
    NSMutableArray *shouldRemove = [NSMutableArray array];
    
    NSMutableArray *copyToRun = [self.targetActionArray mutableCopy];
    for (NSDictionary *registerInfoDict in copyToRun) {
        
        NSInteger events = [registerInfoDict[kevent] integerValue];
        
        if ([self isEventA:events containEventB:event]) {
            
            id target = WeakReferenceNonretainedObjectValue(registerInfoDict[@"target"]);
            NSValue *actionValue = registerInfoDict[@"action"];
            SEL action = [actionValue pointerValue];
            
            if ([target respondsToSelector:action]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                [target performSelector:action withObject:self];
#pragma clang diagnostic pop
            }
            else {
                NSLog(@"該物件 已無法實作");
                [shouldRemove addObject:registerInfoDict];
            }
        }
    }
    [self.targetActionArray removeObjectsInArray:shouldRemove];
}

@end
