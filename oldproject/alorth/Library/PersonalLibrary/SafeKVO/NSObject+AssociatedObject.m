//
//  NSObject+AssociatedObject.m
//
//  Created by w91379137 on 2015/9/4.
//

#import "NSObject+AssociatedObject.h"

@implementation NSObject (AssociatedObject)

- (void)setAssociatedObject:(id)object
                     forKey:(char *)key
{
    objc_setAssociatedObject (self, key, object, OBJC_ASSOCIATION_RETAIN);
}

- (id)associatedObject:(char *)key
{
    return (id)objc_getAssociatedObject(self, key);
}

@end
