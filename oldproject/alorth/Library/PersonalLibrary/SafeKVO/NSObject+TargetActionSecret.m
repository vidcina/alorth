//
//  NSObject+TargetActionSecret.m
//
//  Created by w91379137 on 2015/9/4.
//

#import "NSObject+TargetActionSecret.h"
#import "TargetActionKVO.h"

@implementation NSObject (TargetActionSecret)

#pragma mark - Add Remove
- (void)addTarget:(NSObject *)target
           Action:(SEL)action
           Events:(NSInteger)events
{
    if (![target respondsToSelector:action]) {
        NSLog(@"該物件無法實作 此方法");
        return;
    }
    
    if ([self sameTarget:target
                  Action:action
                  Events:events]) {
        NSLog(@"該物件已 註冊此方法");
        return;
    }
    
    NSDictionary *registerInfoDict = [NSMutableDictionary dictionary];
    
    [registerInfoDict setValue:MakeWeakReference(target) forKey:ktarget];
    [registerInfoDict setValue:[NSValue valueWithPointer:action] forKey:kaction];
    [registerInfoDict setValue:@(events) forKey:kevent];
    [registerInfoDict setValue:@(target.hash) forKey:khash];
    
    [self.targetActionArray addObject:registerInfoDict];
}

- (void)removeTarget:(NSObject *)target
              Action:(SEL)action
              Events:(NSInteger)events
{
    NSDictionary *sameDict = [self sameTarget:target
                                       Action:action
                                       Events:events];
    
    if (sameDict == nil) {
        NSLog(@"該物件並無 註冊此方法");
        return;
    }
    
    [self.targetActionArray removeObject:sameDict];
}

- (NSDictionary *)sameTarget:(NSObject *)target
                      Action:(SEL)action
                      Events:(NSInteger)events
{
    NSDictionary *sameDict = nil;
    for (NSDictionary *checkDict in self.targetActionArray) {
        
        NSUInteger hash = [checkDict[khash] integerValue];
        
        if (target.hash == hash) {
            
            NSValue *actionx = checkDict[kaction];
            
            if ([[NSValue valueWithPointer:action] isEqual:actionx]) {
                
                NSInteger event = [checkDict[kevent] integerValue];
                
                if (events == event) {
                    sameDict = checkDict;
                }
            }
        }
    }
    return sameDict;
}

@end
