//
//  YKFoundation3.h
//
//  Copyright (c) 2012-2015 Yueks Inc. All rights reserved.
//

/*
 Framework Dependencies:
 
 CoreLocation
 SystemConfiguration
 AVFoundation
 CoreMedia
 CoreMotion
 CoreVideo
 QuartzCore
 Security
  
 */
#ifndef YKFoundationDefines
#import "YKMacroFunctions.h"
#import "NSString+URLEncoding.h"
#import "YKAlert.h"
#import "YKCameraManager.h"
#import "YKRequest.h"
#import "YKSystemPaths.h"
#import "YKSystemInfo.h"
#import "UIColor+HTMLAdditions.h"
#import "NSData+YKExtension.h"
#import "YKInitialize.h"
#import "YKShakeDetector.h"
#import "YKWaitForLocationUpdate.h"
#import "YKDegradingAction.h"
#import "YKRegisterRemoteNotification.h"
#import "UIColor+HTMLAdditions.h"
#import "ZSURLProtocol.h"
#import "YKSafeKeyedSubscript.h"
#define YKFoundationDefines 1
#endif