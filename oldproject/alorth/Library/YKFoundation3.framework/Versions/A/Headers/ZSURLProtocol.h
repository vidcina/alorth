//
//  ZSURLProtocol.h
//
//  Copyright (c) 2012-2015 Yueks Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface ZSURLProtocol : NSURLProtocol

+(NSString *)addObserverForScheme:(NSString *)scheme withActionBlock:(NSData *(^)(NSURL *url))block;
+(void)removeObserverForUUID:(NSString *)uuidString;
+(NSArray *)observingArray;
+(NSData *)invokeURL:(NSURL *)url;
@end
