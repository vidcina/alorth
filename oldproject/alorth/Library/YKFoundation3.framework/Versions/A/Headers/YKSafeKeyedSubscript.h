//
//  YKSafeKeyedSubscript.h
//
//  Copyright (c) 2012-2015 Yueks Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YKSafeKeyedSubscriptArray : NSObject
@property(nonatomic,weak,readonly) NSArray *originalData;
@end
@interface YKSafeKeyedSubscriptMutableArray : NSObject
@property(nonatomic,weak,readonly) NSMutableArray *originalData;
@end
@interface YKSafeKeyedSubscriptDictionary : NSObject
@property(nonatomic,weak,readonly) NSDictionary *originalData;
@end
@interface YKSafeKeyedSubscriptMutableDictionary : NSObject 
@property(nonatomic,weak,readonly) NSMutableDictionary *originalData;
@end



@interface NSArray (YKSafeKeyedSubscript)
@property(nonatomic,readonly)id safe;
@end
@interface NSMutableArray (YKSafeKeyedSubscript)
@property(nonatomic,readonly)id safe;
@end
@interface NSDictionary (YKSafeKeyedSubscript)
@property(nonatomic,readonly)id safe;
@end
@interface NSMutableDictionary (YKSafeKeyedSubscript)
@property(nonatomic,readonly)id safe;
@end