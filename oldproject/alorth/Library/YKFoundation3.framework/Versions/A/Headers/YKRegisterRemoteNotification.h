//
//  YKRegisterRemoteNotification.h
//
//  Copyright (c) 2012-2015 Yueks Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

void YKRegisterRemoteNotification(NSSet *categories);
void YKUnRegisterRemoteNotification();
